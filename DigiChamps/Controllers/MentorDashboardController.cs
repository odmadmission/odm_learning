﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Web.Http;
using System.Web.Mvc;
using DigiChamps.Models;


namespace DigiChamps.Controllers
{
    public class MentorDashboardController : Controller
    {
        DigiChampsEntities ob = new DigiChampsEntities();
        public ActionResult Index()
        {
           
            return RedirectToAction("Login");
           
        }
        
        public ActionResult Logout()
        {
            Session["MentorLogin"] = null;
            Session.Abandon();
           
            return RedirectToAction("Login");
        }
         [HttpGet]
        public ActionResult Login()
        {
            if (Session["MentorLogin"] != null)
            {

                return RedirectToAction("Dashboard");
               
                
            }
            else
            {
                return View();
            }
        }
        public ActionResult Login(string User_name, string password)
        {
            
                 var data = ob.tbl_DC_MentorDashboardLogin.Where(x => x.Username.Equals(User_name)
                        && x.Password.Equals(password)&&x.Is_Active==true&&x.Is_Delete==false).FirstOrDefault();
                    if(data!=null)
                    {
                        Session["MentorLogin"] = "Y";
                        return RedirectToAction("Dashboard");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid credential for admin login.";
                        return View();
                    }
                
        }
        public ActionResult Dashboard()
        {
            if (Session["MentorLogin"] == null)
                return RedirectToAction("Login"); 
            else
            return View();
        }
        public ActionResult ViewExamDetails()
        {
            
            if (Request.QueryString["ResId"]==null)
            {
                return View("Index");
            }
            else
            {
                Session["ResId"] = Request.QueryString["ResId"].ToString();
            }
            return View();
        }
        public ActionResult ViewStudentDetails()
        {

            if (Request.QueryString["RegdId"] == null)
            {
                return View("Index");
            }
            else
            {
                Session["RegdId"] = Request.QueryString["RegdId"].ToString();
            }
            return View();
        }
        public class ExamDetailsCls
        {


            public int? CountAppeared { get; set; }
            public int? CountAnswered { get; set; }
            public int? slno { get; set; }
          
            //public string AcademicResult { get; set; }
            //public string SocialResult { get; set; }
            public string studentname { get; set; }
            public string mobile { get; set; }
            public DateTime? examdate { get; set; }
            public int? totalquestions { get; set; }
            public int? Attemptedques { get; set; }
            public int? NotAttemptedques { get; set; }
            public int? Correctques { get; set; }
            public int? Result_ID { get; set; }
            public int? Regd_ID { get; set; }
        }
        public class ExamQuesAnsCls
        {
            public int Result_ID { get; set; }            
            public string Question { get; set; }
            public string First_Option { get; set; }
            public string Second_Option { get; set; }
            public string Third_Option { get; set; }
            public string Fourth_Option { get; set; }
            public string Answer { get; set; }
            public string Correct_Answer { get; set; }
        }
        public ActionResult GetAllExamDetails()
        {
            try
            {
                //var regs = ob.tbl_DC_Registration.ToList();
                //var exams = ob.tbl_DC_Psychometric_Exam_Result.ToList();
                List<ExamDetailsCls> res = (from a in ob.tbl_DC_Registration
                                            let b = ob.tbl_DC_Psychometric_Exam_Result
                                                .Where(
                                                     cl => cl.Regd_ID == a.Regd_ID
                                                    )
                                                .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                                .FirstOrDefault()
                                            where b != null
                                            orderby b.Regd_ID descending                           
                           select new ExamDetailsCls
                           {
                               studentname = a.Customer_Name,
                               mobile = a.Mobile,
                               examdate = a.Modified_Date,
                               totalquestions =b.Question_Nos,
                               Attemptedques =b.Question_Attempted,
                               NotAttemptedques = b.Question_Nos - b.Question_Attempted,
                               Correctques = b.Total_Correct_Ans,
                               Result_ID = b.Result_ID,
                               Regd_ID =b.Regd_ID,


                               //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                               //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                               //            Select(x => x.Comment).FirstOrDefault(),
                               //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                               //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                               //Select(x => x.Comment).FirstOrDefault(),
                               
                           }).ToList();
                int cnt = 1;
                foreach(var a in res.ToList())
                {
                    a.slno = cnt++;
                }
                var result = new
                {
                    draw = 1,
                    recordsTotal = res.ToList().Count(),
                    recordsFiltered = res.ToList().Count(),
                    data = res.ToList().ToArray()
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetDashboardCount()
        {
            try
            {
                //var regs = ob.tbl_DC_Registration.ToList();
                //var exams = ob.tbl_DC_Psychometric_Exam_Result.ToList();
                List<ExamDetailsCls> res = (from a in ob.tbl_DC_Registration
                                            let b = ob.tbl_DC_Psychometric_Exam_Result
                                                .Where(
                                                     cl => cl.Regd_ID == a.Regd_ID
                                                    )
                                                .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                                .FirstOrDefault()
                                            where b != null
                                            orderby b.Regd_ID descending
                                            select new ExamDetailsCls
                                            {
                                                studentname = a.Customer_Name,
                                                mobile = a.Mobile,
                                                examdate = a.Modified_Date,
                                                totalquestions = b.Question_Nos,
                                                Attemptedques = b.Question_Attempted,
                                                NotAttemptedques = b.Question_Nos - b.Question_Attempted,
                                                Correctques = b.Total_Correct_Ans,
                                                Result_ID = b.Result_ID,
                                                Regd_ID = b.Regd_ID,


                                                //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                                                //            Select(x => x.Comment).FirstOrDefault(),
                                                //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                                                //Select(x => x.Comment).FirstOrDefault(),

                                            }).ToList();
                List<ExamDetailsCls> resAnswered = (from a in ob.tbl_DC_Registration
                                            let b = ob.tbl_DC_Psychometric_Exam_Result
                                                .Where(
                                                     cl => cl.Regd_ID == a.Regd_ID&&cl.Total_Correct_Ans==20
                                                    )
                                                .OrderBy(cl => cl.Result_ID) // Todo: Might need to be descending?
                                                .FirstOrDefault()
                                            where b != null
                                            orderby b.Regd_ID descending
                                            select new ExamDetailsCls
                                            {
                                                studentname = a.Customer_Name,
                                                mobile = a.Mobile,
                                                examdate = a.Modified_Date,
                                                totalquestions = b.Question_Nos,
                                                Attemptedques = b.Question_Attempted,
                                                NotAttemptedques = b.Question_Nos - b.Question_Attempted,
                                                Correctques = b.Total_Correct_Ans,
                                                Result_ID = b.Result_ID,
                                                Regd_ID = b.Regd_ID,


                                                //AcademicResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //            .Where(x => x.Pattern_Code.Equals(b.AcademicId) && x.PsychometricType == 1).
                                                //            Select(x => x.Comment).FirstOrDefault(),
                                                //SocialResult = ob.tbl_DC_Psychometric_Result_Pattern
                                                //.Where(x => x.Pattern_Code.Equals(b.InterpersonalId) && x.PsychometricType == 2).
                                                //Select(x => x.Comment).FirstOrDefault(),

                                            }).ToList();
               
           
                var result = new
                {
                    CountAppeared=res.Count,
                    CountAnswered = resAnswered.Count,
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllquestions()
        {
            int id =Convert.ToInt32(Session["ResId"].ToString());
         var Result = ob.tbl_DC_Mentor_Exam_Result.Where(x => x.Result_ID == id).FirstOrDefault();
         int? RegId = Result.Regd_ID;
            var regs = ob.tbl_DC_Registration.ToList();
            var exams = ob.tbl_DC_Mentorship_Exam_Result_Dtl.ToList();
            var ques = ob.tbl_DC_MentorshipExam_Question.ToList();
            var ans = ob.tbl_DC_MentorshipExam_Question_Answer.ToList();
            var result = (from a in exams
                          join b in ques on a.Question_ID equals b.Question_ID
                          where a.Result_ID == id
                          select new ExamQuesAnsCls
                          {
                              Result_ID=Convert.ToInt32(a.Result_ID),
                              Question=b.Question,
                              First_Option=ans.ToList().Where(m=>m.Question_ID==b.Question_ID).ToList()[0].Option_Desc,
                              Second_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[1].Option_Desc,
                              Third_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[2].Option_Desc,
                              //Fourth_Option = ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[3].Option_Desc,
                              Answer = a.Answer_ID == 0 ? "Not Answered" : ans.ToList().
                              Where(m => m.Question_ID == b.Question_ID && m.Is_Answer == true).ToList()[0].Option_Desc,
                              Correct_Answer =ans.ToList().Where(m => m.Question_ID == b.Question_ID).ToList()[0].Score+"",
                          }).ToList();

            int? RegdId = RegId;//.Convert.ToInt32(Session["RegdId"].ToString());
            var data = (from a in ob.tbl_DC_Registration.
                           Where(x => x.Regd_ID == RegdId && x.Is_Active == true && x.Is_Deleted == false)
                        join b in ob.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == RegdId)
                       on a.Regd_ID equals b.Regd_ID
                        select new DigiChamps.Models.Digichamps.Student_Profile
                        {
                            Customer_Name = a.Customer_Name,
                            Mobile = a.Mobile,
                            Image_Url = "https://learn.odmps.org/Images/Profile/" + a.Image,
                            Email = a.Email,
                            DOB = a.DateOfBirth,
                            SchoolName = ob.tbl_DC_School_Info.Where(x => x.SchoolId == a.SchoolId).
                            Select(x => x.SchoolName).FirstOrDefault(),
                            Board_Name = ob.tbl_DC_Board.Where(x => x.Board_Id == b.Board_ID).Select(x => x.Board_Name).FirstOrDefault(),
                            Class_Name = ob.tbl_DC_Class.Where(x => x.Class_Id == b.Class_ID).Select(x => x.Class_Name).FirstOrDefault(),
                            SectionName = ob.tbl_DC_Class_Section.Where(x => x.SectionId == a.SectionId).
                            Select(x => x.SectionName).FirstOrDefault(),
                            Gender = a.Gender == true ? "Male" : "Female",
                        }
                     ).FirstOrDefault();

            var AcademicResult = ob.tbl_DC_MentorshipExam_Result_Pattern
                        .Where(x => x.Pattern_Code.Equals(Result.AcademicId) && x.CategoryType == 1).
                        Select(x => x.Comment).FirstOrDefault();
            var SocialResult = ob.tbl_DC_MentorshipExam_Result_Pattern
            .Where(x => x.Pattern_Code.Equals(Result.DoubtId) && x.CategoryType == 2).
            Select(x => x.Comment).FirstOrDefault();

            var InterestResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.InterestId) && x.CategoryType == 3).
           Select(x => x.Comment).FirstOrDefault();

            var TimeResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.TimeManageId) && x.CategoryType == 4).
           Select(x => x.Comment).FirstOrDefault();

            var SelfStudyResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.SelfStudyId) && x.CategoryType == 5).
           Select(x => x.Comment).FirstOrDefault();


            var SchoolResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.SchoolId) && x.CategoryType == 6).
           Select(x => x.Comment).FirstOrDefault();


            var PersonalResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.PersonalId) && x.CategoryType == 7).
           Select(x => x.Comment).FirstOrDefault();


            var MentorshipResult = ob.tbl_DC_MentorshipExam_Result_Pattern
           .Where(x => x.Pattern_Code.Equals(Result.MentorshipId) && x.CategoryType == 8).
           Select(x => x.Comment).FirstOrDefault();

            var d = new ExamData
            {
                profile=data,
                list = result,
                AcademicResult=AcademicResult,
                SocialResult=SocialResult,
                TQ=Result.Question_Nos,
                QAT = Result.Question_Attempted,
                QANS = Result.Question_Attempted,
            };
            return Json(d, JsonRequestBehavior.AllowGet);
        }
        public class ExamData
        {
            public DigiChamps.Models.Digichamps.Student_Profile profile;
            public List<ExamQuesAnsCls> list;

            public string AcademicResult;
            public string SocialResult;

            public int? TQ;
            public int? QAT;
            public int? QANS;

        }

        public ActionResult GetStudentDetails()
        {
            int RegdId = Convert.ToInt32(Session["RegdId"].ToString());
             var data = (from a in ob.tbl_DC_Registration.
                            Where(x => x.Regd_ID == RegdId && x.Is_Active == true && x.Is_Deleted == false)
                         join b in ob.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == RegdId)
                        on a.Regd_ID equals b.Regd_ID
                        select new DigiChamps.Models.Digichamps.Student_Profile
                        {
                            Customer_Name = a.Customer_Name,
                            Mobile = a.Mobile,
                            Email = a.Email,
                            Image_Url = "https://learn.odmps.org/Images/" + a.Image,
                            DOB = a.DateOfBirth,
                            SchoolName = ob.tbl_DC_School_Info.Where(x => x.SchoolId == a.SchoolId).
                            Select(x => x.SchoolName).FirstOrDefault(),
                            Board_Name = ob.tbl_DC_Board.Where(x => x.Board_Id == b.Board_ID).Select(x => x.Board_Name).FirstOrDefault(),
                            Class_Name = ob.tbl_DC_Class.Where(x => x.Class_Id == b.Class_ID).Select(x => x.Class_Name).FirstOrDefault(),
                            SectionName = ob.tbl_DC_Class_Section.Where(x => x.SectionId == a.SectionId).
                            Select(x => x.SectionName).FirstOrDefault(),
                            Gender = a.Gender == true ? "Male" : "Female",
                        }
                      ).FirstOrDefault();

              return Json(data, JsonRequestBehavior.AllowGet);

        }
    }
}
