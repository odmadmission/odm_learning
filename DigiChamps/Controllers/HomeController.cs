﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace DigiChamps.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        DigiChampsEntities DbContext = new DigiChampsEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ForgotPassword()
        {
            return View();
        }
        public ActionResult Index2()
        {
            return View();
        }
        public ActionResult About_Us()
        {
            return View();
        }
        public ActionResult TermsAndConditions()
        {
            return View();
        }
        public ActionResult PrivacyPolicy()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ContactUs(string name, string mail, string subject, string message)
        {
            string msg = string.Empty;
            try
            {
                tbl_DC_Contactus cv = new tbl_DC_Contactus();
                cv.Email = mail;
                cv.Is_Active = true;
                cv.Is_Deleted = false;
                cv.Message = message;
                cv.Name = name;
                cv.Subject = subject;
                DbContext.tbl_DC_Contactus.Add(cv);
                DbContext.SaveChanges();
                StringBuilder sb = new StringBuilder();
                sb.Append("<body style='margin: 0; padding: 0; border: none;'>");
                sb.Append("<div style='width: 650px; float: left; border: 5px solid #CCC;color:rgba(255, 255, 255, 0.84); font-family: Arial, Helvetica, sans-serif; font-size: 13px; line-height: 22px; padding: 0; background-color: #36a8f5;'>");
                sb.Append("<div style='width: 100%; float: left; background-color: #fff; padding: 10px 0px;'>");
                sb.Append("<div style='margin-left: 20px; margin-right: 20px; background-color: #fff; padding: 0;text-align: center;'>");
                sb.Append("<a href='http://learn.odmps.org'><img src='http://learn.odmps.org/Student_assets/images/digi-champ-logo.png' width='310' height='88' alt='DigiChamps' /></a>");

                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<!-- background pic change  -->");
                sb.Append("<div style='width: 100%; background-position: center bottom; padding: 0; border: none; float: left; background-size: cover'>");
                sb.Append("<div style='margin-left: 20px; margin-right: 20px;'>");
                sb.Append("<p>");
                sb.Append("<strong>Dear ADMIN,</strong></p>");
                sb.Append("<p>");
                sb.Append(" Greetings from DigiChamps !<br/>");
                sb.Append("<span>Some one wants to contact Digichamps, Below are the provided details :</span></p>");
                sb.Append("<p>");
                sb.Append("<strong>Mr/Ms " + name + "'</strong></p>");
                sb.Append("<p>");
                sb.Append("<br/> Email : <span style='color: white'> " + mail + "</span> <br/>  Subject : <span style='color: white'> " + subject + " </span>.<br />Message : <span style='color: white'> " + message + "</span> </p>");
                sb.Append("<p>");
                sb.Append("<p><a style='font-style:oblique; color:#bbd6ff; font-size:17px;' href='http://learn.odmps.org/' title='DigiChamps'> learn.odmps.org </a></p>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("<div style='width: 100%; background-color: #1f2429; float: left; color: #cecece;'>");
                sb.Append("<div style='margin-left: 20px; margin-right: 20px;'>");
                sb.Append("<p style='text-align:center;'>");
                sb.Append("Regards<br />Support , DigiChamps </p>");
                sb.Append("</div>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<div style='clear: both'>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("</body>");

                DMLhelper.sendMail1("info@learn.odmps.org", mail, "Regarding Contact", sb.ToString());
                msg = "2";
                LeadsSquaredAPI obj = new Models.LeadsSquaredAPI();
                bool chk = obj.submitQueryAPI(mail, "", name, "");
                return Json(msg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                msg = "3";

            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Team()
        {
            return View();
        }
        public ActionResult Test()
        {
            return View();
        }
        public class CareerCls
        {
            public string career_Name { get; set; }
            public Nullable<int> No_of_vacancy { get; set; }
            public string Experience { get; set; }
            public string Location { get; set; }
            public string Qualification { get; set; }
            public string openingdate { get; set; }
            public string enddate { get; set; }
            public string Walk_in_Time { get; set; }
            public string Phone { get; set; }
            public string Job_Description { get; set; }
        }
        public ActionResult Career()
        {
            var o = (from a in DbContext.tbl_DC_Career.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList()
                     select new CareerCls
                     {
                         Phone = a.Phone,
                         openingdate = a.Opening_Date == null ? "" : Convert.ToDateTime(a.Opening_Date).ToString("dd/MM/yyyy"),
                         No_of_vacancy = a.No_of_vacancy,
                         Qualification = a.Qualification,
                         Walk_in_Time = a.Walk_in_Time,
                         Location = a.Location,
                         Job_Description = a.Job_Description,
                         Experience = a.Experience,
                         enddate = a.Close_Date == null ? "" : Convert.ToDateTime(a.Close_Date).ToString("dd/MM/yyyy"),
                         career_Name = a.career_Name
                     }).ToList();
            ViewBag.CareerList = o;
            return View();
        }
        public ActionResult RefundsAndCancellation()
        {
            return View();
        }
        public ActionResult Pricing()
        {
            return View();
        }
        public ActionResult Schools()
        {
            return View();
        }
        public ActionResult Learnnearn()
        {
            return View();
        }

        public JsonResult Mobileno_Verify(string mobile)
        {
            string msg = string.Empty;
            try
            {
                if (mobile != "")
                {
                    var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (mob == null)
                    {
                        string otp = random_password(6);
                        Session["otp_det"] = otp;
                        Sendsms("REG", otp.Trim(), mobile.Trim());
                        Session["phone"] = mobile;
                        msg = "1";
                    }
                    else
                    {
                        msg = "-1";//Already Exist Mobile Number
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                msg = "-2";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Forgot_Mobile_Verify(string mobile)
        {
            string msg = string.Empty;
            string otp_data = string.Empty;
            try
            {
                if (mobile != "")
                {
                    var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (mob != null)
                    {
                        int rgt_id = mob.Regd_ID;
                        Session["Reg_Id"] = mob.Regd_ID;
                        string usercode = mobile;
                        var chk_type = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == mobile && x.ROLE_CODE == "C").FirstOrDefault();

                        if (chk_type != null)
                        {
                            var widget = DbContext.tbl_DC_OTP.Where(x => x.Regd_Id == rgt_id).FirstOrDefault();
                            if (widget != null)
                            {
                                string otp = random_password(6);
                                widget.OTP = otp;
                                otp_data = otp;

                                widget.Count = widget.Count + 1;
                                widget.From_Date = DateTime.Now;
                                widget.To_Date = Convert.ToDateTime(DateTime.Now.AddHours(1));
                                DbContext.Entry(widget).State = EntityState.Modified;
                                DbContext.SaveChanges();
                                Sendsms("FRGPASS", otp.Trim(), widget.Mobile.Trim());
                                msg = "1"; // successfully otp send
                            }
                            else
                            {
                                string otp = random_password(6);
                                widget.OTP = otp;
                                otp_data = otp;
                                widget = new tbl_DC_OTP();
                                widget.Regd_Id = rgt_id;
                                widget.Mobile = mobile;

                                widget.OTP = otp;
                                widget.Count = 1;
                                widget.From_Date = DateTime.Now;
                                widget.To_Date = Convert.ToDateTime(DateTime.Now.AddHours(1));
                                DbContext.Entry(widget).State = EntityState.Modified;
                                DbContext.SaveChanges();
                                Sendsms("FRGPASS", otp.Trim(), widget.Mobile.Trim());
                                msg = "1"; // successfully otp send
                               // msg = "-3";// OTP confirmation not checked
                            }
                        }
                        else
                        {
                            msg = "-4"; // user is not active
                        }
                        Session["phone"] = mobile;
                        Session["otp_det"] = otp_data;
                    }
                    else
                    {
                        msg = "-1";// Mobile Number is Not Exist
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                msg = "-2"; //Error
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult OTP_Check(string otp)
        {
            string msg = string.Empty;
            string old_otp = Session["otp_det"].ToString();
            if (old_otp == otp)
            {
                msg = "1";
            }
            else
            {
                msg = "-1";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Changed_Password(string Mobile_OTP, string New_Password, string Confirm_Password)
        {
            string msg = string.Empty; ;
            try
            {
                if (Session["Reg_Id"] != null)
                {
                    #region Forgot password OTP
                    int reg_id = Convert.ToInt32(Session["Reg_Id"].ToString());
                    var to_fetch_reg = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == reg_id).FirstOrDefault();
                    string ucode = to_fetch_reg.Mobile;

                    var data = DbContext.tbl_DC_OTP.Where(x => x.OTP == Mobile_OTP && x.Regd_Id == reg_id).FirstOrDefault();

                    if (data != null)
                    {
                        if (New_Password == Confirm_Password)
                        {
                            tbl_DC_USER_SECURITY obj = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == ucode && x.ROLE_CODE == "C").FirstOrDefault();
                            string status = obj.STATUS;
                            obj.USER_NAME = to_fetch_reg.Mobile;
                            obj.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword(New_Password).ToString();
                            obj.STATUS = "A";
                            obj.IS_ACCEPTED = true;
                            DbContext.Entry(obj).State = EntityState.Modified;
                            DbContext.SaveChanges();

                            to_fetch_reg.Mobile = to_fetch_reg.Mobile;
                            to_fetch_reg.Is_Active = true;
                            to_fetch_reg.Is_Deleted = false;
                            DbContext.Entry(to_fetch_reg).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            Session["Rg_Id"] = reg_id;
                            if (status == "D")
                            {
                                msg = "11";   //return RedirectToAction("choose_board");
                            }
                            else if (status == "A")
                            {
                                msg = "12";   //return RedirectToAction("Login");
                            }
                        }
                        else
                        {
                            msg = "0";
                        }
                    }

                    else
                    {
                        msg = "-1";
                    }
                    #endregion
                }
                else
                {
                    msg = "-3";//Invalid OTP details
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                msg = "-2";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SignUpDetails(string name, string email, string password, string school, int board, int classd, string section)
        {
            string msg = string.Empty;
            try
            {
                string mobile = Session["phone"].ToString();
                if (mobile != "" && name != "")
                {
                    var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    if (mob == null)
                    {
                        tbl_DC_Registration dr = new tbl_DC_Registration();
                        dr.Mobile = mobile;
                        dr.Customer_Name = name;
                        dr.Email = email;
                        var d = DbContext.tbl_DC_School_Info.Where(a => a.SchoolName == school.Trim() && a.IsActive == true).FirstOrDefault();
                        if (d == null)
                        {
                            //tbl_DC_School_Info od = new tbl_DC_School_Info();
                            //od.SchoolName = school;
                            //od.SchoolId = Guid.NewGuid();
                            //od.IsActive = true;
                            //od.CreationDate = DateTime.Now;
                            //od.modifiedDate = DateTime.Now;
                            //DbContext.tbl_DC_School_Info.Add(od);
                            //DbContext.SaveChanges();
                            //dr.SchoolId = od.SchoolId;
                            //tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                            //var sectionId = Guid.NewGuid();
                            //objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                            //objtbl_DC_Class_Section.IsActive = true;
                            //objtbl_DC_Class_Section.SectionId = sectionId;
                            //objtbl_DC_Class_Section.SectionName = Convert.ToString(section);
                            //objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(classd);
                            //objtbl_DC_Class_Section.School_Id = od.SchoolId;
                            //DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                            //DbContext.SaveChanges();
                            //dr.SectionId = sectionId;
                            dr.Organisation_Name = "Other-" + school + "-" + section;
                        }
                        else
                        {
                            dr.SchoolId = d.SchoolId;
                            var h = DbContext.tbl_DC_Class_Section.Where(a => a.School_Id == d.SchoolId && a.Class_Id == classd && a.SectionName == section).FirstOrDefault();
                            if (h == null)
                            {
                                tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                                var sectionId = Guid.NewGuid();
                                objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                                objtbl_DC_Class_Section.IsActive = true;
                                objtbl_DC_Class_Section.SectionId = sectionId;
                                objtbl_DC_Class_Section.SectionName = Convert.ToString(section);
                                objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(classd);
                                objtbl_DC_Class_Section.School_Id = d.SchoolId;
                                DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                                DbContext.SaveChanges();
                                dr.SectionId = sectionId;
                            }
                            else
                            {
                                dr.SectionId = h.SectionId;
                            }

                        }
                        dr.Is_Active = true;
                        dr.Is_Deleted = false;
                        dr.Inserted_By = HttpContext.User.Identity.Name;
                        dr.Inserted_Date = DateTime.Now;
                        DbContext.tbl_DC_Registration.Add(dr);
                        DbContext.SaveChanges();

                        var regno2 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();
                        tbl_DC_Registration_Dtl m = new tbl_DC_Registration_Dtl();
                        m.Regd_ID = regno2.ToList()[0].Regd_ID;
                        m.Regd_No = regno2.ToList()[0].Regd_No;
                        m.Board_ID = board;
                        m.Class_ID = classd;
                        m.Inserted_Date = DateTime.Now;
                        m.Inserted_By = regno2.ToList()[0].Regd_ID.ToString();
                        m.Is_Active = true;
                        m.Is_Deleted = false;
                        DbContext.tbl_DC_Registration_Dtl.Add(m);
                        DbContext.SaveChanges();

                        int reg_num1 = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                        var pri = DbContext.tbl_DC_Prefix.Where(x => x.PrefixType_ID == 2).Select(x => x.Prefix_Name).FirstOrDefault();
                        string prifix = pri.ToString();

                        //registration no. of student autogenerate
                        if (reg_num1 == 0)
                        {
                            dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + 1;
                        }
                        else
                        {
                            int regnum = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                            if (regnum > 0 && regnum <= 9)
                            {
                                dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + Convert.ToString(regnum);
                            }
                            if (regnum > 9 && regnum <= 99)
                            {
                                dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0000" + Convert.ToString(regnum);
                            }
                            if (regnum > 99 && regnum <= 999)
                            {
                                dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "000" + Convert.ToString(regnum);
                            }
                            if (regnum > 999 && regnum <= 9999)
                            {
                                dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00" + Convert.ToString(regnum);
                            }
                            if (regnum > 9999 && regnum <= 99999)
                            {
                                dr.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0" + Convert.ToString(regnum);
                            }
                        }
                        DbContext.Entry(dr).State = EntityState.Modified;
                        DbContext.SaveChanges();

                        //-----------Add details to User-Security Table
                        tbl_DC_USER_SECURITY obj1 = new tbl_DC_USER_SECURITY();
                        obj1.USER_NAME = mobile;
                        obj1.ROLE_CODE = "C";
                        obj1.ROLE_TYPE = 1;
                        obj1.USER_CODE = mobile;
                        obj1.IS_ACCEPTED = false;                               //-------------To Change---------------------
                        obj1.STATUS = "D";                                      //--------------To Change------------------
                        string new_pass_word = CreateRandomPassword(8);         //--------------random password------------------
                        obj1.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword(new_pass_word).ToString();
                        DbContext.tbl_DC_USER_SECURITY.Add(obj1);
                        DbContext.SaveChanges();

                        //------------Add details To OTP TAble
                        tbl_DC_OTP obj2 = new tbl_DC_OTP();
                        var regno1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();
                        obj2.Regd_Id = Convert.ToInt32(regno1.ToList()[0].Regd_ID);
                        obj2.Mobile = mobile;

                        string otp = random_password(6);
                        obj2.OTP = Session["otp_det"].ToString();
                        obj2.From_Date = DateTime.Now;
                        obj2.To_Date = Convert.ToDateTime(DateTime.Now.AddHours(1));
                        obj2.Count = 1;
                        DbContext.tbl_DC_OTP.Add(obj2);
                        DbContext.SaveChanges();


                        string ucode = mobile;
                        tbl_DC_USER_SECURITY obj4 = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == ucode).FirstOrDefault();
                        obj4.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword(password).ToString();
                        obj4.STATUS = "A";
                        obj4.IS_ACCEPTED = true;
                        DbContext.Entry(obj4).State = EntityState.Modified;
                        //FormsAuthentication.RedirectFromLoginPage(obj4.USER_NAME, false);
                        DbContext.SaveChanges();
                        int regd = regno1.ToList()[0].Regd_ID;
                        tbl_DC_Registration obj5 = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regd).FirstOrDefault();
                        obj5.Is_Active = true;
                        obj5.Is_Deleted = false;
                        obj5.Modified_Date = DateTime.Now;
                        DbContext.Entry(obj5).State = EntityState.Modified;
                        DbContext.SaveChanges();



                        //Sendsms("REG", otp.Trim(), mobile.Trim());

                        Session["Rg_Id"] = obj2.Regd_Id;

                        //session for student PreBook()
                        Session["fname"] = name.Trim();
                        Session["email"] = email;
                        Session["phone"] = mobile;
                        string[] names = name.Split(' ');
                        string fname = string.Empty;
                        string lname = string.Empty;
                        LeadsSquaredAPI obj = new Models.LeadsSquaredAPI();
                        if (names.Count() > 1)
                        {
                            fname = names[0];
                            lname = names[1];

                        }
                        else { fname = names[0]; }

                        msg = "1"; //Thank you for Signing up, Conform OTP to continue.
                        if (email != null && email != "")
                        {
                            var getall = DbContext.SP_DC_Get_maildetails("S_REG").FirstOrDefault();
                            if (getall != null)
                            {
                                string msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{OTP}}", otp);
                                // sendMail1("S_REG", email, "Welcome to Digichamps", name, msgbody);
                            }
                        }

                        bool chk = obj.submitQueryAPI(email, mobile, fname, lname);
                    }
                    else
                    {
                        msg = "0"; //Mobile no. already exist.

                    }
                }
                else
                {
                    msg = "-1"; //Please enter name and mobile number.
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message.ToString();
                msg = "-2";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SigninDetails(string User_name, string password)
        {
            string msg = string.Empty;
            try
            {
                string pass_word = DigiChampsModel.Encrypt_Password.HashPassword(password);
                var obj = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_NAME == User_name).FirstOrDefault();

                if (obj != null)
                {
                    if (obj.PASSWORD == pass_word)
                    {
                        if (obj.STATUS == "A")
                        {
                            if (obj.ROLE_CODE == "C")
                            {
                                if (obj.IS_ACCEPTED == true)
                                {
                                    Session["USER_CODE"] = obj.USER_CODE;
                                    Session["USER_NAME"] = obj.USER_NAME;
                                    Session["ROLE_CODE"] = obj.ROLE_CODE;
                                    TempData["USER_NAME"] = obj.USER_NAME;
                                    Session["Time"] = DateTime.Now.ToShortTimeString();
                                    ///  FormsAuthentication.RedirectFromLoginPage(obj.USER_NAME, false);
                                    int sreg_id = DbContext.tbl_DC_Registration.Where(a => a.Mobile == User_name).FirstOrDefault().Regd_ID;
                                    var chkbrd_cls = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == sreg_id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    //if (chkbrd_cls == null)
                                    //{
                                    //    msg = "0"; //choose board
                                    //}
                                    //else
                                    //{
                                    Session["class"] = chkbrd_cls.Class_ID;
                                    msg = "1"; //board already there
                                    // }
                                }
                                else
                                {
                                    msg = "-5";
                                }
                            }
                            else
                            {
                                msg = "-6";//Please login as a student.
                            }
                        }
                        else
                        {
                            msg = "-1"; //otp conformation
                        }
                    }
                    else
                    {
                        msg = "-3"; // "Password doesnot match.";
                    }
                }
                else
                {
                    msg = "-4";   // "User name is not yet registered.";
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                msg = "-2";
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
        public bool Sendsms(string type, string opt, string mobile)
        {
            try
            {
                string message = "OTP for Digichamps is {{otpno}}. Please use this OTP to Reset your password. OTP expires in 1 hour. DIGICHAMPS";
                var regex = new Regex(Regex.Escape("{{otpno}}"));
                var newText = regex.Replace(message, opt, 9);
                //string baseurl = "http://bulksms.sgcinfoways.com/sendSMS?username=thedigichamps&message="
                //  +newText+"&sendername=DCHAMP&smstype=TRANS&numbers="+mobile+"&apikey=bfb13300-eeb0-46ae-b491-5d0149b2d7d0";

                string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=DIGICH&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
                + "&country=91&message=" + newText;

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);
                //Get response from the SMS Gateway Server and read the answer
                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                //}
                return true;// Request.CreateResponse(HttpStatusCode.OK, newText);
                //var sms_obj = DbContext.View_DC_SMS_API.Where(x => x.Sms_Alert_Name == type).FirstOrDefault();
                //if (sms_obj != null)
                //{
                //    string message = sms_obj.Sms_Body;
                //    var regex = new Regex(Regex.Escape("{{otpno}}"));
                //    var newText = regex.Replace(message, opt, 9);
                //    string baseurl = "" + sms_obj.Api_Url.ToString().Replace("mobile", mobile).Replace("message", newText);

                //    HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);
                //    //Get response from the SMS Gateway Server and read the answer
                //    HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                //    System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                //    string responseString = respStreamReader.ReadToEnd();
                //    respStreamReader.Close();
                //    myResp.Close();
                //}
                //return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool sendMail1(string parameter, string email, string sub, string name, string msgbody)
        {
            var getall = DbContext.SP_DC_Get_maildetails(parameter).FirstOrDefault();
            string eidTo = email;
            string mailtoshow = getall.SMTP_Email.ToString();
            string eidFrom = getall.SMTP_User.ToString();
            string password = getall.SMTP_Pwd.ToString();
            string msgsub = sub;
            string hostname = getall.SMTP_HostName;
            string portname = getall.SMTP_Port.ToString();
            bool ssl_tof = true;
            //string msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name);
            MailMessage greetings = new MailMessage();
            SmtpClient smtp = new SmtpClient();
            try
            {
                greetings.From = new MailAddress(mailtoshow, "DIGICHAMPS");//sendername
                greetings.To.Add(eidTo);//to whom
                greetings.IsBodyHtml = true;
                greetings.Priority = MailPriority.High;
                greetings.Body = msgbody;
                greetings.Subject = msgsub;
                smtp.Host = hostname;//host name
                smtp.EnableSsl = ssl_tof;//ssl
                smtp.Port = Convert.ToInt32(portname);//port
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(eidFrom, password);//from(user)//password
                smtp.Send(greetings);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        protected string random_password(int length)
        {
            try
            {
                const string valid = "1234567890";
                StringBuilder res = new StringBuilder();
                Random rnd = new Random();
                while (0 < length--)
                {
                    res.Append(valid[rnd.Next(valid.Length)]);
                }
                return res.ToString();
            }
            catch (Exception)
            {
                return null;
            }
        }
        public JsonResult GetSchools()
        {
            var s = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBoard()
        {
            var s = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClass(int id)
        {
            var s = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Board_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BookCounselling(string name, int board, int classs, string mobile, string email, DateTime Booking_Date, TimeSpan Booking_Time)
        {
            tbl_DC_Counselling k = new tbl_DC_Counselling();
            k.Full_Name = name;
            k.Class_Id = classs;
            k.Email_Id = email;
            k.Inserted_Date = DateTime.Now;
            k.Is_Active = true;
            k.Phone_no = mobile;
            k.Booking_Date = Booking_Date;
            k.Booking_Time = Booking_Time;
            DbContext.tbl_DC_Counselling.Add(k);
            DbContext.SaveChanges();
            
            string message = "Thank you " + name + " for booking a session. Our team will get in touch with you shortly.";

            string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=DIGICH&route=4&mobiles=" + mobile + "&authkey=232520Aycx7OOpF5b790d71"
            + "&country=91&message=" + message;

            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);
            //Get response from the SMS Gateway Server and read the answer
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAppLink(string mob)
        {
            string message = "Welcome to DIGICHAMPS. We are glad to on board you for an truly amazing learning experience on our app. Please click on this link";
            message += " https://play.google.com/store/apps/details?id=com.sseduventures.digichamps  and download our app now. Happy Learning";

            string baseurl = "http://api.msg91.com/api/sendhttp.php?sender=DIGICH&route=4&mobiles=" + mob + "&authkey=232520Aycx7OOpF5b790d71"
            + "&country=91&message=" + message;

            HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);
            //Get response from the SMS Gateway Server and read the answer
            HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
            System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            string responseString = respStreamReader.ReadToEnd();
            respStreamReader.Close();
            myResp.Close();
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPackageCategory(int bid, int cid)
        {
            var j = DbContext.tbl_DC_PackageCategory.Where(a => a.Is_Active == true && a.Is_Delete == false).ToList();
            var m = DbContext.tbl_DC_Package.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var s = DbContext.tbl_DC_Package_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var t = (from a in j
                     join b in m on a.PackageType_ID equals b.Type
                     join c in s on b.Package_ID equals c.Package_ID
                     where c.Board_Id == bid && c.Class_Id == cid
                     select new
                     {
                         a.PackageType_ID,
                         a.PackageType_Name,
                         a.Validity,
                         b.Is_Doubt,
                         b.Is_Mentor,
                         b.Is_Video,
                         b.Price,
                         b.Subscripttion_Period,
                         c.Board_Id,
                         c.Class_Id
                     }).ToList().Distinct();
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPackages()
        {
            var m = DbContext.tbl_DC_PackageCategoryType.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var j = DbContext.tbl_DC_PackageCategory.Where(a => a.Is_Active == true && a.Is_Delete == false).ToList();
            var t = (from a in m
                     join b in j on a.Packagecategory_Id equals b.Packagecategory_Id
                     select new
                     {
                         a.Packagecategory,
                         a.Packagecategory_Id,
                         b.PackageType_Name,
                         b.Validity,
                         b.PackageType_ID
                     }).ToList();
            return Json(t, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getnewsletter(string mail)
        {
            tbl_DC_Newsletter n = new tbl_DC_Newsletter();
            n.Newsletter_MailId = mail;
            n.Is_Active = true;
            n.Is_Delete = false;
            DbContext.tbl_DC_Newsletter.Add(n);
            DbContext.SaveChanges();
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTeamList()
        {
            var o = DbContext.tbl_DC_Our_Team.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            return Json(o, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCareerList()
        {
            var o = (from a in DbContext.tbl_DC_Career.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList()
                     select new
                     {
                         a.Phone,
                         openingdate = a.Opening_Date == null ? "" : Convert.ToDateTime(a.Opening_Date).ToString("dd/MM/yyyy"),
                         a.No_of_vacancy,
                         a.Qualification,
                         a.Walk_in_Time,
                         a.Location,
                         a.Job_Description,
                         a.Experience,
                         enddate = a.Close_Date == null ? "" : Convert.ToDateTime(a.Close_Date).ToString("dd/MM/yyyy"),
                         a.career_Name
                     }).ToList();

            return Json(o, JsonRequestBehavior.AllowGet);
        }
        public ActionResult PaymentDetails()
        {
            return View();
        }
        public JsonResult Getpackagedetailslist(int id, int brd, int cls)
        {
            var catg = DbContext.tbl_DC_PackageCategory.Where(a => a.Is_Active == true && a.Is_Delete == false && a.Packagecategory_Id == id).ToList();
            var pkg = DbContext.tbl_DC_Package.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var pkg_dtl = (from a in DbContext.tbl_DC_Package_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList()
                           select new
                           {
                               a.Package_ID,
                               a.Board_Id,
                               a.Class_Id
                           }).Distinct().ToList();
            var res = (from a in catg
                       join b in pkg on a.PackageType_ID equals b.Type
                       join c in pkg_dtl on b.Package_ID equals c.Package_ID
                       where c.Class_Id == cls && c.Board_Id == brd
                       select b).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public class Package_Buy_class
        {
            public int Package_ID { get; set; }
            public string Package_Name { get; set; }
            public string Package_Desc { get; set; }
            public Nullable<decimal> Price { get; set; }
            public Nullable<int> Total_Chapter { get; set; }
            public Nullable<int> Subscripttion_Period { get; set; }
            public Nullable<bool> Is_School { get; set; }
            public Nullable<bool> Is_Doubt { get; set; }
            public Nullable<bool> Is_Mentor { get; set; }
            public Nullable<bool> Is_Dictionary { get; set; }
            public Nullable<bool> Is_Feed { get; set; }
            public Nullable<bool> Is_Video { get; set; }
            public Nullable<int> Type { get; set; }
            public string Classes { get; set; }
            public string Board { get; set; }
            public List<Subject_Det_Class> subjects { get; set; }

        }
        public class Chapter_Det_Class
        {
            public string Chapter { get; set; }
        }
        public class Subject_Det_Class
        {
            public string Subject { get; set; }
            public List<Chapter_Det_Class> chapters { get; set; }
        }
        public ActionResult checkcoupon(string id)
        {
            try
            {
                var cop = DbContext.tbl_DC_CouponCode.Where(x => x.Coupon_Code == id && x.Valid_From <= DateTime.Now && x.Valid_To >= DateTime.Now && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
                if (cop != null)
                {
                    if (cop.Pricerange_From != null && cop.Pricertange_To != null)
                    {
                        if (Session["prc"] != null)
                        {
                            decimal total = Convert.ToDecimal(Session["prc"]);
                            if (cop.Is_Default == false && total >= cop.Pricerange_From && total <= cop.Pricertange_To)
                            {
                                return Json(cop, JsonRequestBehavior.AllowGet);
                            }
                            else if (cop.Is_Default == true)
                            {
                                return Json(cop, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json("", JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json("", JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(cop, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public int converttoint(int? id)
        {
            return Convert.ToInt32(id);
        }
        public void CreateCartdetails(int id, string price)
        {
            CartController.addtocartmodele nn = new CartController.addtocartmodele();
            DigiChamps.Controllers.CartController.addtocartmodel mn = new CartController.addtocartmodel();
            List<CartController.chkpackage> pp = (from a in DbContext.tbl_DC_Package_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Package_ID == id).ToList()
                                                  select new CartController.chkpackage
                                                  {
                                                      chpter_id = a.Chapter_Id != null ? converttoint(a.Chapter_Id) : 0
                                                  }).ToList();
            mn.chkpackage = pp;

            string us_name = Session["USER_NAME"].ToString();
            var rdata = DbContext.tbl_DC_Registration.Where(x => x.Mobile == us_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

            int regdid = Convert.ToInt32(rdata.Regd_ID);
            mn.id = regdid;
            mn.pkid = id.ToString();
            mn.prc = price;
            mn.lmt = mn.chkpackage.Count().ToString();
            nn.chkpackaged = mn;
            var cart = DbContext.tbl_DC_Cart.Where(a => a.Package_ID == id && a.Regd_ID == regdid && a.Is_Active==true && a.Is_Deleted==false && a.Is_Paid==false).ToList();
            if (cart.Count == 0)
            {
                if (nn.chkpackaged != null)
                {
                    var data1 = nn.chkpackaged.chkpackage.ToList();
                    int u_name = Convert.ToInt32(nn.chkpackaged.id);
                    int pkgid = Convert.ToInt32(nn.chkpackaged.pkid);
                    var prices = DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    int limit = Convert.ToInt32(nn.chkpackaged.lmt);
                    decimal price1 = Convert.ToDecimal(prices.Price);
                    var data = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == u_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

                    int id1 = Convert.ToInt32(data.Regd_ID);
                    string regno = rdata.Regd_No;

                    var ord = DbContext.tbl_DC_Cart.Where(x => x.Regd_ID == u_name && x.Status == true && x.In_Cart == true && x.Is_Paid == false).FirstOrDefault();

                    tbl_DC_Cart crt = new tbl_DC_Cart();
                    crt.Package_ID = pkgid;
                    crt.Regd_ID = id1;
                    crt.Regd_No = regno;
                    crt.Status = true;
                    crt.In_Cart = true;
                    crt.Is_Paid = false;
                    crt.Is_Active = true;
                    crt.Is_Deleted = false;
                    crt.Total_Amt = Math.Round(price1);
                    crt.Inserted_Date = DateTime.Now;
                    DbContext.tbl_DC_Cart.Add(crt);
                    DbContext.SaveChanges();

                    if (ord != null)
                    {
                        crt.Order_ID = ord.Cart_ID;
                        DateTime today = DateTime.Now.Date;
                        var ordid = crt.Order_ID;
                        if (ordid == null)
                        {
                            crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + 1;
                        }
                        else
                        {
                            int ordno = Convert.ToInt32(crt.Order_ID);
                            if (ordno > 0 && ordno <= 9)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + Convert.ToString(ordno);
                            }
                            if (ordno > 9 && ordno <= 99)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0000" + Convert.ToString(ordno);
                            }
                            if (ordno > 99 && ordno <= 999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "000" + Convert.ToString(ordno);
                            }
                            if (ordno > 999 && ordno <= 9999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00" + Convert.ToString(ordno);
                            }
                            if (ordno > 9999 && ordno <= 99999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0" + Convert.ToString(ordno);
                            }
                        }
                        DbContext.SaveChanges();
                    }
                    else
                    {
                        crt.Order_ID = crt.Cart_ID;
                        DateTime today = DateTime.Now.Date;
                        var ordid = crt.Order_ID;
                        if (ordid == null)
                        {
                            crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + 1;
                        }
                        else
                        {
                            int ordno = Convert.ToInt32(crt.Order_ID);
                            if (ordno > 0 && ordno <= 9)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + Convert.ToString(ordno);
                            }
                            if (ordno > 9 && ordno <= 99)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0000" + Convert.ToString(ordno);
                            }
                            if (ordno > 99 && ordno <= 999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "000" + Convert.ToString(ordno);
                            }
                            if (ordno > 999 && ordno <= 9999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00" + Convert.ToString(ordno);
                            }
                            if (ordno > 9999 && ordno <= 99999)
                            {
                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0" + Convert.ToString(ordno);
                            }
                        }
                        DbContext.SaveChanges();
                    }

                    for (int i = 0; i < data1.Count; i++)
                    {
                        int chp_id = Convert.ToInt32(data1.ToList()[i].chpter_id);

                        var data2 = (from a in DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == chp_id && x.Is_Active == true && x.Is_Deleted == false)
                                     join b in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                     on a.Subject_Id equals b.Subject_Id
                                     join c in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                     on b.Class_Id equals c.Class_Id
                                     join d in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                     on c.Board_Id equals d.Board_Id
                                     select new DigiChampCartModel
                                     {
                                         Board_ID = d.Board_Id,
                                         Class_ID = c.Class_Id,
                                         Subject_ID = b.Subject_Id,
                                         Chapter_ID = a.Chapter_Id
                                     }).FirstOrDefault();
                        tbl_DC_Cart_Dtl obj1 = new tbl_DC_Cart_Dtl();
                        obj1.Cart_ID = crt.Cart_ID;
                        obj1.Package_ID = pkgid;
                        obj1.Board_ID = data2.Board_ID;
                        obj1.Class_ID = data2.Class_ID;
                        obj1.Subject_ID = data2.Subject_ID;
                        obj1.Chapter_ID = data2.Chapter_ID;
                        obj1.Status = true;
                        obj1.Inserted_Date = DateTime.Now;
                        obj1.Is_Active = true;
                        obj1.Is_Deleted = false;
                        DbContext.tbl_DC_Cart_Dtl.Add(obj1);
                        DbContext.SaveChanges();
                    }
                }
            }
        }
        public JsonResult Getpackagesdtl(int id)
        {

            var pkg = DbContext.tbl_DC_Package.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Package_ID == id).FirstOrDefault();
            var pkg_dtl = DbContext.tbl_DC_Package_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Package_ID == id).ToList();
            int boardid = Convert.ToInt32(pkg_dtl.ToList()[0].Board_Id);
            int classid = Convert.ToInt32(pkg_dtl.ToList()[0].Class_Id);

            CreateCartdetails(id, pkg.Price.ToString());

            Package_Buy_class m = new Package_Buy_class();
            m.Package_ID = pkg.Package_ID;
            m.Package_Name = pkg.Package_Name;
            m.Package_Desc = pkg.Package_Desc;
            m.Board = DbContext.tbl_DC_Board.Where(a => a.Board_Id == boardid).FirstOrDefault().Board_Name;
            m.Classes = DbContext.tbl_DC_Class.Where(a => a.Class_Id == classid).FirstOrDefault().Class_Name;
            m.Is_Dictionary = pkg.Is_Dictionary;
            m.Is_Doubt = pkg.Is_Doubt;
            m.Is_Feed = pkg.Is_Feed;
            m.Is_Mentor = pkg.Is_Mentor;
            m.Is_School = pkg.Is_School;
            m.Is_Video = pkg.Is_Video;
            m.Price = pkg.Price;
            m.Subscripttion_Period = pkg.Subscripttion_Period;
            m.Total_Chapter = pkg.Total_Chapter;
            m.Type = pkg.Type;
            // m.subjects=

            var subject = DbContext.tbl_DC_Subject.ToList();
            var chapters = DbContext.tbl_DC_Chapter.ToList();
            var res = (from a in pkg_dtl
                       group a by a.Subject_Id into newGroup
                       select newGroup);
            List<Subject_Det_Class> j = new List<Subject_Det_Class>();

            foreach (var a in res)
            {
                Subject_Det_Class b = new Subject_Det_Class();
                b.Subject = subject.Where(f => f.Subject_Id == a.Key).FirstOrDefault().Subject;
                b.chapters = (from c in chapters join d in pkg_dtl on c.Chapter_Id equals d.Chapter_Id where c.Subject_Id == a.Key && d.Package_ID == id select new Chapter_Det_Class { Chapter = c.Chapter }).ToList();
                j.Add(b);
            }

            m.subjects = j.ToList();
            Session["prc"] = m.Price;
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getcustomersdetails()
        {
            string u_name = Session["USER_NAME"].ToString();
            var data = DbContext.tbl_DC_Registration.Where(x => x.Mobile == u_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult OrderConfirm(int pkgid, string paymentid)
        {
            string us_name = Session["USER_NAME"].ToString();
            var rdata = DbContext.tbl_DC_Registration.Where(x => x.Mobile == us_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
            var scart = DbContext.tbl_DC_Cart.Where(a => a.Regd_ID == rdata.Regd_ID && a.Package_ID == pkgid && a.Is_Active == true && a.Is_Deleted == false && a.Is_Paid == false).FirstOrDefault();
            int id = rdata.Regd_ID;int? eid = scart.Order_ID;
            string ClsId = paymentid;
            DateTime today = DateTime.Now;
            var register = DbContext.tbl_DC_Registration.
                  Where(x => x.Regd_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
            var cartt = DbContext.tbl_DC_Cart.Where(x => x.Order_ID == eid && x.Is_Active == true && x.Is_Deleted == false).
                FirstOrDefault();
            string disc_amt = string.Empty;
            decimal disc = 0;
            decimal discA = 0;
            string u_name = register.Mobile;
            int ordid = Convert.ToInt32(eid);
            string ordno = cartt.Order_No.ToString();

            var data2 = (from a in DbContext.tbl_DC_Registration.Where(x => x.Mobile == u_name && x.Is_Active == true
                && x.Is_Deleted == false)
                         join b in DbContext.tbl_DC_Cart.Where(x => x.Status == true && x.In_Cart == true && x.Is_Paid == false
                             && x.Is_Active == true && x.Is_Deleted == false)
                         on a.Regd_ID equals b.Regd_ID
                         select new DigiChampCartModel
                         {
                             firstname = a.Customer_Name,
                             Regd_ID = a.Regd_ID,
                             email = a.Email,
                             Regd_No = a.Regd_No,
                             Cart_ID = b.Cart_ID,
                             Order_ID = b.Order_ID,
                             Order_No = b.Order_No
                         }).ToList();

            int regid = Convert.ToInt32(id);

            var pre_b = DbContext.tbl_DC_Pre_book.Where(x => x.Regd_Id == regid && x.Ord_Status == false).FirstOrDefault();
            if (pre_b != null)
            {
                tbl_DC_Pre_book pre_book = DbContext.tbl_DC_Pre_book.Where(x => x.Regd_Id == regid).FirstOrDefault();
                pre_book.Ord_Status = true;
                pre_book.Order_Id = ordid;
                DbContext.Entry(pre_book).State = EntityState.Modified;
                DbContext.SaveChanges();
            }
            string regno = data2.ToList()[0].Regd_No;

            var data3 = (from e in DbContext.tbl_DC_Cart.Where(x => x.Order_ID == ordid && x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false)
                         join f in DbContext.tbl_DC_Cart_Dtl.Where(x => x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                         on e.Cart_ID equals f.Cart_ID
                         join a in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                         on f.Chapter_ID equals a.Chapter_Id
                         join b in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                         on a.Subject_Id equals b.Subject_Id
                         join c in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                         on b.Class_Id equals c.Class_Id
                         join d in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                         on c.Board_Id equals d.Board_Id
                         select new DigiChampCartModel
                         {
                             Regd_ID = e.Regd_ID,
                             Regd_No = e.Regd_No,
                             Cart_ID = e.Cart_ID,
                             Order_ID = e.Order_ID,
                             Order_No = e.Order_No,
                             Board_ID = f.Board_ID,
                             Board_Name = d.Board_Name,
                             Class_ID = f.Class_ID,
                             Class_Name = c.Class_Name,
                             Subject_ID = f.Subject_ID,
                             Subject = b.Subject,
                             Chapter_ID = f.Chapter_ID,
                             Chapter = a.Chapter
                         }).ToList();

            var data31 = DbContext.tbl_DC_Cart.Where(x => x.Order_ID == ordid && x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false).ToList();
            var data1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == u_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

            var data = (from a in DbContext.tbl_DC_Registration.Where(x => x.Mobile == u_name && x.Is_Active == true && x.Is_Deleted == false)
                        join b in DbContext.tbl_DC_Cart.Where(x => x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false)
                         on a.Regd_ID equals b.Regd_ID
                        join c in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                            on b.Package_ID equals c.Package_ID
                        select new DigiChamps.Controllers.CartController.CartModel1
                        {
                            Package_Name = c.Package_Name,
                            Subscripttion_Period = (int)c.Subscripttion_Period,
                            Price = c.Price,
                            Package_Id = c.Package_ID,
                            Is_Offline = (Boolean)c.Is_Offline
                        }).ToList();
            var data11 = (from x in data.ToList().Where(x => x.Is_Offline == true)
                          join y in DbContext.tbl_DC_Package_Period.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                          on x.Package_Id equals y.Package_ID
                          select new DigiChampCartModel
                          {
                              Is_Offline = x.Is_Offline,
                              Package_ID = x.Package_Id,
                              Price = x.Price,
                              Excluded_Price = y.Excluded_Price
                          }).ToList();

            if (data.Count == 0)
            {
                return Json("Your cart is empty", JsonRequestBehavior.AllowGet);
            }
            else
            {
                //ViewBag.cartitems = data;
                decimal tax = taxcalculate();
                int total_item = Convert.ToInt32(data.Count);
                decimal totalprice = data.ToList().Select(c => (decimal)c.Price).Sum();
                decimal totalpaybletax = (tax * totalprice) / 100;
                decimal totalpayblamt = Math.Round((totalprice + totalpaybletax), 2);
            }
            var price = DbContext.tbl_DC_CouponCode.Where(x => x.Is_Default == true && x.Valid_From <= today && x.Valid_To >= today && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
            decimal totalprice1 = 0;
            decimal totalpaybletax1 = 0;
            decimal totalpayblamt1 = 0;

            var coupon = DbContext.tbl_DC_Cart.Where(x => x.Order_ID == eid && x.Is_Active == true && x.Is_Deleted == false && x.Coupon_Code != null).FirstOrDefault();
            if (price == null && coupon == null)
            {
                decimal taxxx1 = taxcalculate();
                decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                totalprice1 = Math.Round(Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3);
                totalpaybletax1 = (taxxx1 * totalprice1) / 100;
                totalpayblamt1 = Math.Round((totalprice1 + totalpaybletax1), 2);
            }
            else
            {
                if (coupon != null)
                {
                    string co_code = coupon.Coupon_Code;
                    var price1 = DbContext.tbl_DC_CouponCode.Where(x => x.Coupon_Code == co_code && x.Valid_From <= today && x.Valid_To >= today && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
                    if (price1 != null)
                    {
                        if (price1.Discount_Price != null)
                        {
                            decimal taxxx1 = taxcalculate();
                            int ttl_item = Convert.ToInt32(data.Count);
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            decimal total_price1 = Math.Round((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3) - (Convert.ToDecimal(ttl_item * price.Discount_Price)));
                            discA = Convert.ToDecimal(ttl_item * price.Discount_Price);
                            decimal total_paybletax1 = (taxxx1 * total_price1) / 100;
                            decimal total_payblamt1 = Math.Round((total_price1 + total_paybletax1), 2);
                            totalprice1 = total_price1;
                            totalpaybletax1 = total_paybletax1;
                            totalpayblamt1 = total_payblamt1;
                        }
                        else if (price1.Discount_Percent != null)
                        {
                            decimal taxxx1 = taxcalculate();
                            int ttl_item = Convert.ToInt32(data.Count);
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            decimal disc_price1 = Math.Round(((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum())) * Convert.ToDecimal(price1.Discount_Percent)) / 100);
                            disc = disc_price1;

                            decimal total_price1 = Math.Round((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3) - (Convert.ToDecimal(disc_price1)));
                            decimal total_paybletax1 = (taxxx1 * total_price1) / 100;
                            decimal total_payblamt1 = Math.Round((total_price1 + total_paybletax1), 2);
                            totalprice1 = total_price1;
                            totalpaybletax1 = total_paybletax1;
                            totalpayblamt1 = total_payblamt1;
                        }
                        else
                        {
                            decimal taxxx1 = taxcalculate();
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            int ttl_item = Convert.ToInt32(data.Count);
                            totalprice1 = Math.Round(Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3);
                            totalpaybletax1 = (taxxx1 * totalprice1) / 100;
                            totalpayblamt1 = Math.Round((totalprice1 + totalpaybletax1), 2);
                        }
                    }
                }
                else
                {
                    if (price != null)
                    {
                        if (price.Discount_Price != null)
                        {
                            decimal taxxx1 = taxcalculate();
                            int ttl_item = Convert.ToInt32(data.Count);
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            decimal total_price1 = Math.Round((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3) - (Convert.ToDecimal(ttl_item * price.Discount_Price)));
                            discA = Convert.ToDecimal(ttl_item * price.Discount_Price);
                            decimal total_paybletax1 = (taxxx1 * total_price1) / 100;
                            decimal total_payblamt1 = Math.Round((total_price1 + total_paybletax1), 2);
                            totalprice1 = total_price1;
                            totalpaybletax1 = total_paybletax1;
                            totalpayblamt1 = total_payblamt1;
                        }
                        else if (price.Discount_Percent != null)
                        {
                            decimal taxxx1 = taxcalculate();
                            int ttl_item = Convert.ToInt32(data.Count);
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            decimal disc_price1 = Math.Round(((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum())) * Convert.ToDecimal(price.Discount_Percent)) / 100);
                            disc = disc_price1;
                            decimal total_price1 = Math.Round((Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3) - (Convert.ToDecimal(disc_price1)));
                            decimal total_paybletax1 = (taxxx1 * total_price1) / 100;
                            decimal total_payblamt1 = Math.Round((total_price1 + total_paybletax1), 2);
                            totalprice1 = total_price1;
                            totalpaybletax1 = total_paybletax1;
                            totalpayblamt1 = total_payblamt1;
                        }
                        else
                        {
                            decimal taxxx1 = taxcalculate();
                            decimal totalprice3 = data11.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                            int ttl_item = Convert.ToInt32(data.Count);
                            totalprice1 = Math.Round(Convert.ToDecimal(data.ToList().Select(c => (decimal)c.Price).Sum()) + totalprice3);
                            totalpaybletax1 = (taxxx1 * totalprice1) / 100;
                            totalpayblamt1 = Math.Round((totalprice1 + totalpaybletax1), 2);
                        }
                    }
                }
            }

            //totalprice1 = data.ToList().Select(c => (decimal)c.Price).Sum();
            //totalpaybletax1 = (taxxx * totalprice1) / 100;
            //totalpayblamt1 = Math.Round((totalprice1 + totalpaybletax1), 2);  
            decimal taxxx = taxcalculate();
            int total_item1 = Convert.ToInt32(data.Count);
            tbl_DC_Order ordobj = new tbl_DC_Order();
            ordobj.Cart_Order_No = ordno;
            ordobj.Trans_No = ClsId;
            ordobj.Regd_ID = regid;
            ordobj.Regd_No = regno;
            ordobj.Amt_In_Words = "0";
            ordobj.No_of_Package = Convert.ToInt32(total_item1);
            ordobj.Amount = Convert.ToDecimal(totalprice1);
            if (disc > 0)
            {
                ordobj.Disc_Amt = disc;
            }
            if (discA > 0)
            {
                ordobj.Disc_Perc = price.Discount_Percent;
            }

            ordobj.Total = Math.Round(Convert.ToDecimal(totalpayblamt1));
            ordobj.Amt_In_Words = null;
            ordobj.Payment_Mode = "Online";
            ordobj.Is_Paid = true;
            ordobj.Status = true;
            ordobj.Inserted_Date = DateTime.Now;
            string date = Convert.ToDateTime(ordobj.Inserted_Date).ToString("MM/dd/yyyy");
            string purchase_date = date;
            ordobj.Inserted_By = u_name;
            ordobj.Is_Active = true;
            ordobj.Is_Deleted = false;
            DbContext.tbl_DC_Order.Add(ordobj);
            DbContext.SaveChanges();

            var ord_id = ordobj.Order_ID;
            if (ord_id == null)
            {
                ordobj.Order_No = "DCORD" + "00000" + 1;
            }
            else
            {
                int ord_no = Convert.ToInt32(ordobj.Order_ID);
                if (ord_no > 0 && ord_no <= 9)
                {
                    ordobj.Order_No = "DCORD" + "00000" + Convert.ToString(ord_no);
                }
                if (ord_no > 9 && ord_no <= 99)
                {
                    ordobj.Order_No = "DCORD" + "0000" + Convert.ToString(ord_no);
                }
                if (ord_no > 99 && ord_no <= 999)
                {
                    ordobj.Order_No = "DCORD" + "000" + Convert.ToString(ord_no);
                }
                if (ord_no > 999 && ord_no <= 9999)
                {
                    ordobj.Order_No = "DCORD" + "00" + Convert.ToString(ord_no);
                }
                if (ord_no > 9999 && ord_no <= 99999)
                {
                    ordobj.Order_No = "DCORD" + "0" + Convert.ToString(ord_no);
                }
                string Order_Noo = ordobj.Order_No;
            }
            DbContext.SaveChanges();
            if (data2.ToList()[0].email != null && data2.ToList()[0].email != "")
            {
                var getall = DbContext.SP_DC_Get_maildetails("ORD_CONF").FirstOrDefault();

                string name = data2.ToList()[0].firstname;

                string ord_date = date;


                string odno = ordobj.Order_No;
                string total_amt = ordobj.Total.ToString();
                string tx_amt = taxxx.ToString();
                string pbl_amt = ordobj.Amount.ToString();

                var dat = DbContext.tbl_DC_Registration.Where(x => x.Mobile == u_name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                StringBuilder sb = new StringBuilder();
                if (data != null)
                {
                    int m = 1;

                    foreach (var it in data)
                    {

                        string prices = string.Format("{0:0.00}", it.Price);
                        sb.Append("<tr style='text-align:center;font-family: monospace; height:50px;'>");
                        sb.Append("<td style='border:1px solid #0f6fc6;'>" + m++ + "</td>");
                        sb.Append("<td style='border:1px solid #0f6fc6;'>" + it.Package_Name + "</td>");
                        sb.Append("<td style='border:1px solid #0f6fc6;'>" + 1 + "</td>");
                        sb.Append("<td style='border:1px solid #0f6fc6;'> " + prices + "</td>");
                        sb.Append("<td style='border:1px solid #0f6fc6;'>" + total_amt + "</td>");
                        sb.Append("</tr>");
                    }
                }
                string tbl_dtl = sb.ToString();
                if (disc == 0)
                {
                    disc_amt = discA.ToString();
                }
                else
                {
                    disc_amt = disc.ToString();
                }
                string msgbody = getall.EmailConf_Body.ToString().Replace("{{name}}", name).Replace("{{orderno}}", odno).
                    Replace("{{orderdate}}", date).Replace("{{packagedetails}}", tbl_dtl).Replace("{{totaltax}}", tx_amt).Replace("{{totalpbl}}", pbl_amt).Replace("{{address}}", dat.Address).Replace("{{pin}}", dat.Pincode).Replace("{{mobile}}", dat.Mobile).Replace("{{discount}}", disc_amt);
                sendMail1("ORD_CONF", register.Email, "Order Confirmation", name, msgbody);
            }

            var tax3 = (from tax1 in DbContext.tbl_DC_Tax_Type_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                        join tax2 in DbContext.tbl_DC_Tax_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                        on tax1.TaxType_ID equals tax2.TaxType_ID
                        select new DigiChampCartModel
                        {
                            Tax_ID = tax2.Tax_ID,
                            TaxType_ID = tax1.TaxType_ID,
                            Tax_Rate = tax2.Tax_Rate,
                            TAX_Efect_Date = tax2.TAX_Efect_Date
                        }).ToList();

            if (tax3.Count > 0)
            {
                foreach (var item3 in tax3)
                {
                    tbl_DC_Order_Tax ordtax = new tbl_DC_Order_Tax();
                    ordtax.Order_ID = ordobj.Order_ID;
                    ordtax.Order_No = ordobj.Order_No;
                    ordtax.Tax_ID = item3.Tax_ID;
                    ordtax.Tax_Type_ID = item3.TaxType_ID;
                    ordtax.Tax_Effect_Date = item3.TAX_Efect_Date;
                    ordtax.Tax_Amt = Convert.ToDecimal(((ordobj.Amount) * (item3.Tax_Rate)) / 100);
                    ordtax.Status = true;
                    ordtax.Inserted_Date = DateTime.Now;
                    ordtax.Inserted_By = u_name;
                    ordtax.Is_Active = true;
                    ordtax.Is_Deleted = false;
                    DbContext.tbl_DC_Order_Tax.Add(ordtax);
                    DbContext.SaveChanges();
                }
            }

            var order_pkg = (from ord in DbContext.tbl_DC_Order.Where(x => x.Regd_ID == regid && x.Cart_Order_No == ordno && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                             join cart in DbContext.tbl_DC_Cart.Where(x => x.Regd_ID == regid && x.Order_No == ordno && x.In_Cart == true && x.Is_Paid == false && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                             on ord.Cart_Order_No equals cart.Order_No
                             join pkg in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                             on cart.Package_ID equals pkg.Package_ID
                             select new DigiChampCartModel
                             {
                                 OrderID = ord.Order_ID,
                                 OrderNo = ord.Order_No,
                                 Package_ID = cart.Package_ID,
                                 Package_Name = pkg.Package_Name,
                                 Package_Desc = pkg.Package_Desc,
                                 Total_Chapter = pkg.Total_Chapter,
                                 Price = pkg.Price,
                                 Thumbnail = pkg.Thumbnail,
                                 Subscripttion_Period = pkg.Subscripttion_Period,
                                 Is_Offline = pkg.Is_Offline
                             }).ToList();

            foreach (var item in order_pkg)
            {
                tbl_DC_Order_Pkg ordpkg = new tbl_DC_Order_Pkg();
                ordpkg.Order_ID = item.OrderID;
                ordpkg.Order_No = item.OrderNo;
                ordpkg.Package_ID = item.Package_ID;
                ordpkg.Package_Name = item.Package_Name;
                ordpkg.Package_Desc = item.Package_Desc;
                ordpkg.Total_Chapter = item.Total_Chapter;
                ordpkg.Price = item.Price;
                ordpkg.Thumbnail = item.Thumbnail;
                ordpkg.Subscription_Period = item.Subscripttion_Period;
                int days = Convert.ToInt32(ordpkg.Subscription_Period);
                ordpkg.Status = true;
                ordpkg.Inserted_Date = DateTime.Now;
                ordpkg.Expiry_Date = Convert.ToDateTime(ordpkg.Inserted_Date).AddDays(days);
                ordpkg.Inserted_By = u_name;
                ordpkg.Is_Active = true;
                ordpkg.Is_Deleted = false;
                if (order_pkg.ToList()[0].Is_Offline == true)
                {
                    ordpkg.Is_Offline = true;
                }
                else
                {
                    ordpkg.Is_Offline = false;
                }
                DbContext.tbl_DC_Order_Pkg.Add(ordpkg);
                DbContext.SaveChanges();

                int pkg_id = Convert.ToInt32(ordpkg.Package_ID);
                int ord_pkg_id = Convert.ToInt32(ordpkg.OrderPkg_ID);
                foreach (var v in data31)
                {
                    int cart_id = Convert.ToInt32(v.Cart_ID);

                    var ordpkg_sub = (from ab in DbContext.tbl_DC_Order_Pkg.Where(x => x.OrderPkg_ID == ord_pkg_id && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                      join af in DbContext.tbl_DC_Cart_Dtl.Where(x => x.Cart_ID == cart_id && x.Is_Active == true && x.Is_Deleted == false)
                                      on ab.Package_ID equals af.Package_ID
                                      join ag in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                      on af.Chapter_ID equals ag.Chapter_Id
                                      join ah in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                      on ag.Subject_Id equals ah.Subject_Id
                                      join ai in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                      on ah.Class_Id equals ai.Class_Id
                                      join aj in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                      on ai.Board_Id equals aj.Board_Id
                                      select new DigiChampCartModel
                                      {
                                          OrderPkg_ID = ab.OrderPkg_ID,
                                          Package_ID = af.Package_ID,
                                          Board_ID = aj.Board_Id,
                                          Board_Name = aj.Board_Name,
                                          Class_ID = ai.Class_Id,
                                          Class_Name = ai.Class_Name,
                                          Subject_ID = ah.Subject_Id,
                                          Subject = ah.Subject,
                                          Chapter_ID = ag.Chapter_Id,
                                          Chapter = ag.Chapter
                                      }).ToList();

                    foreach (var item1 in ordpkg_sub)
                    {
                        tbl_DC_Order_Pkg_Sub ordpkgsub = new tbl_DC_Order_Pkg_Sub();
                        ordpkgsub.OrderPkg_ID = item1.OrderPkg_ID;
                        ordpkgsub.Package_ID = item1.Package_ID;
                        ordpkgsub.Board_ID = item1.Board_ID;
                        ordpkgsub.Board_Name = item1.Board_Name;
                        ordpkgsub.Class_ID = item1.Class_ID;
                        ordpkgsub.Class_Name = item1.Class_Name;
                        ordpkgsub.Subject_ID = item1.Subject_ID;
                        ordpkgsub.Subject_Name = item1.Subject;
                        ordpkgsub.Chapter_ID = item1.Chapter_ID;
                        ordpkgsub.Chapter_Name = item1.Chapter;
                        ordpkgsub.Status = true;
                        ordpkgsub.Inserted_Date = DateTime.Now;
                        ordpkgsub.Inserted_By = u_name;
                        ordpkgsub.Is_Active = true;
                        ordpkgsub.Is_Deleted = false;
                        DbContext.tbl_DC_Order_Pkg_Sub.Add(ordpkgsub);
                        DbContext.SaveChanges();

                        int ord_pkg_sub_id = Convert.ToInt32(ordpkgsub.OrderPkgSub_ID);

                        var ordpkg_sub_mod = (from ak in DbContext.tbl_DC_Order_Pkg_Sub.Where(x => x.OrderPkgSub_ID == ord_pkg_sub_id && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                              join al in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                              on ak.Chapter_ID equals al.Chapter_Id
                                              select new DigiChampCartModel
                                              {
                                                  OrderPkgSub_ID = ak.OrderPkgSub_ID,
                                                  Chapter_ID = ak.Chapter_ID,
                                                  Chapter = ak.Chapter_Name,
                                                  Module_ID = al.Module_ID,
                                                  Module_Name = al.Module_Name
                                              }).ToList();

                        if (ordpkg_sub_mod.Count > 0)
                        {
                            foreach (var item2 in ordpkg_sub_mod)
                            {
                                tbl_DC_Order_Pkg_Sub_Mod ordpkgsubmod = new tbl_DC_Order_Pkg_Sub_Mod();
                                ordpkgsubmod.OrderPkgSub_ID = item2.OrderPkgSub_ID;
                                ordpkgsubmod.Chapter_ID = item2.Chapter_ID;
                                ordpkgsubmod.Chapter_Name = item2.Chapter;
                                ordpkgsubmod.Module_ID = item2.Module_ID;
                                ordpkgsubmod.Module_Name = item2.Module_Name;
                                ordpkgsubmod.Status = true;
                                ordpkgsubmod.Inserted_Date = DateTime.Now;
                                ordpkgsubmod.Inserted_By = u_name;
                                ordpkgsubmod.Is_Active = true;
                                ordpkgsubmod.Is_Deleted = false;
                                DbContext.tbl_DC_Order_Pkg_Sub_Mod.Add(ordpkgsubmod);
                                DbContext.SaveChanges();
                            }
                        }
                    }
                }
            }


            var cartobj = (from ct in DbContext.tbl_DC_Cart.Where(x => x.Order_ID == ordid && x.Order_No == ordno
                && x.Is_Active == true && x.Is_Deleted == false)
                           select new DigiChampCartModel
                           {
                               Cart_ID = ct.Cart_ID,
                               In_Cart = ct.In_Cart,
                               Is_Paid = ct.Is_Paid,
                               Status = ct.Status,
                               Is_Active = ct.Is_Active,
                               Is_Deleted = ct.Is_Deleted
                           }).ToList();

            foreach (var item4 in cartobj)
            {
                int ctid = Convert.ToInt32(item4.Cart_ID);
                tbl_DC_Cart cartobj1 = DbContext.tbl_DC_Cart.Where(x => x.Cart_ID == ctid && x.Is_Active == true
                    && x.Is_Deleted == false && x.In_Cart == true && x.Is_Paid == false).FirstOrDefault();
                cartobj1.In_Cart = false;
                cartobj1.Is_Paid = true;
                cartobj1.Status = false;
                cartobj1.Is_Active = false;
                cartobj1.Is_Deleted = true;
                DbContext.Entry(cartobj1).State = EntityState.Modified;
                DbContext.SaveChanges();
            }
            DigiChamps.Controllers.CartController.order_conf_model ocm = new DigiChamps.Controllers.CartController.order_conf_model();
            ocm.cartdata = data;
            ocm.price = Math.Round(totalprice1, 2);
            ocm.payble_price = Math.Round(totalpayblamt1, 2);
            ocm.tax_percent = Math.Round(taxxx, 2);
            ocm.tax_amount = Math.Round(totalpaybletax1, 2);
            ocm.total_item = total_item1;
            ocm.purchase_date = purchase_date;
            ocm.Order_Id = ord_id;
            ocm.Order_No = ordobj.Order_No;

            if (ocm != null)
            {


                try
                {

                    string body = "ORID#{{orderid}}#Order Confirmed#Your Package is active now. Now explore more with the app";
                    string msg = body.ToString().
                        Replace("{{orderid}}", ord_id.ToString());


                    if (register != null)
                    {
                        if (register.Device_id != null)
                        {
                            var note = new PushNotiStatus(msg, register.Device_id);
                        }
                    }
                }
                catch (Exception)
                {


                }
                TempData["Package_Id"] = ocm.cartdata.FirstOrDefault().Package_Id;
                TempData["Order_Id"] = ocm.Order_Id;
                TempData["Order_No"] = ocm.Order_No;
                TempData["payble_price"] = ocm.payble_price;
                TempData["price"] = ocm.price;
                TempData["purchase_date"] = ocm.purchase_date;
                TempData["Package_Name"] = ocm.cartdata.FirstOrDefault().Package_Name;
                TempData["Subscripttion_Period"] = ocm.cartdata.FirstOrDefault().Subscripttion_Period;
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getpackagedetails()
        {
            int id = Convert.ToInt32(TempData["Package_Id"]);
            var pkg = DbContext.tbl_DC_Package.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Package_ID == id).FirstOrDefault();
            var pkg_dtl = DbContext.tbl_DC_Package_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Package_ID == id).ToList();
            int boardid = Convert.ToInt32(pkg_dtl.ToList()[0].Board_Id);
            int classid = Convert.ToInt32(pkg_dtl.ToList()[0].Class_Id);


            Package_Buy_class m = new Package_Buy_class();
            m.Package_ID = pkg.Package_ID;
            m.Package_Name = pkg.Package_Name;
            m.Package_Desc = pkg.Package_Desc;
            m.Board = DbContext.tbl_DC_Board.Where(a => a.Board_Id == boardid).FirstOrDefault().Board_Name;
            m.Classes = DbContext.tbl_DC_Class.Where(a => a.Class_Id == classid).FirstOrDefault().Class_Name;
            m.Is_Dictionary = pkg.Is_Dictionary;
            m.Is_Doubt = pkg.Is_Doubt;
            m.Is_Feed = pkg.Is_Feed;
            m.Is_Mentor = pkg.Is_Mentor;
            m.Is_School = pkg.Is_School;
            m.Is_Video = pkg.Is_Video;
            m.Price = pkg.Price;
            m.Subscripttion_Period = pkg.Subscripttion_Period;
            m.Total_Chapter = pkg.Total_Chapter;
            m.Type = pkg.Type;
            // m.subjects=

            var subject = DbContext.tbl_DC_Subject.ToList();
            var chapters = DbContext.tbl_DC_Chapter.ToList();
            var res = (from a in pkg_dtl
                       group a by a.Subject_Id into newGroup
                       select newGroup);
            List<Subject_Det_Class> j = new List<Subject_Det_Class>();

            foreach (var a in res)
            {
                Subject_Det_Class b = new Subject_Det_Class();
                b.Subject = subject.Where(f => f.Subject_Id == a.Key).FirstOrDefault().Subject;
                b.chapters = (from c in chapters join d in pkg_dtl on c.Chapter_Id equals d.Chapter_Id where c.Subject_Id == a.Key && d.Package_ID == id select new Chapter_Det_Class { Chapter = c.Chapter }).ToList();
                j.Add(b);
            }

            m.subjects = j.ToList();
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public decimal taxcalculate()
        {
            DateTime today = DateTime.Now;
            // Get the cart
            var taxcalculate = (from a in DbContext.tbl_DC_Tax_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.TAX_Efect_Date < today)
                                join b in DbContext.tbl_DC_Tax_Type_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                on a.TaxType_ID equals b.TaxType_ID
                                select new DigiChampsModel.TaxModel
                                {
                                    Tax_Rate = a.Tax_Rate
                                }).ToList();

            decimal taxamount = taxcalculate.ToList().Select(c => (decimal)c.Tax_Rate).Sum();
            //TempData["taxamount"] = Math.Round(taxamount, 2);
            return taxamount;
        }
        public ActionResult OrderConfirmation()
        {
         
            return View();
        }
        public ActionResult Champ_Camp()
        {
            return View();
        }
    }
}
