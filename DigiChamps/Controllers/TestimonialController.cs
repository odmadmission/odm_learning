﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class TestimonialController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<Testimonial> Testimonials { get; set; }
        }
        public class Testimonial
        {
            public int Testimonial_Id { get; set; }
            public string Testimonial_Name { get; set; }
            public string Testimonial_Title { get; set; }
            public string TotalMark { get; set; }
            public string Testimonial_Images { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public string Class_Name { get; set; }
            public string Description { get; set; }           
        }
        public class Testimonial_List
        {
            public List<Testimonial> list { get; set; }
        }
        public class success_Testimonial
        {
            public Testimonial_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetTestimonial()
        {
            try
            {
                var obj = new success_Testimonial
                {
                    Success = new Testimonial_List
                    {
                        list = (from a in db.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                join b in db.tbl_DC_Testimonial.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                on a.Class_Id equals b.Class_ID
                                select new Testimonial
                                {
                                    Testimonial_Id = b.Testimonial_Id,
                                    Testimonial_Name = b.Testimonial_Name,
                                    Testimonial_Title=b.Testimonial_Title,
                                    TotalMark = b.Testimonial_Title,
                                    Testimonial_Images = "https://learn.odmps.org/Images/Testimonial/" + b.Testimonial_Images + "",
                                    Description = b.Description,
                                    Class_ID= b.Class_ID,
                                    Class_Name =a.Class_Name
                                }).OrderByDescending(c => c.Testimonial_Id).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
