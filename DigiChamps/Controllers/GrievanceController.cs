﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace DigiChamps.Controllers
{
    public class GrievanceController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();

        // GET: /Grievance/

        public ActionResult GrivanceMaster()
        {
            return View();
        }

        public ActionResult CreateGrievance()
        {
            ViewBag.ModuleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true);
            return View();
        }

        [HttpPost]
        public ActionResult SaveGrievance(GrievanceModel gvModel)
        {
            string u_code = Session["USER_CODE"].ToString();

            Grievance grievanceModel = new Grievance();
            grievanceModel.Module = Convert.ToInt32(gvModel.ModuleId);
            grievanceModel.Title = gvModel.Title;
            grievanceModel.Description = gvModel.Description;
            grievanceModel.ResolvedDateTime = null;
            grievanceModel.InsertedDateTime = DateTime.Now;
            grievanceModel.Active = true;
            grievanceModel.InsertedId = u_code;
            grievanceModel.TeacherId = Convert.ToInt32(0);
            grievanceModel.StudentId = Convert.ToInt32(0);

            DbContext.Grievances.Add(grievanceModel);
            DbContext.SaveChanges();

            int id = grievanceModel.ID;

            if (gvModel.Griv_img[0] != null)
            {

                GrievanceAttachment attachment = new GrievanceAttachment();

                for (int i = 0; i < gvModel.Griv_img.Length; i++)
                {
                    if (gvModel.Griv_img[i] != null)
                    {
                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(gvModel.Griv_img[i].FileName.Replace(gvModel.Griv_img[i].FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Content/Grievance/"), fileName);
                        gvModel.Griv_img[i].SaveAs(path);
                        attachment.FileName = fileName.ToString();
                    }
                }
                attachment.InsertedId = u_code;
                attachment.GrivanceId = id;
                attachment.InsertedDateTime = DateTime.Now;
                attachment.Active = true;

                DbContext.GrievanceAttachments.Add(attachment);
                DbContext.SaveChanges();
            }

            return RedirectToAction("GrievanceList");

        }

        public ActionResult GrievanceList()
        {
            var grievanceList = DbContext.Grievances.Where(a => a.Active == true).ToList();
            var grievanceAttachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true).ToList();
            var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();

            var res = (from a in grievanceList
                       join c in moduleList on a.Module equals c.Module_ID
                       select new GrievanceModel
                       {
                           ID = a.ID,
                           ModuleId = a.Module,
                           ModuleName = c.Module_Name,
                           Title = a.Title,
                           Description = a.Description,
                           FileName = grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).Select(s => s.ID).FirstOrDefault() != 0 ?
                                         grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).FirstOrDefault().FileName : "NA",
                           StudentId = a.StudentId,
                           TeacherId = a.TeacherId,
                       }).ToList();

            return View(res);
        }

        public ActionResult GrievanceDetails(int id)
        {
            var grievanceList = DbContext.Grievances.Where(a => a.Active == true).ToList();
            ViewBag.attachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true && a.GrivanceId == id).ToList();
            var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();

            var res = (from a in grievanceList
                       join c in moduleList on a.Module equals c.Module_ID
                       select new GrievanceModel
                       {
                           ID = a.ID,
                           ModuleId = a.Module,
                           ModuleName = c.Module_Name,
                           Title = a.Title,
                           Description = a.Description, 
                           StudentId = a.StudentId,
                           TeacherId = a.TeacherId,
                       }).Where(p => p.ID == id).FirstOrDefault();

            return View(res);
        }

        public ActionResult GrievanceDelete(int id)
        {
            var grievanceList = DbContext.Grievances.Where(a => a.ID == id).FirstOrDefault();
            var grievanceAttachmentList = DbContext.GrievanceAttachments.Where(a => a.GrivanceId == grievanceList.ID).FirstOrDefault();

            if (grievanceList != null)
            {
                grievanceList.Active = false;
                DbContext.SaveChanges();
                if (grievanceAttachmentList != null)
                {
                    grievanceAttachmentList.Active = false;
                    DbContext.SaveChanges();
                }
            }

            return RedirectToAction("GrievanceList");
        }

        public ActionResult EditGrievance(int id)
        {            
            ViewBag.ModuleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true);
            ViewBag.status = new List<SelectListItem> {
                                new SelectListItem{ Text = "ACTIVE",Value = "true"},
                                new SelectListItem{ Text = "DELETED",Value = "false"}
                                };

            var grievanceList = DbContext.Grievances.Where(a => a.ID == id).FirstOrDefault();
            ViewBag.attachmentList = DbContext.GrievanceAttachments.Where(a => a.GrivanceId == grievanceList.ID && a.Active == true).ToList();           

            return View(grievanceList);
        }

        [HttpPost]
        public ActionResult UpdateGrievance(GrievanceModel gvModel)
        {
            string u_code = Session["USER_CODE"].ToString();

            Grievance grievance = new Grievance();
            grievance.Module = gvModel.ModuleId;
            grievance.Title = gvModel.Title;
            grievance.Description = gvModel.Description;
            grievance.ResolvedId = u_code;
            grievance.ResolvedDateTime = DateTime.Now;
            grievance.InsertedId = gvModel.InsertedId;
            grievance.InsertedDateTime = gvModel.InsertedDateTime;
            grievance.Active = gvModel.Active;

            var res = DbContext.UpdateGrievanceResolve(grievance.ID, grievance.Module, grievance.Title, grievance.Description, grievance.ResolvedId,
                grievance.ResolvedDateTime, grievance.InsertedId, grievance.InsertedDateTime, grievance.Active);
            DbContext.SaveChanges();

            return RedirectToAction("GetGrievanceByUserId");
        }

        public ActionResult GetGrievanceByUserId()
        {
            string u_code = Session["USER_CODE"].ToString();

            var grievanceList = DbContext.Grievances.Where(a => a.Active == true).ToList();
            var grievanceAttachmentList = DbContext.GrievanceAttachments.Where(a => a.Active == true).ToList();
            var moduleList = DbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();

            var res = (from a in grievanceList
                       join c in moduleList on a.Module equals c.Module_ID
                       select new GrievanceModel
                       {
                           ID = a.ID,
                           ModuleId = a.Module,
                           ModuleName = c.Module_Name,
                           Title = a.Title,
                           Description = a.Description,
                           FileName = grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).Select(s => s.ID).FirstOrDefault() != 0 ?
                                         grievanceAttachmentList.Where(s => s.GrivanceId == a.ID).FirstOrDefault().FileName : "NA",
                           StudentId = a.StudentId,
                           TeacherId = a.TeacherId,
                           InsertedId = a.InsertedId,
                       }).Where(a => a.InsertedId == u_code).ToList();

            return View(res);
        }

        public ActionResult DeleteAttachments(int id)
        {
            var res = DbContext.GrievanceAttachments.Where(a => a.ID == id).FirstOrDefault(); 

            if (res != null)
            {
                GrievanceAttachment att = new GrievanceAttachment();
                att.ID = res.ID;
                att.FileName = res.FileName;
                att.GrivanceId = res.GrivanceId;
                att.InsertedId = res.InsertedId;
                att.InsertedDateTime = res.InsertedDateTime;
                att.Active = false;

                var sp = DbContext.UpdateGrievanceAttachment(att.ID, att.GrivanceId, att.FileName, att.InsertedId, att.InsertedDateTime, att.Active);
               
            }

            return RedirectToAction("GetGrievanceByUserId");
        }

        [HttpPost]
        public ActionResult AddGrievanceAttachment(HttpPostedFileBase grivimg,int grivId)
        {
            string u_code = Session["USER_CODE"].ToString();
            GrievanceAttachment attachment = new GrievanceAttachment();
             
            string guid = Guid.NewGuid().ToString();
            var fileName = Path.GetFileName(grivimg.FileName.Replace(grivimg.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
            var path = Path.Combine(Server.MapPath("~/Content/Grievance/"), fileName);
            grivimg.SaveAs(path);
            attachment.FileName = fileName.ToString();
                
            attachment.InsertedId = u_code;
            attachment.GrivanceId = grivId;
            attachment.InsertedDateTime = DateTime.Now;
            attachment.Active = true;

            DbContext.GrievanceAttachments.Add(attachment);
            DbContext.SaveChanges();

            return RedirectToAction("GetGrievanceByUserId");
        }

        public ActionResult GetCommentByGrievanceId(int id)
        { 
            ViewBag.u_code = Session["USER_CODE"].ToString();
            var comment = DbContext.GrievanceComments.Where(a => a.GrievanceId == id).ToList();

            return View(comment);
        }

        [HttpPost]
        public JsonResult CreateComment(string desc,int grivId)
        {
            string u_code = Session["USER_CODE"].ToString();
            GrievanceComment comment = new GrievanceComment();
            comment.GrievanceId = grivId;
            comment.Description = desc;
            comment.InsertedId = u_code;
            comment.InsertedDateTime = DateTime.Now;
            comment.Active = true;

            DbContext.GrievanceComments.Add(comment);
            DbContext.SaveChanges();

            return Json(0);
        }
    }
}
