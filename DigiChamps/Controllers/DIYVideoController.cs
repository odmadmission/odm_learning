﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class DIYVideoController : ApiController
    {
        

        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<DIYVideo> DIYVideos { get; set; }
        }

        public class DIYVideo
        {
            public int? DIYVideo_ID { get; set; }
            public string DIYVideo_Name { get; set; }
            public string DIYVideo_Key { get; set; }
            public string DiyPosterImage_beta { get; set; }
            //public string DiyPosterImage_production { get; set; }
            public string DIYVideo_Description { get; set; }
        }

        public class DIYVideo_List
        {
            public List<DIYVideo> list { get; set; }
        }
        public class success_DIYVideo
        {
            public DIYVideo_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }

        [HttpGet]
        public HttpResponseMessage GetDIYVideo()
        {
            try
            {
                var obj = new success_DIYVideo
                {
                    Success = new DIYVideo_List
                    {
                        list = (from c in db.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                select new DIYVideo
                                {
                                    DIYVideo_ID = c.DIYVideo_ID,
                                    DIYVideo_Name = c.DIYVideo_Name,
                                    DIYVideo_Key = c.DIYVideo_Upload,
                                    DiyPosterImage_beta = c.DIYImages,
                                    //DiyPosterImage_production = "https://learn.odmps.org/Images/Dictionary/" + c.DIYImages + "",
                                    DIYVideo_Description = c.DIYVideo_Description
                                }).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
