﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class SectionController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<Section> Sections { get; set; }
        }
        public class Section
        {
            public Guid? SectionId { get; set; }
            public Guid? ClassId { get; set; }
            public string SectionName { get; set; }
            public Guid? School_Id { get; set; }

        }
        public class Section_List
        {
            public List<Section> list { get; set; }
        }
        public class success_Section
        {
            public Section_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetSectionList(Guid? schoolid, int classid)
        {
            try
            {
                var obj = new success_Section
                {
                    Success = new Section_List
                    {
                        list = (from c in db.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.School_Id == schoolid && x.Class_Id == classid)                        
                                select new Section
                                {
                                    SectionId = c.SectionId,
                                    SectionName = c.SectionName,
                                    School_Id = c.School_Id,
                                    ClassId = c.ClassId,    
                                }).ToList()
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}
