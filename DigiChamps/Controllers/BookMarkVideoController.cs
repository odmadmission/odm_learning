﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class BookMarkVideoController : ApiController
    {
        DateTime today = DigiChampsModel.datetoserver();
        DigiChampsEntities DbContext = new DigiChampsEntities();
        [HttpGet]
        public HttpResponseMessage VideoDetails(int id, int eid) //Student id and Chapter id
        {
            try
            {
                List<DigiChamps.Models.Digichamps_web_Api.Pdf_url> sample1 = new List<DigiChamps.Models.Digichamps_web_Api.Pdf_url>();
                var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == id && x.IS_ACCEPTED == true).FirstOrDefault();
                if (stuobj != null)
                {
                    DateTime dt = Convert.ToDateTime(stuobj.Inserted_Date);
                }
                if (stuobj != null)
                {
                    var Chapobj = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == eid && x.Is_Active == true).FirstOrDefault();
                    if (Chapobj != null)
                    {
                        var Chapters = (from d in DbContext.VW_DC_Package_Learn.Where(x => x.Regd_ID == id && x.Chapter_ID == eid).GroupBy(x => x.Chapter_ID)
                                        select new PackagePreviewModel
                                        {
                                            Chapter = d.FirstOrDefault().Chapter_Name,
                                            Is_Offline = d.FirstOrDefault().Is_Offline
                                        }).ToList();

                        var listmodule = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == eid && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).ToList();
                        var v = DbContext.SP_DC_GetModules_for_api(eid, id).ToList();
                        //var listdata = (from c in v.Where(x => x.Question_PDF != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.No_Of_Question, Question_Pdfs = c.Question_PDF, Modulename = c.Module_Name }).ToList();
                        if (Chapters.Count > 0)
                        {
                            var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                            {
                                success = new Digichamps_web_Api.chapterlist
                                {
                                    Chapter_Name = Chapobj.Chapter,
                                    Chapterid = eid,
                                    Today_date = today,
                                    Is_Offline = Chapters.FirstOrDefault().Is_Offline,
                                    ChapterModules = (from d in v
                                                      select new Digichamps_web_Api.ChapterModuleList
                                                      {
                                                          Module_Id = d.Module_ID,
                                                          Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                                                          Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                                                          Description = d.Module_Desc == null ? "" : d.Module_Desc,
                                                          //pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                                                          //pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                                                          Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                                                          Is_Free = d.Is_Free,
                                                         // Validity = d.Validity,
                                                          //Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                                                          //Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                                                          //No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                                                          //Is_Free_Test = d.Is_Free_Test == null ? false : d.Is_Free_Test,
                                                          Media_Id = d.Module_video == null ? "" : d.Module_video,
                                                          VideoKey = d.Module_video == null ? "" : d.Module_video,
                                                          Is_Expire = d.Module_video == null ? true : false,
                                                          Is_Avail = true,
                                                          //thumbnail_key = "-320",
                                                          thumbnail_key = "",
                                                          //template_id = "-ozl7iD1S",
                                                          template_id = "",
                                                      }).ToList(),                               
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                        }
                        else
                        {
                            var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                            {
                                success = new Digichamps_web_Api.chapterlist
                                {
                                    Chapter_Name = Chapobj.Chapter,
                                    Chapterid = eid,
                                    Today_date = today,
                                    ChapterModules = (from d in DbContext.SP_DC_GetModules_for_api(eid, id)
                                                      select new Digichamps_web_Api.ChapterModuleList
                                                      {

                                                          Module_Id = d.Module_ID,
                                                          Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                                                          Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                                                          Description = d.Module_Desc == null ? "" : d.Module_Desc,
                                                          //pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                                                          //pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                                                          Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                                                          Is_Free = d.Is_Free,
                                                        //  Validity = d.Validity,
                                                          //Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                                                          //Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                                                          //No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                                                          //Is_Free_Test = d.Is_Free_Test == null ? false : d.Is_Free_Test,
                                                        //  Media_Id = d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                                                          //template_id = "-ozl7iD1S",
                                                          template_id = "",
                                                        //  VideoKey = d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                                                        //  Is_Expire = d.Is_Free == true ? (d.Validity > today ? false : true) : true,
                                                          //thumbnail_key = "-320",
                                                          thumbnail_key = "",
                                                          Is_Avail = false,


                                                      }).ToList(),
                                }
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                        }
                    }
                    else
                    {
                        var errobj = new Digichamps_web_Api.Errorresult
                        {
                            Error = new Digichamps_web_Api.Errorresponse
                            {
                                Message = "Invalid Chapter details.",
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, errobj);
                    }
                }
                else
                {
                    var errobj = new Digichamps_web_Api.Errorresult
                    {
                        Error = new Digichamps_web_Api.Errorresponse
                        {
                            Message = "Invalid user details.",
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
