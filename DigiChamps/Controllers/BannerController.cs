﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class BannerController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<Banner> Banners { get; set; }
        }
        public class Banner
        {
            public int? banner_Id { get; set; }
            public string Image_URL { get; set; }
            public string Image_Title { get; set; }
            public string Banner_Description { get; set; }
        }
        public class Banner_List
        {
            public List<Banner> list { get; set; }
        }
        public class success_Banner
        {
            public Banner_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetBanner()
        {
            try
            {
                var obj = new success_Banner
                {
                    Success = new Banner_List
                    {
                        list = (from c in db.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                select new Banner
                                {

                                    banner_Id = c.banner_Id,
                                    Image_URL = c.Image_URL,
                                    Image_Title = c.Image_Title,
                                }).OrderByDescending(c => c.banner_Id).ToList(),
                    }
                };

                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }
    }
}