﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class WorksheetController : ApiController
    {
        DigiChampsEntities dbContext = new DigiChampsEntities();
        [HttpPost]
        public HttpResponseMessage UploadWorksheet()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int Subject_ID = Convert.ToInt32(httprequest.Form["Subject_ID"]);
                int Chapter_ID = Convert.ToInt32(httprequest.Form["Chapter_ID"]);
                int _board_id = Convert.ToInt32(httprequest.Form["Board_id"]);
                int _class_id = Convert.ToInt32(httprequest.Form["Class_id"]);
                int _student_id = Convert.ToInt32(httprequest.Form["Regd_ID"]);
                string _file_type = Convert.ToString(httprequest.Form["File_Type"]);
                int _module_id = Convert.ToInt32(httprequest.Form["Module_ID"]);
                Guid _section_id = Guid.Parse(httprequest.Form["Section_ID"]);
                string guid = Guid.NewGuid().ToString();
                int id = 0;
                tbl_DC_Worksheet ob = dbContext.tbl_DC_Worksheet.Where(a => a.Board_Id == _board_id && a.Class_Id == _class_id && a.Regd_ID == _student_id && a.Is_Active == true && a.Subject_Id == Subject_ID && a.Chapter_Id == Chapter_ID).FirstOrDefault();
                if (ob != null)
                {
                    var attach = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Worksheet_Id == ob.Worksheet_Id).ToList();
                    if (attach.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new { msg = "Already Exits" });
                    }
                    ob.Board_Id = _board_id;
                    ob.Chapter_Id = Chapter_ID;
                    ob.Class_Id = _class_id;
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = DateTime.Now;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.File_Type = _file_type;
                    ob.Subject_Id = Subject_ID;
                    ob.SectionId = _section_id;
                    ob.Verified = false;
                    ob.Regd_ID = _student_id;
                    ob.Verified_By = 0;
                    ob.Total_Mark = 0;
                    ob.is_Read = false;
                    dbContext.SaveChanges();
                    id = ob.Worksheet_Id;

                }
                else
                {
                    ob = new tbl_DC_Worksheet();
                    ob.Board_Id = _board_id;
                    ob.Chapter_Id = Chapter_ID;
                    ob.Class_Id = _class_id;
                    ob.Modified_By = _student_id;
                    ob.Modified_Date = DateTime.Now;
                    ob.Inserted_Date = DateTime.Now;
                    ob.Inserted_By = _student_id;
                    ob.Is_Active = true;
                    ob.Is_Deleted = false;
                    ob.File_Type = _file_type;
                    ob.Subject_Id = Subject_ID;
                    ob.SectionId = _section_id;
                    ob.Verified = false;
                    ob.Regd_ID = _student_id;
                    ob.Verified_By = 0;
                    ob.Total_Mark = 0;
                    ob.is_Read = false;
                    dbContext.tbl_DC_Worksheet.Add(ob);
                    dbContext.SaveChanges();
                    id = ob.Worksheet_Id;
                }

                if (httprequest.Files.Count > 0)
                {
                    foreach (string files in httprequest.Files)
                    {
                        var postedfile = httprequest.Files[files];
                        tbl_DC_Worksheet_Attachment att = new tbl_DC_Worksheet_Attachment();
                        att.File_Name = _file_type == "pdf" ? id + ".pdf" : id + ".pdf";
                        att.Worksheet_Id = id;
                        att.Modified_By = _student_id;
                        att.Modified_Date = DateTime.Now;
                        att.Inserted_Date = DateTime.Now;
                        att.Inserted_By = _student_id;
                        att.Is_Active = true;
                        att.Is_Deleted = false;

                        // Bitmap bitmapImage = ResizeImage(postedfile.InputStream, 300, 300);
                        // System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/Worksheet/"), (guid + att.File_Name));
                        postedfile.SaveAs(path);
                        //docfile.Add(path);
                        att.Path = guid + att.File_Name;
                        dbContext.tbl_DC_Worksheet_Attachment.Add(att);
                        dbContext.SaveChanges();

                        try
                        {

                            var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == Chapter_ID).FirstOrDefault();
                            var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == Subject_ID).FirstOrDefault();
                            var reg = dbContext.tbl_DC_Registration.Where(a => a.Regd_ID == _student_id).FirstOrDefault();

                            var tea = dbContext.tbl_DC_AssignSubjectToTeacher.Where(A => A.Subject_Id == Subject_ID && A.SectionId == _section_id && A.Is_Active == true).FirstOrDefault();
                            var pushnot = (from c in dbContext.Teachers.Where(x => x.TeacherId == tea.Teacher_ID)

                                           select new { c.TeacherId, c.fcmId }).FirstOrDefault();
                            string body = "WSHEET#{{ID}}#{{marks}}#{{pdf}}#{{name}} has been uploaded worksheet for {{chapter}} worksheet of {{subject}}";
                            string msg = body.ToString().Replace("{{chapter}}", chapter.ToString())
                                .Replace("{{subject}}", subject + "").Replace("{{ID}}", ob.Worksheet_Id + "").Replace("{{marks}}", ob.Total_Mark + "").Replace("{{pdf}}",
                                "http://learn.odmps.org/Images/Worksheet/" + att.File_Name + "").Replace("{{name}}", reg.Customer_Name + "");

                            if (pushnot != null)
                            {
                                if (pushnot.fcmId != null)
                                {
                                    var note = new PushNotiStatus("Worksheet New",msg, pushnot.fcmId);
                                }
                            }


                        }
                        catch (Exception e)
                        {

                        }



                    }
                }


              
              

               

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteWorksheet(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Worksheet_Id == id).ToList();
                foreach (tbl_DC_Worksheet_Attachment a in wrk)
                {
                    a.Is_Active = false;
                    a.Is_Deleted = true;
                    a.Modified_Date = DateTime.Now;
                    dbContext.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage IsReadWorksheet(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Worksheet_Id == id).FirstOrDefault();

                wrk.is_Read = true;
                wrk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class worksheetcls
        {
            public int Worksheet_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int Chapter_Id { get; set; }
            public string Chapter_Name { get; set; }
            public bool Verified { get; set; }
            public int Verified_By { get; set; }
            public string Verified_By_Name { get; set; }
            public int Total_Mark { get; set; }
            public string status { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public int Module_Id { get; set; }
            public string Module_Name { get; set; }
            public string File_Type { get; set; }
            public List<tbl_DC_Worksheet_Attachment> attachments { get; set; }
        }

        public class StdName
        {
            public string Name { get; set; }
            public string Mobile { get; set; }
           
        }
        public class WorksheetHolder
        {
            public List<worksheetcls> data { get; set; }

            public List<StdName> students { get; set; }


            public int total { get; set; }

            public int uploaded { get; set; }

            public int NotUploaded { get; set; }

        }
        [HttpGet]
        public HttpResponseMessage GetallWorksheetWithStatus(int classid, Guid sec, int subjectid, int chapterid,string status)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Class_Id == classid 
                    && a.SectionId == sec && a.Subject_Id == a.Subject_Id && a.Chapter_Id == chapterid).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                //   var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();

                

                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //   join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               status=(a.is_Read==false)?"NEW":(((a.is_Read==true || a.is_Read==null) && a.Verified==true)?"SCORED":"PENDING"),
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id 
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();

                List<worksheetcls> list = new List<worksheetcls>();
                if(status!=null && status!=""&&status.ToUpper()!="ALL")
                {
                    list.AddRange ( res.Where(a => a.status == status).ToList());
                }
                else
                {
                    list.AddRange(res);
                }




                WorksheetHolder holder = new WorksheetHolder();
                holder.data = list;
                holder.students = (from a in students.Where(a => a.SectionId == sec && !res.Select(b => b.Regd_ID).ToList().Contains(a.Regd_ID))

                                   select new StdName
                                   {
                                       Name=a.Customer_Name,
                                       Mobile=a.Mobile
                                   }
                                     ).ToList();
                holder.uploaded = res.Select(a => a.Regd_ID).Distinct().Count();
                holder.NotUploaded=students.Where(a=>a.SectionId==sec).Count()-holder.uploaded;

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = holder });
            }
  
            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        public HttpResponseMessage GetallWorksheet(int classid, Guid sec, int subjectid, int chapterid)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Class_Id == classid
                    && a.SectionId == sec && a.Subject_Id == a.Subject_Id && a.Chapter_Id == chapterid).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                //   var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //   join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               status = (a.is_Read == false) ? "NEW" : (((a.is_Read == true || a.is_Read == null) && a.Verified == true) ? "SCORED" : "PENDING"),
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();

                

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetallWorksheetByRegdId(int classid, Guid sec, int subjectid, int chapterid, int regdId)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Class_Id == classid && a.SectionId == sec && a.Subject_Id == subjectid && a.Chapter_Id == chapterid && a.Regd_ID == regdId).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           // join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               status = (a.is_Read == false) ? "NEW" : (((a.is_Read == true || a.is_Read == null) && a.Verified == true) ? "SCORED" : "PENDING"),
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).FirstOrDefault();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheetlist = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpGet]
        public HttpResponseMessage GetWorksheetById(int id)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Worksheet_Id == id).ToList();
                var board = dbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = dbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = dbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = dbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = dbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetcls
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               status = (a.is_Read == false) ? "NEW" : (((a.is_Read == true || a.is_Read == null) && a.Verified == true) ? "SCORED" : "PENDING"),
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).FirstOrDefault();



                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "S", worksheet = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "E" });
            }
        }
        [HttpPost]
        public HttpResponseMessage ScoreToWorksheet(int worksheetId, int teacherId, int score)
        {
            try
            {

                if (score > 20)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
                }

                tbl_DC_Worksheet wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Worksheet_Id == worksheetId).FirstOrDefault();
                wrk.Total_Mark = score;
                wrk.Verified_By = teacherId;
                wrk.Verified = true;
                wrk.Modified_Date = DateTime.Now;
                dbContext.SaveChanges();
                //string subject=dbContext.tbl_DC_Subject.Where(a=>a.Subject_Id==wrk.Subject_Id)
                //var note = new PushNotiStatusMaster(title, body, obj1.Feed_Images, ids[i]);
                try
                {
                    var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == wrk.Chapter_Id).FirstOrDefault();
                    var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == wrk.Subject_Id).FirstOrDefault();
                    var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID)

                                   select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                    string body = "Worksheet score#{{ID}}#Your teacher has been updated score for {{chapter}} worksheet of {{subject}}";
                    string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                        .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}",worksheetId+ "");

                    if (pushnot != null)
                    {
                        if (pushnot.Device_id != null)
                        {
                            var note = new PushNotiStatus(msg, pushnot.Device_id);
                        }
                    }
                }
                catch (Exception)
                {


                }
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", worksheetid = worksheetId });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }

        public class worksheetcount
        {
            public int totalsubmitted { get; set; }
            public int totalscored { get; set; }
            public int totalpending { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetWorksheetDashboard(int classid, Guid sec, int subjectid)
        {
            try
            {
                var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Class_Id == classid && a.SectionId == sec && a.Subject_Id == a.Subject_Id).ToList();
                var wrksheet = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var res = (from a in wrk
                           join b in wrksheet on a.Worksheet_Id equals b.Worksheet_Id
                           select a).Distinct().ToList();
                worksheetcount ob = new worksheetcount();
                ob.totalsubmitted = res.Count();
                ob.totalscored = res.Where(a => a.Verified == true).ToList().Count();
                ob.totalpending = res.Where(a => a.Verified == false).ToList().Count();
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", worksheet = ob });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class WorksheetReviewClass
        {
            public string Attachment { get; set; }
            public DateTime Inserted_On { get; set; }
            public bool Is_Student { get; set; }
            public string Message { get; set; }
            public DateTime Modified_On { get; set; }
            public int Posted_By_ID { get; set; }
            public string Posted_By { get; set; }
            public int WorksheetReview_ID { get; set; }
            public int Worksheet_ID { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetWorksheetReview(int worksheetid)
        {
            try
            {
                var review = dbContext.Tbl_Worksheet_Review.Where(a => a.Is_Active == true && a.Worksheet_ID == worksheetid).ToList();
                var students = dbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var teachers = dbContext.Teachers.Where(a => a.Active == 1).ToList();

                var res = (from a in review
                           select new WorksheetReviewClass
                           {
                               Attachment = a.Attachment == null ? null : "/Images/WorksheetReview/" + a.Attachment,
                               Inserted_On = a.Inserted_On.Value,
                               Is_Student = a.Is_Student.Value,
                               Message = a.Message,
                               Modified_On = a.Modified_On.Value,
                               Posted_By_ID = a.Posted_By_ID.Value,
                               WorksheetReview_ID = a.WorksheetReview_ID,
                               Worksheet_ID = a.Worksheet_ID.Value,
                               Posted_By = a.Is_Student == true ? (students.Where(m => m.Regd_ID == a.Posted_By_ID).FirstOrDefault().Customer_Name) : (teachers.Where(m => m.TeacherId == a.Posted_By_ID).FirstOrDefault().Name)
                           }).ToList();

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success", review = res });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpGet]
        public HttpResponseMessage DeleteWorksheetReview(int id)
        {
            try
            {
                Tbl_Worksheet_Review review = dbContext.Tbl_Worksheet_Review.Where(a => a.Is_Active == true && a.WorksheetReview_ID == id).FirstOrDefault();
                review.Is_Active = false;
                review.Modified_On = DateTime.Now;
                dbContext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        [HttpPost]
        public HttpResponseMessage UploadWorksheetReview()
        {
            try
            {
                var httprequest = HttpContext.Current.Request;
                int Worksheet_ID = Convert.ToInt32(httprequest.Form["Worksheet_ID"]);
                int Posted_By_ID = Convert.ToInt32(httprequest.Form["Posted_By_ID"]);
                string Message = Convert.ToString(httprequest.Form["Message"]);
                bool Is_student = Convert.ToBoolean(httprequest.Form["Is_Student"]);
                string guid = Guid.NewGuid().ToString();
                int id = 0;
                Tbl_Worksheet_Review att = new Tbl_Worksheet_Review();
                att.Is_Student = Is_student;
                att.Worksheet_ID = Worksheet_ID;
                att.Modified_On = DateTime.Now;
                att.Inserted_On = DateTime.Now;
                att.Is_Active = true;
                att.Message = Message;
                att.Posted_By_ID = Posted_By_ID;
                att.Inserted_By = "1";
                att.Modified_By = "1";
                // Bitmap bitmapImage = ResizeImage(postedfile.InputStream, 300, 300);
                // System.IO.MemoryStream stream = new System.IO.MemoryStream();
                if (httprequest.Files != null && httprequest.Files.Count > 0)
                {
                    foreach (string files in httprequest.Files)
                    {
                        var postedfile = httprequest.Files[files];
                        var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Images/WorksheetReview/"), (guid + postedfile.FileName));
                        postedfile.SaveAs(path);
                        //docfile.Add(path);
                        att.Attachment = guid + postedfile.FileName;
                    }

                }
                dbContext.Tbl_Worksheet_Review.Add(att);
                dbContext.SaveChanges();


                try
                {
                    var wrk = dbContext.tbl_DC_Worksheet.Where(a => a.Worksheet_Id == Worksheet_ID).FirstOrDefault();
                     var wrkattch = dbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Worksheet_Id == Worksheet_ID&&a.Is_Active==true&&a.Is_Deleted==false).FirstOrDefault();
                    var chapter = dbContext.tbl_DC_Chapter.Where(a => a.Chapter_Id == wrk.Chapter_Id).FirstOrDefault();
                    var subject = dbContext.tbl_DC_Subject.Where(a => a.Subject_Id == wrk.Subject_Id).FirstOrDefault();


                    if (Is_student)
                    {
                          var reg=dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == Posted_By_ID).FirstOrDefault();

                        var tea=dbContext.tbl_DC_AssignSubjectToTeacher.Where(A=>A.Subject_Id==wrk.Subject_Id&&A.SectionId==reg.SectionId&&A.Is_Active==true).FirstOrDefault();
                        var pushnot = (from c in dbContext.Teachers.Where(x => x.TeacherId == tea.Teacher_ID)

                                       select new { c.TeacherId, c.fcmId }).FirstOrDefault();
                        string body = "WREVIEW#{{ID}}#{{marks}}#{{pdf}}#{{name}} has been replied for {{chapter}} worksheet of {{subject}}";
                        string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                            .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", Worksheet_ID + "").Replace("{{marks}}", wrk.Total_Mark + "").Replace("{{pdf}}",
                            "http://learn.odmps.org/Images/Worksheet/" + wrkattch.File_Name + "").Replace("{{name}}", reg.Customer_Name + "");

                        if (pushnot != null)
                        {
                            if (pushnot.fcmId != null)
                            {
                                var note = new PushNotiStatus("Worksheet Review",msg, pushnot.fcmId);
                            }
                        }
                    }
                    else
                    {
                        //var pushnot = (from c in dbContext.tbl_DC_Registration.Where(x => x.Regd_ID == wrk.Regd_ID)

                        //               select new { c.Regd_ID, c.Device_id }).FirstOrDefault();
                        //string body = "Worksheet score#{{ID}}#Your student has been replied for {{chapter}} worksheet of {{subject}}";
                        //string msg = body.ToString().Replace("{{chapter}}", chapter.Chapter.ToString())
                        //    .Replace("{{subject}}", subject.Subject + "").Replace("{{ID}}", Worksheet_ID + "");

                        //if (pushnot != null)
                        //{
                        //    if (pushnot.Device_id != null)
                        //    {
                        //        var note = new PushNotiStatus(msg, pushnot.Device_id);
                        //    }
                        //}
                    }
                }
                catch (Exception)
                {


                }

                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "success" });
            }

            catch (Exception e1)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }


       

    }
}
