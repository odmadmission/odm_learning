﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DigiChamps.Controllers
{
    public class TestStatisticsController : ApiController
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public class ErrorResult
        {
            public ErrorResponse Error { get; set; }
        }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }

        public class SuccessResponse
        {
            public string Message { get; set; }
        }
        public class SuccessResult
        {
            public List<TestStatistics> TestStatisticss { get; set; }
        }
        public class Easy
        {
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public int? Total_Incorrec_Ans { get; set; }
            public int? Question_Attempted { get; set; }

        }
        public class Medium
        {
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public int? Total_Incorrec_Ans { get; set; }
            public int? Question_Attempted { get; set; }

        }
        public class Difficult
        {
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public int? Total_Incorrec_Ans { get; set; }
            public int? Question_Attempted { get; set; }

        }
        public class TestStatistics
        {
            public Nullable<int> Question_Nos { get; set; }
            public Nullable<int> Total_Correct_Ans { get; set; }
            public Nullable<double> Totaltime { get; set; }
            public Nullable<int> wrong { get; set; }
            public int Accuracy { get; set; }
            public int Rank { get; set; }
            public int Performance { get; set; }
            public List<Difficulty> Difficultys { get; set; }
            public List<Easy> Easy { get; set; }
            public List<Medium> Medium { get; set; }
            public List<Difficult> Difficult { get; set; }
            public List<Startegic_Report_Result> Topic_details { get; set; }

            public int? Exam_ID { get; set; }

            public int? Total_Incorrec_Ans { get; set; }

            public int? Question_Attempted { get; set; }

            public int? Unanswered { get; set; }
        }
        public class TestStatistics_List
        {
            public List<TestStatistics> list { get; set; }
        }
        public class success_TestStatistics
        {
            public TestStatistics_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }
        public class Result_Succes
        {
            public TestStatistics success { get; set; }
        }
        public class Startegic_Report_Result
        {
            public Nullable<int> Topic_ID { get; set; }
            public string Topic_Name { get; set; }
            public Nullable<int> Total_question { get; set; }
            public int Correct_answer { get; set; }
            public int Incorrect_answer { get; set; }
            public Nullable<int> Percentage { get; set; }
            public string Remark { get; set; }
        }
        public class Difficulty
        {
            public int? No_of_Question { get; set; }
            public int? Power_ID { get; set; }
            public string Power_Type { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage TestStatistics_Details(int? id, int? eid)//registration Id/ Result Id
        {
            try
            {

                var startegic = DbContext.SP_DC_Startegic_Report(eid).ToList();

                var examresult_cal = DbContext.SP_DC_Examresultcalulation(id, eid).ToList();

                var question = DbContext.SP_DC_Getallquestion_Appeard(eid);

                var get_Level = DbContext.SP_DC_Get_Power_Result(eid).ToList();

                var get_result_data = DbContext.tbl_DC_Exam_Result.Where(x => x.Result_ID == eid).FirstOrDefault();

                DateTime sdt = Convert.ToDateTime(get_result_data.StartTime);

                DateTime edt = Convert.ToDateTime(get_result_data.EndTime);

                var hours = (edt - sdt).TotalMilliseconds;
                var Leader = DbContext.SP_DC_lead(eid).ToList();
                int count = Leader.FindIndex(X => X.Regd_ID == id);
                int myrank = count + 1;
                int accuracy = Convert.ToInt32(Convert.ToDecimal(examresult_cal.FirstOrDefault().Total_Correct_Ans) / Convert.ToDecimal(examresult_cal.FirstOrDefault().Question_Nos) * 100);

                var obj = new Result_Succes
                {
                    success = new TestStatistics
                    {
                        Question_Nos = examresult_cal.FirstOrDefault().Question_Nos,
                        Total_Correct_Ans = examresult_cal.FirstOrDefault().Total_Correct_Ans,
                        Total_Incorrec_Ans = examresult_cal.FirstOrDefault().Total_InCorrect_Ans,
                        Question_Attempted = examresult_cal.FirstOrDefault().Question_Attempted,
                        Unanswered = examresult_cal.FirstOrDefault().Question_Nos - examresult_cal.FirstOrDefault().Question_Attempted,
                        Totaltime = hours,
                        Exam_ID = get_result_data.Exam_ID,
                        wrong = Convert.ToInt32(examresult_cal.FirstOrDefault().wrong),
                        Accuracy = accuracy,
                        Performance = myrank,
                        Rank = DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == id && x.Result_ID == eid).Count(),
                        Difficultys = (from c in get_Level
                                       select new Difficulty
                                       {
                                           No_of_Question = c.No_of_Question,
                                           Power_ID = c.Power_ID,
                                           Power_Type = c.Power_Type
                                       }).ToList(),

                        Easy = (from a in DbContext.tbl_DC_Question
                                join b in DbContext.tbl_DC_Power_Question on a.Power_ID equals b.Power_Id
                                join c in DbContext.tbl_DC_Exam_Result_Dtl on b.Power_Id equals c.Power_ID
                                join d in DbContext.tbl_DC_Exam_Result on c.Result_ID equals d.Result_ID
                                where b.Power_Type == "Easy"
                                select new Easy 
                                {
                                    Question_Nos = d.Question_Nos,
                                    Total_Correct_Ans = d.Total_Correct_Ans,
                                    Question_Attempted = d.Question_Attempted
                                }).ToList(),
                                   
                        Medium = (from a in DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == id && x.Result_ID == eid)
                                  join b in DbContext.tbl_DC_Exam_Result_Dtl on a.Result_ID equals b.Result_ID
                                  join c in DbContext.tbl_DC_Power_Question on b.Power_ID equals c.Power_Id
                                  where c.Power_Type == "Medium"
                                  select new Medium
                                {
                                    Question_Nos = a.Question_Nos,
                                    Total_Correct_Ans = a.Total_Correct_Ans,
                                    Question_Attempted = a.Question_Attempted
                                }).ToList(),
                        Difficult = (from a in DbContext.tbl_DC_Exam_Result.Where(x => x.Regd_ID == id && x.Result_ID == eid)
                                     join b in DbContext.tbl_DC_Exam_Result_Dtl on a.Result_ID equals b.Result_ID
                                     join c in DbContext.tbl_DC_Power_Question on b.Power_ID equals c.Power_Id
                                     where c.Power_Type == "Difficult"
                                     select new Difficult
                                {
                                    Question_Nos = a.Question_Nos,
                                    Total_Correct_Ans = a.Total_Correct_Ans,
                                    Question_Attempted = a.Question_Attempted
                                }).ToList(),
                        Topic_details = (from d in startegic
                                         select new Startegic_Report_Result
                                         {
                                             Topic_ID = d.Topic_ID,
                                             Topic_Name = d.Topic_Name,
                                             Total_question = d.Total_question,
                                             Correct_answer = d.Correct_answer,
                                             Incorrect_answer = d.Incorrect_answer,
                                             Percentage = d.Percentage,
                                             Remark = d.Remark
                                         }).ToList()
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch
            {
                var obj = new DigiChamps.Models.Digichamps.ErrorResult_Exam
                {
                    error = new Digichamps.ErrorResponse_Exam
                    {
                        Message = "No data Found."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }
    }
}
