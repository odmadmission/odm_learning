﻿using ClosedXML.Excel;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Mvc;
using System.Web;
using System.Data.OleDb;

namespace DigiChamps.Controllers
{
    public class SchoolReportController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string User_name, string password)
        {
            try
            {
                if (User_name == "Admin")
                {

                    if (password == "admin@1234")
                    {
                        Session["id"] = 123;
                        return RedirectToAction("Dashboard", "SchoolReport");
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid credential for admin login.";
                        return View();
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid username for admin login.";
                    return View();
                }

            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View();
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "SchoolReport");
        }
        public JsonResult GetSchool()
        {
            var res = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSection(Guid schoolid, int id)
        {
            var board = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == schoolid).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        #region Get_Doubt_Tracker
        public ActionResult Doubt(Guid? school, int? board, int? cls, Guid? sec, int? subject, int? chapter, int? student)
        {
            if (Session["id"] == null)
            {
                return RedirectToAction("Index");
            }
            var res = DbContext.SP_GET_DOUBT_BY_TEACHER_ID(1).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.chapter = null;
            ViewBag.school = null;
            ViewBag.student = null;
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.studentlist = students;
            int i = 0;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.board_id == board.Value).ToList();
                ViewBag.board = board.Value;
                i = 1;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.class_id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
                i = 1;
            }
            if (school != null)
            {
                res = res.Where(a => a.SchoolId == school.Value).ToList();
                ViewBag.school = school.Value;
                i = 1;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
                i = 1;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_ID == subject.Value).ToList();
                ViewBag.subject = subject.Value;
                i = 1;
            }
            if (chapter != null && chapter != 0)
            {
                res = res.Where(a => a.Chapter_ID == chapter.Value).ToList();
                ViewBag.chapter = chapter.Value;
                i = 1;
            }
            if (student != null && student != 0)
            {
                res = res.Where(a => a.Student_ID == student.Value).ToList();
                ViewBag.student = student.Value;
                i = 1;
            }
            Session["doubtlist"] = res;
            if (i == 0)
            {
                res = res.Take(20).ToList();
            }
            return View(res);
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public class Class_DOUBT_BY_TEACHER_ID_Result
        {
            public string Customer_Name { get; set; }
            public string Board_Name { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Subject { get; set; }
            public string Chapter { get; set; }
            public Nullable<int> Total { get; set; }
            public Nullable<int> Pendingticket { get; set; }
            public Nullable<int> Answerticket { get; set; }
            public Nullable<int> Reject { get; set; }
            public Nullable<int> Closeticket { get; set; }
        }
        public ActionResult DownloadDoubtList()
        {

            XLWorkbook oWB = new XLWorkbook();
            var doubtlist = Session["doubtlist"] as List<SP_GET_DOUBT_BY_TEACHER_ID_Result>;
            var doubt = doubtlist.Select(a => new Class_DOUBT_BY_TEACHER_ID_Result
            {
                Answerticket = a.Answerticket,
                Board_Name = a.Board_Name,
                Chapter = a.Chapter,
                Class_Name = a.Class_Name,
                Closeticket = a.Closeticket,
                Customer_Name = a.Customer_Name,
                Pendingticket = a.Pendingticket,
                Reject = a.Reject,
                SectionName = a.SectionName,
                Subject = a.Subject,
                Total = a.Total
            }).ToList();
            // COUNT TABLE DATA
            DataTable gdt = ToDataTable<Class_DOUBT_BY_TEACHER_ID_Result>(doubt);
            int lastCellNo1 = gdt.Rows.Count + 1;
            gdt.TableName = "Doubt_Details";

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet = oWB.Worksheet(1);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Doubt Report.xlsx");
        }
        #endregion
        #region Get_Worksheet_Tracker
        public ActionResult Worksheet(Guid? school, int? board, int? cls, Guid? sec, int? subject, int? chapter, int? studentid)
        {

            if (Session["id"] == null)
            {
                return RedirectToAction("Index");
            }
            var res = DbContext.SP_GET_WORKSHEET_REPORT().ToList();
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            ViewBag.studentlist = students;
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            ViewBag.chapter = null;
            ViewBag.school = null;
            ViewBag.student = null;
            int i = 0;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.board_id == board.Value).ToList();
                ViewBag.board = board.Value;
                i = 1;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.class_id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
                i = 1;
            }
            if (school != null)
            {
                res = res.Where(a => a.SchoolId == school.Value).ToList();
                ViewBag.school = school.Value;
                i = 1;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
                i = 1;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_ID == subject.Value).ToList();
                ViewBag.subject = subject.Value;
                i = 1;
            }
            if (chapter != null && chapter != 0)
            {
                res = res.Where(a => a.Chapter_ID == chapter.Value).ToList();
                ViewBag.chapter = chapter.Value;
                i = 1;
            }
            if (studentid != null && studentid != 0)
            {
                res = res.Where(a => a.Regd_ID == studentid.Value).ToList();
                ViewBag.student = studentid.Value;
                i = 1;
            }
            Session["worksheetlist"] = res;
            if (i == 0)
            {
                res = res.Take(20).ToList();
            }
            return View(res);
        }
        [HttpPost]
        public ActionResult SaveScore(int worksheetid, string score)
        {
            tbl_DC_Worksheet _Worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Worksheet_Id == worksheetid).FirstOrDefault();
            _Worksheet.Total_Mark = Convert.ToInt32(score);
            _Worksheet.Verified = true;
            _Worksheet.Modified_Date = DateTime.Now;
            DbContext.SaveChanges();
            return RedirectToAction("Worksheet");
        }
        public ActionResult DownloadWorksheetList()
        {

            XLWorkbook oWB = new XLWorkbook();
            var worksheetlist = Session["worksheetlist"] as List<SP_GET_WORKSHEET_REPORT_Result>;
            var works = worksheetlist.Select(a => new Class_WORKSHEET_REPORT_Result
            {
                Board_Name = a.Board_Name,
                Chapter = a.Chapter,
                Class_Name = a.Class_Name,
                Customer_Name = a.Customer_Name,
                Mobile = a.Mobile,
                SectionName = a.SectionName,
                Subject = a.Subject,
                TeacherName = a.TeacherName,
                Total_Mark = a.Total_Mark.Value,
                Uploaded_On = a.Inserted_Date,
                Verified = a.Verified.Value,
                Verified_On = a.Modified_Date
            }).ToList();
            // COUNT TABLE DATA
            DataTable gdt = ToDataTable<Class_WORKSHEET_REPORT_Result>(works);
            int lastCellNo1 = gdt.Rows.Count + 1;
            gdt.TableName = "Worksheet_Details";

            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet = oWB.Worksheet(1);
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "Worksheet Report.xlsx");
        }
        public partial class Class_WORKSHEET_REPORT_Result
        {
            public string Customer_Name { get; set; }
            public string Board_Name { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Subject { get; set; }
            public string Chapter { get; set; }
            public string TeacherName { get; set; }
            public string Mobile { get; set; }
            public int Total_Mark { get; set; }
            public bool Verified { get; set; }
            public Nullable<System.DateTime> Verified_On { get; set; }
            public Nullable<System.DateTime> Uploaded_On { get; set; }
        }
        #endregion
        #region Upload student Details
        public ActionResult Uploadstudent()
        {
            return View();
        }
        public ActionResult DownloadSampleStudentExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("School");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Mobile");
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleStudent.xlsx");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentDetails(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("Uploadstudent");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Student_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                string name = dr["Student_Name"].ToString();
                                if (mobile != "" && name != "")
                                {
                                    tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    if (reg != null)
                                    {
                                        string schoolname = dr["School"].ToString();
                                        string classname = dr["Class"].ToString();
                                        string sectionname = dr["Section"].ToString();
                                        var schoolid = DbContext.tbl_DC_School_Info.Where(a => a.SchoolName == schoolname && a.IsActive == true).FirstOrDefault().SchoolId;
                                        var classid = DbContext.tbl_DC_Class.Where(a => a.Class_Name == classname && a.Board_Id == 1 && a.Is_Active == true && a.Is_Deleted == false).FirstOrDefault().Class_Id;
                                        var sectionid = DbContext.tbl_DC_Class_Section.Where(a => a.SectionName == sectionname && a.Class_Id == classid && a.School_Id == schoolid && a.IsActive == true).FirstOrDefault().SectionId;

                                        reg.Customer_Name = name;
                                        reg.SchoolId = schoolid;
                                        reg.SectionId = sectionid;
                                        reg.Modified_By = HttpContext.User.Identity.Name;
                                        reg.Modified_Date = DateTime.Now;
                                        DbContext.SaveChanges();
                                        var regid = reg.Regd_ID;

                                        tbl_DC_Registration_Dtl m = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == regid && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

                                        m.Board_ID = 1;
                                        m.Class_ID = classid;
                                        m.Modified_Date = DateTime.Now;
                                        m.Modified_By = reg.Regd_ID.ToString();
                                        DbContext.SaveChanges();

                                        var worksheet = DbContext.tbl_DC_Worksheet.Where(a => a.Regd_ID == regid && a.Is_Active == true && a.Is_Deleted == false).ToList();
                                        foreach (tbl_DC_Worksheet wrk in worksheet)
                                        {
                                            wrk.Board_Id = 1;
                                            wrk.Class_Id = classid;
                                            wrk.SectionId = sectionid;
                                            DbContext.SaveChanges();

                                        }
                                        var ticket = DbContext.tbl_DC_Ticket.Where(a => a.Student_ID == regid).ToList();
                                        foreach (tbl_DC_Ticket wrk in ticket)
                                        {
                                            wrk.Board_ID = 1;
                                            wrk.Class_ID = classid;
                                            DbContext.SaveChanges();

                                        }
                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("Uploadstudent");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("Uploadstudent");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("Uploadstudent");
            }
        }

        #endregion
    }
}
