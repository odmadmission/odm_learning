﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using DigiChamps.Models;

namespace DigiChamps.Controllers
{
    public class ScholarShipsController : ApiController
    {
        DigiChampsEntities db = new DigiChampsEntities();

        [HttpGet]
        public IEnumerable<ScholarShip> GetAllScholarShips()
        {
            List<tbl_DC_ScholarShip> scholarShips = db.tbl_DC_ScholarShip.ToList();
            List<ScholarShip> data = new List<ScholarShip>();
            for (int i = 0; i < scholarShips.Count;i++ )
            {
                ScholarShip s = new ScholarShip();
                s.ID = scholarShips[i].ID;
                s.Discount = scholarShips[i].Discount;
                s.Validity = scholarShips[i].Validity;
                s.Active = scholarShips[i].Active;
                s.Deleted = scholarShips[i].Deleted;
                s.InsertDate = scholarShips[i].InsertDate;
                data.Add(s);
            }
            return data;
        }
   



    }
}
