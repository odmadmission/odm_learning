﻿using CrystalDecisions.Shared.Json;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Drawing.Imaging;
//using static DigiChamps.Controllers.SchoolController;
//using static DigiChamps.Controllers.StudentController;

namespace DigiChamps.Controllers
{
    public class TeacherAccessController : Controller
    {
        DigiChampsEntities DbContext = new DigiChampsEntities();
        DateTime today = DigiChampsModel.datetoserver();

        public ActionResult Index()
        {                
        return RedirectToAction("Login");
        }
        #region ---------- Teacher rDashboard ------------
        public ActionResult TeacherDashboard()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        #endregion

        #region -------------- Login --------------

        [HttpGet]
        public ActionResult Login()
        {           
         return View();            
        }


        [HttpPost]
        public ActionResult Login(string User_name, string password)
        {
            try
            {               
                var obj = DbContext.Teachers.Where(x => x.Mobile == User_name && x.Password == password).FirstOrDefault();

                if (obj != null)
                {
                    Session["TeacherId"] = obj.TeacherId;
                    Session["SchoolId"] = obj.SchoolId;
                    Session["Name"] = obj.Name;
                    Session["Email"] = obj.Email;
                    Session["Mobile"] = obj.Mobile;
                    Session["DepartmentName"] = obj.DepartmentName;
                    Session["Photo"] = obj.Photo;
                    Session["IsHead"] = obj.IsHead;

                    return RedirectToAction("TeacherDashboard");
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid credential for admin login.";
                    return View();

                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View();
        }
        #endregion

        #region ------------ MyProfile-------------------
        [HttpGet]
        public ActionResult MyProfile()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                var TeacherId = Convert.ToInt32(Session["TeacherId"]);
                Teacher _teachr = DbContext.Teachers.Where(x => x.TeacherId == TeacherId).FirstOrDefault();
                ViewBag.TeacherId = _teachr.TeacherId;
                ViewBag.Name = _teachr.Name;
                ViewBag.Email = _teachr.Email;
                //string date = Convert.ToString(_teachr.DateOfBirth).Substring(0, 10);
                //ViewBag.dateofbirth = date;
                ViewBag.Mobile = _teachr.Mobile;
                ViewBag.DepartmentName = _teachr.DepartmentName;
                ViewBag.IsHead = _teachr.IsHead;
                ViewBag.image = _teachr.Photo;
               
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
     
        [HttpPost]
        public ActionResult EditProfile(long TeacherId, HttpPostedFileBase Photo, string Name,string Mobile,string Email, string DepartmentName,string IsHead)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    var alldetail = DbContext.Teachers.Where(x => x.TeacherId ==TeacherId).FirstOrDefault();
                    string image = string.Empty;
                    if (alldetail != null)
                    {
                        alldetail.Name = Name;
                        alldetail.ModifiedOn = today;
                        alldetail.Mobile = Mobile;
                        alldetail.Email =Email;
                        alldetail.DepartmentName = DepartmentName;
                        if(IsHead=="on")
                        {
                            alldetail.IsHead = true;
                        }
                        else
                        {
                            alldetail.IsHead = false;
                        }
                        string guid = Guid.NewGuid().ToString();
                        if (Photo != null)
                        {
                            var fileName = Path.GetFileName(Photo.FileName.Replace(Photo.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                            image = Convert.ToString(fileName);
                            var path = Path.Combine(Server.MapPath("~/Images/TeacherAccess/"), fileName);
                            Photo.SaveAs(path);
                            alldetail.Photo = image;
                        }

                        DbContext.Entry(alldetail).State = EntityState.Modified;
                        DbContext.SaveChanges();
                        TempData["SuccessMessage"] = " Successfully Edit Profile ";
                        return RedirectToAction("MyProfile");
                    }
                }
                catch
                {
                    return View();
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        [HttpGet]
        public ActionResult ChangePassword()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }
        [HttpPost]
        public ActionResult TeacherChangePassword(string Old_Password, string New_Password, string Conf_Password)
        {
            string message = string.Empty;
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                if (New_Password != "" && Conf_Password != "" && Old_Password != "")
                {              
                        var data = DbContext.Teachers.Where(x =>x.Password == Old_Password).FirstOrDefault();

                        if (data != null)
                        {
                            if (New_Password == Conf_Password)
                            {                                
                                data.Password = New_Password;
                                DbContext.Entry(data).State = EntityState.Modified;
                                DbContext.SaveChanges();
                                TempData["SuccessMessage"] = " Successfully Edit Profile ";
                                return RedirectToAction("ChangePassword");
                            }
                            else
                            {
                                TempData["ErrorMessage"] = " New Password And Confirm Password Did Not Match, ";
                                return RedirectToAction("ChangePassword");
                            }
                        }
                        else
                        {
                        TempData["ErrorMessage"] = "Please Enter Old Password, Did Not Match, ";
                        return RedirectToAction("ChangePassword");
                    }
                    

                }
                else
                {
                    TempData["WarningMessage"] = "Please Enter Password. ";
                    return RedirectToAction("ChangePassword");
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
         
        }


        #endregion

        #region  ---------Logout --------------

        public ActionResult Logout()
        {           
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }
        #endregion

        #region ------------Assigned Subject----------
        public ActionResult AssignedSubject()
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null  && username != "")
            {
         
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
               var Schoollist = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var teacherslist = DbContext.Teachers.Where(a => a.Active == 1 ).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in teacherslist on a.Teacher_ID equals b.TeacherId
                       join c in subjectlist on a.Subject_Id equals c.Subject_Id
                       join d in sectionlist on a.SectionId equals d.SectionId
                       join e in classlist on a.Class_Id equals e.Class_Id
                       join f in boardlist on a.Board_Id equals f.Board_Id
                       
                       select new AssignTeacherCls
                       {
                           AssignId = a.AssignSubjectToTeacher_Id,
                           TeacherId = a.Teacher_ID,
                           BoardId = a.Board_Id,
                           BoardName = f.Board_Name,
                           ClassId = a.Class_Id,
                           ClassName = e.Class_Name,
                           SchoolId = b.SchoolId,
                           SchoolName= Schoollist.Where(z=>z.SchoolId==b.SchoolId).Select(z=>z.SchoolName).FirstOrDefault(),
                           SectionId = a.SectionId,
                           SectionName = d.SectionName,
                           SubjectId = a.Subject_Id,
                           SubjectName = c.Subject,
                           TeacherName = b.Name
                       }).ToList();
           
            return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        #endregion

        #region ------------- Student Details By Section Id ------------
        public ActionResult StudentDetails(Guid? SectionId)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {

                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.SectionId == SectionId).ToList();
                var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var schools = DbContext.tbl_DC_School_Info.ToList();
                var classs = DbContext.tbl_DC_Class.ToList();
                var sections = DbContext.tbl_DC_Class_Section.ToList();
                var boards = DbContext.tbl_DC_Board.ToList();


                var res = (from a in students
                           join b in std_dtls on a.Regd_ID equals b.Regd_ID

                           select new StudentDetails_Class
                           {
                               Regd_ID = a.Regd_ID,
                               Regd_No = a.Regd_No,
                               Customer_Name = a.Customer_Name,
                               SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                               Class_ID = b.Class_ID == null ? null : b.Class_ID,
                               SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                               Board_ID = b.Board_ID == null ? null : b.Board_ID,
                               boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                               schoolname = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                               Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                               SectionName = (a.SectionId == Guid.Empty && a.SectionId == null) ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName,
                               Mobile = a.Mobile,
                               Email = a.Email
                           }).ToList();
                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public class StudentDetails_Class
        {
            public int? Regd_ID { set; get; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<Guid> SchoolId { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string boardname { get; set; }
            public string schoolname { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }

        }
        #endregion

        #region---------------Student Dashboard -------------
        public ActionResult StudentDashboard(int? board, int? cls, Guid? sec, int? subject ,int ? Chapter)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                Guid id = new Guid(Session["SchoolId"].ToString());
                //var res = DbContext.View_Get_Worksheet_Tracker.Where(a => a.School_Id == id).ToList();


                //List<tbl_DC_Worksheet> wrk = null;    
              
               var  wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == subject && a.Chapter_Id == Chapter).ToList();
               
                var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == id).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in boardlist on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z=>z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();

            
                return View(res);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        public JsonResult GetSchoolList()
        {
            var SchoolId = new Guid(Session["SchoolId"].ToString());
            var school = DbContext.tbl_DC_School_Info.Where(x => x.IsActive == true && x.SchoolId == SchoolId).ToList();
             var res = (from a in school
                        select new
                         {
                             SchoolId = a.SchoolId,
                             School_Name = a.SchoolName,

                         }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBoardList()
        {
            var board = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            var res = (from a in board
                       select new
                       {
                           Board_Id = a.Board_Id,
                           Board_Name = a.Board_Name,

                       }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClassListdtl()
        {
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var classlist = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false ).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in classlist on a.Class_Id equals b.Class_Id

                       select new 
                       {
                           Class_Id = a.Class_Id,
                           Class_Name = b.Class_Name,
                          
                       }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionListdtl(int id)
        {

            Guid? schoolid = new Guid(Session["SchoolId"].ToString());
            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var scetionlist = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true  && x.School_Id == schoolid && x.Class_Id==id).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in scetionlist on a.SectionId equals b.SectionId

                       select new
                       {
                           SectionId = a.SectionId,
                           SectionName = b.SectionName

                       }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSubjectData(int id)
        {

            var teacherid = Convert.ToInt32(Session["TeacherId"]);
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_Id==id).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).ToList();
            var res = (from a in assignteacherlist
                       join b in subjectlist on a.Subject_Id equals b.Subject_Id

                       select new
                       {
                           Subject_Id = a.Subject_Id,
                           Subject = b.Subject

                       }).ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChapterData(int id)
        {
            var s = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Subject_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region--------------student Report---------------------------

        public class worksheetDetails
        {
            public int Worksheet_Id { get; set; }
            public int Regd_ID { get; set; }
            public string Student_Name { get; set; }
            public string Student_Mobile { get; set; }
            public int Board_Id { get; set; }
            public string Board_Name { get; set; }
            public int Class_Id { get; set; }
            public string Class_Name { get; set; }
            public Nullable<System.Guid> SectionId { get; set; }
            public string Section_Name { get; set; }
            public int Subject_Id { get; set; }
            public string Subject_Name { get; set; }
            public int Chapter_Id { get; set; }
            public string Chapter_Name { get; set; }
            public bool Verified { get; set; }
            public int Verified_By { get; set; }
            public string Verified_By_Name { get; set; }
            public int Total_Mark { get; set; }
            public Nullable<System.DateTime> Inserted_Date { get; set; }

            public Nullable<System.DateTime> Modified_Date { get; set; }
            public string Upload_Date { get; set; }
            public string Verify_Date { get; set; }
            public int Module_Id { get; set; }
            public string Module_Name { get; set; }
            public string File_Type { get; set; }
            public List<tbl_DC_Worksheet_Attachment> attachments { get; set; }
            public int TotalVerified { get; set; }
            public int TotalPending { get; set; }
            public int TotalSubmitted { get; set; }

            public string Status { get; set; }

        }
        
        public ActionResult StudentWorksheetReport(int? board, int? cls, Guid? sec, int? subject, int? Chapter,string Status)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
               
                Guid id = new Guid(Session["SchoolId"].ToString());
                ViewBag.Status = Status;

                List<tbl_DC_Worksheet> wrk = null;
                wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == subject && a.Chapter_Id == Chapter).ToList();

                if (Status == "V")
                {
                     wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == subject && a.Chapter_Id == Chapter && a.Verified==true ).ToList();

                }
                if (Status == "U")
                {
                     wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == subject && a.Chapter_Id == Chapter && a.Verified == false).ToList();

                }

               
                var wrksheet = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.SchoolId == id).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in boardlist on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Student_Mobile=h.Mobile,                            
                               Total_Mark = a.Total_Mark,
                               Status= a.Verified==true?"Verified":"Upload",
                               Upload_Date = a.Inserted_Date != null? Convert.ToDateTime(a.Inserted_Date).ToString("dd-MMM-yyyy"):"NA",
                               Verify_Date= a.Verified== true ? Convert.ToDateTime(a.Modified_Date).ToString("dd-MMM-yyyy") : "NA",
                               TotalVerified = wrk.Where(z => z.Verified == true).ToList().Count(),
                               TotalPending = wrk.Where(z => z.Verified == false).ToList().Count(),
                               TotalSubmitted = wrk.ToList().Count(),
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).ToList();

               
                return View(res);
               
            }
            else
            {
                return RedirectToAction("Login");
            }
        }



        [HttpGet]
        public ActionResult EditWorksheet(int id)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                Guid? schoolid = new Guid(Session["SchoolId"].ToString());
                ViewBag.Board_details = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
                ViewBag.Class_details = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
                ViewBag.Section_details = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == schoolid).Distinct().ToList();
                ViewBag.Subject_details = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false ).ToList();
                ViewBag.Chapter_details = DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();


                try
                {
                var wrk = DbContext.tbl_DC_Worksheet.Where(a => a.Is_Active == true && a.Worksheet_Id == id).ToList();
                var board = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true).ToList();
                var classes = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true).ToList();
                var chapters = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true).ToList();
                // var modules = dbContext.tbl_DC_Module.Where(a => a.Is_Active == true).ToList();
                var sections = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true).ToList();
                var teachers = DbContext.Teachers.Where(a => a.Active == 1).ToList();
                var worksheetattachments = DbContext.tbl_DC_Worksheet_Attachment.Where(a => a.Is_Active == true).ToList();
                var res = (from a in wrk
                           join b in board on a.Board_Id equals b.Board_Id
                           join c in classes on a.Class_Id equals c.Class_Id
                           join d in subjects on a.Subject_Id equals d.Subject_Id
                           join e in chapters on a.Chapter_Id equals e.Chapter_Id
                           //  join f in modules on a.Module_Id equals f.Module_ID
                           join g in sections on a.SectionId equals g.SectionId
                           join h in students on a.Regd_ID equals h.Regd_ID
                           select new worksheetDetails
                           {
                               Board_Id = a.Board_Id,
                               Board_Name = b.Board_Name,
                               Chapter_Id = a.Chapter_Id,
                               Chapter_Name = e.Chapter,
                               Class_Id = a.Class_Id,
                               Class_Name = c.Class_Name,
                               File_Type = a.File_Type,
                               Inserted_Date = a.Inserted_Date,
                               Modified_Date = a.Modified_Date,
                               Module_Id = a.Module_Id,
                               Module_Name = "",
                               Regd_ID = a.Regd_ID,
                               Section_Name = g.SectionName,
                               SectionId = a.SectionId,
                               Student_Name = h.Customer_Name,
                               Subject_Id = a.Subject_Id,
                               Subject_Name = d.Subject,
                               Total_Mark = a.Total_Mark,
                               Verified = a.Verified,
                               Verified_By = a.Verified_By,
                               Verified_By_Name = a.Verified_By != 0 ? teachers.Where(m => m.TeacherId == a.Verified_By).FirstOrDefault().Name : "",
                               Worksheet_Id = a.Worksheet_Id,
                               attachments = (from k in worksheetattachments
                                              where k.Worksheet_Id == a.Worksheet_Id
                                              select new tbl_DC_Worksheet_Attachment
                                              {
                                                  File_Name = k.File_Name,
                                                  Modified_Date = k.Modified_Date,
                                                  Path = "/Images/Worksheet/" + k.Path,
                                                  Worksheet_Attachment_Id = k.Worksheet_Attachment_Id,
                                                  Worksheet_Id = k.Worksheet_Id
                                              }).ToList(),
                           }).FirstOrDefault();

                    ViewBag.WorksheetDetails = res;
                    

                    return View(res);
            }

            catch
            {
                return View();
            }

            }
            else
            {
                return RedirectToAction("Login");
            }
        }


        [HttpPost]
        public ActionResult EditWorksheet(tbl_DC_Worksheet worksheet)
        {
            try
            {
                var teacherid = Convert.ToInt32(Session["TeacherId"]);
                var alldetail = DbContext.tbl_DC_Worksheet.Where(x => x.Worksheet_Id == worksheet.Worksheet_Id).FirstOrDefault();
                if (alldetail != null)
                {                   
                    alldetail.Total_Mark = worksheet.Total_Mark;
                    alldetail.Modified_Date = today;
                    alldetail.Modified_By = teacherid;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Edit Worksheet ";
                    return RedirectToAction("StudentWorksheetReport");
                }
            }
            catch
            {
                return View();
            }
            return View();

        }


        #endregion

        #region-------------------Ticket-------------

        public ActionResult TeacherTicket(int? board, int? cls, int? subject, int? Chapter,string Status)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    var teacherid = Convert.ToInt32(Session["TeacherId"]);
                    var assignteacherSubjectlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true && a.Teacher_ID == teacherid).Select(a=>a.Subject_Id).Distinct().ToList();


                    IOrderedQueryable<View_DC_Tickets_and_Teacher> data = null;
                    if (Status == "A")
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => assignteacherSubjectlist.Contains(r.Subject_ID.Value) && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                    }                   
                  else  if (Status == "U")
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => assignteacherSubjectlist.Contains(r.Subject_ID.Value) && r.Teach_ID == null && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);

                        if (data!=null)
                        {
                            ViewBag.Ticket = data;
                        }
                    }
                    else
                    {
                        data = DbContext.View_DC_Tickets_and_Teacher.Where(r => assignteacherSubjectlist.Contains(r.Subject_ID.Value) && r.Status == Status && r.Board_ID == board && r.Class_ID == cls && r.Subject_ID == subject && r.Chapter_Id == Chapter).OrderByDescending(x => x.Ticket_ID);
                        if (data != null)
                        {
                            ViewBag.Ticket = data;
                        }
                    }
                   // ViewBag.Ticket = data;
                    ViewBag.teachernames_tickets = DbContext.View_DC_CourseAssign.ToList();
                }
                catch (Exception ex)
                {
                    TempData["ErrorMessage"] = "Something went wrong.";
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }

        }
        public ActionResult ViewDoughtDetail(int? id)
        {
            try
            {               
                ViewBag.Breadcrumb = "Ticket";
                if (id != null)
                {
                    var get = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).ToList();
                    if (get.Count > 0)
                    {
                        ViewBag.check_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        ViewBag.teacher = get.FirstOrDefault().Teach_ID;
                        ViewBag.status = get.FirstOrDefault().Status;
                        if (get.FirstOrDefault().Teach_ID != null)
                        {
                            int tr = Convert.ToInt32(get.FirstOrDefault().Teach_ID);
                            var tdata = DbContext.Teachers.Where(x => x.TeacherId == tr).FirstOrDefault();
                            if (tdata != null)
                            {
                                if (tdata.Name != null)
                                {
                                    ViewBag.teacher = tdata.Name;
                                }
                                else
                                {
                                    ViewBag.teacher = null;
                                }

                            }
                            else
                            {
                                ViewBag.teacher = null;
                            }
                        }
                        ViewBag.viewticket = get.ToList();
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid ticket details.";
                        return RedirectToAction("TeacherTicket");
                    }


                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid ticket details.";
                    return RedirectToAction("TeacherTicket");
                }
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = "Something went wrong.";
                return RedirectToAction("TeacherTicket");
            }
            return View();
        }

        public ActionResult Ticket_report(string f_Date, string t_Date)
        {
            if (f_Date != "" && t_Date != "")
            {
                if (Convert.ToDateTime(f_Date) <= today.Date)
                {
                    if (Convert.ToDateTime(t_Date) <= today.Date)
                    {


                        if (Convert.ToDateTime(f_Date) <= Convert.ToDateTime(t_Date))
                        {
                            string fdtt = f_Date + " 00:00:00 AM";
                            string tdtt = t_Date + " 23:59:59 PM";
                            DateTime fdt = Convert.ToDateTime(fdtt);
                            DateTime tdt = Convert.ToDateTime(tdtt);
                            var logstatus = (from c in DbContext.View_DC_Tickets_and_Teacher where c.Inserted_Date >= fdt && c.Inserted_Date <= tdt select c).ToList().OrderByDescending(x => x.Inserted_Date).OrderByDescending(x => x.Ticket_ID);
                            ViewBag.Ticket = logstatus.ToList();
                            ViewBag.teachernames_tickets = DbContext.View_DC_CourseAssign.ToList();
                        }
                        else
                        {

                            TempData["ErrorMessage"] = "From date should be less or equal to todate.";
                        }

                    }
                    else
                    {

                        TempData["ErrorMessage"] = "To date should be less or equal to from-date.";
                    }
                }
                else
                {

                    TempData["ErrorMessage"] = "From date should be less than today date.";
                }
            }
            else
            {

                TempData["ErrorMessage"] = "Please enter Date to Search.";
            }
            ViewBag.Breadcrumb = "view Tickets";
            return View("TeacherTicket");
        }

        [HttpGet]
        public ActionResult RejectTicket(int? id)
        {
            try
            {
               
                ViewBag.Breadcrumb = "Ticket";
                if (id != null)
                {
                    var tdata = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).FirstOrDefault();


                    ViewBag.studentname = tdata.Customer_Name;


                    ViewBag.classname = tdata.Class_Name;

                    ViewBag.question = tdata.Question.ToString();
                    ViewBag.tkid = tdata.Ticket_ID;
                    ViewBag.status = tdata.Status;
                    ViewBag.remark = tdata.Remark;
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid ticket details.";
                    return RedirectToAction("TeacherTicket");
                }
            }
            catch (Exception ex)
            {

                TempData["ErrorMessage"] = "Something went wrong.";

            }

            return View();
        }

        [HttpPost]
        public ActionResult RejectTicket(string Remark_Reject, string h_tkid)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                    if (Remark_Reject.Trim() != "")
                    {
                        int id = Convert.ToInt32(h_tkid);
                        tbl_DC_Ticket tkt_rj = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                        tkt_rj.Status = "R";
                        tkt_rj.Remark = Remark_Reject;
                        DbContext.SaveChanges();
                        var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == tkt_rj.Student_ID).FirstOrDefault();
                        //if (get_student.Email != null)
                        //{
                        //    //Close_ticket_mail(tkt_rj.Student_ID.ToString(), tkt_rj.Ticket_No);
                        //    sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, tkt_rj.Ticket_No.ToString(), "R", Remark_Reject.ToString());
                        //}
                        TempData["SuccessMessage"] = "Question is rejected.";
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Please provide reason of rejection.";
                    }
                }
                catch (Exception ex)
                {
                    TempData["WarningMessage"] = "Something went wrong.";

                }

                return RedirectToAction("RejectTicket");
            }
            else
            {
                return RedirectToAction("Logout", "TeacherAccess");
            }

        }

        [HttpGet]
        public ActionResult AnswerTicket(int? id)
        {
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                try
                {
                 
                    ViewBag.Breadcrumb = "Answer Ticket";
                    if (id != null)
                    {

                        var ticket_qsn = DbContext.View_DC_All_Tickets_Details.Where(x => x.Ticket_ID == id).FirstOrDefault();
                        ViewBag.student_name = ticket_qsn.Customer_Name;
                        ViewBag.TClass_name = ticket_qsn.Class_Name;
                        ViewBag.tquestion = ticket_qsn.Question;
                        ViewBag.status = ticket_qsn.Status;
                        ViewBag.h_tkid = ticket_qsn.Ticket_ID;
                        ViewBag.ticketno = ticket_qsn.Ticket_No;
                        ViewBag.studentname = ticket_qsn.Student_ID;
                        var ticket_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();


                        ViewBag.all_ticketansr = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();
                        ViewBag.comments = DbContext.tbl_DC_Ticket_Thread.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).ToList();
                        ViewBag.isclosed = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).Select(x => x.Status).FirstOrDefault();
                        ViewBag.check_answer = DbContext.tbl_DC_Ticket_Dtl.Where(x => x.Ticket_ID == id && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                    }
                    else
                    {
                        return RedirectToAction("TeacherTicket", "TeacherAccess");
                    }
                }
                catch (Exception ex)
                {

                    TempData["ErrorMessage"] = "Something went wrong.";
                    return RedirectToAction("Viewticekts", "TeacherAccess");
                }
            }
            else
            {
                return RedirectToAction("Logout", "TeacherAccess");
            }

            return View();
        }
        [HttpPost]
        public JsonResult AnswerReply(int Ticket_id, int Ticket_answerid, string msgbody, string close, string remark)
        {
          
            string message = string.Empty;
            var username = Convert.ToString(Session["Mobile"]);
            if (username != null && username != "")
            {
                if (remark != "" || msgbody!="")
                {
                    try
                    {
                        var teacherid = Convert.ToInt32(Session["TeacherId"]);                     
                        tbl_DC_Ticket_Thread _ticket_thred = new tbl_DC_Ticket_Thread();
                        _ticket_thred.Ticket_ID = Ticket_id;
                        _ticket_thred.Ticket_Dtl_ID = Ticket_answerid;
                        _ticket_thred.User_Comment = msgbody;
                        _ticket_thred.User_Comment_Date = today;
                        _ticket_thred.User_Id = teacherid;
                        _ticket_thred.Is_Teacher = true;
                        _ticket_thred.Is_Active = true;
                        _ticket_thred.Is_Deleted = false;
                        DbContext.tbl_DC_Ticket_Thread.Add(_ticket_thred);
                        DbContext.SaveChanges();
                        if (close == "on")
                        {
                            tbl_DC_Ticket_Assign _tbl_close = DbContext.tbl_DC_Ticket_Assign.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                            _tbl_close.Is_Close = true;
                            _tbl_close.Remark = remark;
                            _tbl_close.Close_Date = today;
                            _tbl_close.Modified_By = teacherid;
                            _tbl_close.Modified_Date = today;
                            DbContext.SaveChanges();
                            tbl_DC_Ticket _tbl_status = DbContext.tbl_DC_Ticket.Where(x => x.Ticket_ID == Ticket_id).FirstOrDefault();
                            _tbl_status.Status = "C";
                            _tbl_status.Modified_By = teacherid;
                            _tbl_status.Modified_Date = today;
                            DbContext.SaveChanges();
                            var get_student = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == _tbl_close.Student_ID).FirstOrDefault();
                            //if (get_student.Email != null)
                            //{
                            //    sendMail_close_reject("Ticket_close", get_student.Email.ToString(), get_student.Customer_Name, _tbl_close.Ticket_No.ToString(), "C", remark);
                            //}
                        }
                        message = "1";
                    }
                    catch (Exception ex)
                    {
                        message = "2";

                    }
                }
                else
                {
                    message = "3";
                }
            }
            else
            {
                message = "4";
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AnswerTicket(string Answer_Ticket, HttpPostedFileBase RegImage3, string h_tkid)
        {
         
            if (h_tkid != "")
            {
                if (Answer_Ticket.Trim() != "")
                {
                    int id = Convert.ToInt32(h_tkid);

                    tbl_DC_Ticket_Dtl tk_dtl = new tbl_DC_Ticket_Dtl();
                    tk_dtl.Ticket_ID = id;
                    tk_dtl.Answer = Answer_Ticket;
                    tk_dtl.Replied_By = 1;//hard_coded
                    tk_dtl.Replied_Date = today;
                    tk_dtl.Is_Active = true;
                    tk_dtl.Is_Deleted = false;
                    if (RegImage3 != null)
                    {

                        string guid = Guid.NewGuid().ToString();
                        var fileName = Path.GetFileName(RegImage3.FileName.Replace(RegImage3.FileName.Split('.').FirstOrDefault().ToString(), guid.ToString()));
                        var path = Path.Combine(Server.MapPath("~/Images/Qusetionimages/"), fileName);
                        RegImage3.SaveAs(path);
                        tk_dtl.Answer_Image = fileName.ToString();

                    }
                    DbContext.tbl_DC_Ticket_Dtl.Add(tk_dtl);
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = "You have answerd successfully.";
                    return RedirectToAction("AnswerTicket", "TeacherAccess", new { id = h_tkid });
                }
                else
                {
                    TempData["WarningMessage"] = "Insert a answer to the question.";
                    return RedirectToAction("AnswerTicket", "TeacherAccess", new { id = h_tkid });
                }
            }

            else
            {
                TempData["ErrorMessage"] = "Something went wrong.";

            }

            return View();
        }



        #endregion
    }
}
