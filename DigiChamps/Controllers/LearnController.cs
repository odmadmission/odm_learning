﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using DigiChamps.Models;
using DigiChamps.Controllers;
using System.Web;

namespace DigiChamps.Controllers
{
    //[Authorize]
    public class LearnController : ApiController
    {
        DateTime today = DigiChampsModel.datetoserver();
        DigiChampsEntities DbContext = new DigiChampsEntities();
        // GET api/<controller>


        [HttpGet]
        public HttpResponseMessage GetSubjectdetails(int sid)
        {
            try
            {
                var reg = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var regdtl = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var board = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var cls = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var clssec = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
                var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                var res = (from a in reg
                           join b in regdtl on a.Regd_ID equals b.Regd_ID
                           join c in board on b.Board_ID equals c.Board_Id
                           join d in cls on b.Class_ID equals d.Class_Id
                           join e in clssec on a.SectionId equals e.SectionId
                           where a.Regd_ID == sid
                           select new studentsubjectcls
                           {
                               BoardId = c.Board_Id,
                               BoardName = c.Board_Name,
                               ClassName = d.Class_Name,
                               ClassId = d.Class_Id,
                               SectionName = e.SectionName,
                               SectionId = e.SectionId,
                               Regd_ID = a.Regd_ID,
                               subj = (from m in subjects where m.Class_Id == b.Class_ID select new subjectcls { subjectid = m.Subject_Id, subjectname = m.Subject }).ToList()
                           }).FirstOrDefault();
                return Request.CreateResponse(HttpStatusCode.OK, new { result = res, msg = "success" });
            }
            catch (Exception e1)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { msg = "error" });
            }
        }
        public class studentsubjectcls
        {
            public int BoardId { get; set; }
            public string BoardName { get; set; }
            public int ClassId { get; set; }
            public string ClassName { get; set; }
            public Guid SectionId { get; set; }
            public string SectionName { get; set; }
            public int Regd_ID { get; set; }
            public List<subjectcls> subj { get; set; }
        }
        public class subjectcls
        {
            public int subjectid { get; set; }
            public string subjectname { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage learnSubjectdetails(int id, int eid)
        {
            try
            {
                List<Digichamps_web_Api.ProgressData> PDataList = new List<Digichamps_web_Api.ProgressData>();
                var Total_Videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid
                    && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count();

                var Total_Online_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                    x.Exam_type == 5 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count();
                var Total_Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                                   x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count();
                var Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                   x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).
                                   ToList().Select(x => x.Question_PDF).Count();
                var total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                   x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count;


                var Total_Videos_Progress = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                    && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "VIDEO" && x.Regd_ID == id).ToList().Count();

                var Total_Online_test_Progress = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                    && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "CBT" && x.Regd_ID == id).Select(x => x.Module_ID).ToList().Count();

                var Total_Pre_req_test_Progress = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                    && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "PRT" && x.Regd_ID == id).Select(x => x.Module_ID).ToList().Count();

                var Total_question_pdf_Progress = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                    && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "QB" && x.Regd_ID == id).Select(x => x.Module_ID).ToList().Count();

                var total_pdfs_Progress = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                    && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "SN" && x.Regd_ID == id).Select(x => x.Module_ID).ToList().Count();
                tbl_DC_Subject sub = DbContext.tbl_DC_Subject.Where(x => x.Subject_Id == eid && x.Is_Active == true).FirstOrDefault();
                Digichamps_web_Api.ProgressData SubjectProgressData = new Digichamps_web_Api.ProgressData();
                SubjectProgressData.name = sub.Subject;
                SubjectProgressData.type = "S";
                if (Total_Videos_Progress == 0)
                {
                    SubjectProgressData.video = 0;
                }
                else
                {
                    SubjectProgressData.video = ((50.0) / (Total_Videos / Total_Videos_Progress));
                }

                if (Total_Online_test_Progress == 0)
                {
                    SubjectProgressData.cbt = 0;
                }
                else
                {
                    SubjectProgressData.cbt = ((15.0) / (Total_Online_test / Total_Online_test_Progress));
                }

                if (Total_Pre_req_test_Progress == 0)
                {
                    SubjectProgressData.prt = 0;
                }
                else
                {
                    SubjectProgressData.prt = ((5.0) / (Total_Pre_req_test / Total_Pre_req_test_Progress));
                }

                if (Total_question_pdf_Progress == 0)
                {
                    SubjectProgressData.qb = 0;
                }
                else
                {
                    SubjectProgressData.qb = ((15.0) / (Total_question_pdf / Total_question_pdf_Progress));
                }

                if (total_pdfs_Progress == 0)
                {
                    SubjectProgressData.sn = 0;
                }
                else
                {
                    SubjectProgressData.sn = ((15.0) / (total_pdfs / total_pdfs_Progress));
                }

                SubjectProgressData.total = SubjectProgressData.sn + SubjectProgressData.qb + SubjectProgressData.prt + SubjectProgressData.cbt + SubjectProgressData.video;
                // PDataList.Add(SubjectProgressData);

                List<tbl_DC_Chapter> subjectList = DbContext.tbl_DC_Chapter.Where(x => x.Subject_Id == eid && x.Is_Active == true && x.Is_Deleted == false).ToList();
                for (int i = 0; i < subjectList.Count; i++)
                {

                    int chapterId = subjectList[i].Chapter_Id;

                    var Total_VideosSubject = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid
                         && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null && x.Chapter_Id == chapterId).Select(x => x.Module_ID).ToList().Count();

                    var Total_Online_testSubject = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                        x.Exam_type == 5 && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == chapterId).ToList().Count();
                    var Total_Pre_req_testSubject = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                                       x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == chapterId).ToList().Count();
                    var Total_question_pdfSubject = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                       x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == chapterId).
                                       ToList().Select(x => x.Question_PDF).Count();
                    var total_pdfsSubject = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                       x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false && x.Chapter_Id == chapterId).ToList().Count;


                    var Total_Videos_ProgressSubject = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                        && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "VIDEO" && x.Regd_ID == id && x.Chapter_ID == chapterId).Select(x => x.Module_ID).ToList().Count();

                    var Total_Online_test_ProgressSubject = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                        && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "CBT" && x.Regd_ID == id && x.Chapter_ID == chapterId).Select(x => x.Module_ID).ToList().Count();

                    var Total_Pre_req_test_ProgressSubject = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                        && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "PRT" && x.Regd_ID == id && x.Chapter_ID == chapterId).Select(x => x.Module_ID).ToList().Count();

                    var Total_question_pdf_ProgressSubject = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                        && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "QB" && x.Regd_ID == id && x.Chapter_ID == chapterId).Select(x => x.Module_ID).ToList().Count();

                    var total_pdfs_ProgressSubject = DbContext.tbl_DC_StudentSubjectProgress.Where(x => x.Subject_ID == eid
                        && x.Is_Active == true && x.Is_Delete == false && x.Module_Type == "SN" && x.Regd_ID == id && x.Chapter_ID == chapterId).Select(x => x.Module_ID).ToList().Count();

                    Digichamps_web_Api.ProgressData SubjectProgressDataSubject = new Digichamps_web_Api.ProgressData();
                    SubjectProgressDataSubject.name = subjectList[i].Chapter;
                    SubjectProgressDataSubject.type = "C";
                    SubjectProgressDataSubject.chapterId = subjectList[i].Chapter_Id;
                    if (Total_Videos_ProgressSubject == 0)
                    {
                        SubjectProgressDataSubject.video = 0;
                    }
                    else
                    {
                        if (Total_VideosSubject == 0)
                            SubjectProgressDataSubject.video = 50;
                        else
                            SubjectProgressDataSubject.video = ((50.0) / (Total_VideosSubject / Total_Videos_ProgressSubject));
                    }


                    if (Total_Online_test_ProgressSubject == 0)
                    {
                        SubjectProgressDataSubject.cbt = 0;
                    }
                    else
                    {
                        if (Total_Online_testSubject == 0)
                            SubjectProgressDataSubject.cbt = 15;
                        else
                            SubjectProgressDataSubject.cbt = ((15.0) / (Total_Online_testSubject / Total_Online_test_ProgressSubject));
                    }

                    if (Total_Pre_req_test_ProgressSubject == 0)
                    {
                        SubjectProgressDataSubject.prt = 0;
                    }
                    else
                    {
                        if (Total_Pre_req_testSubject == 0)
                            SubjectProgressDataSubject.prt = 5;
                        else
                            SubjectProgressDataSubject.prt = ((5.0) / (Total_Pre_req_testSubject / Total_Pre_req_test_ProgressSubject));
                    }

                    if (Total_question_pdf_ProgressSubject == 0)
                    {
                        SubjectProgressDataSubject.qb = 0;
                    }
                    else
                    {
                        if (Total_question_pdfSubject == 0)
                            SubjectProgressDataSubject.qb = 15;
                        else
                            SubjectProgressDataSubject.qb = ((15.0) / (Total_question_pdfSubject / Total_question_pdf_ProgressSubject));
                    }

                    if (total_pdfs_ProgressSubject == 0)
                    {
                        SubjectProgressDataSubject.sn = 0;
                    }
                    else
                    {
                        if (total_pdfsSubject == 0)
                            SubjectProgressDataSubject.sn = 15;
                        else
                            SubjectProgressDataSubject.sn = ((15.0) / (total_pdfsSubject / total_pdfs_ProgressSubject));
                    }

                    SubjectProgressDataSubject.total = SubjectProgressDataSubject.sn +
                        SubjectProgressDataSubject.qb + SubjectProgressDataSubject.prt + SubjectProgressDataSubject.cbt + SubjectProgressDataSubject.video;

                    PDataList.Add(SubjectProgressDataSubject);

                }
                //Digichamps_web_Api.ProgressDataList PDL = new Digichamps_web_Api.ProgressDataList();
                //PDL.ProgressDataItems = PDataList;

                var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == id && x.IS_ACCEPTED == true).FirstOrDefault();
                if (stuobj != null)
                {
                    var chapters = DbContext.VW_DC_Package_Learn.Where(x => x.Regd_ID == id && x.Subject_ID == eid).ToList();
                    if (chapters.Count > 0)
                    {
                        var data = chapters.Select(x => x.Chapter_ID).Distinct();
                        var resultobj1 = new Digichamps_web_Api.learnsubjectwiseRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkgsubjectwisemodel
                            {
                                SubjectProgressData = SubjectProgressData,
                                Total_Chapters = chapters.Select(x => x.Chapter_ID).Distinct().Count(),
                                Total_Videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                    x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).
                                    Select(x => x.Module_ID).ToList().Count(),
                                Total_Online_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                                    x.Exam_type == 5 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count(),
                                Total_Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == eid &&
                                    x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count(),
                                Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid &&
                                    x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false)
                                    .ToList().Select(x => x.Question_PDF).Count(),
                                Total_question = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid && x.No_Of_Question != null
                                    && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.No_Of_Question).Sum(),
                                total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid && x.Module_Content != null
                                    && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                Chapterlist = (from c in chapters.Select(x => x.Chapter_ID).Distinct()

                                               select new Digichamps_web_Api.pkgLearnChapters
                                               {
                                                   chapterid = c,
                                                   total_question_pdf = DbContext.tbl_DC_Module.
                                                   Where(x => x.Chapter_Id == c && x.Question_PDF != null
                                                       && x.Is_Active == true).ToList().Select(x => x.Question_PDF).Count(),
                                                   online_test = DbContext.tbl_DC_Exam.Where(x => x.Chapter_Id == c
                                                       && x.Exam_type == 5 && x.Is_Active == true).ToList().Count(),
                                                   Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Chapter_Id == c
                                                       && x.Exam_type == 1 && x.Is_Active == true).ToList().Count(),
                                                   Chapter = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == c
                                                       && x.Is_Active == true).FirstOrDefault() != null ? DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == c
                                                       && x.Is_Active == true).FirstOrDefault().Chapter : null,
                                                   total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c
                                                       && x.Module_Content != null && x.Is_Active == true).Select(x => x.Module_ID).
                                                       Distinct().ToList().Count,
                                                   total_videos = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c
                                                       && x.Is_Active == true).Select(x => x.Module_ID).ToList().Count,
                                                   total_study_notes = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c
                                                       && x.Module_Content != null && x.Is_Active == true).Select(x => x.Chapter_Id).Count(),
                                                   total_sbts = DbContext.tbl_DC_Exam.Where(x => x.Chapter_Id == c && x.Is_Active == true).ToList().Count(),
                                                   ChapterImage = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == c
                                                       && x.Is_Active == true).Select(x => "https://learn.odmps.org/Images/Chapter/" +
                                                           x.ChapterImage + "").FirstOrDefault(),

                                               }).ToList()
                            }
                        };
                        for (int i = 0; i < resultobj1.success.Chapterlist.Count; i++)
                        {
                            for (int j = 0; j < PDataList.Count; j++)
                            {
                                if (resultobj1.success.Chapterlist[i].chapterid == PDataList[j].chapterId)
                                {
                                    resultobj1.success.Chapterlist[i].total_progress = PDataList[j].total;
                                }
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
                    }
                    else
                    {
                        var chapters1 = DbContext.Sp_DC_Getall_Packagelearn(stuobj.Class_ID).ToList();
                        if (chapters1.Count > 0)
                        {
                            var data = chapters.Select(x => x.Chapter_ID).Distinct();
                            var resultobj1 = new Digichamps_web_Api.learnsubjectwiseRESPONSE
                            {
                                success = new Digichamps_web_Api.Pkgsubjectwisemodel
                                {
                                    SubjectProgressData = SubjectProgressData,
                                    Total_Chapters = chapters1.Where(x => x.Subject_Id == eid).
                                    Select(x => x.Chapter_Id).Distinct().Count(),
                                    Total_Videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid
                                        && x.Class_Id == stuobj.Class_ID &&
                                        x.Is_Active == true && x.Is_Deleted == false
                                        && x.Module_video != null).Select(x => x.Module_ID).ToList().Count(),

                                    Total_question_pdf = DbContext.tbl_DC_Module.
                                    Where(x => x.Subject_Id == eid && x.Question_PDF != null
                                        && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.Question_PDF).Count(),
                                    Total_question = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid && x.No_Of_Question != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.No_Of_Question).Sum(),
                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == eid && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                    Chapterlist = (from c in chapters1.Where(x => x.Subject_Id == eid).Select(x => x.Chapter_Id).Distinct()
                                                   select new Digichamps_web_Api.pkgLearnChapters
                                                   {
                                                       //chapterid = c,
                                                       //total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c && x.Question_PDF != null).ToList().Select(x => x.Question_PDF).Count(),
                                                       //Chapter = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == c).FirstOrDefault().Chapter,
                                                       //total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c && x.Module_Content != null).Select(x => x.Module_ID).Distinct().ToList().Count,
                                                       //total_videos = DbContext.tbl_DC_Module.Where(x => x.Chapter_Id == c).Select(x => x.Module_ID).ToList().Count
                                                       chapterid = c,
                                                       total_question_pdf = DbContext.tbl_DC_Module.
                                                       Where(x => x.Chapter_Id == c && x.Question_PDF != null && x.Is_Active == true).
                                                       ToList().Select(x => x.Question_PDF).Count(),
                                                       online_test = DbContext.tbl_DC_Exam.
                                                       Where(x => x.Chapter_Id == c && x.Exam_type == 5 && x.Is_Active == true).
                                                       ToList().Count(),
                                                       Pre_req_test = DbContext.tbl_DC_Exam.
                                                       Where(x => x.Chapter_Id == c && x.Exam_type == 1 && x.Is_Active == true).
                                                       ToList().Count(),
                                                       Chapter = DbContext.tbl_DC_Chapter.
                                                       Where(x => x.Chapter_Id == c && x.Is_Active == true).ToList().Count > 0 ? DbContext.tbl_DC_Chapter.
                                                       Where(x => x.Chapter_Id == c && x.Is_Active == true).FirstOrDefault().Chapter : "",
                                                       total_pdfs = DbContext.tbl_DC_Module.
                                                       Where(x => x.Chapter_Id == c && x.Module_Content != null && x.Is_Active == true).
                                                       Select(x => x.Module_ID).Distinct().ToList().Count,
                                                       total_videos = DbContext.tbl_DC_Module.
                                                       Where(x => x.Chapter_Id == c && x.Is_Active == true).Select(x => x.Module_ID).
                                                       ToList().Count,
                                                       total_study_notes = DbContext.tbl_DC_Module.
                                                       Where(x => x.Chapter_Id == c && x.Module_Content != null && x.Is_Active == true).
                                                       Select(x => x.Chapter_Id).Count(),
                                                       total_sbts = DbContext.tbl_DC_Exam.
                                                       Where(x => x.Chapter_Id == c && x.Is_Active == true).ToList().
                                                       Count(),
                                                       ChapterImage = DbContext.tbl_DC_Chapter.
                                                       Where(x => x.Chapter_Id == c && x.Is_Active == true).
                                                       Select(x => "https://learn.odmps.org/Images/Chapter/" + x.ChapterImage + "").FirstOrDefault(),
                                                   }).ToList()
                                }
                            };


                            for (int i = 0; i < resultobj1.success.Chapterlist.Count; i++)
                            {
                                for (int j = 0; j < PDataList.Count; j++)
                                {
                                    if (resultobj1.success.Chapterlist[i].chapterid == PDataList[j].chapterId)
                                    {
                                        resultobj1.success.Chapterlist[i].total_progress = PDataList[j].total;
                                    }
                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    var errobj = new Digichamps_web_Api.Errorresult
                    {
                        Error = new Digichamps_web_Api.Errorresponse
                        {
                            Message = "Invalid user details.",
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                }

            }
            catch (Exception e)
            {

                throw e;//return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        public HttpResponseMessage LearnChapterdetails(int id, int eid) //Student id and Chapter id
        {
            try
            {
                var v = DbContext.SP_DC_GetModules_for_api(eid, id).ToList();
                List<DigiChamps.Models.Digichamps_web_Api.Pdf_url> sample1 = new List<DigiChamps.Models.Digichamps_web_Api.Pdf_url>();
                var stuobj = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == id).FirstOrDefault();

                // if (stuobj != null)
                // {
                //DateTime dt = Convert.ToDateTime(stuobj.Inserted_Date);
                var Chapobj = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == eid && x.Is_Active == true).FirstOrDefault();
                //if (Chapobj != null)
                //{
                //var Chapters = (from d in DbContext.VW_DC_Package_Learn.
                //                    Where(x => x.Regd_ID == id && x.Chapter_ID == eid).GroupBy(x => x.Chapter_ID)
                //                select new PackagePreviewModel
                //                {
                //                    Chapter = d.FirstOrDefault().Chapter_Name,
                //                    Is_Offline = d.FirstOrDefault().Is_Offline
                //                }).ToList();

                var listmodule = DbContext.tbl_DC_Module.
                    Where(x => x.Chapter_Id == eid && x.Is_Active == true &&
                        x.Is_Deleted == false && x.Module_video != null).ToList();

                var listdata = (from c in v.Where(x => x.Question_PDF != null)
                                select new
                                    DigiChamps.Models.Digichamps_web_Api.Questionbanks
                                {
                                    noofques = c.No_Of_Question,
                                    Question_Pdfs = c.Question_PDF,
                                    Modulename = c.Module_Name,
                                    moduleIDss = c.Module_ID

                                }).ToList();
                //if (Chapters.Count > 0)
                //{
                var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                {
                    success = new Digichamps_web_Api.chapterlist
                    {
                        Chapter_Name = Chapobj.Chapter,
                        Chapterid = eid,
                        ChapterImage = "https://learn.odmps.org/Images/Chapter/" + Chapobj.ChapterImage + "",
                        Today_date = today,
                        Is_Offline = false,//Chapters.FirstOrDefault().Is_Offline,
                        ChapterModules = (from d in v
                                          select new Digichamps_web_Api.ChapterModuleList
                                          {
                                              Module_Id = d.Module_ID,
                                              Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                                              Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                                              Description = d.Module_Desc == null ? "" : d.Module_Desc,
                                              pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                                              pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                                              Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                                              Is_Free = d.Is_Free,
                                              // Validity = d.Validity,
                                              Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                                              Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                                              No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                                              Is_Free_Test = d.Is_Free_Test == null ? false : d.Is_Free_Test,
                                              Media_Id = d.Module_video == null ? "" :(d.Video_Type=="Y"?(d.Module_video):d.Module_video),
                                              VideoKey = d.Module_video == null ? "" : (d.Video_Type=="Y"?(d.Module_video):d.Module_video),
                                              Is_Expire = false,// d.Module_video == null ? true : false,
                                              Is_Avail = true,
                                              //thumbnail_key = "-320",
                                              thumbnail_key = "",
                                              Video_Type = d.Video_Type,                                             
                                              //template_id = "-ozl7iD1S",
                                              template_id = "",
                                              is_Video_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 2),
                                              is_question_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 1),
                                              is_studynotebookmarked = FindModuleasBookmarked(d.Module_ID, id, 3),
                                          }).ToList(),
                        Quesbank = (listdata == null) ? (from c in v select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = 0, Question_Pdfs = "", Modulename = "" }).ToList() : (from c in listdata.Where(x => x.Question_Pdfs != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.noofques, Question_Pdfs = c.Question_Pdfs, Modulename = c.Modulename, is_question_bookmarked = FindModuleasBookmarked(c.moduleIDss, id, 1), moduleIDss = c.moduleIDss }).ToList(),
                        ppt = (from f in v.Where(x => x.Class_PPT != null).ToList()
                               select new DigiChamps.Models.Digichamps_web_Api.pptlist
                               {
                                   Class_PPT = f.Class_PPT != null ? ("/Module/PPT/" + f.Class_PPT) : "",
                                   Class_PPT_Name = f.Class_PPT_Name,
                                   ModuleId = f.Module_ID
                               }).ToList(),
                        ncert = (from f in v.Where(x => x.NCERT_PDF != null).ToList()
                                 select new DigiChamps.Models.Digichamps_web_Api.Ncertlist
                                 {
                                     NCERT_PDF = f.NCERT_PDF != null ? ("/Module/NCERT/" + f.NCERT_PDF) : "",
                                     NCERT_PDF_Name = f.NCERT_PDF_Name,
                                     ModuleId = f.Module_ID
                                 }).ToList(),
                        pdfs = (from c in v.Where(x => x.Module_Content != null)
                                select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                {
                                    moduleID = c.Module_ID,
                                    Url = c.Module_Content,
                                    Modulename = c.Module_Name,
                                    is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                }).ToList() == null ? (from c in v
                                                       select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                       {
                                                           Url = "",
                                                           Modulename = ""
                                                       }).ToList() : (from c in v.Where(x => x.Module_Content != null)
                                                                      select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                      {
                                                                          moduleID = c.Module_ID,
                                                                          Url = c.Module_Content,
                                                                          Modulename = c.Module_Name,
                                                                          is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                                                      }).ToList()
                    }
                };

                //DateTime diff = new DateTime(2019, 3, 31);
                //if (DateTime.Today.Date <= diff.Date)
                //{

                //    for (int i = 0; i < resultobj.success.ChapterModules.Count; i++)
                //    {
                //        resultobj.success.ChapterModules[i].Is_Expire = false;

                //    }


                //}
                return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                //}
                //else
                //{
                //    var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                //    {
                //        success = new Digichamps_web_Api.chapterlist
                //        {
                //            Chapter_Name = Chapobj.Chapter,
                //            Chapterid = eid,
                //            Today_date = today,
                //            ChapterModules = (from d in DbContext.SP_DC_GetModules_for_api(eid, id)
                //                              select new Digichamps_web_Api.ChapterModuleList
                //                              {

                //                                  Module_Id = d.Module_ID,
                //                                  Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                //                                  Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                //                                  Description = d.Module_Desc == null ? "" : d.Module_Desc,
                //                                  pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                //                                  pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                //                                  Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                //                                  Is_Free = d.Is_Free,
                //                                  Validity = d.Validity,
                //                                  Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                //                                  Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                //                                  No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                //                                  Is_Free_Test = true, //d.Is_Free_Test == null ? false : d.Is_Free_Test,
                //                                  Media_Id = d.Module_video,// d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                //                                  //template_id = "-ozl7iD1S",
                //                                  template_id = "",
                //                                  VideoKey = d.Module_video,//d.Module_video d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                //                                  Is_Expire = false,//d.Is_Free == true ? (d.Validity > today ? false : true) : true,
                //                                  //thumbnail_key = "-320",
                //                                  thumbnail_key = "",
                //                                  Is_Avail = false,
                //                                  Video_Type = d.Video_Type,
                //                                  is_Video_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 2),
                //                                  is_question_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 1),
                //                                  is_studynotebookmarked = FindModuleasBookmarked(d.Module_ID, id, 3),
                //                              }).ToList(),
                //            Quesbank = (listdata == null) ? (from c in v select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = 0, Question_Pdfs = "", Modulename = "" }).ToList() : (from c in listdata.Where(x => x.Question_Pdfs != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.noofques, Question_Pdfs = c.Question_Pdfs, Modulename = c.Modulename, is_question_bookmarked = FindModuleasBookmarked(c.moduleIDss, id, 1), moduleIDss = c.moduleIDss }).ToList(),
                //            pdfs = (from c in v.Where(x => x.Module_Content != null)
                //                    select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                //                    {
                //                        moduleID = c.Module_ID,
                //                        Url = c.Module_Content,
                //                        Modulename = c.Module_Name,
                //                        is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),

                //                    }).ToList() == null ? (from c in v
                //                                           select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                //                                           {
                //                                               Url = "",
                //                                               Modulename = ""
                //                                           }).ToList() : (from c in v.Where(x => x.Module_Content != null)
                //                                                          select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                //                                                          {
                //                                                              moduleID = c.Module_ID,
                //                                                              Url = c.Module_Content,
                //                                                              Modulename = c.Module_Name,
                //                                                              is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                //                                                          }).ToList()

                //        }
                //    };
                // DateTime diff = dt.AddDays(45);

                //DateTime diff = new DateTime(2019, 4, 1);

                //if (DateTime.Today.Date < diff.Date)
                //{

                //    for (int i = 0; i < resultobj.success.ChapterModules.Count; i++)
                //    {
                //        resultobj.success.ChapterModules[i].Is_Expire = false;
                //    }


                //}
                // return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                //}
                //    }
                //    else
                //    {
                //        var errobj = new Digichamps_web_Api.Errorresult
                //        {
                //            Error = new Digichamps_web_Api.Errorresponse
                //            {
                //                Message = "Invalid Chapter details.",
                //            }
                //        };
                //        return Request.CreateResponse(HttpStatusCode.OK, errobj);
                //    }
                //}
                //else
                //{
                //    var errobj = new Digichamps_web_Api.Errorresult
                //    {
                //        Error = new Digichamps_web_Api.Errorresponse
                //        {
                //            Message = "Invalid user details.",
                //        }
                //    };
                //    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                //}
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }



        [HttpGet]
        public HttpResponseMessage LearnChapterdetailsNew(int id, int eid) //Student id and Chapter id
        {
            try
            {
                var v = DbContext.SP_DC_GetModules_for_api(eid, id).ToList();
                List<DigiChamps.Models.Digichamps_web_Api.Pdf_url> sample1 = new List<DigiChamps.Models.Digichamps_web_Api.Pdf_url>();
                var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == id).FirstOrDefault();

                if (stuobj != null)
                {
                    //DateTime dt = Convert.ToDateTime(stuobj.Inserted_Date);
                    var Chapobj = DbContext.tbl_DC_Chapter.Where(x => x.Chapter_Id == eid && x.Is_Active == true).FirstOrDefault();
                    if (Chapobj != null)
                    {
                        var Chapters = (from d in DbContext.VW_DC_Package_Learn.
                                            Where(x => x.Regd_ID == id && x.Chapter_ID == eid).GroupBy(x => x.Chapter_ID)
                                        select new PackagePreviewModel
                                        {
                                            Chapter = d.FirstOrDefault().Chapter_Name,
                                            Is_Offline = d.FirstOrDefault().Is_Offline
                                        }).ToList();

                        var listmodule = DbContext.tbl_DC_Module.
                            Where(x => x.Chapter_Id == eid && x.Is_Active == true &&
                                x.Is_Deleted == false && x.Module_video != null).ToList();

                        var listdata = (from c in v.Where(x => x.Question_PDF != null)
                                        select new
                                            DigiChamps.Models.Digichamps_web_Api.Questionbanks
                                        {
                                            noofques = c.No_Of_Question,
                                            Question_Pdfs = c.Question_PDF,
                                            Modulename = c.Module_Name,
                                            moduleIDss = c.Module_ID

                                        }).ToList();
                        if (Chapters.Count > 0)
                        {
                            var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                            {
                                success = new Digichamps_web_Api.chapterlist
                                {
                                    Chapter_Name = Chapobj.Chapter,
                                    Chapterid = eid,
                                    ChapterImage = "https://learn.odmps.org/Images/Chapter/" + Chapobj.ChapterImage + "",
                                    Today_date = today,
                                    Is_Offline = Chapters.FirstOrDefault().Is_Offline,
                                    ChapterModules = (from d in v
                                                      select new Digichamps_web_Api.ChapterModuleList
                                                      {
                                                          Module_Id = d.Module_ID,
                                                          Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                                                          Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                                                          Description = d.Module_Desc == null ? "" : d.Module_Desc,
                                                          pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                                                          pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                                                          Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                                                          Is_Free = d.Is_Free,
                                                          Validity = d.Validity,
                                                          Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                                                          Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                                                          No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                                                          Is_Free_Test = d.Is_Free_Test == null ? false : d.Is_Free_Test,
                                                          Media_Id = d.Module_video == null ? "" : (d.Video_Type == "Y" ? ( d.Module_video) : d.Module_video),
                                                          VideoKey = d.Module_video == null ? "" : (d.Video_Type == "Y" ? ( d.Module_video) : d.Module_video),
                                                          

                                                          Is_Expire = false,// d.Module_video == null ? true : false,
                                                          Is_Avail = true,
                                                          //thumbnail_key = "-320",
                                                          thumbnail_key = "",
                                                          Video_Type = d.Video_Type,
                                                          //Class_PPT = d.Class_PPT != null ? ("/Module/PPT/" + d.Class_PPT) : "",
                                                          //Class_PPT_Name = d.Class_PPT_Name,
                                                          //NCERT_PDF = d.NCERT_PDF != null ? ("/Module/NCERT/" + d.NCERT_PDF) : "",
                                                          //NCERT_PDF_Name = d.NCERT_PDF_Name,
                                                          //template_id = "-ozl7iD1S",
                                                          template_id = "",
                                                          is_Video_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 2),
                                                          is_question_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 1),
                                                          is_studynotebookmarked = FindModuleasBookmarked(d.Module_ID, id, 3),
                                                      }).ToList(),
                                    Quesbank = (listdata == null) ? (from c in v select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = 0, Question_Pdfs = "", Modulename = "" }).ToList() : (from c in listdata.Where(x => x.Question_Pdfs != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.noofques, Question_Pdfs = c.Question_Pdfs, Modulename = c.Modulename, is_question_bookmarked = FindModuleasBookmarked(c.moduleIDss, id, 1), moduleIDss = c.moduleIDss }).ToList(),
                                    ppt = (from f in v.Where(x => x.Class_PPT != null).ToList()
                                           select new DigiChamps.Models.Digichamps_web_Api.pptlist
                                           {
                                               Class_PPT = f.Class_PPT != null ? ("/Module/PPT/" + f.Class_PPT) : "",
                                               Class_PPT_Name = f.Class_PPT_Name,
                                               ModuleId = f.Module_ID
                                           }).ToList(),
                                    ncert = (from f in v.Where(x => x.NCERT_PDF != null).ToList()
                                             select new DigiChamps.Models.Digichamps_web_Api.Ncertlist
                                             {
                                                 NCERT_PDF = f.NCERT_PDF != null ? ("/Module/NCERT/" + f.NCERT_PDF) : "",
                                                 NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                 ModuleId = f.Module_ID
                                             }).ToList(),
                                    pdfs = (from c in v.Where(x => x.Module_Content != null)
                                            select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                            {
                                                moduleID = c.Module_ID,
                                                Url = c.Module_Content,
                                                Modulename = c.Module_Name,
                                                is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                            }).ToList() == null ? (from c in v
                                                                   select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                   {
                                                                       Url = "",
                                                                       Modulename = ""
                                                                   }).ToList() : (from c in v.Where(x => x.Module_Content != null)
                                                                                  select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                                  {
                                                                                      moduleID = c.Module_ID,
                                                                                      Url = c.Module_Content,
                                                                                      Modulename = c.Module_Name,
                                                                                      is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                                                                  }).ToList()
                                }
                            };

                            //DateTime diff = new DateTime(2019, 3, 31);
                            //if (DateTime.Today.Date <= diff.Date)
                            //{

                            //    for (int i = 0; i < resultobj.success.ChapterModules.Count; i++)
                            //    {
                            //        resultobj.success.ChapterModules[i].Is_Expire = false;

                            //    }


                            //}
                            return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                        }
                        else
                        {
                            var resultobj = new Digichamps_web_Api.ChapterDetailsRESPONSE
                            {
                                success = new Digichamps_web_Api.chapterlist
                                {
                                    Chapter_Name = Chapobj.Chapter,
                                    Chapterid = eid,
                                    Today_date = today,
                                    ChapterModules = (from d in DbContext.SP_DC_GetModules_for_api(eid, id)
                                                      select new Digichamps_web_Api.ChapterModuleList
                                                      {

                                                          Module_Id = d.Module_ID,
                                                          Module_Title = d.Module_Name == null ? "" : d.Module_Name,
                                                          Module_Image = d.Module_Image == null ? "" : d.Module_Image,
                                                          Description = d.Module_Desc == null ? "" : d.Module_Desc,
                                                          pdf_file = d.Module_Content == null ? "" : d.Module_Content,
                                                          pdf_name = d.Module_Content_Name == null ? d.Module_Name : d.Module_Content_Name,
                                                          Image_Key = d.Image_Key == null ? "" : d.Image_Key,
                                                          Is_Free = d.Is_Free,
                                                          Validity = d.Validity,
                                                          Question_Pdf = d.Question_PDF == null ? "" : d.Question_PDF,
                                                          Question_Pdf_Name = d.Question_PDF_Name == null ? d.Module_Name : d.Question_PDF_Name,
                                                          No_Of_Question = d.No_Of_Question == null ? 0 : d.No_Of_Question,
                                                          Is_Free_Test = true, //d.Is_Free_Test == null ? false : d.Is_Free_Test,
                                                          Media_Id = d.Module_video == null ? "" : (d.Video_Type == "Y" ? ( d.Module_video) : d.Module_video),
                                                          VideoKey = d.Module_video == null ? "" : (d.Video_Type == "Y" ? (d.Module_video) : d.Module_video),
                                                          
                                                        //  Media_Id = d.Module_video,// d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                                                          //template_id = "-ozl7iD1S",
                                                          template_id = "",
                                                        //  VideoKey = d.Module_video,//d.Module_video d.Is_Free == true ? (d.Validity > today ? d.Module_video : "") : "",
                                                          Is_Expire = false,//d.Is_Free == true ? (d.Validity > today ? false : true) : true,
                                                          //thumbnail_key = "-320",
                                                          thumbnail_key = "",
                                                          Is_Avail = false,
                                                          Video_Type = d.Video_Type,
                                                          //Class_PPT = d.Class_PPT != null ? ("/Module/PPT/" + d.Class_PPT) : "",
                                                          //Class_PPT_Name = d.Class_PPT_Name,
                                                          //NCERT_PDF = d.NCERT_PDF != null ? ("/Module/NCERT/" + d.NCERT_PDF) : "",
                                                          //NCERT_PDF_Name = d.NCERT_PDF_Name,
                                                          is_Video_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 2),
                                                          is_question_bookmarked = FindModuleasBookmarked(d.Module_ID, id, 1),
                                                          is_studynotebookmarked = FindModuleasBookmarked(d.Module_ID, id, 3),
                                                      }).ToList(),
                                    Quesbank = (listdata == null) ? (from c in v select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = 0, Question_Pdfs = "", Modulename = "" }).ToList() : (from c in listdata.Where(x => x.Question_Pdfs != null) select new DigiChamps.Models.Digichamps_web_Api.Questionbanks { noofques = c.noofques, Question_Pdfs = c.Question_Pdfs, Modulename = c.Modulename, is_question_bookmarked = FindModuleasBookmarked(c.moduleIDss, id, 1), moduleIDss = c.moduleIDss }).ToList(),
                                    ppt = (from f in v.Where(x => x.Class_PPT != null).ToList()
                                           select new DigiChamps.Models.Digichamps_web_Api.pptlist
                                           {
                                               Class_PPT = f.Class_PPT != null ? ("/Module/PPT/" + f.Class_PPT) : "",
                                               Class_PPT_Name = f.Class_PPT_Name,
                                               ModuleId = f.Module_ID
                                           }).ToList(),
                                    ncert = (from f in v.Where(x => x.NCERT_PDF != null).ToList()
                                             select new DigiChamps.Models.Digichamps_web_Api.Ncertlist
                                             {
                                                 NCERT_PDF = f.NCERT_PDF != null ? ("/Module/NCERT/" + f.NCERT_PDF) : "",
                                                 NCERT_PDF_Name = f.NCERT_PDF_Name,
                                                 ModuleId = f.Module_ID
                                             }).ToList(),
                                    pdfs = (from c in v.Where(x => x.Module_Content != null)
                                            select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                            {
                                                moduleID = c.Module_ID,
                                                Url = c.Module_Content,
                                                Modulename = c.Module_Name,
                                                is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),

                                            }).ToList() == null ? (from c in v
                                                                   select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                   {
                                                                       Url = "",
                                                                       Modulename = ""
                                                                   }).ToList() : (from c in v.Where(x => x.Module_Content != null)
                                                                                  select new DigiChamps.Models.Digichamps_web_Api.Pdf_url
                                                                                  {
                                                                                      moduleID = c.Module_ID,
                                                                                      Url = c.Module_Content,
                                                                                      Modulename = c.Module_Name,
                                                                                      is_studynotebookmarked = FindModuleasBookmarked(c.Module_ID, id, 3),
                                                                                  }).ToList()

                                }
                            };
                            // DateTime diff = dt.AddDays(45);

                            //DateTime diff = new DateTime(2019, 4, 1);

                            //if (DateTime.Today.Date < diff.Date)
                            //{

                            //    for (int i = 0; i < resultobj.success.ChapterModules.Count; i++)
                            //    {
                            //        resultobj.success.ChapterModules[i].Is_Expire = false;
                            //    }


                            //}
                            return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                        }
                    }
                    else
                    {
                        var errobj = new Digichamps_web_Api.Errorresult
                        {
                            Error = new Digichamps_web_Api.Errorresponse
                            {
                                Message = "Invalid Chapter details.",
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, errobj);
                    }
                }
                else
                {
                    var errobj = new Digichamps_web_Api.Errorresult
                    {
                        Error = new Digichamps_web_Api.Errorresponse
                        {
                            Message = "Invalid user details.",
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public string validays(DateTime? insertdate, int? validity)
        {


            string lastdate = Convert.ToString(Convert.ToDateTime(insertdate).AddDays(Convert.ToDouble(validity)));

            return lastdate;

        }
        [HttpGet]
        public HttpResponseMessage LearnVideodetails(int id, int eid) //Student id and Module id
        {
            try
            {
                var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == id && x.IS_ACCEPTED == true).FirstOrDefault();
                if (stuobj != null)
                {
                    var Modobj = DbContext.tbl_DC_Module.Where(x => x.Module_ID == eid && x.Is_Active == true).FirstOrDefault();
                    if (Modobj != null)
                    {
                        var sentobj = new Digichamps_web_Api.VideoDetailsRESPONSE
                        {
                            success = new Digichamps_web_Api.videolist
                            {
                                ModuleName = Modobj.Module_Name,
                                //template_id = "-ozl7iD1S",
                                template_id = "",
                                //thumbnail_key = "-320",
                                thumbnail_key = "",
                             //   media_Id = Modobj.Module_video,
                                 media_Id = Modobj.Module_video == null ? "" : (Modobj.Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + Modobj.Module_video) : Modobj.Module_video)
                                                    //       VideoKey = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                          
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, sentobj);
                    }
                    else
                    {
                        var errobj = new Digichamps_web_Api.Errorresult
                        {
                            Error = new Digichamps_web_Api.Errorresponse
                            {
                                Message = "Invalid Video details.",
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, errobj);
                    }
                }
                else
                {
                    var errobj = new Digichamps_web_Api.Errorresult
                    {
                        Error = new Digichamps_web_Api.Errorresponse
                        {
                            Message = "Invalid user details.",
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetLearnPackage(int? id)
        {
            try
            {
                var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == id && x.IS_ACCEPTED == true).FirstOrDefault();


                if (stuobj != null)
                {
                    int classid = Convert.ToInt16(stuobj.Class_ID);
                    var subjects = (from d in DbContext.VW_DC_Package_Learn.Where(x => x.Regd_ID == id && x.Class_ID == classid).GroupBy(x => x.Subject_ID)
                                    select new PackagePreviewModel
                                    {
                                        Subject_Id = d.FirstOrDefault().Subject_ID,
                                        Subject = d.FirstOrDefault().Subject_Name,
                                        Total_Chapter = d.FirstOrDefault().Total_Chapter
                                    }).ToList();
                    if (subjects.Count > 0)
                    {
                        var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                Subjectlists = (from c in subjects
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c.Subject_Id,
                                                    subject = c.Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    Total_Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == c.Subject_Id && x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count(),
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.Question_PDF).Count(),
                                                    Total_question = (int)DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.No_Of_Question != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.No_Of_Question).Sum(),
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                    //Total_DIYVieos = DbContext.tbl_DC_DIY_Video.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Delete == false).ToList().Count,
                                                }).ToList()
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj1);
                    }
                    else
                    {
                        var freesubs = DbContext.Sp_DC_Getall_Packagelearn(classid).Select(x => x.Subject_Id).Distinct().ToList();
                        var resultobj = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                Subjectlists = (from c in freesubs
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c,
                                                    subject = DbContext.tbl_DC_Subject.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault().Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Question_PDF != null).ToList().Select(x => x.Question_PDF).Count(),
                                                    //Total_DIYVieos = DbContext.tbl_DC_DIY_Video.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Delete == false).ToList().Count,
                                                }).ToList()
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj);
                    }
                }
                else
                {
                    var errobj = new Digichamps_web_Api.Errorresult
                    {
                        Error = new Digichamps_web_Api.Errorresponse
                        {
                            Message = "Invalid user details.",
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, errobj);
                }

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        #region DashboardModel
        public class Banner : Digichamps_web_Api.learnresultRESPONSE
        {
            public int? banner_Id { get; set; }
            public string Image_URL { get; set; }
            public string Image_Title { get; set; }
            public string Banner_Description { get; set; }
        }

        public class Banner_List
        {
            public List<Banner> list { get; set; }
        }
        public class success_Banner
        {
            public Banner_List Success { get; set; }
        }
        public class Errorresult
        {
            public Errorresponse Error { get; set; }
        }
        public class Errorresponse
        {
            public string Message { get; set; }
        }

        public class StudentDetails
        {
            public int RegID { get; set; }
            public int PKGID { get; set; }
            public Guid? schoolid { get; set; }
            public int? Class_ID { get; set; }
        }
        #endregion

        [HttpPost]
        public HttpResponseMessage GetDashboard([FromBody]StudentDetails student)
        {
            //var OrderList = DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID).Select(x => x.Order_ID).ToList();
            //var orderpackgList = DbContext.tbl_DC_Order_Pkg.Where(x => 
            var Pakid = (from a in DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID)
                         join b in DbContext.tbl_DC_Order_Pkg
                         on a.Order_ID equals b.Order_ID
                         select new PackageIdInfo
                         {
                             PackageID = b.Package_ID
                         });
            List<PackageIdInfo> pd = new List<PackageIdInfo>();
            pd = Pakid.ToList<PackageIdInfo>();

            HashSet<int?> list = new HashSet<int?>();
            foreach (PackageIdInfo item in pd)
            {

                list.Add(item.PackageID);
            }


            // List<PackagetDetails> pl=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList();

            bool? Is_School = false; bool? Is_Doubt = false; bool? Is_Mentor = false; bool? Is_Dictionary = false; bool? Is_Feed = false;

            List<tbl_DC_Package> pl = DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false && list.Contains(x.Package_ID)).ToList();


            bool? Is_Video = false;
            foreach (tbl_DC_Package item in pl)
            {
                if (item.Is_Doubt.Equals(true))
                {
                    Is_Doubt = true;
                }
                if (item.Is_School.Equals(true))
                {
                    Is_School = true;
                }
                if (item.Is_Mentor.Equals(true))
                {
                    Is_Mentor = true;
                }
                if (item.Is_Dictionary.Equals(true))
                {
                    Is_Dictionary = true;
                }
                if (item.Is_Feed.Equals(true))
                {
                    Is_Feed = true;
                }
                if (item.Is_Video.Equals(true))
                {
                    Is_Video = true;
                }
            }


            var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == student.RegID && x.IS_ACCEPTED == true).FirstOrDefault();
            try
            {


                if (stuobj != null)
                {
                    int classid = Convert.ToInt16(stuobj.Class_ID);
                    var subjects = (from d in DbContext.VW_DC_Package_Learn.Where(x => x.Regd_ID == student.RegID && x.Class_ID == classid).GroupBy(x => x.Subject_ID)
                                    select new PackagePreviewModel
                                    {
                                        Subject_Id = d.FirstOrDefault().Subject_ID,
                                        Subject = d.FirstOrDefault().Subject_Name,
                                        Total_Chapter = d.FirstOrDefault().Total_Chapter
                                    }).ToList();
                    if (subjects.Count > 0)
                    {
                        var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                //PackageDetailsList=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList(),
                                // PackageDetailsList =list.ToList(),
                                //DbContext.tbl_DC_Package_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false  && list.Contains(x.Package_ID)).ToList(),

                                Mentorid = DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == student.RegID).Select(x => x.Teacher_ID).FirstOrDefault(),
                                PreviousRating = DbContext.tbl_DC_Feedback.Where(x => x.Regd_Id == student.RegID).Select(x => x.Rating).FirstOrDefault(),

                                Boardid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Board_ID).FirstOrDefault(),
                                classid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Class_ID).FirstOrDefault(),
                                schoolid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SchoolId).FirstOrDefault(),
                                asignteacherid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.TeacherId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault() == null ? null : DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault().SectionID,
                                sectionid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault(),
                                roleid = DbContext.tbl_DC_Discussion.Where(x => x.IsActive == true && x.IsDelete == false).Select(x => x.RoleID).FirstOrDefault(),
                                isSectionEnabled = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionClass).FirstOrDefault(),

                                //SectionList = DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == student.schoolid && x.IsActive == true && 
                                //    x.Class_Id == student.Class_ID).ToList(),

                                //StudentPackageDetails = DbContext.Get_Student_Package_Details_For_DashBoard.Where(x => x.Regd_ID == student.RegID).Select(x => x.Package_ID).FirstOrDefault(),
                                Is_School = Is_School,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_School).FirstOrDefault(),
                                Is_Doubt = Is_Doubt,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Doubt).FirstOrDefault(),
                                Is_Mentor = Is_Mentor,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Mentor).FirstOrDefault(),
                                Is_Dictionary = Is_Dictionary,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Dictionary).FirstOrDefault(),
                                Is_Feed = Is_Feed,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Feed).FirstOrDefault(),
                                Is_Video = Is_Video,
                                Subjectlists = (from c in subjects
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c.Subject_Id,
                                                    subject = c.Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    Total_Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == c.Subject_Id && x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count(),
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.Question_PDF).Count(),
                                                    Total_question = (int)DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.No_Of_Question != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.No_Of_Question).Sum(),
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                }).ToList(),
                                BannerList = (from c in DbContext.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                              select new Digichamps_web_Api.BannerModel
                                              {
                                                  banner_Id = c.banner_Id,
                                                  Image_URL = c.Image_URL,
                                                  BannerOnline = "https://learn.odmps.org/Images/Banner/" + c.Image_URL + "",
                                                  BannerBeta = "http://beta.thedigichamps.com/Images/Banner/" + c.Image_URL + "",
                                                  Image_Title = c.Image_Title,
                                                  //Banner_Description = c.Banner_Description,
                                              }).OrderByDescending(c => c.banner_Id).ToList(),
                                DiyModuleLists = (from c in DbContext.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                                  select new Digichamps_web_Api.DiyModuleList
                                                  {
                                                      DIYVideo_ID = c.DIYVideo_ID,
                                                      DIYVideo_Name = c.DIYVideo_Name,
                                                      DIYVideo_Upload = c.DIYVideo_Upload,
                                                      DIYImages = c.DIYImages,
                                                      DiyPosterImage_production = c.DIYImages,
                                                      DiyPosterImage_beta = c.DIYImages,
                                                      DIYVideo_Description = c.DIYVideo_Description
                                                  }).OrderByDescending(c => c.DIYVideo_ID).Take(5).ToList(),
                                Recentwatchedvideos = (from c in DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == student.RegID).ToList()
                                                       select new
                                                           Digichamps_web_Api.Recentvideos
                                                       {
                                                           Module_Id = c.Module_ID,
                                                           Module_Title = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Module_Name = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Image_Key = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Module_video = GetmoduleDetails(c.Module_ID).Module_video,
                                                           Chapter_Id = GetmoduleDetails(c.Module_ID).Chapter_Id,
                                                           Is_Expire = GetmoduleDetails(c.Module_ID) == null ? true : false,
                                                           Is_Avail = true,
                                                           Is_Free = GetmoduleDetails(c.Module_ID).Is_Free,
                                                           Validity = GetmoduleDetails(c.Module_ID).Validity,
                                                           Is_Free_Test = GetmoduleDetails(c.Module_ID).Is_Free_Test,
                                                           Media_Id = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                           VideoKey = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                          
                                                           thumbnail_key = "",
                                                           //template_id = "-ozl7iD1S",
                                                           template_id = "",
                                                           Module_Image = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Description = GetmoduleDetails(c.Module_ID).Module_Desc,
                                                       }).Take(5).ToList()

                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj1);

                    }
                    else
                    {
                        var freesubs = DbContext.Sp_DC_Getall_Packagelearn(classid).Select(x => x.Subject_Id).Distinct().ToList();
                        var resultobj = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                Mentorid = DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == student.RegID).Select(x => x.Teacher_ID).FirstOrDefault(),
                                PreviousRating = DbContext.tbl_DC_Feedback.Where(x => x.Regd_Id == student.RegID).Select(x => x.Rating).FirstOrDefault(),
                                Boardid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Board_ID).FirstOrDefault(),
                                classid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Class_ID).FirstOrDefault(),
                                schoolid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SchoolId).FirstOrDefault(),

                                asignteacherid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.TeacherId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault() == null ? null : DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault().SectionID,
                                sectionid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.SectionId).FirstOrDefault(),
                                roleid = DbContext.tbl_DC_Discussion.Where(x => x.IsActive == true && x.IsDelete == false).Select(x => x.RoleID).FirstOrDefault(),
                                isSectionEnabled = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionClass).FirstOrDefault(),
                                //SectionList = DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == student.schoolid && x.IsActive == true &&
                                //  x.Class_Id == student.Class_ID).ToList(),

                                Is_Video = Is_Video,
                                Is_School = Is_School,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_School).FirstOrDefault(),
                                Is_Doubt = Is_Doubt,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Doubt).FirstOrDefault(),
                                Is_Mentor = Is_Mentor,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Mentor).FirstOrDefault(),
                                Is_Dictionary = Is_Dictionary,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Dictionary).FirstOrDefault(),
                                Is_Feed = Is_Feed,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Feed).FirstOrDefault(),
                                //  PackageDetailsList = DbContext.tbl_DC_Package_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false && list.Contains(x.Package_ID)).ToList(),
                                Subjectlists = (from c in freesubs
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c,
                                                    subject = DbContext.tbl_DC_Subject.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault().Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Question_PDF != null).ToList().Select(x => x.Question_PDF).Count(),
                                                }).ToList(),
                                BannerList = (from c in DbContext.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                              select new Digichamps_web_Api.BannerModel
                                              {
                                                  banner_Id = c.banner_Id,
                                                  Image_URL = c.Image_URL,
                                                  Image_Title = c.Image_Title,
                                                  BannerOnline = "https://learn.odmps.org/Images/Banner/" + c.Image_URL + "",
                                                  BannerBeta = "http://beta.thedigichamps.com/Images/Banner/" + c.Image_URL + "",
                                                  //Banner_Description = c.Banner_Description,
                                              }).OrderByDescending(c => c.banner_Id).ToList(),
                                DiyModuleLists = (from c in DbContext.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                                  select new Digichamps_web_Api.DiyModuleList
                                                  {
                                                      DIYVideo_ID = c.DIYVideo_ID,
                                                      DIYVideo_Name = c.DIYVideo_Name,
                                                      DIYVideo_Upload = c.DIYVideo_Upload,
                                                      DIYImages = c.DIYImages,
                                                      DiyPosterImage_production = c.DIYImages,
                                                      DiyPosterImage_beta = c.DIYImages,
                                                      DIYVideo_Description = c.DIYVideo_Description
                                                  }).OrderByDescending(c => c.DIYVideo_ID).Take(5).ToList(),
                                Recentwatchedvideos = (from c in DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == student.RegID).ToList()
                                                       select new Digichamps_web_Api.Recentvideos
                                                       {
                                                           Module_Id = c.Module_ID,
                                                           Module_Title = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Module_Name = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Image_Key = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Module_video = GetmoduleDetails(c.Module_ID).Module_video,
                                                           Chapter_Id = GetmoduleDetails(c.Module_ID).Chapter_Id,
                                                           Is_Expire = GetmoduleDetails(c.Module_ID) == null ? true : false,
                                                           Is_Avail = false,
                                                           Is_Free = GetmoduleDetails(c.Module_ID).Is_Free,
                                                           Validity = GetmoduleDetails(c.Module_ID).Validity,
                                                           Is_Free_Test = GetmoduleDetails(c.Module_ID).Is_Free_Test,
                                                           Media_Id = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                           VideoKey = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                          
                                                           thumbnail_key = "",
                                                           //template_id = "-ozl7iD1S",
                                                           template_id = "",
                                                           Module_Image = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Description = GetmoduleDetails(c.Module_ID).Module_Desc,
                                                       }).Take(5).ToList()
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj);

                    }

                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "No user found."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }



            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }


        [HttpPost]
        public HttpResponseMessage GetDashboardLearn([FromBody]StudentDetails student)
        {
            //var OrderList = DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID).Select(x => x.Order_ID).ToList();
            //var orderpackgList = DbContext.tbl_DC_Order_Pkg.Where(x => 
            var Pakid = (from a in DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID)
                         join b in DbContext.tbl_DC_Order_Pkg
                         on a.Order_ID equals b.Order_ID
                         select new PackageIdInfo
                         {
                             PackageID = b.Package_ID,
                             PackageName = DbContext.tbl_DC_Package.Where(x => x.Package_ID == b.Package_ID).Select(x => x.Package_Name).FirstOrDefault()
                         });
            List<PackageIdInfo> pd = new List<PackageIdInfo>();
            pd = Pakid.ToList<PackageIdInfo>();

            HashSet<int?> list = new HashSet<int?>();
            foreach (PackageIdInfo item in pd)
            {

                list.Add(item.PackageID);
            }


            // List<PackagetDetails> pl=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList();

            bool? Is_School = false; bool? Is_Doubt = false; bool? Is_Mentor = false; bool? Is_Dictionary = false; bool? Is_Feed = false;

            List<tbl_DC_Package> pl = DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false &&
                list.Contains(x.Package_ID)).ToList();


            bool? Is_Video = false;

            foreach (tbl_DC_Package item in pl)
            {
                if (item.Is_Doubt.Equals(true))
                {
                    Is_Doubt = true;
                }
                if (item.Is_School.Equals(true))
                {
                    Is_School = true;
                }
                if (item.Is_Mentor.Equals(true))
                {
                    Is_Mentor = true;
                }
                if (item.Is_Dictionary.Equals(true))
                {
                    Is_Dictionary = true;
                }
                if (item.Is_Feed.Equals(true))
                {
                    Is_Feed = true;
                }
                if (item.Is_Video.Equals(true))
                {
                    Is_Video = true;
                }
            }


            var stuobj = DbContext.View_All_Student_Details.Where(x => x.Regd_ID == student.RegID && x.IS_ACCEPTED == true).FirstOrDefault();
            try
            {


                if (stuobj != null)
                {
                    int classid = Convert.ToInt16(stuobj.Class_ID);
                    var subjects = (from d in DbContext.VW_DC_Package_Learn.Where(x => x.Regd_ID == student.RegID && x.Class_ID == classid).GroupBy(x => x.Subject_ID)
                                    select new PackagePreviewModel
                                    {
                                        Subject_Id = d.FirstOrDefault().Subject_ID,
                                        Subject = d.FirstOrDefault().Subject_Name,
                                        Total_Chapter = d.FirstOrDefault().Total_Chapter
                                    }).ToList();
                    if (subjects.Count > 0)
                    {
                        var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                //PackageDetailsList=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList(),
                                // PackageDetailsList =list.ToList(),
                                //DbContext.tbl_DC_Package_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false  && list.Contains(x.Package_ID)).ToList(),

                                Mentorid = DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == student.RegID).Select(x => x.Teacher_ID).FirstOrDefault(),
                                PreviousRating = DbContext.tbl_DC_Feedback.Where(x => x.Regd_Id == student.RegID).Select(x => x.Rating).FirstOrDefault(),

                                Boardid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Board_ID).FirstOrDefault(),
                                classid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Class_ID).FirstOrDefault(),
                                schoolid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SchoolId).FirstOrDefault(),
                                asignteacherid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.TeacherId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault() == null ? null : DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault().SectionID,
                                sectionid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault(),
                                roleid = DbContext.tbl_DC_Discussion.Where(x => x.IsActive == true && x.IsDelete == false).Select(x => x.RoleID).FirstOrDefault(),
                                isSectionEnabled = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionClass).FirstOrDefault(),

                                //SectionList = DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == student.schoolid && x.IsActive == true && 
                                //    x.Class_Id == student.Class_ID).ToList(),

                                //StudentPackageDetails = DbContext.Get_Student_Package_Details_For_DashBoard.Where(x => x.Regd_ID == student.RegID).Select(x => x.Package_ID).FirstOrDefault(),
                                Is_School = Is_School,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_School).FirstOrDefault(),
                                Is_Doubt = Is_Doubt,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Doubt).FirstOrDefault(),
                                Is_Mentor = Is_Mentor,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Mentor).FirstOrDefault(),
                                Is_Dictionary = Is_Dictionary,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Dictionary).FirstOrDefault(),
                                Is_Feed = Is_Feed,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Feed).FirstOrDefault(),
                                Is_Video = Is_Video,
                                Subjectlists = (from c in subjects
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c.Subject_Id,
                                                    subject = c.Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    Total_Pre_req_test = DbContext.tbl_DC_Exam.Where(x => x.Subject_Id == c.Subject_Id && x.Exam_type == 1 && x.Is_Active == true && x.Is_Deleted == false).ToList().Count(),
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Question_PDF != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.Question_PDF).Count(),
                                                    Total_question = (int)DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.No_Of_Question != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Select(x => x.No_Of_Question).Sum(),
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c.Subject_Id && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                }).ToList(),
                                BannerList = (from c in DbContext.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                              select new Digichamps_web_Api.BannerModel
                                              {
                                                  banner_Id = c.banner_Id,
                                                  Image_URL = c.Image_URL,
                                                  BannerOnline = "https://learn.odmps.org/Images/Banner/" + c.Image_URL + "",
                                                  BannerBeta = "http://beta.thedigichamps.com/Images/Banner/" + c.Image_URL + "",
                                                  Image_Title = c.Image_Title,
                                                  //Banner_Description = c.Banner_Description,
                                              }).OrderByDescending(c => c.banner_Id).ToList(),
                                DiyModuleLists = (from c in DbContext.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                                  select new Digichamps_web_Api.DiyModuleList
                                                  {
                                                      DIYVideo_ID = c.DIYVideo_ID,
                                                      DIYVideo_Name = c.DIYVideo_Name,
                                                      DIYVideo_Upload = c.DIYVideo_Upload,
                                                      DIYImages = c.DIYImages,
                                                      DiyPosterImage_production = c.DIYImages,
                                                      DiyPosterImage_beta = c.DIYImages,
                                                      DIYVideo_Description = c.DIYVideo_Description
                                                  }).OrderByDescending(c => c.DIYVideo_ID).ToList(),
                                Recentwatchedvideos = (from c in DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == student.RegID).ToList()
                                                       select new
                                                           Digichamps_web_Api.Recentvideos
                                                       {
                                                           Module_Id = c.Module_ID,
                                                           Module_Title = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Module_Name = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Image_Key = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Module_video = GetmoduleDetails(c.Module_ID).Module_video,
                                                           Chapter_Id = GetmoduleDetails(c.Module_ID).Chapter_Id,
                                                           Is_Expire = GetmoduleDetails(c.Module_ID) == null ? true : false,
                                                           Is_Avail = true,
                                                           Is_Free = GetmoduleDetails(c.Module_ID).Is_Free,
                                                           Validity = GetmoduleDetails(c.Module_ID).Validity,
                                                           Is_Free_Test = GetmoduleDetails(c.Module_ID).Is_Free_Test,
                                                           Media_Id = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                           VideoKey = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                          
                                                           thumbnail_key = "",
                                                           //template_id = "-ozl7iD1S",
                                                           template_id = "",
                                                           Module_Image = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Description = GetmoduleDetails(c.Module_ID).Module_Desc,
                                                       }).Take(5).ToList()

                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj1);

                    }
                    else
                    {
                        var freesubs = DbContext.Sp_DC_Getall_Packagelearn(classid).Select(x => x.Subject_Id).ToList();
                        var resultobj = new Digichamps_web_Api.learnresultRESPONSE
                        {
                            success = new Digichamps_web_Api.Pkglearnmodel
                            {
                                Mentorid = DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == student.RegID).Select(x => x.Teacher_ID).FirstOrDefault(),
                                PreviousRating = DbContext.tbl_DC_Feedback.Where(x => x.Regd_Id == student.RegID).Select(x => x.Rating).FirstOrDefault(),
                                Boardid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Board_ID).FirstOrDefault(),
                                classid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Class_ID).FirstOrDefault(),
                                schoolid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SchoolId).FirstOrDefault(),

                                asignteacherid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.TeacherId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault() == null ? null : DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault().SectionID,
                                sectionid = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault(),
                                //sectionid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.SectionId).FirstOrDefault(),
                                roleid = DbContext.tbl_DC_Discussion.Where(x => x.IsActive == true && x.IsDelete == false).Select(x => x.RoleID).FirstOrDefault(),
                                isSectionEnabled = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionClass).FirstOrDefault(),
                                //SectionList = DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == student.schoolid && x.IsActive == true &&
                                //  x.Class_Id == student.Class_ID).ToList(),

                                Is_Video = Is_Video,
                                Is_School = Is_School,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_School).FirstOrDefault(),
                                Is_Doubt = Is_Doubt,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Doubt).FirstOrDefault(),
                                Is_Mentor = Is_Mentor,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Mentor).FirstOrDefault(),
                                Is_Dictionary = Is_Dictionary,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Dictionary).FirstOrDefault(),
                                Is_Feed = Is_Feed,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Feed).FirstOrDefault(),
                                //  PackageDetailsList = DbContext.tbl_DC_Package_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false && list.Contains(x.Package_ID)).ToList(),
                                Subjectlists = (from c in freesubs
                                                select new Digichamps_web_Api.pkgLearnSubjects
                                                {
                                                    subjectid = c,
                                                    subject = DbContext.tbl_DC_Subject.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault().Subject,
                                                    total_chapters = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Chapter_Id).Distinct().ToList().Count,
                                                    total_videos = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Is_Active == true && x.Is_Deleted == false && x.Module_video != null).Select(x => x.Module_ID).ToList().Count,
                                                    total_pdfs = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Module_Content != null && x.Is_Active == true && x.Is_Deleted == false).ToList().Count,
                                                    Total_question_pdf = DbContext.tbl_DC_Module.Where(x => x.Subject_Id == c && x.Question_PDF != null).ToList().Select(x => x.Question_PDF).Count(),
                                                }).ToList(),
                                BannerList = (from c in DbContext.tbl_DC_Banner.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                              select new Digichamps_web_Api.BannerModel
                                              {
                                                  banner_Id = c.banner_Id,
                                                  Image_URL = c.Image_URL,
                                                  Image_Title = c.Image_Title,
                                                  BannerOnline = "https://learn.odmps.org/Images/Banner/" + c.Image_URL + "",
                                                  BannerBeta = "http://beta.thedigichamps.com/Images/Banner/" + c.Image_URL + "",
                                                  //Banner_Description = c.Banner_Description,
                                              }).OrderByDescending(c => c.banner_Id).ToList(),
                                DiyModuleLists = (from c in DbContext.tbl_DC_DIY_Video.Where(x => x.Is_Active == true && x.Is_Delete == false)
                                                  select new Digichamps_web_Api.DiyModuleList
                                                  {
                                                      DIYVideo_ID = c.DIYVideo_ID,
                                                      DIYVideo_Name = c.DIYVideo_Name,
                                                      DIYVideo_Upload = c.DIYVideo_Upload,
                                                      DIYImages = c.DIYImages,
                                                      DiyPosterImage_production = c.DIYImages,
                                                      DiyPosterImage_beta = c.DIYImages,
                                                      DIYVideo_Description = c.DIYVideo_Description
                                                  }).OrderByDescending(c => c.DIYVideo_ID).ToList(),
                                Recentwatchedvideos = (from c in DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == student.RegID).ToList()
                                                       select new Digichamps_web_Api.Recentvideos
                                                       {
                                                           Module_Id = c.Module_ID,
                                                           Module_Title = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Module_Name = GetmoduleDetails(c.Module_ID).Module_Name,
                                                           Image_Key = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Module_video = GetmoduleDetails(c.Module_ID).Module_video,
                                                           Chapter_Id = GetmoduleDetails(c.Module_ID).Chapter_Id,
                                                           Is_Expire = GetmoduleDetails(c.Module_ID) == null ? true : false,
                                                           Is_Avail = false,
                                                           Is_Free = GetmoduleDetails(c.Module_ID).Is_Free,
                                                           Validity = GetmoduleDetails(c.Module_ID).Validity,
                                                           Is_Free_Test = GetmoduleDetails(c.Module_ID).Is_Free_Test,
                                                           Media_Id = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                           VideoKey = GetmoduleDetails(c.Module_ID).Module_video == null ? "" : (GetmoduleDetails(c.Module_ID).Video_Type == "Y" ? ("https://www.youtube.com/watch?v=" + GetmoduleDetails(c.Module_ID).Module_video) : GetmoduleDetails(c.Module_ID).Module_video),
                                                          
                                                           thumbnail_key = "",
                                                           //template_id = "-ozl7iD1S",
                                                           template_id = "",
                                                           Module_Image = GetmoduleDetails(c.Module_ID).Module_Image,
                                                           Description = GetmoduleDetails(c.Module_ID).Module_Desc,
                                                       }).Take(5).ToList()
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, resultobj);

                    }

                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "No user found."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }



            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }



        [HttpPost]
        public HttpResponseMessage GetDashboardNew([FromBody]StudentDetails student)
        {
            //var OrderList = DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID).Select(x => x.Order_ID).ToList();
            //var orderpackgList = DbContext.tbl_DC_Order_Pkg.Where(x => 
            var Pakid = (from a in DbContext.tbl_DC_Order.Where(x => x.Regd_ID == student.RegID)
                         join b in DbContext.tbl_DC_Order_Pkg
                         on a.Order_ID equals b.Order_ID
                         select new PackageIdInfo
                         {
                             PackageID = b.Package_ID,
                             PackageName = DbContext.tbl_DC_Package.Where(x => x.Package_ID == b.Package_ID).Select(x => x.Package_Name).FirstOrDefault(),
                             expire = b.Expiry_Date

                         });
            List<PackageIdInfo> pd = new List<PackageIdInfo>();
            pd = Pakid.ToList<PackageIdInfo>();

            HashSet<int?> list = new HashSet<int?>();
            PackageIdInfo info = null;
            foreach (PackageIdInfo item in pd)
            {

                list.Add(item.PackageID);
                info = item;
            }


            // List<PackagetDetails> pl=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList();

            bool? Is_School = false; bool? Is_Doubt = false; bool? Is_Mentor = false; bool? Is_Dictionary = false; bool? Is_Feed = false;

            List<tbl_DC_Package> pl = DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false && list.Contains(x.Package_ID)).ToList();


            bool? Is_Video = false;

            foreach (tbl_DC_Package item in pl)
            {

                if (item.Is_Doubt.Equals(true))
                {
                    Is_Doubt = true;
                }
                if (item.Is_School.Equals(true))
                {
                    Is_School = true;
                }
                if (item.Is_Mentor.Equals(true))
                {
                    Is_Mentor = true;
                }
                if (item.Is_Dictionary.Equals(true))
                {
                    Is_Dictionary = true;
                }
                if (item.Is_Feed.Equals(true))
                {
                    Is_Feed = true;
                }
                if (item.Is_Video.Equals(true))
                {
                    Is_Video = true;
                }
            }




            DateTime d = new DateTime(2019, 3, 31);


            int count = DbContext.tbl_DC_Ticket.Where(x => x.Student_ID == student.RegID && x.Is_Active == true && x.Is_Deleted == false
                ).ToList().Count();


            string schooldtl = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).
                Select(x => x.SchoolId).FirstOrDefault() == null ? null :
                DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SchoolId).FirstOrDefault().Value.ToString();
            string section = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault() == null ? null : DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionId).FirstOrDefault().Value.ToString();
            Guid? sec = null;
            Guid? sch = null;
            if (schooldtl != null)
            {
                sch = Guid.Parse(schooldtl);
            }
            if (section != null)
            {
                sec = Guid.Parse(section);
            }
            try
            {



                var resultobj1 = new Digichamps_web_Api.learnresultRESPONSE
                {
                    success = new Digichamps_web_Api.Pkglearnmodel
                    {
                        //PackageDetailsList=DbContext.tbl_DC_Package_Dtl.Where(x => list.Contains(Convert.ToInt32(x.Package_ID))).ToList(),
                        // PackageDetailsList =list.ToList(),
                        //DbContext.tbl_DC_Package_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false  && list.Contains(x.Package_ID)).ToList(),

                        pack = info,
                        Mentorid = DbContext.tbl_DC_MentorMaster.Where(x => x.Student_ID == student.RegID).Select(x => x.Teacher_ID).FirstOrDefault(),
                        PreviousRating = DbContext.tbl_DC_Feedback.Where(x => x.Regd_Id == student.RegID).Select(x => x.Rating).FirstOrDefault(),

                        Boardid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Board_ID).FirstOrDefault(),
                        classid = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == student.RegID).Select(x => x.Class_ID).FirstOrDefault(),
                        schoolid = sch,
                        asignteacherid = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.IsActive == true).Select(x => x.TeacherId).FirstOrDefault(),
                        //sectionid = DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault() == null ? null : DbContext.tbl_DC_SectionLog.Where(x => x.RegId == student.RegID && x.ClassId == classid).FirstOrDefault().SectionID,
                        sectionid = sec,
                        roleid = DbContext.tbl_DC_Discussion.Where(x => x.IsActive == true && x.IsDelete == false).Select(x => x.RoleID).FirstOrDefault(),
                        isSectionEnabled = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == student.RegID).Select(x => x.SectionClass).FirstOrDefault(),

                        //SectionList = DbContext.tbl_DC_Class_Section.Where(x => x.School_Id == student.schoolid && x.IsActive == true && 
                        //    x.Class_Id == student.Class_ID).ToList(),

                        //StudentPackageDetails = DbContext.Get_Student_Package_Details_For_DashBoard.Where(x => x.Regd_ID == student.RegID).Select(x => x.Package_ID).FirstOrDefault(),
                        Is_School = Is_School,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_School).FirstOrDefault(),
                        Is_Doubt = Is_Doubt,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Doubt).FirstOrDefault(),
                        Is_Mentor = Is_Mentor,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Mentor).FirstOrDefault(),
                        Is_Dictionary = Is_Dictionary,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Dictionary).FirstOrDefault(),
                        Is_Feed = Is_Feed,//DbContext.tbl_DC_Package.Where(x => x.Package_ID == student.PKGID).Select(x => x.Is_Feed).FirstOrDefault(),
                        Is_Video = Is_Video,


                        DoubtCount = count,
                        MentorVideoLink = "https://www.youtube.com/channel/UCzAIAxzYmwubv1CcPJej8pw",


                        SchoolCode = (schooldtl == null) ? null : (DbContext.StudentSchoolCodes.
                        Where(a => a.RegId == student.RegID &&
                            a.SchoolId == schooldtl && a.SectionId == section).ToList().Count > 0
                            ? (schooldtl == null ? null : DbContext.tbl_DC_School_Info.Where(a => a.SchoolId == sch).FirstOrDefault().SchoolThumbnail) : null),
                        resultCount = DbContext.tbl_DC_Mentor_Exam_Result.
                                  Where(x => x.Regd_ID == student.RegID && x.Is_Active == true
                                && x.Is_Deleted == false).ToList().Count(),
                        totalCoins = DbContext.tbl_DC_CoinEarn.
                         Where(x => x.RegdId == student.RegID && x.Active == true
                     ).ToList().Select(x => x.Coins).Sum(),

                    }
                };
                if (DateTime.Now.Date <= d.Date)
                {
                    resultobj1.success.trialDate = d;
                }
                return Request.CreateResponse(HttpStatusCode.OK, resultobj1);



            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        public tbl_DC_Module GetmoduleDetails(int? moduleid)
        {
            return DbContext.tbl_DC_Module.Where(x => x.Module_ID == moduleid).FirstOrDefault();
        }

        public class VideoLogData
        {
            public int moduleId { get; set; }

            public int redgId { get; set; }
        }

        public class SuccessResult
        {
            public Success Success { get; set; }
        }

        public class Success
        {
            public string message { get; set; }
        }

        public class VideoLog
        {
            public int moduleId { get; set; }
            public int redgId { get; set; }
        }
        [HttpPost]
        public HttpResponseMessage SetvideoLog()
        {

            var httpRequest = HttpContext.Current.Request;
            int moduleId = Convert.ToInt32(httpRequest.Form["moduleId"]);
            int redgId = Convert.ToInt32(httpRequest.Form["redgId"]);
            VideoLogData Videolog = new VideoLogData
            {
                moduleId = moduleId,

                redgId = redgId
            };
            if (moduleId == 0 || redgId == 0)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "0"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            try
            {
                var getVideoLog = DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == Videolog.redgId && x.Module_ID == Videolog.moduleId).FirstOrDefault();
                if (getVideoLog == null)
                {
                    List<tbl_DC_Video_Log_Status> list = DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == Videolog.redgId).ToList();
                    if (list.Count.Equals(5))
                    {
                        //int count = list.Count - 1;
                        // List<tbl_DC_Video_Log_Status> list2=new  List<tbl_DC_Video_Log_Status> ();
                        //for (int i = 0; i < count; i++)
                        //{
                        //    tbl_DC_Video_Log_Status tbl_DC_Video_Log_Status = new tbl_DC_Video_Log_Status();
                        //    tbl_DC_Video_Log_Status.Regd_ID = Videolog.redgId;
                        //    tbl_DC_Video_Log_Status.Module_ID = Videolog.moduleId;
                        //    tbl_DC_Video_Log_Status.Inserted_On = today.Date;
                        //    list2.Add(tbl_DC_Video_Log_Status);
                        //}



                        DbContext.tbl_DC_Video_Log_Status.Remove(list[0]);
                    }

                    tbl_DC_Video_Log_Status tbl_DC_Video_Log_Status = new tbl_DC_Video_Log_Status();
                    tbl_DC_Video_Log_Status.Regd_ID = Videolog.redgId;
                    tbl_DC_Video_Log_Status.Module_ID = Videolog.moduleId;
                    tbl_DC_Video_Log_Status.Inserted_On = today.Date;
                    DbContext.tbl_DC_Video_Log_Status.Add(tbl_DC_Video_Log_Status);
                    DbContext.SaveChanges();

                }
                else
                {
                    getVideoLog.Inserted_On = today.Date;
                    DbContext.SaveChanges();
                }
                var obj = new SuccessResult
                {
                    Success = new Success
                    {
                        message = "1"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "0"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);

            }
        }

        [HttpPost]
        public HttpResponseMessage SetvideoLogTwo(VideoLog videoLog)
        {

            // var httpRequest = HttpContext.Current.Request;
            int moduleId = videoLog.moduleId;//.ToInt32(httpRequest.Form["moduleId"]);
            int redgId = videoLog.redgId;// Convert.ToInt32(httpRequest.Form["redgId"]);
            VideoLogData Videolog = new VideoLogData
            {
                moduleId = moduleId,

                redgId = redgId
            };
            if (moduleId == 0 || redgId == 0)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "0"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            try
            {
                var getVideoLog = DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == Videolog.redgId && x.Module_ID == Videolog.moduleId).FirstOrDefault();
                if (getVideoLog == null)
                {
                    List<tbl_DC_Video_Log_Status> list = DbContext.tbl_DC_Video_Log_Status.Where(x => x.Regd_ID == Videolog.redgId).ToList();
                    if (list.Count.Equals(5))
                    {
                        //int count = list.Count - 1;
                        // List<tbl_DC_Video_Log_Status> list2=new  List<tbl_DC_Video_Log_Status> ();
                        //for (int i = 0; i < count; i++)
                        //{
                        //    tbl_DC_Video_Log_Status tbl_DC_Video_Log_Status = new tbl_DC_Video_Log_Status();
                        //    tbl_DC_Video_Log_Status.Regd_ID = Videolog.redgId;
                        //    tbl_DC_Video_Log_Status.Module_ID = Videolog.moduleId;
                        //    tbl_DC_Video_Log_Status.Inserted_On = today.Date;
                        //    list2.Add(tbl_DC_Video_Log_Status);
                        //}



                        DbContext.tbl_DC_Video_Log_Status.Remove(list[0]);
                    }

                    tbl_DC_Video_Log_Status tbl_DC_Video_Log_Status = new tbl_DC_Video_Log_Status();
                    tbl_DC_Video_Log_Status.Regd_ID = Videolog.redgId;
                    tbl_DC_Video_Log_Status.Module_ID = Videolog.moduleId;
                    tbl_DC_Video_Log_Status.Inserted_On = today.Date;
                    DbContext.tbl_DC_Video_Log_Status.Add(tbl_DC_Video_Log_Status);
                    DbContext.SaveChanges();

                }
                else
                {
                    getVideoLog.Inserted_On = today.Date;
                    DbContext.SaveChanges();
                }
                var obj = new SuccessResult
                {
                    Success = new Success
                    {
                        message = "1"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "0"
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);

            }
        }

        #region---------------------------------Bookmark------------------------------------
        public class BookmarkData
        {
            public int RegID { get; set; }
            public int DatatypeId { get; set; }
            public int DataId { get; set; }
        }

        public class SucessBookmarkVideoresult
        {
            public SuccessBookmarkVideo successbookmarkvideo { get; set; }
        }
        public class SuccessBookmarkVideo
        {
            public BookmarkListvideo SuccessBookmarkedVideo { get; set; }
        }

        public class SuccessBookmarkQuestion
        {
            public BookmarkListQuestion SuccessBookmarkedQuestion { get; set; }
        }

        public class SuccessBookmarkStudynote
        {
            public BookmarkListStudynote SuccessBookmarkedStudynote { get; set; }
        }
        public class BookmarkListvideo
        {
            public List<Bookmarkvideo> VideoList { get; set; }
            public List<BookmarkQuestion> QuestionList { get; set; }
            public List<BookmarkStudynote> StudynoteList { get; set; }
        }

        public class BookmarkListQuestion
        {

            public List<BookmarkQuestion> QuestionList { get; set; }

        }
        public class BookmarkListStudynote
        {
            public List<BookmarkStudynote> StudynoteList { get; set; }
        }



        public class Bookmarkvideo
        {
            public int? Module_Id { get; set; }
            public string ModuleName { get; set; }
            public string Module_Title { get; set; }
            public string Module_Image { get; set; }
            public string Description { get; set; }
            public string Video_Type { get; set; }
            public string Image_Key { get; set; }
            public string VideoKey { get; set; }
            public bool Is_Expire { get; set; }
            public bool? Is_Free { get; set; }
            public bool Is_Avail { get; set; }

        }
        public class BookmarkQuestion
        {
            public int? Module_Id { get; set; }
            public string ModuleName { get; set; }
            public string Module_Title { get; set; }
            public string Module_Image { get; set; }
            public string Description { get; set; }
            public string Question { get; set; }
            public bool Is_Expire { get; set; }
            public bool? Is_Free { get; set; }
            public bool Is_Avail { get; set; }
        }
        public class BookmarkStudynote
        {
            public int? Module_Id { get; set; }
            public string ModuleName { get; set; }
            public string Studynote { get; set; }
            public bool Is_Expire { get; set; }
            public bool? Is_Free { get; set; }
            public bool Is_Avail { get; set; }
        }


        public class setbookmark_cls
        {
            public string RegID { get; set; }
            public string DatatypeId { get; set; }
            public string DataId { get; set; }
        }

        [HttpPost]
        public HttpResponseMessage SetBookMark([FromBody]setbookmark_cls m)
        {
            try
            {

                //var httpRequest = HttpContext.Current.Request;
                int RegID1 = Convert.ToInt32(m.RegID);
                int DatatypeId1 = Convert.ToInt32(m.DatatypeId);
                int DataId1 = Convert.ToInt32(m.DataId);

                BookmarkData BookmarkData = new BookmarkData
                {
                    RegID = RegID1,
                    DatatypeId = DatatypeId1,
                    DataId = DataId1
                };
                var getregId = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == BookmarkData.RegID).FirstOrDefault();
                var moduleId = DbContext.tbl_DC_Module.Where(x => x.Module_ID == DataId1).FirstOrDefault();


                if (getregId != null && moduleId != null)
                {
                    var Is_BookMarkAvailable = DbContext.tbl_DC_BookMark.Where(x => x.RedgID == BookmarkData.RegID && x.DataID == BookmarkData.DataId && x.TypeID == BookmarkData.DatatypeId).FirstOrDefault();
                    if (Is_BookMarkAvailable == null)
                    {
                        tbl_DC_BookMark tbl_DC_BookMark = new tbl_DC_BookMark();
                        tbl_DC_BookMark.TypeID = BookmarkData.DatatypeId;
                        tbl_DC_BookMark.RedgID = BookmarkData.RegID;
                        tbl_DC_BookMark.DataID = BookmarkData.DataId;
                        tbl_DC_BookMark.Is_Active = true;
                        tbl_DC_BookMark.Is_Delete = false;
                        tbl_DC_BookMark.Inserted_On = today;
                        DbContext.tbl_DC_BookMark.Add(tbl_DC_BookMark);
                        DbContext.SaveChanges();
                        var obj = new SuccessResult
                        {
                            Success = new Success
                            {
                                message = "Bookmarked successfully."
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {

                        DbContext.tbl_DC_BookMark.Remove(Is_BookMarkAvailable);
                        DbContext.SaveChanges();
                        var obj = new SuccessResult
                        {
                            Success = new Success
                            {
                                message = "Book mark deleted successfully."
                            }
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, obj);
                    }

                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "Please provide valid data."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }

            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }
        }

        public class BookmarkPost
        {
            public int RegID { get; set; }
        }
        [HttpGet]
        public HttpResponseMessage GetBookmarks(int? id)
        {
            try
            {


                var obj = new SuccessBookmarkVideo
                {
                    SuccessBookmarkedVideo = new BookmarkListvideo
                    {
                        VideoList = (from video in DbContext.tbl_DC_BookMark.Where(x => x.RedgID == id && x.TypeID == 2).ToList()
                                     select new Bookmarkvideo
                                     {
                                         Module_Id = video.DataID,
                                         ModuleName = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                         Module_Title = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                         Module_Image = GetmoduleDataForBookmark(video.DataID).Module_Image,
                                         Description = GetmoduleDataForBookmark(video.DataID).Module_Desc,
                                         Image_Key = GetmoduleDataForBookmark(video.DataID).Module_Image,
                                         VideoKey = GetmoduleDataForBookmark(video.DataID).Module_video,
                                         Is_Expire = GetmoduleDataForBookmark(video.DataID).Module_video == null ? true : false,
                                         Is_Free = GetmoduleDataForBookmark(video.DataID).Is_Free,
                                         Is_Avail = true,
                                     }).ToList(),
                        QuestionList = (from video in DbContext.tbl_DC_BookMark.Where(x => x.RedgID == id && x.TypeID == 1).ToList()
                                        select new BookmarkQuestion
                                        {
                                            Module_Id = video.DataID,
                                            ModuleName = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                            Module_Title = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                            Module_Image = GetmoduleDataForBookmark(video.DataID).Module_Image,
                                            Description = GetmoduleDataForBookmark(video.DataID).Module_Desc,
                                            Question = "https://learn.odmps.org/Module/Question_PDF/" + GetmoduleDataForBookmark(video.DataID).Question_PDF + "",
                                            Is_Expire = GetmoduleDataForBookmark(video.DataID).Module_video == null ? true : false,
                                            Is_Free = GetmoduleDataForBookmark(video.DataID).Is_Free,
                                            Is_Avail = true,
                                        }).ToList(),
                        StudynoteList = (from video in DbContext.tbl_DC_BookMark.Where(x => x.RedgID == id && x.TypeID == 3).ToList()
                                         select new BookmarkStudynote
                                         {
                                             Module_Id = video.DataID,
                                             ModuleName = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                             //Studynote = GetmoduleDataForBookmark(video.DataID).Module_Content
                                             Studynote = "https://learn.odmps.org/Module/PDF/" + GetmoduleDataForBookmark(video.DataID).Module_Content + "",
                                             Is_Expire = GetmoduleDataForBookmark(video.DataID).Module_video == null ? true : false,
                                             Is_Free = GetmoduleDataForBookmark(video.DataID).Is_Free,
                                             Is_Avail = true,
                                         }).ToList()




                    },

                };




                return Request.CreateResponse(HttpStatusCode.OK, obj);



            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }

        [HttpGet]
        public HttpResponseMessage GetBookmarkListVideo(int? id)
        {
            try
            {
                var RegDtl = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == id).FirstOrDefault();

                int? cld = RegDtl.Class_ID;
                var bookmarks = DbContext.tbl_DC_BookMark.SqlQuery(@"select * from [odm_lms].[testuid].[tbl_DC_BookMark]").ToList();
                var modules = DbContext.tbl_DC_Module.SqlQuery(@"select * from [odm_lms].[dbo].[tbl_DC_Module]").ToList();
                var videolist =
                (from video in bookmarks.Where(x => x.RedgID == id && x.TypeID == 2)
                 join b in modules.Where(x => x.Class_Id == cld)
                  on video.DataID equals b.Module_ID
                 select new Bookmarkvideo
                 {
                     Module_Id = video.DataID,
                     ModuleName = b.Module_Name,
                     Module_Title = b.Module_Name,
                     Module_Image = b.Module_Image,
                     Description = b.Module_Desc,
                     Image_Key = b.Module_Image,
                     VideoKey = b.Module_video,
                     Is_Expire = b.Module_video == null ? true : false,
                     Is_Free = b.Is_Free,
                     Is_Avail = true,
                     Video_Type = b.Video_Type
                 }).ToList();
                if (videolist.Count > 0)
                {

                    //var Videolist = getBookmarkForStudent.Where(x => x.TypeID == 2).ToList();


                    var obj = new SuccessBookmarkVideo
                    {
                        SuccessBookmarkedVideo = new BookmarkListvideo
                        {
                            VideoList = videolist




                        },

                    };




                    return Request.CreateResponse(HttpStatusCode.OK, obj);

                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "You have not added any bookmark yet."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }

        [HttpGet]
        public HttpResponseMessage GetBookmarkListQusetion(int? id)
        {
            try
            {

                var RegDtl = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == id).FirstOrDefault();
                int? cld = RegDtl.Class_ID;
                var bookmarks = DbContext.tbl_DC_BookMark.SqlQuery(@"select * from [odm_lms].[testuid].[tbl_DC_BookMark]").ToList();
                var modules = DbContext.tbl_DC_Module.SqlQuery(@"select * from [odm_lms].[dbo].[tbl_DC_Module]").ToList();
                var videolist =
                (from video in bookmarks.Where(x => x.RedgID == id && x.TypeID == 1)
                 join b in modules.Where(x => x.Class_Id == cld)
                  on video.DataID equals b.Module_ID
                 select new BookmarkQuestion
                                         {
                                             Module_Id = video.DataID,
                                             ModuleName = b.Module_Name,
                                             Module_Title = b.Module_Name,
                                             Module_Image = b.Module_Image,
                                             Description = b.Module_Desc,
                                             Question = "https://learn.odmps.org/Module/Question_PDF/" + b.Question_PDF + "",
                                             Is_Expire = b.Module_video == null ? true : false,
                                             Is_Free = b.Is_Free,
                                             Is_Avail = true,
                                         }).ToList();
                if (videolist.Count > 0)
                {
                    //var Questionlist = getBookmarkForStudent.Where(x => x.TypeID == 1).ToList();
                    var obj = new SuccessBookmarkQuestion
                    {
                        SuccessBookmarkedQuestion = new BookmarkListQuestion
                        {

                            QuestionList = videolist
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "You have not added  any bookmark yet."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }


        [HttpGet]
        public HttpResponseMessage GetBookmarkListStudynote(int? id)
        {
            try
            {
                var RegDtl = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == id).FirstOrDefault();
                int? cld = RegDtl.Class_ID;
                var bookmarks = DbContext.tbl_DC_BookMark.SqlQuery(@"select * from [odm_lms].[testuid].[tbl_DC_BookMark]").ToList();
                var modules = DbContext.tbl_DC_Module.SqlQuery(@"select * from [odm_lms].[dbo].[tbl_DC_Module]").ToList();
                var videolist =
                (from video in bookmarks.Where(x => x.RedgID == id && x.TypeID == 3)
                 join b in modules.Where(x => x.Class_Id == cld)
                  on video.DataID equals b.Module_ID
                 select new BookmarkStudynote
                                           {
                                               Module_Id = video.DataID,
                                               ModuleName = b.Module_Name,
                                               //Studynote = GetmoduleDataForBookmark(video.DataID).Module_Content
                                               Studynote = "https://learn.odmps.org/Module/PDF/" + b.Module_Content + "",
                                               Is_Expire = b.Module_video == null ? true : false,
                                               Is_Free = b.Is_Free,
                                               Is_Avail = true,
                                           }).ToList();


                if (videolist.Count > 0)
                {

                    var obj = new SuccessBookmarkStudynote
                    {
                        SuccessBookmarkedStudynote = new BookmarkListStudynote
                        {

                            StudynoteList = videolist

                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "You have not added  any bookmark yet."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.OK, obj);
            }

        }

        [HttpGet]
        public HttpResponseMessage GetbookamrkedvideoList(int id, int eid)
        {
            try
            {
                var Videolist = DbContext.tbl_DC_BookMark.Where(x => x.RedgID == id && x.TypeID == 2 && x.DataID != eid).ToList();
                if (Videolist.Count > 0)
                {
                    var obj = new SuccessBookmarkVideo
                    {
                        SuccessBookmarkedVideo = new BookmarkListvideo
                        {
                            VideoList = (from video in Videolist
                                         select new Bookmarkvideo
                                         {
                                             Module_Id = video.DataID,
                                             ModuleName = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                             Module_Title = GetmoduleDataForBookmark(video.DataID).Module_Name,
                                             Module_Image = GetmoduleDataForBookmark(video.DataID).Module_Image,
                                             Description = GetmoduleDataForBookmark(video.DataID).Module_Desc,
                                             Image_Key = GetmoduleDataForBookmark(video.DataID).Module_Image,
                                             VideoKey = GetmoduleDataForBookmark(video.DataID).Module_video,
                                             Is_Expire = GetmoduleDataForBookmark(video.DataID).Module_video == null ? true : false,
                                             Is_Free = GetmoduleDataForBookmark(video.DataID).Is_Free,
                                             Is_Avail = true,
                                         }).ToList()
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
                else
                {
                    var obj = new Errorresult
                    {
                        Error = new Errorresponse
                        {
                            Message = "You have added any bookmark yet."
                        }
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception)
            {

                var obj = new Errorresult
                {
                    Error = new Errorresponse
                    {
                        Message = "Something went wrong."
                    }
                };
                return Request.CreateResponse(HttpStatusCode.InternalServerError, obj);
            }
        }
        public tbl_DC_Module GetmoduleDataForBookmark(int? moduleid)
        {
            return DbContext.tbl_DC_Module.Where(x => x.Module_ID == moduleid).FirstOrDefault();
        }

        public bool FindModuleasBookmarked(int moduleid, int regid, int dataid)
        {
            var Is_Bookmarked = DbContext.tbl_DC_BookMark.Where(x => x.DataID == moduleid && x.RedgID == regid && x.TypeID == dataid).FirstOrDefault();
            if (Is_Bookmarked == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        #endregion






        [HttpGet]
        public HttpResponseMessage AssignSchoolZone(int regId, Guid? schoolId, String scholCode)
        {

            tbl_DC_Registration reg = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regId && x.SchoolId == schoolId
                && x.Is_Active == true && x.Is_Deleted == false)
                .FirstOrDefault();

            if (reg == null || reg.SchoolId == null || reg.SectionId == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, "SchoolId or SectionId null");
            }


            tbl_DC_School_Info info = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == schoolId && x.IsActive == true
                && x.SchoolThumbnail == scholCode)
                .FirstOrDefault();


            if (info == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotImplemented, "SchoolCode Wrong");
            }



            StudentSchoolCode code = DbContext.StudentSchoolCodes.Where(x => x.RegId == regId && x.Active == true)
                .FirstOrDefault();

            if (code == null)
            {




                code = new StudentSchoolCode();
                code.RegId = regId;
                code.Active = true;
                code.InsertedOn = DateTime.Now;

                DbContext.StudentSchoolCodes.Add(code);
                DbContext.SaveChanges();
            }




            return Request.CreateResponse(HttpStatusCode.Created, "SchoolZone Assigned");


        }



        [HttpGet]
        public HttpResponseMessage Addschoolzone(int regId, Guid? schoolId, String schoolCode)
        {


            tbl_DC_Registration mob = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regId && x.SchoolId == schoolId
                && x.Is_Active == true && x.Is_Deleted == false)
                .FirstOrDefault();

            if (mob == null || mob.SchoolId == null || mob.SectionId == null)
            {
                return Request.CreateResponse(HttpStatusCode.NoContent, "SchoolId or SectionId null");
            }

            Guid? s = schoolId;
            Guid? se = mob.SectionId;

            tbl_DC_School_Info info = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == schoolId && x.IsActive == true
                && x.SchoolThumbnail == schoolCode)
                .FirstOrDefault();


            if (info == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotImplemented, "SchoolCode Wrong");
            }



            StudentSchoolCode code = DbContext.StudentSchoolCodes.Where(x => x.RegId == regId && x.Active == true)
                .FirstOrDefault();

            if (code == null)
            {



                tbl_DC_Registration_Dtl dtl2 = DbContext.tbl_DC_Registration_Dtl.Where(x => x.Regd_ID == regId
                 && x.Is_Active == true && x.Is_Deleted == false)
                 .FirstOrDefault();

                int p = 0;


                code = new StudentSchoolCode();
                code.RegId = regId;
                code.Active = true;
                code.ClassId = dtl2.Class_ID;
                code.SchoolId = Convert.ToString(s);
                code.SectionId = Convert.ToString(se);
                code.InsertedOn = DateTime.Now;

                DbContext.StudentSchoolCodes.Add(code);
                DbContext.SaveChanges();



                if (dtl2.Board_ID == 1)
                {

                    switch (dtl2.Class_ID)
                    {
                        case 1021:

                            p = 147;//"SCHOOL PACKAGE CBSE X";
                            break;
                        case 1022: p = 148;//= "SCHOOL PACKAGE CBSE IX";
                            break;
                        case 1023: p = 150;//= "SCHOOL PACKAGE CBSE VIII";
                            break;
                        case 1024: p = 152;// "SCHOOL PACKAGE CBSE VII";
                            break;
                        case 1025: p = 151;// "SCHOOL PACKAGE CBSE VI";
                            break;

                    }
                }
                else
                {


                    switch (dtl2.Class_ID)
                    {
                        case 1035: p = 154;//= "SCHOOL PACKAGE CBSE VI";
                            break;
                        case 1036: p = 155;// "SCHOOL PACKAGE CBSE VII";
                            break;
                        case 1037: p = 156;// "SCHOOL PACKAGE CBSE VIII";
                            break;
                        case 1038: p = 157;// "SCHOOL PACKAGE CBSE IX";
                            break;
                        case 1039: p = 158;// "SCHOOL PACKAGE CBSE X";
                            break;

                    }

                }

                if (p > 0)
                {

                    var order = (from a in DbContext.tbl_DC_Order.Where(x => x.Is_Paid == true && x.Regd_ID == regId)
                                 join b in DbContext.tbl_DC_Order_Pkg.Where(x => x.Package_ID == p)
                                 on a.Order_ID equals b.Order_ID
                                 select new
                                 {
                                     OrderId = a.Order_ID
                                 }
                                 ).ToList();



                    if (order == null || order.Count == 0)
                    {
                        decimal payblamt = 0;
                        try
                        {

                            if (mob != null)
                            {
                                var regd_dtl = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == mob.Regd_ID).FirstOrDefault();
                                var student_class = DbContext.tbl_DC_Class.Where(x => x.Class_Id == regd_dtl.Class_ID && x.Board_Id == regd_dtl.Board_ID && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                var package = DbContext.tbl_DC_Package.Where(x => x.Package_ID == p && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                if (student_class != null && package != null)
                                {

                                    #region----------------------------------Cart Entry----------------------------------------------

                                    int?[] chapter;
                                    chapter = DbContext.tbl_DC_Package_Dtl.Where(x => x.Package_ID == p).Select(x => x.Chapter_Id).ToArray();
                                    var data_chapter = chapter.ToList();
                                    int id = Convert.ToInt32(mob.Regd_ID);
                                    string regno = mob.Regd_No;

                                    var ord = DbContext.tbl_DC_Cart.Where(x => x.Regd_ID == id && x.Status == true && x.In_Cart == true && x.Is_Paid == false).FirstOrDefault();

                                    tbl_DC_Cart crt = new tbl_DC_Cart();
                                    crt.Package_ID = Convert.ToInt32(package.Package_ID);
                                    crt.Order_ID = null;
                                    crt.Order_No = null;
                                    crt.Regd_ID = id;
                                    crt.Regd_No = regno;
                                    crt.Status = true;
                                    crt.In_Cart = true;
                                    crt.Is_Paid = false;
                                    crt.Is_Active = true;
                                    crt.Is_Deleted = false;
                                    crt.Total_Amt = Convert.ToDecimal(package.Price);
                                    crt.Inserted_Date = DateTime.Now;
                                    crt.Inserted_By = "Admin";
                                    DbContext.tbl_DC_Cart.Add(crt);
                                    DbContext.SaveChanges();

                                    if (ord != null)
                                    {
                                        crt.Order_ID = ord.Cart_ID;

                                        var ordid = crt.Order_ID;
                                        if (ordid == null)
                                        {
                                            crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + 1;
                                        }
                                        else
                                        {
                                            int ordno = Convert.ToInt32(crt.Order_ID);
                                            if (ordno > 0 && ordno <= 9)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 9 && ordno <= 99)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 99 && ordno <= 999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 999 && ordno <= 9999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 9999 && ordno <= 99999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0" + Convert.ToString(ordno);
                                            }
                                        }
                                        DbContext.SaveChanges();
                                    }
                                    else
                                    {
                                        crt.Order_ID = crt.Cart_ID;

                                        var ordid = crt.Order_ID;
                                        if (ordid == null)
                                        {
                                            crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + 1;
                                        }
                                        else
                                        {
                                            int ordno = Convert.ToInt32(crt.Order_ID);
                                            if (ordno > 0 && ordno <= 9)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 9 && ordno <= 99)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 99 && ordno <= 999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "000" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 999 && ordno <= 9999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "00" + Convert.ToString(ordno);
                                            }
                                            if (ordno > 9999 && ordno <= 99999)
                                            {
                                                crt.Order_No = "DGCRT" + "/" + today.ToString("yyMMdd") + "/" + "0" + Convert.ToString(ordno);
                                            }
                                        }
                                        DbContext.SaveChanges();
                                    }

                                    for (int i = 0; i < data_chapter.Count; i++)
                                    {
                                        int chp_id = Convert.ToInt32(data_chapter[i]);

                                        var data2 = (from a in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                     join b in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                     on a.Subject_Id equals b.Subject_Id
                                                     join c in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                     on b.Class_Id equals c.Class_Id
                                                     join d in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                     on c.Board_Id equals d.Board_Id
                                                     where a.Chapter_Id == chp_id
                                                     select new DigiChampCartModel
                                                     {
                                                         Board_ID = d.Board_Id,
                                                         Class_ID = c.Class_Id,
                                                         Subject_ID = b.Subject_Id,
                                                         Chapter_ID = a.Chapter_Id
                                                     }).FirstOrDefault();
                                        if (data2 != null)
                                        {
                                            tbl_DC_Cart_Dtl obj1 = new tbl_DC_Cart_Dtl();
                                            obj1.Cart_ID = crt.Cart_ID;
                                            obj1.Package_ID = Convert.ToInt32(package.Package_ID);
                                            obj1.Board_ID = data2.Board_ID;
                                            obj1.Class_ID = data2.Class_ID;
                                            obj1.Subject_ID = data2.Subject_ID;
                                            obj1.Chapter_ID = data2.Chapter_ID;
                                            obj1.Status = true;
                                            obj1.Inserted_Date = DateTime.Now;
                                            obj1.Inserted_By = "Admin";
                                            obj1.Is_Active = true;
                                            obj1.Is_Deleted = false;
                                            DbContext.tbl_DC_Cart_Dtl.Add(obj1);
                                            DbContext.SaveChanges();
                                        }
                                    }

                                    var data_crt = (from a in DbContext.tbl_DC_Registration.Where(x => x.Mobile == mob.Mobile && x.Is_Active == true && x.Is_Deleted == false)
                                                    join b in DbContext.tbl_DC_Cart.Where(x => x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false)
                                                     on a.Regd_ID equals b.Regd_ID
                                                    join c in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                        on b.Package_ID equals c.Package_ID
                                                    select new DigiChampCartModel
                                                    {
                                                        Package_Name = c.Package_Name,
                                                        Subscripttion_Period = c.Subscripttion_Period,
                                                        Package_ID = c.Package_ID,
                                                        Price = c.Price,
                                                        Is_Offline = c.Is_Offline,
                                                        Cart_ID = b.Cart_ID
                                                    }).ToList();

                                    if (data_crt.Count == 0)
                                    {


                                    }
                                    else
                                    {
                                        //ViewBag.cartitems = data_crt;
                                        decimal tax = 10;// taxcalculate();
                                        // ViewBag.taxper = tax;
                                        // TempData["totalitem"] = Convert.ToInt32(data_crt.Count);
                                        decimal totalprice1 = data_crt.ToList().Where(x => x.Is_Offline == false || x.Is_Offline == null).Select(c => (decimal)c.Price).Sum();

                                        var data_cart = (from x in data_crt.ToList().Where(x => x.Is_Offline == true)
                                                         join y in DbContext.tbl_DC_Package_Period.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                         on x.Package_ID equals y.Package_ID
                                                         select new DigiChampCartModel
                                                         {
                                                             Is_Offline = x.Is_Offline,
                                                             Package_ID = x.Package_ID,
                                                             Price = x.Price,
                                                             Excluded_Price = y.Excluded_Price
                                                         }).ToList();
                                        if (data_cart.Count > 0)
                                        {
                                            decimal totalprice2 = data_cart.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Price).Sum();
                                            decimal totalprice3 = data_cart.ToList().Where(x => x.Is_Offline == true).Select(c => (decimal)c.Excluded_Price).Sum();
                                            decimal totalpriceee = Convert.ToDecimal(totalprice1 + totalprice2);
                                            decimal totalprice = Convert.ToDecimal(totalprice1 + totalprice2 + totalprice3);
                                            decimal totalpaybletax = (tax * totalprice) / 100;
                                            decimal totalpayblamt = Math.Round((totalprice + totalpaybletax), 2);
                                            var price = DbContext.tbl_DC_CouponCode.Where(x => x.Is_Default == true && x.Valid_From <= today && x.Valid_To >= today && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
                                            if (price != null)
                                            {
                                                if (price.Discount_Price != null)
                                                {
                                                    int totl_pkg = Convert.ToInt32(data_crt.ToList().Count);
                                                    //TempData["discountprice"] = Convert.ToDecimal(price.Discount_Price);
                                                    decimal prc = Convert.ToDecimal(price.Discount_Price);

                                                    decimal priced = Math.Round(totalpriceee, 2);
                                                    decimal priceddisc = Math.Round((prc * totl_pkg), 2);
                                                    // TempData["price"] = Math.Round((priced - priceddisc + totalprice3), 2);

                                                    // Session["price1"] = Convert.ToDecimal(TempData["price"]);
                                                    decimal totalpaybletax1 = Math.Round(tax * Convert.ToDecimal(Math.Round((priced - priceddisc + totalprice3), 2))) / 100;
                                                    decimal totalpayblamt1 = Math.Round((Convert.ToDecimal(Math.Round((priced - priceddisc + totalprice3), 2)) + totalpaybletax1), 2);

                                                    payblamt = Math.Round(totalpayblamt1, 2); ;
                                                    //TempData["payblamt"] = Math.Round(totalpayblamt1, 2);
                                                    // Session["payamount"] = totalpayblamt1;
                                                    //TempData["Tax"] = Math.Round((Convert.ToDecimal(TempData["payblamt"]) - Convert.ToDecimal(TempData["price"])), 2);

                                                    //  decimal payblamt = Math.Round(totalpayblamt1, 2);
                                                    decimal taxAmt = Math.Round((Convert.ToDecimal(payblamt -
                                                        Convert.ToDecimal(Math.Round((priced - priceddisc + totalprice3), 2)))), 2); ;

                                                }
                                                else
                                                {
                                                    int totl_pkg = Convert.ToInt32(data_crt.ToList().Count);
                                                    //TempData["discountprice1"] = Convert.ToDecimal(price.Discount_Percent);
                                                    decimal prc = Convert.ToDecimal(Convert.ToDecimal(price.Discount_Percent));
                                                    decimal t_price = Convert.ToDecimal((Math.Round(totalpriceee, 2) * prc) / 100);

                                                    decimal priced = Math.Round(totalpriceee, 2);
                                                    decimal priceddisc = Math.Round((t_price * totl_pkg), 2);
                                                    // TempData["price"] = Math.Round((priced - t_price + totalprice3), 2);

                                                    // Session["price1"] = Convert.ToDecimal(Math.Round((priced - t_price + totalprice3), 2));
                                                    decimal totalpaybletax1 = (tax * Convert.ToDecimal(Math.Round((priced - t_price + totalprice3), 2))) / 100;
                                                    decimal totalpayblamt1 = Math.Round((Convert.ToDecimal(Math.Round((priced - t_price + totalprice3), 2)) + totalpaybletax1), 2);

                                                    payblamt = Math.Round(totalpayblamt1, 2); ;
                                                    // TempData["payblamt"] = Math.Round(totalpayblamt1, 2); ;
                                                    // Session["payamount"] = totalpayblamt1;
                                                    decimal taxAmt = Math.Round((Convert.ToDecimal(Math.Round(totalpayblamt1, 2))
                                                        - Convert.ToDecimal(Math.Round((priced - t_price + totalprice3), 2))), 2);
                                                }
                                            }
                                            else
                                            {

                                                payblamt = totalpayblamt;
                                                // TempData["payblamt"] = totalpayblamt;
                                                // Session["payamount"] = totalpayblamt;
                                                // TempData["price"] = Math.Round(totalprice, 2);
                                                // Session["price1"] = Convert.ToDecimal(TempData["price"]);
                                                // TempData["Tax"] = Math.Round((Convert.ToDecimal(TempData["payblamt"]) - Convert.ToDecimal(TempData["price"])), 2);
                                            }
                                        }
                                        else
                                        {
                                            decimal totalprice = Convert.ToDecimal(totalprice1);
                                            decimal totalpaybletax = (tax * totalprice) / 100;
                                            decimal totalpayblamt = Math.Round((totalprice + totalpaybletax), 2);
                                            var price = DbContext.tbl_DC_CouponCode.Where(x => x.Is_Default == true && x.Valid_From <= today && x.Valid_To >= today && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
                                            if (price != null)
                                            {
                                                if (price.Discount_Price != null)
                                                {
                                                    int totl_pkg = Convert.ToInt32(data_crt.ToList().Count);
                                                    // TempData["discountprice"] = Convert.ToDecimal(price.Discount_Price);
                                                    decimal prc = Convert.ToDecimal(price.Discount_Price);

                                                    decimal priced = Math.Round(totalprice, 2);
                                                    decimal priceddisc = Math.Round((prc * totl_pkg), 2);
                                                    //TempData["price"] = Math.Round((priced - priceddisc), 2);

                                                    // Session["price1"] = Convert.ToDecimal(TempData["price"]);
                                                    decimal totalpaybletax1 = (tax * Convert.ToDecimal(Math.Round((priced - priceddisc), 2))) / 100;
                                                    decimal totalpayblamt1 = Math.Round((Convert.ToDecimal(Math.Round((priced - priceddisc), 2)) + totalpaybletax1), 2);

                                                    payblamt = totalpayblamt1;
                                                    // TempData["payblamt"] = totalpayblamt1;
                                                    // Session["payamount"] = totalpayblamt1;
                                                    decimal taxAmt = Math.Round((Convert.ToDecimal(totalpayblamt1) -
                                                        Convert.ToDecimal(Math.Round((priced - priceddisc), 2))), 2);
                                                }
                                                else
                                                {
                                                    int totl_pkg = Convert.ToInt32(data_crt.ToList().Count);
                                                    //TempData["discountprice1"] = Convert.ToDecimal(price.Discount_Percent);
                                                    decimal prc = Convert.ToDecimal(Convert.ToDecimal(price.Discount_Percent));
                                                    decimal t_price = Convert.ToDecimal((Math.Round(totalprice, 2) * prc) / 100);

                                                    decimal priced = Math.Round(totalprice, 2);
                                                    decimal priceddisc = Math.Round((t_price * totl_pkg), 2);
                                                    // TempData["price"] = Math.Round((priced - t_price), 2);

                                                    // Session["price1"] = Convert.ToDecimal(Math.Round((priced - t_price), 2));
                                                    decimal totalpaybletax1 = (tax * Convert.ToDecimal(Math.Round((priced - t_price), 2))) / 100;
                                                    decimal totalpayblamt1 = Math.Round((Convert.ToDecimal(Math.Round((priced - t_price), 2)) + totalpaybletax1), 2);
                                                    payblamt = totalpayblamt1;
                                                    // TempData["payblamt"] = totalpayblamt1;
                                                    // Session["payamount"] = totalpayblamt1;
                                                    decimal taxAmt = Math.Round((Convert.ToDecimal(totalpayblamt1) -
                                                         Convert.ToDecimal(Math.Round((priced - t_price), 2))), 2);
                                                }
                                            }
                                            else
                                            {

                                                payblamt = Math.Round(totalpayblamt, 2);
                                                decimal priced = Math.Round(totalprice, 2);
                                                // TempData["payblamt"] = Math.Round(totalpayblamt, 2);
                                                //Session["payamount"] = totalpayblamt;
                                                //TempData["price"] = Math.Round(totalprice, 2);
                                                // Session["price1"] = Convert.ToDecimal(TempData["price"]);
                                                // TempData["Tax"] 
                                                decimal taxAmt = Math.Round((Convert.ToDecimal(payblamt) -
                                                    Convert.ToDecimal(Math.Round(totalprice, 2))), 2);
                                            }

                                        }
                                    }


                                    #endregion

                                    #region----------------------------------Package Entry-------------------------------------------

                                    var data3 = (from e in DbContext.tbl_DC_Cart.Where(x => x.Order_ID == crt.Order_ID && x.Order_No == crt.Order_No && x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false)
                                                 join f in DbContext.tbl_DC_Cart_Dtl.Where(x => x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                                 on e.Cart_ID equals f.Cart_ID
                                                 join a in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                 on f.Chapter_ID equals a.Chapter_Id
                                                 join b in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                 on a.Subject_Id equals b.Subject_Id
                                                 join c in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                 on b.Class_Id equals c.Class_Id
                                                 join d in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                 on c.Board_Id equals d.Board_Id
                                                 select new DigiChampCartModel
                                                 {
                                                     Regd_ID = e.Regd_ID,
                                                     Regd_No = e.Regd_No,
                                                     Cart_ID = e.Cart_ID,
                                                     Order_ID = e.Order_ID,
                                                     Order_No = e.Order_No,
                                                     Board_ID = f.Board_ID,
                                                     Board_Name = d.Board_Name,
                                                     Class_ID = f.Class_ID,
                                                     Class_Name = c.Class_Name,
                                                     Subject_ID = f.Subject_ID,
                                                     Subject = b.Subject,
                                                     Chapter_ID = f.Chapter_ID,
                                                     Chapter = a.Chapter
                                                 }).ToList();

                                    var data31 = DbContext.tbl_DC_Cart.Where(x => x.Order_ID == crt.Order_ID && x.Order_No == crt.Order_No && x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false).ToList();

                                    //  ViewBag.cartdata = data31;
                                    //int cart_id = Convert.ToInt32(data3.ToList()[0].Cart_ID);

                                    //------------------------------Order cofirm region-----------------------

                                    var data1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mob.Mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();

                                    //  ViewBag.mobile = data1.Mobile;
                                    //  ViewBag.custmer_name = data1.Customer_Name;

                                    var data = (from a in DbContext.tbl_DC_Registration.Where(x => x.Mobile == mob.Mobile && x.Is_Active == true && x.Is_Deleted == false)
                                                join b in DbContext.tbl_DC_Cart.Where(x => x.In_Cart == true && x.Is_Paid == false && x.Is_Active == true && x.Is_Deleted == false)
                                                 on a.Regd_ID equals b.Regd_ID
                                                join c in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                    on b.Package_ID equals c.Package_ID
                                                select new DigiChampCartModel
                                                {
                                                    Package_Name = c.Package_Name,
                                                    Subscripttion_Period = c.Subscripttion_Period,
                                                    Price = b.Total_Amt,
                                                    Order_ID = b.Order_ID,
                                                    Order_No = b.Order_No,
                                                    Inserted_Date = c.Inserted_Date,
                                                    Cart_ID = b.Cart_ID
                                                }).ToList();
                                    //ViewBag.cartitems = data;
                                    //ViewBag.data = data;

                                    if (data.Count == 0)
                                    {
                                        // ViewBag.cartitems = null;
                                        //TempData["emptycart"] = "Your cart is empty";
                                    }
                                    else
                                    {
                                        // Session["price"] = data.Sum(x => x.Price);
                                        //ViewBag.cartitems = data;
                                        decimal tax = 10;//taxcalculate();
                                        // TempData["totalitem"] = Convert.ToInt32(data.Count);
                                        decimal totalprice = Math.Round((Convert.ToDecimal(data.Sum(x => x.Price))), 2);
                                        decimal amt = (Convert.ToDecimal(totalprice) / (100 + tax)) * 100;
                                        decimal totalpaybletax = totalprice - amt;
                                        // decimal totalpayblamt = Math.Round((totalprice + totalpaybletax), 2);
                                        // TempData["payblamt"] = totalprice;
                                        // Session["payamount"] = totalprice;
                                        //Session["ordid"] = data.ToList()[0].Order_ID;
                                        //Session["ordno"] = data.ToList()[0].Order_No;
                                        //TempData["price"] = Math.Round(totalprice, 2);
                                        //Session["price"] = Math.Round(totalprice, 2);
                                        //TempData["Tax"] = totalpaybletax;
                                    }

                                    //------------------------------end region--------------------------------

                                    //-------------------------------Cart update region------------------------
                                    //string disc_amt = "";
                                    tbl_DC_Order ordobj = new tbl_DC_Order();
                                    ordobj.Cart_Order_No = crt.Order_No;
                                    ordobj.Trans_No = "";
                                    ordobj.Regd_ID = mob.Regd_ID;
                                    ordobj.Regd_No = regno;
                                    ordobj.No_of_Package = Convert.ToInt32(data.ToList().Count);
                                    ordobj.Amount = Convert.ToDecimal(data.Sum(x => x.Price));
                                    //if (Session["percent"] != null)
                                    //{
                                    //    ordobj.Disc_Perc = Convert.ToDecimal(Session["disc"]);
                                    //    disc_amt = Session["disc"] + " %";
                                    //    ordobj.Disc_Amt = null;
                                    //}
                                    //if (Session["percent1"] != null)
                                    //{
                                    //    ordobj.Disc_Perc = null;
                                    //    ordobj.Disc_Amt = Convert.ToDecimal(Session["disc"]);
                                    //    disc_amt = Convert.ToDecimal(Session["disc"]).ToString("N2");
                                    //}
                                    ordobj.Total = Convert.ToDecimal(payblamt);
                                    ordobj.Amt_In_Words = null;
                                    ordobj.Payment_Mode = "Offline";
                                    ordobj.Is_Paid = true;
                                    ordobj.Status = true;
                                    ordobj.Inserted_Date = DateTime.Now;
                                    string date = Convert.ToDateTime(ordobj.Inserted_Date).ToString("MM/dd/yyyy");
                                    // TempData["Purcahse_date"] = date;
                                    ordobj.Inserted_By = "Admin";
                                    ordobj.Is_Active = true;
                                    ordobj.Is_Deleted = false;
                                    DbContext.tbl_DC_Order.Add(ordobj);
                                    DbContext.SaveChanges();
                                    // Session["percent"] = null;
                                    // Session["percent1"] = null;
                                    var ord_id = ordobj.Order_ID;
                                    if (ord_id > 0)
                                    {
                                        ordobj.Order_No = "DCORD" + "0" + ord_id;
                                        //TempData["ordernumber"] = ord_id;
                                    }
                                    else
                                    {
                                        int ord_no = Convert.ToInt32(ordobj.Order_ID);
                                        if (ord_no > 0 && ord_no <= 9)
                                        {
                                            ordobj.Order_No = "DCORD" + "00000" + Convert.ToString(ord_no);
                                        }
                                        if (ord_no > 9 && ord_no <= 99)
                                        {
                                            ordobj.Order_No = "DCORD" + "0000" + Convert.ToString(ord_no);
                                        }
                                        if (ord_no > 99 && ord_no <= 999)
                                        {
                                            ordobj.Order_No = "DCORD" + "000" + Convert.ToString(ord_no);
                                        }
                                        if (ord_no > 999 && ord_no <= 9999)
                                        {
                                            ordobj.Order_No = "DCORD" + "00" + Convert.ToString(ord_no);
                                        }
                                        if (ord_no > 9999 && ord_no <= 99999)
                                        {
                                            ordobj.Order_No = "DCORD" + "0" + Convert.ToString(ord_no);
                                        }
                                        //TempData["ordernumber"] = ordobj.Order_No;
                                    }
                                    DbContext.SaveChanges();

                                    var tax3 = (from tax1 in DbContext.tbl_DC_Tax_Type_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                join tax2 in DbContext.tbl_DC_Tax_Master.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                on tax1.TaxType_ID equals tax2.TaxType_ID
                                                select new DigiChampCartModel
                                                {
                                                    Tax_ID = tax2.Tax_ID,
                                                    TaxType_ID = tax1.TaxType_ID,
                                                    Tax_Rate = tax2.Tax_Rate,
                                                    TAX_Efect_Date = tax2.TAX_Efect_Date
                                                }).ToList();
                                    //ViewBag.tax3 = tax3;

                                    if (tax3.Count > 0)
                                    {
                                        foreach (var item3 in tax3)
                                        {
                                            tbl_DC_Order_Tax ordtax = new tbl_DC_Order_Tax();
                                            ordtax.Order_ID = ordobj.Order_ID;
                                            ordtax.Order_No = ordobj.Order_No;
                                            ordtax.Tax_ID = item3.Tax_ID;
                                            ordtax.Tax_Type_ID = item3.TaxType_ID;
                                            ordtax.Tax_Effect_Date = item3.TAX_Efect_Date;
                                            ordtax.Tax_Amt = Convert.ToDecimal(((ordobj.Amount) * (item3.Tax_Rate)) / 100);
                                            ordtax.Status = true;
                                            ordtax.Inserted_Date = DateTime.Now;
                                            ordtax.Inserted_By = "Admin";
                                            ordtax.Is_Active = true;
                                            ordtax.Is_Deleted = false;
                                            DbContext.tbl_DC_Order_Tax.Add(ordtax);
                                            DbContext.SaveChanges();
                                        }
                                    }

                                    var order_pkg = (from ord1 in DbContext.tbl_DC_Order.Where(x => x.Regd_ID == mob.Regd_ID && x.Cart_Order_No == crt.Order_No && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                                     join cart in DbContext.tbl_DC_Cart.Where(x => x.Regd_ID == mob.Regd_ID && x.Order_No == crt.Order_No && x.In_Cart == true && x.Is_Paid == false && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                                     on ord1.Cart_Order_No equals cart.Order_No
                                                     join pkg in DbContext.tbl_DC_Package.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                     on cart.Package_ID equals pkg.Package_ID
                                                     select new DigiChampCartModel
                                                     {
                                                         OrderID = ord1.Order_ID,
                                                         OrderNo = ord1.Order_No,
                                                         Package_ID = cart.Package_ID,
                                                         Package_Name = pkg.Package_Name,
                                                         Package_Desc = pkg.Package_Desc,
                                                         Total_Chapter = pkg.Total_Chapter,
                                                         Price = pkg.Price,
                                                         Thumbnail = pkg.Thumbnail,
                                                         Subscripttion_Period = pkg.Subscripttion_Period,
                                                         Is_Offline = pkg.Is_Offline
                                                     }).ToList();
                                    // ViewBag.package = order_pkg;

                                    foreach (var item in order_pkg)
                                    {
                                        tbl_DC_Order_Pkg ordpkg = new tbl_DC_Order_Pkg();
                                        ordpkg.Order_ID = item.OrderID;
                                        ordpkg.Order_No = item.OrderNo;
                                        ordpkg.Package_ID = item.Package_ID;
                                        ordpkg.Package_Name = item.Package_Name;
                                        ordpkg.Package_Desc = item.Package_Desc;
                                        ordpkg.Total_Chapter = item.Total_Chapter;
                                        ordpkg.Price = item.Price;
                                        ordpkg.Thumbnail = item.Thumbnail;
                                        ordpkg.Subscription_Period = item.Subscripttion_Period;
                                        int days = Convert.ToInt32(ordpkg.Subscription_Period);
                                        ordpkg.Status = true;
                                        ordpkg.Inserted_Date = DateTime.Now;
                                        ordpkg.Expiry_Date = Convert.ToDateTime(ordpkg.Inserted_Date).AddDays(days);
                                        ordpkg.Inserted_By = "Admin";
                                        ordpkg.Is_Active = true;
                                        ordpkg.Is_Deleted = false;
                                        if (order_pkg.ToList()[0].Is_Offline == true)
                                        {
                                            ordpkg.Is_Offline = true;
                                        }
                                        else
                                        {
                                            ordpkg.Is_Offline = false;
                                        }
                                        DbContext.tbl_DC_Order_Pkg.Add(ordpkg);
                                        DbContext.SaveChanges();

                                        int pkg_id = Convert.ToInt32(ordpkg.Package_ID);
                                        int ord_pkg_id = Convert.ToInt32(ordpkg.OrderPkg_ID);
                                        foreach (var v in data31)
                                        {
                                            int cart_id = Convert.ToInt32(v.Cart_ID);

                                            var ordpkg_sub = (from ab in DbContext.tbl_DC_Order_Pkg.Where(x => x.OrderPkg_ID == ord_pkg_id && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                                              join af in DbContext.tbl_DC_Cart_Dtl.Where(x => x.Cart_ID == cart_id && x.Is_Active == true && x.Is_Deleted == false)
                                                              on ab.Package_ID equals af.Package_ID
                                                              join ag in DbContext.tbl_DC_Chapter.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                              on af.Chapter_ID equals ag.Chapter_Id
                                                              join ah in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                              on ag.Subject_Id equals ah.Subject_Id
                                                              join ai in DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                              on ah.Class_Id equals ai.Class_Id
                                                              join aj in DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                              on ai.Board_Id equals aj.Board_Id
                                                              select new DigiChampCartModel
                                                              {
                                                                  OrderPkg_ID = ab.OrderPkg_ID,
                                                                  Package_ID = af.Package_ID,
                                                                  Board_ID = aj.Board_Id,
                                                                  Board_Name = aj.Board_Name,
                                                                  Class_ID = ai.Class_Id,
                                                                  Class_Name = ai.Class_Name,
                                                                  Subject_ID = ah.Subject_Id,
                                                                  Subject = ah.Subject,
                                                                  Chapter_ID = ag.Chapter_Id,
                                                                  Chapter = ag.Chapter
                                                              }).ToList();
                                            //ViewBag.ordpkg_sub = ordpkg_sub;

                                            foreach (var item1 in ordpkg_sub)
                                            {
                                                tbl_DC_Order_Pkg_Sub ordpkgsub = new tbl_DC_Order_Pkg_Sub();
                                                ordpkgsub.OrderPkg_ID = item1.OrderPkg_ID;
                                                ordpkgsub.Package_ID = item1.Package_ID;
                                                ordpkgsub.Board_ID = item1.Board_ID;
                                                ordpkgsub.Board_Name = item1.Board_Name;
                                                ordpkgsub.Class_ID = item1.Class_ID;
                                                ordpkgsub.Class_Name = item1.Class_Name;
                                                ordpkgsub.Subject_ID = item1.Subject_ID;
                                                ordpkgsub.Subject_Name = item1.Subject;
                                                ordpkgsub.Chapter_ID = item1.Chapter_ID;
                                                ordpkgsub.Chapter_Name = item1.Chapter;
                                                ordpkgsub.Status = true;
                                                ordpkgsub.Inserted_Date = DateTime.Now;
                                                ordpkgsub.Inserted_By = "Admin";
                                                ordpkgsub.Is_Active = true;
                                                ordpkgsub.Is_Deleted = false;
                                                DbContext.tbl_DC_Order_Pkg_Sub.Add(ordpkgsub);
                                                DbContext.SaveChanges();

                                                int ord_pkg_sub_id = Convert.ToInt32(ordpkgsub.OrderPkgSub_ID);

                                                var ordpkg_sub_mod = (from ak in DbContext.tbl_DC_Order_Pkg_Sub.Where(x => x.OrderPkgSub_ID == ord_pkg_sub_id && x.Status == true && x.Is_Active == true && x.Is_Deleted == false)
                                                                      join al in DbContext.tbl_DC_Module.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                          on ak.Chapter_ID equals al.Chapter_Id
                                                                      select new DigiChampCartModel
                                                                      {
                                                                          OrderPkgSub_ID = ak.OrderPkgSub_ID,
                                                                          Chapter_ID = ak.Chapter_ID,
                                                                          Chapter = ak.Chapter_Name,
                                                                          Module_ID = al.Module_ID,
                                                                          Module_Name = al.Module_Name
                                                                      }).ToList();
                                                //  ViewBag.ordpkg_sub_mod = ordpkg_sub_mod;
                                                if (ordpkg_sub_mod.Count > 0)
                                                {
                                                    foreach (var item2 in ordpkg_sub_mod)
                                                    {
                                                        tbl_DC_Order_Pkg_Sub_Mod ordpkgsubmod = new tbl_DC_Order_Pkg_Sub_Mod();
                                                        ordpkgsubmod.OrderPkgSub_ID = item2.OrderPkgSub_ID;
                                                        ordpkgsubmod.Chapter_ID = item2.Chapter_ID;
                                                        ordpkgsubmod.Chapter_Name = item2.Chapter;
                                                        ordpkgsubmod.Module_ID = item2.Module_ID;
                                                        ordpkgsubmod.Module_Name = item2.Module_Name;
                                                        ordpkgsubmod.Status = true;
                                                        ordpkgsubmod.Inserted_Date = DateTime.Now;
                                                        ordpkgsubmod.Inserted_By = "Admin";
                                                        ordpkgsubmod.Is_Active = true;
                                                        ordpkgsubmod.Is_Deleted = false;
                                                        DbContext.tbl_DC_Order_Pkg_Sub_Mod.Add(ordpkgsubmod);
                                                        DbContext.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }


                                    var cartobj = (from ct in DbContext.tbl_DC_Cart.Where(x => x.Order_ID == crt.Order_ID && x.Order_No == crt.Order_No && x.Is_Active == true && x.Is_Deleted == false)
                                                   select new DigiChampCartModel
                                                   {
                                                       Cart_ID = ct.Cart_ID,
                                                       In_Cart = ct.In_Cart,
                                                       Is_Paid = ct.Is_Paid,
                                                       Status = ct.Status,
                                                       Is_Active = ct.Is_Active,
                                                       Is_Deleted = ct.Is_Deleted
                                                   }).ToList();
                                    // ViewBag.cartobj = cartobj;

                                    foreach (var item4 in cartobj)
                                    {
                                        int ctid = Convert.ToInt32(item4.Cart_ID);
                                        tbl_DC_Cart cartobj1 = DbContext.tbl_DC_Cart.Where(x => x.Cart_ID == ctid && x.Is_Active == true && x.Is_Deleted == false && x.In_Cart == true && x.Is_Paid == false).FirstOrDefault();
                                        cartobj1.In_Cart = false;
                                        cartobj1.Is_Paid = true;
                                        cartobj1.Status = false;
                                        cartobj1.Is_Active = false;
                                        cartobj1.Is_Deleted = true;
                                        DbContext.Entry(cartobj1).State = EntityState.Modified;
                                        DbContext.SaveChanges();
                                    }

                                    #endregion
                                    mob.SchoolId = s;
                                    mob.SectionId = se;
                                    DbContext.SaveChanges();



                                }
                                return Request.CreateResponse(HttpStatusCode.OK, "YES");
                            }
                            else
                            {
                                // TempData["SuccessMessage"] = "Saved Successfully...";
                                return Request.CreateResponse(HttpStatusCode.OK, "YES");
                            }
                        }


                        catch (Exception e)
                        {
                            throw e;
                            //TempData["ErrorMessage"] = "Something Went Wrong! Please try again...";
                            //return Json(0, JsonRequestBehavior.AllowGet);
                        }

                    }
                    else

                        return Request.CreateResponse(HttpStatusCode.NoContent, "ORDER FAILED");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "P ID ZERO");
            }
            else
                return Request.CreateResponse(HttpStatusCode.Conflict, "NO");
        }
    }
}