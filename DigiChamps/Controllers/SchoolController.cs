﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DigiChamps.Models;
using System.IO;
using DigiChamps.DigiChampsEnum;
using System.Net.Mail;
using DigiChamps.Common;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using DigiChamps.Enums;

using System.Net;
using System.Threading.Tasks;
using System.Web.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ClosedXML.Excel;
using System.Data.OleDb;
using System.Collections;

namespace DigiChamps.Controllers
{
    public class SchoolController : Controller
    {

        public static string t { get; set; }
        public static Guid? schoolId { get; set; }

        DigiChampsEntities DbContext = new DigiChampsEntities();

        public ActionResult Index()
        {
            return RedirectToAction("SchoolDashBoard");
        }

        #region SchoolDashboard
        public ActionResult SchoolDashBoard(Guid? id, string type)
        {
            try
            {
                if (id != null && id != Guid.Empty)
                    Session["id"] = id;
                else if (Session["id"] != null)
                    id = new Guid(Session["id"].ToString());
                else

                    return RedirectToAction("AdminDashBoard", "Admin");

                ViewBag.SchoolName = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == id).Select(x => x.SchoolName).FirstOrDefault();

                ViewBag.Description = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == id).Select(x => x.SchoolDescription).FirstOrDefault();
                ViewBag.Logo = DbContext.tbl_DC_School_Info.Where(x => x.SchoolId == id).Select(x => x.SchoolLogo).FirstOrDefault();
                ViewBag.TeacherCount = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "Teacher" && x.SchoolId == id && x.IsActive == true).Count();
                ViewBag.SchoolAdmin = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "SchoolAdmin" && x.SchoolId == id && x.IsActive == true).Count();
                ViewBag.PrincipleCount = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "Principle" && x.SchoolId == id && x.IsActive == true).Count();
            }
            catch (Exception ex)
            {

            }
            if (type != null)
            {
                t = type;
            }
            schoolId = id;
            ViewBag.Message = t;
            return View();

        }
        #endregion

        #region SchoolDetail
        public ActionResult GetSchoolList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            SchoolModel.SchoolInformationOutput objOutput = new SchoolModel.SchoolInformationOutput();
            objOutput = obj.GetSchool(null);

            return View(objOutput.schoolInformation);
        }

        public ActionResult DeleteSchool(Guid id)
        {
            tbl_DC_School_Info obj = DbContext.tbl_DC_School_Info.Where(a => a.SchoolId == id).FirstOrDefault();
            obj.IsActive = false;
            obj.modifiedDate = DateTime.Now;
            DbContext.SaveChanges();
            return RedirectToAction("GetSchoolList", "School");
        }
        public ActionResult AddSchool(Guid? Id)
        {
            SchoolModel.SchoolInformation objSchoolInformation = new SchoolModel.SchoolInformation();

            if (Id != null)
            {
                SchoolAPIController schoolApi = new SchoolAPIController();
                objSchoolInformation = schoolApi.GetSchoolBySchoolId(Id);
                return View(objSchoolInformation);
            }

            return View(objSchoolInformation);
        }


        [HttpPost]
        public ActionResult AddSchool(SchoolModel.SchoolInformation objSchoolInformation)
        {
            //Add School 
            if (objSchoolInformation.SchoolId == Guid.Empty || objSchoolInformation.SchoolId == null)
            {
                //Check duplicate value for school
                if (DbContext.tbl_DC_School_Info.Any(x => x.SchoolName == objSchoolInformation.SchoolName.Trim()))
                {
                    TempData["Message"] = "School Name Already Exists.";
                    return View(objSchoolInformation);
                }

                objSchoolInformation.SchoolId = Guid.NewGuid();
                objSchoolInformation.IsActive = true;
                #region UploadFile
                FileHelper fileHelper = new FileHelper();
                string browserType = Request.Browser.Browser.ToUpper();
                UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", objSchoolInformation.SchoolId.ToString(), "Image", browserType);
                objSchoolInformation.Logo = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                objSchoolInformation.DocumentaryVideo = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                //objSchoolInformation.Logo = uploadFileDetailModel.ImageName;
                #endregion


                DbContext.Sp_DC_SchoolInfo(objSchoolInformation.SchoolId, objSchoolInformation.SchoolName.Trim(), (!string.IsNullOrEmpty(objSchoolInformation.Information) ? objSchoolInformation.Information.Trim() : objSchoolInformation.Information), objSchoolInformation.Logo, objSchoolInformation.DocumentaryVideo, objSchoolInformation.ThumbnailPath, DateTime.Now, DateTime.Now, objSchoolInformation.IsActive);
                TempData["Message"] = "School Added Successfully.";
            }
            else
            {
                #region UploadFile
                FileHelper fileHelper = new FileHelper();
                string browserType = Request.Browser.Browser.ToUpper();
                UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", objSchoolInformation.SchoolId.ToString(), "Image", browserType);
                if (uploadFileDetailModel != null)
                {
                    if (uploadFileDetailModel.ImagePath != null && uploadFileDetailModel.ImageName != null)
                        objSchoolInformation.Logo = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                    if (uploadFileDetailModel.VideoPath != null && uploadFileDetailModel.VideoName != null)
                        objSchoolInformation.DocumentaryVideo = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                    //objSchoolInformation.Logo = uploadFileDetailModel.ImageName;
                }
                #endregion

                DbContext.Sp_DC_SchoolInfo(objSchoolInformation.SchoolId, objSchoolInformation.SchoolName.Trim(), (!string.IsNullOrEmpty(objSchoolInformation.Information) ? objSchoolInformation.Information.Trim() : objSchoolInformation.Information), objSchoolInformation.Logo, objSchoolInformation.DocumentaryVideo, objSchoolInformation.ThumbnailPath, DateTime.Now, DateTime.Now, objSchoolInformation.IsActive);
                TempData["Message"] = "School Updated Successfully.";
            }
            return RedirectToAction("GetSchoolList", "School");
        }

        #endregion

        #region SchoolAdminDetail
        public ActionResult GetSchoolAdminList()
        {
            SchoolModel.SchoolAdminOrPrincipleModel objOutput = new SchoolModel.SchoolAdminOrPrincipleModel();
            List<SchoolModel.SchoolAdminOrPrincipleModel> objList = new List<SchoolModel.SchoolAdminOrPrincipleModel>();

            try
            {

                Guid schoolId = new Guid(Session["id"].ToString());

                var schoolAdminList = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "SchoolAdmin" && x.IsActive == true && x.SchoolId == schoolId).OrderByDescending(x => x.CreatedDate).ToList();
                if (schoolAdminList.Any())
                {
                    foreach (var item in schoolAdminList)
                    {
                        SchoolModel.SchoolAdminOrPrincipleModel objSchoolInformation = new SchoolModel.SchoolAdminOrPrincipleModel();
                        objSchoolInformation.Id = item.UserId;
                        objSchoolInformation.SchoolId = (Guid)item.SchoolId;
                        objSchoolInformation.FirstName = item.UserFirstname;
                        objSchoolInformation.LastName = item.UserLastname;
                        objSchoolInformation.IsActive = (bool)item.IsActive;
                        objSchoolInformation.Image = item.UserProfilePhoto;
                        //objSchoolInformation.ph = item.UserPhoneNumber;
                        objSchoolInformation.EmailAddress = item.UserEmailAddress;
                        objSchoolInformation.Password = item.UserPassword;
                        objList.Add(objSchoolInformation);


                    }
                }
                objOutput.schoolInformation = objList;
            }

            catch (Exception ex)
            {

            }
            return View(objOutput.schoolInformation);
        }

        public ActionResult AddSchoolAdmin(Guid? Id)
        {
            SchoolModel.SchoolAdminOrPrincipleModel objSchoolAdminOrPrincipleModel = new SchoolModel.SchoolAdminOrPrincipleModel();
            if (Id != null && Id != Guid.Empty)
            {
                var admin = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == Id).FirstOrDefault();
                objSchoolAdminOrPrincipleModel.Id = admin.UserId;
                objSchoolAdminOrPrincipleModel.Password = admin.UserPassword;
                objSchoolAdminOrPrincipleModel.EmailAddress = admin.UserEmailAddress;
                objSchoolAdminOrPrincipleModel.FirstName = admin.UserFirstname;
                objSchoolAdminOrPrincipleModel.Image = admin.UserProfilePhoto;
                objSchoolAdminOrPrincipleModel.LastName = admin.UserLastname;
                objSchoolAdminOrPrincipleModel.SchoolId = (Guid)admin.SchoolId;
            }
            ViewBag.School_Id = new SelectList(DbContext.tbl_DC_School_Info.Where(x => x.IsActive == true), "SchoolId", "SchoolName");

            return View(objSchoolAdminOrPrincipleModel);
        }

        [HttpPost]
        public ActionResult AddSchoolAdmin(SchoolModel.SchoolAdminOrPrincipleModel objSchoolAdminOrPrincipleModel)//CreatOrEditSchoolAdmin
        {
            try
            {
                objSchoolAdminOrPrincipleModel.SchoolId = new Guid(Session["id"].ToString());
                if (objSchoolAdminOrPrincipleModel.Id == Guid.Empty || objSchoolAdminOrPrincipleModel.Id == null)
                {
                    //Check email Id already exist
                    if (DbContext.tbl_DC_SchoolUser.Any(x => x.SchoolId == objSchoolAdminOrPrincipleModel.SchoolId && x.UserEmailAddress == objSchoolAdminOrPrincipleModel.EmailAddress && x.UserRole == "SchoolAdmin"))
                    {
                        TempData["Message"] = "Email Address Already Exists.";
                        return RedirectToAction("GetSchoolAdminList", "School");
                    }
                    objSchoolAdminOrPrincipleModel.Id = Guid.NewGuid();
                    objSchoolAdminOrPrincipleModel.IsActive = true;
                    TempData["Message"] = "School Admin Added Successfully.";
                }
                else
                {
                    objSchoolAdminOrPrincipleModel.IsActive = true;
                    TempData["Message"] = "School Admin Updated Successfully.";
                }
                #region UploadFile
                if (Request.Files.Count > 0 && Request.Files[0] != null && Request.Files[0].ContentLength > 0)
                {
                    FileHelper fileHelper = new FileHelper();
                    string browserType = Request.Browser.Browser.ToUpper();
                    UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", objSchoolAdminOrPrincipleModel.SchoolId.ToString(), "Admin", browserType);
                    objSchoolAdminOrPrincipleModel.Image = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                }
                #endregion
                int result = 0;
                result = DbContext.SP_DC_SchoolUser(objSchoolAdminOrPrincipleModel.Id, objSchoolAdminOrPrincipleModel.SchoolId, objSchoolAdminOrPrincipleModel.FirstName, objSchoolAdminOrPrincipleModel.LastName, objSchoolAdminOrPrincipleModel.EmailAddress, objSchoolAdminOrPrincipleModel.EmailAddress, objSchoolAdminOrPrincipleModel.Password, objSchoolAdminOrPrincipleModel.Image, "SchoolAdmin", "", DateTime.Now, DateTime.Now, objSchoolAdminOrPrincipleModel.IsActive);

                //---------------Mail Send functionality------------
                if (objSchoolAdminOrPrincipleModel.Id == Guid.Empty || objSchoolAdminOrPrincipleModel.Id == null)
                {
                    string emailMsg = "Hi, this is to inform you your school admin credential are Username " + objSchoolAdminOrPrincipleModel.EmailAddress + " password " + objSchoolAdminOrPrincipleModel.Password + ".";
                    EmailHelper.SendEmail(objSchoolAdminOrPrincipleModel.EmailAddress, "Admin Credentials", emailMsg);
                }
                //Mail Send functionality
            }
            catch (Exception EX)
            {

            }
            return new JsonResult()
            {
                Data = true,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

        }

        #endregion

        #region TeacherDetail

        public ActionResult GetSchoolTeacherList()
        {
            SchoolModel.SchoolAdminOrPrincipleModel objOutput = new SchoolModel.SchoolAdminOrPrincipleModel();
            List<SchoolModel.SchoolAdminOrPrincipleModel> objList = new List<SchoolModel.SchoolAdminOrPrincipleModel>();


            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                var teacherList = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "Teacher" && x.IsActive == true && x.SchoolId == schoolId).OrderByDescending(x => x.CreatedDate).ToList();
                if (teacherList.Any())
                {
                    foreach (var item in teacherList)
                    {
                        SchoolModel.SchoolAdminOrPrincipleModel objSchoolInformation = new SchoolModel.SchoolAdminOrPrincipleModel();
                        objSchoolInformation.Id = item.UserId;
                        objSchoolInformation.SchoolId = (Guid)item.SchoolId;
                        objSchoolInformation.FirstName = item.UserFirstname;
                        objSchoolInformation.LastName = item.UserLastname;
                        objSchoolInformation.IsActive = (bool)item.IsActive;
                        objSchoolInformation.Image = item.UserProfilePhoto;
                        //objSchoolInformation.ph = item.UserPhoneNumber;
                        objSchoolInformation.EmailAddress = item.UserEmailAddress;
                        objSchoolInformation.Password = item.UserPassword;
                        objList.Add(objSchoolInformation);
                    }
                }
                objOutput.schoolInformation = objList;
            }

            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return View(objOutput.schoolInformation);
        }

        public ActionResult AddSchoolTeacher(Guid? Id)
        {

            SchoolModel.SchoolAdminOrPrincipleModel objSchoolAdminOrPrincipleModel = new SchoolModel.SchoolAdminOrPrincipleModel();
            if (Id != null && Id != Guid.Empty)
            {
                var teacher = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == Id).SingleOrDefault();
                objSchoolAdminOrPrincipleModel.Id = teacher.UserId;
                objSchoolAdminOrPrincipleModel.Password = teacher.UserPassword;
                objSchoolAdminOrPrincipleModel.EmailAddress = teacher.UserEmailAddress;
                objSchoolAdminOrPrincipleModel.FirstName = teacher.UserFirstname;
                objSchoolAdminOrPrincipleModel.Image = teacher.UserProfilePhoto;
                objSchoolAdminOrPrincipleModel.LastName = teacher.UserLastname;
                objSchoolAdminOrPrincipleModel.SchoolId = (Guid)teacher.SchoolId;
            }
            ViewBag.Message = t;

            return View(objSchoolAdminOrPrincipleModel);

        }


        //  return
        [HttpPost]
        public ActionResult AddSchoolTeacher(SchoolModel.SchoolAdminOrPrincipleModel objSchoolTeacherModel)//CreatOrEditTeacherProfile
        {
            try
            {
                objSchoolTeacherModel.SchoolId = new Guid(Session["id"].ToString());
                if (objSchoolTeacherModel.Id == Guid.Empty || objSchoolTeacherModel.Id == null)
                {
                    //Check unique email
                    if (DbContext.tbl_DC_SchoolUser.Any(x => x.SchoolId == objSchoolTeacherModel.SchoolId && x.UserEmailAddress == objSchoolTeacherModel.EmailAddress && x.UserRole == "Teacher"))
                    {
                        TempData["Message"] = "Email Address Already Exists.";
                        return RedirectToAction("GetSchoolTeacherList", "School");
                    }
                    objSchoolTeacherModel.IsActive = true;
                    objSchoolTeacherModel.Id = Guid.NewGuid();
                    TempData["Message"] = "School Teacher Added Successfully.";
                }
                else
                {
                    objSchoolTeacherModel.IsActive = true;
                    TempData["Message"] = "School Teacher Updated Successfully.";
                }
                int result = DbContext.SP_DC_SchoolUser(objSchoolTeacherModel.Id, objSchoolTeacherModel.SchoolId, objSchoolTeacherModel.FirstName, objSchoolTeacherModel.LastName, objSchoolTeacherModel.EmailAddress, "", objSchoolTeacherModel.Password, objSchoolTeacherModel.Image, "Teacher", "", DateTime.Now, DateTime.Now, objSchoolTeacherModel.IsActive);

                //---------------Mail Send functionality------------
                if (result > 0)
                {
                    string emailMsg = "Hi, this is to inform you your school admin credential are Username " + objSchoolTeacherModel.EmailAddress + " password " + objSchoolTeacherModel.Password + ".";
                    EmailHelper.SendEmail(objSchoolTeacherModel.EmailAddress, "Admin Credentials", emailMsg);
                }
                //Mail Send functionality
            }
            catch (Exception EX)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetSchoolTeacherList", "School");
        }
        #endregion

        #region PrincipalDetail
        public ActionResult GetSchoolPrincipleList()
        {
            SchoolModel.SchoolAdminOrPrincipleModel objOutput = new SchoolModel.SchoolAdminOrPrincipleModel();
            List<SchoolModel.SchoolAdminOrPrincipleModel> objList = new List<SchoolModel.SchoolAdminOrPrincipleModel>();

            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                var schoolList = DbContext.tbl_DC_SchoolUser.Where(x => x.UserRole == "Principle" && x.IsActive == true && x.SchoolId == schoolId).ToList();
                if (schoolList.Any())
                {
                    foreach (var item in schoolList)
                    {
                        SchoolModel.SchoolAdminOrPrincipleModel objSchoolInformation = new SchoolModel.SchoolAdminOrPrincipleModel();
                        objSchoolInformation.Id = item.UserId;
                        objSchoolInformation.SchoolId = (Guid)item.SchoolId;
                        objSchoolInformation.FirstName = item.UserFirstname;
                        objSchoolInformation.LastName = item.UserLastname;
                        objSchoolInformation.IsActive = (bool)item.IsActive;
                        objSchoolInformation.Image = item.UserProfilePhoto;
                        //objSchoolInformation.ph = item.UserPhoneNumber;
                        objSchoolInformation.EmailAddress = item.UserEmailAddress;
                        objSchoolInformation.Password = item.UserPassword;
                        objList.Add(objSchoolInformation);
                    }
                    objOutput.schoolInformation = objList;
                }
            }

            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return View(objOutput.schoolInformation);
        }

        public ActionResult AddSchoolPrincipal(Guid? Id)//CreateOrEditPrincipleProfile
        {
            SchoolModel.SchoolAdminOrPrincipleModel objSchoolAdminOrPrincipleModel = new SchoolModel.SchoolAdminOrPrincipleModel();
            if (Id != null && Id != Guid.Empty)
            {
                var principle = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == Id).SingleOrDefault();
                objSchoolAdminOrPrincipleModel.Id = principle.UserId;
                objSchoolAdminOrPrincipleModel.Password = principle.UserPassword;
                objSchoolAdminOrPrincipleModel.EmailAddress = principle.UserEmailAddress;
                objSchoolAdminOrPrincipleModel.FirstName = principle.UserFirstname;
                objSchoolAdminOrPrincipleModel.Image = principle.UserProfilePhoto;
                objSchoolAdminOrPrincipleModel.LastName = principle.UserLastname;
                objSchoolAdminOrPrincipleModel.SchoolId = (Guid)principle.SchoolId;
            }
            ViewBag.Message = t;
            return View(objSchoolAdminOrPrincipleModel);
        }

        [HttpPost]
        public ActionResult AddSchoolPrincipal(SchoolModel.SchoolAdminOrPrincipleModel objSchoolPrincipalModel)
        {
            try
            {

                objSchoolPrincipalModel.SchoolId = new Guid(Session["id"].ToString());
                objSchoolPrincipalModel.Image = "";
                if (objSchoolPrincipalModel.Id == Guid.Empty || objSchoolPrincipalModel.Id == null)
                {
                    //Check unique email
                    if (DbContext.tbl_DC_SchoolUser.Any(x => x.SchoolId == objSchoolPrincipalModel.SchoolId && x.UserEmailAddress == objSchoolPrincipalModel.EmailAddress && x.UserRole == "Principle"))
                    {
                        TempData["Message"] = "Email Address Already Exists.";
                        return RedirectToAction("GetSchoolPrincipleList", "School");
                    }

                    objSchoolPrincipalModel.IsActive = true;
                    objSchoolPrincipalModel.Id = Guid.NewGuid();
                    TempData["Message"] = "School Prinipal Added Successfully.";
                }
                else
                {
                    objSchoolPrincipalModel.IsActive = true;
                    TempData["Message"] = "School Principal Updated Successfully.";
                }
                int result = 0;
                result = DbContext.SP_DC_SchoolUser(objSchoolPrincipalModel.Id, objSchoolPrincipalModel.SchoolId, objSchoolPrincipalModel.FirstName, objSchoolPrincipalModel.LastName, objSchoolPrincipalModel.EmailAddress, "", objSchoolPrincipalModel.Password, objSchoolPrincipalModel.Image, "Principle", "", DateTime.Now, DateTime.Now, objSchoolPrincipalModel.IsActive);
                //---------------Mail Send functionality------------
                if (result > 0)
                {
                    string emailMsg = "Hi, this is to inform you your school admin credential are Username " + objSchoolPrincipalModel.EmailAddress + " password " + objSchoolPrincipalModel.Password + ".";
                    EmailHelper.SendEmail(objSchoolPrincipalModel.EmailAddress, "Admin Credentials", emailMsg);
                }
                //Mail Send functionality
            }
            catch
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetSchoolPrincipleList", "School");
        }
        #endregion

        #region Subject
        public ActionResult GetSubjectList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.CreateSubject> objOutput = new List<SchoolModel.CreateSubject>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetSubjectList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        public ActionResult AddSubject(Guid? Id)//CreateOrEditSubject
        {
            SchoolModel.CreateSubject objCreateSubject = new SchoolModel.CreateSubject();
            if (Id != null && Id != Guid.Empty)
            {
                var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == Id).SingleOrDefault();
                if (subject != null)
                {
                    objCreateSubject.SubjectId = subject.SubjectId;
                    objCreateSubject.SubjectName = subject.SubjectName;
                }
            }
            ViewBag.Message = t;
            return View(objCreateSubject);
        }
        [HttpPost]
        public ActionResult AddSubject(SchoolModel.CreateSubject objCreateSubject)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                if (objCreateSubject.SubjectId != null && objCreateSubject.SubjectId != Guid.Empty)
                {
                    var subject = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == objCreateSubject.SubjectId && x.SchoolId == schoolId).FirstOrDefault();
                    //Check subject name already exist
                    if (DbContext.tbl_DC_School_Subject.Where(x => x.SubjectName.ToLower() == objCreateSubject.SubjectName.ToLower() && x.SchoolId == schoolId && x.IsActive == true).Any())
                    {
                        TempData["Message"] = "Subject Aready Exist.";
                        return View(objCreateSubject);
                    }
                    if (subject != null)
                    {
                        subject.SubjectId = objCreateSubject.SubjectId;
                        subject.SubjectName = objCreateSubject.SubjectName;
                        DbContext.SaveChanges();
                    }
                    TempData["Message"] = "Subject Updated Successfully.";
                }
                else
                {
                    //Check subject name already exist
                    if (DbContext.tbl_DC_School_Subject.Where(x => x.SubjectName.ToLower() == objCreateSubject.SubjectName.ToLower() && x.SchoolId == schoolId && x.IsActive == true).Any())
                    {
                        TempData["Message"] = "Subject Aready Exist.";
                        return View(objCreateSubject);
                    }
                    //Else save subject
                    objCreateSubject.Id = Guid.NewGuid();
                    objCreateSubject.SchoolId = new Guid(Session["id"].ToString());
                    tbl_DC_School_Subject objtbl_DC_School_Subject = new tbl_DC_School_Subject();
                    objtbl_DC_School_Subject.SubjectId = objCreateSubject.Id;
                    objtbl_DC_School_Subject.SchoolId = objCreateSubject.SchoolId;
                    objtbl_DC_School_Subject.SubjectName = objCreateSubject.SubjectName;
                    objtbl_DC_School_Subject.CreatedDate = DateTime.Now;
                    objtbl_DC_School_Subject.IsActive = true;
                    var status = false;

                    DbContext.tbl_DC_School_Subject.Add(objtbl_DC_School_Subject);
                    var id = DbContext.SaveChanges();
                    if (id != null)
                    {
                        status = true;
                    }
                    TempData["Message"] = "Subject Added Successfully.";
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetSubjectList", "School");

        }

        public ActionResult DeleteSubject(Guid id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_Subject.Where(x => x.SubjectId == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }
            TempData["Message"] = "Assign Teacher Deleted Successfully.";
            return RedirectToAction("GetSubjectList", "School");
        }
        #endregion

        #region ExamType
        public ActionResult GetExamType()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.ExamTypeModel> objOutput = new List<SchoolModel.ExamTypeModel>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetExamTypeList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        public ActionResult ExamType(Guid? Id)
        {
            SchoolModel.ExamType objExamType = new SchoolModel.ExamType();

            if (Id != null && Id != Guid.Empty)
            {
                var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == Id).FirstOrDefault();
                if (examType != null)
                {
                    objExamType.ExamTypeId = examType.ExamTypeId;
                    objExamType.ExamTypenname = examType.ExamTypeName;
                }
            }
            ViewBag.Message = t;
            return View(objExamType);
        }

        [HttpPost]
        public ActionResult ExamType(SchoolModel.ExamType objExamType)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                //check exam type already exist
                if (DbContext.tbl_DC_School_ExamType.Any(x => x.ExamTypeName.ToLower() == objExamType.ExamTypenname.ToLower() && x.SchoolId == schoolId && x.IsActive == true))
                {
                    TempData["Message"] = "Exam Type Aready Exist.";
                    return View(objExamType);
                }
                //Edit Case                
                if (objExamType.ExamTypeId != null && objExamType.ExamTypeId != Guid.Empty)
                {
                    var examType = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == objExamType.ExamTypeId && x.SchoolId == schoolId).FirstOrDefault();
                    if (examType != null)
                    {
                        examType.ExamTypeId = objExamType.ExamTypeId;
                        examType.ExamTypeName = objExamType.ExamTypenname;

                        DbContext.SaveChanges();
                        TempData["Message"] = "Exam Type Updated Successfully.";
                    }
                }
                else
                {
                    tbl_DC_School_ExamType objExmam = new tbl_DC_School_ExamType();
                    //Else add record.
                    objExmam.ExamTypeId = Guid.NewGuid();
                    objExmam.SchoolId = new Guid(Session["id"].ToString());
                    objExmam.ExamTypeName = objExamType.ExamTypenname;
                    objExmam.IsActive = true;
                    objExmam.CreatedDate = DateTime.Now;
                    DbContext.tbl_DC_School_ExamType.Add(objExmam);
                    DbContext.SaveChanges();
                    TempData["Message"] = "Exam Type Added Successfully.";
                }

                //  DbContext();
            }
            catch (Exception ex)
            {
                TempData["Message"] = "There is some error please contact admin.";
            }
            ViewBag.Message = t;
            return RedirectToAction("GetExamType", "School");
        }

        public ActionResult DeleteExamType(Guid id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_ExamType.Where(x => x.ExamTypeId == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetExamType", "School");
        }




        #endregion

        #region Exam

        public ActionResult GetExam()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.CreateExamModel> objOutput = new List<SchoolModel.CreateExamModel>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetExamListList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        public ActionResult CreateOrEditCreateExam(Guid? Id)
        {
            SchoolModel.CreateExamModel objCreateExamModel = new SchoolModel.CreateExamModel();
            objCreateExamModel.DateofExam = DateTime.Today;
            Guid schoolId = new Guid(Session["id"].ToString());
            DbContext = new DigiChampsEntities();
            if (Id != null && Id != Guid.Empty)
            {
                var exam = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.ExamScheduleId == Id && x.SchoolId == schoolId && x.IsActive == true).FirstOrDefault();
                {
                    objCreateExamModel.Id = exam.ExamScheduleId;
                    //objCreateExamModel.ClassId = (Guid)exam.ClassId;
                    objCreateExamModel.Class_Id = (int)exam.Class_Id;
                    objCreateExamModel.DateofExam = (DateTime)exam.DateOfExam;
                    objCreateExamModel.SubjectId = (Guid)exam.SubjectId;
                    objCreateExamModel.ExamType = (Guid)exam.ExamTypeId;
                    objCreateExamModel.TimeSlot = exam.TimeSlot;
                    objCreateExamModel.StartTimeSlot = !string.IsNullOrEmpty(objCreateExamModel.TimeSlot) ? objCreateExamModel.TimeSlot.Split('-')[0].ToString() : string.Empty;
                    objCreateExamModel.EndTimeSlot = !string.IsNullOrEmpty(objCreateExamModel.TimeSlot) ? objCreateExamModel.TimeSlot.Split('-')[1].ToString() : string.Empty;


                }
            }
            //ViewBag.Class_Id = new SelectList(DbContext.tbl_DC_School_Class.Where(x => x.SchoolId==schoolId && x.IsActive == true), "ClassId", "ClassName");
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.ExamType_Id = new SelectList(DbContext.tbl_DC_School_ExamType.Where(x => x.IsActive == true && x.SchoolId == schoolId), "ExamTypeId", "ExamTypeName");
            ViewBag.Subject_Id = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.Subject_Id = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.Message = t;
            return View(objCreateExamModel);
        }
        [HttpPost]
        public ActionResult CreateOrEditCreateExam(SchoolModel.CreateExamModel objCreateExamModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                if (objCreateExamModel.Id != null && objCreateExamModel.Id != Guid.Empty)
                {
                    var exam = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.ExamScheduleId == objCreateExamModel.Id).FirstOrDefault();
                    {
                        exam.ExamScheduleId = objCreateExamModel.Id;
                        // exam.Class_Id = objCreateExamModel.Class_Id;
                        exam.DateOfExam = objCreateExamModel.StartDate;
                        exam.SubjectId = objCreateExamModel.SubjectId;
                        exam.ExamTypeId = objCreateExamModel.ExamType;

                        DbContext.SaveChanges();
                        TempData["ErrorMessage"] = "Exam Updated Successfully.";
                    }
                }
                else
                {
                    tbl_DC_School_ExamSchedule objexam = new tbl_DC_School_ExamSchedule();
                    //check exam already exist
                    var chk = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.Class_Id == objCreateExamModel.Class_Id && x.SchoolId == schoolId
                        && x.SubjectId == objCreateExamModel.SubjectId && x.ExamTypeId == objCreateExamModel.ExamType
                        && x.DateOfExam == objCreateExamModel.DateofExam && x.IsActive == true).ToList();
                    if (chk.Count > 0)
                    {

                        TempData["ErrorMessage"] = "Exam Type Aready Exist.";
                        return RedirectToAction("CreateOrEditCreateExam");

                    }

                    //objexam.ClassId = objCreateExamModel.ClassId;                    
                    //  objexam.se = objCreateExamModel.SectionId;
                    objexam.ExamScheduleId = Guid.NewGuid();
                    objexam.Class_Id = objCreateExamModel.Class_Id;
                    objexam.SchoolId = schoolId;
                    objexam.SubjectId = objCreateExamModel.SubjectId;
                    objexam.ExamTypeId = objCreateExamModel.ExamType;
                    objexam.DateOfExam = objCreateExamModel.DateofExam;
                    objexam.IsActive = true;
                    objexam.TimeSlot = objCreateExamModel.TimeSlot;
                    objexam.CreatedDate = DateTime.Now;
                    objexam.TotalMarks = objCreateExamModel.TotalMarks.ToString(); ;


                    objexam.ExamTypeId = objexam.ExamTypeId;
                    objexam.CreatedDate = DateTime.Now;
                    DbContext.tbl_DC_School_ExamSchedule.Add(objexam);
                    var id = DbContext.SaveChanges();
                    TempData["SuccessMessage"] = "Exam Added Successfully.";
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetExam", "School");
        }

        public ActionResult DeleteExam(Guid id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_ExamSchedule.Where(x => x.ExamScheduleId == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetExam", "School");
        }
        #endregion

        #region HomeWork
        public ActionResult GetHomeWorkList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.HomeWorkModel> objOutput = new List<SchoolModel.HomeWorkModel>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetHomeWorkList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        /// <summary>SubjectId
        /// Create home work form
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult AddHomeWork(Guid? HomeWorkId)//CreateOrEditHomeWork
        {
            SchoolModel.HomeWorkModel objHomeWorkModel = new SchoolModel.HomeWorkModel();
            Guid schoolId = new Guid(Session["id"].ToString());
            if (HomeWorkId != null && HomeWorkId != Guid.Empty)
            {
                var HomeworkDetail = DbContext.tbl_DC_School_Homework.Where(x => x.HomeworkId == HomeWorkId).SingleOrDefault();
                objHomeWorkModel.HomeworkId = HomeworkDetail.HomeworkId;
                objHomeWorkModel.SchoolId = HomeworkDetail.SchoolId;
                objHomeWorkModel.Class_Id = HomeworkDetail.Class_Id;
                objHomeWorkModel.SectionId = HomeworkDetail.SectionId;
                objHomeWorkModel.SubjectId = HomeworkDetail.SubjectId;
                objHomeWorkModel.HomeworkDetail = HomeworkDetail.HomeworkDetail;
                objHomeWorkModel.CreatedDate = HomeworkDetail.CreatedDate;
                objHomeWorkModel.DateOfHomework = HomeworkDetail.DateOfHomework;
                objHomeWorkModel.PeriodID = HomeworkDetail.PeriodID;
            }
            //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
            ViewBag.SubjectList = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.PeriodList = new SelectList(DbContext.tbl_DC_Period.Where(x => x.IsActive == true && x.SchoolId == schoolId), "Id", "Title");
            ViewBag.Message = t;
            return View(objHomeWorkModel);
        }
        [HttpPost]

        public ActionResult AddHomeWork(SchoolModel.HomeWorkModel objHomeWorkModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                if (objHomeWorkModel.HomeworkId != null && objHomeWorkModel.HomeworkId != Guid.Empty)
                {
                    var HomeWorkDetail = DbContext.tbl_DC_School_Homework.Where(x => x.HomeworkId == objHomeWorkModel.HomeworkId).FirstOrDefault();
                    if (HomeWorkDetail != null)
                    {
                        HomeWorkDetail.Class_Id = objHomeWorkModel.Class_Id;
                        HomeWorkDetail.SectionId = objHomeWorkModel.SectionId;
                        HomeWorkDetail.SubjectId = objHomeWorkModel.SubjectId;
                        HomeWorkDetail.DateOfHomework = objHomeWorkModel.DateOfHomework;
                        HomeWorkDetail.PeriodID = objHomeWorkModel.PeriodID;
                        HomeWorkDetail.HomeworkDetail = objHomeWorkModel.HomeworkDetail;

                        DbContext.SaveChanges();
                        TempData["Message"] = "Home Work Updated Successfully.";
                    }
                }
                else
                {

                    //Check duplicate
                    if (DbContext.tbl_DC_School_Homework.Any(x => x.SchoolId == schoolId && x.SectionId == objHomeWorkModel.SectionId && x.SubjectId == objHomeWorkModel.SubjectId && x.Class_Id == objHomeWorkModel.Class_Id && x.PeriodID == objHomeWorkModel.PeriodID))
                    {
                        TempData["Message"] = "Home Work Aready Exist.";
                        //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
                        ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
                        ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
                        ViewBag.SubjectList = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
                        return View(objHomeWorkModel);
                    }

                    tbl_DC_School_Homework tblSchoolHomeWork = new tbl_DC_School_Homework();
                    tblSchoolHomeWork.HomeworkId = Guid.NewGuid();
                    //assignTeacher.ClassId = assignTeacherModel.ClassId;

                    tblSchoolHomeWork.SchoolId = schoolId;
                    tblSchoolHomeWork.Class_Id = objHomeWorkModel.Class_Id;
                    tblSchoolHomeWork.SubjectId = objHomeWorkModel.SubjectId;
                    tblSchoolHomeWork.SectionId = objHomeWorkModel.SectionId;
                    tblSchoolHomeWork.DateOfHomework = objHomeWorkModel.DateOfHomework;
                    tblSchoolHomeWork.PeriodID = objHomeWorkModel.PeriodID;
                    tblSchoolHomeWork.CreatedDate = DateTime.Now;
                    tblSchoolHomeWork.IsActive = true;
                    tblSchoolHomeWork.HomeworkDetail = objHomeWorkModel.HomeworkDetail;

                    DbContext.tbl_DC_School_Homework.Add(tblSchoolHomeWork);
                    var id = DbContext.SaveChanges();
                    TempData["Message"] = "Home Work Added Successfully.";
                }
                return RedirectToAction("GetHomeWorkList", "School");
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetHomeWorkList", "School");
        }

        public ActionResult DeleteHomeWork(Guid HomeWorkId)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_Homework.Where(x => x.HomeworkId == HomeWorkId && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetHomeWorkList", "School");
        }
        #endregion

        #region StudyMaterial
        public ActionResult GetStudyMaterialList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.StudyMaterialModel> objOutput = new List<SchoolModel.StudyMaterialModel>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetStudyMaterialList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        /// <summary>
        /// Insert Exam type
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>




        public ActionResult AddStudyMaterial(Guid? StudyMaterialId)//CreateOrEditStudyMaterial
        {
            SchoolModel.StudyMaterialModel objStudyMaterialModel = new SchoolModel.StudyMaterialModel();
            Guid schoolId = new Guid(Session["id"].ToString());
            if (StudyMaterialId != null && StudyMaterialId != Guid.Empty)
            {
                var StudyMaterialDetail = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.StudyMaterialId == StudyMaterialId).FirstOrDefault();
                if (StudyMaterialDetail != null)
                {
                    //objStudyMaterialModel.SchoolId = schoolId;
                    objStudyMaterialModel.Id = (Guid)StudyMaterialDetail.StudyMaterialId;
                    objStudyMaterialModel.Class_Id = (int)StudyMaterialDetail.Class_Id;
                    objStudyMaterialModel.SubjectId = (Guid)StudyMaterialDetail.SubjectId;
                    objStudyMaterialModel.Topic = StudyMaterialDetail.Topic;
                    objStudyMaterialModel.FilePath = StudyMaterialDetail.FilePath;
                    objStudyMaterialModel.FileType = StudyMaterialDetail.FileType;
                    objStudyMaterialModel.Title = StudyMaterialDetail.Title;
                    objStudyMaterialModel.FileName = !string.IsNullOrEmpty(StudyMaterialDetail.FilePath) ? Path.GetFileName(StudyMaterialDetail.FilePath) : "";
                    objStudyMaterialModel.MaterialText = StudyMaterialDetail.StudyMaterialTxt;
                    objStudyMaterialModel.MaterialType = string.IsNullOrEmpty(StudyMaterialDetail.StudyMaterialTxt) ? "file" : "text";
                    objStudyMaterialModel.IsActive = true;
                }
            }
            //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.Subject_Id = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.Message = t;
            return View(objStudyMaterialModel);
        }


        [HttpPost]
        public ActionResult AddStudyMaterial(SchoolModel.StudyMaterialModel objStudyMaterialModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                objStudyMaterialModel.IsActive = true;
                //Edit Case
                if (objStudyMaterialModel.Id != Guid.Empty && objStudyMaterialModel.Id != null)
                {
                    var StudyMaterialDetail = DbContext.tbl_DC_School_StudyMaterial.
                        Where(x => x.StudyMaterialId == objStudyMaterialModel.Id).FirstOrDefault();
                    if (StudyMaterialDetail != null)
                    {
                        StudyMaterialDetail.Class_Id = objStudyMaterialModel.Class_Id;
                        StudyMaterialDetail.SubjectId = objStudyMaterialModel.SubjectId;
                        StudyMaterialDetail.Topic = objStudyMaterialModel.Topic;
                        StudyMaterialDetail.Title = objStudyMaterialModel.Title;
                        if (!string.IsNullOrEmpty(objStudyMaterialModel.MaterialType)
                            && objStudyMaterialModel.MaterialType == "text")
                        {
                            StudyMaterialDetail.StudyMaterialTxt = objStudyMaterialModel.MaterialText;
                        }
                        else
                        {
                            #region UploadFile
                            FileHelper fileHelper = new FileHelper();
                            string browserType = Request.Browser.Browser.ToUpper();
                            UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "StudyMaterial", browserType);
                            objStudyMaterialModel.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                            objStudyMaterialModel.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;

                            if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName))
                                StudyMaterialDetail.FilePath = objStudyMaterialModel.FileName;
                            else
                                StudyMaterialDetail.FilePath = objStudyMaterialModel.FilePath;

                            string p = StudyMaterialDetail.FilePath;
                            string e = Path.GetExtension(p);
                            StudyMaterialDetail.FileType = e;
                            #endregion
                        }
                        DbContext.SaveChanges();
                        TempData["Message"] = "Study Material Updated Successfully.";
                    }


                }
                else
                {
                    //Add Case
                    //Check if Topic already exist for same class.
                    if (DbContext.tbl_DC_School_StudyMaterial.Any(x => x.SchoolId == schoolId && x.Class_Id == objStudyMaterialModel.Class_Id && x.SubjectId == objStudyMaterialModel.SubjectId && x.Topic == objStudyMaterialModel.Topic))
                    {
                        TempData["Message"] = "Study Material Aready Added";
                        //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
                        ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
                        ViewBag.Subject_Id = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
                        return View(objStudyMaterialModel);
                    }

                    tbl_DC_School_StudyMaterial objstudy = new tbl_DC_School_StudyMaterial();
                    objstudy.StudyMaterialId = Guid.NewGuid();
                    objstudy.SchoolId = schoolId;
                    objstudy.Class_Id = objStudyMaterialModel.Class_Id;
                    objstudy.SubjectId = objStudyMaterialModel.SubjectId;
                    objstudy.Topic = objStudyMaterialModel.Topic;
                    objstudy.Title = objStudyMaterialModel.Title;
                    objstudy.IsActive = true;
                    if (!string.IsNullOrEmpty(objStudyMaterialModel.MaterialType) && objStudyMaterialModel.MaterialType == "text")
                    {
                        objstudy.StudyMaterialTxt = objStudyMaterialModel.MaterialText;
                    }
                    else
                    {
                        #region UploadFile
                        FileHelper fileHelper = new FileHelper();
                        string browserType = Request.Browser.Browser.ToUpper();
                        UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "StudyMaterial", browserType);
                        objStudyMaterialModel.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                        objStudyMaterialModel.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;

                        if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName))
                            objStudyMaterialModel.FilePath = objStudyMaterialModel.FileName;
                        else
                            objStudyMaterialModel.FilePath = objStudyMaterialModel.FilePath;

                        objstudy.FilePath = objStudyMaterialModel.FilePath;
                        objstudy.FileType = "PDF";
                        string p = objStudyMaterialModel.FilePath;
                        string e = Path.GetExtension(p);
                        objStudyMaterialModel.FileType = e;
                        #endregion
                    }
                    objstudy.CreatedDate = DateTime.Now;
                    DbContext.tbl_DC_School_StudyMaterial.Add(objstudy);
                    DbContext.SaveChanges();

                }

                #region UploadFileOld
                //HttpFileCollectionBase files = Request.Files;
                //string fname = null;

                //for (int i = 0; i < files.Count; i++)
                //{
                //    HttpPostedFileBase file = files[i];

                //    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                //    {
                //        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                //        fname = testfiles[testfiles.Length - 1];
                //    }
                //    else
                //    {
                //        fname = file.FileName;
                //    }




                //    string path = System.Web.HttpContext.Current.Server.MapPath("~/Upload/") + "School\\StudyMaterial";
                //    string subPath = "~/Upload/School/StudyMaterial/";
                //    bool exists = System.IO.Directory.Exists(path);

                //    if (!exists)
                //        System.IO.Directory.CreateDirectory(path);
                //    // objimage.Add(Path.Combine("/Upload/" + "Product/" + productId + "/" + fname));

                //    file.SaveAs(Path.Combine(path, fname));
                //    objStudyMaterialModel.Image = subPath + fname;

                //    //Stream strm = file.InputStream;
                //}
                #endregion


            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetStudyMaterialList", "School");
        }

        public ActionResult DeleteStudyMaterial(Guid StudyMaterialId)
        {
            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_StudyMaterial.Where(x => x.StudyMaterialId == StudyMaterialId).FirstOrDefault();
                if (result != null)
                {
                    // Set status false for delete
                    result.IsActive = false;
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("GetStudyMaterialList", "School");
        }
        #endregion

        #region TopperWay
        public ActionResult GetToppersWay()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.ToppersWayModel> objOutput = new List<SchoolModel.ToppersWayModel>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetToppersWay(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }
        public ActionResult AddTopperWay(Guid? Id)//CreateOrEditToppersWay
        {
            SchoolModel.ToppersWayModel objToppersWayModel = new SchoolModel.ToppersWayModel();
            Guid schoolId = new Guid(Session["id"].ToString());
            if (Id != null && Id != Guid.Empty)
            {
                var TopperWayDetail = DbContext.tbl_DC_ToppersWay.Where(x => x.ToppersId == Id).FirstOrDefault();
                if (TopperWayDetail != null)
                {
                    objToppersWayModel.Class_Id = (int)TopperWayDetail.Class_Id;
                    objToppersWayModel.Class_Id = (int)TopperWayDetail.Class_Id;
                    objToppersWayModel.SectionId = (Guid)TopperWayDetail.SectionId;
                    objToppersWayModel.path = TopperWayDetail.FileURL;
                    objToppersWayModel.FileName = TopperWayDetail.FileType;
                    objToppersWayModel.Title = TopperWayDetail.Title;

                }
            }
            //ViewBag.Class_Id = new SelectList(DbContext.tbl_DC_School_Class.Where(x => x.IsActive == true), "ClassId", "ClassName");            
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.Message = t;
            return View(objToppersWayModel);
        }



        [HttpPost]
        public ActionResult AddTopperWay(SchoolModel.ToppersWayModel objToppersWayModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                //SchoolModel.StudyMaterialModel objStudyMaterialModel = new SchoolModel.StudyMaterialModel();

                if (objToppersWayModel.Id != Guid.Empty && objToppersWayModel.Id != null)
                {
                    var toppersWayDetail = DbContext.tbl_DC_ToppersWay.Where(x => x.ToppersId == objToppersWayModel.Id).FirstOrDefault();
                    if (toppersWayDetail != null)
                    {
                        toppersWayDetail.Class_Id = objToppersWayModel.Class_Id;
                        toppersWayDetail.SectionId = (Guid)objToppersWayModel.SectionId;
                        toppersWayDetail.FileURL = objToppersWayModel.path;
                        toppersWayDetail.FileType = objToppersWayModel.FileName;
                        toppersWayDetail.Title = objToppersWayModel.Title;

                        #region UploadFile
                        if (Request.Files[0] != null && Request.Files[0].ContentLength > 0)
                        {
                            FileHelper fileHelper = new FileHelper();
                            string browserType = Request.Browser.Browser.ToUpper();
                            UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "ToppersWay", browserType);
                            if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName) && !string.IsNullOrEmpty(uploadFileDetailModel.ImagePath))
                            {
                                toppersWayDetail.FileURL = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                                toppersWayDetail.FileType = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImageName : string.Empty;
                            }
                            else
                            {
                                toppersWayDetail.FileURL = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                                toppersWayDetail.FileType = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImageName : string.Empty;
                            }
                        }
                        //objToppersWayModel.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;

                        #endregion

                        DbContext.SaveChanges();
                        TempData["Message"] = "Toppers Way Updated Successfully.";
                    }


                }
                else
                {
                    //Add Case
                    //Check if Topic already exist for same class.
                    /*if (DbContext.tbl_DC_ToppersWay.Any(x => x.SchoolId == schoolId && x.Class_Id == objToppersWayModel.Class_Id && x.SectionId == objToppersWayModel.SectionId && x.IsActive == true))
                    {
                        TempData["Message"] = "Toppers Way Aready Added.";
                        //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
                        ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
                        return View(objToppersWayModel);
                    }*/

                    tbl_DC_ToppersWay tblTopperWay = new tbl_DC_ToppersWay();
                    tblTopperWay.ToppersId = Guid.NewGuid();
                    tblTopperWay.SchoolId = schoolId;
                    tblTopperWay.Title = objToppersWayModel.Title;

                    tblTopperWay.Class_Id = objToppersWayModel.Class_Id;
                    tblTopperWay.SectionId = objToppersWayModel.SectionId;
                    tblTopperWay.IsActive = true;
                    #region UploadFile
                    if (Request.Files[0] != null && Request.Files[0].ContentLength > 0)
                    {
                        FileHelper fileHelper = new FileHelper();
                        string browserType = Request.Browser.Browser.ToUpper();
                        UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "StudyMaterial", browserType);
                        if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName) && !string.IsNullOrEmpty(uploadFileDetailModel.ImagePath))
                        {
                            tblTopperWay.FileURL = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                            tblTopperWay.FileType = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImageName : string.Empty;
                        }
                        else
                        {
                            tblTopperWay.FileURL = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                            tblTopperWay.FileType = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImageName : string.Empty;
                        }
                    }
                    #endregion

                    tblTopperWay.CreatedDate = DateTime.Now;
                    DbContext.tbl_DC_ToppersWay.Add(tblTopperWay);
                    DbContext.SaveChanges();

                }
            }
            catch
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetToppersWay", "School");
        }

        public ActionResult DeleteToppersWay(Guid id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_ToppersWay.Where(x => x.ToppersId == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetToppersWay", "School");
        }

        #endregion

        #region AssignTeacher
        public ActionResult GetAllAssignedTeacherList()
        {
            List<DigiChamps.Models.SchoolModel.AssignTeacher> objAssignTeacherList = new List<SchoolModel.AssignTeacher>();
            DigiChamps.Models.SchoolModel.AssignTeacher assignTeacher = new DigiChamps.Models.SchoolModel.AssignTeacher();
            Guid schoolId = new Guid(Session["id"].ToString());
            var assignedTeacherList = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.SchoolId == schoolId && x.IsActive == true).OrderByDescending(x => x.CreatedDate).ToList();
            if (assignedTeacherList.Any())
            {
                foreach (var item in assignedTeacherList)
                {
                    assignTeacher = new DigiChamps.Models.SchoolModel.AssignTeacher();
                    assignTeacher.AssignmentId = item.Id;


                    assignTeacher.TeacherName = DbContext.tbl_DC_SchoolUser.Where(a => a.UserId == item.TeacherId).FirstOrDefault().UserFirstname + " " + DbContext.tbl_DC_SchoolUser.Where(a => a.UserId == item.TeacherId).FirstOrDefault().UserLastname;
                    assignTeacher.ClassName = item.Class_Id != null && DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id).Any() ? DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id).FirstOrDefault().Class_Name : string.Empty;
                    assignTeacher.SectionName = item.SectionId != null ? DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == item.SectionId).FirstOrDefault().SectionName : "";
                    assignTeacher.SubjectName = item.SubjectId != null ? DbContext.tbl_DC_School_Subject.Where(a => a.SubjectId == item.SubjectId).FirstOrDefault().SubjectName : "";
                    objAssignTeacherList.Add(assignTeacher);
                }
            }
            ViewBag.Message = t;
            return View(objAssignTeacherList);
        }
        public ActionResult AssignedTeacher(Guid? AssignTeacherId)
        {
            DigiChamps.Models.SchoolModel.AssignTeacher assignTeacher = new DigiChamps.Models.SchoolModel.AssignTeacher();
            Guid schoolId = new Guid(Session["id"].ToString());

            if (AssignTeacherId != null && AssignTeacherId != Guid.Empty)
            {
                var assignTeacherDetails = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.Id == AssignTeacherId).FirstOrDefault();
                if (assignTeacherDetails != null)
                {
                    assignTeacher.AssignmentId = assignTeacherDetails.Id;
                    assignTeacher.Class_Id = assignTeacherDetails.Class_Id;
                    assignTeacher.SectionId = (Guid)assignTeacherDetails.SectionId;
                    assignTeacher.SubjectId = (Guid)assignTeacherDetails.SubjectId;
                    assignTeacher.AssignmentId = assignTeacherDetails.Id;
                    assignTeacher.TeacherId = (Guid)assignTeacherDetails.TeacherId;
                }
            }
            ViewBag.TeacherList = new SelectList(DbContext.tbl_DC_SchoolUser.Where(x => x.IsActive == true && x.SchoolId == schoolId && x.UserRole == "Teacher"), "UserId", "UserFirstname");
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
            ViewBag.SubjectList = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.Message = t;
            return View(assignTeacher);
        }

        [HttpPost]
        public ActionResult GetsectionList(string ClassId)
        {
            if (ClassId != "")
            {
                int _classId = Convert.ToInt32(ClassId);
                Guid schoolId = new Guid(Session["id"].ToString());
                //Here I'll bind the list of cities corresponding to selected state's state id  
                List<tbl_DC_Class_Section> lstsection = new List<tbl_DC_Class_Section>();
                int stateiD = Convert.ToInt32(ClassId);

                lstsection = (DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == _classId && x.School_Id == schoolId)).ToList<tbl_DC_Class_Section>();

                return this.Json(
              new
              {
                  Result = (from obj in lstsection select new { SectionId = obj.SectionId, SectionName = obj.SectionName })
              }
              , JsonRequestBehavior.AllowGet
              );
            }
            else
            {
                return this.Json(new { Result = string.Empty }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AssignedTeacher(DigiChamps.Models.SchoolModel.AssignTeacher assignTeacherModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                //Check duplicate
                if (DbContext.tbl_DC_School_AssingTeacher.Any(x => x.SchoolId == schoolId && x.SectionId == assignTeacherModel.SectionId && x.SubjectId == assignTeacherModel.SubjectId && x.IsActive == true))
                {
                    TempData["Message"] = "Teacher Aready assign to same class and section.";
                    ViewBag.TeacherList = new SelectList(DbContext.tbl_DC_SchoolUser.Where(x => x.IsActive == true && x.SchoolId == schoolId && x.UserRole == "Teacher"), "UserId", "UserFirstname");
                    ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
                    ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
                    ViewBag.SubjectList = new SelectList(DbContext.tbl_DC_School_Subject.Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
                    return View(assignTeacherModel);
                }
                if (assignTeacherModel.AssignmentId != null && assignTeacherModel.AssignmentId != Guid.Empty)
                {
                    var AssignTeacherDetail = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.Id == assignTeacherModel.AssignmentId).FirstOrDefault();
                    if (AssignTeacherDetail != null)
                    {
                        //AssignTeacherDetail.ClassId = assignTeacherModel.ClassId;
                        AssignTeacherDetail.Class_Id = assignTeacherModel.Class_Id;
                        AssignTeacherDetail.SectionId = assignTeacherModel.SectionId;
                        AssignTeacherDetail.SubjectId = assignTeacherModel.SubjectId;
                        AssignTeacherDetail.TeacherId = assignTeacherModel.TeacherId;
                        DbContext.SaveChanges();
                        TempData["Message"] = "Assign Teacher Updated Successfully.";
                    }
                }
                else
                {
                    tbl_DC_School_AssingTeacher assignTeacher = new tbl_DC_School_AssingTeacher();
                    assignTeacher.Id = Guid.NewGuid();
                    //assignTeacher.ClassId = assignTeacherModel.ClassId;

                    assignTeacher.SchoolId = schoolId;
                    assignTeacher.Class_Id = assignTeacherModel.Class_Id;
                    assignTeacher.SubjectId = assignTeacherModel.SubjectId;
                    assignTeacher.SectionId = assignTeacherModel.SectionId;
                    assignTeacher.TeacherId = assignTeacherModel.TeacherId;
                    assignTeacher.CreatedDate = DateTime.Now;
                    assignTeacher.IsActive = true;

                    DbContext.tbl_DC_School_AssingTeacher.Add(assignTeacher);
                    var id = DbContext.SaveChanges();
                    TempData["Message"] = "Assign Teacher Added Successfully.";
                }
                return RedirectToAction("GetAllAssignedTeacherList", "School");
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return View();
        }

        public ActionResult DeleteAssignTeacher(Guid? AssignTeacherId)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_AssingTeacher.Where(x => x.Id == AssignTeacherId && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }
            TempData["Message"] = "Assign Teacher Deleted Successfully.";
            return RedirectToAction("GetAllAssignedTeacherList", "School");
        }
        #endregion

        #region Message
        public ActionResult AddSchoolMessage(Guid? MessageId)//CreateOrEditMessageCreation
        {
            SchoolModel.MessageCreation objMessageCreation = new SchoolModel.MessageCreation();
            Guid schoolId = new Guid(Session["id"].ToString());
            if (MessageId != null && MessageId != Guid.Empty)
            {
                var MessageDetail = DbContext.tbl_DC_School_MessageCreation.Where(x => x.MessageId == MessageId).FirstOrDefault();
                if (MessageDetail != null)
                {
                    objMessageCreation.MessageId = MessageDetail.MessageId;
                    objMessageCreation.SchoolId = MessageDetail.SchoolId;
                    objMessageCreation.Class_Id = MessageDetail.Class_Id;
                    objMessageCreation.SectionId = (Guid)MessageDetail.SectionId;
                    objMessageCreation.MassageDisplayDate = MessageDetail.MassageDisplayDate;
                    objMessageCreation.MassageText = MessageDetail.MassageText;
                    objMessageCreation.FileName = MessageDetail.FileName;
                    objMessageCreation.FilePath = MessageDetail.FilePath;
                    objMessageCreation.Title = MessageDetail.Title;
                    objMessageCreation.CreatedDate = MessageDetail.CreatedDate;
                    objMessageCreation.IsActive = MessageDetail.IsActive;
                }
            }
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            //new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
            ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
            //ViewBag.Section_Id = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
            ViewBag.Message = t;
            return View(objMessageCreation);
        }

        [HttpPost]
        public ActionResult AddSchoolMessage(SchoolModel.MessageCreation objMessageCreation)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                if (objMessageCreation.MessageId != null && objMessageCreation.MessageId != Guid.Empty)
                {
                    var MessageDetail = DbContext.tbl_DC_School_MessageCreation.Where(x => x.MessageId == objMessageCreation.MessageId).FirstOrDefault();
                    if (MessageDetail != null)
                    {

                        MessageDetail.Class_Id = objMessageCreation.Class_Id;
                        MessageDetail.SectionId = (Guid)objMessageCreation.SectionId;
                        // MessageDetail.MassageDisplayDate = objMessageCreation.MassageDisplayDate;
                        MessageDetail.MassageText = objMessageCreation.MassageText;
                        MessageDetail.FileName = objMessageCreation.FileName;
                        MessageDetail.FilePath = objMessageCreation.FilePath;
                        MessageDetail.Title = objMessageCreation.Title;
                        MessageDetail.IsActive = true;
                        #region UploadFile
                        FileHelper fileHelper = new FileHelper();
                        string browserType = Request.Browser.Browser.ToUpper();
                        if (Request.Files[0] != null && Request.Files[0].ContentLength > 0)
                        {
                            UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "Notice", browserType);
                            if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName) && !string.IsNullOrEmpty(uploadFileDetailModel.ImagePath))
                            {
                                MessageDetail.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                                MessageDetail.FileName = uploadFileDetailModel.ImageName;
                            }
                            else
                            {
                                MessageDetail.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                                MessageDetail.FileName = uploadFileDetailModel.VideoName;
                            }
                        }
                        //objmsg.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoName : string.Empty;
                        //objmsg.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoPath) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                        #endregion
                        DbContext.SaveChanges();
                        TempData["Message"] = "Message Updated Successfully.";
                    }
                }
                else
                {
                    /* //Check duplicate
                     if (DbContext.tbl_DC_School_MessageCreation.Any(x => x.SchoolId == schoolId && x.SectionId == objMessageCreation.SectionId && x.Class_Id == objMessageCreation.Class_Id))
                     {
                         TempData["Message"] = "Message Aready Exist.";
                         //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
                         ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
                         ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true), "SectionId", "SectionName");
                         return View(objMessageCreation);
                     }
                     */

                    tbl_DC_School_MessageCreation objmsg = new tbl_DC_School_MessageCreation();
                    objmsg.MessageId = Guid.NewGuid();
                    objmsg.SchoolId = schoolId;
                    objmsg.SectionId = (Guid)objMessageCreation.SectionId;
                    // objmsg.MassageDisplayDate = objMessageCreation.MassageDisplayDate;
                    objmsg.Class_Id = objMessageCreation.Class_Id;
                    objmsg.MassageText = objMessageCreation.MassageText;
                    objmsg.FileName = objMessageCreation.FileName;
                    objmsg.FilePath = objMessageCreation.FilePath;
                    objmsg.Title = objMessageCreation.Title;
                    objmsg.CreatedDate = DateTime.Now;
                    objmsg.IsActive = true;
                    objmsg.IsDeleted = false;

                    #region UploadFile
                    if (Request.Files[0] != null && Request.Files[0].ContentLength > 0)
                    {
                        FileHelper fileHelper = new FileHelper();
                        string browserType = Request.Browser.Browser.ToUpper();
                        UploadFileDetailModel uploadFileDetailModel = fileHelper.UploadDoc(Request.Files, "School", schoolId.ToString(), "Notice", browserType);
                        if (!string.IsNullOrEmpty(uploadFileDetailModel.ImageName) && !string.IsNullOrEmpty(uploadFileDetailModel.ImagePath))
                        {
                            objmsg.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.ImageName) ? uploadFileDetailModel.ImagePath + "/" + uploadFileDetailModel.ImageName : string.Empty;
                            objmsg.FileName = uploadFileDetailModel.ImageName;
                        }
                        else
                        {
                            objmsg.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                            objmsg.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoName : string.Empty;
                        }
                    }
                    //objmsg.FileName = !string.IsNullOrEmpty(uploadFileDetailModel.VideoName) ? uploadFileDetailModel.VideoName : string.Empty;
                    //objmsg.FilePath = !string.IsNullOrEmpty(uploadFileDetailModel.VideoPath) ? uploadFileDetailModel.VideoPath + "/" + uploadFileDetailModel.VideoName : string.Empty;
                    #endregion
                    DbContext.tbl_DC_School_MessageCreation.Add(objmsg);
                    var id = DbContext.SaveChanges();
                    TempData["Message"] = "Message Added Successfully.";
                }
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetMessageList", "School");
        }

        public ActionResult GetMessageList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.MessageCreation> objOutput = new List<SchoolModel.MessageCreation>();
            SchoolModel.InputModel objInput = new SchoolModel.InputModel();
            objInput.SchoolId = new Guid(Session["id"].ToString());
            objOutput = obj.GetMessageList(objInput);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }

        public ActionResult DeleteSchoolMessage(Guid MessageId)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_School_MessageCreation.Where(x => x.MessageId == MessageId && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetMessageList", "School");
        }

        #endregion

        #region TimeTable
        public ActionResult GetTimeTableList()
        {
            List<DigiChamps.Models.SchoolModel.TimeTableModel> timeTableList = new List<SchoolModel.TimeTableModel>();
            DigiChamps.Models.SchoolModel.TimeTableModel timeTableModel = new DigiChamps.Models.SchoolModel.TimeTableModel();
            Guid schoolId = new Guid(Session["id"].ToString());
            var timetableList = DbContext.tbl_DC_Shool_TimeTable.Where(x => x.SchoolId == schoolId && x.IsActive == true).ToList();
            if (timetableList.Any())
            {
                foreach (var item in timetableList)
                {
                    timeTableModel = new DigiChamps.Models.SchoolModel.TimeTableModel();
                    timeTableModel.TimeTableId = item.TimeTableId;
                    timeTableModel.ClassName = item.Class_Id != null && DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id).Any() ? DbContext.tbl_DC_Class.Where(x => x.Class_Id == item.Class_Id).FirstOrDefault().Class_Name : string.Empty;
                    timeTableModel.SectionName = item.SectionId != null ? DbContext.tbl_DC_Class_Section.Where(a => a.SectionId == item.SectionId).FirstOrDefault().SectionName : "";
                    timeTableModel.SubjectName = DbContext.tbl_DC_School_Subject.Where(a => a.SubjectId == item.SubjectId).FirstOrDefault().SubjectName;
                    timeTableModel.PeriodName = DbContext.tbl_DC_Period.Where(a => a.Id == item.PeriodId).FirstOrDefault().Title;
                    if (Session["id"].ToString().Equals("ce2ef645-0069-4da9-a987-1ab3566ee7a0"))
                        timeTableModel.Day = item.Day;
                    else
                        timeTableModel.Day = Convert.ToString(EnumHelper<WeekDaysEnum>.ParseEnum(item.Day));

                    timeTableList.Add(timeTableModel);
                }
            }
            ViewBag.Message = t;
            return View(timeTableList);
        }
        public ActionResult AddTimeTable(Guid? TimeTableId)
        {
            DigiChamps.Models.SchoolModel.TimeTableModel timeTableModel = new DigiChamps.Models.SchoolModel.TimeTableModel();
            Guid schoolId = new Guid(Session["id"].ToString());

            if (TimeTableId != null && TimeTableId != Guid.Empty)
            {
                var timeTableDetail = DbContext.tbl_DC_Shool_TimeTable.Where(x => x.TimeTableId == TimeTableId).FirstOrDefault();
                if (timeTableDetail != null)
                {
                    timeTableModel.TimeTableId = timeTableDetail.TimeTableId;
                    timeTableModel.Class_Id = timeTableDetail.Class_Id;
                    timeTableModel.SectionId = (Guid)timeTableDetail.SectionId;
                    timeTableModel.SubjectId = (Guid)timeTableDetail.SubjectId;
                    timeTableModel.PeriodId = timeTableDetail.PeriodId;
                    timeTableModel.Day = timeTableDetail.Day;
                }
            }
            //ViewBag.ClassList = new SelectList(DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Class_Id", "Class_Name");
            ViewBag.ClassList = new SelectList(GetClassListDropDown(), "Value", "Text");
            ViewBag.SectionList = new SelectList(DbContext.tbl_DC_Class_Section.
                Where(x => x.IsActive == true), "SectionId", "SectionName");
            ViewBag.SubjectList = new SelectList(DbContext.tbl_DC_School_Subject.
                Where(x => x.IsActive == true && x.SchoolId == schoolId), "SubjectId", "SubjectName");
            ViewBag.PeriodList = new SelectList(DbContext.tbl_DC_Period.
                Where(x => x.IsActive == true && x.SchoolId == schoolId), "Id", "Title");
            if (Session["id"].ToString().Equals("ce2ef645-0069-4da9-a987-1ab3566ee7a0"))
                ViewBag.DayList = new SelectList(CommonHelper.GetEnumToList<WeekDaysEnum>().ToList(), "Key", "Key");
            else
                ViewBag.DayList = new SelectList(CommonHelper.GetEnumToList<WeekDaysEnum>().ToList(), "Key", "Name");
            ViewBag.Message = t;
            return View(timeTableModel);
        }
        [HttpPost]
        public ActionResult AddTimeTable(DigiChamps.Models.SchoolModel.TimeTableModel timeTableModel)
        {
            try
            {
                Guid schoolId = new Guid(Session["id"].ToString());
                if (timeTableModel.TimeTableId != null && timeTableModel.TimeTableId != Guid.Empty)
                {
                    var timeTableDetail = DbContext.tbl_DC_Shool_TimeTable.Where(x => x.TimeTableId == timeTableModel.TimeTableId).FirstOrDefault();
                    if (timeTableDetail != null)
                    {
                        timeTableDetail.TimeTableId = timeTableModel.TimeTableId;
                        timeTableDetail.Class_Id = timeTableModel.Class_Id;
                        timeTableDetail.SectionId = (Guid)timeTableModel.SectionId;
                        timeTableDetail.SubjectId = (Guid)timeTableModel.SubjectId;
                        timeTableDetail.PeriodId = timeTableModel.PeriodId;
                        timeTableDetail.Day = timeTableModel.Day;
                        DbContext.SaveChanges();
                        TempData["Message"] = "TimeTable Updated Successfully.";
                    }
                }
                else
                {

                    //Check duplicate
                    if (DbContext.tbl_DC_Shool_TimeTable.Any(x => x.SchoolId == schoolId &&
                        x.Class_Id == timeTableModel.Class_Id && x.SectionId == timeTableModel.SectionId
                        && x.SubjectId == timeTableModel.SubjectId && x.Day == timeTableModel.Day))
                    {
                        TempData["Message"] = "TimeTable already exist.";
                        return View(timeTableModel);
                    }

                    tbl_DC_Shool_TimeTable tblDCShoolTimeTable = new tbl_DC_Shool_TimeTable();
                    tblDCShoolTimeTable.TimeTableId = Guid.NewGuid();


                    tblDCShoolTimeTable.SchoolId = schoolId;
                    tblDCShoolTimeTable.Class_Id = timeTableModel.Class_Id;
                    tblDCShoolTimeTable.SubjectId = timeTableModel.SubjectId;
                    tblDCShoolTimeTable.SectionId = timeTableModel.SectionId;
                    tblDCShoolTimeTable.Day = timeTableModel.Day;
                    tblDCShoolTimeTable.PeriodId = timeTableModel.PeriodId;
                    tblDCShoolTimeTable.CreatedDate = DateTime.Now;
                    tblDCShoolTimeTable.IsActive = true;

                    DbContext.tbl_DC_Shool_TimeTable.Add(tblDCShoolTimeTable);
                    var id = DbContext.SaveChanges();
                    TempData["Message"] = "TimeTable Added Successfully.";
                }
                return RedirectToAction("GetTimeTableList", "School");
            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return View();
        }

        public ActionResult DeleteTimeTable(Guid TimeTableId)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_Shool_TimeTable.Where(x => x.TimeTableId == TimeTableId && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetTimeTableList", "School");
        }
        #endregion

        #region Report
        [HttpPost]
        public ActionResult GetAllSection(int brdId)
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            List<SelectListItem> SectionNames = new List<SelectListItem>();
            List<tbl_DC_Class_Section> states = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == brdId && x.School_Id == schoolId && x.IsActive == true).ToList();
            states.ForEach(x =>
            {
                SectionNames.Add(new SelectListItem { Text = x.SectionName, Value = x.SectionId.ToString() });
            });
            //ViewBag.state = new SelectList(dbContext.tbl_JV_State.Where(x => x.FK_Country_ID == conId && x.Is_Active == true && x.Is_Deleted == false), "PK_State_ID", "State_Name");

            return Json(SectionNames, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAllSubType(int rtypId)
        {
            List<SelectListItem> SubNames = new List<SelectListItem>();
            List<tbl_DC_ReportSubType> states = DbContext.tbl_DC_ReportSubType.Where(x => x.ReportType_ID == rtypId && x.Is_Active == true && x.Is_Delete == false).ToList();
            states.ForEach(x =>
            {
                SubNames.Add(new SelectListItem { Text = x.ReportSubType_Name, Value = x.ReportSubType_ID.ToString() });
            });
            //ViewBag.state = new SelectList(dbContext.tbl_JV_State.Where(x => x.FK_Country_ID == conId && x.Is_Active == true && x.Is_Deleted == false), "PK_State_ID", "State_Name");

            return Json(SubNames, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ViewReport()
        {

            ViewBag.ReportType_ID = new SelectList(DbContext.tbl_DC_ReportType.Where(x => x.Is_Active == true && x.Is_Delete == false),
                "ReportType_ID", "ReportType_Name");
            ViewBag.ReportSubType_ID = new SelectList(DbContext.tbl_DC_ReportSubType.Where(x => x.Is_Active == true && x.Is_Delete == false),
                "ReportSubType_ID", "ReportSubType_Name");
            List<SelectListItem> rptNamess = new List<SelectListItem>();
            DigiChampsModel.DigichampsReportType mods = new DigiChampsModel.DigichampsReportType();

            List<tbl_DC_ReportType> reports = DbContext.tbl_DC_ReportType.ToList();
            reports.ForEach(x =>
            {
                rptNamess.Add(new SelectListItem { Text = x.ReportType_Name, Value = x.ReportType_ID.ToString() });
            });
            mods.ReportType_Name = rptNamess.ToString();

            ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
            //ViewBag.totalstu = (from a in DbContext.View_All_Student_SMS.Where(x => x.Class_ID != null && x.SchoolId == schoolId).OrderByDescending(x => x.Regd_ID) select new DigiChampsModel.DigiChampsDailySalesModel { Regd_ID = a.Regd_ID, Customer_Name = a.Customer_Name + " - " + a.Mobile }).ToList();

            List<SelectListItem> BrdNamess = new List<SelectListItem>();
            DigiChampsModel.DigiChampsSubjectModel boaer = new DigiChampsModel.DigiChampsSubjectModel();

            List<tbl_DC_Board> boardss = DbContext.tbl_DC_Board.ToList();
            boardss.ForEach(x =>
            {
                BrdNamess.Add(new SelectListItem { Text = x.Board_Name, Value = x.Board_Id.ToString() });
            });
            boaer.BoardNames = BrdNamess;
            Guid? school_id = new Guid(Session["id"].ToString());
            ViewBag.section = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.School_Id == school_id), "SectionId", "SectionName");
            //  ViewBag.SchoolID = new SelectList(DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Regd_ID", "SchoolId");



            return View();
        }
        [HttpPost]
        public ActionResult ViewReport(string f_Date, string t_Date)
        {
            ViewBag.ViewReport = "active";
            ViewBag.report = "report";
            if (f_Date != "" && t_Date != "")
            {
                if (Convert.ToDateTime(f_Date) <= today.Date)
                {
                    if (Convert.ToDateTime(t_Date) <= today.Date)
                    {


                        if (Convert.ToDateTime(f_Date) <= Convert.ToDateTime(t_Date))
                        {
                            string fdtt = f_Date + " 00:00:00 AM";
                            string tdtt = t_Date + " 23:59:59 PM";
                            DateTime fdt = Convert.ToDateTime(fdtt);
                            DateTime tdt = Convert.ToDateTime(tdtt);
                            ViewBag.Breadcrumb = "ViewReport Customers";
                            IEnumerable<tbl_DC_UserReport> pre = (from c in DbContext.tbl_DC_UserReport where c.StartDate >= fdt && c.StartDate <= tdt select c).OrderByDescending(c => c.StartDate);
                            return View("ViewReport", pre);
                        }
                        else
                        {

                            TempData["ErrorMessage"] = "From date should be less or equal to todate.";
                        }

                    }
                    else
                    {

                        TempData["ErrorMessage"] = "To date should be less or equal to from-date.";
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "From date should be less than today date.";
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Please enter Date to Search.";
            }


            return RedirectToAction("ViewReport");
        }
        DateTime today = DigiChampsModel.datetoserver();
        public ActionResult DeleteReport(int? id)
        {
            ViewBag.viewsampleimage = "active";
            try
            {
                if (id != null)
                {
                    tbl_DC_ReportModuleMaster obj = DbContext.tbl_DC_ReportModuleMaster.Where(x => x.ReportModuleMaster_ID == id && x.Is_Active == true && x.Is_Delete == false).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Is_Delete = true;
                        obj.Is_Active = false;
                        DbContext.SaveChanges();
                        TempData["SuccessMessage"] = "Report deleted  successfully.";
                    }
                    else
                    {
                        TempData["WarningMessage"] = "Invalid Report details.";
                    }
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return RedirectToAction("ViewReport");
        }

        [HttpPost]
        public JsonResult NewChartSection(string f_Date, string t_Date, int classId, Guid sectionId)
        {
            List<List<DataPoint>> dataPointList = new List<List<DataPoint>>();
            if (f_Date != "" && t_Date != "")
            {
                if (Convert.ToDateTime(f_Date) <= today.Date)
                {
                    string fdtt = f_Date + " 00:00:00 AM";
                    string tdtt = t_Date + " 23:59:59 PM";
                    DateTime fdt = Convert.ToDateTime(fdtt);
                    DateTime tdt = Convert.ToDateTime(tdtt);

                    Guid? school_id = new Guid(Session["id"].ToString());
                    List<DataPoint> dataPoints = new List<DataPoint>();
                    var reportList = (DbContext.SP_Report_Total_Users_SectionWise(fdt,
                        tdt, school_id, classId, sectionId)).ToList();
                    if (reportList != null && reportList.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                    List<DataPoint> dataPoints2 = new List<DataPoint>();
                    var reportList2 = (DbContext.SP_Report_Total_User_Time_SectionWise(fdt,
                        tdt, school_id, classId, sectionId)).ToList();
                    if (reportList2 != null && reportList2.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList2.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints2.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints2);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                }

            }

            if (dataPointList.Count > 0)
            {
                ViewBag.showCharts = true;
            }
            else
            {
                ViewBag.showCharts = false;
            }

            return Json(dataPointList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult NewChartClass(string f_Date, string t_Date, int classId)
        {
            List<List<DataPoint>> dataPointList = new List<List<DataPoint>>();
            if (f_Date != "" && t_Date != "")
            {
                if (Convert.ToDateTime(f_Date) <= today.Date)
                {
                    string fdtt = f_Date + " 00:00:00 AM";
                    string tdtt = t_Date + " 23:59:59 PM";
                    DateTime fdt = Convert.ToDateTime(fdtt);
                    DateTime tdt = Convert.ToDateTime(tdtt);

                    Guid? school_id = new Guid(Session["id"].ToString());
                    List<DataPoint> dataPoints = new List<DataPoint>();
                    var reportList = (DbContext.SP_Report_Total_Users_ClassWise(fdt,
                        tdt, school_id, classId)).ToList();
                    if (reportList != null && reportList.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                    List<DataPoint> dataPoints2 = new List<DataPoint>();
                    var reportList2 = (DbContext.SP_Report_Total_User_Time_ClassWise(fdt,
                        tdt, school_id, classId)).ToList();
                    if (reportList2 != null && reportList2.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList2.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints2.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints2);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                }

            }
            if (dataPointList.Count > 0)
            {
                ViewBag.showCharts = true;
            }
            else
            {
                ViewBag.showCharts = false;
            }
            return Json(dataPointList, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetReportList(string f_Date, string t_Date)
        {
            ViewBag.showChart = false;
            string fdtt = f_Date + " 00:00:00 AM";
            string tdtt = t_Date + " 23:59:59 PM";
            DateTime fdt = Convert.ToDateTime(fdtt);
            DateTime tdt = Convert.ToDateTime(tdtt);

            Guid? school_id = new Guid(Session["id"].ToString());
            List<DigiChampsModel.DigichampsReportDetails> data = (from a in DbContext.tbl_DC_UserReport.Where(x => x.SchoolId == school_id && x.Report_Date >= fdt && x.Report_Date <= tdt)
                                                                  join b in DbContext.tbl_DC_ReportModuleMaster on a.ReportModuleMaster_ID equals b.ReportModuleMaster_ID
                                                                  //join c in DbContext.tbl_DC_ReportMaster on a.UserReport_ID equals c.UserReport_ID
                                                                  join d in DbContext.tbl_DC_Registration on a.Regd_ID equals d.Regd_ID
                                                                  join e in DbContext.tbl_DC_Module on a.Module_ID equals e.Module_ID
                                                                  select new DigiChampsModel.DigichampsReportDetails
                                                                  {
                                                                      UserReport_ID = a.UserReport_ID,
                                                                      StartDate = a.StartDate,
                                                                      EndDate = a.EndDate,
                                                                      Inserted_On = a.Inserted_On,
                                                                      Report_Date = a.Report_Date,
                                                                      Video_Time = a.Video_Time,
                                                                      ReportModuleMaster_ID = b.ReportModuleMaster_ID,
                                                                      ReportName = b.ReportName,
                                                                      Regd_ID = a.Regd_ID,
                                                                      Customer_Name = d.Customer_Name,
                                                                      Module_Name = e.Module_Name,
                                                                  }).OrderByDescending(x => x.UserReport_ID).ToList();

            // ViewBag.data = data;

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetReportListClass(string f_Date, string t_Date, int classId)
        {
            ViewBag.showChart = false;
            string fdtt = f_Date + " 00:00:00 AM";
            string tdtt = t_Date + " 23:59:59 PM";
            DateTime fdt = Convert.ToDateTime(fdtt);
            DateTime tdt = Convert.ToDateTime(tdtt);

            Guid? school_id = new Guid(Session["id"].ToString());
            List<DigiChampsModel.DigichampsReportDetails> data = (from a in DbContext.tbl_DC_UserReport.Where(x => x.SchoolId == school_id && x.Report_Date >= fdt
                && x.Report_Date <= tdt && x.Class_ID == classId)
                                                                  join b in DbContext.tbl_DC_ReportModuleMaster on a.ReportModuleMaster_ID equals b.ReportModuleMaster_ID
                                                                  //join c in DbContext.tbl_DC_ReportMaster on a.UserReport_ID equals c.UserReport_ID
                                                                  join d in DbContext.tbl_DC_Registration on a.Regd_ID equals d.Regd_ID
                                                                  join e in DbContext.tbl_DC_Module on a.Module_ID equals e.Module_ID
                                                                  select new DigiChampsModel.DigichampsReportDetails
                                                                  {
                                                                      UserReport_ID = a.UserReport_ID,
                                                                      StartDate = a.StartDate,
                                                                      EndDate = a.EndDate,
                                                                      Inserted_On = a.Inserted_On,
                                                                      Report_Date = a.Report_Date,
                                                                      Video_Time = a.Video_Time,
                                                                      ReportModuleMaster_ID = b.ReportModuleMaster_ID,
                                                                      ReportName = b.ReportName,
                                                                      Regd_ID = a.Regd_ID,
                                                                      Customer_Name = d.Customer_Name,
                                                                      Module_Name = e.Module_Name,
                                                                  }).OrderByDescending(x => x.UserReport_ID).ToList();

            ViewBag.data = data;

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetReportListSection(string f_Date, string t_Date, int classId, Guid section)
        {
            ViewBag.showChart = false;
            string fdtt = f_Date + " 00:00:00 AM";
            string tdtt = t_Date + " 23:59:59 PM";
            DateTime fdt = Convert.ToDateTime(fdtt);
            DateTime tdt = Convert.ToDateTime(tdtt);

            Guid? school_id = new Guid(Session["id"].ToString());
            List<DigiChampsModel.DigichampsReportDetails> data = (from a in DbContext.tbl_DC_UserReport.Where(x => x.SchoolId == school_id
                && x.Report_Date >= fdt && x.Report_Date <= tdt && x.Class_ID == classId && x.SectionId == section)
                                                                  join b in DbContext.tbl_DC_ReportModuleMaster on a.ReportModuleMaster_ID equals b.ReportModuleMaster_ID
                                                                  //join c in DbContext.tbl_DC_ReportMaster on a.UserReport_ID equals c.UserReport_ID
                                                                  join d in DbContext.tbl_DC_Registration on a.Regd_ID equals d.Regd_ID
                                                                  join e in DbContext.tbl_DC_Module on a.Module_ID equals e.Module_ID
                                                                  select new DigiChampsModel.DigichampsReportDetails
                                                                  {
                                                                      UserReport_ID = a.UserReport_ID,
                                                                      StartDate = a.StartDate,
                                                                      EndDate = a.EndDate,
                                                                      Inserted_On = a.Inserted_On,
                                                                      Report_Date = a.Report_Date,
                                                                      Video_Time = a.Video_Time,
                                                                      ReportModuleMaster_ID = b.ReportModuleMaster_ID,
                                                                      ReportName = b.ReportName,
                                                                      Regd_ID = a.Regd_ID,
                                                                      Customer_Name = d.Customer_Name,
                                                                      Module_Name = e.Module_Name,
                                                                  }).OrderByDescending(x => x.UserReport_ID).ToList();

            // ViewBag.data = data;

            return Json(data);
        }

        [HttpPost]
        public JsonResult NewChart(string f_Date, string t_Date)
        {

            List<List<DataPoint>> dataPointList = new List<List<DataPoint>>();
            if (f_Date != "" && t_Date != "")
            {
                if (Convert.ToDateTime(f_Date) <= today.Date)
                {
                    string fdtt = f_Date + " 00:00:00 AM";
                    string tdtt = t_Date + " 23:59:59 PM";
                    DateTime fdt = Convert.ToDateTime(fdtt);
                    DateTime tdt = Convert.ToDateTime(tdtt);

                    Guid? school_id = new Guid(Session["id"].ToString());
                    List<DataPoint> dataPoints = new List<DataPoint>();
                    var reportList = (DbContext.SP_Report_Total_Users(fdt,
                        tdt, school_id)).ToList();
                    if (reportList != null && reportList.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                    List<DataPoint> dataPoints2 = new List<DataPoint>();
                    var reportList2 = (DbContext.SP_Report_Total_User_Time(fdt,
                        tdt, school_id)).ToList();
                    if (reportList2 != null && reportList2.Count > 0)
                    {
                        List<ReportChartModel> list = (from a in reportList2.OrderBy(x => x.Inserted_Date)
                                                       select new ReportChartModel
                                                       {
                                                           Count = Convert.ToInt32(a.Order_ID),
                                                           ReportDate = a.Inserted_Date,
                                                       }).ToList();


                        for (int i = 0; i < list.Count; i++)
                        {
                            dataPoints2.Add(new DataPoint(list[i].ReportDate, list[i].Count));
                        }
                        dataPointList.Add(dataPoints2);
                        //ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
                    }


                }

            }
            if (dataPointList.Count > 0)
            {
                ViewBag.showCharts = true;
            }
            else
            {
                ViewBag.showCharts = false;
            }
            return Json(dataPointList, JsonRequestBehavior.AllowGet);
        }
        [DataContract]
        public class DataPoint
        {
            public DataPoint(string x, int y)
            {
                this.date = x;
                this.value = y;
            }

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "date")]
            public string date = null;

            //Explicitly setting the name to be used while serializing to JSON.
            [DataMember(Name = "value")]
            public Nullable<int> value = null;
        }
        public ActionResult ChartView(string f_Date, string t_Date)
        {
            // var reportList = (DbContext.SP_Report_Total_Users(Convert.ToDateTime("2018-04-04 00:00:00"), Convert.ToDateTime("2018-04-15 00:00:00"))).ToList();
            //if (reportList != null && reportList.Count > 0)
            // {
            //     List<ReportChartModel> list = (from a in reportList.OrderBy(x => x.Inserted_Date)
            //                                    select new ReportChartModel
            //                                      {
            //                                          Count = Convert.ToInt32(a.Order_ID),
            //                                          ReportDate = a.Inserted_Date,
            //                                        }).ToList();
            //     List<DataPoint> dataPoints = new List<DataPoint>();

            //     for (int i = 0; i < list.Count;i++ )
            //     {
            //         dataPoints.Add(new DataPoint(list[i].ReportDate,list[i].Count));
            //     }
            //     ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            // }

            return View();
        }
        #endregion
        /// <summary>
        /// Create Message Creation
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>

        #region classDetail

        public ActionResult GetClassList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.CreateSection> objOutput = new List<SchoolModel.CreateSection>();
            objOutput = obj.GetsectionList(new Guid(Session["id"].ToString()));
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }
        public ActionResult CreateOrEditClass(int classid = 0, string SectionName = null)
        {
            SchoolModel.CreateClass objCreateClass = new SchoolModel.CreateClass();

            var SectionList = Enum.GetValues(typeof(EnumValue.Section))
                                      .Cast<EnumValue.Section>()
                                      .ToList();
            List<SelectListItem> objCIFFOB = new List<SelectListItem>();
            foreach (var item in SectionList)
            {
                objCIFFOB.Add(new SelectListItem { Text = item.ToString(), Value = item.ToString(), Selected = true });

            }


            List<SelectListItem> objclassList = new List<SelectListItem>();

            var model = (from pd in DbContext.tbl_DC_Class
                         join od in DbContext.tbl_DC_Board on pd.Board_Id equals od.Board_Id
                         where pd.Is_Active == true
                         orderby od.Board_Id
                         select new
                         {
                             pd.Class_Name,
                             od.Board_Name,
                             pd.Class_Id

                         }).ToList();

            var newclassList = model;


            foreach (var item in newclassList)
            {
                if (classid > 0 && item.Class_Id == classid)
                {
                    objclassList.Add(new SelectListItem
                    {
                        Text = item.Class_Name.ToString() + "(" + item.Board_Name + ")",
                        Value = item.Class_Id.ToString(),
                        Selected = true
                    });
                }
                if (item.Class_Id != classid)
                {
                    objclassList.Add(new SelectListItem
                    {
                        Text = item.Class_Name.ToString() + "(" + item.Board_Name + ")",
                        Value = item.Class_Id.ToString(),
                    });
                }
            }

            objCreateClass.Section = objCIFFOB;
            objCreateClass.Class = objclassList;
            objCreateClass.Class_Id = classid;
            //objCreateClass.SectionId = SectionId;
            //  ViewBag.Accounts=
            if (classid > 0)
            {
                Guid? school_id = new Guid(Session["id"].ToString());
                var sectionName = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == classid && x.School_Id == school_id && x.IsActive == true).Distinct().ToList();
                objCreateClass.SectionList = sectionName;

                foreach (var section in sectionName)
                {
                    //if (section.SectionName != )
                    objCreateClass.SectionName += section.SectionName + ",";
                    //objCreateClass.SectionId += section.SectionId + ",";
                }
                objCreateClass.SectionName = objCreateClass.SectionName.TrimEnd(',');
                //objCreateClass.SectionId = objCreateClass.SectionId.TrimEnd(',');
            }
            ViewBag.Message = t;
            return View(objCreateClass);
        }
        [HttpPost]
        public ActionResult CreateOrEditClass(SchoolModel.CreateClass input)
        {
            return null;

        }
        public ActionResult AddClass(SchoolModel.CreateClass input)
        {
            var status = false;
            if (input.Class_Id > 0)
            {

                Guid? school_id = new Guid(Session["id"].ToString());
                var sections = input.SectionName.Split(',');
                sections = sections.Distinct().ToArray();
                if (sections != null && sections.Length > 0)
                {
                    for (int sec = 0; sec < sections.Length; sec++)
                    {
                        var sectionName = sections[sec].ToString();
                        var classSection = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == input.Class_Id && x.SectionName == sectionName && x.School_Id == school_id).FirstOrDefault();
                        if (classSection == null)
                        {
                            tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                            var sectionId = Guid.NewGuid();
                            objtbl_DC_Class_Section.ClassId = input.Id;
                            objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                            objtbl_DC_Class_Section.IsActive = true;
                            objtbl_DC_Class_Section.SectionId = sectionId;
                            objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
                            objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(input.ClassName);
                            objtbl_DC_Class_Section.School_Id = new Guid(Session["id"].ToString());
                            DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                            var id = DbContext.SaveChanges();
                        }
                        else
                        {
                            if (classSection.IsActive == false)
                            {
                                classSection.IsActive = true;
                                DbContext.SaveChanges();

                            }
                        }
                    }
                    status = true;

                    var classSectionDlt = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == input.Class_Id && x.School_Id == school_id && x.IsActive == true).ToList();
                    if (classSectionDlt != null && classSectionDlt.Count > 0)
                    {
                        foreach (var item in classSectionDlt)
                        {
                            if (!input.SectionName.Contains(item.SectionName))
                            {
                                item.IsActive = false;
                                DbContext.SaveChanges();
                            }
                        }
                    }

                }


                TempData["Message"] = "Class update Successfully.";

            }
            else
            {



                status = InsertSection(input);


                TempData["Message"] = "Class added Successfully.";
            }
            // return RedirectToAction("GetClassList", "School");

            return new JsonResult()
            {
                Data = status,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            //return View();
        }


        public bool InsertSection(SchoolModel.CreateClass input)
        {
            var status = false;
            var sections = input.SectionName.Split(',');
            if (sections != null && sections.Length > 0)
            {
                for (int sec = 0; sec < sections.Length; sec++)
                {
                    tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                    var sectionId = Guid.NewGuid();
                    objtbl_DC_Class_Section.ClassId = input.Id;
                    objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                    objtbl_DC_Class_Section.IsActive = true;
                    objtbl_DC_Class_Section.SectionId = sectionId;
                    objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
                    objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(input.ClassName);
                    objtbl_DC_Class_Section.School_Id = new Guid(Session["id"].ToString());
                    DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                    var id = DbContext.SaveChanges();
                    if (id != null)
                    {
                        status = true;
                    }
                }
            }
            else
            {

            }
            return status;
        }

        public ActionResult DeleteClass(int class_id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_Class_Section.Where(x => x.Class_Id == class_id && x.IsActive == true).ToList();
                //                  //Set status false for delete

                foreach (var item in result)
                {
                    item.IsActive = false;
                    DbContext.SaveChanges();
                }

                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetClassList", "School");
        }

        /// <summary>
        /// ///Insert class
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        //[HttpPost]

        //public ActionResult CreateOrEditClass(SchoolModel.CreateClass objCreateClass)
        //{
        //    try
        //    {
        //        tbl_DC_School_Class objClass = new tbl_DC_School_Class();
        //        if (objCreateClass.Id == Guid.Empty || objCreateClass.Id == null)
        //        {
        //            objCreateClass.Id = Guid.NewGuid();

        //        }

        //        objClass.ClassId = objCreateClass.Id;
        //        objClass.ClassName = objCreateClass.ClassName;
        //        objClass.IsActive = true;
        //        objClass.CreatedDate = DateTime.Now;
        //        objClass.SchoolId = new Guid(Session["id"].ToString());
        //        DbContext.tbl_DC_School_Class.Add(objClass);

        //    }

        //    catch
        //    {

        //    }
        //    return RedirectToAction("GetClassList", "School");
        //}



        #endregion classDetail

        #region periodDetails
        public ActionResult GetPeriodList()
        {
            SchoolAPIController obj = new SchoolAPIController();
            List<SchoolModel.CreatePeriod> objOutput = new List<SchoolModel.CreatePeriod>();

            Guid SchoolId = new Guid(Session["id"].ToString());

            objOutput = obj.GetPeriodList(SchoolId);
            //  objOutput.HomeWork = objOutput;
            ViewBag.Message = t;
            return View(objOutput);
        }
        public ActionResult AddPeriod(Guid? Id)
        {
            SchoolModel.CreatePeriod objPeriodInformation = new SchoolModel.CreatePeriod();

            if (Id != null)
            {
                SchoolAPIController schoolApi = new SchoolAPIController();
                return View(schoolApi.GetPeriodById(Id));
            }
            ViewBag.Message = t;
            return View(objPeriodInformation);
        }
        [HttpPost]
        public ActionResult AddPeriod(SchoolModel.CreatePeriod objmodel)
        {
            try
            {
                if (objmodel.Id != null && objmodel.Id != Guid.Empty)
                {
                    var Period = DbContext.tbl_DC_Period.Where(x => x.Id == objmodel.Id).SingleOrDefault();
                    {
                        Period.Title = objmodel.Title;
                        Period.FromTime = objmodel.FromTime;
                        Period.ToTime = objmodel.ToTime;
                        Period.IsActive = true;
                        Period.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                        TempData["Message"] = "Period Update Successfully.";
                    }
                }
                else
                {
                    tbl_DC_Period objPeriod = new tbl_DC_Period();
                    //school objam= new objam();

                    objPeriod.Id = Guid.NewGuid();
                    //  objPeriod.se = objmodel.SectionId;
                    objPeriod.Title = objmodel.Title;
                    objPeriod.IsActive = true;
                    objPeriod.Create_Date = DateTime.Now;
                    objPeriod.FromTime = objmodel.FromTime;
                    objPeriod.ToTime = objmodel.ToTime;
                    objPeriod.SchoolId = new Guid(Session["id"].ToString());


                    DbContext.tbl_DC_Period.Add(objPeriod);
                    var id = DbContext.SaveChanges();
                    TempData["Message"] = "Period Added Successfully.";
                }



            }
            catch (Exception ex)
            {

            }
            ViewBag.Message = t;
            return RedirectToAction("GetPeriodList", "School");
        }

        public ActionResult DeletePeriod(Guid id)
        {

            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_Period.Where(x => x.Id == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                //data = result.UserRole;

            }

            catch (Exception ex)
            {

            }

            return RedirectToAction("GetPeriodList", "School");
        }
        #endregion periodDetails

        #region CommonFunction
        public List<SelectListItem> GetClassListDropDown()
        {
            List<SelectListItem> classSelectList = new List<SelectListItem>();

            var classList = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();
            if (classList.Any())
            {
                foreach (var item in classList)
                {
                    classSelectList.Add(new SelectListItem { Text = item.Class_Name + " (" + DbContext.tbl_DC_Board.Where(a => a.Board_Id == item.Board_Id).FirstOrDefault().Board_Name + ")", Value = item.Class_Id.ToString() });

                }
            }
            ViewBag.Message = t;
            return classSelectList;
        }
        #endregion
        [HttpGet]
        public JsonResult IsEmailExist(string eMail)
        {

            // bool isExist = DbContext.tbl_DC_SchoolUser.Where(u => u.UserEmailAddress.ToLowerInvariant().Equals(eMail.ToLower()))).FirstOrDefault() != null;
            bool isExist = DbContext.tbl_DC_SchoolUser.Where(x => x.UserEmailAddress.Equals(eMail) && x.IsActive == true).FirstOrDefault() != null; ; ;

            return Json(!isExist, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult CreateOrEditClass(Guid? Id)
        //{
        //    SchoolModel.CreateClass objCreateClass = new SchoolModel.CreateClass();

        //    var SectionList = Enum.GetValues(typeof(EnumValue.Section))
        //                              .Cast<EnumValue.Section>()
        //                              .ToList();
        //    List<SelectListItem> objCIFFOB = new List<SelectListItem>();
        //    foreach (var item in SectionList)
        //    {


        //        objCIFFOB.Add(new SelectListItem { Text = item.ToString(), Value = item.ToString() });


        //    }

        //    //var ClassList = Enum.GetValues(typeof(EnumValue.SchoolClass))
        //    //                        .Cast<EnumValue.SchoolClass>()
        //    //                        .ToList();
        //    List<SelectListItem> objclassList = new List<SelectListItem>();
        //    //foreach (var classItem in ClassList)
        //    //{

        //    //    string name = Enum.GetName(typeof(EnumValue.SchoolClass), classItem);
        //    //    objclassList.Add(new SelectListItem { Text = name, Value = name });


        //    //}
        //    for (int schoolClass = 1; schoolClass <= 12; schoolClass++)
        //    {

        //        // string name = Enum.GetName(typeof(EnumValue.SchoolClass), classItem);
        //        objclassList.Add(new SelectListItem { Text = schoolClass.ToString(), Value = schoolClass.ToString() });


        //    }

        //    objCreateClass.Section = objCIFFOB;
        //    objCreateClass.Class = objclassList;
        //    //  ViewBag.Accounts=
        //    if (Id != Guid.Empty && Id != null)
        //    {
        //        var schoolClass = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == Id).SingleOrDefault();

        //        objCreateClass.Id = schoolClass.ClassId;
        //        objCreateClass.ClassName = schoolClass.ClassName;
        //        var sectionName = DbContext.tbl_DC_Class_Section.Where(x => x.ClassId == Id).ToList();

        //        foreach (var section in sectionName)
        //        {
        //            objCreateClass.SectionName += section.SectionName + ",";
        //        }
        //        objCreateClass.SectionName = objCreateClass.SectionName.TrimEnd(',');

        //    }
        //    return View(objCreateClass);
        //}
        //public ActionResult AddClass(SchoolModel.CreateClass input)
        //{
        //    var status = false;
        //    if (input.Id != null && input.Id != Guid.Empty)
        //    {
        //        var classDetail = DbContext.tbl_DC_School_Class.Where(x => x.ClassId == input.Id).SingleOrDefault();
        //        classDetail.ClassName = input.ClassName;
        //        var sections = input.SectionName.Split(',');
        //        DbContext.SaveChanges();
        //        if (classDetail != null)
        //        {
        //            if (sections != null && sections.Length > 0)
        //            {
        //                for (int sec = 0; sec < sections.Length; sec++)
        //                {
        //                    var sectionName = sections[sec].ToString();
        //                    var classSection = DbContext.tbl_DC_Class_Section.Where(x => x.ClassId == classDetail.ClassId && x.SectionName == sectionName).SingleOrDefault();
        //                    if (classSection == null)
        //                    {
        //                        tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
        //                        var sectionId = Guid.NewGuid();
        //                        objtbl_DC_Class_Section.ClassId = input.Id;
        //                        objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
        //                        objtbl_DC_Class_Section.IsActive = true;
        //                        objtbl_DC_Class_Section.SectionId = sectionId;
        //                        objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
        //                        DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
        //                        var id = DbContext.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        if (classSection.IsActive == false)
        //                        {
        //                            classSection.IsActive = true;
        //                            DbContext.SaveChanges();
        //                        }
        //                    }
        //                }

        //                var classSectionDlt = DbContext.tbl_DC_Class_Section.Where(x => x.ClassId == classDetail.ClassId && x.IsActive == true).ToList();
        //                if (classSectionDlt != null && classSectionDlt.Count > 0)
        //                {
        //                    foreach (var item in classSectionDlt)
        //                    {
        //                        if (!input.SectionName.Contains(item.SectionName))
        //                        {
        //                            item.IsActive = false;
        //                            DbContext.SaveChanges();
        //                        }
        //                    }
        //                }

        //            }

        //            //var class
        //            //foreach(var item in classSection)
        //            //{
        //            //     var classSectiondlt = DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == item.SectionId).SingleOrDefault();
        //            //     //DbContext.tbl_DC_Class_Section.Remove(classSectiondlt);
        //            //     //DbContext.SaveChanges();

        //            //   if(classSectiondlt!=null)
        //            //   {

        //            //   }

        //            //}


        //        }
        //    }
        //    else
        //    {

        //        input.Id = Guid.NewGuid();
        //        tbl_DC_School_Class objtbl_DC_School_Class = new tbl_DC_School_Class();
        //        objtbl_DC_School_Class.ClassId = input.Id;
        //        objtbl_DC_School_Class.ClassName = input.ClassName;
        //        objtbl_DC_School_Class.CreatedDate = DateTime.Now;
        //        objtbl_DC_School_Class.IsActive = true;
        //        objtbl_DC_School_Class.SchoolId = new Guid("CE83C8BA-FAD3-4ED7-AD98-36BB7D08D497");
        //        DbContext.tbl_DC_School_Class.Add(objtbl_DC_School_Class);

        //        var classId = DbContext.SaveChanges();
        //        if (classId != null)
        //        {


        //            InsertSection(input);

        //        }


        //    }
        //    return new JsonResult()
        //    {
        //        Data = status,
        //        JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //    };

        //    //return View();
        //}


        //public bool InsertSection(SchoolModel.CreateClass input)
        //{
        //    var status = false;
        //    var sections = input.SectionName.Split(',');
        //    if (sections != null && sections.Length > 0)
        //    {
        //        for (int sec = 0; sec < sections.Length; sec++)
        //        {
        //            tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
        //            var sectionId = Guid.NewGuid();
        //            objtbl_DC_Class_Section.ClassId = input.Id;
        //            objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
        //            objtbl_DC_Class_Section.IsActive = true;
        //            objtbl_DC_Class_Section.SectionId = sectionId;
        //            objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
        //            DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
        //            var id = DbContext.SaveChanges();
        //            if (id != null)
        //            {
        //                status = true;
        //            }
        //        }
        //    }
        //    else
        //    {

        //    }
        //    return status;
        //}
        ///// <summary>
        ///// ///Insert class
        ///// </summary>
        ///// <param name="Id"></param>
        ///// <returns></returns>
        //[HttpPost]

        //public ActionResult CreateOrEditClass(SchoolModel.CreateClass objCreateClass)
        //{
        //    try
        //    {
        //        tbl_DC_School_Class objClass = new tbl_DC_School_Class();
        //        if (objCreateClass.Id == Guid.Empty || objCreateClass.Id == null)
        //        {
        //            objCreateClass.Id = Guid.NewGuid();

        //        }

        //        objClass.ClassId = objCreateClass.Id;
        //        objClass.ClassName = objCreateClass.ClassName;
        //        objClass.IsActive = true;
        //        objClass.CreatedDate = DateTime.Now;
        //        objClass.SchoolId = new Guid(Session["id"].ToString());
        //        DbContext.tbl_DC_School_Class.Add(objClass);

        //    }

        //    catch
        //    {

        //    }
        //    return RedirectToAction("GetClassList", "School");
        //}

        public ActionResult DeleteUserList(Guid id)
        {

            var data = "";
            try
            {
                // get data for same id 
                var result = DbContext.tbl_DC_SchoolUser.Where(x => x.UserId == id && x.IsActive == true).FirstOrDefault();
                //                  //Set status false for delete
                result.IsActive = false;
                DbContext.SaveChanges();
                data = result.UserRole;

            }

            catch (Exception ex)
            {



            }
            if (data == "SchoolAdmin")
                return RedirectToAction("GetSchoolAdminList", "School");
            if (data == "Teacher")
                return RedirectToAction("GetSchoolTeacherList", "School");
            else
                return RedirectToAction("GetSchoolPrincipleList", "School");
        }

        //public ActionResult GetClassList()
        //{
        //    SchoolAPIController obj = new SchoolAPIController();
        //    List<SchoolModel.CreateClass> objOutput = new List<SchoolModel.CreateClass>();
        //    SchoolModel.InputModel objInput = new SchoolModel.InputModel();
        //    objInput.SchoolId = new Guid(Session["id"].ToString());
        //    objOutput = obj.GetClassList(objInput);
        //    //  objOutput.HomeWork = objOutput;

        //    return View(objOutput);
        //}

        #region Send SMS
        [HttpGet]
        public ActionResult ViewSendSMS()
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            try
            {

                var data = (from a in DbContext.tbl_DC_SMS.Where(x => x.School_ID == school_id)

                            select new DigiChampsModel.DigichampsSMSDetails
                            {
                                SMS_ID = a.SMS_ID,
                                SMS_Message = a.SMS_Message,
                                Class_Name = a.Class_ID != null ? DbContext.tbl_DC_Class.Where(x => x.Class_Id == a.Class_ID).
                                Select(x => x.Class_Name).FirstOrDefault() : "No Data",
                                Section_Name = a.Section_ID != null ? DbContext.tbl_DC_Class_Section.Where(x => x.SectionId == a.Section_ID).
                                Select(x => x.SectionName).FirstOrDefault() : "No Data",
                                Customer_Name = a.Regd_ID != null ? DbContext.tbl_DC_Registration.
                                Where(x => x.Regd_ID == a.Regd_ID).Select(x => x.Customer_Name).FirstOrDefault() : "No Data",
                                Inserted_On = a.Inserted_On
                            }).OrderByDescending(x => x.SMS_ID).ToList();
                ViewBag.data = data;
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return View();
        }
        [HttpGet]
        public ActionResult AddSendSMS(int? id)
        {
            Guid? schoolId = (Guid?)Session["id"];

            ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
            //ViewBag.totalstu = (from a in DbContext.View_All_Student_SMS.Where(x => x.Class_ID != null && x.SchoolId == schoolId).OrderByDescending(x => x.Regd_ID) select new DigiChampsModel.DigiChampsDailySalesModel { Regd_ID = a.Regd_ID, Customer_Name = a.Customer_Name + " - " + a.Mobile }).ToList();

            List<SelectListItem> BrdNamess = new List<SelectListItem>();
            DigiChampsModel.DigiChampsSubjectModel mods = new DigiChampsModel.DigiChampsSubjectModel();

            List<tbl_DC_Board> boardss = DbContext.tbl_DC_Board.ToList();
            boardss.ForEach(x =>
            {
                BrdNamess.Add(new SelectListItem { Text = x.Board_Name, Value = x.Board_Id.ToString() });
            });
            mods.BoardNames = BrdNamess;

            ViewBag.SchoolID = new SelectList(DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Regd_ID", "SchoolId");


            return View();
        }
        [HttpPost]
        public ActionResult GetAllStudents(int brdId)
        {
            Guid? schoolId = (Guid?)Session["id"];
            List<DigiChampsModel.DigiChampsDailySalesModel> names = (from a in DbContext.View_All_Student_SMS.Where(x => x.Class_ID == brdId && x.SchoolId == schoolId).OrderByDescending(x => x.Regd_ID)
                                                                     select new DigiChampsModel.DigiChampsDailySalesModel { Mobile = a.Mobile, Regd_ID = a.Regd_ID, Customer_Name = a.Customer_Name + " - " + a.Mobile }).ToList();
            return Json(names, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult AddSendSMS(string txtdesc, string Board_Name = null, int Class_Name = 0, string Customer_Name = null)
        {
            //Response.Write("<script> alert('test');</script>");
            try
            {
                if (Session["id"] != null)
                {
                    Guid? schoolId = (Guid?)Session["id"];
                    //List<tbl_DC_Registration> regList = DbContext.tbl_DC_Registration.
                    //    Where(x => x.SchoolId == schoolId && x.Is_Active == true && x.Is_Deleted == false).ToList();

                    if (Class_Name == null || Class_Name == 0)
                    {
                        List<DigiChampsModel.View_All_Students> regList = (from a in DbContext.tbl_DC_Registration.Where(x => x.Is_Active == true &&
                            x.SchoolId == schoolId && x.Is_Deleted == false)
                                                                           join b in DbContext.tbl_DC_Registration_Dtl.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                                                           on a.Regd_ID equals b.Regd_ID
                                                                           select new DigiChampsModel.View_All_Students
                                                                           {
                                                                               Mobile = a.Mobile,

                                                                           }).ToList();
                        for (int i = 0; i < regList.Count; i++)
                            Sendsms(txtdesc, regList[i].Mobile);

                        tbl_DC_SMS sms = new tbl_DC_SMS();
                        sms.School_ID = schoolId;

                        sms.SMS_Message = txtdesc;
                        sms.Inserted_On = System.DateTime.Now;
                        DbContext.tbl_DC_SMS.Add(sms);
                        DbContext.SaveChanges();
                    }
                    else

                        if (Customer_Name == null || Customer_Name.Equals("") || Customer_Name.Equals("NULL") || Customer_Name.Equals("null"))
                        {
                            List<DigiChampsModel.View_All_Students> regList = (from a in DbContext.tbl_DC_Registration.
                                                                                   Where(x => x.Is_Active == true &&
                                                                                       x.SchoolId == schoolId && x.Is_Deleted == false)
                                                                               join b in DbContext.tbl_DC_Registration_Dtl.
                                                                               Where(x => x.Is_Active == true && x.Class_ID == Class_Name
                                                                                   && x.Is_Deleted == false)
                                                                               on a.Regd_ID equals b.Regd_ID
                                                                               select new DigiChampsModel.View_All_Students
                                                                               {
                                                                                   Mobile = a.Mobile,

                                                                               }).ToList();
                            for (int i = 0; i < regList.Count; i++)
                                Sendsms(txtdesc, regList[i].Mobile);

                            tbl_DC_SMS sms = new tbl_DC_SMS();
                            sms.School_ID = schoolId;
                            sms.Class_ID = Class_Name;
                            sms.SMS_Message = txtdesc;

                            sms.Inserted_On = System.DateTime.Now;
                            DbContext.tbl_DC_SMS.Add(sms);
                            DbContext.SaveChanges();

                        }
                        else
                        {
                            Sendsms(txtdesc, Customer_Name);
                            var registration = DbContext.tbl_DC_Registration.
                                Where(x => x.Mobile == Customer_Name && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                            tbl_DC_SMS sms = new tbl_DC_SMS();
                            sms.School_ID = schoolId;
                            sms.Regd_ID = registration.Regd_ID;
                            sms.SMS_Message = txtdesc;
                            sms.Class_ID = Class_Name;
                            sms.Section_ID = registration.SectionId;
                            sms.Inserted_On = System.DateTime.Now;
                            DbContext.tbl_DC_SMS.Add(sms);
                            DbContext.SaveChanges();
                        }
                    TempData["Message"] = "SMS Send Successfully.";
                    //TempData["Message"] = "Message Added Successfully.";
                    return RedirectToAction("ViewSendSMS");
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }

            return RedirectToAction("ViewSendSMS");
        }

        #endregion

        public bool Sendsms(string message, string mobile)
        {
            try
            {
                var sms_obj = DbContext.View_DC_SMS_API.Where(x => x.Sms_Alert_Name == "REG").FirstOrDefault();
                //  if (sms_obj != null)
                // {
                // string message = sms_obj.Sms_Body;
                // var regex = new Regex(Regex.Escape("{{otpno}}"));
                //var newText = regex.Replace(message, opt, 9);


                string baseurl = "" + sms_obj.Api_Url.ToString().Replace("mobile", mobile).Replace("message", message);

                HttpWebRequest myReq = (HttpWebRequest)WebRequest.Create(baseurl);
                //Get response from the SMS Gateway Server and read the answer
                HttpWebResponse myResp = (HttpWebResponse)myReq.GetResponse();
                System.IO.StreamReader respStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
                string responseString = respStreamReader.ReadToEnd();
                respStreamReader.Close();
                myResp.Close();
                // }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region Test Report
        [HttpGet]
        public ActionResult ViewTestReport()
        {
            ViewBag.Board_Id = new SelectList(DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Board_Id", "Board_Name");
            List<SelectListItem> BrdNamess = new List<SelectListItem>();
            DigiChampsModel.DigiChampsSubjectModel boaer = new DigiChampsModel.DigiChampsSubjectModel();
            List<tbl_DC_Board> boardss = DbContext.tbl_DC_Board.ToList();
            boardss.ForEach(x =>
            {
                BrdNamess.Add(new SelectListItem { Text = x.Board_Name, Value = x.Board_Id.ToString() });
            });
            boaer.BoardNames = BrdNamess;
            Guid? school_id = new Guid(Session["id"].ToString());
            ViewBag.section = new SelectList(DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.School_Id == school_id), "SectionId", "SectionName");
            ViewBag.Breadcrumb = "Question";
            ViewBag.subjlist = (from a in DbContext.tbl_DC_Question.Where(x => x.Is_Active == true && x.Is_Deleted == false).Select(a => a.Subject_Id).Distinct()
                                join b in DbContext.tbl_DC_Subject.Where(x => x.Is_Active == true && x.Is_Deleted == false)
                                on a equals b.Subject_Id
                                select new DigiChampsModel.DigiChampsSubjectModel
                                {
                                    Class_Id = b.Class_Id,
                                    Subject = b.Subject,
                                    Subject_Id = (int)a
                                }).ToList();

            ViewBag.Question = DbContext.tbl_DC_Question.Where(x => x.Is_Active == true && x.Is_Deleted == false).OrderByDescending(x => x.Question_ID).Take(200).ToList();

            var Count_Que = DbContext.SP_DC_Question_Count(0).ToList();
            ViewBag.Count_Que = Count_Que.ToList();
            ViewBag.Exam = new SelectList(DbContext.tbl_DC_Exam.Where(x => x.Is_Active == true && x.Is_Deleted == false), "Exam_ID", "Exam_Name");

            return View();
        }

        [HttpPost]
        public JsonResult GetTestReportList(string f_Date, string t_Date)
        {
            ViewBag.showChart = false;
            string fdtt = f_Date + " 00:00:00 AM";
            string tdtt = t_Date + " 23:59:59 PM";
            DateTime fdt = Convert.ToDateTime(fdtt);
            DateTime tdt = Convert.ToDateTime(tdtt);

            Guid? school_id = new Guid(Session["id"].ToString());
            List<DigiChampsModel.DigichampsTestReport> data = (from a in DbContext.tbl_DC_Exam_Result
                                                               join b in DbContext.tbl_DC_Exam_Result_Dtl on a.Result_ID equals b.Result_ID
                                                               join c in DbContext.tbl_DC_Registration on a.Regd_ID equals c.Regd_ID
                                                               select new DigiChampsModel.DigichampsTestReport
                                                               {
                                                                   StudentName = c.Customer_Name,
                                                                   Mark = a.Question_Attempted
                                                               }).OrderByDescending(x => x.Class_Id).ToList();

            // ViewBag.data = data;

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ViewReportData
        public ActionResult ViewReportData()
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            var school = DbContext.tbl_DC_School_Info.ToList().Where(a => a.SchoolId == school_id).FirstOrDefault();
            ViewBag.SchoolName = school.SchoolName;
            ViewBag.Schoolid = school.SchoolId;
            return View();
        }
        public JsonResult GetBoard()
        {
            var board = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClasses(int B_id)
        {
            var classes = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Board_Id == B_id).ToList();
            return Json(classes, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSections(int C_id)
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            var sections = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == C_id && x.School_Id == school_id).ToList();
            return Json(sections, JsonRequestBehavior.AllowGet);
        }
        public class Video_Report_Class
        {
            public int UserReport_ID { get; set; }
            public int? Module_ID { get; set; }
            public int? Regd_ID { get; set; }
            public DateTime? Inserted_On { get; set; }
            public string InsertedOn { get; set; }
            public int? Class_ID { get; set; }
            public Guid? SchoolId { get; set; }
            public DateTime? Report_Date { get; set; }
            public string ReportDate { get; set; }
            public string ReportName { get; set; }
            public string Customer_Name { get; set; }
            public Guid? SectionId { get; set; }
            public string Module_Name { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public int? Video_Time { get; set; }
            public int ReportModuleMaster_ID { get; set; }

        }
        public JsonResult GetVideoReportList(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            Guid? school_id = new Guid(Session["id"].ToString());
            var school = DbContext.tbl_DC_UserReport.Where(x => x.SchoolId == school_id && x.Report_Date >= fdt && x.Report_Date <= tdt).ToList();
            var ReportModuleMaster = DbContext.tbl_DC_ReportModuleMaster.ToList();
            var Registration = DbContext.tbl_DC_Registration.ToList();
            var module = DbContext.tbl_DC_Module.ToList();
            List<Video_Report_Class> data = (from a in school
                                             join b in ReportModuleMaster on a.ReportModuleMaster_ID equals b.ReportModuleMaster_ID
                                             join d in Registration on a.Regd_ID equals d.Regd_ID
                                             join e in module on a.Module_ID equals e.Module_ID
                                             select new Video_Report_Class
                                             {
                                                 UserReport_ID = a.UserReport_ID,
                                                 StartDate = a.StartDate,
                                                 EndDate = a.EndDate,
                                                 Inserted_On = a.Inserted_On,
                                                 Report_Date = a.Report_Date,
                                                 Video_Time = a.Video_Time,
                                                 ReportModuleMaster_ID = b.ReportModuleMaster_ID,
                                                 ReportName = b.ReportName,
                                                 Regd_ID = a.Regd_ID,
                                                 Customer_Name = d.Customer_Name,
                                                 Module_Name = e.Module_Name,
                                                 Class_ID = a.Class_ID,
                                                 SchoolId = a.SchoolId,
                                                 SectionId = d.SectionId,
                                                 ReportDate = Convert.ToDateTime(a.Report_Date).ToString("dd/MM/yyyy"),
                                                 InsertedOn = Convert.ToDateTime(a.Inserted_On).ToString("dd/MM/yyyy"),
                                                 Module_ID = a.Module_ID
                                             }).OrderByDescending(x => x.UserReport_ID).ToList();
            if (C_Id != null)
            {
                data = data.Where(a => a.Class_ID == C_Id).ToList();
            }
            if (S_Id != null)
            {
                data = data.Where(a => a.SectionId == S_Id).ToList();
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChartTotalUser(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            List<DataPoint> datapoints = new List<DataPoint>();
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            Guid? school_id = new Guid(Session["id"].ToString());
            if (C_Id == null && S_Id == null)
            {
                var results = DbContext.Database.SqlQuery<List<DataPoint>>(@"SELECT count( distinct(A.[Regd_ID])) as [Order_ID], CONVERT(varchar(30), CONVERT(date, [Report_Date], 110))  [Inserted_Date] FROM [odm_lms].[dbo].[tbl_DC_UserReport] AS A 
  INNER JOIN [odm_lms].[dbo].[tbl_DC_ReportModuleMaster] AS B ON A.ReportModuleMaster_ID=B.ReportModuleMaster_ID
  INNER JOIN [odm_lms].[dbo].[tbl_DC_Registration] AS C ON A.Regd_ID=C.Regd_ID
  INNER JOIN [odm_lms].[dbo].[tbl_DC_Module] AS D ON A.Module_ID=D.Module_ID
  WHERE (A.SchoolId='" + school_id + "' AND A.Report_Date BETWEEN '" + fdt + "' and '" + tdt + "') GROUP BY A.Report_Date").ToList();
            }


            return Json(datapoints, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region ViewActivityTime
        public ActionResult ViewActivityTime()
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            var school = DbContext.tbl_DC_School_Info.ToList().Where(a => a.SchoolId == school_id).FirstOrDefault();
            ViewBag.SchoolName = school.SchoolName;
            ViewBag.Schoolid = school.SchoolId;
            return View();
        }
        SchoolCls scl = new SchoolCls();
        public JsonResult GetActivityReportList(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            Guid school_id = new Guid(Session["id"].ToString());
            List<Activity_Time_Class> data = scl.GetActivityTimeDetails(fdt, tdt, C_Id, S_Id, school_id).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActivityChartTotalUser(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            List<DataPoint> datapoints = new List<DataPoint>();
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            Guid school_id = new Guid(Session["id"].ToString());
            datapoints = scl.GetActivityChartTotalUser(fdt, tdt, C_Id, S_Id, school_id).ToList();
            return Json(datapoints, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetActivityChartTotalTime(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            List<DataPoint> datapoints = new List<DataPoint>();
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            Guid school_id = new Guid(Session["id"].ToString());
            datapoints = scl.GetActivityChartTotalTime(fdt, tdt, C_Id, S_Id, school_id).ToList();
            return Json(datapoints, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult GetUsageReportList()
        //{

        //}
        //public ActionResult GeneratePDF()
        //{
        //    return new Rotativa.ActionAsPdf("GetPersons");
        //}
        public ActionResult GetReportData(DateTime f_Date, DateTime t_Date, int? C_Id, Guid? S_Id)
        {
            Guid school_id = new Guid(Session["id"].ToString());
            DateTime fdt = Convert.ToDateTime(f_Date);
            DateTime tdt = Convert.ToDateTime(t_Date);
            DataSet ds = new DataSet("Usage_Reports");

            DataTable dt1 = new DataTable();
            dt1 = scl.ConvertToDataTable(scl.GetSchoolData(school_id));
            dt1.TableName = "School_Details";
            ds.Tables.Add(dt1.Copy());

            DataTable dt2 = new DataTable();
            dt2 = scl.ConvertToDataTable(scl.GetActivityTotalUser(fdt, tdt, C_Id, S_Id, school_id));
            dt2.TableName = "All_Users";
            ds.Tables.Add(dt2.Copy());

            DataTable dt3 = new DataTable();
            dt3 = scl.ConvertToDataTable(scl.GetActivityTotalTime(fdt, tdt, C_Id, S_Id, school_id));
            dt3.TableName = "All_Activity_Time";
            ds.Tables.Add(dt3.Copy());

            DataTable dt4 = new DataTable();
            dt4 = scl.ConvertToDataTable(scl.GetAllUsernames(fdt, tdt, C_Id, S_Id, school_id));
            dt4.TableName = "All_Users_List";
            ds.Merge(dt4.Copy());

            DataTable dt5 = new DataTable("All_Activity_Time_List");
            dt5 = scl.ConvertToDataTable(scl.GetAlltimeUsernames(fdt, tdt, C_Id, S_Id, school_id));
            dt5.TableName = "All_Activity_Time_List";
            ds.Tables.Add(dt5.Copy());
            // ds.AcceptChanges(); 
            // ds.WriteXml(@"D:\parismita\XML_FILES\Usage_Report.xml");
            ReportDocument rd = new ReportDocument();
            rd.Load(Path.Combine(Server.MapPath("~/Reports/School"), "usagereport.rpt"));
            rd.SetDataSource(ds);
            TextObject txtObj = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text12"];
            txtObj.Text = "From :" + Convert.ToDateTime(fdt).ToString("dd/MM/yyyy") + "   To :" + Convert.ToDateTime(tdt).ToString("dd/MM/yyyy");

            TextObject txtObj1 = (TextObject)rd.ReportDefinition.Sections["Section2"].ReportObjects["Text1"];
            txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "TOTAL USERS (ALL STUDENTS)" : "TOTAL USERS (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == C_Id).FirstOrDefault().Class_Name + ")";

            TextObject txtObj2 = (TextObject)rd.ReportDefinition.Sections["Section2"].ReportObjects["Text3"];
            txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == C_Id).FirstOrDefault().Class_Name + ")";



            System.IO.Stream m = null;
            m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            byte[] byteArray = null;
            byteArray = new byte[m.Length];
            m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
            rd.Close();
            rd.Dispose();
            Response.ClearContent();
            Response.ClearHeaders();
            Response.Buffer = true;
            //Stream stream = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
            //stream.Seek(0, SeekOrigin.Begin);
            return File(m, "application/pdf", "CustomerList.pdf");
            // return Redirect("~/Reports/Reports.aspx?type=School_Usage_Report&f_Date='" + fdt + "'&t_Date='" + tdt + "'&C_Id='" + C_Id + "'&S_Id='" + S_Id + "'&school_id='" + school_id + "'");
        }
        public ActionResult UsageReport()
        {
            DateTime f_Date = Convert.ToDateTime(Request.QueryString["f_Date"].ToString());
            DateTime t_Date = Convert.ToDateTime(Request.QueryString["t_Date"].ToString());
            int C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
            string S_Id = Request.QueryString["S_Id"].ToString();
            Guid school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
            return View();
        }

        #endregion
        #region ViewTestReport
        public ActionResult ViewTestData()
        {
            Guid? school_id = new Guid(Session["id"].ToString());
            var school = DbContext.tbl_DC_School_Info.ToList().Where(a => a.SchoolId == school_id).FirstOrDefault();
            ViewBag.SchoolName = school.SchoolName;
            ViewBag.Schoolid = school.SchoolId;
            return View();
        }
        public JsonResult GetSubjectData(int id)
        {
            var s = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Class_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetChapterData(int id)
        {
            var s = DbContext.tbl_DC_Chapter.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Subject_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTestData(int id)
        {
            var s = DbContext.tbl_DC_Exam.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Chapter_Id == id).ToList();
            return Json(s, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTestMarkData(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            List<DigiChamps.Models.SchoolCls.Test_Mark_Class> m = scl.GetTestMarkData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);
            return Json(m, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTestMarks(int C_Id, string S_Id, Guid school_id, int Sub_Id, int Chap_Id, int T_Id, int B_Id)
        {
            var s = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id).ToList();
            Guid sid = Guid.Parse(S_Id);
            var m = DbContext.tbl_DC_Registration.Where(a => a.SectionId == sid).ToList();
            var r = (from a in s.ToList()
                     join b in m.ToList() on a.Regd_ID equals b.Regd_ID
                     select new
                     {
                         b.Customer_Name,
                         a.Question_Attempted,
                         a.Question_Nos,
                         a.Total_Correct_Ans
                     }).ToList();

            return Json(r, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult Createclasssection(Guid? id)
        {
            Guid? school_id = id;
            string SectionName = "A,B,C,D,E,F,G,RA,RB,RC,RD,N40M,N40B,D1,D2,D3,DB,P1,P2,P3,P4,COMM-A,COMM-B,FRUIT,FLOWER,R1,R2,R3,R4";
            var sections = SectionName.Split(',');
            sections = sections.Distinct().ToArray();
            if (sections != null && sections.Length > 0)
            {

                var brd = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                foreach (var board in brd.ToList())
                {
                    var cls = DbContext.tbl_DC_Class.Where(a => a.Board_Id == board.Board_Id && a.Is_Active == true && a.Is_Deleted == false).ToList();
                    foreach (var classes in cls.ToList())
                    {
                        for (int sec = 0; sec < sections.Length; sec++)
                        {
                            var sectionName = sections[sec].ToString();
                            var classSection = DbContext.tbl_DC_Class_Section.Where(x => x.SectionName == sectionName &&
                                x.Class_Id == classes.Class_Id && x.School_Id == school_id).FirstOrDefault();
                            if (classSection == null)
                            {
                                tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                                var sectionId = Guid.NewGuid();
                                objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                                objtbl_DC_Class_Section.IsActive = true;
                                objtbl_DC_Class_Section.SectionId = sectionId;
                                objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
                                objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(classes.Class_Id);
                                objtbl_DC_Class_Section.School_Id = school_id;

                                DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                                DbContext.SaveChanges();
                            }
                            else
                            {
                                if (classSection.IsActive == false)
                                {
                                    classSection.IsActive = true;
                                    DbContext.SaveChanges();
                                }
                            }
                        }

                    }
                }
            }
            TempData["SuccessMessage"] = "Class and Sections are Created Successfully";
            return RedirectToAction("GetSchoolList");
        }
        public ActionResult UploadSectionDetails()
        {
            var school = DbContext.tbl_DC_School_Info.Where(a => a.IsActive == true).ToList();
            foreach (var sch in school.ToList())
            {
                Guid? school_id = sch.SchoolId;
                string SectionName = "A,B,C,D,E,F,G,RA,RB,RC,RD,N40M,N40B,D1,D2,D3,DB,P1,P2,P3,P4,COMM-A,COMM-B,FRUIT,FLOWER,R1,R2,R3,R4";
                var sections = SectionName.Split(',');
                sections = sections.Distinct().ToArray();
                if (sections != null && sections.Length > 0)
                {

                    var brd = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
                    foreach (var board in brd.ToList())
                    {
                        var cls = DbContext.tbl_DC_Class.Where(a => a.Board_Id == board.Board_Id && a.Is_Active == true && a.Is_Deleted == false).ToList();
                        foreach (var classes in cls.ToList())
                        {
                            for (int sec = 0; sec < sections.Length; sec++)
                            {
                                var sectionName = sections[sec].ToString();
                                var classSection = DbContext.tbl_DC_Class_Section.Where(x => x.SectionName == sectionName &&
                                    x.Class_Id == classes.Class_Id && x.School_Id == school_id).FirstOrDefault();
                                if (classSection == null)
                                {
                                    tbl_DC_Class_Section objtbl_DC_Class_Section = new tbl_DC_Class_Section();
                                    var sectionId = Guid.NewGuid();
                                    objtbl_DC_Class_Section.CreatedDate = DateTime.Now;
                                    objtbl_DC_Class_Section.IsActive = true;
                                    objtbl_DC_Class_Section.SectionId = sectionId;
                                    objtbl_DC_Class_Section.SectionName = Convert.ToString(sections[sec]);
                                    objtbl_DC_Class_Section.Class_Id = Convert.ToInt32(classes.Class_Id);
                                    objtbl_DC_Class_Section.School_Id = sch.SchoolId;

                                    DbContext.tbl_DC_Class_Section.Add(objtbl_DC_Class_Section);
                                    var id = DbContext.SaveChanges();
                                }
                                else
                                {
                                    if (classSection.IsActive == false)
                                    {
                                        classSection.IsActive = true;
                                        DbContext.SaveChanges();

                                    }
                                }
                            }

                        }
                    }
                }
            }
            return RedirectToAction("GetSchoolList");
        }
        #region GetRegisteredUserReport
        public ActionResult GetRegisteredUserReport()
        {
            return View();
        }
        public ActionResult Uploadstudent()
        {
            return View();
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
        public ActionResult DownloadSampleStudentExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Student_Name");
            validationTable.Columns.Add("Email");
            validationTable.Columns.Add("Mobile");
            validationTable.TableName = "Student_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleStudent.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadStudentDetails(HttpPostedFileBase file, int board, int cls, Guid sec)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("Uploadstudent");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Student_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string mobile = dr["Mobile"].ToString();
                                string name = dr["Student_Name"].ToString();
                                Guid id = new Guid(Session["id"].ToString());
                                if (mobile != "" && name != "")
                                {
                                    var mob = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).FirstOrDefault();
                                    if (mob == null)
                                    {
                                        tbl_DC_Registration reg = new tbl_DC_Registration();
                                        reg.Mobile = mobile;
                                        reg.Customer_Name = name;
                                        reg.Email = dr["Email"].ToString();
                                        reg.SchoolId = id;
                                        reg.SectionId = sec;
                                        reg.Is_Active = true;
                                        reg.Is_Deleted = false;
                                        reg.Inserted_By = HttpContext.User.Identity.Name;
                                        reg.Inserted_Date = DateTime.Now;
                                        DbContext.tbl_DC_Registration.Add(reg);
                                        DbContext.SaveChanges();

                                        var regno2 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();
                                        tbl_DC_Registration_Dtl m = new tbl_DC_Registration_Dtl();
                                        m.Regd_ID = regno2.ToList()[0].Regd_ID;
                                        m.Regd_No = regno2.ToList()[0].Regd_No;
                                        m.Board_ID = board;
                                        m.Class_ID = cls;
                                        m.Inserted_Date = DateTime.Now;
                                        m.Inserted_By = regno2.ToList()[0].Regd_ID.ToString();
                                        m.Is_Active = true;
                                        m.Is_Deleted = false;
                                        DbContext.tbl_DC_Registration_Dtl.Add(m);
                                        DbContext.SaveChanges();

                                        int reg_num1 = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                                        var pri = DbContext.tbl_DC_Prefix.Where(x => x.PrefixType_ID == 2).Select(x => x.Prefix_Name).FirstOrDefault();
                                        string prifix = pri.ToString();

                                        //registration no. of student autogenerate
                                        if (reg_num1 == 0)
                                        {
                                            reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + 1;
                                        }
                                        else
                                        {
                                            int regnum = Convert.ToInt32(regno2.ToList()[0].Regd_ID);
                                            if (regnum > 0 && regnum <= 9)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 9 && regnum <= 99)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 99 && regnum <= 999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "000" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 999 && regnum <= 9999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "00" + Convert.ToString(regnum);
                                            }
                                            if (regnum > 9999 && regnum <= 99999)
                                            {
                                                reg.Regd_No = prifix + DateTime.Now.ToString("yyyyMMdd") + "0" + Convert.ToString(regnum);
                                            }
                                        }
                                        DbContext.Entry(reg).State = EntityState.Modified;
                                        DbContext.SaveChanges();

                                        //-----------Add details to User-Security Table
                                        tbl_DC_USER_SECURITY obj1 = new tbl_DC_USER_SECURITY();
                                        obj1.USER_NAME = mobile;
                                        obj1.ROLE_CODE = "C";
                                        obj1.ROLE_TYPE = 1;
                                        obj1.USER_CODE = mobile;
                                        obj1.IS_ACCEPTED = false;                               //-------------To Change---------------------
                                        obj1.STATUS = "D";              //--------------random password------------------
                                        obj1.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                                        DbContext.tbl_DC_USER_SECURITY.Add(obj1);
                                        DbContext.SaveChanges();

                                        var regno1 = DbContext.tbl_DC_Registration.Where(x => x.Mobile == mobile && x.Is_Active == true && x.Is_Deleted == false).Take(1).ToList();



                                        string ucode = mobile;
                                        tbl_DC_USER_SECURITY obj4 = DbContext.tbl_DC_USER_SECURITY.Where(x => x.USER_CODE == ucode).FirstOrDefault();
                                        obj4.PASSWORD = DigiChampsModel.Encrypt_Password.HashPassword("12345").ToString();
                                        obj4.STATUS = "A";
                                        obj4.IS_ACCEPTED = true;
                                        DbContext.Entry(obj4).State = EntityState.Modified;
                                        //FormsAuthentication.RedirectFromLoginPage(obj4.USER_NAME, false);
                                        DbContext.SaveChanges();
                                        int regd = regno1.ToList()[0].Regd_ID;
                                        tbl_DC_Registration obj5 = DbContext.tbl_DC_Registration.Where(x => x.Regd_ID == regd).FirstOrDefault();
                                        obj5.Is_Active = true;
                                        obj5.Is_Deleted = false;
                                        obj5.Modified_Date = DateTime.Now;
                                        DbContext.Entry(obj5).State = EntityState.Modified;
                                        DbContext.SaveChanges();

                                    }
                                }
                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Student Details Entered Successfully";
                    return RedirectToAction("Uploadstudent");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("Uploadstudent");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("Uploadstudent");
            }
        }
        #region-----------------school wise teacher details-------------------
        public ActionResult uploadTeachers()
        {
            return View();
        }
        public ActionResult GetallTeachers()
        {
            Guid? id = new Guid(Session["id"].ToString());
            return View(DbContext.Teachers.Where(a => a.Active == 1 && a.SchoolId == id).ToList());
        }
        [HttpGet]
        public ActionResult EditTeachers(long TeacherId)
        {
           
            Teacher _teachr = DbContext.Teachers.Where(x => x.TeacherId == TeacherId).FirstOrDefault();
            ViewBag.TeacherId = _teachr.TeacherId;
            ViewBag.Name = _teachr.Name;
            ViewBag.Email = _teachr.Email;            
            ViewBag.Mobile = _teachr.Mobile;
            ViewBag.DepartmentName = _teachr.DepartmentName;
            ViewBag.IsHead = _teachr.IsHead;
            ViewBag.Password = _teachr.Password;
            ViewBag.image = _teachr.Photo;
            return View();
           
        }
        [HttpPost]
        public ActionResult EditTeachers(long TeacherId, string Name, string Mobile, string Email,string Password)
        {
            try
            {
               
                var alldetail = DbContext.Teachers.Where(x => x.TeacherId == TeacherId).FirstOrDefault();
                string image = string.Empty;
                if (alldetail != null)
                {
                    alldetail.Name = Name;
                    alldetail.ModifiedOn = today;
                    alldetail.Mobile = Mobile;
                    alldetail.Email = Email;
                    alldetail.Password = Password;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Edit Profile ";
                    return RedirectToAction("GetallTeachers");
                }
            }
            catch
            {
                return View();
            }
            return View();
        }
        [HttpGet]
        public ActionResult CreateTeachers()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateTeachers(string Name, string Mobile, string Email)
        {
            try
            {
                var details= DbContext.Teachers.Where(a => a.Mobile == Mobile && a.Active==1).FirstOrDefault();
                var alldetail = DbContext.Teachers.Where(a => a.Mobile == Mobile && a.Active == 0).FirstOrDefault();
                if (details==null)
                {
                    Teacher obj = new Teacher();
                    obj.Name = Name;
                    obj.Mobile = Mobile;
                    obj.InsertedOn = today;
                    obj.ModifiedOn = today;
                    obj.Email = Email;
                    obj.Active = 1;
                    obj.Password = "12345";
                    DbContext.Teachers.Add(obj);
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Create Teacher ";
                    return RedirectToAction("CreateTeachers");
                }
               else if (alldetail != null)
                {                   
                    alldetail.Active = 1;
                    DbContext.Entry(alldetail).State = EntityState.Modified;
                    DbContext.SaveChanges();
                    TempData["SuccessMessage"] = " Successfully Teacher Mobile Number Active ";
                    return RedirectToAction("CreateTeachers");
                }
                else
                {
                    TempData["ErrorMessage"] = "Mobile No Already Exit.. ";
                    return RedirectToAction("CreateTeachers");
                }
            }
            catch
            {
                return View();
            }            
        }



        public ActionResult DeleteTeacher(int? id ,string Status)
        {
            try
            {
                if (id != null)
                {

                    if(Status=="D")
                    {
                        var Teacher_found = DbContext.Teachers.Where(x => x.TeacherId == id && x.Active == 1).FirstOrDefault();
                        if (Teacher_found != null)
                        {
                            var obj = DbContext.Teachers.Where(x => x.TeacherId == id).FirstOrDefault();
                            obj.ModifiedOn = today;
                            obj.Active = 0;
                            DbContext.Entry(obj).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            TempData["SuccessMessage"] = "Teacher deleted successfully.";
                            return RedirectToAction("GetallTeachers");

                        }
                        else
                        {
                            TempData["ErrorMessage"] = "Teacher can not be deleted because its in use.";
                        }
                    }
                    else if (Status == "A")
                    {
                        var Teacher_found = DbContext.Teachers.Where(x => x.TeacherId == id && x.Active == 0).FirstOrDefault();
                        if (Teacher_found != null)
                        {
                            var obj = DbContext.Teachers.Where(x => x.TeacherId == id).FirstOrDefault();
                            obj.ModifiedOn = today;
                            obj.Active = 1;
                            DbContext.Entry(obj).State = EntityState.Modified;
                            DbContext.SaveChanges();
                            TempData["SuccessMessage"] = "Teacher Active successfully.";
                            return RedirectToAction("GetallTeachers");

                        }
                        else
                        {
                            TempData["ErrorMessage"] = "Teacher can not be deleted because its in use.";
                        }
                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "No data found";
                }
            }
            catch (Exception ex)
            {
                TempData["ErrorMessage"] = "Something went wrong.";
            }
            return RedirectToAction("GetallTeachers");
        }


        #endregion
        public ActionResult DownloadSampleTeacherExcel()
        {
            XLWorkbook oWB = new XLWorkbook();

            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Teacher_Name");
            validationTable.Columns.Add("Mobile");
            validationTable.Columns.Add("Email");
            validationTable.TableName = "Teacher_Details";
            var worksheet = oWB.AddWorksheet(validationTable);

            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleTeacher.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadTeacherDetails(HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("uploadTeachers");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Teacher_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string t_name = dr["Teacher_Name"].ToString();
                                string mobile = dr["Mobile"].ToString();
                                string email = dr["Email"].ToString();
                                Guid? id = new Guid(Session["id"].ToString());
                                if (email != "" && t_name != "" && mobile != "")
                                {
                                    Teacher teacher = new Teacher();
                                    teacher.Active = 1;
                                    teacher.Email = email;
                                    teacher.InsertedOn = DateTime.Now;
                                    teacher.Mobile = mobile;
                                    teacher.ModifiedOn = DateTime.Now;
                                    teacher.Name = t_name;
                                    teacher.Password = "12345";
                                    teacher.StageId = 0;

                                    teacher.SchoolId = id;
                                    DbContext.Teachers.Add(teacher);
                                    DbContext.SaveChanges();
                                    //string emailMsg = "Hi, this is to inform you your school admin credential are Username " + email + " password " + teacher.Password + ".";
                                    //EmailHelper.SendEmail(email, "Admin Credentials", emailMsg);

                                }
                            }
                        }
                    }

                    TempData["SuccessMessage"] = "Teacher Details Entered Successfully";
                    return RedirectToAction("uploadTeachers");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("uploadTeachers");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("uploadTeachers");
            }
        }




        public JsonResult GetRegisteredUsersDetails(int? board, int? cls, Guid? sec)
        {
            Guid? school = new Guid(Session["id"].ToString());
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var schools = DbContext.tbl_DC_School_Info.ToList();
            var classs = DbContext.tbl_DC_Class.ToList();
            var sections = DbContext.tbl_DC_Class_Section.ToList();
            var boards = DbContext.tbl_DC_Board.ToList();
            var res = (from a in students
                       join b in std_dtls on a.Regd_ID equals b.Regd_ID
                       where a.SchoolId == school
                       //&& a.Regd_ID == 258
                       select new Schoolwise_User_Class
                       {
                           Regd_ID = a.Regd_ID,
                           Regd_No = a.Regd_No,
                           Customer_Name = a.Customer_Name,
                           SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                           Class_ID = b.Class_ID == null ? null : b.Class_ID,
                           SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                           Board_ID = b.Board_ID == null ? null : b.Board_ID,
                           boardname = b.Board_ID == null ? "" : boards.Where(c => c.Board_Id == b.Board_ID).FirstOrDefault().Board_Name,
                           schoolname = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? "" : schools.Where(c => c.SchoolId == a.SchoolId).FirstOrDefault().SchoolName,
                           Class_Name = b.Class_ID == null ? "" : classs.Where(c => c.Class_Id == b.Class_ID).FirstOrDefault().Class_Name,
                           //SectionName = (a.SectionId == null && a.SectionId == Guid.Empty ? "" : sections.Where(c => c.SectionId == a.SectionId).FirstOrDefault().SectionName,
                           Mobile = a.Mobile,
                           Email = a.Email
                       }).ToList();

            if (board != null)
            {
                res = res.Where(a => a.Board_ID == board).ToList();
            }
            if (cls != null)
            {
                res = res.Where(a => a.Class_ID == cls).ToList();
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec).ToList();
            }
            Session["Get_Registered_User"] = res;
            var json = Json(res, JsonRequestBehavior.AllowGet);
            json.MaxJsonLength = int.MaxValue;
            return json;
        }
        public ActionResult EditStudentDetails(int id)
        {
            var students = DbContext.tbl_DC_Registration.Where(a => a.Is_Active == true && a.Is_Deleted == false && a.Regd_ID == id).ToList();
            var std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var res = (from a in students
                       join b in std_dtls on a.Regd_ID equals b.Regd_ID
                       select new Schoolwise_User_Class
                       {
                           Regd_ID = a.Regd_ID,
                           Customer_Name = a.Customer_Name,
                           SchoolId = (a.SchoolId == null && a.SchoolId == Guid.Empty) ? Guid.Empty : a.SchoolId,
                           Class_ID = b.Class_ID == null ? null : b.Class_ID,
                           SectionId = (a.SectionId == null && a.SectionId == Guid.Empty) ? Guid.Empty : a.SectionId,
                           Board_ID = b.Board_ID == null ? null : b.Board_ID,
                           Mobile = a.Mobile,
                           Email = a.Email
                       }).FirstOrDefault();
            ViewBag.studentdet = res;
            ViewBag.boardlist = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            ViewBag.classlist = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Board_Id == res.Board_ID).Distinct().ToList();
            ViewBag.sectionlist = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == res.Class_ID).Distinct().ToList();

            return View();
        }
        [HttpPost]
        public ActionResult UpdateStudentDetails(int Regd_ID, int cls, Guid sec, int board, string Mobile, string Email, string Customer_Name)
        {

            tbl_DC_Registration students = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == Regd_ID).FirstOrDefault();
            if (students.Mobile != Mobile)
            {
                tbl_DC_USER_SECURITY security = DbContext.tbl_DC_USER_SECURITY.Where(a => a.USER_CODE == Mobile).FirstOrDefault();
                if (security != null)
                {
                    TempData["ErrorMessage"] = "Enter Another Mobile";
                }
                security.USER_CODE = Mobile;
                security.USER_NAME = Mobile;
                DbContext.SaveChanges();
                return RedirectToAction("EditStudentDetails", new { id = Regd_ID });
            }


            students.Customer_Name = Customer_Name;
            students.Email = Email;
            students.Mobile = Mobile;
            students.SectionId = sec;
            DbContext.SaveChanges();
            tbl_DC_Registration_Dtl std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == Regd_ID).FirstOrDefault();
            std_dtls.Board_ID = board;
            std_dtls.Class_ID = cls;
            DbContext.SaveChanges();
            return RedirectToAction("GetRegisteredUserReport", "School");
        }
        public ActionResult DeleteStudentDetails(int id)
        {
            tbl_DC_Registration students = DbContext.tbl_DC_Registration.Where(a => a.Regd_ID == id).FirstOrDefault();
            students.Is_Active = false;
            students.Is_Deleted = true;

            DbContext.SaveChanges();
            tbl_DC_Registration_Dtl std_dtls = DbContext.tbl_DC_Registration_Dtl.Where(a => a.Regd_ID == id).FirstOrDefault();
            std_dtls.Is_Active = false;
            std_dtls.Is_Deleted = true;

            DbContext.SaveChanges();
            return RedirectToAction("GetRegisteredUserReport", "School");
        }
        public JsonResult GetBoardList()
        {
            var board = DbContext.tbl_DC_Board.Where(x => x.Is_Active == true && x.Is_Deleted == false).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetClassListdtl(int id)
        {
            var board = DbContext.tbl_DC_Class.Where(x => x.Is_Active == true && x.Is_Deleted == false && x.Board_Id == id).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSectionListdtl(int id)
        {
            Guid? school = new Guid(Session["id"].ToString());
            var board = DbContext.tbl_DC_Class_Section.Where(x => x.IsActive == true && x.Class_Id == id && x.School_Id == school).Distinct().ToList();
            return Json(board, JsonRequestBehavior.AllowGet);
        }
        public class Schoolwise_User_Class
        {
            public int? Regd_ID { set; get; }
            public string Regd_No { get; set; }
            public string Customer_Name { get; set; }
            public Nullable<Guid> SchoolId { get; set; }
            public Nullable<int> Class_ID { get; set; }
            public Nullable<Guid> SectionId { get; set; }
            public Nullable<int> Board_ID { get; set; }
            public string boardname { get; set; }
            public string schoolname { get; set; }
            public string Class_Name { get; set; }
            public string SectionName { get; set; }
            public string Mobile { get; set; }
            public string Email { get; set; }

        }
        public ActionResult Exportallregisteredreportdata()
        {
            //CREATE OBJECT OF WORKBOOK
            XLWorkbook oWB = new XLWorkbook();
            //create worksheet for material table and add these columns in that MAS_MATERIAL table
            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Sl. No.");
            validationTable.Columns.Add("School Name");
            validationTable.Columns.Add("Regd No");
            validationTable.Columns.Add("Student Name");
            validationTable.Columns.Add("Board");
            validationTable.Columns.Add("Class");
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Mobile");

            validationTable.TableName = "Registered_Details";
            List<Schoolwise_User_Class> res = (Session["Get_Registered_User"]) as List<Schoolwise_User_Class>;
            int i = 0;
            foreach (var gs in res.ToList())
            {
                DataRow dr = validationTable.NewRow();
                dr["Sl. No."] = i + 1;
                dr["School Name"] = gs.schoolname;
                dr["Regd No"] = gs.Regd_No;
                dr["Student Name"] = gs.Customer_Name;
                dr["Board"] = gs.boardname;
                dr["Class"] = gs.Class_Name;
                dr["Section"] = gs.SectionName;
                dr["Mobile"] = gs.Mobile;
                validationTable.Rows.Add(dr);
                i += 1;
            }



            oWB.Worksheets.Add(validationTable);
            //oWB.SaveAs("User_Details.xlsx");
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Registered_User_Details.xlsx");
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.Charset = "";
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                oWB.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
            TempData["Message"] = "Successfully Downloaded Excel File";
            return RedirectToAction("GetRegisteredUserReport", "School");
        }
        #endregion
        #region Assign Subject To teacher

        public ActionResult UploadAssignSubjectToTeacher()
        {
            return View();
        }
        public ActionResult DownloadSampleAssignSubjectToTeacher(int board, int cls)
        {
            if (board == null)
            {
                TempData["ErrorMessage"] = "Select Board";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
            if (cls == null)
            {
                TempData["ErrorMessage"] = "Select Class";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
            XLWorkbook oWB = new XLWorkbook();
            Guid id = new Guid(Session["id"].ToString());
            var teacherlist = DbContext.Teachers.Where(a => a.Active == 1 && a.SchoolId == id).Select(a => (a.Name + "-" + a.Mobile)).ToList();

            DataTable gdt = new DataTable("Teacher");
            gdt.Columns.Add("Name");
            //Here i call a method, in this method i store the table data in list format. You can direct write the Linq query here...
            var g = teacherlist;
            foreach (var gs in g.ToList())
            {
                DataRow dr = gdt.NewRow();
                dr["Name"] = gs;
                gdt.Rows.Add(dr);
            }
            // COUNT TABLE DATA
            int lastCellNo1 = gdt.Rows.Count + 1;
            //ADD THAT DATATABLE IN WORKSHEET AND ADD THAT WORKSHEET IN WORKBOOK
            oWB.AddWorksheet(gdt);
            var worksheet1 = oWB.Worksheet(1);






            DataTable validationTable = new DataTable();
            validationTable.Columns.Add("Section");
            validationTable.Columns.Add("Subject");
            validationTable.Columns.Add("Teacher");

            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == cls && a.School_Id == id).ToList();
            var subjects = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == cls).ToList();

            foreach (var a in sectionlist)
            {
                foreach (var b in subjects)
                {
                    DataRow dr1 = validationTable.NewRow();
                    dr1["Section"] = a.SectionName;
                    dr1["Subject"] = b.Subject;
                    validationTable.Rows.Add(dr1);
                }
            }
            validationTable.TableName = "Assign_Details";
            var worksheet = oWB.AddWorksheet(validationTable);
            worksheet.Column(3).SetDataValidation().List(worksheet1.Range("A2:A" + lastCellNo1), true);
            worksheet1.Hide();
            Byte[] workbookBytes;
            MemoryStream ms = GetStream(oWB);
            workbookBytes = ms.ToArray();


            return File(workbookBytes, "application/ms-excel", "SampleAssignSubject.xlsx");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadAssignSubjectToTeacherList(HttpPostedFileBase file, int board, int cls)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    var allowedExtensions = new[] { ".xlsx", ".xls" };

                    var checkextension = Path.GetExtension(file.FileName).ToLower();
                    if (!allowedExtensions.Contains(checkextension))
                    {
                        TempData["ErrorMessage"] = "Select Excel File";
                        return RedirectToAction("UploadAssignSubjectToTeacher");
                    }


                    if (file != null && file.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Excels/" + file.FileName));
                        //save File
                        file.SaveAs(path);
                        string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);

                        OleDbConnection connection = new OleDbConnection();
                        connection.ConnectionString = excelConnectionString;
                        OleDbDataAdapter command = new OleDbDataAdapter("select * from [Assign_Details$]", connection);
                        // connection.Open();
                        // Create DbDataReader to Data Worksheet
                        DataTable dt = new DataTable();
                        command.Fill(dt);
                        Guid id = new Guid(Session["id"].ToString());
                        var sectiondata = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true && a.Class_Id == cls && a.School_Id == id).ToList();
                        var subjectdata = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Class_Id == cls).ToList();
                        var teacherdata = DbContext.Teachers.Where(a => a.Active == 1).ToList();

                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                string subject = dr["Subject"].ToString();
                                string section = dr["Section"].ToString();
                                string teacher = dr["Teacher"].ToString();
                                
                                if (subject != null && teacher != null && section != null)
                                {
                                    var mobile = teacher.Split('-')[1];
                                    var sectionlist = sectiondata.Where(a => a.SectionName == section).FirstOrDefault();
                                    var subjects = subjectdata.Where(a => a.Subject == subject).FirstOrDefault();
                                    var teacherlist = teacherdata.Where(a => a.Mobile == mobile).FirstOrDefault();


                                    tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Teacher_ID == teacherlist.TeacherId && a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sectionlist.SectionId && a.Subject_Id == subjects.Subject_Id).FirstOrDefault();
                                    if (ob != null)
                                    {
                                        ob.Is_Active = false;
                                        ob.Modified_Date = DateTime.Now;
                                        DbContext.SaveChanges();
                                    }
                                    ob = new tbl_DC_AssignSubjectToTeacher();
                                    ob.Board_Id = board;
                                    ob.Class_Id = cls;
                                    ob.Inserted_Date = DateTime.Now;
                                    ob.Is_Active = true;
                                    ob.Modified_Date = DateTime.Now;
                                    ob.SectionId = sectionlist.SectionId;
                                    ob.Subject_Id = subjects.Subject_Id;
                                    ob.Teacher_ID = teacherlist.TeacherId;
                                    DbContext.tbl_DC_AssignSubjectToTeacher.Add(ob);
                                    DbContext.SaveChanges();
                                }


                            }
                        }
                        if ((System.IO.File.Exists(path)))
                        {
                            System.IO.File.Delete(path);
                        }
                    }

                    TempData["SuccessMessage"] = "Subject Assigned Successfully";
                    return RedirectToAction("UploadAssignSubjectToTeacher");
                }
                else
                {
                    TempData["ErrorMessage"] = "Select Excel File";
                    return RedirectToAction("UploadAssignSubjectToTeacher");
                }
            }
            else
            {
                TempData["ErrorMessage"] = "Select Excel File";
                return RedirectToAction("UploadAssignSubjectToTeacher");
            }
        }




        public ActionResult GetAssignSubjectToTeacher(int? board, int? cls, Guid? sec, int? subject)
        {
            Guid id = new Guid(Session["id"].ToString());
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var teacherslist = DbContext.Teachers.Where(a => a.Active == 1).ToList();
            var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true).ToList();
            var res = (from a in assignteacherlist
                       join b in teacherslist on a.Teacher_ID equals b.TeacherId
                       join c in subjectlist on a.Subject_Id equals c.Subject_Id
                       join d in sectionlist on a.SectionId equals d.SectionId
                       join e in classlist on a.Class_Id equals e.Class_Id
                       join f in boardlist on a.Board_Id equals f.Board_Id
                       where b.SchoolId == id
                       select new AssignTeacherCls
                       {
                           AssignId = a.AssignSubjectToTeacher_Id,
                           TeacherId = a.Teacher_ID,
                           BoardId = a.Board_Id,
                           BoardName = f.Board_Name,
                           ClassId = a.Class_Id,
                           ClassName = e.Class_Name,
                           SchoolId = b.SchoolId,
                           SectionId = a.SectionId,
                           SectionName = d.SectionName,
                           SubjectId = a.Subject_Id,
                           SubjectName = c.Subject,
                           TeacherName = b.Name
                       }).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.BoardId == board.Value).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.ClassId == cls.Value).ToList();
                ViewBag.cls = cls.Value;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.SubjectId == subject.Value).ToList();
                ViewBag.subject = subject.Value;
            }
            return View(res);

        }
        public ActionResult DeleteAssignSubject(int id)
        {
            tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.AssignSubjectToTeacher_Id == id).FirstOrDefault();
            ob.Is_Active = false;
            DbContext.SaveChanges();
            return RedirectToAction("GetAssignSubjectToTeacher");
        }
        public ActionResult CreateAssignSubjectToTeacher()
        {
            Guid id = new Guid(Session["id"].ToString());

            return View();
        }
        public JsonResult GetTeachersList(int subject, Guid sec)
        {
            Guid id = new Guid(Session["id"].ToString());
            var teacherslist = DbContext.Teachers.Where(a => a.Active == 1 && a.SchoolId == id).ToList();

            return Json(teacherslist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateAssignSubjectDetails(int board, int cls, Guid sec, int[] subject, int[] tid)
        {
            ArrayList subjectList = new ArrayList(subject);
            ArrayList teacherList = new ArrayList(tid);

            foreach (int sub in subjectList)
            {

                foreach (int teacher in teacherList)
                {

                    tbl_DC_AssignSubjectToTeacher ob = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Teacher_ID == teacher && a.Is_Active == true && a.Board_Id == board && a.Class_Id == cls && a.SectionId == sec && a.Subject_Id == sub).FirstOrDefault();
                    if (ob != null)
                    {
                        ob.Is_Active = false;
                        ob.Modified_Date = DateTime.Now;
                        DbContext.SaveChanges();
                    }
                    ob = new tbl_DC_AssignSubjectToTeacher();
                    ob.Board_Id = board;
                    ob.Class_Id = cls;
                    ob.Inserted_Date = DateTime.Now;
                    ob.Is_Active = true;
                    ob.Modified_Date = DateTime.Now;
                    ob.SectionId = sec;
                    ob.Subject_Id = sub;
                    ob.Teacher_ID = teacher;
                    DbContext.tbl_DC_AssignSubjectToTeacher.Add(ob);
                    DbContext.SaveChanges();
                }

            }
            TempData["SuccessMessage"] = "Assign Subject To Teacher Created successfully.";
            return RedirectToAction("GetAssignSubjectToTeacher", "School");
        }
        #endregion
        #region Get_Doubt_Tracker
        public ActionResult Get_Doubt_Tracker(int? board, int? cls, Guid? sec, int? subject)
        {
            Guid id = new Guid(Session["id"].ToString());
            var res = DbContext.View_Get_Doubt_Tracker.Where(a => a.School_Id == id).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.Board_Id == board.Value).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.Class_Id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_Id == subject.Value).ToList();
                ViewBag.subject = subject.Value;
            }
            return View(res);
        }
        #endregion
        #region Get_Worksheet_Tracker
        public ActionResult Get_Worksheet_Tracker(int? board, int? cls, Guid? sec, int? subject)
        {
            Guid id = new Guid(Session["id"].ToString());
            var res = DbContext.View_Get_Worksheet_Tracker.Where(a => a.School_Id == id).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.Board_Id == board.Value).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.Class_Id == cls.Value).ToList();
                ViewBag.cls = cls.Value;
            }
            if (sec != null)
            {
                res = res.Where(a => a.SectionId == sec.Value).ToList();
                ViewBag.sec = sec.Value;
            }
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.Subject_Id == subject.Value).ToList();
                ViewBag.subject = subject.Value;
            }
            return View(res);
        }
        #endregion
        #region Upload Exam PDF
        public ActionResult Exam_PDF_List(int? board, int? cls, int? subject)
        {
            Guid id = new Guid(Session["id"].ToString());
            var boardlist = DbContext.tbl_DC_Board.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var classlist = DbContext.tbl_DC_Class.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
          //  var sectionlist = DbContext.tbl_DC_Class_Section.Where(a => a.IsActive == true).ToList();
            var subjectlist = DbContext.tbl_DC_Subject.Where(a => a.Is_Active == true && a.Is_Deleted == false).ToList();
            var exampdflist = DbContext.tbl_DC_PdfExam.Where(a => a.Is_Active == true).ToList();
           // var assignteacherlist = DbContext.tbl_DC_AssignSubjectToTeacher.Where(a => a.Is_Active == true).ToList();
            var res = (from a in exampdflist
                       join c in subjectlist on a.Subject_Id equals c.Subject_Id
                       join e in classlist on a.Class_Id equals e.Class_Id
                      // join d in sectionlist on a.SectionId equals d.SectionId                       
                       join f in boardlist on a.Board_Id equals f.Board_Id
                       where a.School_Id == id
                       select new ExamPDFCls
                       {
                           Name=a.Name,
                           PdfExam_Id = a.PdfExam_Id,
                           StartTime = a.StartTime,
                           EndTime=a.EndTime,
                           Total_Mark=a.Total_Mark,
                           BoardId = a.Board_Id,
                           BoardName = f.Board_Name,
                           ClassId = a.Class_Id,
                           ClassName = e.Class_Name,
                           SchoolId = a.School_Id,
                          // SectionId = a.SectionId,
                          // SectionName = a.SectionId==Guid.Empty?"All": sectionlist.Where(m=>m.SectionId==a.SectionId).FirstOrDefault().SectionName,
                           SubjectId = a.Subject_Id,
                           SubjectName = c.Subject,
                           FilePath ="/Images/Exampdf/"+a.File_Name
                       }).ToList();
            ViewBag.board = null;
            ViewBag.cls = null;
            ViewBag.sec = null;
            ViewBag.subject = null;
            if (board != null && board != 0)
            {
                res = res.Where(a => a.BoardId == board.Value).ToList();
                ViewBag.board = board.Value;
            }
            if (cls != null && cls != 0)
            {
                res = res.Where(a => a.ClassId == cls.Value).ToList();
                ViewBag.cls = cls.Value;
            }
            //if (sec != null && sec != Guid.Empty)
            //{
            //    res = res.Where(a => a.SectionId == sec.Value).ToList();
            //    ViewBag.sec = sec.Value;
            //}
            if (subject != null && subject != 0)
            {
                res = res.Where(a => a.SubjectId == subject.Value).ToList();
                ViewBag.subject = subject.Value;
            }
            return View(res);
        }
        public ActionResult Upload_Exam_PDF()
        {
            Guid id = new Guid(Session["id"].ToString());

            return View();
        }
        [HttpPost]
        public ActionResult Save_Upload_Exam_PDF(int board, int cls, int subject, DateTime StartTime,string name, DateTime EndTime, int Total_Mark, HttpPostedFileBase examfile)
        {
            Guid id = new Guid(Session["id"].ToString());
            tbl_DC_PdfExam obj = new tbl_DC_PdfExam();
            obj.Board_Id = board;
            obj.Class_Id = cls;
            obj.EndTime = EndTime;
            obj.Inserted_Date = DateTime.Now;
            obj.Is_Active = true;
            obj.Is_Deleted = false;
            obj.Modified_Date = DateTime.Now;
            obj.School_Id=id;
            //obj.SectionId = sec;
            obj.StartTime = StartTime;
            obj.Subject_Id = subject;
            obj.Name = name;
            obj.Total_Mark = Total_Mark;
            string guid = Guid.NewGuid().ToString();
            if (examfile != null)
            {
                var postedfile = examfile;
                var path = Path.Combine(Server.MapPath("~/Images/Exampdf/"), (guid+postedfile.FileName));
                postedfile.SaveAs(path);
                //docfile.Add(path);
                obj.File_Name = guid + postedfile.FileName;
            }
            DbContext.tbl_DC_PdfExam.Add(obj);
            DbContext.SaveChanges();
            TempData["SuccessMessage"] = "Uploaded Successfully";
            return RedirectToAction("Exam_PDF_List", "School");
        }

        public ActionResult Delete_Upload_Exam_PDF(int id)
        {
            tbl_DC_PdfExam ob = DbContext.tbl_DC_PdfExam.Where(a => a.PdfExam_Id == id).FirstOrDefault();
            ob.Is_Active = false;
            ob.Is_Deleted = true;
            DbContext.SaveChanges();
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Exam_PDF_List");
        }

        #endregion
    }


}
