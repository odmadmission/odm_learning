﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DigiChamps.Reports
{
    public partial class Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SchoolCls scl = new SchoolCls();
            DigiChampsEntities DbContext = new DigiChampsEntities();
            string type = Request.QueryString["type"].ToString();
            switch (type)
            {
                case "School_Usage_Report":
                    DateTime f_Date = Convert.ToDateTime(Request.QueryString["f_Date"].ToString());
                    DateTime t_Date = Convert.ToDateTime(Request.QueryString["t_Date"].ToString());
                    int C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    string S_Id = Request.QueryString["S_Id"].ToString();
                    Guid school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    DataSet ds = scl.GetReportData(f_Date, t_Date, C_Id, S_Id, school_id);
                    // ds.WriteXml(@"D:\parismita\XML_FILES\Usage_Report.xml");


                    ReportDocument rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_UsageReport.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    TextObject txtObj = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txtObj.Text = "From :" + Convert.ToDateTime(f_Date).ToString("dd/MM/yyyy") + "   To :" + Convert.ToDateTime(t_Date).ToString("dd/MM/yyyy");

                    TextObject txtObj1 = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                    txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "TOTAL USERS (ALL STUDENTS)" : "TOTAL USERS (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    TextObject txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    //ChartObject chartControl1=(ChartObject)rd.Subreports[1].ReportDefinition.Sections["]
                    //this.chartControl1.PrimaryXAxis.LabelRotate = true;
                    //this.chartControl1.PrimaryXAxis.LabelRotateAngle = 135;

                    ////Alignment for axis labels
                    //this.chartControl1.PrimaryXAxis.LabelAlignment = StringAlignment.Near;

                    System.IO.Stream m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byte[] byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);

                    break;
                case "School_Usage_Report_Excel":

                    f_Date = Convert.ToDateTime(Request.QueryString["f_Date"].ToString());
                    t_Date = Convert.ToDateTime(Request.QueryString["t_Date"].ToString());
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    ds = scl.GetReportData(f_Date, t_Date, C_Id, S_Id, school_id);

                    // ds.WriteXml(@"D:\parismita\XML_FILES\Usage_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_UsageReport.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;

                    txtObj = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text1"];
                    txtObj.Text = "From :" + Convert.ToDateTime(f_Date).ToString("dd/MM/yyyy") + "   To :" + Convert.ToDateTime(t_Date).ToString("dd/MM/yyyy");

                    txtObj1 = (TextObject)rd.ReportDefinition.Sections["Section1"].ReportObjects["Text2"];
                    txtObj1.Text = Convert.ToInt32(C_Id) == 0 ? "TOTAL USERS (ALL STUDENTS)" : "TOTAL USERS (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";

                    txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";


                    rd.ExportToHttpResponse(ExportFormatType.Excel, Response, true, "Usage Report");
                    Response.End();

                    break;
                case "School_Test_Report":
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    int Sub_Id = Convert.ToInt32(Request.QueryString["Sub_Id"].ToString());
                    int Chap_Id = Convert.ToInt32(Request.QueryString["Chap_Id"].ToString());
                    int T_Id = Convert.ToInt32(Request.QueryString["T_Id"].ToString());
                    int B_Id = Convert.ToInt32(Request.QueryString["B_Id"].ToString());
                    ds = new DataSet();
                    ds = scl.GetSchoolTestReport(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);

                    //   ds.WriteXml(@"D:\parismita\XML_FILES\Test_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_Test_Report.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;
                    int s = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Max(a => a.Question_Nos).Value;
                    //int g = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Sum(a => a.Total_Correct_Ans).Value;
                    //int t = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Count();

                    string avg = DbContext.Database.SqlQuery<string>(@"SELECT [odm_lms].[dbo].[FN_DC_GET_AVERAGE_CLASS_PERCENTAGE](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id.ToString()), new SqlParameter("@SectionId", S_Id)).FirstOrDefault();

                    txtObj = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text8"];
                    txtObj.Text = "FULL MARKS: " + s.ToString();

                    txtObj1 = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text11"];
                    txtObj1.Text = "CLASS AVERAGE PERCENTAGE: " + avg;

                    //txtObj2 = (TextObject)rd.Subreports[0].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text1"];
                    //txtObj2.Text = Convert.ToInt32(C_Id) == 0 ? "ACTIVITY TIME (ALL STUDENTS)" : "ACTIVITY TIME (" + DbContext.tbl_DC_Class.ToList().Where(a => a.Class_Id == Convert.ToInt32(C_Id)).FirstOrDefault().Class_Name + ")";


                    m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);
                    break;
                case "School_Test_Report_Excel":
                    C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                    S_Id = Request.QueryString["S_Id"].ToString();
                    school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                    Sub_Id = Convert.ToInt32(Request.QueryString["Sub_Id"].ToString());
                    Chap_Id = Convert.ToInt32(Request.QueryString["Chap_Id"].ToString());
                    T_Id = Convert.ToInt32(Request.QueryString["T_Id"].ToString());
                    B_Id = Convert.ToInt32(Request.QueryString["B_Id"].ToString());
                    ds = new DataSet();
                    ds = scl.GetAllSchoolTestReport(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id);

                   //  ds.WriteXml(@"C:\Code\New folder\Digi\XML_FILES\All_Test_Report.xml");
                    rd = new ReportDocument();
                    rd.Load(Server.MapPath("~/Reports/School/School_All_Test_Report.rpt"));
                    rd.SetDataSource(ds);
                    CrystalReportViewer1.ReportSource = rd;
                    // s = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Max(a => a.Question_Nos).Value;
                    //int g = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Sum(a => a.Total_Correct_Ans).Value;
                    //int t = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Count();
                    //  avg = DbContext.Database.SqlQuery<string>(@"SELECT [odm_lms].[dbo].[FN_DC_GET_AVERAGE_CLASS_PERCENTAGE](@Exam_ID,@Board_Id,@Class_Id,@Subject_Id,@Chapter_Id,@SchoolId,@SectionId)", new SqlParameter("@Exam_ID", T_Id), new SqlParameter("@Board_Id", B_Id), new SqlParameter("@Class_Id", C_Id), new SqlParameter("@Subject_Id", Sub_Id), new SqlParameter("@Chapter_Id", Chap_Id), new SqlParameter("@SchoolId", school_id.ToString()), new SqlParameter("@SectionId", S_Id)).FirstOrDefault();

                    // avg = Convert.ToDecimal((g / (s * t)) * 100) + " %";

                    //txtObj = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text8"];
                    //txtObj.Text ="FULL MARKS: "+s.ToString();

                    //txtObj1 = (TextObject)rd.Subreports[1].ReportDefinition.Sections["ReportHeaderSection1"].ReportObjects["Text11"];
                    //txtObj1.Text = "CLASS AVERAGE PERCENTAGE: " + avg;
                    m = null;
                    m = rd.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                    byteArray = null;
                    byteArray = new byte[m.Length];
                    m.Read(byteArray, 0, Convert.ToInt32(m.Length - 1));
                    rd.Close();
                    rd.Dispose();
                    Response.ClearContent();
                    Response.ClearHeaders();
                    Response.Buffer = true;
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(byteArray);

                    break;

            }
        }
    }
}