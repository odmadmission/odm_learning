﻿using DigiChamps.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;
using System.Text.RegularExpressions;

namespace DigiChamps.Reports.School
{
    public partial class Test_Reports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int C_Id = Convert.ToInt32(Request.QueryString["C_Id"].ToString());
                string S_Id = Request.QueryString["S_Id"].ToString();
                Guid school_id = Guid.Parse(Request.QueryString["school_id"].ToString());
                int Sub_Id = Convert.ToInt32(Request.QueryString["Sub_Id"].ToString());
                int Chap_Id = Convert.ToInt32(Request.QueryString["Chap_Id"].ToString());
                int T_Id = Convert.ToInt32(Request.QueryString["T_Id"].ToString());
                int B_Id = Convert.ToInt32(Request.QueryString["B_Id"].ToString());
                SchoolCls scl = new SchoolCls();
                DigiChampsEntities DbContext = new DigiChampsEntities();
                DataTable dt1 = new DataTable();
                dt1 = scl.ConvertToDataTable(scl.GetTestMarkData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id));
                dt1.TableName = "All_Test_Mark";



                //storing total rows count to loop on each Record  
                string[] XPointMember = new string[dt1.Rows.Count];
                decimal[] YPointMember = new decimal[dt1.Rows.Count];

                for (int count = 0; count < dt1.Rows.Count; count++)
                {
                    //storing Values for X axis  
                    XPointMember[count] = dt1.Rows[count]["Student_Name"].ToString();
                    //storing values for Y Axis  
                    YPointMember[count] = Convert.ToDecimal(dt1.Rows[count]["Mark"]);


                }
                LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = YPointMember });
                LineChart1.CategoriesAxis = string.Join(",", XPointMember);


                var s = scl.GetSchoolData(school_id).FirstOrDefault();
                lblschoolname.Text = s.SchoolName;
                imglogo.ImageUrl = s.SchoolLogo;



                var m = scl.GetMarkDetails(C_Id, S_Id, Sub_Id, Chap_Id, T_Id, B_Id).FirstOrDefault();
                lblclass.Text = m.Class_Name;
                lblsection.Text = m.Section_Name;
                lbltest.Text = m.Test_Name;
                lblchapter.Text = m.Chapter_Name;


                var ir = scl.GetTestindividualData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id).ToList();
                var r = (from a in ir.ToList()
                         select new
                         {
                             a.Student_Name,
                             a.Mark,
                             a.Regd_ID
                         }).ToList().Distinct();
                gvindividual.DataSource = r;
                gvindividual.DataBind();

                for (int i = 0; i < gvindividual.Rows.Count; i++)
                {
                    Label regdid = (Label)gvindividual.Rows[i].FindControl("lblid");
                    DataList dl = (DataList)gvindividual.Rows[i].FindControl("ddltopics");
                    int id = Convert.ToInt32(regdid.Text);
                    var b = (from a in ir.ToList()
                             where a.Regd_ID == id
                             select new
                             {
                                 a.Topic_Name
                             }).ToList().Distinct();
                    dl.DataSource = b;
                    dl.DataBind();
                }


                var t = scl.GetTestcumulativeData(C_Id, S_Id, school_id, Sub_Id, Chap_Id, T_Id, B_Id).ToList();
                gvcumulative.DataSource = t;
                gvcumulative.DataBind();
                for (int i = 0; i < gvcumulative.Rows.Count; i++)
                {
                    Label regdid = (Label)gvcumulative.Rows[i].FindControl("lblid");
                    DataList dl = (DataList)gvcumulative.Rows[i].FindControl("ddltopics");
                    int id = Convert.ToInt32(regdid.Text);
                    var b = (from a in ir.ToList()
                             where a.Topic_ID == id
                             select new
                             {
                                 a.Student_Name
                             }).ToList().Distinct();
                    dl.DataSource = b;
                    dl.DataBind();
                }

                int su = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Max(a => a.Question_Nos).Value;
                int g = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Sum(a => a.Total_Correct_Ans).Value;
                int to = DbContext.tbl_DC_Exam_Result.Where(a => a.Exam_ID == T_Id && a.Class_Id == C_Id && a.Subject_Id == Sub_Id && a.Chapter_Id == Chap_Id && a.Is_Active == true && a.Is_Deleted == false).ToList().Count();

                string avg = Convert.ToDecimal((g * 100) / (su * to)) + " %";

                lblfullmark.Text = su.ToString();
                lblpercentage.Text = avg;

               // Page.ClientScript.RegisterStartupScript(this.GetType(),"CallMyFunction", "printDiv("+print.ClientID+");", true);
                //Response.ContentType = "application/pdf";
                //Response.AddHeader("content-disposition", "attachment;filename=TestPage.pdf");
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //StringWriter sw = new StringWriter();
                //HtmlTextWriter hw = new HtmlTextWriter(sw);
                //this.Page.RenderControl(hw);
                //StringReader sr = new StringReader(sw.ToString());
                //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                //pdfDoc.Open();
                //htmlparser.Parse(sr);
                //pdfDoc.Close();
                //Response.Write(pdfDoc);
                //Response.End();

                //    Response.ContentType = "application/pdf";
                //    Response.AddHeader("content-disposition", "attachment;filename=FileName.pdf");
                //    Response.Cache.SetCacheability(HttpCacheability.NoCache);

                //    StringWriter sw = new StringWriter();

                //    HtmlTextWriter w = new HtmlTextWriter(sw);
                //    print.RenderControl(w);

                //    string htmWrite = sw.GetStringBuilder().ToString();
                //    htmWrite = Regex.Replace(htmWrite, "</?(a|A).*?>", "");
                //    htmWrite = htmWrite.Replace("\r\n", "");
                //    StringReader reader = new StringReader(htmWrite);

                //    Document doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
                //    string pdfFilePath = Server.MapPath(".") + "/PDFFiles";

                //    HTMLWorker htmlparser = new HTMLWorker(doc);
                //    PdfWriter.GetInstance(doc, Response.OutputStream);

                //    doc.Open();
                //    try
                //    {
                //        htmlparser.Parse(reader);
                //        doc.Close();
                //        Response.Write(doc);
                //        Response.End();
                //    }
                //    catch (Exception ex)
                //    { }
                //    finally
                //    {
                //        doc.Close();
                //    }  
                //}
            }
        }

       
    }
}