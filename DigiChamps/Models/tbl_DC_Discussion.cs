//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_Discussion
    {
        public tbl_DC_Discussion()
        {
            this.tbl_DC_DiscussionDetail = new HashSet<tbl_DC_DiscussionDetail>();
        }
    
        public int DiscussionID { get; set; }
        public string Title { get; set; }
        public int RoleID { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDelete { get; set; }
        public System.Guid SchoolID { get; set; }
    
        public virtual ICollection<tbl_DC_DiscussionDetail> tbl_DC_DiscussionDetail { get; set; }
    }
}
