//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_Testimonial
    {
        public int Testimonial_Id { get; set; }
        public string Testimonial_Name { get; set; }
        public string Testimonial_Title { get; set; }
        public string Testimonial_Images { get; set; }
        public string TotalMark { get; set; }
        public Nullable<int> Class_ID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Inserted_On { get; set; }
        public string Inserted_By { get; set; }
        public Nullable<bool> Is_Active { get; set; }
        public Nullable<bool> Is_Delete { get; set; }
    }
}
