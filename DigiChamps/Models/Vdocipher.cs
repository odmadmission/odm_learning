﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Threading.Tasks;
namespace DigiChamps.Models
{
    public class Vdocipher
    {
        public   string videoupload(string url, string key, string XAmzCredential,
            string XAmzAlgorithm, string XAmzDate, string Policy, string XAmzSignature, string success_action_status, string success_action_redirect, Stream filestream, string filename)
        {
            HttpContent stringContent0 = new StringContent(key);
            HttpContent stringContent7 = new StringContent(XAmzCredential);
            HttpContent stringContent1 = new StringContent(XAmzAlgorithm);
            HttpContent stringContent2 = new StringContent(XAmzDate);
            HttpContent stringContent3 = new StringContent(Policy);
            HttpContent stringContent4 = new StringContent(XAmzSignature);
            HttpContent stringContent5 = new StringContent(success_action_status);
            HttpContent stringContent6 = new StringContent(success_action_redirect);
            // examples of converting both Stream and byte [] to HttpContent objects
            // representing input type file
            HttpContent fileStreamContent = new StreamContent(filestream);
            //HttpContent bytesContent = new ByteArrayContent(file.ContentLength);

            // Submit the form using HttpClient and 
            // create form data as Multipart (enctype="multipart/form-data")

            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                // Add the HttpContent objects to the form data

                // <input type="text" name="filename" />
                formData.Add(stringContent0, "key", key);
                formData.Add(stringContent7, "X-Amz-Credential", XAmzCredential);
                formData.Add(stringContent1, "X-Amz-Algorithm", XAmzAlgorithm);
                formData.Add(stringContent2, "X-Amz-Date", XAmzDate);
                formData.Add(stringContent3, "Policy", Policy);
                formData.Add(stringContent4, "X-Amz-Signature", XAmzSignature);
                formData.Add(stringContent5, "success_action_status", success_action_status);
                formData.Add(stringContent6, "success_action_redirect", success_action_redirect);
                // <input type="file" name="file1" />
                formData.Add(fileStreamContent, "file", filename);
                // <input type="file" name="file2" />

                client.Timeout = TimeSpan.FromMinutes(60);
                var response =  client.PostAsync(url, formData).Result;






                // Actually invoke the request to the server


               

                // equivalent to (action="{url}" method="post")
                
               // PostAsync(client, url, formData);

                // equivalent of pressing the submit button on the form

                return null;
            }


        }


    //    public async Task<string> PostAsync(HttpClient client,string uri, MultipartFormDataContent data)
    //     {

    //         var response = await client.PostAsync(uri, data);

    //response.EnsureSuccessStatusCode();

    //string content = await response.Content.ReadAsStringAsync();
    //return await Task.Run(() =>content);
                 
    //    }
    }



    //class UploadFileMPUHighLevelAPITest
    //{
    //    private const string bucketName = "*** provide bucket name ***";
    //    private const string keyName = "*** provide a name for the uploaded object ***";
    //    private const string filePath = "*** provide the full path name of the file to upload ***";
    //    // Specify your bucket region (an example region is shown).
    //    private static readonly RegionEndpoint bucketRegion = RegionEndpoint.USWest2;
    //    private static IAmazonS3 s3Client;

    //    public static void Main()
    //    {
    //        s3Client = new AmazonS3Client(bucketRegion);
    //        UploadFileAsync().Wait();
    //    }

    //    private static async Task UploadFileAsync()
    //    {
    //        try
    //        {
    //            var fileTransferUtility =
    //                new TransferUtility(s3Client);

    //            // Option 1. Upload a file. The file name is used as the object key name.
    //            await fileTransferUtility.UploadAsync(filePath, bucketName);
    //            Console.WriteLine("Upload 1 completed");

    //            // Option 2. Specify object key name explicitly.
    //            await fileTransferUtility.UploadAsync(filePath, bucketName, keyName);
    //            Console.WriteLine("Upload 2 completed");

    //            // Option 3. Upload data from a type of System.IO.Stream.
    //            using (var fileToUpload =
    //                new FileStream(filePath, FileMode.Open, FileAccess.Read))
    //            {
    //                await fileTransferUtility.UploadAsync(fileToUpload,
    //                                           bucketName, keyName);
    //            }
    //            Console.WriteLine("Upload 3 completed");

    //            // Option 4. Specify advanced settings.
    //            var fileTransferUtilityRequest = new TransferUtilityUploadRequest
    //            {
    //                BucketName = bucketName,
    //                FilePath = filePath,
    //                StorageClass = S3StorageClass.StandardInfrequentAccess,
    //                PartSize = 6291456, // 6 MB.
    //                Key = keyName,
    //                CannedACL = S3CannedACL.PublicRead
    //            };
    //            fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
    //            fileTransferUtilityRequest.Metadata.Add("param2", "Value2");

    //            await fileTransferUtility.UploadAsync(fileTransferUtilityRequest);
    //            Console.WriteLine("Upload 4 completed");
    //        }
    //        catch (AmazonS3Exception e)
    //        {
    //            Console.WriteLine("Error encountered on server. Message:'{0}' when writing an object", e.Message);
    //        }
    //        catch (Exception e)
    //        {
    //            Console.WriteLine("Unknown encountered on server. Message:'{0}' when writing an object", e.Message);
    //        }

    //    }
    //}
    
}