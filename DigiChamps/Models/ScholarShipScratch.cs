﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class ScholarShipScratch
    {
        public int? ID { get; set; }
        public int? StudentID { get; set; }
        public int? ScholarShipID { get; set; }
        public bool? Active { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? InsertDate { get; set; }

        public int? CartSize { get; set; }
    }
}