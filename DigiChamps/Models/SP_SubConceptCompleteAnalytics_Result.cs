//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    
    public partial class SP_SubConceptCompleteAnalytics_Result
    {
        public int Subject_Id { get; set; }
        public string Subject { get; set; }
        public Nullable<int> Chapter_Id { get; set; }
        public string Chapter { get; set; }
        public Nullable<int> PRT_Question_nos { get; set; }
        public Nullable<int> PRT_Correct_Ans { get; set; }
        public Nullable<int> CBT_Question_nos { get; set; }
        public Nullable<int> CBT_Correct_Ans { get; set; }
    }
}
