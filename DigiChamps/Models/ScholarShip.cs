﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace DigiChamps.Models
{
    
    public class ScholarShip
    {
        
        public int? ID { get; set; }
        public int? Discount { get; set; }
        public int? Validity { get; set; }
        public bool? Active { get; set; }
        public bool? Deleted { get; set; }
        public DateTime? InsertDate { get; set; }
    }
}