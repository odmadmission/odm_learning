﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace DigiChamps.Models
{
    public class PushNotiStatus
    {

        public PushNotiStatus(string bodys, string deviceId)
        {
            try
            {
                string applicationID = "AIzaSyCCq1c6Y6PXYiIGG9dG4JOIQ9_yaa2a8AQ";

                string senderId = "117057854111";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = new string[]{deviceId},

                    data = new

                    {

                        body = bodys,

                        title = "",

                       

                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                //string str = ex.Message;
                throw ex;

            }
        }
        public PushNotiStatus(string TITLE, string bodys, string deviceId)
        {
            try
            {
                string applicationID = "AAAAG0ExGp8:APA91bEaIJ32-rJPsw90yyDr9QsHNmXa3GyBSt0cIOLdd3wgoH_D9b_YU9AUWWebANXEkHVt5x2c0lbqFXcMLDym3uXFjednxB9ypTlXye5j0OrpjlbhBvdr6dym9DV9tr0pW7Gajr1W";

                string senderId = "117057854111";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = new string[] { deviceId },

                    data = new

                    {

                        body = bodys,

                        title = TITLE,



                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                //string str = ex.Message;
                throw ex;

            }
        }

        public PushNotiStatus()
        {
            // TODO: Complete member initialization
        }


    }

    public class PushNotiStatusMaster
    {
        public PushNotiStatusMaster(string title, string bodyss, string image, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        icon = image

                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusMaster()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusPackage
    {
        public PushNotiStatusPackage(string title, string bodyss, string image, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusPackage()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusVideo
    {
        public PushNotiStatusVideo(string title, string bodyss, string image, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        icon = image,

                        //video = video

                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusVideo()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusMentor
    {
        public PushNotiStatusMentor(string title, string bodyss, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        //icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusMentor()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusMentorTask
    {
        public PushNotiStatusMentorTask(string title, string bodyss, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        //icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusMentorTask()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusMentorTaskStatus
    {
        public PushNotiStatusMentorTaskStatus(string title, string bodyss, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        //icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusMentorTaskStatus()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusMentorTaskStatusOverDue
    {
        public PushNotiStatusMentorTaskStatusOverDue(string title, string bodyss, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        //icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusMentorTaskStatusOverDue()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusVideoDIY
    {
        public PushNotiStatusVideoDIY(string title, string bodyss, string image, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        icon = image,

                        //video = video

                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        public PushNotiStatusVideoDIY()
        {
            // TODO: Complete member initialization
        }
    }

    public class PushNotiStatusNorification
    {
        public PushNotiStatusNorification(string title, string bodyss, string image, List<string> Listextension)
        {
            try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    registration_ids = Listextension,

                    data = new

                    {

                        body = bodyss,

                        title = title,

                        icon = image
                    }

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }


        public PushNotiStatusNorification(DataMessage dm,String deviceId,String msg)
        {
          
             try
            {
                string applicationID = "AIzaSyD9lb6427EJTG0Frw61Bnc4fnwUHgk0JIs";

                string senderId = "226584391764";



                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.UseDefaultCredentials = true;

                tRequest.PreAuthenticate = true;

                tRequest.Credentials = CredentialCache.DefaultCredentials;


                tRequest.Method = "POST";

                tRequest.ContentType = "application/json";

                var data = new

                {

                    to = deviceId,

                    data = dm,
                    //notification = new

                    //{

                    //    body = bodyss,

                    //    title = title,

                    //    icon = image
                    //}

                };

                var serializer = new JavaScriptSerializer();

                var json = serializer.Serialize(data);

                Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                tRequest.ContentLength = byteArray.Length;


                using (Stream dataStream = tRequest.GetRequestStream())
                {

                    dataStream.Write(byteArray, 0, byteArray.Length);


                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {

                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {

                                String sResponseFromServer = tReader.ReadToEnd();

                                string str = sResponseFromServer;

                            }
                        }
                    }
                }
            }

            catch (Exception ex)
            {

                string str = ex.Message;

            }
        }

        }

       
   


}
