//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigiChamps.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_DC_Mentorship_Exam_Score
    {
        public int Result_ID { get; set; }
        public Nullable<int> Academic { get; set; }
        public Nullable<int> Doubt { get; set; }
        public Nullable<int> Interest { get; set; }
        public Nullable<int> Time { get; set; }
        public Nullable<int> SelfStudy { get; set; }
        public Nullable<int> School { get; set; }
        public Nullable<int> Personal { get; set; }
        public Nullable<int> Mentorship { get; set; }
        public Nullable<int> Total { get; set; }
        public Nullable<int> ExamResultID { get; set; }
    }
}
