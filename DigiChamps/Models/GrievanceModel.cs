﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiChamps.Models
{
    public class GrievanceModel
    {
        public int ID { get; set; }
        public int? TeacherId { get; set; }
        public int? StudentId { get; set; }
        public int? ModuleId { get; set; }
        public string ModuleName { get; set; }
        public int ClassId { get; set; }
        public int SectionId { get; set; }
        public int SubjectId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public int ResolvedId { get; set; }
        public Nullable<DateTime> ResolvedDateTime { get; set; }
        public string InsertedId { get; set; }
        public Nullable<DateTime> InsertedDateTime { get; set; }
        public Nullable<bool> Active { get; set; }

        public HttpPostedFileBase[] Griv_img { get; set; }
        public string FileName { get; set; }
    }

    public class GrivAttachments
    {
        public int ID { get; set; }
        public int GrivID { get; set; }
    }
    public class GrivAttachmentModel
    { 
        public Grievance  grievances { get; set; }
        public List<GrievanceAttachment> attachments { get; set; }
    }
}