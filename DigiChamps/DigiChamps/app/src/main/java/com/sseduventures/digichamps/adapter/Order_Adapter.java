package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.Model_Order;
import com.sseduventures.digichamps.R;

import com.sseduventures.digichamps.activity.Order_details_page;
import com.sseduventures.digichamps.domain.Order;
import com.sseduventures.digichamps.utils.DateTimeUtil;

import java.util.List;

public class Order_Adapter extends RecyclerView.Adapter<Order_Adapter.MyViewHolder> {


    private Context context;
    private List<Order> moviesList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date, packages, grand_total, order_no;
        CardView container;

        public MyViewHolder(View view) {
            super(view);
            container = (CardView) itemView.findViewById(R.id.pack);
            date = (TextView) view.findViewById(R.id.clas);
            grand_total = (TextView) view.findViewById(R.id.pric);
            packages = (TextView) view.findViewById(R.id.chap);
            order_no = (TextView) view.findViewById(R.id.sub);
        }
    }


    public Order_Adapter(List<Order> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_order, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Order movie = moviesList.get(position);
        Log.v("TAG", movie.getOrder_No() + "");
        holder.date.setText(
                DateTimeUtil.convertMillisAsDateTimeString(
                        DateTimeUtil.StringDoubtDateStrToDate(movie.getOrder_date()).getTime()));
        holder.order_no.setText(movie.getOrder_No() + "");
        holder.packages.setText(movie.getCount() + "");

        holder.container.setTag(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        if (movie.getDiscount() != null &&
                !movie.getDiscount().equalsIgnoreCase("null")) {

            double newPrice = movie.getGrand_Total();
            double Price = newPrice;
            double discPrice = .65 * Price;
            holder.grand_total.setText("\u20B9 " + discPrice);

        } else {
            holder.grand_total.setText("\u20B9 " + movie.getGrand_Total());
        }

    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

}

