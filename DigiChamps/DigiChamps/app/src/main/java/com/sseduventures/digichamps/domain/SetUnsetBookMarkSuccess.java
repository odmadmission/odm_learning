package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class SetUnsetBookMarkSuccess implements Parcelable{

     private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
    }

    public SetUnsetBookMarkSuccess() {
    }

    protected SetUnsetBookMarkSuccess(Parcel in) {
        this.message = in.readString();
    }

    public static final Creator<SetUnsetBookMarkSuccess> CREATOR = new Creator<SetUnsetBookMarkSuccess>() {
        @Override
        public SetUnsetBookMarkSuccess createFromParcel(Parcel source) {
            return new SetUnsetBookMarkSuccess(source);
        }

        @Override
        public SetUnsetBookMarkSuccess[] newArray(int size) {
            return new SetUnsetBookMarkSuccess[size];
        }
    };
}
