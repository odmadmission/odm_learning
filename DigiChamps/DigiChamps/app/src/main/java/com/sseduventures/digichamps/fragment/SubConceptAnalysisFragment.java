package com.sseduventures.digichamps.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.TSSubConceptDataModel;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.TestStatisticsActivity;
import com.sseduventures.digichamps.adapter.AdapterForTSSubAnalysis;
import com.sseduventures.digichamps.domain.TestHighlightsResponse;
import com.sseduventures.digichamps.domain.TestHighlightsSubconcept;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class SubConceptAnalysisFragment extends android.support.v4.app.Fragment implements ServiceReceiver.Receiver  {


    RecyclerView legend_recycler_s_a;
    TextView test_type_txt,test_name_txt,chap_name_txt,tsscPerformance;
    String user_id;
    //ArrayList
    private ArrayList<TSSubConceptDataModel> subTSSCList;
    private ArrayList<TestHighlightsSubconcept> mSubConceptList;
    AdapterForTSSubAnalysis tSSCAdapater;

    String resId;
    RelativeLayout btnPM,rltvLay;
    SharedPreferences preferencePaper;
    private FormActivity mContext;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TestHighlightsResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sub_concept_analysis, container, false);
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_sc_analysis_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_sc_analysis_frag,
                    LogEventUtil.EVENT_sc_analysis_frag);
        }
        init(view);

        SharedPreferences preference = getActivity().getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
        //"No name defined" is the default value.
        user_id = preference.getString("User_ID", null);

        resId= TestStatisticsActivity.getResultId();

        if(AppUtil.isInternetConnected(getActivity())){
            //getResult(resId);
            getResultNew();
        }else {
            startActivity(new Intent(getContext(), Internet_Activity.class));
            Activity activity = (Activity) getContext();

            activity.finish();
        }

        preferencePaper = getActivity().getSharedPreferences("paper_zone",
                MODE_PRIVATE);
        String mentorId = preferencePaper.getString("Mentorid",null);


        if(mentorId.equals(null) || mentorId.equalsIgnoreCase("null")){
            rltvLay.setVisibility(View.VISIBLE);
        }else {
            rltvLay.setVisibility(View.GONE);
        }

        btnPM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getActivity(),PersonalMentorActivity.class);
                getActivity().startActivity(in);



            }
        });


        return view;

    }
    private void init(View view) {

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mContext = (TestStatisticsActivity) getActivity();

        legend_recycler_s_a = view.findViewById(R.id.legend_recycler_s_a);
        test_type_txt = view.findViewById(R.id.test_type_txt);
        test_name_txt = view.findViewById(R.id.test_name_txt);
        chap_name_txt = view.findViewById(R.id.chap_name_txt);

        btnPM = view.findViewById(R.id.rltv_pm);
        rltvLay = view.findViewById(R.id.relMaster1123);


        subTSSCList = new ArrayList<TSSubConceptDataModel>();



        // Recyclerview Layout
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), OrientationHelper.VERTICAL, false);
        legend_recycler_s_a.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        legend_recycler_s_a.setLayoutManager(mLayoutManager);
        legend_recycler_s_a.setNestedScrollingEnabled(false);


        mSubConceptList = new ArrayList<TestHighlightsSubconcept>();
        tSSCAdapater = new AdapterForTSSubAnalysis(mSubConceptList,getContext());
        legend_recycler_s_a.setAdapter(tSSCAdapater);

    }




    private void getResultNew(){

        int resultId = 0;
        if(resId!=null)
        {
            resultId=Integer.parseInt(resId);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID",resultId);
        if(AppUtil.isInternetConnected(getActivity())){

            //mContext.showDialog(null,"Please wait...");
            NetworkService.startActionGetTestHighlights(getActivity(),mServiceReceiver,bundle);
        }else{
            Intent in = new Intent(getActivity(),Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mContext.hideDialog();
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);

        switch (resultCode)
        {
            case ResponseCodes.SUCCESS:
                success=resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null&&success.getSuccess()!=null
                        && success.getSuccess().getDifficulty()!=null
                        && success.getSuccess().getDifficulty().size()>0){

                    chap_name_txt.setText(success.getSuccess().getChapterName());
                    test_name_txt.setText(success.getSuccess().getTestName());

                    if(success.getSuccess().getTestType().contains("Online")){
                        test_type_txt.setText("CBT");
                    }else{
                        test_type_txt.setText(success.getSuccess().getTestType());
                    }

                    mSubConceptList.clear();
                    mSubConceptList.addAll(success.getSuccess().getSubConcept());
                    tSSCAdapater.notifyDataSetChanged();

                }
                else
                {

                }

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }
}
