package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.DataModel_ReviewAnswers;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewNotificationActivity;
import com.sseduventures.digichamps.adapter.DividerItemDecoration;
import com.sseduventures.digichamps.adapter.RecyclerTouchListener;
import com.sseduventures.digichamps.adapter.ReviewAnswer_Adapter;
import com.sseduventures.digichamps.domain.ReviewSolutionQuestionList;
import com.sseduventures.digichamps.domain.ReviewSolutionResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;
import java.util.List;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ReviewAnswers extends FormActivity implements ServiceReceiver.Receiver {


    private List<DataModel_ReviewAnswers> list = new ArrayList<>();
    private List<ReviewSolutionQuestionList> mlist = new ArrayList<>();
    private RecyclerView recyclerView;
    private ReviewAnswer_Adapter mAdapter;
    ImageButton notification;
    ImageView back_arrow;
    LinearLayout next_but,prev_but;
    ImageView imvBack,cart_back_image_white;
    private String resultId;
    private SpotsDialog dialog;
    private Toolbar toolbar;
    private String examid;
    private String  chapIdid;
    private String  module_typetype ;
    private String subJectId;
    private  String  subNameName;
    private String   flagNameName;
    String user_id;
    LinearLayoutManager mLayoutManager;
    int count = 0;

    int i = 0;

    private String navigation;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ReviewSolutionResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        setContentView(R.layout.review_ans_new);
        setModuleName(LogEventUtil.EVENT_Review_ans);
        logEvent(LogEventUtil.KEY_Review_ans,LogEventUtil.EVENT_Review_ans);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        init();
        Intent intent = getIntent();
        examid = intent.getStringExtra("exam_id");
        resultId = intent.getStringExtra("Result_ID");
        navigation = intent.getStringExtra("navigation");

        chapIdid = intent.getStringExtra("chapter_id");
        module_typetype = intent.getStringExtra("module_type");
        subJectId = intent.getStringExtra("subject_id");
        subNameName = intent.getStringExtra("subName");
        flagNameName = intent.getStringExtra("flagName");
        if (AppUtil.isInternetConnected(this)) {


            getReviewAnswerDataNew();

        } else {
            startActivity(new Intent(ReviewAnswers.this, Internet_Activity.class));

            finish();
        }

        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReviewAnswers.this, NewNotificationActivity.class));

                finish();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ReviewSolutionQuestionList movie = mlist.get(position);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        prev_but.setVisibility(View.INVISIBLE);


        next_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (i < count - 1) {

                    ++i;
                    if (i == 0) {
                        prev_but.setVisibility(View.INVISIBLE);

                    } else {
                        prev_but.setVisibility(View.VISIBLE);

                    }

                    if (i == count - 1) {
                        next_but.setVisibility(View.INVISIBLE);

                    }

                    recyclerView.getLayoutManager().scrollToPosition(i);

                }else{
                }

            }
        });





        prev_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (i >= 1) {
                    --i;
                    if (i == 0) {
                        prev_but.setVisibility(View.INVISIBLE);

                    } else {
                        prev_but.setVisibility(View.VISIBLE);

                    }

                    if (count> 1) {
                        next_but.setVisibility(View.VISIBLE);

                    } else{
                        next_but.setVisibility(View.INVISIBLE);

                    }

                    recyclerView.getLayoutManager().scrollToPosition(i);

                }

            }
        });



    }

    private void init(){

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        prev_but = (LinearLayout)findViewById(R.id.prev_but);
        next_but = (LinearLayout)findViewById(R.id.next_but);

        cart_back_image_white = (ImageView) findViewById(R.id.cart_back_image_white);

        notification = (ImageButton) findViewById(R.id.notif);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        toolbar.setTitle("Review Your Answer");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        toolbar.setNavigationIcon(R.drawable.ic_back123);
        imvBack = (ImageView) toolbar.findViewById(R.id.cart_back_image);


        recyclerView = (RecyclerView) findViewById(R.id.review_recylr);
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper .attachToRecyclerView(recyclerView);
        mlist = new ArrayList<ReviewSolutionQuestionList>();
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new ReviewAnswer_Adapter(mlist, ReviewAnswers.this);
        recyclerView.setAdapter(mAdapter);
        cart_back_image_white.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReviewAnswers.this,
                        TestHighlight_Activity.class).putExtra("Result_ID",resultId).
                        putExtra("activity", "LeaderBoard")
                        .putExtra("Result_ID", resultId)
                        .putExtra("chapter_id",chapIdid)
                        .putExtra("module_type",module_typetype)
                        .putExtra("subName",subNameName)
                        .putExtra("navigation",navigation)
                        .putExtra("subject_id",subJectId)
                        .putExtra("flagName",flagNameName));

                finish();


            }
        });



    }

    @Override
    public void onBackPressed() {

        finish();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }







    private void getReviewAnswerDataNew(){

        int mresultId = 0;
        if(resultId!=null)
        {
            mresultId=Integer.parseInt(resultId);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID",mresultId);
        if(AppUtil.isInternetConnected(this)){

            NetworkService.startActionGetReviewSolutionData(
                    this,mServiceReceiver,bundle);
        }else{
            Intent in = new Intent(ReviewAnswers.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);

        switch (resultCode)
        {
            case ResponseCodes.SUCCESS:
                success=resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null&&success.getSuccessReviewQuestion()!=null
                        &&success.getSuccessReviewQuestion().getQuestionReviewList()!= null
                        &&success.getSuccessReviewQuestion().getQuestionReviewList().size()>0)
                {



                    count=success.getSuccessReviewQuestion().getQuestionReviewList().size();
                    mlist.clear();
                    mlist.addAll(success.getSuccessReviewQuestion().getQuestionReviewList());
                    mAdapter.notifyDataSetChanged();

                }
                else
                {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ReviewAnswers.this,ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ReviewAnswers.this,ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ReviewAnswers.this,Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

}
