package com.sseduventures.digichamps.services;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.Model.MentorStudentChat;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.domain.AddDiscussionRequest;
import com.sseduventures.digichamps.domain.AddDiscussionResponse;
import com.sseduventures.digichamps.domain.AddToCartRequest;
import com.sseduventures.digichamps.domain.AnalyticsResponse;
import com.sseduventures.digichamps.domain.AnalyticsSubjectResponse;
import com.sseduventures.digichamps.domain.AppStatusResponse;
import com.sseduventures.digichamps.domain.AssignedTeacherRequest;
import com.sseduventures.digichamps.domain.AssignedTeacherSuccess;
import com.sseduventures.digichamps.domain.BoardRequest;
import com.sseduventures.digichamps.domain.BoardResponse;
import com.sseduventures.digichamps.domain.BookMarkRequest;
import com.sseduventures.digichamps.domain.BookMarkVideoResponse;
import com.sseduventures.digichamps.domain.BookmarkQuestionResponse;
import com.sseduventures.digichamps.domain.BookmarkStudyNotesResponse;
import com.sseduventures.digichamps.domain.CartChapterId;
import com.sseduventures.digichamps.domain.CartData;
import com.sseduventures.digichamps.domain.ChangePasswordRequest;
import com.sseduventures.digichamps.domain.ChangePasswordSuccess;
import com.sseduventures.digichamps.domain.ChapterDetailsRequestModel;
import com.sseduventures.digichamps.domain.ChapterDetailsSuccess;
import com.sseduventures.digichamps.domain.CoinEarn;
import com.sseduventures.digichamps.domain.CoinLeaderboardResponse;
import com.sseduventures.digichamps.domain.DailyTTRequest;
import com.sseduventures.digichamps.domain.DailyTTResponse;
import com.sseduventures.digichamps.domain.DashboardRequest;
import com.sseduventures.digichamps.domain.DashboardResponse;
import com.sseduventures.digichamps.domain.DictonaryPostRequest;
import com.sseduventures.digichamps.domain.DictonaryResponse;
import com.sseduventures.digichamps.domain.DiscountCouponSuccess;
import com.sseduventures.digichamps.domain.DiscussionRequest;
import com.sseduventures.digichamps.domain.DiscussionResponse;
import com.sseduventures.digichamps.domain.DoubtListResponse;
import com.sseduventures.digichamps.domain.DoubtReplyResponse;
import com.sseduventures.digichamps.domain.EditProfileResponse;
import com.sseduventures.digichamps.domain.EditProfileRequestNew;
import com.sseduventures.digichamps.domain.EnCashResponse;
import com.sseduventures.digichamps.domain.ExamScheduleDetailsRequest;
import com.sseduventures.digichamps.domain.ExamScheduleDetailsResponse;
import com.sseduventures.digichamps.domain.ExamScheduleRequest;
import com.sseduventures.digichamps.domain.ExamScheduleResponse;
import com.sseduventures.digichamps.domain.FAQResponse;
import com.sseduventures.digichamps.domain.FeedResponse;
import com.sseduventures.digichamps.domain.GetCartDataResponse;
import com.sseduventures.digichamps.domain.GetClassRequest;
import com.sseduventures.digichamps.domain.GetClassResponse;
import com.sseduventures.digichamps.domain.GetDiscusiionDetailsResponse;
import com.sseduventures.digichamps.domain.GetDiscussionDetailsRequest;
import com.sseduventures.digichamps.domain.GetPackageResponse;
import com.sseduventures.digichamps.domain.LearnDetailsResponse;
import com.sseduventures.digichamps.domain.OrderConfResponse;
import com.sseduventures.digichamps.domain.SetBookmarkRequest;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.webservice.model.GetPrtExamResponse;
import com.sseduventures.digichamps.domain.HomeworkRequest;
import com.sseduventures.digichamps.domain.HomeworkResponse;
import com.sseduventures.digichamps.domain.InformationRequest;
import com.sseduventures.digichamps.domain.LeaderBoardResponse;
import com.sseduventures.digichamps.domain.LearnChapResponse;
import com.sseduventures.digichamps.domain.LoginDto;
import com.sseduventures.digichamps.domain.LogoutResponse;
import com.sseduventures.digichamps.domain.MyOrdersResponse;
import com.sseduventures.digichamps.domain.NcertSolutionResponse;
import com.sseduventures.digichamps.domain.NewLearnRequest;
import com.sseduventures.digichamps.domain.NewLearnResponse;
import com.sseduventures.digichamps.domain.NoticesRequest;
import com.sseduventures.digichamps.domain.NoticesResponse;
import com.sseduventures.digichamps.domain.OlympiadDataResponse;
import com.sseduventures.digichamps.domain.PMResponse;
import com.sseduventures.digichamps.domain.PackageModel;
import com.sseduventures.digichamps.domain.PostDiscussionDetailsResponse;
import com.sseduventures.digichamps.domain.PostDisscussionDetailsRequest;
import com.sseduventures.digichamps.domain.ProfileDataResponse;
import com.sseduventures.digichamps.domain.PrvYrPaperResponse;
import com.sseduventures.digichamps.domain.PsychometricPostData;
import com.sseduventures.digichamps.domain.PsychometricPostRequest;
import com.sseduventures.digichamps.domain.PsychometricQuestionList;
import com.sseduventures.digichamps.domain.Registration;
import com.sseduventures.digichamps.domain.RegistrationDtl;
import com.sseduventures.digichamps.domain.RemoveCartRequest;
import com.sseduventures.digichamps.domain.ReviewSolutionResponse;
import com.sseduventures.digichamps.domain.ScLeaderboardRequest;
import com.sseduventures.digichamps.domain.ScLeaderboardResponse;
import com.sseduventures.digichamps.domain.SetUnsetBookMarkSuccess;
import com.sseduventures.digichamps.domain.SmsResponseNew;
import com.sseduventures.digichamps.domain.StudyMaterialsRequest;
import com.sseduventures.digichamps.domain.StudyMaterialsResponse;
import com.sseduventures.digichamps.domain.SubConceptResponse;
import com.sseduventures.digichamps.domain.SubjectResponse;
import com.sseduventures.digichamps.domain.TestHighlightsResponse;
import com.sseduventures.digichamps.domain.TopperResponse;
import com.sseduventures.digichamps.domain.ToppersRequest;
import com.sseduventures.digichamps.domain.UploadImageResponse;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.scholarship.ScholarShipScratch;
import com.sseduventures.digichamps.scholarship.ScratchSuccess;
import com.sseduventures.digichamps.utils.BitmapUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FileUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.StringUtils;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.RestService;
import com.sseduventures.digichamps.webservice.RestServiceBuilder;
import com.sseduventures.digichamps.webservice.model.CheckoutDataModel;
import com.sseduventures.digichamps.webservice.model.DoubtDetailsModel;
import com.sseduventures.digichamps.webservice.model.DoubtSubmitModel;
import com.sseduventures.digichamps.webservice.model.FeedBackFormModel;
import com.sseduventures.digichamps.webservice.model.FeedBackFormRequestModel;
import com.sseduventures.digichamps.webservice.model.GetCbtResponseModel;
import com.sseduventures.digichamps.webservice.model.GetExamResponse;
import com.sseduventures.digichamps.webservice.model.GetPostExamData;
import com.sseduventures.digichamps.webservice.model.OrderDetailsModel;
import com.sseduventures.digichamps.webservice.model.PostQuestionRequestModel;
import com.sseduventures.digichamps.webservice.model.ProfileModelClass;
import com.sseduventures.digichamps.webservice.model.RecentlyWatchedModel;
import com.sseduventures.digichamps.webservice.model.RecentlyWatchedRequestBody;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


public class NetworkService extends IntentService {


    private static final String TAG = NetworkService.class.getSimpleName();
    private ResultReceiver mResultReceiver;
    private int mResultCode = ResponseCodes.UNEXPECTED_ERROR;

    public NetworkService() {
        super(TAG);

    }


    public static void startActionAssignSchoolZone(Context context,
                                                   ResultReceiver receiver,
                                                   Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ASSIGN_SCHOOL_ZONE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionRecentlyWatched(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_RECENTLY_WATCHED);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionDiscountCoupon(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_DISCOUNT_COUPON);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionLearnChapDetailsNew(Context context,
                                                 ResultReceiver receiver,
                                                 Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_LEARN_CHAP_DETAILS_NEW);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionSaveVVideoCoins(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SAVE_VIDEO_COINS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetScratch(Context context,
                                             ResultReceiver receiver,
                                             Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SCRATCH);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    //new
    public static void startActionLogout(Context context,
                                         ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_LOGOUT);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetCoinsLeaderboard(Context context,
                                                      ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_COIN_LEADERBOARD);

        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionLogin(Context context,
                                        ResultReceiver receiver,
                                        Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_NEW_LOGIN);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionSignupDetails(Context context,
                                                ResultReceiver receiver,
                                                Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_NEW_SIGNUP);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetSetUsetBookmark(Context context,
                                                     ResultReceiver receiver,
                                                     Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SET_UNSET_BOOKMARK);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostLearn(Context context,
                                            ResultReceiver receiver,
                                            Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_NEW_LEARN);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionPostNewDashboard(Context context,
                                                   ResultReceiver receiver
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_NEW_DASHBOARD);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetOrders(Context context,
                                            ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_ORDERS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetPackages(Context context,
                                              ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_PACKAGES);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetPsychoExamData(Context context,
                                                    ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_PSYCHO_GET_EXAM);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostPsychoExamData(Context context,
                                                     Bundle bundle,
                                                     ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_PSYCHO_POST_EXAM);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostScLeaderboardData(Context context,
                                                        ResultReceiver receiver
            , Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SCLEADERBOARD_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionPostDictonaryData(Context context,
                                                    Bundle bundle,
                                                    ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_DICTONARY_POST);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionAddToCart(Context context,
                                            ResultReceiver receiver,
                                            Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ADD_TO_CART);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetCartData(Context context,
                                              ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_CART_DATA);

        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startcheckoutData(Context context,
                                         ResultReceiver receiver,
                                         Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_CHECKOUT_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startPostEPImage(Context context,
                                        ResultReceiver receiver,
                                        Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_IMAGE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startPostEditProfile(Context context,
                                            ResultReceiver receiver,
                                            Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_EDIT_PROFILE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionRemoveCartData(Context context,
                                                 ResultReceiver receiver,
                                                 Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_REMOVE_CART_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionExamSchedule(Context context,
                                               ResultReceiver receiver,
                                               Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_EXAM_SCHEDULE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionOrderConfirmation(Context context,
                                                    ResultReceiver receiver,
                                                    Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ORDER_CONFIRMATION);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionUpdateScratch(Context context,
                                                ResultReceiver receiver,
                                                Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_UPDATE_SCRATCH);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetTestHighlights(Context context,
                                                    ResultReceiver receiver,
                                                    Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_TESTHIGLIGHTS_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetLeaderBoardData(Context context,
                                                     ResultReceiver receiver,
                                                     Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_LEADERBOARD_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetLogoutData(Context context,
                                                ResultReceiver receiver
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_LOGOUT_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetSubConceptData(Context context,
                                                    ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SUBCONCEPT_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetAnalyticsSubjectData(Context context,
                                                          ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_ANALYTICS_SUBJECT_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetAnalyticsDetailsData(Context context,
                                                          ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_ANALYTICS_DETAILS_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetReviewSolutionData(Context context,
                                                        ResultReceiver receiver,
                                                        Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_REVIEW_SOLUTION_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionSendMessage(Context context,
                                              ResultReceiver receiver,
                                              Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SEND_MESSAGE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetMessages(Context context,
                                              ResultReceiver receiver,
                                              Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_LIST);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionSendImage(Context context,
                                            ResultReceiver receiver,
                                            Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SEND_IMAGE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionLearnChapDetails(Context context,
                                                   ResultReceiver receiver,
                                                   Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_LEARN_CHAP_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionSetUnsetBookmarks(Context context,
                                                    ResultReceiver receiver,
                                                    Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_SET_UNSET_BOOKMARKS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetProfileData(Context context,
                                                 ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_PROFILE_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetFaqData(Context context,
                                             ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_FAQ_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetFeedData(Context context,
                                              ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_FEED_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetOlympiads(Context context,
                                               ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_OLYMPIAD_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetSignUpData(Context context,
                                                ResultReceiver receiver,
                                                Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SIGNUP);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetResetPassword(Context context,
                                                   ResultReceiver receiver,
                                                   Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_RESET_PASSWORD);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetSignUpMobile(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SIGNUP_MOBILE);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetPostEncash(Context context,
                                                ResultReceiver receiver,
                                                Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_ENCASH);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetSubject(Context context,
                                             ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SUBJECT_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetDoubtList(Context context,
                                               ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SUBJECT_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetChangePassword(Context context,
                                                    ResultReceiver receiver,
                                                    Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_CHANGE_PASSWORD);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    //Added By Abhishek
    public static void startOrderDetailsData(Context context,
                                             ResultReceiver receiver,
                                             Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_ORDERDETAILS_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);

        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    //Added By Abhishek

    public static void startfeedbackformData(Context context,
                                             ResultReceiver receiver,
                                             Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_FEEDEBACK_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetPrvsYrPaper(Context context,
                                                 ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_PRVS_YR_PAPER_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetNcertSolution(Context context,
                                                   ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_NCERT_SOLUTION_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetDoubtListData(Context context,
                                                   ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_DOUBT_LIST_DATA);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetNewDoubtList(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_NEW_DOUBT_LIST);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetLiveAppStatus(Context context,
                                                   ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_APP_STATUS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    //create static method
    public static void startActionGetExamData(Context context,
                                              ResultReceiver receiver,
                                              Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_EXAM);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startDoubtDetails(Context context,
                                         ResultReceiver receiver,
                                         Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_DOUBTDETAILS_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    /****************************/


    //Added By Abhishek
    public static void startActionGetPostExamData(Context context,
                                                  ResultReceiver receiver,
                                                  Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_EXAM_DATA);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    /*************************/


    /****** SchoolZone******/
    public static void startActionPostExamSchedule(Context context,
                                                   ResultReceiver receiver
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_EXAM_SCHEDULE);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostExamScheduleDetails(Context context,
                                                          ResultReceiver receiver,
                                                          Bundle bundle
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_EXAM_SCHEDULE_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostDailyTTDetails(Context context,
                                                     ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_DAILYTT_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostAssignedTeacherDetails(Context context,
                                                             ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ASSIGN_TEACHER_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostStudyMaterialsList(Context context,
                                                         ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_STUDY_MATERIALS_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostToppersDetails(Context context,
                                                     ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_TOPPERS_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostBoardDetails(Context context,
                                                   ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_BOARD_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostClassDetails(Context context,
                                                   ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_CLASS_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostNoticesList(Context context,
                                                  ResultReceiver receiver

    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_NOTICES_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostDiscussionList(Context context,
                                                     ResultReceiver receiver,
                                                     Bundle bundle
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_DISCUSSION_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostForumDetails(Context context,
                                                   ResultReceiver receiver,
                                                   Bundle bundle
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_DETAILS_FORUM_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostAddDiscussionList(Context context,
                                                        ResultReceiver receiver,
                                                        Bundle bundle
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ADD_DISCUSSION_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostAddForumList(Context context,
                                                   ResultReceiver receiver,
                                                   Bundle bundle
    ) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_ADD_FORUM_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionPostHomeworkDetails(Context context,
                                                      ResultReceiver receiver,
                                                      Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_HOMEWORK);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    /*************************/
    /*************************/

    public static void startActionPostChangePassword(Context context,
                                                     ResultReceiver receiver,
                                                     Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_POST_CHANGE_PASSWORD);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetInfo(Context context,
                                          ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_INFO);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetPmDetails(Context context,
                                               ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_PM_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetBookmarkVideo(Context context,
                                                   ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_BOOKMARK_VIDEO);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetBookmarkStudyNotes(Context context,
                                                        ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_BOOKMARK_STUDY_NOTES);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetBookmarkQuestion(Context context,
                                                      ResultReceiver receiver) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_BOOKMARK_QUESTION);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }


    public static void startActionGetBoardClass(Context context,
                                                ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_BOARD_CLASS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionGetOrderConfirmation(Context context,
                                                       ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_ORDER_CONF);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    public static void startActionSubmitDoubt(Context context,
                                              ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_SUBMIT_DOUBT);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        context.startService(intent);
    }

    public static void startActionReplyDoubt(Context context,
                                             ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_REPLY_DOUBT);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        context.startService(intent);
    }

    public static void startGetCbtExam(Context context,
                                       ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_CBT_EXAM);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        context.startService(intent);
    }

    public static void startGetPrtExam(Context context,
                                       ResultReceiver receiver, Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_GET_PRT_EXAM);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        context.startService(intent);
    }

    public static void startGetChapterDetails(Context context,
                                              ResultReceiver receiver,
                                              Bundle bundle) {
        Intent intent = new Intent(context, NetworkService.class);
        intent.setAction(IntentHelper.ACTION_CHAPTER_DETAILS);
        intent.putExtra(IntentHelper.EXTRA_DATA1, bundle);
        intent.putExtra(IntentHelper.EXTRA_PARCELABLE, receiver);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            mResultReceiver = intent.
                    getParcelableExtra(IntentHelper.EXTRA_PARCELABLE);
            final String action = intent.getAction();

            if (action != null && IntentHelper.ACTION_SEND_MESSAGE.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);

                handleActionSendMessage(bundle);
            } else if (action != null && IntentHelper.ACTION_SEND_IMAGE.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);

                handleSendImage(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_LIST.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);

                handleActionGetMessages(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_SCRATCH.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getScholarshipScratch(bundle);
            } else if (action != null && IntentHelper.ACTION_UPDATE_SCRATCH.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                updateScholarshipScratch(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_TESTHIGLIGHTS_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getTestHighlightsData(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_LEADERBOARD_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getLeaderboardData(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_LOGOUT_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getLogoutData();
            } else if (action != null && IntentHelper.ACTION_GET_SUBCONCEPT_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getSubConceptData();
            } else if (action != null && IntentHelper.ACTION_GET_ANALYTICS_SUBJECT_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getAnalyticsSubjectData();
            } else if (action != null && IntentHelper.ACTION_GET_ANALYTICS_DETAILS_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getAnalyticsDetailsData();
            } else if (action != null && IntentHelper.ACTION_GET_REVIEW_SOLUTION_DATA.equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                getReviewSolutionData(bundle);
            } else if (action != null && IntentHelper.ACTION_ORDER_CONFIRMATION
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleOrderConfirmation(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_PACKAGES
                    .equals(action)) {

                handleGetPackages();
            } else if (action != null && IntentHelper.ACTION_ADD_TO_CART
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleAddToCart(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_CART_DATA
                    .equals(action)) {
                handleGetCartData();
            } else if (action != null && IntentHelper.ACTION_REMOVE_CART_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleRemoveCartData(bundle);
            } else if (action != null && IntentHelper.ACTION_PSYCHO_GET_EXAM
                    .equals(action)) {

                handleGetPsychoExamData();
            } else if (action != null && IntentHelper.ACTION_PSYCHO_POST_EXAM
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostPsychoExamData(bundle);
            } else if (action != null && IntentHelper.ACTION_LEARN_CHAP_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetLearnChapDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_SET_UNSET_BOOKMARKS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostSetUnsetBookMark(bundle);
            } else if (action != null && IntentHelper.ACTION_DICTONARY_POST
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostDictonary(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_ORDERS
                    .equals(action)) {

                handleGetOrders();
            } else if (action != null && IntentHelper.ACTION_GET_PROFILE_DATA
                    .equals(action)) {

                handleGetProfileData();
            } else if (action != null && IntentHelper.ACTION_GET_FAQ_DATA
                    .equals(action)) {

                handleGetFaqData();
            } else if (action != null && IntentHelper.ACTION_GET_FEED_DATA
                    .equals(action)) {

                handleGetFeedData();
            } else if (action != null && IntentHelper.ACTION_GET_OLYMPIAD_DATA
                    .equals(action)) {

                handleGetOlympiadData();
            } else if (action != null && IntentHelper.ACTION_GET_SIGNUP
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetSignUp(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_ENCASH
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetEncash(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_RESET_PASSWORD
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetResetPassword(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_SIGNUP_MOBILE
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetSignupMobile(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_SUBJECT_DETAILS
                    .equals(action)) {

                handleGetSubject();
            } else if (action != null && IntentHelper.ACTION_GET_CHANGE_PASSWORD
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetChangePassword(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_PRVS_YR_PAPER_DATA
                    .equals(action)) {

                handleGetPrvsYrPaperData();
            } else if (action != null && IntentHelper.ACTION_POST_CHANGE_PASSWORD
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostChangePassword(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_HOMEWORK
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostHomework(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_NCERT_SOLUTION_DATA
                    .equals(action)) {

                handleGetNcertSolution();
            } else if (action != null && IntentHelper.ACTION_GET_DOUBT_LIST_DATA
                    .equals(action)) {

                handleGetDoubtListData();


            } else if (action != null && IntentHelper.ACTION_GET_NEW_DOUBT_LIST
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleDoubtListNewData(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_APP_STATUS
                    .equals(action)) {
                handleGetAppStatus();
            } else if (action != null && IntentHelper.ACTION_POST_FEEDEBACK_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetFeedbackForm(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_ORDERDETAILS_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleOrderDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_EXAM
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetExam(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_EXAM_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetPostExam(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_DOUBTDETAILS_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleDoubtDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_CHECKOUT_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleCheckoutDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_IMAGE
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostImageDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_POST_EDIT_PROFILE
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostEditProfile(bundle);
            } else if (action != null && IntentHelper.ACTION_EXAM_SCHEDULE
                    .equals(action)) {

                handlePostExamSchedule();
            } else if (action != null && IntentHelper.ACTION_EXAM_SCHEDULE_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostExamScheduleDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_DAILYTT_DETAILS
                    .equals(action)) {
                handlePostDailyTTDetails();
            } else if (action != null && IntentHelper.ACTION_GET_INFO
                    .equals(action)) {
                handleGetInfoDetails();
            } else if (action != null && IntentHelper.ACTION_GET_PM_DETAILS
                    .equals(action)) {
                handleGetPMDetails();
            } else if (action != null && IntentHelper.ACTION_GET_BOARD_CLASS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetBoardClassDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_ORDER_CONF
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetOrderConf(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_BOOKMARK_VIDEO
                    .equals(action)) {
                handleGetBookmarkVideo();
            } else if (action != null && IntentHelper.ACTION_GET_BOOKMARK_STUDY_NOTES
                    .equals(action)) {
                handleGetBookmarkStudyNotes();
            } else if (action != null && IntentHelper.ACTION_GET_BOOKMARK_QUESTION
                    .equals(action)) {
                handleGetBookmarkQuestion();
            } else if (action != null && IntentHelper.ACTION_ASSIGN_TEACHER_DETAILS
                    .equals(action)) {
                handlePostAssignedTeacherDetails();
            } else if (action != null && IntentHelper.ACTION_STUDY_MATERIALS_DETAILS
                    .equals(action)) {
                handlePostStudyMaterialsDetails();
            } else if (action != null && IntentHelper.ACTION_NOTICES_DETAILS
                    .equals(action)) {
                handlePostNoticesDetails();
            } else if (action != null && IntentHelper.ACTION_DISCUSSION_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostDiscussionDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_ADD_DISCUSSION_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostAddDiscussionDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_ADD_FORUM_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostAddForumDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_DETAILS_FORUM_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostDetailsForumDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_TOPPERS_DETAILS
                    .equals(action)) {
                handlePostToppersDetails();
            } else if (action != null && IntentHelper.ACTION_BOARD_DETAILS
                    .equals(action)) {
                handlePostBoardDetails();
            } else if (action != null && IntentHelper.ACTION_CLASS_DETAILS
                    .equals(action)) {
                handlePostClassDetails();
            } else if (action != null && IntentHelper.ACTION_SCLEADERBOARD_DATA
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostScLeaderboardDetails(bundle);
            } else if (action != null && IntentHelper.ACTION_NEW_LOGIN
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleLogin(bundle);
            } else if (action != null && IntentHelper.ACTION_NEW_SIGNUP
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleSignUp(bundle);
            } else if (action != null && IntentHelper.ACTION_SET_UNSET_BOOKMARK
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleSetUnsetBookmark(bundle);
            } else if (action != null && IntentHelper.ACTION_NEW_LEARN
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePostLearn(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_NEW_DASHBOARD
                    .equals(action)) {

                handlePostNewDashBoard();
            } else if (action != null && IntentHelper.ACTION_LOGOUT
                    .equals(action)) {

                handleLogout();
            } else if (action != null && IntentHelper.ACTION_GET_COIN_LEADERBOARD
                    .equals(action)) {

                handleGetCoinLeaderboard();
            } else if (action != null && IntentHelper.ACTION_GET_SUBMIT_DOUBT
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleGetSubmitDoubt(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_REPLY_DOUBT
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleReplyDoubt(bundle);
            } else if (action != null && IntentHelper.ACTION_ASSIGN_SCHOOL_ZONE
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleAssignSchoolZone(bundle);
            } else if (action != null && IntentHelper.ACTION_RECENTLY_WATCHED
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlerecentlywatched(bundle);
            } else if (action != null && IntentHelper.ACTION_SAVE_VIDEO_COINS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleSaveVideoCoins(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_CBT_EXAM
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleCbtExam(bundle);
            } else if (action != null && IntentHelper.ACTION_GET_PRT_EXAM
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlePrtExam(bundle);
            } else if (action != null && IntentHelper.ACTION_CHAPTER_DETAILS
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handlechapterdetails(bundle);
            }else if (action != null && IntentHelper.ACTION_DISCOUNT_COUPON
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleDiscountCoupon(bundle);
            }else if (action != null && IntentHelper.ACTION_LEARN_CHAP_DETAILS_NEW
                    .equals(action)) {
                final Bundle bundle = intent.getBundleExtra(IntentHelper
                        .EXTRA_DATA1);
                handleLearnChapDetailsNew(bundle);
            }

        }
    }


    private void handleActionSendMessage(Bundle bundle) {

        Bundle resultData = new Bundle();
        try {

            MentorStudentChat msc = bundle.getParcelable("msc");
            long rowId = 0;
            RestService restService = RestServiceBuilder.get().getRestService();
            Response<MentorStudentChat> chatResponse = restService.
                    createChat(msc).execute();
            //  //Log.v(TAG, chatResponse.body() + "");
            if (chatResponse != null) {
                mResultCode = chatResponse.code();
                if (mResultCode == 201) {
                    mResultCode = 201;
                    resultData.putParcelable("msc", chatResponse.body());
                } else if (mResultCode == 404) {

                }
            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);
    }

    private void handleActionGetMessages(Bundle bundle) {

        Bundle resultData = new Bundle();
        try {

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ArrayList<MentorStudentChat>> chatResponse
                    = restService.getList(
                    Long.parseLong(bundle.getString("regId")))
                    .execute();
            ////Log.v(TAG, chatResponse.body() + "");
            if (chatResponse != null) {
                mResultCode = chatResponse.code();
                if (mResultCode == 200) {
                    mResultCode = 200;
                    resultData.putParcelableArrayList("data", chatResponse.body());
                } else if (mResultCode == 404) {

                }
            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);
    }


    private void handleSendImage(Bundle bundle) {
        Bundle resultData = new Bundle();

        //writeToFile(base64);
        try {
            File compressedImageFile = null;
            Uri tempUri = bundle.getParcelable("tempuri");
            Uri imageUri = bundle.getParcelable("imageuri");
            String base64 = null;
            int type = bundle.getInt("type");
            File photoFile = (File) bundle.getSerializable("file");
            int width = bundle.getInt("width");
            int height = bundle.getInt("height");
            switch (type) {
                case 1:
                    compressedImageFile = BitmapUtil.compressImage(
                            this,
                            photoFile,
                            height,
                            width);
                    BitmapFactory.Options options = new BitmapFactory.
                            Options();
                    options.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(compressedImageFile.
                            getAbsolutePath(), options);

                    options.inSampleSize = 2;

                    options.inJustDecodeBounds = false;
                    Bitmap bitmap = BitmapFactory.decodeFile(
                            compressedImageFile.getAbsolutePath(),
                            options);

                    base64 = StringUtils.bitmapToBase64(bitmap);
                    bitmap = null;
                    break;
                case 2:
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().
                            query(tempUri, filePathColumn,
                                    null, null, null);

                    if (cursor == null || cursor.getCount() < 1) {

                    } else {

                        cursor.moveToFirst();
                        int columnIndex = cursor.
                                getColumnIndex(filePathColumn[0]);

                        if (columnIndex < 0) // no column index
                        {


                        } else {
                            String picturePath = cursor.getString(columnIndex);
                            cursor.close(); // close cursor
                            File photo = FileUtil.
                                    getOutputMediaFile(FileUtil.MEDIA_TYPE_IMAGE,
                                            FileUtil.IMAGE_SENT_DIR);

                            copy(new File(picturePath), photo);

                            imageUri = FileProvider.
                                    getUriForFile(this,
                                            getResources().
                                                    getString(R.string.freshchat_file_provider_authority),
                                            photo);
                            compressedImageFile = BitmapUtil.compressImage(
                                    this,
                                    photo,
                                    height,
                                    width);
                            options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(compressedImageFile.
                                    getAbsolutePath(), options);

                            options.inSampleSize = 2;

                            options.inJustDecodeBounds = false;
                            bitmap = BitmapFactory.decodeFile(
                                    compressedImageFile.getAbsolutePath(),
                                    options);

                            base64 = StringUtils.bitmapToBase64(bitmap);
                            bitmap = null;
                            break;
                        }
                    }

            }


            if (base64 != null) {

                //  //Log.v(TAG, "yes");
                MentorStudentChat msc = bundle.getParcelable("msc");
                msc.setMessage(base64);
                msc.setFileName(compressedImageFile.getName());
                long rowId = 0;
                RestService restService = RestServiceBuilder.get().
                        getRestService();
                Response<MentorStudentChat> chatResponse = restService.
                        createChat(msc).execute();
                // //Log.v(TAG, chatResponse.body() + "");
                if (chatResponse != null) {
                    mResultCode = chatResponse.code();
                    if (mResultCode == 201) {
                        mResultCode = 201;
                        resultData.putParcelable("msc", chatResponse.body());
                    } else if (mResultCode == 404) {

                    }
                } else
                    mResultCode = ResponseCodes.RESPONSE_NULL;


            }


        } catch (Exception e) {
            //  //Log.v(TAG, "yes");
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.
                    RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);

    }


    public void writeToFile(String data) {
        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/DigiMentor/"
                        );

        // Make sure the path directory exists.
        if (!path.exists()) {
            // Make it, if it doesn't exit
            path.mkdirs();
        }

        final File file = new File(path, "config.txt");

        // Save your stream, don't forget to flush() it before closing it.

        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);

            myOutWriter.close();

            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            //Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public void copy(File src, File dst) {
        try {
            FileInputStream inStream = new FileInputStream(src);
            FileOutputStream outStream = new FileOutputStream(dst);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0,
                    inChannel.size(),
                    outChannel);
            inStream.close();
            outStream.close();
        } catch (Exception e) {

        }
    }


    private void getReviewSolutionData(Bundle bundle) {
        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getReviewSolutionData(
                            bundle.getInt("Result_ID")
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            ReviewSolutionResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ReviewSolutionResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetChangePassword(Bundle bundle) {
        try {


            //final String token = SharedPrefManager.getInstance(this).getDeviceToken();
            RestService restService = RestServiceBuilder.get().
                    getRestService();

            //Log.v("TAGA", bundle.getString("deviceId"));
            Call<ResponseBody> scholarShipCall = restService.
                    getChangePassword(
                            bundle.getString("mobile"),
                            bundle.getString("password"),
                            bundle.getString("deviceId")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        Registration myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        Registration.class);

                        if (myclass1Object.getRegistrationDtl() == null
                                || myclass1Object.getRegistrationDtl().getBoardId() == null
                                || myclass1Object.getRegistrationDtl().getBoardId().
                                equals("null")
                                || myclass1Object.getRegistrationDtl().getClassId() == null
                                || myclass1Object.getRegistrationDtl().getClassId().equals("null")
                                || myclass1Object.getSectionId() == null
                                || myclass1Object.getSectionId().equals("null")
                                || myclass1Object.getSchoolId() == null
                                || myclass1Object.getSchoolId().equals("null")) {
                            mResultCode = 600;


                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else {

//                            RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());
//
//                            RegPrefManager.getInstance(this).setStatus(1);
//                            RegPrefManager.getInstance(this).setUserName(myclass1Object.getCustomerName());
//                            RegPrefManager.getInstance(this).setImageString(myclass1Object.getImage());
//                            RegPrefManager.getInstance(this).setRegId(myclass1Object.getRegdId());
//                            RegPrefManager.getInstance(this).setPhone(myclass1Object.getPhone());
//                            RegPrefManager.getInstance(this).setEmail(myclass1Object.getEmail());
//                            RegPrefManager.getInstance(this).setKeyBoardId(String.valueOf(myclass1Object.getRegistrationDtl().getBoardId()));
//                            RegPrefManager.getInstance(this).setKeyClassId(Long.valueOf(myclass1Object.getRegistrationDtl().getClassId()));
//                            RegPrefManager.getInstance(this).setSchoolName(myclass1Object.getOrganisationName());
//
//                            RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());

                            RegPrefManager.getInstance(this).setStatus(1);
                            RegPrefManager.getInstance(this).setUserName(myclass1Object.getCustomerName());
                            RegPrefManager.getInstance(this).setImageString(myclass1Object.getImage());
                            RegPrefManager.getInstance(this).setRegId(myclass1Object.getRegdId());
                            RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());
                            RegPrefManager.getInstance(this).setPhone(myclass1Object.getMobile());
                            RegPrefManager.getInstance(this).setEmail(myclass1Object.getEmail());
                            RegPrefManager.getInstance(this).setSchoolName(myclass1Object.getOrganisationName());
                            //RegPrefManager.getInstance(this).setDOB(myclass1Object.getDateOfBirth());
                            RegPrefManager.getInstance(this).setBoardName(myclass1Object.getBoard());
                            RegPrefManager.getInstance(this).setschoolId(myclass1Object.getSchoolId());
                            RegPrefManager.getInstance(this).setClassName(myclass1Object.getClassName());
                            RegPrefManager.getInstance(this).setSectionId(myclass1Object.getSectionId());
                            RegPrefManager.getInstance(this).setsectionName(myclass1Object.getSectionName());

                            RegPrefManager.getInstance(this).setKeyBoardId(String.valueOf(myclass1Object.
                                    getRegistrationDtl().getBoardId()));
                            long my = myclass1Object.getRegistrationDtl().getClassId();
                            RegPrefManager.getInstance(this).setKeyClassId(myclass1Object.
                                    getRegistrationDtl().getClassId());
                            RegPrefManager.getInstance(this).setLoginId(myclass1Object
                                    .getLoginStatus().
                                            getId());


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        }


                    } else {
                        mResultCode = ResponseCodes.FAILURE;
                    }
                } else if (mResultCode == 204) {

                    mResultCode = ResponseCodes.NO_USER_FOUND;

                } else {

                    mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetSignupMobile(Bundle bundle) {
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();

            Call<ResponseBody> scholarShipCall = restService.
                    getSignUpMobile(
                            bundle.getString("mobile"),
                            "Your OTP for DIGICHAMPS registration is : "
                                    + bundle.getString("otp")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        SmsResponseNew myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        SmsResponseNew.class);


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else {
                        mResultCode = ResponseCodes.FAILURE;
                    }
                } else if (mResultCode == 409) {

                    mResultCode = ResponseCodes.USER_ALREADY_DONE;
                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetResetPassword(Bundle bundle) {
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();

            Call<ResponseBody> scholarShipCall = restService.
                    getResetPassword(
                            bundle.getString("mobile"),
                            "Your OTP to reset DIGICHAMPS password is : " + bundle.getString("otp")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        SmsResponseNew myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        SmsResponseNew.class);


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else {
                        mResultCode = ResponseCodes.FAILURE;
                    }
                } else if (mResultCode == 204) {

                    mResultCode = ResponseCodes.NO_USER_FOUND;
                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetEncash(Bundle bundle) {
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getPostencash(
                            regId,
                            bundle.getInt("coins"),
                            bundle.getString("mobile"),
                            bundle.getString("address")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 201) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        EnCashResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        EnCashResponse.class);


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else {
                        mResultCode = ResponseCodes.FAILURE;
                    }
                } else if (mResultCode == 409) {

                    mResultCode = ResponseCodes.IFMOBILEEXITS;
                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetSignUp(Bundle bundle) {
        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getSignUpOtp(
                            bundle.getInt("mobile"),
                            bundle.getInt("otp")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        SmsResponseNew myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        SmsResponseNew.class);


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else {
                        mResultCode = ResponseCodes.FAILURE;
                    }
                } else if (mResultCode == 409) {

                    mResultCode = ResponseCodes.IFMOBILEEXITS;
                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetOrderConf(Bundle bundle) {
        try {


            long RegId = RegPrefManager.getInstance(this).getRegId();
            String orderId = bundle.getString("order_id");
            String trackingId = bundle.getString("tracking_id");
            int discount = bundle.getInt("discount");

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getOrderConf(
                            RegId, orderId, trackingId, discount
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        OrderConfResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        OrderConfResponse.class);

                        mResultCode = ResponseCodes.ORDERCONF;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);
                        // } else
                        //  mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetBoardClassDetails(Bundle bundle) {

        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getBoardClass();

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONArray jsonArray = new JSONArray();
                        jsonArray.put(jsonObject);
                        //if (jsonObject.optString("schoolInformation") != null) {


                        ArrayList<ProfileModelClass> myclass1Object = new Gson().
                                fromJson(jsonArray.toString(),
                                        new TypeToken<ArrayList<ProfileModelClass>>() {
                                        }.getType());


                        /*ProfileModelClass myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        ProfileModelClass.class);*/

                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelableArrayList(IntentHelper.RESULT_DATA,
                                myclass1Object);
                        // } else
                        //  mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetBookmarkQuestion() {
        Bundle bundle = new Bundle();
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getBookmarkQuestion(
                            regId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        BookmarkQuestionResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        BookmarkQuestionResponse.class);

                        if (myclass1Object.getSuccessBookmarkedQuestion() != null) {
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else {
                            mResultCode = ResponseCodes.NOLIST;
                        }


                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetBookmarkStudyNotes() {
        Bundle bundle = new Bundle();
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getBookmarkStudyNotes(
                            regId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        BookmarkStudyNotesResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        BookmarkStudyNotesResponse.class);

                        if (myclass1Object.getSuccessBookmarkedStudynote() != null) {
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else {
                            mResultCode = ResponseCodes.NOLIST;
                        }


                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);

    }

    private void handleGetBookmarkVideo() {
        Bundle bundle = new Bundle();
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getBookMarkVideo(
                            regId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        BookMarkVideoResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        BookMarkVideoResponse.class);

                        if (myclass1Object.getSuccessBookmarkedVideo() != null) {
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else {
                            mResultCode = ResponseCodes.NOLIST;
                        }


                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetPMDetails() {
        Bundle bundle = new Bundle();
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getPMDetails(
                            regId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("SuccessresultTask") != null) {


                            PMResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            PMResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);


    }

    private void handleGetInfoDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getInfoDetails(
                            schoolid
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("schoolInformation") != null) {


                            InformationRequest myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            InformationRequest.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void getLeaderboardData(Bundle bundle) {
        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getLeaderBoardData(
                            (int) RegPrefManager.getInstance(this).getRegId(),
                            bundle.getInt("Result_ID")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            LeaderBoardResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            LeaderBoardResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void getTestHighlightsData(Bundle bundle) {


        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getTestHighlightsData(
                            (int) RegPrefManager.getInstance(this).getRegId(),
                            bundle.getInt("Result_ID")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            TestHighlightsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            TestHighlightsResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void updateScholarshipScratch(Bundle bundle) {


        try {

            RestService restService = RestServiceBuilder.get().getRestService();
            Call<ScholarShipScratch> scholarShipCall = restService.
                    SaveScratch(
                            bundle.getLong("RegId"),
                            bundle.getInt("ScholarId")
                    );

            Response<ScholarShipScratch> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();
                if (mResultCode == 200) {
                    if (response.body() != null) {
                        bundle.putBoolean(
                                IntentHelper.EXTRA_DATA2, true);
                        SharedPreferences sharedPreferences
                                = getSharedPreferences("scratch_data", MODE_PRIVATE);
                        sharedPreferences.edit().putBoolean("scratch", true).apply();
                    } else
                        bundle.putBoolean(
                                IntentHelper.EXTRA_DATA2, false);

                }
            }

        } catch (Exception e) {

        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void getScholarshipScratch(Bundle bundle) {

        try {

            RestService restService = RestServiceBuilder.get().getRestService();
            Call<ScratchSuccess> scholarShipCall = restService.
                    GetScratch(bundle.getLong("RegID"));

            Response<ScratchSuccess> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {
                    if (response.body() != null) {
                        if (response.body().getScratch() != null) {
                            bundle.putBoolean(
                                    IntentHelper.EXTRA_DATA2, true);

                            SharedPreferences sharedPreferences
                                    = getSharedPreferences("scratch_data",
                                    MODE_PRIVATE);
                            sharedPreferences.edit().putBoolean("scratch", true).apply();
                        }

                        bundle.putInt(
                                IntentHelper.EXTRA_DATA1, response.body().getCart());

                    } else
                        bundle.putBoolean(
                                IntentHelper.EXTRA_DATA2, false);

                }
            }

        } catch (Exception e) {

            //Log.v(TAG, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleOrderConfirmation(Bundle bundle) {

        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    OrderConfirmation(
                           RegPrefManager.getInstance(this).getRegId()+"",
                            bundle.getString("OrderID"),
                            bundle.getString("TrackID"),
                            bundle.getInt("Discount")
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if(mResultCode==200) {
                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            // //Log.v(TAG,"yes");


                            OrderConfResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                    OrderConfResponse.class);


                            mResultCode = ResponseCodes.ORDERCONF;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.ORDER_CONFIRM_FAILURE;

                    } else
                        mResultCode = ResponseCodes.ORDER_CONFIRM_FAILURE;
                }
                else
                    mResultCode = ResponseCodes.ORDER_CONFIRM_FAILURE;

            }

        } catch (Exception e) {

        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetPackages() {
        Bundle bundle = new Bundle();
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    GetPackages(
                            AppConfig.Auth_Key,
                            (int) RegPrefManager.getInstance(this).getRegId()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            // //Log.v(TAG,"yes");


                            GetPackageResponse myclass1Object = new Gson().fromJson(jsonObject.toString(),
                                    GetPackageResponse.class);


                            mResultCode = ResponseCodes.PACKAGES_SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object.getSuccess());
                        } else
                            mResultCode = ResponseCodes.PACKAGES_FAILURE;

                    } else
                        mResultCode = ResponseCodes.PACKAGES_FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void getAnalyticsDetailsData() {
        Bundle bundle = new Bundle();
        try {


            long userId = RegPrefManager.getInstance(this).getRegId();
            long classId = RegPrefManager.getInstance(this).getKeyClassId();


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getAnalyticsDetails(
                            (int) userId,
                            classId + ""
                            //classId + ""
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            AnalyticsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            AnalyticsResponse.class);


                            //Log.v(TAG, myclass1Object.toString());
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);

    }

    private void getAnalyticsSubjectData() {

        Bundle bundle = new Bundle();
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getAnalyticsSubject(
                            (int) RegPrefManager.getInstance(this).getRegId(),
                            (int) RegPrefManager.getInstance(this).getclassid()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            AnalyticsSubjectResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            AnalyticsSubjectResponse.class);


                            //Log.v(TAG, myclass1Object.toString());
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void getSubConceptData() {
        Bundle bundle = new Bundle();
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getSubConceptAnalysis(
                            (int) RegPrefManager.getInstance(this).getRegId(),
                            (int) RegPrefManager.getInstance(this).getclassid()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            SubConceptResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            SubConceptResponse.class);


                            //Log.v(TAG, myclass1Object.toString());
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void getLogoutData() {
        Bundle bundle = new Bundle();
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    Context.MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            String sessionId = preference.getString("SessionID", null);
            int session_id = 0;
            if (sessionId != null) {
                session_id = Integer.parseInt(sessionId);
            }

            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getLogoutStatus(
                            session_id,
                            (int) RegPrefManager.getInstance(this).getRegId()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        //Log.v(TAG, "yes");
                        if (jsonObject.optString("success") != null) {


                            LogoutResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            LogoutResponse.class);


                            mResultCode = ResponseCodes.LOGOUTSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.LOGOUTFAILURE;

                    } else
                        mResultCode = ResponseCodes.LOGOUTFAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleAddToCart(Bundle bundle) {

        try {

            PackageModel packageModel = bundle.getParcelable(IntentHelper.EXTRA_DATA1);

            CartData cartData = new CartData();
            cartData.setId((int) RegPrefManager.getInstance(this).getRegId());
            cartData.setLmt(packageModel.getSubscripttion_Limit() + "");
            cartData.setPkid(packageModel.getPackage_ID() + "");
            cartData.setPrc(packageModel.getDiscPrice() + "");

            ArrayList<CartChapterId> cartChapterIds = new ArrayList<>();
            for (int i = 0; i < packageModel.getChapterList().size(); i++) {
                CartChapterId cartChapterId = new CartChapterId();
                cartChapterId.setChpter_id(packageModel.getChapterList().get(i));
                cartChapterIds.add(cartChapterId);
            }
            cartData.setChkpackage(cartChapterIds);

            AddToCartRequest addToCartRequest = new AddToCartRequest();
            addToCartRequest.setChkpackaged(cartData);

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    addToCart(
                            AppConfig.Auth_Key,
                            addToCartRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {
                    mResultCode = ResponseCodes.ADD_TO_CART_SUCCESS;


                        //JSONObject jsonObject = new JSONObject(response.body().string());

                        //if (jsonObject.optString("success") != null) {


                    } else
                        mResultCode = ResponseCodes.ADD_TO_CART_FAILURE;


            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetCartData() {
        Bundle bundle = new Bundle();
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getCartData(
                            AppConfig.Auth_Key,
                            (int) RegPrefManager.getInstance(this).getRegId()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        //Log.v(TAG, "yes");
                        if (jsonObject.optString("success") != null) {


                            GetCartDataResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            GetCartDataResponse.class);


                            mResultCode = ResponseCodes.GET_CART_DATA_SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object.getSuccess());
                        } else
                            mResultCode = ResponseCodes.GET_CART_DATA_FAILURE;

                    } else
                        mResultCode = ResponseCodes.GET_CART_DATA_FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetProfileData() {
        Bundle bundle = new Bundle();
        try {

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    profileData(
                            AppConfig.Auth_Key,
                            (int) RegPrefManager.getInstance(this).getRegId()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            ProfileDataResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ProfileDataResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetSubject() {
        Bundle bundle = new Bundle();
        try {
            long regId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getSubjectName(
                            regId

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONArray jsonObject = new JSONArray(response.body().string());

                        ArrayList<SubjectResponse> myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        new TypeToken<ArrayList<SubjectResponse>>() {
                                        }.getType());


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelableArrayList(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetOlympiadData() {

        Bundle bundle = new Bundle();
        try {

            String Boardid = RegPrefManager.getInstance(this).getKeyBoardId();
            int classid = (int) RegPrefManager.getInstance(this).getKeyClassId();
            int boardid = 0;
            if (Boardid != null) {
                boardid = Integer.parseInt(Boardid);
            }


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getOlympiadsData(

                            boardid,
                            classid


                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            OlympiadDataResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            OlympiadDataResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);

    }


    private void handleGetPrvsYrPaperData() {
        Bundle bundle = new Bundle();
        try {

            String Boardid = RegPrefManager.getInstance(this).getKeyBoardId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            int boardid = 0;
            if (Boardid != null) {
                boardid = Integer.parseInt(Boardid);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getPrvsYearPaperData(
                            boardid,
                            classid

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            PrvYrPaperResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            PrvYrPaperResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);

    }


    private void handleDoubtListNewData(Bundle bundle) {
        try {
            long id = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getDoubtListNewData(
                            id,
                            bundle.getInt("subid")
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            DoubtListResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DoubtListResponse.class);


                            mResultCode = ResponseCodes.GETDOUBTSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else if (jsonObject.optString("error") != null) {
                            JSONObject user_error = jsonObject.getJSONObject("error");
                            String error_message = user_error.getString("Message");
                            mResultCode = ResponseCodes.UNEXPECTED_ERROR;
                            bundle.putString(IntentHelper.RESULT_DATA,
                                    error_message);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetAppStatus() {
        Bundle bundle = new Bundle();
        try {

            long userId = RegPrefManager.getInstance(this).getRegId();
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getLiveAppStatus(
                            userId,
                            RegPrefManager.getInstance(this).getLoginId()
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        AppStatusResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        AppStatusResponse.class);


                        mResultCode = ResponseCodes.SUCCESS;
                        bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetDoubtListData() {

        Bundle bundle = new Bundle();
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getDoubtListData(
                            userId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            DoubtListResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DoubtListResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else if (jsonObject.optString("error") != null) {
                            JSONObject user_error = jsonObject.getJSONObject("error");
                            String error_message = user_error.getString("Message");
                            mResultCode = ResponseCodes.UNEXPECTED_ERROR;
                            bundle.putString(IntentHelper.RESULT_DATA,
                                    error_message);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetNcertSolution() {
        Bundle bundle = new Bundle();
        try {

            String Boardid = RegPrefManager.getInstance(this).getKeyBoardId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            int boardid = 0;
            if (Boardid != null) {
                boardid = Integer.parseInt(Boardid);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getNcertSolutions(
                            boardid,
                            classid

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            NcertSolutionResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            NcertSolutionResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleGetFeedData() {
        Bundle bundle = new Bundle();
        try {
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getFeedData(
                            AppConfig.Auth_Key

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            FeedResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            FeedResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetFaqData() {
        Bundle bundle = new Bundle();
        try {
            /*SharedPreferences  preference =getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId=0;
            if(user_id!=null)
            {
                userId=Integer.parseInt(user_id);
            }*/
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getFaqData(
                            AppConfig.Auth_Key

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            FAQResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            FAQResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetOrders() {
        Bundle bundle = new Bundle();
        try {

            long id = RegPrefManager.getInstance(this).getRegId();

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    myorders(
                            AppConfig.Auth_Key,
                            (int) id
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            MyOrdersResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            MyOrdersResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleRemoveCartData(Bundle bundle) {

        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            RemoveCartRequest cartRequest = bundle.
                    getParcelable(IntentHelper.EXTRA_DATA1);

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    removePackageFromCart(
                            AppConfig.Auth_Key,
                            cartRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            mResultCode = ResponseCodes.MULTI_CART_REMOVE_SUCCESS;


                        } else
                            mResultCode = ResponseCodes.MULTI_CART_REMOVE_FAILURE;

                    } else
                        mResultCode = ResponseCodes.MULTI_CART_REMOVE_FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetPsychoExamData() {
        Bundle bundle = new Bundle();
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    GetMentorshipExam(

                            1, userId
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());


                        if (jsonObject.optString("success") != null) {
                            PsychometricQuestionList myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            PsychometricQuestionList.class);
                            mResultCode = ResponseCodes.GET_PSYCHO_EXAM_DATA_SUCCESS;
                            bundle.putParcelableArrayList(IntentHelper.RESULT_DATA,
                                    myclass1Object.getSuccess());
                            //Log.v(TAG, "yes");

                        } else
                            mResultCode = ResponseCodes.GET_PSYCHO_EXAM_DATA_FAILURE;

                    } else
                        mResultCode = ResponseCodes.GET_PSYCHO_EXAM_DATA_FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    public void handlePostPsychoExamData(Bundle bundle) {
        try {

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response =
                    restService.PostMentorshipExamResult(
                            (PsychometricPostRequest)
                                    bundle.getParcelable("data"))
                            .execute();
            //Log.v(TAG, response.body() + "");
            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            PsychometricPostData myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            PsychometricPostData.class);
                            mResultCode = ResponseCodes.POST_PSYCHO_EXAM_DATA_SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else
                            mResultCode = ResponseCodes.POST_PSYCHO_EXAM_DATA_FAILURE;

                    } else
                        mResultCode = ResponseCodes.POST_PSYCHO_EXAM_DATA_FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetLearnChapDetails(Bundle bundleb) {
        Bundle bundle = new Bundle();
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    LearnChapterdetails(
                            AppConfig.Auth_Key,
                            userId, bundleb.getInt("ChapIdKey", 0)
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        //Log.v(TAG, "yes");
                        if (jsonObject.optString("success") != null) {


                            LearnChapResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            LearnChapResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object.getSuccess());
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostSetUnsetBookMark(Bundle bundleb) {
        try {

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response =
                    restService.SetUnsetBookMark(
                            (BookMarkRequest)
                                    bundleb.getParcelable("data"))
                            .execute();
            //Log.v(TAG, response.body() + "");
            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            SetUnsetBookMarkSuccess myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            SetUnsetBookMarkSuccess.class);
                            mResultCode = ResponseCodes.SUCCESS;
                            bundleb.putParcelable(IntentHelper.ACTION_SET_UNSET_BOOKMARKS,
                                    myclass1Object);

                        } else
                            mResultCode = ResponseCodes.SUCCESS;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundleb.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundleb.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundleb);
    }

    private void handlePostDictonary(Bundle bundleb) {
        try {

            DictonaryPostRequest d = new DictonaryPostRequest();
            d.setWord(bundleb.getString("word"));
            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response =
                    restService.PostDictonaryResult(
                            d
                    )
                            .execute();
            //Log.v(TAG, response.body() + "");
            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {
                            DictonaryResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DictonaryResponse.class);
                            mResultCode = ResponseCodes.SUCCESS;
                            bundleb.putParcelable(IntentHelper.ACTION_DICTONARY_POST,
                                    myclass1Object);

                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundleb.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundleb.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundleb);

    }


    private void handlePostHomework(Bundle bundle) {
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            String sectionid = RegPrefManager.getInstance(this).getSectionId();

            HomeworkRequest homeworkRequest = new HomeworkRequest();
            homeworkRequest.setSchoolId(schoolid);
            homeworkRequest.setClassId(classid);
            homeworkRequest.setSectionId(sectionid);
            homeworkRequest.setHomeWorkDate(bundle.getString("homeworkdate"));


            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response = restService.
                    postHomeworkDetails(
                            homeworkRequest
                    )
                    .execute();
            //Log.v(TAG, response.body() + "");
            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            HomeworkResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            HomeworkResponse.class);
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostChangePassword(Bundle bundleb) {
        try {
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            ChangePasswordRequest req = bundleb.getParcelable("change_password");

            req.setId(userId);

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response =
                    restService.Changepassword(
                            AppConfig.Auth_Key,
                            req
                    )
                            .execute();
            //Log.v(TAG, response.body() + "");
            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {
                            ChangePasswordSuccess myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ChangePasswordSuccess.class);
                            mResultCode = ResponseCodes.SUCCESS;
                            bundleb.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);

                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundleb.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            bundleb.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundleb);

    }


    //Added By Abhishek

    private void handleGetFeedbackForm(Bundle bundle) {
        try {


            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            FeedBackFormRequestModel feedBackFormRequestModel = new FeedBackFormRequestModel();

            feedBackFormRequestModel.setCategory(bundle.getString("Category"));
            feedBackFormRequestModel.setClassName(bundle.getString("Class"));
            feedBackFormRequestModel.setMessage(bundle.getString("Message"));
            feedBackFormRequestModel.setPhoneNo(bundle.getString("PhoneNo"));
            feedBackFormRequestModel.setRegd_Id(user_id);

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getFeedbackForm(

                            feedBackFormRequestModel

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            FeedBackFormModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            FeedBackFormModel.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    //Added by Abhishek


    private void handleOrderDetails(Bundle bundle) {
        try {


            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getorderDetails(

                            AppConfig.Auth_Key,
                            String.valueOf(RegPrefManager.getInstance(this).getRegId()),
                            bundle.getString("id")


                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            OrderDetailsModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            OrderDetailsModel.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleDoubtDetails(Bundle bundle) {
        try {


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getDoubtDetails(

                            AppConfig.Auth_Key,
                            bundle.getString("ticket_id")

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            DoubtDetailsModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DoubtDetailsModel.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetExam(Bundle bundle) {
        try {

            long regId = RegPrefManager.getInstance(this).getRegId();
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    GetQuestions(

                            bundle.getString("Exam_ID"),
                            (int) regId

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            GetExamResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            GetExamResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostExamScheduleDetails(Bundle bundle) {
        try {


            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();

            ExamScheduleDetailsRequest examScheduleDetailsRequest = new ExamScheduleDetailsRequest();
            examScheduleDetailsRequest.setSchoolId(schoolid);
            examScheduleDetailsRequest.setClassId(classid);
            examScheduleDetailsRequest.setExamId(bundle.getString("ExamId"));

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postExamScheduleDetails(
                            examScheduleDetailsRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            //change it

                            ExamScheduleDetailsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ExamScheduleDetailsResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostDetailsForumDetails(Bundle bundle) {
        try {


            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();


            GetDiscussionDetailsRequest getDiscussionDetailsRequest =
                    new GetDiscussionDetailsRequest();
            getDiscussionDetailsRequest.setSchoolId(schoolid);
            getDiscussionDetailsRequest.setClassId(classid);
            getDiscussionDetailsRequest.setDiscussionId(bundle.getInt("discussionid"));
            getDiscussionDetailsRequest.setPageSize(Constants.PAGE_SIZE);
            getDiscussionDetailsRequest.setStartIndex(bundle.getInt("index"));

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getDetailsForumDiscussion(
                            getDiscussionDetailsRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Message") != null) {


                            GetDiscusiionDetailsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            GetDiscusiionDetailsResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostAddForumDetails(Bundle bundle) {
        try {

            String regId = String.valueOf(RegPrefManager.getInstance(this).getRegId());


            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            String roleid = String.valueOf(RegPrefManager.getInstance(this).getroleid());

            int roleId = 0;
            if (roleid != null) {
                roleId = Integer.parseInt(roleid);
            }

            int createdBy = 0;
            if (regId != null) {
                createdBy = Integer.parseInt(regId);
            }


            PostDisscussionDetailsRequest postDisscussionDetailsRequest = new PostDisscussionDetailsRequest();
            postDisscussionDetailsRequest.setDiscussionId(bundle.getInt("discussionid"));
            postDisscussionDetailsRequest.setCreatedBy(createdBy);
            postDisscussionDetailsRequest.setRoleID(roleId);
            postDisscussionDetailsRequest.setDetailText(bundle.getString("titleA"));


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postAddForumDetails(
                            postDisscussionDetailsRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Message") != null) {


                            PostDiscussionDetailsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            PostDiscussionDetailsResponse.class);


                            mResultCode = ResponseCodes.ADDFORUMSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostAddDiscussionDetails(Bundle bundle) {
        try {


            String regId = String.valueOf(RegPrefManager.getInstance(this).getRegId());

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            String roleid = String.valueOf(RegPrefManager.getInstance(this).getroleid());

            int roleId = 0;
            if (roleid != null) {
                roleId = Integer.parseInt(roleid);
            }

            int createdBy = 0;
            if (regId != null) {
                createdBy = Integer.parseInt(regId);
            }


            AddDiscussionRequest addDiscussionRequest = new AddDiscussionRequest();
            addDiscussionRequest.setSchoolId(schoolid);
            addDiscussionRequest.setCreatedBy(createdBy);
            addDiscussionRequest.setRoleID(roleId);
            addDiscussionRequest.setTitle(bundle.getString("title"));


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postAddDiscussionDetails(
                            addDiscussionRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Message") != null) {


                            AddDiscussionResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            AddDiscussionResponse.class);


                            mResultCode = ResponseCodes.ADDFORUMSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostDiscussionDetails(Bundle bundle) {

        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();


            DiscussionRequest discussionRequest = new DiscussionRequest();
            discussionRequest.setSchoolId(schoolid);
            discussionRequest.setPageSize(Constants.PAGE_SIZE);
            discussionRequest.setStartIndex(bundle.getInt("index"));


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postDisscussionDetails(
                            discussionRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            DiscussionResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DiscussionResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostNoticesDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            String sectionid = RegPrefManager.getInstance(this).getSectionId();

            NoticesRequest noticesRequest = new NoticesRequest();
            noticesRequest.setSchoolId(schoolid);
            noticesRequest.setClassId(classid);
            noticesRequest.setSectionId(sectionid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postNoticesDetails(
                            noticesRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            NoticesResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            NoticesResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostNewDashBoard() {
        Bundle bundle = new Bundle();
        try {


            long Regid = RegPrefManager.getInstance(this).getRegId();

            int userIdID = (int) Regid;
            DashboardRequest dashboardRequest = new DashboardRequest();
            dashboardRequest.setRegID(userIdID);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getNewDashBoard(
                            dashboardRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            DashboardResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DashboardResponse.class);


                            RegPrefManager.getInstance(this).setKeyBoardId(String.valueOf(myclass1Object.getSuccess().getBoardid()));
                            RegPrefManager.getInstance(this).setMentorid(myclass1Object.getSuccess().getMentorid());
                            RegPrefManager.getInstance(this).setschoolId(myclass1Object.getSuccess().getSchoolid());
                            RegPrefManager.getInstance(this).setclassid(myclass1Object.getSuccess().getClassid());
                            RegPrefManager.getInstance(this).setroleid(myclass1Object.getSuccess().getRoleid());
                            RegPrefManager.getInstance(this).setIsMentor(myclass1Object.getSuccess().isIs_Mentor());
                            RegPrefManager.getInstance(this).setResultCount(myclass1Object.getSuccess().getResultCount());
                            RegPrefManager.getInstance(this).setIsSectionEnabled(myclass1Object.getSuccess().isSectionEnabled());
                            RegPrefManager.getInstance(this).setIsSchool(myclass1Object.getSuccess().isIs_School());
                            RegPrefManager.getInstance(this).setCoinCount(myclass1Object.getSuccess().getTotalCoins());

                            String mentorVideoLink = myclass1Object.getSuccess().getMentorVideoLink();
                            mentorVideoLink = mentorVideoLink.replace("\"", "");

                            RegPrefManager.getInstance(this).setMentorVideoLink(mentorVideoLink);
                            RegPrefManager.getInstance(this).setDoubtCount(myclass1Object.getSuccess().getDoubtCount());
                            RegPrefManager.getInstance(this).setschoolCode
                                    (myclass1Object.getSuccess().getSchoolCode());
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostClassDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();


            GetClassRequest classRequest = new GetClassRequest();
            classRequest.setSchoolId(schoolid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postClassDetails(
                            classRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("list") != null) {


                            GetClassResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            GetClassResponse.class);


                            mResultCode = ResponseCodes.CLASSSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostBoardDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();


            BoardRequest boardRequest = new BoardRequest();
            boardRequest.setSchoolId(schoolid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postBoardDetails(
                            boardRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            BoardResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            BoardResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostScLeaderboardDetails(Bundle bundle) {
        try {


            String schoolid = RegPrefManager.getInstance(this).getSchoolId();


            ScLeaderboardRequest scLeaderboardRequest = new ScLeaderboardRequest();
            scLeaderboardRequest.setSchoolId(schoolid);
            scLeaderboardRequest.setClassId(bundle.getInt("classId"));

            RestService restService = RestServiceBuilder.get().
                    getRestService();

            Call<ResponseBody> scholarShipCall = restService.
                    postScLeaderboardDetails(

                            scLeaderboardRequest


                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            ScLeaderboardResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ScLeaderboardResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostToppersDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();


            ToppersRequest toppersRequest = new ToppersRequest();
            toppersRequest.setSchoolId(schoolid);
            toppersRequest.setClassId(classid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postToppersDetails(
                            toppersRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            TopperResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            TopperResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostStudyMaterialsDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            String sectionid = RegPrefManager.getInstance(this).getSectionId();

            StudyMaterialsRequest studyMaterialsRequest = new StudyMaterialsRequest();
            studyMaterialsRequest.setSchoolId(schoolid);
            studyMaterialsRequest.setClassId(classid);
            studyMaterialsRequest.setSectionId(sectionid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postStudyMaterialsDetails(
                            studyMaterialsRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            StudyMaterialsResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            StudyMaterialsResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostAssignedTeacherDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            String sectionid = RegPrefManager.getInstance(this).getSectionId();

            AssignedTeacherRequest assignedTeacherRequest = new AssignedTeacherRequest();
            assignedTeacherRequest.setSchoolId(schoolid);
            assignedTeacherRequest.setClassId(classid);
            assignedTeacherRequest.setSectionId(sectionid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postAssignedTeacher(
                            assignedTeacherRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            AssignedTeacherSuccess myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            AssignedTeacherSuccess.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostDailyTTDetails() {
        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();
            String sectionid = RegPrefManager.getInstance(this).getSectionId();

            DailyTTRequest dailyTTRequest = new DailyTTRequest();
            dailyTTRequest.setSchoolId(schoolid);
            dailyTTRequest.setClassId(classid);
            dailyTTRequest.setSectionId(sectionid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postDailyTimeTable(
                            dailyTTRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            DailyTTResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DailyTTResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);
    }

    private void handlePostExamSchedule() {

        Bundle bundle = new Bundle();
        try {

            String schoolid = RegPrefManager.getInstance(this).getSchoolId();
            int classid = (int) RegPrefManager.getInstance(this).getclassid();

            ExamScheduleRequest examScheduleRequest = new ExamScheduleRequest();
            examScheduleRequest.setSchoolId(schoolid);
            examScheduleRequest.setClassId(classid);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postExamSchedule(
                            examScheduleRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("ExamScheduleList") != null) {


                            ExamScheduleResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ExamScheduleResponse.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            bundle.putString(IntentHelper.RESULT_DATA, e.getMessage() + "");
        }

        mResultReceiver.send(mResultCode, bundle);

    }


    private void handleGetPostExam(Bundle bundle) {
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            PostQuestionRequestModel req = bundle.getParcelable("data");
            req.getGet_question().setStudent_id(String.valueOf(regId));

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postExamData(

                            AppConfig.Auth_Key,
                            req

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            GetPostExamData myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            GetPostExamData.class);


                            mResultCode = ResponseCodes.STATUS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostEditProfile(Bundle bundle) {
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();


            EditProfileRequestNew editProfileRequest = new EditProfileRequestNew();
            editProfileRequest.setRegd_ID(regId);
            editProfileRequest.setBoard_ID(bundle.getInt("boardId"));
            editProfileRequest.setClass_ID(bundle.getInt("classId"));
            editProfileRequest.setCustomer_Name(bundle.getString("name"));
            editProfileRequest.setSecurity_Answer(bundle.getString(null));
            editProfileRequest.setSecurity_Question_ID(bundle.getInt(null));
            editProfileRequest.setEmail(bundle.getString("email"));
            editProfileRequest.setPhone(bundle.getString("phone"));
            editProfileRequest.setOrganisation_Name(bundle.getString("schoolName"));
            editProfileRequest.setSectionname(bundle.getString("sectionName"));
            editProfileRequest.setSectionId(bundle.getString("sectionId"));
            editProfileRequest.setSchoolId(bundle.getString("schoolId"));

            // //Log.d(TAG,"value====>");

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postEditProfile(
                            editProfileRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            //change it

                            EditProfileResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            EditProfileResponse.class);


//                            RegPrefManager.getInstance(this)
//                                    .setSectionId(bundle.getString("sectionId"));

//                            if(bundle.getString("sectionId")!=null&&
//                                    !bundle.getString("sectionId")
//                                    .equals("null")&&!bundle.getString("sectionId").
//                                    equals("\"00000000-0000-0000-0000-000000000000\"")) {
//                                RegPrefManager.getInstance(this)
//                                        .setSectionId(bundle.getString("sectionId"));
//
//                                RegPrefManager.getInstance(this).setIsSectionEnabled(true);
//                            }


                            mResultCode = ResponseCodes.PROFILESUBMITSUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleCheckoutDetails(Bundle bundle) {
        try {


            SharedPreferences preference = getSharedPreferences("user_data",
                    getApplicationContext().MODE_PRIVATE);
            String user_id = preference.getString("User_ID", null);
            int userId = 0;
            if (user_id != null) {
                userId = Integer.parseInt(user_id);
            }

            String regIdId = String.valueOf(RegPrefManager.getInstance(this).getRegId());

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getCheckoutData(

                            AppConfig.Auth_Key,
                            regIdId


                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            CheckoutDataModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            CheckoutDataModel.class);


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handlePostLearn(Bundle bundle) {
        try {


            long userId = RegPrefManager.getInstance(this).getRegId();
            String id = String.valueOf(userId);

            NewLearnRequest newLearnRequest = new NewLearnRequest();
            newLearnRequest.setRegID(id);


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postLearnDetails(newLearnRequest);

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            NewLearnResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            NewLearnResponse.class);

                            //Log.v(TAG, myclass1Object.toString());


                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else {

                    mResultCode = ResponseCodes.FAILURE;

                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleSetUnsetBookmark(Bundle bundle) {

        try {


            long regId = RegPrefManager.getInstance(this).getRegId();


            SetBookmarkRequest setBookmarkRequest = new SetBookmarkRequest();
            setBookmarkRequest.setRegID(regId);
            setBookmarkRequest.setDatatypeId(bundle.getString("dataType"));
            setBookmarkRequest.setDataId(bundle.getString("dataId"));


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    setUnsetBookmark(
                            setBookmarkRequest
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            //change it

                            SetBookmarkResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            SetBookmarkResponse.class);

                            if (myclass1Object.getSuccess().getMessage().equals("Bookmarked successfully.")) {
                                mResultCode = ResponseCodes.BOOKMARKED;
                                bundle.putParcelable(IntentHelper.RESULT_DATA,
                                        myclass1Object);
                                bundle.putString("type", "added");
                            } else {
                                mResultCode = ResponseCodes.UNBOOKMARKED;
                                bundle.putParcelable(IntentHelper.RESULT_DATA,
                                        myclass1Object);
                                bundle.putString("type", "deleted");
                            }

                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else if (mResultCode == 409) {

                    mResultCode = ResponseCodes.USER_ALREADY_DONE;

                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);

    }

    private void handleSignUp(Bundle bundle) {
        try {


            String d = null;
            if (d == null)
                d = FirebaseInstanceId.getInstance().getToken();

            Registration signupRequest = new Registration();
            signupRequest.setRegdId(bundle.getLong("regId"));
            signupRequest.setCustomerName(bundle.getString("customerName"));
            signupRequest.setMobile(bundle.getString("mobile"));
            signupRequest.setEmail(bundle.getString("email"));
            signupRequest.setDeviceId(d);
            signupRequest.setPassword(bundle.getString("password"));
            signupRequest.setCoinTypeId(RegPrefManager.getInstance(
                    this).getTypeId());
            signupRequest.setInvitorMobile(RegPrefManager.getInstance(
                    this).getInvitorFamilyId());

            signupRequest.setOrganisationName(bundle.getString("schoolName"));
            signupRequest.setSchoolId(bundle.getString("schoolId"));
            signupRequest.setSectionClass(bundle.getString("schoolName"));
            signupRequest.setSectionId(bundle.getString("sectionName"));
            RegistrationDtl dtl = new RegistrationDtl();
            dtl.setBoardId(bundle.getLong("boardId"));
            dtl.setClassId(bundle.getLong("classId"));
            signupRequest.setRegistrationDtl(dtl);
            //  //Log.v("TAGCLASS",bundle.getInt("classId")+"");
            //  //Log.v("TAGBOARD",bundle.getInt("boardId")+"");

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postSignupDetails(signupRequest);

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 201) {
                    RegPrefManager.getInstance(
                            this).setTypeId(0);

                    RegPrefManager.getInstance(
                            this).setInvitorFamilyId(null);
                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            Registration myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            Registration.class);

                            //Log.v(TAG, myclass1Object.toString());

                            RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());

                            RegPrefManager.getInstance(this).setStatus(1);
                            RegPrefManager.getInstance(this).setUserName(myclass1Object.getCustomerName());
                            RegPrefManager.getInstance(this).setImageString(myclass1Object.getImage());
                            RegPrefManager.getInstance(this).setRegId(myclass1Object.getRegdId());
                            RegPrefManager.getInstance(this).setPhone(myclass1Object.getMobile());
                            RegPrefManager.getInstance(this).setEmail(myclass1Object.getEmail());
                            RegPrefManager.getInstance(this).setKeyBoardId(String.valueOf(myclass1Object.getRegistrationDtl().getBoardId()));
                            RegPrefManager.getInstance(this).setKeyClassId(myclass1Object.getRegistrationDtl().getClassId());
                            RegPrefManager.getInstance(this).setSchoolName(myclass1Object.getOrganisationName());
                            RegPrefManager.getInstance(this).setSchoolId(myclass1Object.getSchoolId());
                            RegPrefManager.getInstance(this).setSectionId(myclass1Object.getSectionId());
                            RegPrefManager.getInstance(this).setBoardName(myclass1Object.getBoard());
                            RegPrefManager.getInstance(this).setClassName(myclass1Object.getClassName());

                            RegPrefManager.getInstance(this).setsectionName(myclass1Object.getSectionName());
                            RegPrefManager.getInstance(this).setInvitorFamilyId(null);
                            RegPrefManager.getInstance(this).setTypeId(0);
                            RegPrefManager.getInstance(this).setLoginId(myclass1Object.getLoginStatus().
                                    getId());


                            RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());
                            mResultCode = ResponseCodes.SUCCESSFULLYSIGNEDUP;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else if (mResultCode == 409) {

                    mResultCode = ResponseCodes.USER_ALREADY_DONE;

                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);

    }

    private void handleLogin(Bundle bundle) {
        try {


            String d = null;
            if (d == null)
                d = FirebaseInstanceId.getInstance().getToken();

            LoginDto loginDto = new LoginDto();
            loginDto.setMobile(bundle.getString("MobileKey"));
            loginDto.setPassword(bundle.getString("PasswordKey"));
            loginDto.setDeviceId(d);
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    login(
                            loginDto
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {


                            //change it

                            Registration myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            Registration.class);
                            if (myclass1Object.getRegistrationDtl() == null
                                    || myclass1Object.getRegistrationDtl().getBoardId() == null
                                    || myclass1Object.getRegistrationDtl().getBoardId().
                                    equals("null")
                                    || myclass1Object.getRegistrationDtl().getClassId() == null
                                    || myclass1Object.getRegistrationDtl().getClassId().equals("null")
                                    || myclass1Object.getSectionId() == null
                                    || myclass1Object.getSectionId().equals("null")
                                    || myclass1Object.getSchoolId() == null
                                    || myclass1Object.getSchoolId().equals("null")) {
                                mResultCode = 600;
                                bundle.putParcelable(IntentHelper.RESULT_DATA,
                                        myclass1Object);
                            } else {
                                //Log.v(TAG, myclass1Object.toString());
                                RegPrefManager.getInstance(this).setStatus(1);
                                RegPrefManager.getInstance(this).setUserName(myclass1Object.getCustomerName());
                                RegPrefManager.getInstance(this).setImageString(myclass1Object.getImage());
                                RegPrefManager.getInstance(this).setRegId(myclass1Object.getRegdId());
                                RegPrefManager.getInstance(this).setFamilyInviteLink(myclass1Object.getAddress());
                                RegPrefManager.getInstance(this).setPhone(myclass1Object.getMobile());
                                RegPrefManager.getInstance(this).setEmail(myclass1Object.getEmail());
                                RegPrefManager.getInstance(this).setSchoolName(myclass1Object.getOrganisationName());
                                //RegPrefManager.getInstance(this).setDOB(myclass1Object.getDateOfBirth());
                                RegPrefManager.getInstance(this).setBoardName(myclass1Object.getBoard());
                                RegPrefManager.getInstance(this).setschoolId(myclass1Object.getSchoolId());
                                RegPrefManager.getInstance(this).setClassName(myclass1Object.getClassName());
                                RegPrefManager.getInstance(this).setSectionId(myclass1Object.getSectionId());
                                RegPrefManager.getInstance(this).setsectionName(myclass1Object.getSectionName());

                                RegPrefManager.getInstance(this).setKeyBoardId(String.valueOf(myclass1Object.getRegistrationDtl().getBoardId()));
                                long my = myclass1Object.getRegistrationDtl().getClassId();
                                RegPrefManager.getInstance(this).setKeyClassId(myclass1Object.
                                        getRegistrationDtl().getClassId());
                                RegPrefManager.getInstance(this).setLoginId(myclass1Object
                                        .getLoginStatus().
                                        getId());

                                mResultCode = ResponseCodes.SUCCESS;
                                bundle.putParcelable(IntentHelper.RESULT_DATA,
                                        myclass1Object);
                            }
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else if (mResultCode == 204) {

                    mResultCode = ResponseCodes.NO_USER_FOUND;

                } else if (mResultCode == 400) {

                    mResultCode = ResponseCodes.PASSWORD_MISMATCH;

                } else if (mResultCode == 500) {

                    mResultCode = ResponseCodes.SERVER_ERROR;
                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleLogout() {

        Bundle resultData = new Bundle();
        try {
            String mobile = RegPrefManager.getInstance(this).getPhone() + "";

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> chatResponse
                    = restService.logout(
                    mobile
            )
                    .execute();
            //Log.v(TAG, chatResponse.body() + "");
            if (chatResponse != null) {
                RegPrefManager.getInstance(this).logout();
                mResultCode = chatResponse.code();
            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);
    }

    private void handleGetCoinLeaderboard() {

        Bundle resultData = new Bundle();
        try {


            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> chatResponse
                    = restService.getCoinLeaderboard(
                    RegPrefManager.getInstance(this)
                            .getRegId()
            )
                    .execute();
            //Log.v(TAG, chatResponse.body() + "");
            if (chatResponse != null) {

                JSONArray jsonObject = new JSONArray(chatResponse.body().string());

                ArrayList<CoinLeaderboardResponse> myclass1Object = new Gson().
                        fromJson(jsonObject.toString(),
                                new TypeToken<ArrayList<CoinLeaderboardResponse>>() {
                                }.getType());

                mResultCode = ResponseCodes.SUCCESS;
                resultData.putParcelableArrayList(IntentHelper.RESULT_DATA,
                        myclass1Object);

            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);
    }

    private void handleAssignSchoolZone(Bundle bundle) {

        Bundle resultData = new Bundle();
        try {

            //Log.v(TAG, "yes");

            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> chatResponse
                    = restService.AssignSchoolZone(bundle.getLong("regId"),
                    bundle.getString("schoolId"),
                    bundle.getString("scolCode"))
                    .execute();

            if (chatResponse != null) {

                mResultCode = chatResponse.code();

                if (mResultCode == 200) {
                    mResultCode = 200;
                    //Log.v(TAG,bundle.getString("scolCode"));
                    RegPrefManager.getInstance(this).
                            setschoolCode(bundle.getString("scolCode"));
                    RegPrefManager.getInstance(this).setIsSectionEnabled(true);

                } else if (mResultCode == 204) {

                }
                if (mResultCode == 409) {

                }

            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, resultData);
    }


    private void handlePostImageDetails(Bundle bundle) {
        try {


            long regId = RegPrefManager.getInstance(this).getRegId();
            String regI = String.valueOf(regId);
            File file = null;
            MultipartBody.Part filePart = null;
            if (bundle.getString("image") != null) {
                file = new File(bundle.getString("image"));

            }


            RequestBody regIdid = RequestBody.create(MediaType.parse("text/plain"), regI);
            if (file != null) {
                filePart = MultipartBody.Part.createFormData("Image",
                        file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            }


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    postImageEditProfile(
                            regIdid, filePart
                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("Success") != null) {


                            //change it

                            UploadImageResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            UploadImageResponse.class);
                            RegPrefManager.getInstance(this).setImageString(myclass1Object.getSuccess().getImage());

                            mResultCode = ResponseCodes.IMAGESUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);
    }


    private void handleReplyDoubt(Bundle bundle) {
        try {

            String re = String.valueOf(RegPrefManager.getInstance(this).getRegId());

            File file = null;
            MultipartBody.Part filePart = null;
            if (bundle.getString("Image") != null) {
                file = new File(bundle.getString("Image"));

            }


            RequestBody ticketID = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("ticketID"));
            RequestBody answerId = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("answerId"));
            RequestBody msgDetails = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("msgDetails"));
            RequestBody regId = RequestBody.create(MediaType.parse("text/plain"), re);

            if (file != null) {
                filePart = MultipartBody.Part.createFormData("File",
                        file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            }


            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response
                    = restService.postReplyDoubt(
                    ticketID, answerId, msgDetails, regId, filePart
            )
                    .execute();

            if (response != null) {

                mResultCode = response.code();


                if (mResultCode == 200) {
                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {

                            //change it

                            DoubtReplyResponse myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DoubtReplyResponse.class);

                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else if (mResultCode == 500) {

                    mResultCode = ResponseCodes.SERVER_ERROR;
                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleGetSubmitDoubt(Bundle bundle) {

        try {

            String re = String.valueOf(RegPrefManager.getInstance(this).getRegId());

            File file = null;
            MultipartBody.Part filePart = null;
            if (bundle.getString("Image") != null) {
                file = new File(bundle.getString("Image"));

            }


            RequestBody regId = RequestBody.create(MediaType.parse("text/plain"), re);
            RequestBody Class_id = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("Class_id"));
            RequestBody Subject_ID = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("Subject_ID"));
            RequestBody Board_id = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("Board_id"));
            RequestBody Chapter_ID = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("Chapter_ID"));
            RequestBody Question_Detail = RequestBody.create(MediaType.parse("text/plain"), bundle.getString("Question_Detail"));


            if (file != null) {
                filePart = MultipartBody.Part.createFormData("Image",
                        file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
            }

            //retrofit2.Call<okhttp3.ResponseBody> req = service.postImage(body, name);

//
//            SubmitDoubtRequestParaModel submitDoubtRequestParaModel = new SubmitDoubtRequestParaModel();
//
//            submitDoubtRequestParaModel.setRegId(re);
//            submitDoubtRequestParaModel.setClass_id(bundle.getString("Class_id"));
//            submitDoubtRequestParaModel.setSubject_ID(bundle.getString("Subject_ID"));
//            submitDoubtRequestParaModel.setBoard_id(bundle.getString("Board_id"));
//            submitDoubtRequestParaModel.setChapter_ID(bundle.getString("Chapter_ID"));
//            submitDoubtRequestParaModel.setQuestion_Detail(bundle.getString("Question_Detail"));
//            submitDoubtRequestParaModel.setImage(bundle.getString("Image"));


            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> response
                    = restService.postsubmitdoubt(
                    regId, Class_id, Subject_ID, Board_id, Chapter_ID, Question_Detail, filePart
            )
                    .execute();

            if (response != null) {

                mResultCode = response.code();


                if (mResultCode == 200) {
                    if (response.body() != null) {
                        JSONObject jsonObject = new JSONObject(response.body().string());

                        if (jsonObject.optString("success") != null) {

                            //change it

                            DoubtSubmitModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DoubtSubmitModel.class);

                            mResultCode = ResponseCodes.STATUS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                } else if (mResultCode == 204) {

                    mResultCode = ResponseCodes.NO_USER_FOUND;

                } else if (mResultCode == 400) {

                    mResultCode = ResponseCodes.PASSWORD_MISMATCH;

                } else if (mResultCode == 500) {

                    mResultCode = ResponseCodes.SERVER_ERROR;
                }

            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }


        mResultReceiver.send(mResultCode, bundle);
    }

    private void handleSaveVideoCoins(Bundle bundle) {

        Bundle resultData = new Bundle();
        try {


            RegPrefManager.getInstance(this).setCoinCount(
                    RegPrefManager.getInstance(this).getCoinCount() + 15
            );


            ContentValues contentValues = new ContentValues();
            contentValues.put(DbContract.CoinTable.COIN_ID, 0);
            contentValues.put(DbContract.CoinTable.COIN_TYPE, 2);
            contentValues.put(DbContract.CoinTable.INSERTED_DATE, System.currentTimeMillis());
            contentValues.put(DbContract.CoinTable.COINS, 15);
            contentValues.put(DbContract.CoinTable.MODULE_ID, bundle.getLong("moduleid"));


            Uri uri = getContentResolver().
                    insert(DbContract.CoinTable.CONTENT_URI, contentValues);

            Cursor cursor = getContentResolver().query(DbContract.CoinTable.CONTENT_URI,
                    DbContract.CoinTable.MEMBER_PROJECTION,
                    null, null, null);
            ArrayList<CoinEarn> coins = new ArrayList<>();
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                do {

                    CoinEarn coinEarn = new CoinEarn();
                    coinEarn.setCoins(cursor.getInt(cursor
                            .getColumnIndex(DbContract.CoinTable.COINS)));

                    coinEarn.setCoinType(cursor.getLong(cursor
                            .getColumnIndex(DbContract.CoinTable.COIN_TYPE)));
                    coinEarn.setActive(true);
                    coinEarn.setRegdId(RegPrefManager.getInstance(this).getRegId());
                    coinEarn.setInsertedOn(new Date(
                            cursor.getLong(cursor
                                    .getColumnIndex(DbContract.CoinTable.INSERTED_DATE))
                    ));
                    coinEarn.setRowId(cursor.getInt(cursor
                            .getColumnIndex(DbContract.CoinTable._ID)));

                    coinEarn.setTypeId(cursor.getLong(cursor
                            .getColumnIndex(DbContract.CoinTable.MODULE_ID)));

                    coins.add(coinEarn);
                }
                while (cursor.moveToNext());
            }


            //Log.v(TAG, coins.size() + "");
            RestService restService = RestServiceBuilder.
                    get().getRestService();
            Response<ResponseBody> chatResponse
                    = restService.saveVideoCoins(coins)
                    .execute();

            if (chatResponse != null) {

                mResultCode = chatResponse.code();

                if (mResultCode == 201) {
                    mResultCode = 201;
                    JSONArray jsonObject = new JSONArray(chatResponse.body().string());

                    if (jsonObject != null) {
                        ArrayList<CoinEarn> myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        new TypeToken<ArrayList<CoinEarn>>() {
                                        }.getType());
                        if (myclass1Object != null) {
                            ArrayList<ContentProviderOperation> ops = new ArrayList<>();
                            for (int i = 0; i < myclass1Object.size(); i++) {
                                ops.add(ContentProviderOperation
                                        .newDelete(DbContract.CoinTable.CONTENT_URI)
                                        .withSelection(DbContract.CoinTable._ID + " like ? ",
                                                new String[]{myclass1Object.get(i).getRowId() + ""})


                                        .build());

                            }

                            if (ops.size() > 0) {
                                try {
                                    getContentResolver().
                                            applyBatch(DbContract.Schema.CONTENT_AUTHORITY, ops);
                                } catch (RemoteException e) {
                                    // do s.th.
                                    //Log.v(TAG, e.getMessage());
                                } catch (OperationApplicationException e) {
                                    // do s.th.
                                    //Log.v(TAG, e.getMessage());
                                }
                            }

                        }
                    }
                }

            } else
                mResultCode = ResponseCodes.RESPONSE_NULL;


        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
            mResultCode = ResponseCodes.EXCEPTION;
            resultData.putString(IntentHelper.RESULT_EXCEPTION, e.getMessage() + "");
        }


        // mResultReceiver.send(mResultCode, resultData);
    }



    private void handleLearnChapDetailsNew(Bundle bundle){
        try {


            long user_id = RegPrefManager.getInstance(this).getRegId();



            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getLearnChapDetails(
                            user_id,
                            bundle.getString("subId")
                    );

            Response<ResponseBody> chatResponse =
                    scholarShipCall.execute();

            if (chatResponse != null) {


                mResultCode = chatResponse.code();

                if (mResultCode == 200) {

                    if (chatResponse.body() != null) {
                        JSONObject jsonObject = new JSONObject(chatResponse.body().string());



                        LearnDetailsResponse myclass1Object = new Gson().
                                fromJson(jsonObject.toString(),
                                        LearnDetailsResponse.class);

                        if(myclass1Object.getSuccess()!=null){
                            mResultCode = ResponseCodes.SUCCESS;
                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        }else{
                            mResultCode = ResponseCodes.FAILURE;
                        }




                    } else
                        mResultCode = ResponseCodes.RESPONSE_NULL;


                }


                mResultReceiver.send(mResultCode, bundle);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void handleDiscountCoupon(Bundle bundle){
        try {


            long user_id = RegPrefManager.getInstance(this).getRegId();



            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getDiscountCoupon(
                            user_id,
                            bundle.getString("coupon")
                    );

            Response<ResponseBody> chatResponse =
                    scholarShipCall.execute();

            if (chatResponse != null) {


                mResultCode = chatResponse.code();

                if (mResultCode == 200) {

                    if (chatResponse.body() != null) {
                        JSONObject jsonObject = new JSONObject(chatResponse.body().string());



                            DiscountCouponSuccess myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            DiscountCouponSuccess.class);

                            if(myclass1Object.getSuccess()!=null){
                                mResultCode = ResponseCodes.DISCOUNTSUCCESS;
                                bundle.putParcelable(IntentHelper.RESULT_DATA,
                                        myclass1Object);
                            }else{
                                mResultCode = ResponseCodes.DISCOUNTUNSUCCESS;
                            }




                    } else
                        mResultCode = ResponseCodes.RESPONSE_NULL;


                }


                 mResultReceiver.send(mResultCode, bundle);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handlerecentlywatched(Bundle bundle) {

        try {


            String user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());

            RecentlyWatchedRequestBody recentlyWatchedRequestBody = new RecentlyWatchedRequestBody();
            recentlyWatchedRequestBody.setRedgId(user_id);
            recentlyWatchedRequestBody.setModuleId(bundle.getString("moduleId"));

            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    RecentlyWatched(
                            recentlyWatchedRequestBody
                    );

            Response<ResponseBody> chatResponse =
                    scholarShipCall.execute();

            if (chatResponse != null) {


                mResultCode = chatResponse.code();

                if (mResultCode == 200) {

                    if (chatResponse.body() != null) {
                        JSONObject jsonObject = new JSONObject(chatResponse.body().string());

                        if (jsonObject.optString("success") != null) {

                            RecentlyWatchedModel myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            RecentlyWatchedModel.class);
                        }

                    } else
                        mResultCode = ResponseCodes.RESPONSE_NULL;


                }


                // mResultReceiver.send(mResultCode, resultData);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleCbtExam(Bundle bundle) {


        try {

            long regId = RegPrefManager.getInstance(this).getRegId();
            String eid = bundle.getString("Chapter_ID");
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getcbtexam(
                            regId,
                            eid

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONArray jsonObject = new JSONArray(response.body().string());

                        if (jsonObject != null) {


                            ArrayList<GetCbtResponseModel> myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            new TypeToken<ArrayList<GetCbtResponseModel>>() {
                                            }.getType());

                            mResultCode = ResponseCodes.STATUS;
                            bundle.putParcelableArrayList(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);


    }

    public void handlePrtExam(Bundle bundle) {


        try {

            long regId = RegPrefManager.getInstance(this).getRegId();
            String eid = bundle.getString("Chapter_ID");
            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getPrtexam(
                            regId,
                            eid

                    );

            Response<ResponseBody> response =
                    scholarShipCall.execute();

            if (response != null) {

                mResultCode = response.code();

                if (mResultCode == 200) {

                    if (response.body() != null) {
                        JSONArray jsonObject = new JSONArray(response.body().string());

                        if (jsonObject != null) {


                            ArrayList<GetPrtExamResponse> myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            new TypeToken<ArrayList<GetPrtExamResponse>>() {
                                            }.getType());

                            mResultCode = ResponseCodes.SUCCESSPRT;
                            bundle.putParcelableArrayList(IntentHelper.RESULT_DATA,
                                    myclass1Object);
                        } else
                            mResultCode = ResponseCodes.FAILURE;

                    } else
                        mResultCode = ResponseCodes.FAILURE;

                }
            } else {
                mResultCode = ResponseCodes.RESPONSE_NULL;
                bundle.putString(IntentHelper.RESULT_DATA, "Response Null");
            }

        } catch (Exception e) {
            //Log.v(TAG, e.getMessage() + "");
        }
        mResultReceiver.send(mResultCode, bundle);


    }


    public void handlechapterdetails(Bundle bundle) {

        try {


            long user_id = RegPrefManager.getInstance(this).getRegId();


            RestService restService = RestServiceBuilder.get().
                    getRestService();
            Call<ResponseBody> scholarShipCall = restService.
                    getchapterdetails(
                            user_id,
                            bundle.getString("chap_id")
                    );

            Response<ResponseBody> Response =
                    scholarShipCall.execute();

            if (Response != null) {


                mResultCode = Response.code();

                if (mResultCode == 200) {

                    if (Response.body() != null) {
                        JSONObject jsonObject = new JSONObject(Response.body().string());



                            ChapterDetailsSuccess myclass1Object = new Gson().
                                    fromJson(jsonObject.toString(),
                                            ChapterDetailsSuccess.class);

                            mResultCode = ResponseCodes.CHAPTERDETAILS;

                            bundle.putParcelable(IntentHelper.RESULT_DATA,
                                myclass1Object);


                    } else{
                        mResultCode = ResponseCodes.FAILURE;
                    }



                }


                 mResultReceiver.send(mResultCode, bundle);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
