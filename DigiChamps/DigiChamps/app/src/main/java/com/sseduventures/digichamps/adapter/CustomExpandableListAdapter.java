package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.Model_Subject;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Model_Subject> expandableListTitle;
    private HashMap<String, ArrayList<Model_Subject>> expandableListDetail;



    public CustomExpandableListAdapter(Context context, ArrayList<Model_Subject> expandableListTitle,
                                           HashMap<String, ArrayList<Model_Subject>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;

    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
       return this.expandableListDetail.get(this.expandableListTitle.get(listPosition).getSub_id()).get(expandedListPosition);
    }


    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText1 = (String) getChild(listPosition, expandedListPosition);
        final String expandedListText2 = (String) getChild2(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item_cart, null);
        }

        TextView question = (TextView) convertView
                .findViewById(R.id.tv_question);
        TextView answer = (TextView) convertView
                .findViewById(R.id.tv_answer);

        question.setText(expandedListText1);
        answer.setText(expandedListText2);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition).getSub_name();
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

            ExpandableListView mExpandableListView = (ExpandableListView) parent;

        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group_cart, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.tv_title);
        listTitleTextView.setText(listTitle);
        return convertView;
    }
    public Object getChild2(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition).getSub_name()).get(expandedListPosition);
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}