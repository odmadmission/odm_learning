package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class TopperResponse implements Parcelable{

    private List<ToppersList> list;
    private boolean status;
    private String message;

    public List<ToppersList> getList() {
        return list;
    }

    public void setList(List<ToppersList> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public TopperResponse() {
    }

    protected TopperResponse(Parcel in) {
        this.list = in.createTypedArrayList(ToppersList.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<TopperResponse> CREATOR = new Creator<TopperResponse>() {
        @Override
        public TopperResponse createFromParcel(Parcel source) {
            return new TopperResponse(source);
        }

        @Override
        public TopperResponse[] newArray(int size) {
            return new TopperResponse[size];
        }
    };
}
