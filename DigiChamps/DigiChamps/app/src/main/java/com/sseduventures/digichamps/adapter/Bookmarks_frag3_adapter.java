package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.Bookmarks_frag3_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.BookmarkQuestionList;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks3;
import com.sseduventures.digichamps.helper.RegPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Bookmarks_frag3_adapter extends RecyclerView.Adapter<Bookmarks_frag3_adapter.MyLearnViewHolder> {

    public static ArrayList<BookmarkQuestionList> dataSet = new ArrayList<>();

    public static int position;
    static CardView container, container1;
    String sub_name, module_id, user_id, resp, error, TAG = "QB";
    View view;

    private Context context;
    private Typeface tf;
    Fragment_Bookmarks3 fragment;

    public Bookmarks_frag3_adapter(ArrayList<BookmarkQuestionList> data, String sub_name, Context context, Fragment_Bookmarks3 fragment) {
        this.context = context;
        this.dataSet = data;
        this.sub_name = sub_name;
        this.fragment = fragment;
        tf = Typeface.createFromAsset(context.getAssets(), "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
    }

    @Override
    public MyLearnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bookmark_frag3, parent, false);
        MyLearnViewHolder myViewHolder = new MyLearnViewHolder(view);

        return myViewHolder;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyLearnViewHolder holder, final int listPosition) {
        TextView chap_names = holder.chap_names;
        chap_names.setText(dataSet.get(holder.getAdapterPosition()).getModuleName());
        container1 = holder.container;

        holder.container.setOnClickListener(onClickListener(listPosition));

        user_id = String.valueOf(RegPrefManager.getInstance(context).getRegId());

        holder.bookmark_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                module_id = dataSet.get(position).getModule_Id()+"";

                fragment.setUnsetBookMark(module_id,"1");
//

            }
        });

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataSet.get(position).getIs_Expire().equalsIgnoreCase("true") && dataSet.get(position).getIs_Free().equalsIgnoreCase("false")) {

                    @SuppressLint("RestrictedApi") final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MyDialogTheme_buy));
                    LayoutInflater factory = LayoutInflater.from(context);
                    final View vieww = factory.inflate(R.layout.dialog_buy, null);
                    Button ok = (Button) vieww.findViewById(R.id.button2);
                    final AlertDialog alertDialog = builder.create();
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            Intent i = new Intent(context, PackageActivityNew.class);
                            Activity activity = (Activity) context;
                            context.startActivity(i);


                        }
                    });

                    builder.setView(vieww);
                    builder.setCancelable(true);
                    builder.show();
                } else {


                    String PDF_URL = dataSet.get(position).getQuestion();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(PDF_URL));
                    context.startActivity(browserIntent);
                }
            }
        };
    }

    public static class MyLearnViewHolder extends RecyclerView.ViewHolder {
        public TextView chap_names;
        public TextView title, year, genre;
        public ImageView play;
        ImageView bookmark_image;
        CardView container;
        private ImageView thumbnail_image;


        public MyLearnViewHolder(View itemView) {
            super(itemView);

            this.chap_names = (TextView) itemView.findViewById(R.id.chap_name);
            container = (CardView) itemView.findViewById(R.id.card_quesbank);
            bookmark_image = (ImageView) itemView.findViewById(R.id.bookmark);

        }
    }


}





