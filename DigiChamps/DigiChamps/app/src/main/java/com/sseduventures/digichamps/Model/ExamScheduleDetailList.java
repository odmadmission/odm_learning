
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExamScheduleDetailList {

    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("TimeSlot")
    @Expose
    private String timeSlot;
    @SerializedName("SubjectName")
    @Expose
    private String subjectName;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

}
