package com.sseduventures.digichamps.utils;

import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;

public class StringUtils {

    private static final String TAG="StringUtils";
public static boolean invalidString(String s)
{
return !validString(s);
}
public static boolean validString(String s) {
if (s == null) {
return false;
}
    if (s.trim().equalsIgnoreCase("")) {
        return false;
    }
    return true;
}
    public static byte[] dataByte;
    public static String base64String;
    public static String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, baos);
         dataByte = baos.toByteArray();
        return Base64.encode(dataByte, 0, dataByte.length);
    }

    public static String byteToBase64(byte[] data) {
        dataByte = data;
        return Base64.encode(data, 0, data.length);



    }
    public static byte[] baseToByte(String s) {

        try {
            return Base64.decode(s);
        } catch (Exception e) {
            //Log.v(TAG, e.getMessage());
             }

        return  null;
    }
    public static byte[] base64ToBitmap(String base64) {

        try {
            byte[] bitmapBytes = Base64.decode(base64);

            return  bitmapBytes;
        }
        catch (Exception e)
        {
            //Log.v(TAG,e.getMessage());
        }

        return  null;

    }

}