package com.sseduventures.digichamps.activities;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.CustomPagerAdapter_Package;
import com.sseduventures.digichamps.fragment.BottomSheetFragment;
import com.sseduventures.digichamps.frags.SignInFragment;
import com.sseduventures.digichamps.frags.SignupSheetFragment;
import com.sseduventures.digichamps.utils.DateTimeUtil;

import java.util.Timer;
import java.util.TimerTask;

public class OnBoardActivity extends FormActivity {
    PageIndicatorView pageIndicatorView;


    private Button login_but;
    private Button signup_but;


    private SignupSheetFragment bottomSheetFragment;
    private SignInFragment signInFragment;
    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_onboard_layout);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        logEvent();
        init();

        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_package);
        viewPager.setAdapter(new CustomPagerAdapter_Onboard(this));


        pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(6); // specify total count of indicators
        pageIndicatorView.setAnimationType(AnimationType.DROP);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {/*empty*/}

            @Override
            public void onPageSelected(int p) {
                pageIndicatorView.setSelection(p);
            }

            @Override
            public void onPageScrollStateChanged(int state) {/*empty*/}
        });



    }

    private void init() {

        login_but = findViewById(R.id.login_but);
        signup_but = findViewById(R.id.signup_but);

        login_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialogFragment1(null);

            }
        });

        signup_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showBottomSheetDialogFragment(null);
            }
        });


    }

    public void showBottomSheetDialogFragment(String mobile) {
        // showToast(pageIndicatorView.getSelection()+"");
        bottomSheetFragment = SignupSheetFragment.
                newInstance(1, mobile);

        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

    }

    public void showBottomSheetDialogFragment1(String mobile) {
        // showToast(pageIndicatorView.getSelection()+"");


        signInFragment = SignInFragment.
                newInstance(mobile);
        signInFragment.show(getSupportFragmentManager(), signInFragment.getTag());
    }


    private void logEvent()
    {
        Bundle bundle = new Bundle();
        bundle.putString("OnSignIn", DateTimeUtil.convertMillisAsDateTimeString(
                System.currentTimeMillis()));
        mFirebaseAnalytics.logEvent("Screen_OnBoarding", bundle);
    }

}
