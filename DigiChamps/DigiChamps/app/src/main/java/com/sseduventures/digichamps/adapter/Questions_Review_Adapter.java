package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.Model_Exam;
import com.sseduventures.digichamps.Model.Questions_Review;
import com.sseduventures.digichamps.Model.RealmExamDetails;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewExamActivity;

import java.util.ArrayList;
import java.util.List;


public class Questions_Review_Adapter extends RecyclerView.Adapter<Questions_Review_Adapter.MyViewHolder> {

    private Context mContext;
    private List<Questions_Review> albumList;
    private ArrayList<Model_Exam> filelist;
    private String exam_id;
    int result_id;
    ArrayList<RealmExamDetails> results;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, count;
        RelativeLayout relativeLayout;
        CardView card;


        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.ques_no);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.recyc);
            card = (CardView) view.findViewById(R.id.exam_cart);

        }
    }


    public Questions_Review_Adapter(Context mContext, String exam_id,
                                    int result_id, ArrayList<Model_Exam> filelist,
                                    ArrayList<RealmExamDetails> r) {
        this.mContext = mContext;
        this.albumList = albumList;

        this.exam_id = exam_id;
        this.filelist = filelist;
        this.result_id = result_id;
        this.results = r;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.questions_review_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        RealmExamDetails album = results.get(position);
        holder.title.setText((position + 1) + "");
        if (album.getoption() > 0) {

            holder.relativeLayout.
                    setBackgroundResource(R.drawable.diamondshapeattempted);


        } else if (album.getReviewed() == 1) {
            holder.relativeLayout.setBackgroundResource(R.drawable.option_background1);

        } else {
            holder.relativeLayout.setBackgroundResource(R.drawable.option_background2);

        }
        holder.card.setTag(position);
        holder.card.setOnClickListener(onClickListener(position));

    }


    @Override
    public int getItemCount() {
        return results.size();
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, NewExamActivity.class);
                intent.putExtra("position", (Integer) v.getTag());//chap_id filelist
                intent.putExtra("exam_id", exam_id);
                intent.putExtra("result_id", result_id);
                intent.putExtra("activity", "Question Review");
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("Birds", filelist);
                bundle.putParcelableArrayList("Results", results);
                intent.putExtras(bundle);
                mContext.startActivity(intent);
            }
        };
    }
}
