package com.sseduventures.digichamps.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class RegistrationDtl implements Parcelable {



  private Long regdDtlId;

  private String regdNo;
  private Long regdId;

  private Long boardId;

  private Long classId;

  private Long secureId;

  private String answer;

  private String year;

  private Date insertedDate;

  private String insertedBy;

  private String modifiedBy;

  private Date modifiedDate;

  private Boolean isActive;

  private Boolean isDeleted;


  public Long getRegdDtlId() {
    return regdDtlId;
  }

  public void setRegdDtlId(Long regdDtlId) {
    this.regdDtlId = regdDtlId;
  }


  public String getRegdNo() {
    return regdNo;
  }

  public void setRegdNo(String regdNo) {
    this.regdNo = regdNo;
  }


  public Long getRegdId() {
    return regdId;
  }

  public void setRegdId(Long regdId) {
    this.regdId = regdId;
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public Boolean getDeleted() {
    return isDeleted;
  }

  public void setDeleted(Boolean deleted) {
    isDeleted = deleted;
  }

  public Long getBoardId() {
    return boardId;
  }

  public void setBoardId(Long boardId) {
    this.boardId = boardId;
  }


  public Long getClassId() {
    return classId;
  }

  public void setClassId(Long classId) {
    this.classId = classId;
  }


  public Long getSecureId() {
    return secureId;
  }

  public void setSecureId(Long secureId) {
    this.secureId = secureId;
  }


  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }


  public String getYear() {
    return year;
  }

  public void setYear(String year) {
    this.year = year;
  }


  public Date getInsertedDate() {
    return insertedDate;
  }

  public void setInsertedDate(Date insertedDate) {
    this.insertedDate = insertedDate;
  }


  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }


  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }


  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }


  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }

  public RegistrationDtl() {
  }

  public RegistrationDtl(String regdNo, Long regdId, Long boardId, Long classId, Long secureId, String answer, String year, Date insertedDate,
                         String insertedBy, String modifiedBy, Date modifiedDate, Boolean isActive, Boolean isDeleted) {
    this.regdNo = regdNo;
    this.regdId = regdId;
    this.boardId = boardId;
    this.classId = classId;
    this.secureId = secureId;
    this.answer = answer;
    this.year = year;
    this.insertedDate = insertedDate;
    this.insertedBy = insertedBy;
    this.modifiedBy = modifiedBy;
    this.modifiedDate = modifiedDate;
    this.isActive = isActive;
    this.isDeleted = isDeleted;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.regdDtlId);
    dest.writeString(this.regdNo);
    dest.writeValue(this.regdId);
    dest.writeValue(this.boardId);
    dest.writeValue(this.classId);
    dest.writeValue(this.secureId);
    dest.writeString(this.answer);
    dest.writeString(this.year);
    dest.writeLong(this.insertedDate != null ? this.insertedDate.getTime() : -1);
    dest.writeString(this.insertedBy);
    dest.writeString(this.modifiedBy);
    dest.writeLong(this.modifiedDate != null ? this.modifiedDate.getTime() : -1);
    dest.writeValue(this.isActive);
    dest.writeValue(this.isDeleted);
  }

  protected RegistrationDtl(Parcel in) {
    this.regdDtlId = (Long) in.readValue(Long.class.getClassLoader());
    this.regdNo = in.readString();
    this.regdId = (Long) in.readValue(Long.class.getClassLoader());
    this.boardId = (Long) in.readValue(Long.class.getClassLoader());
    this.classId = (Long) in.readValue(Long.class.getClassLoader());
    this.secureId = (Long) in.readValue(Long.class.getClassLoader());
    this.answer = in.readString();
    this.year = in.readString();
    long tmpInsertedDate = in.readLong();
    this.insertedDate = tmpInsertedDate == -1 ? null : new Date(tmpInsertedDate);
    this.insertedBy = in.readString();
    this.modifiedBy = in.readString();
    long tmpModifiedDate = in.readLong();
    this.modifiedDate = tmpModifiedDate == -1 ? null : new Date(tmpModifiedDate);
    this.isActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.isDeleted = (Boolean) in.readValue(Boolean.class.getClassLoader());
  }

  public static final Creator<RegistrationDtl> CREATOR = new Creator<RegistrationDtl>() {
    @Override
    public RegistrationDtl createFromParcel(Parcel source) {
      return new RegistrationDtl(source);
    }

    @Override
    public RegistrationDtl[] newArray(int size) {
      return new RegistrationDtl[size];
    }
  };
}
