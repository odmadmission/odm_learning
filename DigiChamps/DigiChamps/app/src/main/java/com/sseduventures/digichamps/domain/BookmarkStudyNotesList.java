package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkStudyNotesList implements Parcelable{


    private int Module_Id;
    private String ModuleName;
    private String Studynote;
    private String Is_Expire;
    private boolean Is_Free;
    private boolean Is_Avail;

    protected BookmarkStudyNotesList(Parcel in) {
        Module_Id = in.readInt();
        ModuleName = in.readString();
        Studynote = in.readString();
        Is_Expire = in.readString();
        Is_Free = in.readByte() != 0;
        Is_Avail = in.readByte() != 0;
    }

    public static final Creator<BookmarkStudyNotesList> CREATOR = new Creator<BookmarkStudyNotesList>() {
        @Override
        public BookmarkStudyNotesList createFromParcel(Parcel in) {
            return new BookmarkStudyNotesList(in);
        }

        @Override
        public BookmarkStudyNotesList[] newArray(int size) {
            return new BookmarkStudyNotesList[size];
        }
    };

    public int getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(int module_Id) {
        Module_Id = module_Id;
    }

    public String getModuleName() {
        return ModuleName;
    }

    public void setModuleName(String moduleName) {
        ModuleName = moduleName;
    }

    public String getStudynote() {
        return Studynote;
    }

    public void setStudynote(String studynote) {
        Studynote = studynote;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public void setIs_Expire(String is_Expire) {
        Is_Expire = is_Expire;
    }

    public boolean isIs_Free() {
        return Is_Free;
    }

    public void setIs_Free(boolean is_Free) {
        Is_Free = is_Free;
    }

    public boolean isIs_Avail() {
        return Is_Avail;
    }

    public void setIs_Avail(boolean is_Avail) {
        Is_Avail = is_Avail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(Module_Id);
        parcel.writeString(ModuleName);
        parcel.writeString(Studynote);
        parcel.writeString(Is_Expire);
        parcel.writeByte((byte) (Is_Free ? 1 : 0));
        parcel.writeByte((byte) (Is_Avail ? 1 : 0));
    }
}
