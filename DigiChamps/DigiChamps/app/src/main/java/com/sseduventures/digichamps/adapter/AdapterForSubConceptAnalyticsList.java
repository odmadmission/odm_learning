package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.SubChapterList;
import com.sseduventures.digichamps.helper.CircularSeekBar;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.List;



public class AdapterForSubConceptAnalyticsList extends RecyclerView.Adapter<AdapterForSubConceptAnalyticsList.MyViewHolder> {
    View view;
    private boolean isInternetPresent = false;


    private SpotsDialog dialog;


    private Context context;
    private List<SubChapterList> moviesList;


    public AdapterForSubConceptAnalyticsList(List<SubChapterList> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;

    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_adapter_sub_concept_analysis, parent, false);

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyViewHolder holder,  int position) {

        holder.txt_chap_name.setText(moviesList.get(position).getChapter());
        holder.txt_sub_cncpt.setText(moviesList.get(position).getSubConcept());
        holder.prog_text.setText(moviesList.get(position).getAccuracy()+"%");

        holder.sub_circle_progress_view.setProgress(moviesList.get(position).getAccuracy());
        holder.sub_circle_progress_view.setMax(100);
        holder.sub_circle_progress_view.setTag(moviesList.get(position).getAccuracy());
        holder.sub_circle_progress_view.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                holder.sub_circle_progress_view.setProgress((int)holder.sub_circle_progress_view.getTag());
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                holder.sub_circle_progress_view.setProgress((int)holder.sub_circle_progress_view.getTag());
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                holder.sub_circle_progress_view.setProgress((int)holder.sub_circle_progress_view.getTag());
            }


        });


    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final QNChap_Details_Adapter.MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
                if (isInternetPresent) {

                } else {

                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {




        isInternetPresent = AppUtil.isInternetConnected(context);


        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        CardView container;
        TextView txt_chap_name,txt_sub_cncpt,prog_text;
      CircularSeekBar sub_circle_progress_view;

        public MyViewHolder(View view) {
            super(view);
            txt_chap_name = (TextView) itemView.findViewById(R.id.txt_chap_name);
            txt_sub_cncpt = (TextView) itemView.findViewById(R.id.txt_sub_cncpt);
            prog_text = (TextView) itemView.findViewById(R.id.prog_text);
            sub_circle_progress_view = (CircularSeekBar) itemView.findViewById(R.id.sub_circle_progress_view);

        }
    }











}
