package com.sseduventures.digichamps.Model;

/**
 * Created by Swaraj on 6/28/2017.
 */

public class Data_Model_TestList {
    public String text_testName,test_id,chapter_id;

    public Data_Model_TestList(String text_testName , String test_id, String chapter_id){
        this.text_testName = text_testName;
        this.test_id = test_id;
        this.chapter_id = chapter_id;
    }
    public String getText_testName() {
        return text_testName;
    }

    public void setText_testName(String text_testName) {
        this.text_testName = text_testName;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getChapterID() {
        return chapter_id;
    }

    public void setChapterID(String chapterID) {
        this.chapter_id = chapterID;
    }

    public String getExamID() {
        return test_id;
    }

    public void setExamID(String test_id) {
        this.test_id = test_id;
    }


}
