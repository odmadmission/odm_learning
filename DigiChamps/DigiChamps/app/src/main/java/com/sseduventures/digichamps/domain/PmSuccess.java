package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/25/2018.
 */

public class PmSuccess  implements Parcelable {


    private PMActiveTask ActiveTaskList;
    private PMCompletedTask CompletedTaskList;
    private PMOverDueTask OverDueTaskList;
    private PMTeacherDetails TeacherDetails;
    private PMGoalDetails GoalDetails;
    private String studentName;
    private String rating;
    private String studentimage;
    private String Remark;

    public PMActiveTask getActiveTaskList() {
        return ActiveTaskList;
    }

    public void setActiveTaskList(PMActiveTask activeTaskList) {
        ActiveTaskList = activeTaskList;
    }

    public PMCompletedTask getCompletedTaskList() {
        return CompletedTaskList;
    }

    public void setCompletedTaskList(PMCompletedTask completedTaskList) {
        CompletedTaskList = completedTaskList;
    }

    public PMOverDueTask getOverDueTaskList() {
        return OverDueTaskList;
    }

    public void setOverDueTaskList(PMOverDueTask overDueTaskList) {
        OverDueTaskList = overDueTaskList;
    }

    public PMTeacherDetails getTeacherDetails() {
        return TeacherDetails;
    }

    public void setTeacherDetails(PMTeacherDetails teacherDetails) {
        TeacherDetails = teacherDetails;
    }

    public PMGoalDetails getGoalDetails() {
        return GoalDetails;
    }

    public void setGoalDetails(PMGoalDetails goalDetails) {
        GoalDetails = goalDetails;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getStudentimage() {
        return studentimage;
    }

    public void setStudentimage(String studentimage) {
        this.studentimage = studentimage;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.ActiveTaskList, flags);
        dest.writeParcelable((Parcelable) this.CompletedTaskList, flags);
        dest.writeParcelable((Parcelable) this.OverDueTaskList, flags);
        dest.writeParcelable((Parcelable) this.TeacherDetails, flags);
        dest.writeParcelable((Parcelable) this.GoalDetails, flags);
        dest.writeString(this.studentName);
        dest.writeString(this.rating);
        dest.writeString(this.studentimage);
        dest.writeString(this.Remark);
    }

    public PmSuccess() {
    }

    protected PmSuccess(Parcel in) {
        this.ActiveTaskList = in.readParcelable(PMActiveTask.class.getClassLoader());
        this.CompletedTaskList = in.readParcelable(PMCompletedTask.class.getClassLoader());
        this.OverDueTaskList = in.readParcelable(PMOverDueTask.class.getClassLoader());
        this.TeacherDetails = in.readParcelable(PMTeacherDetails.class.getClassLoader());
        this.GoalDetails = in.readParcelable(PMGoalDetails.class.getClassLoader());
        this.studentName = in.readString();
        this.rating = in.readString();
        this.studentimage = in.readString();
        this.Remark = in.readString();
    }

    public static final Creator<PmSuccess> CREATOR = new Creator<PmSuccess>() {
        @Override
        public PmSuccess createFromParcel(Parcel source) {
            return new PmSuccess(source);
        }

        @Override
        public PmSuccess[] newArray(int size) {
            return new PmSuccess[size];
        }
    };
}
