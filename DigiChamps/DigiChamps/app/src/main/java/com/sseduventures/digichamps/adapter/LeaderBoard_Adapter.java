package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eralp.circleprogressview.CircleProgressView;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.DataModel_LeaderBoard;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.LeaderBoardBestTenData;
import com.sseduventures.digichamps.helper.CircularSeekBar;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LeaderBoard_Adapter extends RecyclerView.Adapter<LeaderBoard_Adapter.MyViewHolder> {

    private List<LeaderBoardBestTenData> leardList;
    public static int position;
    //card view
    static CardView container, container1;
    //RelativeLayout
    public static RelativeLayout relative;
    public ArrayList<LeaderBoardBestTenData> show_result_list = new ArrayList<>();

    Context mContext;
    View view;



    private Context context;
    private List<LeaderBoardBestTenData> dataModelActivities;

    public LeaderBoard_Adapter(ArrayList<LeaderBoardBestTenData> data, Context context) {
        this.context = context;
        this.leardList = data;


    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_leader_new_test, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int listPosition) {
        position = listPosition;

        TextView user_name = holder.user_name;
        TextView rank = holder.rank;
        TextView tol_no_ans = holder.tol_no_ans;
        TextView tol_que = holder.tol_que;
        TextView tol_no_wro = holder.tol_no_wro;
        ImageView imageView_profile = holder.imageView_profile;
        TextView prog_text_leader = holder.prog_text_leader;
        ImageView trophy_image = holder.trophy_image;


        if(position>0){
            trophy_image.setVisibility(View.GONE);
        }else{
            trophy_image.setVisibility(View.VISIBLE);

        }


        String chars = capitalize(leardList.get(listPosition).getCustomer_Name());


        user_name.setText(chars);
        rank.setText(leardList.get(listPosition).getRank()+"");
        tol_que.setText(leardList.get(listPosition).getQuestion_Nos()+"");
        tol_no_ans.setText(leardList.get(listPosition).getTotal_Correct_Ans()+"");
        tol_no_wro.setText(leardList.get(listPosition).getIncorrect()+"");


        if(leardList.get(listPosition).getImage() != null
                && !leardList.get(listPosition).getImage().equalsIgnoreCase(
                        "null") ){

            String mImage = leardList.get(listPosition).getImage();
            String mmImage = AppConfig.SRVR_URL+ "Images/Profile/" + mImage;
        Picasso.with(context).
                load(mmImage)
                .error(R.drawable.boy)
                .placeholder(R.drawable.boy)
                .into(imageView_profile);
        }else{
            Picasso.with(context).load(R.drawable.boy).into(imageView_profile);
        }


        final double AccuracyData = leardList.get(listPosition).getAccuracy();



        Double d = new Double(AccuracyData);
        final int AccuracyDataNew = d.intValue();
        prog_text_leader.setText(AccuracyDataNew+"%");
        holder.circle_progress_view.getProgress();
        holder.circle_progress_view.setMax(100);
        holder.circle_progress_view.setPointerHaloColor(R.color.white);
        holder.circle_progress_view.setProgress(AccuracyDataNew);
        holder.circle_progress_view.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                holder.circle_progress_view.setProgress(AccuracyDataNew);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                holder.circle_progress_view.setProgress(AccuracyDataNew);
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                holder.circle_progress_view.setProgress(AccuracyDataNew);
            }


        });


        container1 = holder.container;

        holder.container.setOnClickListener(onClickListener(position));
    }

        @Override
        public int getItemCount() {
            return leardList.size();
        }
    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        };
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView container;
        TextView user_name, rank, time, tol_que, tol_ans,
                tol_wro, tol_no_que, tol_no_ans, tol_no_wro,prog_text_leader;

        ImageView  imageView_profile, clock,trophy_image;
        CircularSeekBar circle_progress_view;


        public MyViewHolder(View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.card_view_leader);
            this.user_name = (TextView) itemView.findViewById(R.id.name_text);
            this.rank = (TextView) itemView.findViewById(R.id.rank);
            this.tol_que = (TextView) itemView.findViewById(R.id.total_que);
            this.tol_ans = (TextView) itemView.findViewById(R.id.correct_que);
            this.tol_no_ans = (TextView) itemView.findViewById(R.id.number_ans);
            this.tol_wro = (TextView) itemView.findViewById(R.id.wrong_que);
            this.tol_no_wro = (TextView) itemView.findViewById(R.id.wrong_ans);
            this.trophy_image = (ImageView) itemView.findViewById(R.id.trophy_image);
            this.circle_progress_view = (CircularSeekBar)itemView.findViewById(R.id.circle_progress_view);
            this.prog_text_leader = (TextView) itemView.findViewById(R.id.prog_text_leader);
            imageView_profile = (ImageView) itemView.findViewById(R.id.leader_header);
            clock = (ImageView) itemView.findViewById(R.id.watch_icon);
        }
    }

    private String capitalize(String capString){
        StringBuffer capBuffer = new StringBuffer();
        Matcher capMatcher = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString);
        while (capMatcher.find()){
            capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase());
        }

        return capMatcher.appendTail(capBuffer).toString();
    }
}
