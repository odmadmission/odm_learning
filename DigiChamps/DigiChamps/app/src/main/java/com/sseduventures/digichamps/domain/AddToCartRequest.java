package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class AddToCartRequest implements Parcelable
{
    private CartData chkpackaged;

    public CartData getChkpackaged() {
        return chkpackaged;
    }

    public void setChkpackaged(CartData chkpackaged) {
        this.chkpackaged = chkpackaged;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.chkpackaged, flags);
    }

    public AddToCartRequest() {
    }

    protected AddToCartRequest(Parcel in) {
        this.chkpackaged = in.readParcelable(CartData.class.getClassLoader());
    }

    public static final Creator<AddToCartRequest> CREATOR = new Creator<AddToCartRequest>() {
        @Override
        public AddToCartRequest createFromParcel(Parcel source) {
            return new AddToCartRequest(source);
        }

        @Override
        public AddToCartRequest[] newArray(int size) {
            return new AddToCartRequest[size];
        }
    };
}

