package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class CartPackage implements Parcelable
{

    private String Package_Name;
    private int Validity;
    private int Subscripttion_Limit;
    private int Package_ID;
    private int Price;
    private int Discounted_Price;
    private int Package_Price;
    private String packageimage;

    private boolean Is_Offline;

    private int Cart_ID;
    private int discountpercentage;
    private int Discount;

    public String getPackage_Name() {
        return Package_Name;
    }

    public void setPackage_Name(String package_Name) {
        Package_Name = package_Name;
    }

    public int getValidity() {
        return Validity;
    }

    public void setValidity(int validity) {
        Validity = validity;
    }

    public int getSubscripttion_Limit() {
        return Subscripttion_Limit;
    }

    public void setSubscripttion_Limit(int subscripttion_Limit) {
        Subscripttion_Limit = subscripttion_Limit;
    }

    public int getPackage_ID() {
        return Package_ID;
    }

    public void setPackage_ID(int package_ID) {
        Package_ID = package_ID;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getDiscounted_Price() {
        return Discounted_Price;
    }

    public void setDiscounted_Price(int discounted_Price) {
        Discounted_Price = discounted_Price;
    }

    public int getPackage_Price() {
        return Package_Price;
    }

    public void setPackage_Price(int package_Price) {
        Package_Price = package_Price;
    }

    public String getPackageimage() {
        return packageimage;
    }

    public void setPackageimage(String packageimage) {
        this.packageimage = packageimage;
    }

    public boolean isIs_Offline() {
        return Is_Offline;
    }

    public void setIs_Offline(boolean is_Offline) {
        Is_Offline = is_Offline;
    }

    public int getCart_ID() {
        return Cart_ID;
    }

    public void setCart_ID(int cart_ID) {
        Cart_ID = cart_ID;
    }

    public int getDiscountpercentage() {
        return discountpercentage;
    }

    public void setDiscountpercentage(int discountpercentage) {
        this.discountpercentage = discountpercentage;
    }

    public int getDiscount() {
        return Discount;
    }

    public void setDiscount(int discount) {
        Discount = discount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Package_Name);
        dest.writeInt(this.Validity);
        dest.writeInt(this.Subscripttion_Limit);
        dest.writeInt(this.Package_ID);
        dest.writeInt(this.Price);
        dest.writeInt(this.Discounted_Price);
        dest.writeInt(this.Package_Price);
        dest.writeString(this.packageimage);
        dest.writeByte(this.Is_Offline ? (byte) 1 : (byte) 0);
        dest.writeInt(this.Cart_ID);
        dest.writeInt(this.discountpercentage);
        dest.writeInt(this.Discount);
    }

    public CartPackage() {
    }



    protected CartPackage(Parcel in) {
        this.Package_Name = in.readString();
        this.Validity = in.readInt();
        this.Subscripttion_Limit = in.readInt();
        this.Package_ID = in.readInt();
        this.Price = in.readInt();
        this.Discounted_Price = in.readInt();
        this.Package_Price = in.readInt();
        this.packageimage = in.readString();
        this.Is_Offline = in.readByte() != 0;
        this.Cart_ID = in.readInt();
        this.discountpercentage = in.readInt();
        this.Discount = in.readInt();
    }

    public static final Creator<CartPackage> CREATOR = new Creator<CartPackage>() {
        @Override
        public CartPackage createFromParcel(Parcel source) {
            return new CartPackage(source);
        }

        @Override
        public CartPackage[] newArray(int size) {
            return new CartPackage[size];
        }
    };
}
