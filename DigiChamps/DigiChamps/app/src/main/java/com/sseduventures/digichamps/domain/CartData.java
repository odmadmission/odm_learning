package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class CartData implements Parcelable
{
    public List<CartChapterId> chkpackage ;
    public int id;
    public String lmt;
    public String pkid;
    public String prc;

    public List<CartChapterId> getChkpackage() {
        return chkpackage;
    }

    public void setChkpackage(List<CartChapterId> chkpackage) {
        this.chkpackage = chkpackage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLmt() {
        return lmt;
    }

    public void setLmt(String lmt) {
        this.lmt = lmt;
    }

    public String getPkid() {
        return pkid;
    }

    public void setPkid(String pkid) {
        this.pkid = pkid;
    }

    public String getPrc() {
        return prc;
    }

    public void setPrc(String prc) {
        this.prc = prc;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.chkpackage);
        dest.writeInt(this.id);
        dest.writeString(this.lmt);
        dest.writeString(this.pkid);
        dest.writeString(this.prc);
    }

    public CartData() {
    }

    protected CartData(Parcel in) {
        this.chkpackage = in.createTypedArrayList(CartChapterId.CREATOR);
        this.id = in.readInt();
        this.lmt = in.readString();
        this.pkid = in.readString();
        this.prc = in.readString();
    }

    public static final Creator<CartData> CREATOR = new Creator<CartData>() {
        @Override
        public CartData createFromParcel(Parcel source) {
            return new CartData(source);
        }

        @Override
        public CartData[] newArray(int size) {
            return new CartData[size];
        }
    };
}
