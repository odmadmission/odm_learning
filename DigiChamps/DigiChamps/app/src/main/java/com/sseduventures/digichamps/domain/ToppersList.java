package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class ToppersList implements Parcelable{

   private String createdDate;
   private String fileType;
   private String fileURL;
   private String className;
   private String sectionName;
   private String title;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.createdDate);
        dest.writeString(this.fileType);
        dest.writeString(this.fileURL);
        dest.writeString(this.className);
        dest.writeString(this.sectionName);
        dest.writeString(this.title);
    }

    public ToppersList() {
    }

    protected ToppersList(Parcel in) {
        this.createdDate = in.readString();
        this.fileType = in.readString();
        this.fileURL = in.readString();
        this.className = in.readString();
        this.sectionName = in.readString();
        this.title = in.readString();
    }

    public static final Creator<ToppersList> CREATOR = new Creator<ToppersList>() {
        @Override
        public ToppersList createFromParcel(Parcel source) {
            return new ToppersList(source);
        }

        @Override
        public ToppersList[] newArray(int size) {
            return new ToppersList[size];
        }
    };
}
