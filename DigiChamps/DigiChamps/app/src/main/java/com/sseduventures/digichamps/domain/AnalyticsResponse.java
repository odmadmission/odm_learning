package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/10/2018.
 */

public class AnalyticsResponse implements Parcelable{

    private AnalyticsSuccess success;

    public AnalyticsSuccess getSuccess() {
        return success;
    }

    public void setSuccess(AnalyticsSuccess success) {
        this.success = success;
    }







    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.success, flags);
    }

    public AnalyticsResponse() {
    }

    protected AnalyticsResponse(Parcel in) {
        this.success = in.readParcelable(AnalyticsSuccess.class.getClassLoader());
    }

    public static final Creator<AnalyticsResponse> CREATOR =
            new Creator<AnalyticsResponse>() {

        @Override
        public AnalyticsResponse createFromParcel(Parcel source) {
            return new AnalyticsResponse(source);
        }

        @Override
        public AnalyticsResponse[] newArray(int size) {
            return new AnalyticsResponse[size];
        }
    };
}
