package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/24/2018.
 */

public class DashboardResponse implements Parcelable {

    private DashboardSuccess success;


    public DashboardSuccess getSuccess() {
        return success;
    }

    public void setSuccess(DashboardSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public DashboardResponse() {
    }

    protected DashboardResponse(Parcel in) {
        this.success = in.readParcelable(DashboardSuccess.class.getClassLoader());
    }

    public static final Creator<DashboardResponse> CREATOR = new Creator<DashboardResponse>() {
        @Override
        public DashboardResponse createFromParcel(Parcel source) {
            return new DashboardResponse(source);
        }

        @Override
        public DashboardResponse[] newArray(int size) {
            return new DashboardResponse[size];
        }
    };
}
