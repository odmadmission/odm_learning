package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/17/2018.
 */

public class SignupRequest implements Parcelable {

    	private String customerName;
        private String mobile;
        private String email;
        private String deviceId;
        private String password;
        private RegisDetails registrationDtl;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RegisDetails getRegistrationDtl() {
        return registrationDtl;
    }

    public void setRegistrationDtl(RegisDetails registrationDtl) {
        this.registrationDtl = registrationDtl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.customerName);
        dest.writeString(this.mobile);
        dest.writeString(this.email);
        dest.writeString(this.deviceId);
        dest.writeString(this.password);
        dest.writeParcelable((Parcelable) this.registrationDtl, flags);
    }

    public SignupRequest() {
    }

    protected SignupRequest(Parcel in) {
        this.customerName = in.readString();
        this.mobile = in.readString();
        this.email = in.readString();
        this.deviceId = in.readString();
        this.password = in.readString();
        this.registrationDtl = in.readParcelable(RegisDetails.class.getClassLoader());
    }

    public static final Creator<SignupRequest> CREATOR = new Creator<SignupRequest>() {
        @Override
        public SignupRequest createFromParcel(Parcel source) {
            return new SignupRequest(source);
        }

        @Override
        public SignupRequest[] newArray(int size) {
            return new SignupRequest[size];
        }
    };
}
