package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class LearnChapterlist implements Parcelable {

    private int chapterid;
    private String Chapter;
    private String ChapterImage;
    private int total_pdfs;
    private int total_videos;
    private int total_question_pdf;
    private int online_test;
    private int Pre_req_test;
    private int total_study_notes;
    private int total_sbts;
    private double total_progress;


    protected LearnChapterlist(Parcel in) {
        chapterid = in.readInt();
        Chapter = in.readString();
        ChapterImage = in.readString();
        total_pdfs = in.readInt();
        total_videos = in.readInt();
        total_question_pdf = in.readInt();
        online_test = in.readInt();
        Pre_req_test = in.readInt();
        total_study_notes = in.readInt();
        total_sbts = in.readInt();
        total_progress = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(chapterid);
        dest.writeString(Chapter);
        dest.writeString(ChapterImage);
        dest.writeInt(total_pdfs);
        dest.writeInt(total_videos);
        dest.writeInt(total_question_pdf);
        dest.writeInt(online_test);
        dest.writeInt(Pre_req_test);
        dest.writeInt(total_study_notes);
        dest.writeInt(total_sbts);
        dest.writeDouble(total_progress);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LearnChapterlist> CREATOR = new Creator<LearnChapterlist>() {
        @Override
        public LearnChapterlist createFromParcel(Parcel in) {
            return new LearnChapterlist(in);
        }

        @Override
        public LearnChapterlist[] newArray(int size) {
            return new LearnChapterlist[size];
        }
    };

    public int getChapterid() {
        return chapterid;
    }

    public void setChapterid(int chapterid) {
        this.chapterid = chapterid;
    }

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String chapter) {
        Chapter = chapter;
    }

    public String getChapterImage() {
        return ChapterImage;
    }

    public void setChapterImage(String chapterImage) {
        ChapterImage = chapterImage;
    }

    public int getTotal_pdfs() {
        return total_pdfs;
    }

    public void setTotal_pdfs(int total_pdfs) {
        this.total_pdfs = total_pdfs;
    }

    public int getTotal_videos() {
        return total_videos;
    }

    public void setTotal_videos(int total_videos) {
        this.total_videos = total_videos;
    }

    public int getTotal_question_pdf() {
        return total_question_pdf;
    }

    public void setTotal_question_pdf(int total_question_pdf) {
        this.total_question_pdf = total_question_pdf;
    }

    public int getOnline_test() {
        return online_test;
    }

    public void setOnline_test(int online_test) {
        this.online_test = online_test;
    }

    public int getPre_req_test() {
        return Pre_req_test;
    }

    public void setPre_req_test(int pre_req_test) {
        Pre_req_test = pre_req_test;
    }

    public int getTotal_study_notes() {
        return total_study_notes;
    }

    public void setTotal_study_notes(int total_study_notes) {
        this.total_study_notes = total_study_notes;
    }

    public int getTotal_sbts() {
        return total_sbts;
    }

    public void setTotal_sbts(int total_sbts) {
        this.total_sbts = total_sbts;
    }

    public double getTotal_progress() {
        return total_progress;
    }

    public void setTotal_progress(double total_progress) {
        this.total_progress = total_progress;
    }
}
