package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighlightsResponse implements Parcelable{

    private TestHighlightsSuccess success;

    public TestHighlightsSuccess getSuccess() {
        return success;
    }

    public void setSuccess(TestHighlightsSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public TestHighlightsResponse() {
    }

    protected TestHighlightsResponse(Parcel in) {
        this.success = in.readParcelable(TestHighlightsSuccess.class.getClassLoader());
    }

    public static final Creator<TestHighlightsResponse> CREATOR = new Creator<TestHighlightsResponse>() {
        @Override
        public TestHighlightsResponse createFromParcel(Parcel source) {
            return new TestHighlightsResponse(source);
        }

        @Override
        public TestHighlightsResponse[] newArray(int size) {
            return new TestHighlightsResponse[size];
        }
    };
}
