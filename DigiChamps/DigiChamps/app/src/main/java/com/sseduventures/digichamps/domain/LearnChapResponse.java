package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class LearnChapResponse implements Parcelable
{
  private LearnChapSuccess success;

    public LearnChapSuccess getSuccess() {
        return success;
    }

    public void setSuccess(LearnChapSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public LearnChapResponse() {
    }

    protected LearnChapResponse(Parcel in) {
        this.success = in.readParcelable(LearnChapSuccess.class.getClassLoader());
    }

    public static final Creator<LearnChapResponse> CREATOR = new Creator<LearnChapResponse>() {
        @Override
        public LearnChapResponse createFromParcel(Parcel source) {
            return new LearnChapResponse(source);
        }

        @Override
        public LearnChapResponse[] newArray(int size) {
            return new LearnChapResponse[size];
        }
    };
}
