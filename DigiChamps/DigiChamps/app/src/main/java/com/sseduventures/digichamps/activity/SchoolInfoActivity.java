package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.Preferences;

import de.hdodenhof.circleimageview.CircleImageView;

public class SchoolInfoActivity extends FormActivity {

    CircleImageView iv_logo;
    TextView tv_school_name,tv_description;
    Context mContext;
    CoordinatorLayout coordinatorLayout ;
    ImageView iv_video;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);
        setModuleName(LogEventUtil.EVENT_school_info);
        logEvent(LogEventUtil.KEY_school_info,LogEventUtil.EVENT_school_info);


        mContext = this;
        coordinatorLayout = findViewById(R.id.coordinatorLayout);
        iv_logo = findViewById(R.id.iv_logo);
        tv_school_name = findViewById(R.id.tv_school_name);
        tv_description =  findViewById(R.id.tv_description);
        ImageView iv_menu = findViewById(R.id.iv_menu);
        iv_video = findViewById(R.id.iv_video);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView tv_at =  findViewById(R.id.tv_school_name);
        TextView tv_aboutSchool =  findViewById(R.id.tv_aboutSchool);
        TextView tv_schoolVideo =  findViewById(R.id.tv_schoolVideo);
        FontManage.setFontImpact(mContext,tv_at);
        FontManage.setFontImpact(mContext,tv_aboutSchool);
        FontManage.setFontImpact(mContext,tv_schoolVideo);
        FontManage.setFontMeiryo(mContext,tv_description);

        if(!Preferences.get(mContext,Preferences.KEY_SCHOOL_NAME).equals("")){
                tv_school_name.setText(Preferences.get(mContext,Preferences.KEY_SCHOOL_NAME));
                tv_description.setText(Preferences.get(mContext,Preferences.KEY_SCHOOL_INFO));
        }

        if(!Preferences.get(mContext,Preferences.KEY_SCHOOL_LOGO).equals("")){
                Picasso.with(mContext).load(Constants.IMAGE_URL+Preferences.get(mContext,Preferences.KEY_SCHOOL_LOGO))
                        .placeholder(R.drawable.tooper_answer)
                        .into(iv_logo);
        }


        iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Preferences.get(mContext,Preferences.KEY_SCHOOL_VIDEO).equals("")){
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.IMAGE_URL+Preferences.get(mContext,Preferences.KEY_SCHOOL_VIDEO)));
                    startActivity(browserIntent);
                }else{
                    Snackbar snackbar = Snackbar
                            .make(coordinatorLayout, "Sorry!", Snackbar.LENGTH_LONG)
                            .setAction("No Documentory Video Found", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(Color.YELLOW);
                    snackbar.show();
                }
            }
        });
    }

}
