package com.sseduventures.digichamps.frags;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Feed_VerticalViewPager;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Feed_VerticlePagerAdapter;
import com.sseduventures.digichamps.domain.FeedListData;
import com.sseduventures.digichamps.domain.FeedResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;



public class FeedFragment extends BaseFragment implements ServiceReceiver.Receiver {

    private List<FeedListData> mFeedList;
    private Feed_VerticlePagerAdapter lAdapter;
    Feed_VerticalViewPager verticalViewPager;
    ImageView feed_backbtn;
    private RelativeLayout topLevelLayout;
    private SharedPreferences preferences;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private FeedResponse success;
    private static final String TAG = FeedFragment.class.getSimpleName();
    private View mView;
    private ShimmerFrameLayout mShimmerViewContainer;

    public static FeedFragment getInstance() {
        FeedFragment fragment = new FeedFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NewDashboardActivity) getActivity())
                .logEvent(LogEventUtil.KEY_Feed_PAGE,
                        LogEventUtil.EVENT_Feed_PAGE);
        ((NewDashboardActivity) getActivity())
                .setModuleName(LogEventUtil.EVENT_Feed_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_feed,
                container, false);

        ((NewDashboardActivity) getActivity()).updateStatusBarColor("#FFBC7625");


        init(mView);
        topLevelLayout = (RelativeLayout) mView.findViewById(R.id.top_layout);
        initSwipePager();
        topLevelLayout.setVisibility(View.GONE);



        feed_backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getBoolean("RanBefore", false)) {
                    ReturnHome(view);
                } else {
                }
            }
        });
        return mView;
    }

    public void init(View view) {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mFeedList = new ArrayList<>();
        lAdapter = new Feed_VerticlePagerAdapter(mFeedList, getActivity());
        feed_backbtn = (ImageView) view.findViewById(R.id.cart_back_image);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);


    }

    public void ReturnHome(View view) {
        Intent in = new Intent(getActivity(), NewDashboardActivity.class);
        startActivity(in);
    }


    private void initSwipePager() {
        verticalViewPager = (Feed_VerticalViewPager) mView.findViewById(R.id.vPager);
        verticalViewPager.setAdapter(lAdapter);
    }


    private void getFeedNew() {

        if (AppUtil.isInternetConnected(getActivity())) {

            NetworkService.startActionGetFeedData(getActivity(), mServiceReceiver);
        } else {
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        getFeedNew();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getList() != null
                        && success.getSuccess().getList().size() > 0) {

                    mFeedList.clear();
                    mFeedList.addAll(success.getSuccess().getList());
                    lAdapter = new Feed_VerticlePagerAdapter(mFeedList, getActivity());
                    verticalViewPager.setAdapter(lAdapter);

                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


}
