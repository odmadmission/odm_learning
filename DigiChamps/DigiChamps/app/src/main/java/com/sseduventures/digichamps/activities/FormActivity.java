package com.sseduventures.digichamps.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.database.DbContract;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.utils.DateTimeUtil;
import com.sseduventures.digichamps.utils.IntentHelper;

public class FormActivity extends AppCompatActivity {


    public ProgressDialog dialog;
    protected Handler mHandler;
    protected ServiceReceiver mServiceReceiver;
    private FirebaseAnalytics mFirebaseAnalytics;


    private static final String TAG=FormActivity.class.getSimpleName();
    protected long rowId=0;
    protected String moduleName;


    private IntentFilter intentFilter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

    }

    public void setModuleName(String moduleName)
    {
        this.moduleName=moduleName;
    }
    protected void setUpReciever()
    {
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
    }
    private BroadcastReceiver r=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent!=null&&intent.getAction().equals(
                    IntentHelper.ACTION_LOGOUT_NOTIFI
            ))
            {
                finishAffinity();
                Intent in = new Intent(FormActivity.this,OnBoardActivity.class);
                startActivity(in);
            }
        }
    };

    @Override
    protected void onStart() {

        super.onStart();


        if(RegPrefManager.getInstance(this).getStatus()==1) {
            ActivityLogSaveTask task = new ActivityLogSaveTask();
            task.execute(moduleName, 1 + "", 0 + "");

            intentFilter = new IntentFilter();
            intentFilter.addAction(IntentHelper.ACTION_LOGOUT_NOTIFI);
            LocalBroadcastManager.getInstance(this)
                    .registerReceiver(r, intentFilter);
        }

    }
    public void hideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);


    }
    @Override
    protected void onStop()
    {
        super.onStop();
        hideDialog();
        if(RegPrefManager.getInstance(this).getStatus()==1) {
            ActivityLogUpdateTask task = new ActivityLogUpdateTask();
            task.execute(rowId + "", 2 + "", 0 + "");

            LocalBroadcastManager.getInstance(this)
                    .unregisterReceiver(r);
        }

    }

    public void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(FormActivity.this,
                        message,
                        Toast.LENGTH_SHORT).show();
            }
        });


}
    public void showDialog(String title,String message)
    {
        if(dialog==null){
            dialog=ProgressDialog.show(this,title,message);
        }
    }

    public void hideDialog()
    {
          if(dialog!=null)
          {
              dialog.dismiss();
              dialog=null;
          }
    }


    public void logEvent(String key,String event)
    {
        Bundle bundle = new Bundle();
        bundle.putString(key, DateTimeUtil.convertMillisAsDateTimeString(
                System.currentTimeMillis()));
       // bundle.putString(FirebaseAnalytics.Param.VALUE,RegPrefManager.getInstance(this).getRegId()+"");
        mFirebaseAnalytics.logEvent(event, bundle);
    }


    public class ActivityLogSaveTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {

            long time=System.currentTimeMillis();
            ContentValues mContentValues=new ContentValues();
            mContentValues.put(DbContract.
                    ActivityTimeTable.MODULEID,strings[0]);

            mContentValues.put(DbContract.ActivityTimeTable.REGDID,
                    RegPrefManager.getInstance(FormActivity.this).getRegId());

            mContentValues.put(DbContract.ActivityTimeTable.SCHOOL_CODE,
                    RegPrefManager.getInstance(FormActivity.this).
                            getSchoolId());
            mContentValues.put(DbContract.ActivityTimeTable.SECTION_CODE,
                    RegPrefManager.getInstance(FormActivity.this).
                            getSectionId());
            mContentValues.put(DbContract.ActivityTimeTable.CLASS_CODE,
                    RegPrefManager.getInstance(FormActivity.this).
                            getclassid());

            mContentValues.put(DbContract.ActivityTimeTable.STATUS,
                    Integer.parseInt(strings[1]));

            mContentValues.put(DbContract.ActivityTimeTable.START_TIME,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.END_TIME,
                    time);


            mContentValues.put(DbContract.ActivityTimeTable.SYNC_DATE,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.CREATE_DATE,
                    time);

            mContentValues.put(DbContract.ActivityTimeTable.SEEN_TIME,
                    Integer.parseInt(strings[2]));



            Uri uri= getContentResolver().
                    insert(DbContract.ActivityTimeTable.CONTENT_URI,
                            mContentValues);

            if(uri!=null)
            {
                rowId=Integer.parseInt(
                        uri.getLastPathSegment());

               // Log.v(TAG,uri.getLastPathSegment());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
    public class ActivityLogUpdateTask extends AsyncTask<String,Void,Long>
    {


        @Override
        protected Long doInBackground(String... strings)
        {
            ContentValues mContentValues=new ContentValues();

            mContentValues.put(DbContract.ActivityTimeTable.STATUS,
                    Integer.parseInt(strings[1]));

            mContentValues.put(DbContract.ActivityTimeTable.SEEN_TIME,
                    Integer.parseInt(strings[2]));

            mContentValues.put(DbContract.ActivityTimeTable.END_TIME,
                    System.currentTimeMillis());

            long update= getContentResolver().
                    update(DbContract.ActivityTimeTable.CONTENT_URI,
                            mContentValues,
                            DbContract.ActivityTimeTable._ID+" LIKE ? ",
                            new String[]{strings[0]});



           // Log.v(TAG,update+"");


            if(update==1)
            {
                rowId=0;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long aLong) {
            super.onPostExecute(aLong);
        }
    }
}
