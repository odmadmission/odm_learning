package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/9/2018.
 */

public class PsychometricPostData implements Parcelable
{
    private PsychometricPostResponse success;

    public PsychometricPostResponse getSuccess() {
        return success;
    }

    public void setSuccess(PsychometricPostResponse success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public PsychometricPostData() {
    }

    protected PsychometricPostData(Parcel in) {
        this.success = in.readParcelable(PsychometricPostResponse.class.getClassLoader());
    }

    public static final Creator<PsychometricPostData> CREATOR = new Creator<PsychometricPostData>() {
        @Override
        public PsychometricPostData createFromParcel(Parcel source) {
            return new PsychometricPostData(source);
        }

        @Override
        public PsychometricPostData[] newArray(int size) {
            return new PsychometricPostData[size];
        }
    };
}
