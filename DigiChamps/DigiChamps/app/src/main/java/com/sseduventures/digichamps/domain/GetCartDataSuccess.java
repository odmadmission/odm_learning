package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class GetCartDataSuccess implements Parcelable
{
    private CartPackageWrapper getcartdata;

    public CartPackageWrapper getGetcartdata() {
        return getcartdata;
    }

    public void setGetcartdata(CartPackageWrapper getcartdata) {
        this.getcartdata = getcartdata;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.getcartdata, flags);
    }

    public GetCartDataSuccess() {
    }

    protected GetCartDataSuccess(Parcel in) {
        this.getcartdata = in.readParcelable(CartPackageWrapper.class.getClassLoader());
    }

    public static final Creator<GetCartDataSuccess> CREATOR = new Creator<GetCartDataSuccess>() {
        @Override
        public GetCartDataSuccess createFromParcel(Parcel source) {
            return new GetCartDataSuccess(source);
        }

        @Override
        public GetCartDataSuccess[] newArray(int size) {
            return new GetCartDataSuccess[size];
        }
    };
}
