package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class RemoveCartPackageList implements Parcelable
{
    private ArrayList<CartPackageId> removecart;

    public ArrayList<CartPackageId> getRemovecart() {
        return removecart;
    }

    public void setRemovecart(ArrayList<CartPackageId> removecart) {
        this.removecart = removecart;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.removecart);
    }

    public RemoveCartPackageList() {
    }

    protected RemoveCartPackageList(Parcel in) {
        this.removecart = in.createTypedArrayList(CartPackageId.CREATOR);
    }

    public static final Creator<RemoveCartPackageList> CREATOR = new Creator<RemoveCartPackageList>() {
        @Override
        public RemoveCartPackageList createFromParcel(Parcel source) {
            return new RemoveCartPackageList(source);
        }

        @Override
        public RemoveCartPackageList[] newArray(int size) {
            return new RemoveCartPackageList[size];
        }
    };
}
