
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClassListModel {

    @SerializedName("ClassID")
    @Expose
    private Integer classID;
    @SerializedName("ClassName")
    @Expose
    private String className;
    @SerializedName("boardName")
    @Expose
    private String boardName;
    @SerializedName("boardID")
    @Expose
    private String boardID;

    public Integer getClassID() {
        return classID;
    }

    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }


    public String getBoardID() {
        return boardID;
    }

    public void setBoardID(String boardID) {
        this.boardID = boardID;
    }
}
