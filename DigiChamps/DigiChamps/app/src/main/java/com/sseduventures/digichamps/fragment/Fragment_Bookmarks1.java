package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.DoItYourselfActivity;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.*;
import com.sseduventures.digichamps.adapter.Bookmarks_frag1_adapter;
import com.sseduventures.digichamps.adapter.DiyDetails_Adapter;
import com.sseduventures.digichamps.domain.BookMarkVideoList;
import com.sseduventures.digichamps.domain.BookMarkVideoResponse;
import com.sseduventures.digichamps.domain.SetBookmarkResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;

/**
 * Created by Tech_1 on 12/30/2017.
 */

public class Fragment_Bookmarks1 extends android.support.v4.app.Fragment
        implements ServiceReceiver.Receiver {
    private RecyclerView recyclerView;
    ArrayList<BookMarkVideoList> list;
    private Bookmarks_frag1_adapter adapter;
    private String Module_Id,TAG = "Videos";
    SpotsDialog pDialog;

    private long User_Id;
    private TextView no_video_bookmark;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private BookMarkVideoResponse success;

    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bookmarks_frag1, container, false);
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_bookmark_video);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_bookmark_video,
                    LogEventUtil.EVENT_bookmark_video);
        }
        init(view);
        //initVisibility();

        //checking for internet connection
        if (AppUtil.isInternetConnected(getActivity())) {

            User_Id = RegPrefManager.getInstance(getActivity()).getRegId();
            //bookmarks_video();
           // getBookmarkVideoDetails();
        } else {
            // redirect to No internet activity
            Intent i = new Intent(getActivity(), Internet_Activity.class);
            startActivity(i);
            getActivity().overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
        }
        return view;

    }
    private void init(View view) {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);

        recyclerView = (RecyclerView) view.findViewById(R.id.video_recycler_view);
        no_video_bookmark = view.findViewById(R.id.no_video_bookmark);
        no_video_bookmark.setVisibility(View.GONE);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        //list
        list = new ArrayList<>();

        //Progress dialog initialisation
        pDialog = new SpotsDialog(getActivity(), "Fun+Education", R.style.Custom);
        adapter = new Bookmarks_frag1_adapter(list,getActivity(),Fragment_Bookmarks1.this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);


    }




    public void refreshAdapter()
    {
        adapter.moviesList = list;
        //  adapter.dataSet=list;
        adapter.notifyDataSetChanged();
    }

    //method for showing progress dialog
    private void showDialog() {
        pDialog.showDialog();
    }

    //method for hiding progress dialog
    private void hideDialog() {
        pDialog.hideDialog();
    }

    public void initVisibility(){
        recyclerView.setVisibility(View.GONE);
        no_video_bookmark.setVisibility(View.VISIBLE);
    }

    public void setUnsetBookMark(String dataid,String dataType){
        if (AppUtil.isInternetConnected(getActivity())){

            Bundle bun = new Bundle();
            bun.putString("dataType",dataType);
            bun.putString("dataId",dataid);

            NetworkService.startActionGetSetUsetBookmark(getActivity(),mServiceReceiver,bun);
        }else{
            Intent in = new Intent(getActivity(), Internet_Activity.class);
            startActivity(in);
        }
    }

    private void getBookmarkVideoDetails(){
        if(AppUtil.isInternetConnected(getActivity())){

            NetworkService.startActionGetBookmarkVideo(getActivity(),
                    mServiceReceiver);

        }else{
            Intent in = new Intent(getActivity(),Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch(resultCode){

            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                recyclerView.setVisibility(View.VISIBLE);
                no_video_bookmark.setVisibility(View.GONE);



                if(success!=null && success.getSuccessBookmarkedVideo().
                        getVideoList()!=null &&
                        success.getSuccessBookmarkedVideo().getVideoList().size()>0){



                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                            list.clear();
                            list.addAll(success.getSuccessBookmarkedVideo().getVideoList());
                            adapter.notifyDataSetChanged();

                        }
                    },1000);

                }


                break;

            case ResponseCodes.BOOKMARKED:
                SetBookmarkResponse msuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                Toast.makeText(getActivity(), ""+msuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();
                break;
            case ResponseCodes.UNBOOKMARKED:
                SetBookmarkResponse mmsuccess =
                        resultData.getParcelable(IntentHelper.RESULT_DATA);

                ArrayList<BookMarkVideoList> l=new ArrayList<>();
                for(int i=0;i<list.size();i++)
                {
                    String id=resultData.getString("dataId");

                    if(id.equals(list.get(i).getModule_Id()))
                    {

                    }
                    else {
                        l.add(list.get(i));
                    }
                }
                Toast.makeText(getActivity(), ""+mmsuccess.getSuccess().getMessage(), Toast.LENGTH_SHORT).show();

                list.clear();
                list.addAll(l);
                adapter.notifyDataSetChanged();

                if(list.size()==0)
                {
                    recyclerView.setVisibility(View.GONE);
                    no_video_bookmark.setVisibility(View.VISIBLE);
                }
                break;

            case ResponseCodes.NOLIST:
                recyclerView.setVisibility(View.GONE);
                no_video_bookmark.setVisibility(View.VISIBLE);

                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;

            case ResponseCodes.SERVER_ERROR:
                break;


        }



    }

    @Override
    public void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onResume() {
        getBookmarkVideoDetails();
        super.onResume();
    }
}

