package com.sseduventures.digichamps.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.LeaderBoard_Adapter;
import com.sseduventures.digichamps.adapter.RecyclerTouchListener;
import com.sseduventures.digichamps.domain.LeaderBoardBestTenData;
import com.sseduventures.digichamps.domain.LeaderBoardResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


import java.util.ArrayList;

import java.util.concurrent.TimeUnit;


/**
 * Created by ARR on 6/21/2017.
 */

public class Activity_LeaderBoard extends FormActivity implements ServiceReceiver.Receiver {


    private Toolbar toolbar_leader;
    private CollapsingToolbarLayout collaps;
    private AppBarLayout appBarLayout;
    private RecyclerView recyclerView;
    private LeaderBoard_Adapter mAdapter;

    private ArrayList<LeaderBoardBestTenData> mLeaderList;
    private String chapIdid;
    private String module_typetype;
    private String subJectId;
    private String subNameName;
    private String flagNameName;
    private SpotsDialog dialog;
    String examid, resultId;
    private CardView myresultCard;
    private TextView myName, myRank, myTotTime, myTotalQstn, myTotCorrect, myTotWrong;
    private ImageView myProfile;
    private CoordinatorLayout leaderBoaerd_Layout;
    String user_id;
    private String navigation;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private LeaderBoardResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_leaderboard);
        logEvent(LogEventUtil.KEY_LeaderBoardOfTests_PAGE,
                LogEventUtil.EVENT_LeaderboardOfTests_PAGE);
        setModuleName(LogEventUtil.EVENT_LeaderboardOfTests_PAGE);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        Intent intent = getIntent();
        examid = intent.getStringExtra("exam_id");
        resultId = intent.getStringExtra("Result_ID");
        navigation = intent.getStringExtra("navigation");
        chapIdid = intent.getStringExtra("chapter_id");
        module_typetype = intent.getStringExtra("module_type");
        subJectId = intent.getStringExtra("subject_id");
        subNameName = intent.getStringExtra("subName");
        flagNameName = intent.getStringExtra("flagName");
        init();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);

        if (AppUtil.isInternetConnected(Activity_LeaderBoard.this)) {

            getLeaderDataNew();

        } else {
            startActivity(new Intent(getApplicationContext(), Internet_Activity.class));



        }

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void init() {


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        toolbar_leader = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar_leader);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar_leader.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        toolbar_leader.setTitle("Leaderboard");

        collaps = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collaps.setTitle("Leaderboard");
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_leaderboard);

        myName = (TextView) findViewById(R.id.myname_text);
        myRank = (TextView) findViewById(R.id.myrank);
        myTotalQstn = (TextView) findViewById(R.id.mynumber_que);
        myTotCorrect = (TextView) findViewById(R.id.mynumber_ans);
        myTotWrong = (TextView) findViewById(R.id.mywrong_ans);
        myTotTime = (TextView) findViewById(R.id.myleader_time);
        myProfile = (ImageView) findViewById(R.id.myProfile_header);
        myresultCard = (CardView) findViewById(R.id.myresult_layout);
        myresultCard.setVisibility(View.GONE);
        leaderBoaerd_Layout = (CoordinatorLayout) findViewById(R.id.leaderboard_layout);
        leaderBoaerd_Layout.setVisibility(View.VISIBLE);

        dialog = new SpotsDialog(this, "FUN + EDUCATION", R.style.Custom);
        mLeaderList = new ArrayList<LeaderBoardBestTenData>();
        mAdapter = new LeaderBoard_Adapter(mLeaderList, Activity_LeaderBoard.this);
        recyclerView.setAdapter(mAdapter);


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collaps.setTitle("Leaderboard");
                    isShow = true;
                } else if (isShow) {
                    collaps.setTitle(" ");
                    isShow = false;
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private void getLeaderDataNew() {

        int mresultId = 0;
        if (resultId != null) {
            mresultId = Integer.parseInt(resultId);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID", mresultId);
        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionGetLeaderBoardData(
                    this, mServiceReceiver, bundle);
        } else {
            Intent in = new Intent(Activity_LeaderBoard.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();

         new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mShimmerViewContainer.stopShimmer();
                            mShimmerViewContainer.setVisibility(View.GONE);
                        }
                    },1000);

        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getTop10() != null
                        && success.getSuccess().getTop10().size() > 0) {
                    mLeaderList.clear();
                     mLeaderList.addAll(success.getSuccess().getTop10());
                    mAdapter.notifyDataSetChanged();

                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(Activity_LeaderBoard.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(Activity_LeaderBoard.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(Activity_LeaderBoard.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


}
