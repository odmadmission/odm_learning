package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class LearnSubjectProgressData implements Parcelable{


    private String type;
    private String name;
    private double video;
    private double cbt;
    private double prt;
    private double qb;
    private double sn;
    private double total;
    private double chapterId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    protected LearnSubjectProgressData(Parcel in) {
        type = in.readString();
        name = in.readString();
        video = in.readInt();
        cbt = in.readInt();
        prt = in.readInt();
        qb = in.readInt();
        sn = in.readInt();
        total = in.readInt();
        chapterId = in.readInt();
    }

    public static final Creator<LearnSubjectProgressData> CREATOR = new Creator<LearnSubjectProgressData>() {
        @Override
        public LearnSubjectProgressData createFromParcel(Parcel in) {
            return new LearnSubjectProgressData(in);
        }

        @Override
        public LearnSubjectProgressData[] newArray(int size) {
            return new LearnSubjectProgressData[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getVideo() {
        return video;
    }

    public void setVideo(double video) {
        this.video = video;
    }

    public double getCbt() {
        return cbt;
    }

    public void setCbt(double cbt) {
        this.cbt = cbt;
    }

    public double getPrt() {
        return prt;
    }

    public void setPrt(double prt) {
        this.prt = prt;
    }

    public double getQb() {
        return qb;
    }

    public void setQb(double qb) {
        this.qb = qb;
    }

    public double getSn() {
        return sn;
    }

    public void setSn(double sn) {
        this.sn = sn;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getChapterId() {
        return chapterId;
    }

    public void setChapterId(double chapterId) {
        this.chapterId = chapterId;
    }
}
