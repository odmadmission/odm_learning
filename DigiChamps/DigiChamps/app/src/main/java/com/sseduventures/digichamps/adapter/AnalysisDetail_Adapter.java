package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.Model.AnalysisDetaillistModel;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ClickListenerClassForSubConcept;
import com.sseduventures.digichamps.domain.AnalyticsSubjectResponse;

import java.util.ArrayList;
import java.util.List;

public class AnalysisDetail_Adapter extends RecyclerView.Adapter<AnalysisDetail_Adapter.MyViewHolder> {

    private List<AnalyticsSubjectResponse.AnalyticsSubjectChapterList> AnalysisList;



    static CardView container;
    public static RelativeLayout relative;
    Context context;
    int count = 0;
    View view;
    TextView chap;

    public AnalysisDetail_Adapter(ArrayList<AnalyticsSubjectResponse.AnalyticsSubjectChapterList> AnalysisList, Context context) {
        this.context = context;
        this.AnalysisList = AnalysisList;

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.analysis_detail_itemlist, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        view.setTag(myViewHolder);
        return myViewHolder;


    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int listPosition) {

        holder.chap.setText(AnalysisList.get(listPosition).getChapter());
        holder.serial_number.setText(AnalysisList.get(listPosition).getChapter());

        holder.container.setOnClickListener(onClickListener(listPosition));
        holder.llRoot.setTag(listPosition);
        holder.container.setOnClickListener(new ClickListenerClassForSubConcept(holder.llRoot));
        int p=(listPosition+1);
        holder.serial_number.setText(p+""+".");



        holder.CBT_TotalQnos.setText(AnalysisList.get(listPosition).getChapter()+" CBT ");
        if((Integer)AnalysisList.get(listPosition).getCBT_TotalQnos()!=null&&
                (Integer)AnalysisList.get(listPosition).getCBT_TotalQnos()!=null
                &&AnalysisList.get(listPosition).getCBT_TotalQnos()>0
                ) {

            holder.text3.setText(AnalysisList.get(listPosition).getCBT_TotalAns()
                    + "/" + AnalysisList.get(listPosition).getCBT_TotalQnos());
            holder.text3.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);


        }
        else
        {


            holder.text3.setText("");
            holder.text3.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_error,0);
        }



        holder.PRT_TotalQnos.setText(AnalysisList.get(listPosition).getChapter()+" PRT ");
        if((Integer)AnalysisList.get(listPosition).getPRT_TotalQnos()!=null&&
                (Integer)AnalysisList.get(listPosition).getPRT_TotalQnos()!=null
                &&AnalysisList.get(listPosition).getPRT_TotalQnos()>0
                ) {

            holder.text4.setText(AnalysisList.get(listPosition).getPRT_TotalAns()
                    + "/" + AnalysisList.get(listPosition).getPRT_TotalQnos());
            holder.text4.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);

        }
        else
        {

            holder.text4.setText("");
            holder.text4.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_error,0);
        }




    }


    @Override
    public int getItemCount() {
        return AnalysisList.size();
    }
    private LinearLayout previousRoot;
    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyViewHolder holder=(MyViewHolder) view.getTag();
                int p=(int)holder.llRoot.getTag();
                if(previousRoot!=null) {
                    previousRoot.setVisibility(View.GONE);

                }
                previousRoot=holder.llRoot;
                holder.llRoot.setVisibility(View.VISIBLE);

                holder. PRT_TotalQnos.setText(AnalysisList.get(p).getPRT_TotalQnos());

                holder. CBT_TotalQnos.setText(AnalysisList.get(p).getCBT_TotalQnos());

            }
        };
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        CardView container,PRT,CBT;

        TextView chap, PRT_TotalQnos, CBT_TotalQnos,serial_number,text4,text3;
        LinearLayout llRoot;


        public MyViewHolder(View itemView) {
            super(itemView);
            container = (CardView) itemView.findViewById(R.id.card_analysisDetail);


            this.chap = (TextView) itemView.findViewById(R.id.chap);
            this.PRT_TotalQnos = (TextView) itemView.findViewById(R.id.text1);
            this.CBT_TotalQnos = (TextView) itemView.findViewById(R.id.text2);
            this.llRoot = (LinearLayout) itemView.findViewById(R.id.root);
            this.serial_number = (TextView) itemView.findViewById(R.id.serial_number);
            this.text4 = (TextView) itemView.findViewById(R.id.text4);
            this.text3 = (TextView) itemView.findViewById(R.id.text3);


            this.PRT = (CardView) itemView.findViewById(R.id.cardPRT);
            this.CBT = (CardView) itemView.findViewById(R.id.cardCBT);

        }
    }
}
