package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class FeedSuccessData implements Parcelable{

    private List<FeedListData> list;

    public List<FeedListData> getList() {
        return list;
    }

    public void setList(List<FeedListData> list) {
        this.list = list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public FeedSuccessData() {
    }

    protected FeedSuccessData(Parcel in) {
        this.list = in.createTypedArrayList(FeedListData.CREATOR);
    }

    public static final Creator<FeedSuccessData> CREATOR = new Creator<FeedSuccessData>() {
        @Override
        public FeedSuccessData createFromParcel(Parcel source) {
            return new FeedSuccessData(source);
        }

        @Override
        public FeedSuccessData[] newArray(int size) {
            return new FeedSuccessData[size];
        }
    };
}
