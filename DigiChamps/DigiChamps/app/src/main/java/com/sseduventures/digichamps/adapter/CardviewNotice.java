package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.sseduventures.digichamps.Model.NoticeListModel;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.NoticeDetailActivity;
import com.sseduventures.digichamps.domain.NoticesList;
import com.sseduventures.digichamps.utils.Utils;

import java.util.ArrayList;
import java.util.List;



public class CardviewNotice extends RecyclerView.Adapter<CardviewNotice.DataObjectHolder> {

    Context context;
    private List<NoticesList> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        RelativeLayout rel_master;
        android.widget.RadioButton RadioButton;
        TextView tv_subject,tv_date;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            rel_master = (RelativeLayout) itemView.findViewById(R.id.rel_master);
        }
    }

    public CardviewNotice(List<NoticesList> getlist, Context context) {
        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewNotice.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_notice, parent, false);

        CardviewNotice.DataObjectHolder dataObjectHolder = new CardviewNotice.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewNotice.DataObjectHolder holder, final int position) {
       String subject = "<b>Subject:</b> "+list.get(position).getTitle();
       String date = "<b>Date:</b> "+ Utils.parseDateChange(list.get(position).getCreatedDate());;

        holder.tv_subject.setText(Html.fromHtml(subject));
       holder.tv_date.setText(Html.fromHtml(date));
        holder.rel_master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,NoticeDetailActivity.class);

                i.putExtra("createdDate",list.get(position).getCreatedDate());
                i.putExtra("fileURL",list.get(position).getFileURL());
                i.putExtra("description",list.get(position).getDescription());
                i.putExtra("title",list.get(position).getTitle());

                context.startActivity(i);
                Activity activity = (Activity) context;
                activity.overridePendingTransition(R.anim.from_middle, R.anim.to_middle);

            }
        });



    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

