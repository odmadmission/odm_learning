package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class OlympiadListData implements Parcelable {


    private int CompetitiveExamQs_ID;
    private String CompetitiveExamQs_PDFName;
    private String PDF_UploadPath;
    private String PDFOnline;
    private String PDFBeta;

    public int getCompetitiveExamQs_ID() {
        return CompetitiveExamQs_ID;
    }

    public void setCompetitiveExamQs_ID(int competitiveExamQs_ID) {
        CompetitiveExamQs_ID = competitiveExamQs_ID;
    }

    public String getCompetitiveExamQs_PDFName() {
        return CompetitiveExamQs_PDFName;
    }

    public void setCompetitiveExamQs_PDFName(String competitiveExamQs_PDFName) {
        CompetitiveExamQs_PDFName = competitiveExamQs_PDFName;
    }

    public String getPDF_UploadPath() {
        return PDF_UploadPath;
    }

    public void setPDF_UploadPath(String PDF_UploadPath) {
        this.PDF_UploadPath = PDF_UploadPath;
    }

    public String getPDFOnline() {
        return PDFOnline;
    }

    public void setPDFOnline(String PDFOnline) {
        this.PDFOnline = PDFOnline;
    }

    public String getPDFBeta() {
        return PDFBeta;
    }

    public void setPDFBeta(String PDFBeta) {
        this.PDFBeta = PDFBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.CompetitiveExamQs_ID);
        dest.writeString(this.CompetitiveExamQs_PDFName);
        dest.writeString(this.PDF_UploadPath);
        dest.writeString(this.PDFOnline);
        dest.writeString(this.PDFBeta);
    }

    public OlympiadListData() {
    }

    protected OlympiadListData(Parcel in) {
        this.CompetitiveExamQs_ID = in.readInt();
        this.CompetitiveExamQs_PDFName = in.readString();
        this.PDF_UploadPath = in.readString();
        this.PDFOnline = in.readString();
        this.PDFBeta = in.readString();
    }

    public static final Creator<OlympiadListData> CREATOR = new Creator<OlympiadListData>() {
        @Override
        public OlympiadListData createFromParcel(Parcel source) {
            return new OlympiadListData(source);
        }

        @Override
        public OlympiadListData[] newArray(int size) {
            return new OlympiadListData[size];
        }
    };
}
