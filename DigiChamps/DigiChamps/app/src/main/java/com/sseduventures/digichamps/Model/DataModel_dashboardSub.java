package com.sseduventures.digichamps.Model;

import android.widget.ImageView;

/**
 * Created by Asish on 5/9/2017.
 */

public class DataModel_dashboardSub {

    private String sub_name,chapter_num,video_num,sn_number,qb_number,prt_number,sub_id;
    private Integer sub_image;



    public DataModel_dashboardSub(String sub_name, String chapter_num, String video_num
            ,String prt_number,String sn_number,String qb_number,String sub_id) {
        this.sub_name = sub_name;
        this.chapter_num = chapter_num;
        this.video_num = video_num;
        this.sn_number = sn_number;
        this.qb_number = qb_number;
        this.prt_number = prt_number;
        this.sub_id = sub_id;

    }



    public String getSubjectName() {
        return sub_name;
    }

    public void setTopicName(String sub_name) {
        this.sub_name = sub_name;
    }
    public String getTopicName() {
        return sub_name;
    }
    public String getChapterNumber() {
        return chapter_num;
    }

    public void setChapterNumber(String chapter_num) {
        this.chapter_num = chapter_num;
    }



    public String getVideoNumber(){
        return video_num;
    }
    public void setVideoNumber(String video_num){
        this.video_num = video_num;
    }

    public String getSnNumber(){
        return sn_number;
    }

    public String getQbNumber(){
        return  qb_number ;
    }

    public String getPrtNumber(){
        return prt_number;
    }
    public  String getSubId(){
        return  sub_id;
    }

}
