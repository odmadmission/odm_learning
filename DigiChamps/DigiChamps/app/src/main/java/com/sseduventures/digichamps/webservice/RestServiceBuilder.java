package com.sseduventures.digichamps.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by HP on 8/19/2017.
 */

public class RestServiceBuilder
{

    //52.172.183.131
    public static final String BASE_URL ="http://52.172.183.131:8080";//",192.168.1.119http://43.239.200.180";


    private static RestServiceBuilder restServiceBuilder;
    private RestService restService;
   // private IBugService bugService;
    //read  timeout in seconds
    private static final int READ_TIMEOUT = 70;

    //CONS
    private RestServiceBuilder() {


    }

    //STATIC
    public static RestServiceBuilder get()
    {

        if (restServiceBuilder == null) {
            restServiceBuilder = new RestServiceBuilder();
        }
        return restServiceBuilder;

    }


    public RestService getRestService() {
        if(restService !=null)
        {

            return restService;
        }
        else {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(70,TimeUnit.SECONDS)

                    .build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")

                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addConverterFactory(
                            GsonConverterFactory.create(
                                    new GsonBuilder()
                                    .serializeNulls()
                                    .excludeFieldsWithModifiers(
                                            Modifier.FINAL,
                                            Modifier.TRANSIENT,
                                            Modifier.STATIC)
                                    .create()))
                    .build();

            restService = retrofit.create(RestService.class);
            return restService;
        }
    }

}
