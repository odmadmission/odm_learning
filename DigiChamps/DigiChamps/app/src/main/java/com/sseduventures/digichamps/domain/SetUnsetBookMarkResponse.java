package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class SetUnsetBookMarkResponse implements Parcelable{


    private SetUnsetBookMarkResponse Success;

    public SetUnsetBookMarkResponse getSuccess() {
        return Success;
    }

    public void setSuccess(SetUnsetBookMarkResponse success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public SetUnsetBookMarkResponse() {
    }

    protected SetUnsetBookMarkResponse(Parcel in) {
        this.Success = in.readParcelable(SetUnsetBookMarkResponse.class.getClassLoader());
    }

    public static final Creator<SetUnsetBookMarkResponse> CREATOR = new Creator<SetUnsetBookMarkResponse>() {
        @Override
        public SetUnsetBookMarkResponse createFromParcel(Parcel source) {
            return new SetUnsetBookMarkResponse(source);
        }

        @Override
        public SetUnsetBookMarkResponse[] newArray(int size) {
            return new SetUnsetBookMarkResponse[size];
        }
    };
}
