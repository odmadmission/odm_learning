package com.sseduventures.digichamps.Model;

import android.widget.ImageView;

/**
 * Created by ARR on 4/28/2017.
 */

public class DataModel_NotificationDetails {

    private Integer id;

    private String ticketNo, status, sub, date, time;
    private Integer imageView;


    public DataModel_NotificationDetails() {
    }

    public DataModel_NotificationDetails(Integer id, String sub) {
        this.id = id;
        this.ticketNo = ticketNo;
        this.status = status;
        this.date = date;
        this.time = time;
        this.imageView = imageView;
        this.sub = sub;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String date) {
        this.time = time;
    }

    public Integer getImageView() {
        return imageView;
    }

    public Integer setImageView(Integer imageView) {
        this.imageView = imageView;
        return imageView;
    }


}

