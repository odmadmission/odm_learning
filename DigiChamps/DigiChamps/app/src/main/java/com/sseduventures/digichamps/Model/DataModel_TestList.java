package com.sseduventures.digichamps.Model;

/**
 * Created by Swaraj on 6/28/2017.
 */

public class DataModel_TestList {
    private String text_testName,test_id,chapter_id,is_offline;
            private Integer stu_attempt_limit,stu_attempted;
    private  boolean limit_reached;
    public DataModel_TestList(String text_testName , String test_id, String chapter_id, Integer stu_attempted, Integer stu_attempt_limit){
        this.text_testName = text_testName;
        this.test_id = test_id;
        this.chapter_id = chapter_id;
        this.stu_attempted = stu_attempted;
        this.stu_attempt_limit = stu_attempt_limit;
    }
    public String getText_testName() {
        return text_testName;
    }
    public boolean getLimitReached() {
        return limit_reached;
    }

    public void setText_testName(String text_testName) {
        this.text_testName = text_testName;
    }

    public String getTest_id() {
        return test_id;
    }

    public void setTest_id(String test_id) {
        this.test_id = test_id;
    }

    public String getChapterID() {
        return chapter_id;
    }

    public void setChapterID(String chapterID) {
        this.chapter_id = chapterID;
    }

    public Integer getStu_attempted() {
        return stu_attempted;
    }
    public Integer getStu_attempt_limit() {
        return stu_attempt_limit;
    }
    public String getIs_offline() {
        return is_offline;
    }



}
