package com.sseduventures.digichamps.utils;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by HP on 5/1/2017.
 */

public class DateTimeUtil {

    private static final String TAG=DateTimeUtil.class.getSimpleName();
    public static String getSatelliteDateTimeAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        return sdf.format(new Date(time));
    }
    public static String get(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        return sdf.format(new Date(time));


    }

    public static String getAudiRecordTimeFormat(long time) {
        return   String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(time),
                TimeUnit.MILLISECONDS.toSeconds(time) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time))
        );
    }
    public static String convertMillisAsDateString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(new Date(time));
    }
    public static String convertMillisAsChatDateString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
        return sdf.format(new Date(time));
    }
    public static String convertMillisAsTimeString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        return sdf.format(new Date(time));
    }
    public static String convertMillisAsDateTimeString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        return sdf.format(new Date(time));
    }
    public static String getDeviceDateTimeAsString(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }
    public static String getChatFileFormat(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(new Date(time));
    }

    static final String DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public static Date GetUTCdatetimeAsDate()
    {
        //note: doesn't check for null
        return StringDateStrToDate(GetUTCdatetimeAsString());
    }




    public static String GetUTCdatetimeAsString()
    {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static Date StringDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            ////////Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static Date StringFcmDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy, hh:mm:ss a");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static Date StringDoubtDateStrToDate(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            Log.v(TAG,e.getMessage());
        }

        return dateToReturn;
    }
    public static long StringDateStrToTimeMillis(String StrDate)
    {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        try
        {
            dateToReturn = (Date)dateFormat.parse(StrDate);
        }
        catch (ParseException e)
        {
            ////////Log.v(TAG,e.getMessage());
        }

        return dateToReturn.getTime();
    }

    public static String getDateAsTodayEtc(long millis)
    {
           if(DateUtils.isToday(millis))
           {
               return  "Today";

           }
           else

               if(isYesterday(millis))
               {
                   return  "Yesterday";

               }
           else
               return convertMillisAsDateString(millis);
    }
    public static String getChatReadDeliveryFormat(long millis)
    {
        if(DateUtils.isToday(millis))
        {
            return  "today"+" at "+convertMillisAsTimeString(millis);

        }
        else

        if(isYesterday(millis))
        {
            return  "yesterday"+" at "+convertMillisAsTimeString(millis);

        }
        else
            return convertMillisAsChatDateString(millis)+" at "+convertMillisAsTimeString(millis);
    }
    public static boolean isYesterday(long date) {
        Calendar now = Calendar.getInstance();
        Calendar cdate = Calendar.getInstance();
        cdate.setTimeInMillis(date);

        now.add(Calendar.DATE,-1);

        return now.get(Calendar.YEAR) == cdate.get(Calendar.YEAR)
                && now.get(Calendar.MONTH) == cdate.get(Calendar.MONTH)
                && now.get(Calendar.DATE) == cdate.get(Calendar.DATE);
    }
    public static String getSurfResultFormat(long time)
    {
        long difference = System.currentTimeMillis()-time;
        int days=(int)(difference/(1000*60*60*24));
        if(days==0)
        {
            String format=  DateUtils.getRelativeTimeSpanString(
                    time, System.currentTimeMillis(),
                    0L, DateUtils.FORMAT_ABBREV_ALL).toString();
            if(format.contains("sec"))
                return "Just Now";
            else
                return convertMillisAsTimeString(time);

        }
        else

        if(DateUtils.isToday(time))
        {
            return  "Today";

        }
        else
            return convertMillisAsDateString(time);
    }

    public static String getTimeAsNowEtc(long time)
    {
      String format=  DateUtils.getRelativeTimeSpanString(
                time, System.currentTimeMillis(),
                0L, DateUtils.FORMAT_ABBREV_ALL).toString();
        if(format.contains("sec"))
            return "Just Now";
        else
            return convertMillisAsTimeString(time);
    }

    public static String formatDateFromString(Context context, String date)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);

        } catch (ParseException e) {
           // //////Log.v(TAG,e.getMessage());
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM yyyy");
        String finalDate = timeFormat.format(myDate);

        return  finalDate;
    }
    public static Date convertToDate(String dateString)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
        try {
            Date d = sdf.parse(dateString);
            return d;
        } catch (ParseException ex) {

        }

        return null;
    }
    public static Date convertToDate(String dateString,String format)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date d = sdf.parse(dateString);
            return d;
        } catch (ParseException ex) {

        }

        return null;
    }

//    public static String formatDateFromString(String date)
//    {
//        SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.FileAndFolder.MEDIA_FILE_NAME_TIME_SUFFIX);
//        Date myDate = null;
//        try {
//            myDate = dateFormat.parse(date);
//
//        } catch (ParseException e) {
//          //  //////Log.v(TAG,e.getMessage());
//        }
//
//        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//        String finalDate = timeFormat.format(myDate);
//
//        return  finalDate;
//    }
public static long getUtcMillis()
{
    //Instant currTimeStamp = Instant.now();
    return System.currentTimeMillis();
}
    public static String getLastSeen(long time) {

        String d=getDateAsTodayEtc(time);

        return d.toLowerCase()+" at "+convertMillisAsTimeString(time);
    }
    public static String getBugReportUploadTime()
    {

        SimpleDateFormat timeFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String finalDate = timeFormat.format(System.currentTimeMillis());

        return  finalDate;
    }

   /* public static String getTaskTimeStamp()
    {
        return new SimpleDateFormat(
                Constants.FileAndFolder.MEDIA_FILE_NAME_TIME_SUFFIX)
                .format(new Date());
    }*/

    public static String getSatelliteDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        return sdf.format(new Date(System.currentTimeMillis()));
    }
}
