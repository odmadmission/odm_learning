package com.sseduventures.digichamps.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.adapter.DiyDetails_Adapter;
import com.sseduventures.digichamps.domain.NewLearnDiyModuleLists;
import com.sseduventures.digichamps.firebase.LogEventUtil;

import java.util.ArrayList;

public class DoItYourselfActivity extends FormActivity {

    RecyclerView diy_recycl;

    private ArrayList<NewLearnDiyModuleLists> diyList;
    private DiyDetails_Adapter mAdapter;
    private CollapsingToolbarLayout collapsing_toolbar;
    private Toolbar toolbar;
    AppBarLayout appBarLayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setModuleName(LogEventUtil.EVENT_Do_It_Yourself);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        logEvent(LogEventUtil.KEY_Do_It_Yourself,LogEventUtil.EVENT_Do_It_Yourself);
        setContentView(R.layout.activity_do_it_yourself2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DoItYourselfActivity.this, LearnActivity.class);
                startActivity(in);
            }
        });
        appBarLayout = findViewById(R.id.appbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Do It Yourself");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });



        Intent in = getIntent();
        diyList = in.getParcelableArrayListExtra("diylist");

        diy_recycl = findViewById(R.id.diy_recycl);


        diy_recycl.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        diy_recycl.setLayoutManager(mLayoutManager);



        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                if(diyList!=null &&
                        diyList.size()>0){
                    mAdapter = new DiyDetails_Adapter(diyList,DoItYourselfActivity.this);
                    diy_recycl.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();

                }

            }
        },1000);



    }
}
