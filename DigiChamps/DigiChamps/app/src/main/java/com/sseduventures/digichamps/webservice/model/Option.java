package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Option implements Parcelable
{

private int Answer_ID;
private int Question_ID;
private String Option_Desc;
private String Option_Image;

    protected Option(Parcel in) {
        Answer_ID = in.readInt();
        Question_ID = in.readInt();
        Option_Desc = in.readString();
        Option_Image = in.readString();
        Answer_desc = in.readString();
        Answer_Image = in.readString();
        Is_Answer = in.readByte() != 0;
    }

    public static final Creator<Option> CREATOR = new Creator<Option>() {
        @Override
        public Option createFromParcel(Parcel in) {
            return new Option(in);
        }

        @Override
        public Option[] newArray(int size) {
            return new Option[size];
        }
    };

    public int getAnswer_ID() {
        return Answer_ID;
    }

    public void setAnswer_ID(int answer_ID) {
        Answer_ID = answer_ID;
    }

    public int getQuestion_ID() {
        return Question_ID;
    }

    public void setQuestion_ID(int question_ID) {
        Question_ID = question_ID;
    }

    public String getOption_Desc() {
        return Option_Desc;
    }

    public void setOption_Desc(String option_Desc) {
        Option_Desc = option_Desc;
    }

    public String getOption_Image() {
        return Option_Image;
    }

    public void setOption_Image(String option_Image) {
        Option_Image = option_Image;
    }

    public String getAnswer_desc() {
        return Answer_desc;
    }

    public void setAnswer_desc(String answer_desc) {
        Answer_desc = answer_desc;
    }

    public String getAnswer_Image() {
        return Answer_Image;
    }

    public void setAnswer_Image(String answer_Image) {
        Answer_Image = answer_Image;
    }

    public boolean isIs_Answer() {
        return Is_Answer;
    }

    public void setIs_Answer(boolean is_Answer) {
        Is_Answer = is_Answer;
    }

    private String Answer_desc;
private String Answer_Image;
private boolean Is_Answer;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Answer_ID);
        dest.writeInt(Question_ID);
        dest.writeString(Option_Desc);
        dest.writeString(Option_Image);
        dest.writeString(Answer_desc);
        dest.writeString(Answer_Image);
        dest.writeByte((byte) (Is_Answer ? 1 : 0));
    }
}