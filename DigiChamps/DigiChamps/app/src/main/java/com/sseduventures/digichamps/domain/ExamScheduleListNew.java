package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/11/2018.
 */

public class ExamScheduleListNew implements Parcelable {

    private String ExamTypeId;

    private String ExamName;

    private String StartDate;

    public String getExamTypeId() {
        return ExamTypeId;
    }

    public void setExamTypeId(String examTypeId) {
        ExamTypeId = examTypeId;
    }

    public String getExamName() {
        return ExamName;
    }

    public void setExamName(String examName) {
        ExamName = examName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ExamTypeId);
        dest.writeString(this.ExamName);
        dest.writeString(this.StartDate);
    }

    public ExamScheduleListNew() {
    }

    protected ExamScheduleListNew(Parcel in) {
        this.ExamTypeId = in.readString();
        this.ExamName = in.readString();
        this.StartDate = in.readString();
    }

    public static final Creator<ExamScheduleListNew> CREATOR = new Creator<ExamScheduleListNew>() {
        @Override
        public ExamScheduleListNew createFromParcel(Parcel source) {
            return new ExamScheduleListNew(source);
        }

        @Override
        public ExamScheduleListNew[] newArray(int size) {
            return new ExamScheduleListNew[size];
        }
    };
}
