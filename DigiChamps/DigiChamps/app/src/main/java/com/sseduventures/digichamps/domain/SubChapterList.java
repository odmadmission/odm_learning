package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/9/2018.
 */

public class SubChapterList  implements Parcelable{

    private int ChapterId;
    private String Chapter;
    private int SubConceptId;
    private String SubConcept;
    private int Accuracy;
    private String Rank;


    public int getChapterId() {
        return ChapterId;
    }

    public void setChapterId(int chapterId) {
        ChapterId = chapterId;
    }

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String chapter) {
        Chapter = chapter;
    }

    public int getSubConceptId() {
        return SubConceptId;
    }

    public void setSubConceptId(int subConceptId) {
        SubConceptId = subConceptId;
    }

    public String getSubConcept() {
        return SubConcept;
    }

    public void setSubConcept(String subConcept) {
        SubConcept = subConcept;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public String getRank() {
        return Rank;
    }

    public void setRank(String rank) {
        Rank = rank;
    }

    public SubChapterList() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.ChapterId);
        dest.writeString(this.Chapter);
        dest.writeInt(this.SubConceptId);
        dest.writeString(this.SubConcept);
        dest.writeInt(this.Accuracy);
        dest.writeString(this.Rank);
    }

    protected SubChapterList(Parcel in) {
        this.ChapterId = in.readInt();
        this.Chapter = in.readString();
        this.SubConceptId = in.readInt();
        this.SubConcept = in.readString();
        this.Accuracy = in.readInt();
        this.Rank = in.readString();
    }

    public static final Creator<SubChapterList> CREATOR = new Creator<SubChapterList>() {
        @Override
        public SubChapterList createFromParcel(Parcel source) {
            return new SubChapterList(source);
        }

        @Override
        public SubChapterList[] newArray(int size) {
            return new SubChapterList[size];
        }
    };
}
