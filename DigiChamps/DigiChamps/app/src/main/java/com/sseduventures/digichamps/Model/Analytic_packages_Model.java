package com.sseduventures.digichamps.Model;

/**
 * Created by User25 on 7/27/2017.
 */

public class Analytic_packages_Model {

    String package_name;
    String subject_name;
    String expire;
    String price;

    public Analytic_packages_Model(String package_name,  String subject_name, String expire, String price) {
        this.package_name = package_name;
        this.subject_name= subject_name;
        this.expire = expire;
        this.price = price;
    }

    public String getPackage_name() {
        return package_name;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public String getExpire() {
        return expire;
    }

    public String getPrice() {
        return price;
    }
}
