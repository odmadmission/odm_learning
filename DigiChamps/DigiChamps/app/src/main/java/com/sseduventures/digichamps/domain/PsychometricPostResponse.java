package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/8/2018.
 */

public class PsychometricPostResponse implements Parcelable
{
    public String getAcademicMessage() {
        return AcademicMessage;
    }

    public void setAcademicMessage(String academicMessage) {
        AcademicMessage = academicMessage;
    }

    public String getInterpersonalMessage() {
        return InterpersonalMessage;
    }


    public String Academic;
    public String Doubts;
    public String Interest;// { set; get; }
    public String Time;// { set; get; }
    public String SelfStudy;// { set; get; }
    public String School;// { set; get; }
    public String Personal;// { set; get; }
    public String Mentorship;// { set; get; }

    public String getAcademic() {
        return Academic;
    }

    public void setAcademic(String academic) {
        Academic = academic;
    }

    public String getDoubts() {
        return Doubts;
    }

    public void setDoubts(String doubts) {
        Doubts = doubts;
    }

    public String getInterest() {
        return Interest;
    }

    public void setInterest(String interest) {
        Interest = interest;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getSelfStudy() {
        return SelfStudy;
    }

    public void setSelfStudy(String selfStudy) {
        SelfStudy = selfStudy;
    }

    public String getSchool() {
        return School;
    }

    public void setSchool(String school) {
        School = school;
    }

    public String getPersonal() {
        return Personal;
    }

    public void setPersonal(String personal) {
        Personal = personal;
    }

    public String getMentorship() {
        return Mentorship;
    }

    public void setMentorship(String mentorship) {
        Mentorship = mentorship;
    }

    public void setInterpersonalMessage(String interpersonalMessage) {
        InterpersonalMessage = interpersonalMessage;
    }

    private String AcademicMessage;
    private String InterpersonalMessage;

    public PsychometricPostResponse() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Academic);
        dest.writeString(this.Doubts);
        dest.writeString(this.Interest);
        dest.writeString(this.Time);
        dest.writeString(this.SelfStudy);
        dest.writeString(this.School);
        dest.writeString(this.Personal);
        dest.writeString(this.Mentorship);
        dest.writeString(this.AcademicMessage);
        dest.writeString(this.InterpersonalMessage);
    }

    protected PsychometricPostResponse(Parcel in) {
        this.Academic = in.readString();
        this.Doubts = in.readString();
        this.Interest = in.readString();
        this.Time = in.readString();
        this.SelfStudy = in.readString();
        this.School = in.readString();
        this.Personal = in.readString();
        this.Mentorship = in.readString();
        this.AcademicMessage = in.readString();
        this.InterpersonalMessage = in.readString();
    }

    public static final Creator<PsychometricPostResponse> CREATOR = new Creator<PsychometricPostResponse>() {
        @Override
        public PsychometricPostResponse createFromParcel(Parcel source) {
            return new PsychometricPostResponse(source);
        }

        @Override
        public PsychometricPostResponse[] newArray(int size) {
            return new PsychometricPostResponse[size];
        }
    };
}
