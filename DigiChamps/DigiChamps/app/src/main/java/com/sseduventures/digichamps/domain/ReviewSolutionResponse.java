package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class ReviewSolutionResponse implements Parcelable {

    private ReviewSolutionSuccess SuccessReviewQuestion;

    public ReviewSolutionSuccess getSuccessReviewQuestion() {
        return SuccessReviewQuestion;
    }

    public void setSuccessReviewQuestion(ReviewSolutionSuccess successReviewQuestion) {
        SuccessReviewQuestion = successReviewQuestion;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.SuccessReviewQuestion, flags);
    }

    public ReviewSolutionResponse() {
    }

    protected ReviewSolutionResponse(Parcel in) {
        this.SuccessReviewQuestion = in.readParcelable(ReviewSolutionSuccess.class.getClassLoader());
    }

    public static final Creator<ReviewSolutionResponse> CREATOR = new Creator<ReviewSolutionResponse>() {
        @Override
        public ReviewSolutionResponse createFromParcel(Parcel source) {
            return new ReviewSolutionResponse(source);
        }

        @Override
        public ReviewSolutionResponse[] newArray(int size) {
            return new ReviewSolutionResponse[size];
        }
    };
}
