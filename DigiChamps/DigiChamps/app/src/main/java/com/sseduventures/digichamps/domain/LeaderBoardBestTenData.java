package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class LeaderBoardBestTenData implements Parcelable {

    private int Rank;
    private int Max_rid;
    private int Regd_ID;
    private int Totaltime;
    private int Question_Nos;
    private int Total_Correct_Ans;
    private int Appear;
    private String Customer_Name;
    private String Image;
    private int Accuracy;
    private int Incorrect;
    private String StartTime;


    public int getRank() {
        return Rank;
    }

    public void setRank(int rank) {
        Rank = rank;
    }

    public int getMax_rid() {
        return Max_rid;
    }

    public void setMax_rid(int max_rid) {
        Max_rid = max_rid;
    }

    public int getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(int regd_ID) {
        Regd_ID = regd_ID;
    }

    public int getTotaltime() {
        return Totaltime;
    }

    public void setTotaltime(int totaltime) {
        Totaltime = totaltime;
    }

    public int getQuestion_Nos() {
        return Question_Nos;
    }

    public void setQuestion_Nos(int question_Nos) {
        Question_Nos = question_Nos;
    }

    public int getTotal_Correct_Ans() {
        return Total_Correct_Ans;
    }

    public void setTotal_Correct_Ans(int total_Correct_Ans) {
        Total_Correct_Ans = total_Correct_Ans;
    }

    public int getAppear() {
        return Appear;
    }

    public void setAppear(int appear) {
        Appear = appear;
    }

    public String getCustomer_Name() {
        return Customer_Name;
    }

    public void setCustomer_Name(String customer_Name) {
        Customer_Name = customer_Name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }

    public int getIncorrect() {
        return Incorrect;
    }

    public void setIncorrect(int incorrect) {
        Incorrect = incorrect;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String startTime) {
        StartTime = startTime;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Rank);
        dest.writeInt(this.Max_rid);
        dest.writeInt(this.Regd_ID);
        dest.writeInt(this.Totaltime);
        dest.writeInt(this.Question_Nos);
        dest.writeInt(this.Total_Correct_Ans);
        dest.writeInt(this.Appear);
        dest.writeString(this.Customer_Name);
        dest.writeString(this.Image);
        dest.writeInt(this.Accuracy);
        dest.writeInt(this.Incorrect);
        dest.writeString(this.StartTime);
    }

    public LeaderBoardBestTenData() {
    }

    protected LeaderBoardBestTenData(Parcel in) {
        this.Rank = in.readInt();
        this.Max_rid = in.readInt();
        this.Regd_ID = in.readInt();
        this.Totaltime = in.readInt();
        this.Question_Nos = in.readInt();
        this.Total_Correct_Ans = in.readInt();
        this.Appear = in.readInt();
        this.Customer_Name = in.readString();
        this.Image = in.readString();
        this.Accuracy = in.readInt();
        this.Incorrect = in.readInt();
        this.StartTime = in.readString();
    }

    public static final Creator<LeaderBoardBestTenData> CREATOR = new Creator<LeaderBoardBestTenData>() {
        @Override
        public LeaderBoardBestTenData createFromParcel(Parcel source) {
            return new LeaderBoardBestTenData(source);
        }

        @Override
        public LeaderBoardBestTenData[] newArray(int size) {
            return new LeaderBoardBestTenData[size];
        }
    };
}
