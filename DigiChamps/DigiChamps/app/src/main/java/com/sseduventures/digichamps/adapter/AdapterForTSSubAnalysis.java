package com.sseduventures.digichamps.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.TestHighlightsSubconcept;
import com.sseduventures.digichamps.helper.CircularSeekBar;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.List;



public class AdapterForTSSubAnalysis extends RecyclerView.Adapter<AdapterForTSSubAnalysis.MyViewHolder> {
    View view;
    private boolean isInternetPresent = false;


    private SpotsDialog dialog;


    private Context context;
    private List<TestHighlightsSubconcept> moviesList;
    String moduleId;
    String User_Id;

    public AdapterForTSSubAnalysis(List<TestHighlightsSubconcept> moviesList, Context context) {
        this.moviesList = moviesList;
        this.context = context;


    }



    @Override
    public AdapterForTSSubAnalysis.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_test_statistics_sub_cncpt_analysis, parent, false);

        return new AdapterForTSSubAnalysis.MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final AdapterForTSSubAnalysis.MyViewHolder holder, final int position) {



        holder.subConceptName.setText(moviesList.get(position).getSubConceptName());
        holder.correct.setText(moviesList.get(position).getTotalCorrect() + " Correct");
        holder.incorrect.setText(moviesList.get(position).getTotalInCorrect()  + " Incorrect");
        holder.skipped.setText(moviesList.get(position).getTotalSkipped()  + " Skipped");
        holder.prog_text_sub.setText(moviesList.get(position).getAccuracy()+"%");


        final int AccuracyData = moviesList.get(holder.getAdapterPosition()).getAccuracy();

        holder.sub_circle_progress_view_ts.getProgress();
        holder.sub_circle_progress_view_ts.setMax(100);
        holder.sub_circle_progress_view_ts.setPointerHaloColor(R.color.white);
        holder.sub_circle_progress_view_ts.setProgress(AccuracyData);
        holder.sub_circle_progress_view_ts.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                holder.sub_circle_progress_view_ts.setProgress(AccuracyData);
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {
                holder.sub_circle_progress_view_ts.setProgress(AccuracyData);
            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {
                holder.sub_circle_progress_view_ts.setProgress(AccuracyData);
            }


        });

        SharedPreferences preference = context.getApplicationContext().getSharedPreferences("user_data", context.getApplicationContext().MODE_PRIVATE);
        User_Id = preference.getString("User_ID", null);


    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    private View.OnClickListener onClickListener(final int position, final QNChap_Details_Adapter.MyViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
                if (isInternetPresent) {

                } else {
                    Intent i = new Intent(context, Internet_Activity.class);
                    Activity activity = (Activity) context;

                    context.startActivity(i);

                }
            }

        };
    }


    private void init() {


        isInternetPresent = AppUtil.isInternetConnected(context);

        dialog = new SpotsDialog(context, "FUN + EDUCATION", R.style.Custom);


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView subConceptName,correct,incorrect,skipped,prog_text_sub;
        CircularSeekBar sub_circle_progress_view_ts;

        public MyViewHolder(View view) {
            super(view);

            subConceptName = itemView.findViewById(R.id.txt_sub_cncpt_name);
            sub_circle_progress_view_ts = itemView.findViewById(R.id.sub_circle_progress_view_ts);
            correct = itemView.findViewById(R.id.correct);
            incorrect = itemView.findViewById(R.id.incorrect);
            skipped = itemView.findViewById(R.id.skipped);
            prog_text_sub = itemView.findViewById(R.id.prog_text_sub);

        }
    }











}
