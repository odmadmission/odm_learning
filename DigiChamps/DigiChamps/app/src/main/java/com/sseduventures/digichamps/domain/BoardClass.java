package com.sseduventures.digichamps.domain;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class BoardClass implements Parcelable
{
    private long boardId;
    private String boardName;

    private List<BoardClassName> classList;


    public long getBoardId() {
        return boardId;
    }

    public void setBoardId(long boardId) {
        this.boardId = boardId;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public List<BoardClassName> getClassList() {
        return classList;
    }

    public void setClassList(List<BoardClassName> classList) {
        this.classList = classList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.boardId);
        dest.writeString(this.boardName);
        dest.writeList(this.classList);
    }

    public BoardClass() {
    }

    protected BoardClass(Parcel in) {
        this.boardId = in.readLong();
        this.boardName = in.readString();
        this.classList = new ArrayList<BoardClassName>();
        in.readList(this.classList, BoardClassName.class.getClassLoader());
    }

    public static final Creator<BoardClass> CREATOR = new Creator<BoardClass>() {
        @Override
        public BoardClass createFromParcel(Parcel source) {
            return new BoardClass(source);
        }

        @Override
        public BoardClass[] newArray(int size) {
            return new BoardClass[size];
        }
    };
}
