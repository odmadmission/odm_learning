package com.sseduventures.digichamps.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.VideoView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Chapter_Details;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.domain.AppStatusResponse;
import com.sseduventures.digichamps.helper.RegPrefManager;

import com.sseduventures.digichamps.utils.AlarmManagerUtils;

import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.DoubtSubmitModel;


import org.json.JSONObject;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class NewSplashActivity extends FormActivity implements ServiceReceiver.Receiver {


    private static final String TAG = NewSplashActivity.class.getSimpleName();
    private ServiceReceiver mServiceReceiver;
    private Handler mHandler;
    private AppStatusResponse success;
    private int version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_splash);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(AppUtil.isInternetConnected(this))
        getLiveVersion();
        else {
            Intent intent = new Intent( this,
                    Internet_Activity.class);
            startActivity(intent);
        }
    }

    private void init() {

        if (RegPrefManager.getInstance(this).getStatus() == 1)
        {

            Intent intent;
            intent = new Intent(this, NewDashboardActivity.class);
            startActivity(intent);
            finish();

        } else {
            AlarmManagerUtils.cancelSyncAlarmMDM(this,true);
            Intent intent;
            intent = new Intent(this, OnBoardActivity.class);
            startActivity(intent);


            finish();
        }
    }


    private void getInvitorId() {
        Branch.getInstance().initSession(new Branch.BranchReferralInitListener() {
            @Override
            public void onInitFinished(JSONObject referringParams, BranchError error) {
                if (error == null) {
                    try {
                        String famid = referringParams.get("custom_string_mobile").toString();
                        int typeid = referringParams.getInt("custom_string_type");
                        if (famid != null) {
                            RegPrefManager.getInstance(NewSplashActivity.this).setInvitorFamilyId(famid);
                            RegPrefManager.getInstance(NewSplashActivity.this).setTypeId(typeid);
                        }
                        //Log(TAG,famid);
                    } catch (Exception e) {

                    }

                } else {

                }
                startApp();

            }
        }, this.getIntent().getData(), this);
    }

    private void startApp() {
            init();
    }

    public void getLiveVersion(){

        NetworkService.startActionGetLiveAppStatus(this,mServiceReceiver);

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData)
    {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:

                Intent in = new Intent(NewSplashActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:

                Intent intent = new Intent(NewSplashActivity.this,
                        Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(NewSplashActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
             success = resultData.getParcelable(IntentHelper.RESULT_DATA);

             if(success!=null)
             {

                 if(success.getStatus()==-1)
                 {
                        RegPrefManager.getInstance(this).logout();

                 }
                 else {

                     int scode = Integer.parseInt(success.getData());
                     getVersionCode();

                     int appCode = version;

                     if (scode > appCode) {

                         AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
                         builder1.setMessage("New version of app is available in playstore. please update the app");
                         builder1.setCancelable(false);

                         builder1.setPositiveButton(
                                 "UPDATE",
                                 new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int id) {
                                         dialog.cancel();
                                         final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                         try {
                                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                         } catch (android.content.ActivityNotFoundException anfe) {
                                             startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                         }

                                     }
                                 });

                         builder1.setNegativeButton(
                                 "NOT NOW",
                                 new DialogInterface.OnClickListener() {
                                     public void onClick(DialogInterface dialog, int id) {
                                         dialog.cancel();

                                         getInvitorId();
                                     }
                                 });

                         AlertDialog alert11 = builder1.create();
                         alert11.show();
                         alert11.setCancelable(false);
                         alert11.setCanceledOnTouchOutside(false);


                     } else {
                        getInvitorId();
                     }
                 }

             }break;



        }
    }

    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }


    private void getVersionCode(){
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
