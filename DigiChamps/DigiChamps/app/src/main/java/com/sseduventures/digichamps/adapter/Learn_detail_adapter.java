package com.sseduventures.digichamps.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Handler;
import android.widget.SeekBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.Learn_Detail_Model;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.Chapter_Details;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;




public class Learn_detail_adapter extends
        RecyclerView.Adapter<Learn_detail_adapter.MyLearnViewHolder> {


    public static ArrayList<Learn_Detail_Model> dataSet = new ArrayList<>();
    public static int position;
    static RelativeLayout container, container1;
    String sub_name, sub_id;
    public ArrayList<Learn_Detail_Model> selected_usersList = new ArrayList<>();
    View view;

    private Context context;
    private List<Learn_Detail_Model> stringArrayList;
    Handler handler = new Handler();
    private Typeface tf;
    String flags;


    public Learn_detail_adapter(ArrayList<Learn_Detail_Model> data, String sub_name,
                                String sub_id, Context context, String flags) {
        this.context = context;
        this.dataSet = data;
        this.sub_name = sub_name;
        this.sub_id = sub_id;
        tf = Typeface.createFromAsset(context.getAssets(), "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        this.flags = flags;
    }


    @Override
    public MyLearnViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_learn, parent, false);
        MyLearnViewHolder myViewHolder = new MyLearnViewHolder(view);


        return myViewHolder;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final MyLearnViewHolder holder, final int listPosition) {

        ImageView chap_numbers = holder.chap_numbers;
        TextView chap_videos = holder.chap_videos;
        TextView chap_names = holder.chap_names;
        TextView cbt_numbers = holder.cbt_numbers;
        TextView sn_numbers = holder.sn_numbers;
        TextView qb_numbers = holder.qb_numbers;
        TextView prt_numbers = holder.prt_numbers;

        final  SeekBar progressBar = (SeekBar) view.findViewById(R.id.pb);
        SharedPreferences preference = getApplicationContext().getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
        String flag = preference.getString("flag", null);
        if (flag.equalsIgnoreCase("#3949ab")) {
            int colorCodeDarkBlue = Color.parseColor("#3671b3");

        } else if (flag.equalsIgnoreCase("#ffb74d")) {
            int colorCodeDark = Color.parseColor("#f89b31");


        }

        chap_videos.setText(dataSet.get(holder.getAdapterPosition()).getTotal_video());
        prt_numbers.setText(dataSet.get(holder.getAdapterPosition()).getPrt());
        chap_names.setText(dataSet.get(holder.getAdapterPosition()).getChapter_name());
        cbt_numbers.setText(dataSet.get(holder.getAdapterPosition()).getCBT());
        qb_numbers.setText(dataSet.get(holder.getAdapterPosition()).getQues());
        sn_numbers.setText(dataSet.get(holder.getAdapterPosition()).getStudy_notes());


        int colorCodeDarkBlack = Color.parseColor("#363435");
        chap_videos.setTextColor(colorCodeDarkBlack);
        cbt_numbers.setTextColor(colorCodeDarkBlack);
        sn_numbers.setTextColor(colorCodeDarkBlack);
        qb_numbers.setTextColor(colorCodeDarkBlack);
        prt_numbers.setTextColor(colorCodeDarkBlack);

        chap_videos.setTypeface(tf);
        cbt_numbers.setTypeface(tf);
        sn_numbers.setTypeface(tf);
        qb_numbers.setTypeface(tf);
        prt_numbers.setTypeface(tf);
        chap_names.setTypeface(tf);


        container1 = holder.container;



        holder.container.setOnClickListener(onClickListener(listPosition));


        holder.container.setOnClickListener(onClickListener(listPosition));
        progressBar.setProgress((int)dataSet.get(listPosition).getTotalProgress());


        if(dataSet.get(holder.getAdapterPosition()).getChapImage().equals(null) || dataSet.get(holder.getAdapterPosition()).getChapImage().equalsIgnoreCase("null")){
            if (flags.equalsIgnoreCase("#3949ab")) {
                chap_numbers.setImageResource(R.drawable.ic_math);
                progressBar.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.learn_blue), PorterDuff.Mode.MULTIPLY);
                progressBar.getThumb().setColorFilter(context.getResources().getColor(R.color.learn_blue), PorterDuff.Mode.SRC_ATOP);
                chap_videos.setTextColor(context.getResources().getColor(R.color.learn_blue));
                cbt_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                sn_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                qb_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                prt_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));

            }else if(flags.equalsIgnoreCase("#ffb74d")){
                chap_numbers.setImageResource(R.drawable.science_ic);
                progressBar.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.learn_yellow), PorterDuff.Mode.MULTIPLY);
                progressBar.getThumb().setColorFilter(context.getResources().getColor(R.color.learn_yellow), PorterDuff.Mode.SRC_ATOP);
                chap_videos.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                cbt_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                sn_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                qb_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                prt_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
            }
        }else{

            Picasso.with(context).load(dataSet.get(holder.getAdapterPosition()).getChapImage()).into(holder.chap_numbers);

            if (flags.equalsIgnoreCase("#3949ab")) {
                progressBar.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.learn_blue), PorterDuff.Mode.MULTIPLY);
                progressBar.getThumb().setColorFilter(context.getResources().getColor(R.color.learn_blue), PorterDuff.Mode.SRC_ATOP);
                chap_videos.setTextColor(context.getResources().getColor(R.color.learn_blue));
                cbt_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                sn_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                qb_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));
                prt_numbers.setTextColor(context.getResources().getColor(R.color.learn_blue));

                // changing card imageview color to yellow
            }else if(flags.equalsIgnoreCase("#ffb74d")){
                progressBar.getProgressDrawable().setColorFilter(context.getResources().getColor(R.color.learn_yellow), PorterDuff.Mode.MULTIPLY);
                progressBar.getThumb().setColorFilter(context.getResources().getColor(R.color.learn_yellow), PorterDuff.Mode.SRC_ATOP);
                chap_videos.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                cbt_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                sn_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                qb_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
                prt_numbers.setTextColor(context.getResources().getColor(R.color.learn_yellow));
            }



        }
        progressBar.setProgress((int)dataSet.get(listPosition).getTotalProgress());
        progressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                progressBar.setProgress((int)dataSet.get(listPosition).getTotalProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                progressBar.setProgress((int)dataSet.get(listPosition).getTotalProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                progressBar.setProgress((int)dataSet.get(listPosition).getTotalProgress());
            }
        });


    }



    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,Chapter_Details.class);
                intent.putExtra("chapter_id", dataSet.get(position).getChapter_id());
                intent.putExtra("subject_name", sub_name);
                intent.putExtra("subject_id", sub_id);
                intent.putExtra("flagNewKey",flags);
                Activity activity = (Activity) context;
                context.startActivity(intent);

            }
        };
    }

    public static class MyLearnViewHolder extends RecyclerView.ViewHolder {
        public TextView chap_names, chap_videos, cbt_numbers, sn_numbers, qb_numbers, prt_numbers;
        public RelativeLayout ll_listitem;
        RelativeLayout container;
        ImageView left_drawable, right_drawable, chap_numbers;
        Button buy;


        public MyLearnViewHolder(View itemView) {
            super(itemView);
            container = (RelativeLayout) itemView.findViewById(R.id.card_view_learn);
            this.chap_numbers = (ImageView) itemView.findViewById(R.id.serial_number);
            this.chap_names = (TextView) itemView.findViewById(R.id.chap_text);
            this.chap_videos = (TextView) itemView.findViewById(R.id.text_video);
            this.cbt_numbers = (TextView) itemView.findViewById(R.id.text_cbt);
            this.sn_numbers = (TextView) itemView.findViewById(R.id.text_sn);
            this.qb_numbers = (TextView) itemView.findViewById(R.id.text_qb);
            this.prt_numbers = (TextView) itemView.findViewById(R.id.text_prt);




        }
    }


}

