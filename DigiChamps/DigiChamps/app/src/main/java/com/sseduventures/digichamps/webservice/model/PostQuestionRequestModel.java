package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PostQuestionRequestModel implements Parcelable {


    private GetQuestion Get_question;
    public PostQuestionRequestModel() {

    }

    protected PostQuestionRequestModel(Parcel in) {
        Get_question = in.readParcelable(GetQuestion.class.getClassLoader());
    }

    public static final Creator<PostQuestionRequestModel> CREATOR = new Creator<PostQuestionRequestModel>() {
        @Override
        public PostQuestionRequestModel createFromParcel(Parcel in) {
            return new PostQuestionRequestModel(in);
        }

        @Override
        public PostQuestionRequestModel[] newArray(int size) {
            return new PostQuestionRequestModel[size];
        }
    };

    public GetQuestion getGet_question() {
        return Get_question;
    }

    public void setGet_question(GetQuestion get_question) {
        Get_question = get_question;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(Get_question, flags);
    }
}
