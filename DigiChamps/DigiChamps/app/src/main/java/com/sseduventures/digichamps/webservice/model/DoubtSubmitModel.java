package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DoubtSubmitModel implements Parcelable {

    protected DoubtSubmitModel(Parcel in) {
    }

    public static final Creator<DoubtSubmitModel> CREATOR = new Creator<DoubtSubmitModel>() {
        @Override
        public DoubtSubmitModel createFromParcel(Parcel in) {
            return new DoubtSubmitModel(in);
        }

        @Override
        public DoubtSubmitModel[] newArray(int size) {
            return new DoubtSubmitModel[size];
        }
    };

    public DoubtSubmitSuccess getSuccess() {
        return success;
    }

    public void setSuccess(DoubtSubmitSuccess success) {
        this.success = success;
    }

    private DoubtSubmitSuccess success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }

    public class DoubtSubmitSuccess implements Parcelable {

        protected DoubtSubmitSuccess(Parcel in) {
            Message = in.readString();
            Ticket_id = in.readInt();
        }

        public final Creator<DoubtSubmitSuccess> CREATOR = new Creator<DoubtSubmitSuccess>() {
            @Override
            public DoubtSubmitSuccess createFromParcel(Parcel in) {
                return new DoubtSubmitSuccess(in);
            }

            @Override
            public DoubtSubmitSuccess[] newArray(int size) {
                return new DoubtSubmitSuccess[size];
            }
        };

        public String getMessage() {
            return Message;
        }

        public void setMessage(String message) {
            Message = message;
        }

        public int getTicket_id() {
            return Ticket_id;
        }

        public void setTicket_id(int ticket_id) {
            Ticket_id = ticket_id;
        }

        private String Message;
        private int Ticket_id;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(Message);
            parcel.writeInt(Ticket_id);
        }
    }
}
