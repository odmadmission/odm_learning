package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class OlympiadDataSuccess implements Parcelable {


    private List<OlympiadListData> list;

    public List<OlympiadListData> getList() {
        return list;
    }

    public void setList(List<OlympiadListData> list) {
        this.list = list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public OlympiadDataSuccess() {
    }

    protected OlympiadDataSuccess(Parcel in) {
        this.list = in.createTypedArrayList(OlympiadListData.CREATOR);
    }

    public static final Creator<OlympiadDataSuccess> CREATOR = new Creator<OlympiadDataSuccess>() {
        @Override
        public OlympiadDataSuccess createFromParcel(Parcel source) {
            return new OlympiadDataSuccess(source);
        }

        @Override
        public OlympiadDataSuccess[] newArray(int size) {
            return new OlympiadDataSuccess[size];
        }
    };
}
