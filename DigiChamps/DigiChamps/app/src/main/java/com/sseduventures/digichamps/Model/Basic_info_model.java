package com.sseduventures.digichamps.Model;

import java.io.Serializable;

public class Basic_info_model implements Serializable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    String name;
    boolean flag;

    public String chapid;

    public String getChapid() {
        return chapid;
    }

    public void setChapid(String chapid) {
        this.chapid = chapid;
    }

    public String getSubid() {
        return subid;
    }

    public void setSubid(String subid) {
        this.subid = subid;
    }

    public String subid;
    public String subname;
    public String boardId;

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String classId;
}
