package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.Model.ModelObject_Package;
import com.sseduventures.digichamps.activity.PackageActivityNew;

import static com.sseduventures.digichamps.Model.ModelObject_Package.*;

public class CustomPagerAdapter_Package extends PagerAdapter {

    private Context mContext;
    private PackageActivityNew packageActivityNew;
    private Typeface tf;

    public CustomPagerAdapter_Package(Context context,PackageActivityNew packageActivityNew) {
        mContext = context;
        this.packageActivityNew=packageActivityNew;
        tf = Typeface.createFromAsset(context.getAssets(), "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ModelObject_Package modelObject = values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View layout =  inflater.inflate(modelObject.getLayoutResId(), collection, false);

        overrideFonts(mContext,layout);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        ModelObject_Package customPagerEnum = values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

    public void overrideFonts(Context context,View v){
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView ) {
                ((TextView) v).setTypeface(tf);
            }
        } catch (Exception e) {

        }
    }

}