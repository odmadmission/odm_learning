package com.sseduventures.digichamps.domain;



import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class LoginStatus implements Parcelable {

  private Long id;

  private String loginId;

  private Date loginDateTime;

  private Date logoutDateTime;

  private String loginIpAddress;

  private String loginBy;

  private Boolean status;

  private Boolean isAndroid;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLoginId() {
    return loginId;
  }

  public void setLoginId(String loginId) {
    this.loginId = loginId;
  }

  public Date getLoginDateTime() {
    return loginDateTime;
  }

  public void setLoginDateTime(Date loginDateTime) {
    this.loginDateTime = loginDateTime;
  }

  public Date getLogoutDateTime() {
    return logoutDateTime;
  }

  public void setLogoutDateTime(Date logoutDateTime) {
    this.logoutDateTime = logoutDateTime;
  }

  public String getLoginIpAddress() {
    return loginIpAddress;
  }

  public void setLoginIpAddress(String loginIpAddress) {
    this.loginIpAddress = loginIpAddress;
  }

  public String getLoginBy() {
    return loginBy;
  }

  public void setLoginBy(String loginBy) {
    this.loginBy = loginBy;
  }

  public Boolean getStatus() {
    return status;
  }

  public void setStatus(Boolean status) {
    this.status = status;
  }

  public Boolean getAndroid() {
    return isAndroid;
  }

  public void setAndroid(Boolean android) {
    isAndroid = android;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.id);
    dest.writeString(this.loginId);
    dest.writeLong(this.loginDateTime != null ? this.loginDateTime.getTime() : -1);
    dest.writeLong(this.logoutDateTime != null ? this.logoutDateTime.getTime() : -1);
    dest.writeString(this.loginIpAddress);
    dest.writeString(this.loginBy);
    dest.writeValue(this.status);
    dest.writeValue(this.isAndroid);
  }

  public LoginStatus() {
  }

  protected LoginStatus(Parcel in) {
    this.id = (Long) in.readValue(Long.class.getClassLoader());
    this.loginId = in.readString();
    long tmpLoginDateTime = in.readLong();
    this.loginDateTime = tmpLoginDateTime == -1 ? null : new Date(tmpLoginDateTime);
    long tmpLogoutDateTime = in.readLong();
    this.logoutDateTime = tmpLogoutDateTime == -1 ? null : new Date(tmpLogoutDateTime);
    this.loginIpAddress = in.readString();
    this.loginBy = in.readString();
    this.status = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.isAndroid = (Boolean) in.readValue(Boolean.class.getClassLoader());
  }

  public static final Creator<LoginStatus> CREATOR = new Creator<LoginStatus>() {
    @Override
    public LoginStatus createFromParcel(Parcel source) {
      return new LoginStatus(source);
    }

    @Override
    public LoginStatus[] newArray(int size) {
      return new LoginStatus[size];
    }
  };
}
