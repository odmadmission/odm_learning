package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class UploadImageResponse implements Parcelable {

private UploadImageSuccess Success;


    public UploadImageSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(UploadImageSuccess success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public UploadImageResponse() {
    }

    protected UploadImageResponse(Parcel in) {
        this.Success = in.readParcelable(UploadImageSuccess.class.getClassLoader());
    }

    public static final Creator<UploadImageResponse> CREATOR = new Creator<UploadImageResponse>() {
        @Override
        public UploadImageResponse createFromParcel(Parcel source) {
            return new UploadImageResponse(source);
        }

        @Override
        public UploadImageResponse[] newArray(int size) {
            return new UploadImageResponse[size];
        }
    };
}
