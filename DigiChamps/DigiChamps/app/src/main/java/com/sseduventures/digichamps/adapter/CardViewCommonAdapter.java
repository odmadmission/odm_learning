package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


import com.sseduventures.digichamps.R;

import java.util.ArrayList;
import java.util.List;



public class CardViewCommonAdapter extends RecyclerView.Adapter<CardViewCommonAdapter.DataObjectHolder> {
    private String LOG_TAG = "MyRecyclerViewAdapter";
    Context context;
    TextView tv_view;
    private List<String> list = new ArrayList<>();

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name;
        LinearLayout linearMaster;
        RadioButton radioButton;

        public DataObjectHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            radioButton = (RadioButton) itemView.findViewById(R.id.RadioButton);
            linearMaster= (LinearLayout) itemView.findViewById(R.id.linearMaster);
        }
    }

    public CardViewCommonAdapter(List<String> getlist, Context context,TextView tv_view) {
        this.list = getlist;
        this.context = context;
        this.tv_view = tv_view;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_dialog, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {
        holder.name.setText(list.get(position));
        if(list.get(position).equals(tv_view.getText().toString().trim())){
            holder.radioButton.setChecked(true);
        }else{
            holder.radioButton.setChecked(false);
        }
        holder.linearMaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

