package com.sseduventures.digichamps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.domain.ExamScheduleDetailsSuccess;
import com.sseduventures.digichamps.utils.Utils;
import java.util.*;

import com.sseduventures.digichamps.Model.ExamScheduleDetailList;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



public class CardviewExamDetail extends RecyclerView.Adapter<CardviewExamDetail.DataObjectHolder> {
    private String LOG_TAG = "MyRecyclerViewAdapter";
    Context context;
    private List<ExamScheduleDetailsSuccess> list = new ArrayList<>();


    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView tv_subject,tv_time,tv_date;


        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_time = (TextView) itemView.findViewById(R.id.tv_time);
            tv_date = (TextView) itemView.findViewById(R.id.tv_date);
            tv_subject = (TextView) itemView.findViewById(R.id.tv_subject);
        }
    }

    public CardviewExamDetail(List<ExamScheduleDetailsSuccess> getlist, Context context) {


        this.list = getlist;
        this.context = context;
    }

    @Override
    public CardviewExamDetail.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_exam_detail, parent, false);

        CardviewExamDetail.DataObjectHolder dataObjectHolder = new CardviewExamDetail.DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final CardviewExamDetail.DataObjectHolder holder, final int position) {
        holder.tv_subject.setText(list.get(position).getSubjectName());
        if (!list.get(position).getTimeSlot().equals("")) {
            holder.tv_time.setText(list.get(position).getTimeSlot().replace("-", " to "));
        } else {
            holder.tv_time.setText("");
        }
        holder.tv_date.setText(Utils.parseDateChange(list.get(position).getStartDate()));
        }

    @Override
    public int getItemCount() {
        return list.size();
    }


}

