package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class OrderDetailsModel implements Parcelable {

    protected OrderDetailsModel(Parcel in) {
    }

    public static final Creator<OrderDetailsModel> CREATOR = new Creator<OrderDetailsModel>() {
        @Override
        public OrderDetailsModel createFromParcel(Parcel in) {
            return new OrderDetailsModel(in);
        }

        @Override
        public OrderDetailsModel[] newArray(int size) {
            return new OrderDetailsModel[size];
        }
    };

    public OrderSuccess getSuccess() {
        return success;
    }

    public void setSuccess(OrderSuccess success) {
        this.success = success;
    }

    private OrderSuccess success;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public class OrderSuccess implements Parcelable {

        private String Customer_Name;
        private String Email;

        protected OrderSuccess(Parcel in) {
            Customer_Name = in.readString();
            Email = in.readString();
            Mobile = in.readString();
            Phone = in.readString();
            Pincode = in.readString();
            Address = in.readString();
            Order_ID = in.readInt();
            Order_No = in.readString();
            Regd_ID = in.readInt();
            order_date = in.readString();
            tax_Amt = in.readDouble();
            Total = in.readDouble();
            Total_savings = in.readDouble();
            Grand_Total = in.readDouble();
        }

        public final Creator<OrderSuccess> CREATOR = new Creator<OrderSuccess>() {
            @Override
            public OrderSuccess createFromParcel(Parcel in) {
                return new OrderSuccess(in);
            }

            @Override
            public OrderSuccess[] newArray(int size) {
                return new OrderSuccess[size];
            }
        };

        public String getCustomer_Name() {
            return Customer_Name;
        }

        public void setCustomer_Name(String customer_Name) {
            Customer_Name = customer_Name;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getMobile() {
            return Mobile;
        }

        public void setMobile(String mobile) {
            Mobile = mobile;
        }

        public String getPhone() {
            return Phone;
        }

        public void setPhone(String phone) {
            Phone = phone;
        }

        public String getPincode() {
            return Pincode;
        }

        public void setPincode(String pincode) {
            Pincode = pincode;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public int getOrder_ID() {
            return Order_ID;
        }

        public void setOrder_ID(int order_ID) {
            Order_ID = order_ID;
        }

        public String getOrder_No() {
            return Order_No;
        }

        public void setOrder_No(String order_No) {
            Order_No = order_No;
        }

        public int getRegd_ID() {
            return Regd_ID;
        }

        public void setRegd_ID(int regd_ID) {
            Regd_ID = regd_ID;
        }

        public String getOrder_date() {
            return order_date;
        }

        public void setOrder_date(String order_date) {
            this.order_date = order_date;
        }

        public Double getTax_Amt() {
            return tax_Amt;
        }

        public void setTax_Amt(Double tax_Amt) {
            this.tax_Amt = tax_Amt;
        }

        public Double getTotal() {
            return Total;
        }

        public void setTotal(Double total) {
            Total = total;
        }

        public Double getTotal_savings() {
            return Total_savings;
        }

        public void setTotal_savings(Double total_savings) {
            Total_savings = total_savings;
        }

        public Double getGrand_Total() {
            return Grand_Total;
        }

        public void setGrand_Total(Double grand_Total) {
            Grand_Total = grand_Total;
        }

        public ArrayList<Orderclass> getOrder() {
            return order;
        }

        public void setOrder(ArrayList<Orderclass> order) {
            this.order = order;
        }

        private String Mobile;
        private String Phone;
        private String Pincode;
        private String Address;
        private int Order_ID;
        private String Order_No;
        private int Regd_ID;
        private String order_date;
        private Double tax_Amt;
        private Double Total;
        private Double Total_savings;
        private Double Grand_Total;
        private ArrayList<Orderclass> order = null;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Customer_Name);
            dest.writeString(Email);
            dest.writeString(Mobile);
            dest.writeString(Phone);
            dest.writeString(Pincode);
            dest.writeString(Address);
            dest.writeInt(Order_ID);
            dest.writeString(Order_No);
            dest.writeInt(Regd_ID);
            dest.writeString(order_date);
            dest.writeDouble(tax_Amt);
            dest.writeDouble(Total);
            dest.writeDouble(Total_savings);
            dest.writeDouble(Grand_Total);
        }
    }

    public class Orderclass implements Parcelable {

        private int OrderPkg_ID;
        private int Package_ID;
        private String Package_Name;
        private String Package_Desc;
        private String Expiry_Date;

        public int getValidity() {
            return validity;
        }

        public void setValidity(int validity) {
            this.validity = validity;
        }

        private int validity;

        protected Orderclass(Parcel in) {
            OrderPkg_ID = in.readInt();
            Package_ID = in.readInt();
            Package_Name = in.readString();
            Package_Desc = in.readString();
            Expiry_Date = in.readString();
            Board_Name = in.readString();
            Class_Name = in.readString();
            Chapters = in.readInt();
            validity = in.readInt();
        }

        public final Creator<Orderclass> CREATOR = new Creator<Orderclass>() {
            @Override
            public Orderclass createFromParcel(Parcel in) {
                return new Orderclass(in);
            }

            @Override
            public Orderclass[] newArray(int size) {
                return new Orderclass[size];
            }
        };

        public int getOrderPkg_ID() {
            return OrderPkg_ID;
        }

        public void setOrderPkg_ID(int orderPkg_ID) {
            OrderPkg_ID = orderPkg_ID;
        }

        public int getPackage_ID() {
            return Package_ID;
        }

        public void setPackage_ID(int package_ID) {
            Package_ID = package_ID;
        }

        public String getPackage_Name() {
            return Package_Name;
        }

        public void setPackage_Name(String package_Name) {
            Package_Name = package_Name;
        }

        public String getPackage_Desc() {
            return Package_Desc;
        }

        public void setPackage_Desc(String package_Desc) {
            Package_Desc = package_Desc;
        }

        public String getExpiry_Date() {
            return Expiry_Date;
        }

        public void setExpiry_Date(String expiry_Date) {
            Expiry_Date = expiry_Date;
        }

        public String getBoard_Name() {
            return Board_Name;
        }

        public void setBoard_Name(String board_Name) {
            Board_Name = board_Name;
        }

        public String getClass_Name() {
            return Class_Name;
        }

        public void setClass_Name(String class_Name) {
            Class_Name = class_Name;
        }

        public int getChapters() {
            return Chapters;
        }

        public void setChapters(int chapters) {
            Chapters = chapters;
        }

        public ArrayList<Subjects> getSubjects() {
            return subjects;
        }

        public void setSubjects(ArrayList<Subjects> subjects) {
            this.subjects = subjects;
        }

        private String Board_Name;
        private String Class_Name;
        private int Chapters;
        private ArrayList<Subjects> subjects = null;


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(OrderPkg_ID);
            dest.writeInt(validity);
            dest.writeInt(Package_ID);
            dest.writeString(Package_Name);
            dest.writeString(Package_Desc);
            dest.writeString(Expiry_Date);
            dest.writeString(Board_Name);
            dest.writeString(Class_Name);
            dest.writeInt(Chapters);
        }
    }

    public class Subjects implements Parcelable {
        protected Subjects(Parcel in) {
            Subject_Name = in.readString();
        }

        public final Creator<Subjects> CREATOR = new Creator<Subjects>() {
            @Override
            public Subjects createFromParcel(Parcel in) {
                return new Subjects(in);
            }

            @Override
            public Subjects[] newArray(int size) {
                return new Subjects[size];
            }
        };

        public String getSubject_Name() {
            return Subject_Name;
        }

        public void setSubject_Name(String subject_Name) {
            Subject_Name = subject_Name;
        }

        private String Subject_Name;

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(Subject_Name);
        }
    }
}
