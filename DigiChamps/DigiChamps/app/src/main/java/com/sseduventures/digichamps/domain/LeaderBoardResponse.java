package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class LeaderBoardResponse implements Parcelable {

    private LeaderBoardSuccess success;

    public LeaderBoardSuccess getSuccess() {
        return success;
    }

    public void setSuccess(LeaderBoardSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public LeaderBoardResponse() {
    }

    protected LeaderBoardResponse(Parcel in) {
        this.success = in.readParcelable(LeaderBoardSuccess.class.getClassLoader());
    }

    public static final Creator<LeaderBoardResponse> CREATOR = new Creator<LeaderBoardResponse>() {
        @Override
        public LeaderBoardResponse createFromParcel(Parcel source) {
            return new LeaderBoardResponse(source);
        }

        @Override
        public LeaderBoardResponse[] newArray(int size) {
            return new LeaderBoardResponse[size];
        }
    };
}
