package com.sseduventures.digichamps.Model;

import com.sseduventures.digichamps.R;

/**
 * Created by anupamchugh on 26/12/15.
 */
public enum ModelViewPager_ExamDesc {

    RED(R.string.red, R.layout.exam_desc_pager1),
    BLUE(R.string.blue, R.layout.exam_desc_pager2),
    GREEN(R.string.green, R.layout.exam_desc_pager3);

    private int mTitleResId;
    private int mLayoutResId;

    ModelViewPager_ExamDesc(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
