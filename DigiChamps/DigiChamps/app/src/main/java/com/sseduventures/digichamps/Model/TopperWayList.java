
package com.sseduventures.digichamps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopperWayList {

    @SerializedName("createdDate")
    @Expose
    private String createdDate;
    @SerializedName("fileType")
    @Expose
    private String fileType;
    @SerializedName("fileURL")
    @Expose
    private String fileURL;
    @SerializedName("className")
    @Expose
    private String className;
    @SerializedName("sectionName")
    @Expose
    private String sectionName;
    @SerializedName("title")
    @Expose
    private String title;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}
