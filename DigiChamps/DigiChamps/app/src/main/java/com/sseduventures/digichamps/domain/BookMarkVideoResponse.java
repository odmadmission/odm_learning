package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class BookMarkVideoResponse implements Parcelable{

    private BookMarkVideoSuccess SuccessBookmarkedVideo;

    protected BookMarkVideoResponse(Parcel in) {
    }

    public static final Creator<BookMarkVideoResponse> CREATOR = new Creator<BookMarkVideoResponse>() {
        @Override
        public BookMarkVideoResponse createFromParcel(Parcel in) {
            return new BookMarkVideoResponse(in);
        }

        @Override
        public BookMarkVideoResponse[] newArray(int size) {
            return new BookMarkVideoResponse[size];
        }
    };

    public BookMarkVideoSuccess getSuccessBookmarkedVideo() {
        return SuccessBookmarkedVideo;
    }

    public void setSuccessBookmarkedVideo(BookMarkVideoSuccess successBookmarkedVideo) {
        SuccessBookmarkedVideo = successBookmarkedVideo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
    }
}
