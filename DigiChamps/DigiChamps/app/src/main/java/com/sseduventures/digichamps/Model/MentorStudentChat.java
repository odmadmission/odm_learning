package com.sseduventures.digichamps.Model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class MentorStudentChat implements Parcelable{


  private long chatId;
  private long mentorId;
  private long teacherId;
  private String message;

  private long regId;
  private Date insertedDate;
  private Date syncDate;
  private int sender;
  private int mime;

  private String fileName;


  private byte[] image;

  public byte[] getImage() {
    return image;
  }

  public void setImage(byte[] image) {
    this.image = image;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public int getSender() {
    return sender;
  }

  public void setSender(int sender) {
    this.sender = sender;
  }

  public int getMime() {
    return mime;
  }

  public void setMime(int mime) {
    this.mime = mime;
  }

  private long rowId;



  public long getRowId() {
    return rowId;
  }

  public void setRowId(long rowId) {
    this.rowId = rowId;
  }

  public long getChatId() {
    return chatId;
  }

  public void setChatId(long chatId) {
    this.chatId = chatId;
  }


  public long getMentorId() {
    return mentorId;
  }

  public void setMentorId(long mentorId) {
    this.mentorId = mentorId;
  }


  public long getTeacherId() {
    return teacherId;
  }

  public void setTeacherId(long teacherId) {
    this.teacherId = teacherId;
  }


  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }



  public long getRegId() {
    return regId;
  }

  public void setRegId(long regId) {
    this.regId = regId;
  }

  public Date getInsertedDate() {
    return insertedDate;
  }

  public void setInsertedDate(Date insertedDate) {
    this.insertedDate = insertedDate;
  }

  public Date getSyncDate() {
    return syncDate;
  }

  public void setSyncDate(Date syncDate) {
    this.syncDate = syncDate;
  }


  public MentorStudentChat() {
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeLong(this.chatId);
    dest.writeLong(this.mentorId);
    dest.writeLong(this.teacherId);
    dest.writeString(this.message);
    dest.writeLong(this.regId);
    dest.writeLong(this.insertedDate != null ? this.insertedDate.getTime() : -1);
    dest.writeLong(this.syncDate != null ? this.syncDate.getTime() : -1);
    dest.writeInt(this.sender);
    dest.writeInt(this.mime);
    dest.writeString(this.fileName);
    dest.writeByteArray(this.image);
    dest.writeLong(this.rowId);
  }

  protected MentorStudentChat(Parcel in) {
    this.chatId = in.readLong();
    this.mentorId = in.readLong();
    this.teacherId = in.readLong();
    this.message = in.readString();
    this.regId = in.readLong();
    long tmpInsertedDate = in.readLong();
    this.insertedDate = tmpInsertedDate == -1 ? null : new Date(tmpInsertedDate);
    long tmpSyncDate = in.readLong();
    this.syncDate = tmpSyncDate == -1 ? null : new Date(tmpSyncDate);
    this.sender = in.readInt();
    this.mime = in.readInt();
    this.fileName = in.readString();
    this.image = in.createByteArray();
    this.rowId = in.readLong();
  }

  public static final Creator<MentorStudentChat> CREATOR = new Creator<MentorStudentChat>() {
    @Override
    public MentorStudentChat createFromParcel(Parcel source) {
      return new MentorStudentChat(source);
    }

    @Override
    public MentorStudentChat[] newArray(int size) {
      return new MentorStudentChat[size];
    }
  };
}
