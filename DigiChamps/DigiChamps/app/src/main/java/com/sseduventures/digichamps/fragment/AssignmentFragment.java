package com.sseduventures.digichamps.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.HomeworkActivity;
import com.sseduventures.digichamps.activity.MainActivity;
import com.sseduventures.digichamps.activity.StudyMaterialActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.FontManage;


/**
 * Created by user on 06-12-2017.
 */

public class AssignmentFragment extends Fragment {

    Context mContext;
    RelativeLayout btnStudyMaterial,btnDailyHome;
    TextView llToolbar1,llToolbar2,lblDailyHome,ll_want,ll_want1,ll_data,ll_data1;
    TextView click1,click2,here1,here2,lblStduy;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_assign, container, false);
        mContext = getActivity();

        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_assignment_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_assignment_frag,LogEventUtil.EVENT_assignment_frag);
        }

        llToolbar1 = view.findViewById(R.id.llToolbar1);
        llToolbar2 = view.findViewById(R.id.llToolbar2);
        lblDailyHome = view.findViewById(R.id.lblDailyHome);
        ll_want = view.findViewById(R.id.ll_want);
        ll_want1 = view.findViewById(R.id.ll_want1);
        ll_data = view.findViewById(R.id.ll_data);
        ll_data1 = view.findViewById(R.id.ll_data1);
        click1 = view.findViewById(R.id.click1);
        click2 = view.findViewById(R.id.click2);
        here1 = view.findViewById(R.id.here1);
        here2 = view.findViewById(R.id.here2);
        lblStduy = view.findViewById(R.id.lblStduy);


        btnStudyMaterial = view.findViewById(R.id.btnStudyMaterial);
        btnDailyHome =  view.findViewById(R.id.btnDailyHome);

        btnDailyHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, HomeworkActivity.class);
                startActivity(i);
            }
        });

        btnStudyMaterial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, StudyMaterialActivity.class);
                startActivity(i);
            }
        });

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                    Intent i = new Intent(mContext, MainActivity.class);
                    mContext.startActivity(i);
                    return true;
                } else {
                    return false;
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FontManage.setFontHeaderBold(mContext,llToolbar1);
        FontManage.setFontHeaderBold(mContext,llToolbar2);
        FontManage.setFontTextLandingBold(mContext,ll_want);
        FontManage.setFontTextLandingBold(mContext,ll_data);
        FontManage.setFontTextLandingBold(mContext,ll_data1);
        FontManage.setFontTextLandingBold(mContext,ll_want1);
        FontManage.setFontMeiryo(mContext,click1);
        FontManage.setFontMeiryo(mContext,here1);
        FontManage.setFontMeiryo(mContext,click2);
        FontManage.setFontMeiryo(mContext,here2);
        FontManage.setFontImpact(mContext,lblDailyHome);
        FontManage.setFontImpact(mContext,lblStduy);


    }
}
