package com.sseduventures.digichamps.Model;

import java.util.Date;

public class ProgressModule
{
    public int Id;
    public int StudentSubjectProgress_ID;
    public int Subject_ID;
    public int Chapter_ID;
    public int Class_ID;
    public String School_ID;
    public String Section_ID;
    public long Regd_ID;
    public int Module_ID;
    public String Module_Type;
    public Date Inserted_On;
    public boolean Is_Active;
    public boolean Is_Delete;

    public int rowId;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getStudentSubjectProgress_ID() {
        return StudentSubjectProgress_ID;
    }

    public void setStudentSubjectProgress_ID(int studentSubjectProgress_ID) {
        StudentSubjectProgress_ID = studentSubjectProgress_ID;
    }

    public int getSubject_ID() {
        return Subject_ID;
    }

    public void setSubject_ID(int subject_ID) {
        Subject_ID = subject_ID;
    }

    public int getChapter_ID() {
        return Chapter_ID;
    }

    public void setChapter_ID(int chapter_ID) {
        Chapter_ID = chapter_ID;
    }

    public int getClass_ID() {
        return Class_ID;
    }

    public void setClass_ID(int class_ID) {
        Class_ID = class_ID;
    }

    public String getSchool_ID() {
        return School_ID;
    }

    public void setSchool_ID(String school_ID) {
        School_ID = school_ID;
    }

    public String getSection_ID() {
        return Section_ID;
    }

    public void setSection_ID(String section_ID) {
        Section_ID = section_ID;
    }

    public long getRegd_ID() {
        return Regd_ID;
    }

    public void setRegd_ID(long regd_ID) {
        Regd_ID = regd_ID;
    }

    public int getModule_ID() {
        return Module_ID;
    }

    public void setModule_ID(int module_ID) {
        Module_ID = module_ID;
    }

    public String getModule_Type() {
        return Module_Type;
    }

    public void setModule_Type(String module_Type) {
        Module_Type = module_Type;
    }

    public Date getInserted_On() {
        return Inserted_On;
    }

    public void setInserted_On(Date inserted_On) {
        Inserted_On = inserted_On;
    }

    public boolean isIs_Active() {
        return Is_Active;
    }

    public void setIs_Active(boolean is_Active) {
        Is_Active = is_Active;
    }

    public boolean isIs_Delete() {
        return Is_Delete;
    }

    public void setIs_Delete(boolean is_Delete) {
        Is_Delete = is_Delete;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }
}
