package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class DoubtListData implements Parcelable{

  private String Question;
  private int Ticket_id;
  private int SubId;
  private int ChapId;
  private String ChapName;
  private String Created_on;
  private boolean Is_Answred;
  private String Status;
  private String Ticket_No;

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getTicket_id() {
        return Ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        Ticket_id = ticket_id;
    }

    public int getSubId() {
        return SubId;
    }

    public void setSubId(int subId) {
        SubId = subId;
    }

    public int getChapId() {
        return ChapId;
    }

    public void setChapId(int chapId) {
        ChapId = chapId;
    }

    public String getChapName() {
        return ChapName;
    }

    public void setChapName(String chapName) {
        ChapName = chapName;
    }

    public String getCreated_on() {
        return Created_on;
    }

    public void setCreated_on(String created_on) {
        Created_on = created_on;
    }

    public boolean isIs_Answred() {
        return Is_Answred;
    }

    public void setIs_Answred(boolean is_Answred) {
        Is_Answred = is_Answred;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getTicket_No() {
        return Ticket_No;
    }

    public void setTicket_No(String ticket_No) {
        Ticket_No = ticket_No;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Question);
        dest.writeInt(this.Ticket_id);
        dest.writeInt(this.SubId);
        dest.writeInt(this.ChapId);
        dest.writeString(this.ChapName);
        dest.writeString(this.Created_on);
        dest.writeByte(this.Is_Answred ? (byte) 1 : (byte) 0);
        dest.writeString(this.Status);
        dest.writeString(this.Ticket_No);
    }

    public DoubtListData() {
    }

    protected DoubtListData(Parcel in) {
        this.Question = in.readString();
        this.Ticket_id = in.readInt();
        this.SubId = in.readInt();
        this.ChapId = in.readInt();
        this.ChapName = in.readString();
        this.Created_on = in.readString();
        this.Is_Answred = in.readByte() != 0;
        this.Status = in.readString();
        this.Ticket_No = in.readString();
    }

    public static final Creator<DoubtListData> CREATOR = new Creator<DoubtListData>() {
        @Override
        public DoubtListData createFromParcel(Parcel source) {
            return new DoubtListData(source);
        }

        @Override
        public DoubtListData[] newArray(int size) {
            return new DoubtListData[size];
        }
    };
}
