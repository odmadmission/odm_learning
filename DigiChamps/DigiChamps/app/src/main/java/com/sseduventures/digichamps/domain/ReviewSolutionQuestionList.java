package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class ReviewSolutionQuestionList implements Parcelable {

    private String Question;
    private String Image;
    private List<ReviewSolutionOptions> Options;
    private ReviewSolutionAnswers Answers;

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public List<ReviewSolutionOptions> getOptions() {
        return Options;
    }

    public void setOptions(List<ReviewSolutionOptions> options) {
        Options = options;
    }

    public ReviewSolutionAnswers getAnswers() {
        return Answers;
    }

    public void setAnswers(ReviewSolutionAnswers answers) {
        Answers = answers;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Question);
        dest.writeString(this.Image);
        dest.writeTypedList(this.Options);
        dest.writeParcelable(this.Answers, flags);
    }

    public ReviewSolutionQuestionList() {
    }

    protected ReviewSolutionQuestionList(Parcel in) {
        this.Question = in.readString();
        this.Image = in.readString();
        this.Options = in.createTypedArrayList(ReviewSolutionOptions.CREATOR);
        this.Answers = in.readParcelable(ReviewSolutionAnswers.class.getClassLoader());
    }

    public static final Creator<ReviewSolutionQuestionList> CREATOR = new Creator<ReviewSolutionQuestionList>() {
        @Override
        public ReviewSolutionQuestionList createFromParcel(Parcel source) {
            return new ReviewSolutionQuestionList(source);
        }

        @Override
        public ReviewSolutionQuestionList[] newArray(int size) {
            return new ReviewSolutionQuestionList[size];
        }
    };
}
