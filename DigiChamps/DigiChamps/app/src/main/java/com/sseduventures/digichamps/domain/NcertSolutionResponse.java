package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class NcertSolutionResponse implements Parcelable{

    private NcertSolutionSuccess Success;

    public NcertSolutionSuccess getSuccess() {
        return Success;
    }

    public void setSuccess(NcertSolutionSuccess success) {
        Success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.Success, flags);
    }

    public NcertSolutionResponse() {
    }

    protected NcertSolutionResponse(Parcel in) {
        this.Success = in.readParcelable(NcertSolutionSuccess.class.getClassLoader());
    }

    public static final Creator<NcertSolutionResponse> CREATOR = new Creator<NcertSolutionResponse>() {
        @Override
        public NcertSolutionResponse createFromParcel(Parcel source) {
            return new NcertSolutionResponse(source);
        }

        @Override
        public NcertSolutionResponse[] newArray(int size) {
            return new NcertSolutionResponse[size];
        }
    };
}
