package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/9/2018.
 */

public class LogoutSuccess implements Parcelable{

    private String Message;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Message);
    }

    public LogoutSuccess() {
    }

    protected LogoutSuccess(Parcel in) {
        this.Message = in.readString();
    }

    public static final Creator<LogoutSuccess> CREATOR = new Creator<LogoutSuccess>() {
        @Override
        public LogoutSuccess createFromParcel(Parcel source) {
            return new LogoutSuccess(source);
        }

        @Override
        public LogoutSuccess[] newArray(int size) {
            return new LogoutSuccess[size];
        }
    };
}
