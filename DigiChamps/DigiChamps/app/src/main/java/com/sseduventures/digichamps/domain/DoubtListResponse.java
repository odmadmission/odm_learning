package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/6/2018.
 */

public class DoubtListResponse implements Parcelable{

    private DoubtListSuccess success;

    public DoubtListSuccess getSuccess() {
        return success;
    }

    public void setSuccess(DoubtListSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public DoubtListResponse() {
    }

    protected DoubtListResponse(Parcel in) {
        this.success = in.readParcelable(DoubtListSuccess.class.getClassLoader());
    }

    public static final Creator<DoubtListResponse> CREATOR = new Creator<DoubtListResponse>() {
        @Override
        public DoubtListResponse createFromParcel(Parcel source) {
            return new DoubtListResponse(source);
        }

        @Override
        public DoubtListResponse[] newArray(int size) {
            return new DoubtListResponse[size];
        }
    };
}
