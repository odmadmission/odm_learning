package com.sseduventures.digichamps.Model;

/**
 * Created by khushbu on 2/28/2018.
 */

public class OptionItem
{
    private String Option;
    private String OptionID;

    public String getOption() {
        return Option;
    }

    public void setOption(String option) {
        Option = option;
    }

    public String getOptionID() {
        return OptionID;
    }

    public void setOptionID(String optionID) {
        OptionID = optionID;
    }
}
