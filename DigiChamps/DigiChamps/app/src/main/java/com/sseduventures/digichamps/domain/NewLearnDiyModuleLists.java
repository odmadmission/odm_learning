package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/20/2018.
 */

public class NewLearnDiyModuleLists implements Parcelable {

    private String Module_Id;
    private String Module_Title;
    private String Module_Name;
    private String Module_video;
    private String Description;
    private String Module_Image;
    private String Image_Key;
    private boolean Is_Avail;
    private String Is_Free;
    private String Validity;
    private String Is_Free_Test;
    private String Media_Id;
    private String VideoKey;
    private String template_id;
    private String thumbnail_key;
    private boolean Is_Expire;
    private int DIYVideo_ID;
    private String DIYVideo_Name;
    private String DIYVideo_Upload;
    private String DIYVideo_Description;
    private String DIYImages;
    private String DiyPosterImage_beta;
    private String DiyPosterImage_production;

    private boolean isFlag;

    public boolean isFlag() {
        return isFlag;
    }

    public void setFlag(boolean flag) {
        isFlag = flag;
    }

    public String getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(String module_Id) {
        Module_Id = module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public void setModule_Title(String module_Title) {
        Module_Title = module_Title;
    }

    public String getModule_Name() {
        return Module_Name;
    }

    public void setModule_Name(String module_Name) {
        Module_Name = module_Name;
    }

    public String getModule_video() {
        return Module_video;
    }

    public void setModule_video(String module_video) {
        Module_video = module_video;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public void setModule_Image(String module_Image) {
        Module_Image = module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public void setImage_Key(String image_Key) {
        Image_Key = image_Key;
    }

    public boolean isIs_Avail() {
        return Is_Avail;
    }

    public void setIs_Avail(boolean is_Avail) {
        Is_Avail = is_Avail;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public void setIs_Free(String is_Free) {
        Is_Free = is_Free;
    }

    public String getValidity() {
        return Validity;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }

    public String getIs_Free_Test() {
        return Is_Free_Test;
    }

    public void setIs_Free_Test(String is_Free_Test) {
        Is_Free_Test = is_Free_Test;
    }

    public String getMedia_Id() {
        return Media_Id;
    }

    public void setMedia_Id(String media_Id) {
        Media_Id = media_Id;
    }

    public String getVideoKey() {
        return VideoKey;
    }

    public void setVideoKey(String videoKey) {
        VideoKey = videoKey;
    }

    public String getTemplate_id() {
        return template_id;
    }

    public void setTemplate_id(String template_id) {
        this.template_id = template_id;
    }

    public String getThumbnail_key() {
        return thumbnail_key;
    }

    public void setThumbnail_key(String thumbnail_key) {
        this.thumbnail_key = thumbnail_key;
    }

    public boolean isIs_Expire() {
        return Is_Expire;
    }

    public void setIs_Expire(boolean is_Expire) {
        Is_Expire = is_Expire;
    }

    public int getDIYVideo_ID() {
        return DIYVideo_ID;
    }

    public void setDIYVideo_ID(int DIYVideo_ID) {
        this.DIYVideo_ID = DIYVideo_ID;
    }

    public String getDIYVideo_Name() {
        return DIYVideo_Name;
    }

    public void setDIYVideo_Name(String DIYVideo_Name) {
        this.DIYVideo_Name = DIYVideo_Name;
    }

    public String getDIYVideo_Upload() {
        return DIYVideo_Upload;
    }

    public void setDIYVideo_Upload(String DIYVideo_Upload) {
        this.DIYVideo_Upload = DIYVideo_Upload;
    }

    public String getDIYVideo_Description() {
        return DIYVideo_Description;
    }

    public void setDIYVideo_Description(String DIYVideo_Description) {
        this.DIYVideo_Description = DIYVideo_Description;
    }

    public String getDIYImages() {
        return DIYImages;
    }

    public void setDIYImages(String DIYImages) {
        this.DIYImages = DIYImages;
    }

    public String getDiyPosterImage_beta() {
        return DiyPosterImage_beta;
    }

    public void setDiyPosterImage_beta(String diyPosterImage_beta) {
        DiyPosterImage_beta = diyPosterImage_beta;
    }

    public String getDiyPosterImage_production() {
        return DiyPosterImage_production;
    }

    public void setDiyPosterImage_production(String diyPosterImage_production) {
        DiyPosterImage_production = diyPosterImage_production;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Module_Id);
        dest.writeString(this.Module_Title);
        dest.writeString(this.Module_Name);
        dest.writeString(this.Module_video);
        dest.writeString(this.Description);
        dest.writeString(this.Module_Image);
        dest.writeString(this.Image_Key);
        dest.writeByte(this.Is_Avail ? (byte) 1 : (byte) 0);
        dest.writeString(this.Is_Free);
        dest.writeString(this.Validity);
        dest.writeString(this.Is_Free_Test);
        dest.writeString(this.Media_Id);
        dest.writeString(this.VideoKey);
        dest.writeString(this.template_id);
        dest.writeString(this.thumbnail_key);
        dest.writeByte(this.Is_Expire ? (byte) 1 : (byte) 0);
        dest.writeInt(this.DIYVideo_ID);
        dest.writeString(this.DIYVideo_Name);
        dest.writeString(this.DIYVideo_Upload);
        dest.writeString(this.DIYVideo_Description);
        dest.writeString(this.DIYImages);
        dest.writeString(this.DiyPosterImage_beta);
        dest.writeString(this.DiyPosterImage_production);
    }

    public NewLearnDiyModuleLists() {
    }

    protected NewLearnDiyModuleLists(Parcel in) {
        this.Module_Id = in.readString();
        this.Module_Title = in.readString();
        this.Module_Name = in.readString();
        this.Module_video = in.readString();
        this.Description = in.readString();
        this.Module_Image = in.readString();
        this.Image_Key = in.readString();
        this.Is_Avail = in.readByte() != 0;
        this.Is_Free = in.readString();
        this.Validity = in.readString();
        this.Is_Free_Test = in.readString();
        this.Media_Id = in.readString();
        this.VideoKey = in.readString();
        this.template_id = in.readString();
        this.thumbnail_key = in.readString();
        this.Is_Expire = in.readByte() != 0;
        this.DIYVideo_ID = in.readInt();
        this.DIYVideo_Name = in.readString();
        this.DIYVideo_Upload = in.readString();
        this.DIYVideo_Description = in.readString();
        this.DIYImages = in.readString();
        this.DiyPosterImage_beta = in.readString();
        this.DiyPosterImage_production = in.readString();
    }

    public static final Creator<NewLearnDiyModuleLists> CREATOR = new Creator<NewLearnDiyModuleLists>() {
        @Override
        public NewLearnDiyModuleLists createFromParcel(Parcel source) {
            return new NewLearnDiyModuleLists(source);
        }

        @Override
        public NewLearnDiyModuleLists[] newArray(int size) {
            return new NewLearnDiyModuleLists[size];
        }
    };
}
