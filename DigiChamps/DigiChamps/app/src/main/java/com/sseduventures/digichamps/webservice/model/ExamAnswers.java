package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ExamAnswers implements Parcelable {
    public ExamAnswers() {
    }

    protected ExamAnswers(Parcel in) {
        Answer_id = in.readString();
    }

    public static final Creator<ExamAnswers> CREATOR = new Creator<ExamAnswers>() {
        @Override
        public ExamAnswers createFromParcel(Parcel in) {
            return new ExamAnswers(in);
        }

        @Override
        public ExamAnswers[] newArray(int size) {
            return new ExamAnswers[size];
        }
    };

    public String getAnswer_id() {
        return Answer_id;
    }

    public void setAnswer_id(String answer_id) {
        Answer_id = answer_id;
    }

    private String Answer_id;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Answer_id);
    }
}
