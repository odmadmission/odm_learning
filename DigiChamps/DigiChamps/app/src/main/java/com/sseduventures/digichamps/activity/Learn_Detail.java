package com.sseduventures.digichamps.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;


import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.Learn_Detail_Model;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnActivity;
import com.sseduventures.digichamps.adapter.Learn_detail_adapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.config.AppController;
import com.sseduventures.digichamps.domain.LearnChapterlist;
import com.sseduventures.digichamps.domain.LearnDetailsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SessionManager;
import com.sseduventures.digichamps.helper.SpotsDialog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;


public class Learn_Detail extends FormActivity implements ServiceReceiver.Receiver {

    private ArrayList<Learn_Detail_Model> list;
    private SeekBar pb;
    private RecyclerView recyclerView;
    private Learn_detail_adapter mAdapter;
    ImageView subject_img;
    private TextView subject_name;
    private Toolbar toolbar;
    private ImageButton cart_btnn;
    private String resp, error, TAG = Learn_Detail.class.getSimpleName();
    private SessionManager session;
    String sub_name, sub_id, flag;
    long user_id;
    private SpotsDialog pDialog;
    private CoordinatorLayout sub_details_layouts;
    private AppBarLayout app;
    private CollapsingToolbarLayout collapsingToolbar;
    private AppBarLayout appbar_layout;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    ArrayList<LearnChapterlist> mList;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setModuleName(LogEventUtil.EVENT_Learn_details);
        logEvent(LogEventUtil.KEY_Learn_details, LogEventUtil.EVENT_Learn_details);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        Intent intent = getIntent();
        flag = intent.getStringExtra("flag");

        if (flag.equalsIgnoreCase("#3949ab"))

        {
            setTheme(R.style.AppTheme_MathsTheme);

        } else if (flag.equalsIgnoreCase("#ffb74d")) {

            setTheme(R.style.AppTheme_ScienceTheme);
        }

        setContentView(R.layout.activity_learn__detail);
        logEvent(LogEventUtil.KEY_CHAPTER_PAGE,
                LogEventUtil.EVENT_CHAPTER_PAGE);


        sub_name = intent.getStringExtra("subject_name");
        sub_id = intent.getStringExtra("subject_id");

        init();


        try {
            if (sub_name.equalsIgnoreCase("Mathematics")) {
                subject_img.setImageResource(R.drawable.math_subject_icon);

            } else if (sub_name.equalsIgnoreCase("Mathematics (DAV)")) {
                subject_img.setImageResource(R.drawable.math_subject_icon);
            } else if (sub_name.equalsIgnoreCase("Science")) {
                subject_img.setImageResource(R.drawable.sciencelogo);

            } else if (sub_name.equalsIgnoreCase("Science (DAV)")) {
                subject_img.setImageResource(R.drawable.sciencelogo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        subject_name.setTypeface(face);


        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.welcome_backgroundColor));
        drawable.setSize(2, 1);

        mAdapter = new Learn_detail_adapter(list, sub_name, sub_id, this, flag);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(mAdapter);


        if (flag.equalsIgnoreCase("#3949ab")) {
            int colorCodeDarkBlue = Color.parseColor("#1976D2");
            int colorCodeDarkwhite = Color.parseColor("#ffffff");
            appbar_layout.setBackgroundColor(colorCodeDarkBlue);
            toolbar.setBackgroundColor(colorCodeDarkBlue);
            collapsingToolbar.setBackgroundColor(colorCodeDarkBlue);
            pb.getProgressDrawable().setColorFilter(colorCodeDarkwhite, PorterDuff.Mode.MULTIPLY);
            pb.getThumb().setColorFilter(colorCodeDarkwhite, PorterDuff.Mode.SRC_ATOP);


        } else if (flag.equalsIgnoreCase("#ffb74d")) {
            int colorCodeDark = Color.parseColor("#f89b31");
            int colorCodeDarkwhite = Color.parseColor("#ffffff");
            appbar_layout.setBackgroundColor(colorCodeDark);
            toolbar.setBackgroundColor(colorCodeDark);
            collapsingToolbar.setBackgroundColor(colorCodeDark);
            pb.getProgressDrawable().setColorFilter(colorCodeDarkwhite, PorterDuff.Mode.MULTIPLY);
            pb.getThumb().setColorFilter(colorCodeDarkwhite, PorterDuff.Mode.SRC_ATOP);

        }

        if (AppUtil.isInternetConnected(this)) {


            user_id = RegPrefManager.getInstance(this).getRegId();
            getChapDetailsNew(sub_id);


        } else {
            Intent i = new Intent(Learn_Detail.this, Internet_Activity.class);
            startActivity(i);
        }

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }

    public void init() {


        appbar_layout = (AppBarLayout) findViewById(R.id.appbar);
        pb = findViewById(R.id.pb);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);

        session = new SessionManager(this);
        list = new ArrayList<>();
        mList = new ArrayList<>();
        cart_btnn = (ImageButton) findViewById(R.id.cart_btn);

        toolbar = (Toolbar) findViewById(R.id.toolbar_learn_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Learn_Detail.this, LearnActivity.class);
                startActivity(i);
                finish();
            }
        });
        toolbar.setTitle("      CHAPTER DETAILS");


        subject_img = (ImageView) findViewById(R.id.learn_subject_icon);
        subject_name = (TextView) findViewById(R.id.learn_subject_text);
        subject_name.setText(sub_name);

        recyclerView = (RecyclerView) findViewById(R.id.learn_chapter_recyclerview);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("");

        appbar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("      CHAPTER DETAILS");
                    isShow = true;

                    if (flag.equalsIgnoreCase("#3949ab")) {
                        int colorCodeDarkBlue = Color.parseColor("#1976D2");
                        toolbar.setBackgroundColor(colorCodeDarkBlue);

                    }else {
                        int colorCodeDark = Color.parseColor("#f89b31");
                        toolbar.setBackgroundColor(colorCodeDark);
                    }
                   //toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                    toolbar.setBackgroundColor(getResources().getColor(R.color.transparent));
                }
            }
        });

        sub_details_layouts = (CoordinatorLayout) findViewById(R.id.content_learn);


    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getChapDetailsNew(String subId) {
        if (AppUtil.isInternetConnected(this)) {

            Bundle bun = new Bundle();
            bun.putString("subId", subId);

            NetworkService.startActionLearnChapDetailsNew(this, mServiceReceiver, bun);


        } else {
            Intent in = new Intent(Learn_Detail.this, Internet_Activity.class);
            startActivity(in);
        }


    }

    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(Learn_Detail.this, LearnActivity.class);
        startActivity(i);

        finish();

    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);

        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                sub_details_layouts.setVisibility(View.VISIBLE);
                LearnDetailsResponse success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess().getSubjectProgressData() != null
                        ) {


                    final double total = success.getSuccess().getSubjectProgressData().getTotal();
                    pb.setMax(100);
                    pb.setProgress((int) total);
                    pb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            pb.setProgress((int) total);
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {
                            pb.setProgress((int) total);
                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {
                            pb.setProgress((int) total);
                        }
                    });

                }

                if (success != null && success.getSuccess().getChapterlist() != null &&
                        success.getSuccess().getChapterlist().size() > 0) {


                    for (int i = 0; i < success.getSuccess().getChapterlist().size(); i++) {

                        String chapter_id = String.valueOf(success.getSuccess().getChapterlist().get(i).getChapterid());
                        String chapter_name = String.valueOf(success.getSuccess().getChapterlist().get(i).getChapter());
                        String study_notes = String.valueOf(success.getSuccess().getChapterlist().get(i).getTotal_study_notes());
                        String total_video = String.valueOf(success.getSuccess().getChapterlist().get(i).getTotal_videos());
                        String ques = String.valueOf(success.getSuccess().getChapterlist().get(i).getTotal_question_pdf());
                        String CBT = String.valueOf(success.getSuccess().getChapterlist().get(i).getOnline_test());
                        String prt = String.valueOf(success.getSuccess().getChapterlist().get(i).getPre_req_test());
                        String chapImage = success.getSuccess().getChapterlist().get(i).getChapterImage();
                        if (success.getSuccess().getChapterlist().get(i).getChapter() != null &&
                                !success.getSuccess().getChapterlist().get(i).getChapter().equals("null") &&
                                !success.getSuccess().getChapterlist().get(i).getChapter().isEmpty()) {


                            list.add(new Learn_Detail_Model(chapter_id, chapter_name,
                                    total_video + " VIDEOS", study_notes + " SN", ques + " QB", prt + " PRT", CBT + " CBT",
                                    success.getSuccess().getChapterlist().get(i).getTotal_progress(), chapImage));
                            SharedPreferences.Editor editor =
                                    getSharedPreferences("user_data", MODE_PRIVATE).edit();
                            editor.putString("chapterid", chapter_id).apply();
                        }

                    }

                    mAdapter.notifyDataSetChanged();


                }


                break;
            case ResponseCodes.FAILURE:

                Intent inin = new Intent(Learn_Detail.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:

                Intent in = new Intent(Learn_Detail.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:

                Intent intent = new Intent(Learn_Detail.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }
}

