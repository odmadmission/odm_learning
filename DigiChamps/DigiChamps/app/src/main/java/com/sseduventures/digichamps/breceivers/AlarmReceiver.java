package com.sseduventures.digichamps.breceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.sseduventures.digichamps.utils.AlarmManagerUtils;
import com.sseduventures.digichamps.utils.Constants;

public class AlarmReceiver extends BroadcastReceiver
{


    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent!=null&&intent.getAction()!=null)
        {
            if( intent.getAction().equals(Constants.ACTION_SET_ALARM))
            {
               setAlarmManager(context);
            }

            if( intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
            {
                  setAlarmManager(context);
            }


        }
    }

    private void setAlarmManager(Context context)
    {
         AlarmManagerUtils.setSyncAlarmMDM(context,false);
    }
}
