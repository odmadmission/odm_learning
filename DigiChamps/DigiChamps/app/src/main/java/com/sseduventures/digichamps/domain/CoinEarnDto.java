package com.sseduventures.digichamps.domain;

public class CoinEarnDto
{

    private String name;
    private long coins;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCoins() {
        return coins;
    }

    public void setCoins(long coins) {
        this.coins = coins;
    }
}