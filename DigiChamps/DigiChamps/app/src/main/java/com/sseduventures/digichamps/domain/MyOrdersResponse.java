package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class MyOrdersResponse implements Parcelable
{

    private MyOrderSuccess success;

    public MyOrderSuccess getSuccess() {
        return success;
    }

    public void setSuccess(MyOrderSuccess success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.success, flags);
    }

    public MyOrdersResponse() {
    }

    protected MyOrdersResponse(Parcel in) {
        this.success = in.readParcelable(MyOrderSuccess.class.getClassLoader());
    }

    public static final Creator<MyOrdersResponse> CREATOR = new Creator<MyOrdersResponse>() {
        @Override
        public MyOrdersResponse createFromParcel(Parcel source) {
            return new MyOrdersResponse(source);
        }

        @Override
        public MyOrdersResponse[] newArray(int size) {
            return new MyOrdersResponse[size];
        }
    };
}
