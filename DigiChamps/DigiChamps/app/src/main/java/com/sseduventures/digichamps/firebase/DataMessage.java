package com.sseduventures.digichamps.firebase;

public class DataMessage
{
	private int type;
	private Object messageObject;
	private Object notificationObject;
	private long creationTimeStamp;

	public long getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(long creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Object getMessageObject() {
		return messageObject;
	}
	public void setMessageObject(Object messageObject) {
		this.messageObject = messageObject;
	}
	public Object getNotificationObject() {
		return notificationObject;
	}
	public void setNotificationObject(Object notificationObject) {
		this.notificationObject = notificationObject;
	}

	
}
