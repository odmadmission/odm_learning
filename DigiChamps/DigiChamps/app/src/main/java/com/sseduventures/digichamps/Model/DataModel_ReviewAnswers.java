package com.sseduventures.digichamps.Model;

import android.util.Log;

import com.crashlytics.android.answers.Answers;

import java.util.ArrayList;

/**
 * Created by Khushbu on 9/11/2017.
 */

public class DataModel_ReviewAnswers {
    private String Question, Option, OptionID, UserAnswerId,CorrectAnswerId,answers;
    private ArrayList<OptionItem> optionItems;

    public String getUserAnswerId() {
        return UserAnswerId;
    }

    public ArrayList<OptionItem> getOptionItems() {
        return optionItems;
    }

    public void setOptionItems(ArrayList<OptionItem> optionItems) {
        this.optionItems = optionItems;
    }

    public DataModel_ReviewAnswers(String question,
                                   String userAnswerId, String correctAnswerId, ArrayList<OptionItem> optionItems
    ,String answers) {
        Question = question;
        UserAnswerId = userAnswerId;
        CorrectAnswerId = correctAnswerId;
        this.optionItems = optionItems;
        this.answers = answers;

        Log.v(DataModel_ReviewAnswers.class.getSimpleName(),question+"/"+userAnswerId+"/"+
        correctAnswerId+"/"+optionItems.size());
    }

    public String getQuestion() {
        return Question;
    }

    public void setUserAnswerId(String genre3) {
        this.UserAnswerId = genre3;
    }
    public String getCorrectAnswerId() {
        return CorrectAnswerId;
    }

    public String getAnswers() {
        return answers;
    }

    public void setAnswers(String answers) {
        this.answers = answers;
    }
}
