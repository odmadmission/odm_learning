package com.sseduventures.digichamps.firebase;

public class LogEventUtil
{

    public static final String KEY_BASIC_INFO="OnBasicInformationPage";
    public static final String EVENT_BASIC_INFO="Screen_BasicInformationPage";
    public static final String EVENT_Learn_details = "Screen_learn_details";
    public static final String KEY_Learn_details = "learn_details_page";
    public static final String EVENT_topper_way = "Screen_topper_way";
    public static final String KEY_topper_way = "topper_way_page";


    public static final String EVENT_assignment_frag = "Screen_assignment_frag";
    public static final String KEY_assignment_frag = "assignment_frag_page";


    public static final String EVENT_studymaterial = "Screen_studymaterial";
    public static final String KEY_studymaterial = "studymaterial";
    public static final String EVENT_studymaterial_details = "Screen_studymaterial_details";
    public static final String KEY_studymaterial_details = "studymaterial_details";


    public static final String EVENT_Doubt_reply = "Screen_doubt_reply";
    public static final String KEY_Doubt_reply = "doubt_reply";

    public static final String EVENT_Doubt_details = "Screen_doubt_details";
    public static final String KEY_Doubt_details = "doubt_details";



    public static final String EVENT_test_statistics = "Screen_test_statistics";
    public static final String KEY_test_statistics = "test_statistics";


    public static final String EVENT_test_highlights = "Screen_test_highlights";
    public static final String EVENT_Ncert_Page = "Screen_Ncert_page";
    public static final String EVENT_Olympiad_Page = "Screen_Ncert_page";
    public static final String EVENT_Prvs_Page = "Screen_Ncert_page";
    public static final String EVENT_Order = "Screen_order";
    public static final String EVENT_pdf_viewer = "Screen_pdf_viewer";
    public static final String EVENT_Package_details = "Screen_package_details";
    public static final String EVENT_Review_ans = "Screen_review_ans";
    public static final String EVENT_scratch_activity = "Screen_scratch_activity";
    public static final String EVENT_Score_page = "Screen_score_page";
    public static final String KEY_Score_page = "score_page";
    public static final String EVENT_sub_concept_analysis = "Screen_sub_concept_analysis";
    public static final String KEY_Ncert_Page = "Ncert_page";
    public static final String KEY_Order = "order_page";

    public static final String KEY_sub_concept_analysis = "sub_concept_analysis";


    public static final String KEY_Review_ans = "review_ans_page";

    public static final String KEY_DASHBOARD="OnDashboardPage";
    public static final String EVENT_DASHBOARD="Screen_DashboardPage";

    public static final String KEY_BOOKMARKS_VIDEOS_PAGE="OnBookMarks_Videos_Page";
    public static final String EVENT_BOOKMARKS_VIDEOS_PAGE="Screen_BookMarks_Videos_Page";

    public static final String KEY_BOOKMARKS_PAGE="OnBookMarksPage";
    public static final String EVENT_BOOKMARKS_PAGE="Screen_BookMarksPage";

    public static final String KEY_LEARNPAGE="OnLearnPage";
    public static final String EVENT_LEARNPAGE="Screen_LearnPage";

    public static final String KEY_CHAPTER_PAGE="OnChapterPage";
    public static final String EVENT_CHAPTER_PAGE="Screen_ChapterPage";


    public static final String KEY_CHAPTER_DETAILS="OnChapterDetails";
    public static final String EVENT_CHAPTER_DETAILS="Screen_ChapterDetails";


    public static final String KEY_TEST_INST_PAGE="OnTestInstructionPage";
    public static final String EVENT_TEST_INST_PAGE="Screen_TestInstructionPage";
    public static final String Event_Forget_Password="Screen_forget_password";
    public static final String Event_Leaderboard="Screen_leaderboar";
    public static final String Key_Leaderboard="leaderboar_page";



    public static final String KEY_DIY_VIDEOS="OnDiyVideosPage";
    public static final String EVENT_DIY_VIDEOS="Screen_DiyVideosPage";
    public static final String EVENT_Do_It_Yourself="Screen_Do_It_Yourself";

    public static final String KEY_Do_It_Yourself="doityourselfpage";

    public static final String EVENT_LEARN_EARN_PAGE="Screen_learn_earn_page";
    public static final String EVENT_Mentor_details="Screen_mentor_details_page";
    public static final String KEY_LEARN_EARN_PAGE="learn_earn_page";

    public static final String EVENT_NEW_DASHBOARD_PAGE="Screen_new_dashboard_page";
    public static final String EVENT_NEW_EXAM_ACTIVITY="Screen_new_exam_activity";



    public static final String KEY_NEW_EXAM_ACTIVITY="new_exam_activity_page";


    public static final String KEY_RECNT_WATCHED_PAGE="OnRecentlyWatchedPage";
    public static final String EVENT_RECNT_WATCHED_PAGE="Screen_RecentlyWatchedPage";


    public static final String KEY_Previous_Years_QsPAGE="OnPreviousYearsQsPage";
    public static final String EVENT_Previous_Years_Qs_PAGE="Screen_PreviousYearsQsPage";


    public static final String KEY_Olympiads_PAGE="OnOlympiadsPage";
    public static final String EVENT_Olympiads_PAGE="Screen_OlympiadsPage";


    public static final String KEY_Solved_Papers_PAGE="OnSolvedPapersPage";
    public static final String EVENT_Solved_Papers_PAGE="Screen_SolvedPapersPage";


    public static final String KEY_Doubts_Page_PAGE="OnDoubtsPage";
    public static final String EVENT_Doubts_Page_PAGE="Screen_DoubtsPage";


    public static final String EVENT_ORDER_DETAILS="Screen_ORDER_DETAILS";
    public static final String Key_Order_details="ORDER_DETAILS_page";



    public static final String KEY_Doubts_List_PAGE="OnDoubtsListPage";
    public static final String EVENT_Doubts_List_PAGE="Screen_DoubtsListPage";


    public static final String EVENT_FEED_KNOW_MORE_ACTIVITY="Screen_feed_know_more";
    public static final String KEY_FEED_KNOW_MORE_ACTIVITY="feed_know_more_page";



    public static final String KEY_Mentor_Page_PAGE="OnMentorPage";
    public static final String EVENT_Mentor_Page_PAGE="Screen_MentorPage";



    public static final String KEY_ChatwithMentor_PAGE="OnChatwithMentorPage";
    public static final String EVENT_ChatwithMentor_PAGE="Screen_ChatwithMentorPage";



    public static final String KEY_Know_your_Mentor_PAGE="OnKnowYourMentorPage";
    public static final String EVENT_KnowYourMentor_PAGE="Screen_KnowYourMentorPage";



    public static final String KEY_LearnAndEarnGifts_PAGE=
            "OnLearnAndEarnGiftsPage";
    public static final String EVENT_LearnAndEarnGifts_PAGE=
            "Screen_LearnAndEarnGiftsPage";



    public static final String KEY_LearnAndEarnPoints_PAGE="OnLearnAndEarnPointsPage";
    public static final String EVENT_LearnAndEarnPoints_PAGE="Screen_LearnAndEarnPointsPage";



    public static final String KEY_LearnAndEarnLeaderboard_PAGE="OnLearnAndEarnLeaderBoardPage";
    public static final String EVENT_LearnAndEarnLeaderboard_PAGE="Screen_LearnAndEarnLeaderBoardPage";



    public static final String KEY_ShareWithFriends_PAGE="OnShareWithFriendsPage";
    public static final String EVENT_ShareWithFriends_PAGE="Screen_ShareWithFriendsPage";




    public static final String KEY_MentorTest_PAGE="OnMentorTestPage";
    public static final String EVENT_MentorTest_PAGE="Screen_MentorTestPage";



    public static final String KEY_Settings_PAGE="OnSettingsPage";
    public static final String EVENT_Settings_PAGE="Screen_SettingsPage";



    public static final String KEY_ChatWithUs_PAGE="OnChatWithUsPage";
    public static final String EVENT_ChatWithUs_PAGE="Screen_ChatWithUsPage";


    public static final String KEY_EditProfile_PAGE="OnEditProfilePage";
    public static final String EVENT_EditProfile_PAGE="Screen_EditProfilePage";


    public static final String KEY_Dictionary_PAGE="OnDictionaryPage";
    public static final String EVENT_Dictionary_PAGE="Screen_DictionaryPage";



    public static final String KEY_Analytics_PAGE="OnAnalyticsPage";
    public static final String EVENT_Analytics_PAGE="Screen_AnalyticsPage";



    public static final String KEY_Bookmarks_PAGE="OnBookmarks_Fragment_Page";
    public static final String EVENT_Bookmarks_PAGE="Screen_Bookmarks_Fragment_Page";


    public static final String KEY_Feed_PAGE="OnFeedPage";
    public static final String EVENT_Feed_PAGE="Screen_FeedPage";



    public static final String KEY_Notifications_PAGE="OnNotificationsPage";
    public static final String EVENT_Notifications_PAGE="Screen_NotificationsPage";


    public static final String KEY_GiftCard_PAGE="OnGiftCardPage";
    public static final String EVENT_GiftCard_PAGE="Screen_GiftCardPage";


    public static final String KEY_Packages_PAGE="OnPackagesPage";
    public static final String EVENT_Packages_PAGE="Screen_PackagesPage";


    public static final String KEY_Cart_Checkout_PAGE="OnCartPage";
    public static final String EVENT_Cart_Checkout_PAGE="Screen_CartPage";


    public static final String KEY_Checkout_PAGE="OnCheckoutPage";
    public static final String EVENT_Checkout_PAGE="Screen_CheckoutPage";



    public static final String KEY_FinalPayment_PAGE="OnCart_FinalPaymentPage";
    public static final String EVENT_FinalPayment_PAGE="Screen_FinalPaymentPage";




    public static final String KEY_PAYMENT_FAIL_PAGE="OnCart_PaymentFailPage";
    public static final String EVENT_PAYMENT_FAIL_PAGE="Screen_PaymentFailPage";




    public static final String KEY_SchoolZoneCode_PAGE="OnCart_CheckoutPage";
    public static final String EVENT_SchoolZoneCode_PAGE="Screen_Cart_CheckoutPage";




    public static final String KEY_SchoolZoneMain_PAGE="OnSchoolZoneMainPage";
    public static final String EVENT_SchoolZoneMain_PAGE="Screen_SchoolZoneMainPage";



    public static final String KEY_TestAnalytics_PAGE="OnTestAnalyticsPage";
    public static final String EVENT_TestAnalytics_PAGE="Screen_TestAnalyticsPage";



    public static final String KEY_LeaderBoardOfTests_PAGE="OnLeaderboardOfTestsPage";
    public static final String EVENT_LeaderboardOfTests_PAGE="Screen_LeaderboardOfTestsPage";


    public static final String EVENT_FAQ_PAGE="Screen_faq_activity";

    public static final String EVENT_Analysis_details="Screen_analysis_details";
    public static final String EVENT_assigned_teacher="Screen_assigned_teacher";
    public static final String KEY_assigned_teacher="assigned_teacher_page";

    public static final String EVENT_bookmark="Screen_bookmark";
    public static final String KEY_bookmark="Screen_bookmark_page";

    public static final String EVENT_Class_list_activity="Screen_Class_list_activity";
    public static final String KEY_Class_list_activity="Class_list_activity_page";

    public static final String EVENT_daily_tt="Screen_daily_tt";
    public static final String KEY_daily_tt="daily_tt_page";


    public static final String EVENT_discussion_activity="Screen_discussion_activity";
    public static final String KEY_discussion_activity="discussion_activity_page";

    public static final String EVENT_discussion_details_activity="Screen_discussion_activity_details";
    public static final String KEY_discussion_details_activity="discussion_activity_details_page";


    public static final String EVENT_exam_schedule_activity="Screen_exam_schedule_activity";
    public static final String KEY_exam_schedule_activity="exam_schedule_activity_page";


    public static final String EVENT_homework_activity="Screen_homework_activity";
    public static final String KEY_homework_activity="homework_activity_page";

    public static final String KEY_VIDEOS_PAGE="OnChapterVideosPage";
    public static final String EVENT_VIDEOS_PAGE="Screen_ChapterVideosPage";


    public static final String KEY_notice_details="notice_detailsPage";
    public static final String EVENT_notice_details="notice_detailsPage";

    public static final String KEY_notice_activity="notice_activityPage";
    public static final String EVENT_notice_activity="Screen_notice_activity";


    public static final String KEY_Question_Review="notice_Question_Review";
    public static final String EVENT_Question_Review="Screen_Question_Review";

    public static final String KEY_school_info="school_info";
    public static final String EVENT_school_info="Screen_school_info";



    public static final String KEY_difficulty_basis_frag="school_info";
    public static final String EVENT_difficulty_basis_frag="Screen_school_info";
    public static final String KEY_Forum_frag="Forum_frag";
    public static final String EVENT_Forum_frag="Screen_Forum_frag";
    public static final String KEY_bookmark_video="bookmark_video";
    public static final String EVENT_bookmark_video="Screen_bookmark_video";
    public static final String KEY_bookmark_sn="bookmark_sn";
    public static final String EVENT_bookmark_sn="Screen_bookmark_sn";
    public static final String KEY_bookmark_qn="bookmark_qn";
    public static final String EVENT_bookmark_qn="Screen_bookmark_qn";
    public static final String KEY_home_frag="home_frag";
    public static final String EVENT_home_frag="Screen_home_frag";
    public static final String KEY_infor_frag="infor_frag";
    public static final String EVENT_infor_frag="Screen_infor_frag";
    public static final String KEY_pmchat_frag="pmchat_frag";
    public static final String EVENT_pmchat_frag="Screen_pmchat_frag";
    public static final String KEY_schedule_frag="schedule_frag";
    public static final String EVENT_schedule_frag="Screen_schedule_frag";
    public static final String KEY_sc_analysis_frag="sc_analysis_frag";
    public static final String EVENT_sc_analysis_frag="Screen_sc_analysis_frag";
    public static final String KEY_tooper_info="tooper_info";
    public static final String EVENT_tooper_info="Screen_tooper_info";
    public static final String KEY_quesBank_frag="quesBank_frag";
    public static final String EVENT_quesBank_frag="Screen_quesBank_frag";
    public static final String KEY_pm_active_task="pm_active_task";
    public static final String EVENT_pm_active_task="Screen_pm_active_task";
    public static final String KEY_pm_completed_task="pm_completed_task";
    public static final String EVENT_pm_completed_task="Screen_pm_completed_task";
    public static final String KEY_pm_overdue_task="pm_overdue_task";
    public static final String EVENT_pm_overdue_task="Screen_pm_overdue_task";

    public static final String KEY_sign_in="sign_in";
    public static final String EVENT_sign_in="Screen_sign_in";
    public static final String KEY_sign_up="sign_up";
    public static final String EVENT_sign_up="Screen_sign_up";


}
