package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/7/2018.
 */

public class PsychometricQuestion implements Parcelable
{

    private int QuestionId;
    private String Question;
    private String QuestionDesc;
    private String QuestionImage;
    private ArrayList<PsychometricQuestionOption> optionList;

    private int SkippedId;
    private int AnsweredId;
    private int ReviewedId;
    private int sno;

    private int Type;
    private int SubType;

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public int getSkippedId() {
        return SkippedId;
    }

    public void setSkippedId(int skippedId) {
        SkippedId = skippedId;
    }

    public int getAnsweredId() {
        return AnsweredId;
    }

    public void setAnsweredId(int answeredId) {
        AnsweredId = answeredId;
    }

    public int getReviewedId() {
        return ReviewedId;
    }

    public void setReviewedId(int reviewedId) {
        ReviewedId = reviewedId;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public int getSubType() {
        return SubType;
    }

    public void setSubType(int subType) {
        SubType = subType;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getQuestionDesc() {
        return QuestionDesc;
    }

    public void setQuestionDesc(String questionDesc) {
        QuestionDesc = questionDesc;
    }

    public String getQuestionImage() {
        return QuestionImage;
    }

    public void setQuestionImage(String questionImage) {
        QuestionImage = questionImage;
    }

    public ArrayList<PsychometricQuestionOption> getOptionList() {
        return optionList;
    }

    public void setOptionList(ArrayList<PsychometricQuestionOption> optionList) {
        this.optionList = optionList;
    }

    public PsychometricQuestion() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.QuestionId);
        dest.writeString(this.Question);
        dest.writeString(this.QuestionDesc);
        dest.writeString(this.QuestionImage);
        dest.writeTypedList(this.optionList);
        dest.writeInt(this.SkippedId);
        dest.writeInt(this.AnsweredId);
        dest.writeInt(this.ReviewedId);
        dest.writeInt(this.sno);
        dest.writeInt(this.Type);
        dest.writeInt(this.SubType);
    }

    protected PsychometricQuestion(Parcel in) {
        this.QuestionId = in.readInt();
        this.Question = in.readString();
        this.QuestionDesc = in.readString();
        this.QuestionImage = in.readString();
        this.optionList = in.createTypedArrayList(PsychometricQuestionOption.CREATOR);
        this.SkippedId = in.readInt();
        this.AnsweredId = in.readInt();
        this.ReviewedId = in.readInt();
        this.sno = in.readInt();
        this.Type = in.readInt();
        this.SubType = in.readInt();
    }

    public static final Creator<PsychometricQuestion> CREATOR = new Creator<PsychometricQuestion>() {
        @Override
        public PsychometricQuestion createFromParcel(Parcel source) {
            return new PsychometricQuestion(source);
        }

        @Override
        public PsychometricQuestion[] newArray(int size) {
            return new PsychometricQuestion[size];
        }
    };
}
