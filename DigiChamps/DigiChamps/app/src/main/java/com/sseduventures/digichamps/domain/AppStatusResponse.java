package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/20/2018.
 */

public class AppStatusResponse implements Parcelable{


    private int resId;
    private String name;
    private String data;
    private boolean active;
    private int status;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.resId);
        dest.writeString(this.name);
        dest.writeString(this.data);
        dest.writeByte(this.active ? (byte) 1 : (byte) 0);
        dest.writeInt(this.status);
    }

    public AppStatusResponse() {
    }

    protected AppStatusResponse(Parcel in) {
        this.resId = in.readInt();
        this.name = in.readString();
        this.data = in.readString();
        this.active = in.readByte() != 0;
        this.status = in.readInt();
    }

    public static final Creator<AppStatusResponse> CREATOR = new Creator<AppStatusResponse>() {
        @Override
        public AppStatusResponse createFromParcel(Parcel source) {
            return new AppStatusResponse(source);
        }

        @Override
        public AppStatusResponse[] newArray(int size) {
            return new AppStatusResponse[size];
        }
    };
}
