package com.sseduventures.digichamps.Model;

/**
 * Created by Tech_1 on 1/6/2018.
 */

public class PrevYr_Model {
    private String PreviousYearPaper_ID,ClassName,PreviousYearPaper_PDFName,PDF_UploadPath;


    public PrevYr_Model( String PreviousYearPaper_ID,String PreviousYearPaper_PDFName, String ClassName,
                              String PDF_UploadPath) {

            this.PreviousYearPaper_PDFName = PreviousYearPaper_PDFName;
            this.ClassName = ClassName;
            this.PDF_UploadPath = PDF_UploadPath;
            this.PreviousYearPaper_ID=PreviousYearPaper_ID;
        }

        public String getPreviousYearPaper_ID() {
            return PreviousYearPaper_ID;
        }

        public String getPreviousYearPaper_PDFName() {
            return PreviousYearPaper_PDFName;
        }

        public String getClassName() {
            return ClassName;
        }

        public String getPDF_UploadPath() {
            return PDF_UploadPath;
        }


}



