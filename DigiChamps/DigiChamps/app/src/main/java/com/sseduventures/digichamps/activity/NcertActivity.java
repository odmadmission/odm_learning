package com.sseduventures.digichamps.activity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.NCERT_Model;


import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.adapter.NCERT_Adapter;

import com.sseduventures.digichamps.domain.NcertSolutionListData;
import com.sseduventures.digichamps.domain.NcertSolutionResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SessionManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.R;

import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class NcertActivity extends FormActivity implements ServiceReceiver.Receiver {

    private ArrayList<NCERT_Model> list;
    private ArrayList<NcertSolutionListData> newList;
    private RecyclerView recyclerView;
    private NCERT_Adapter mAdapter;


    private Toolbar toolbar;
    private ArrayList<String> pdfs_list;
    private String resp, error, TAG = "Learn-Detail", Boardid;
    int classid;
    private SessionManager session;
    String sub_name, sub_id, flag, user_id;
    private SpotsDialog dialog;
    private CoordinatorLayout sub_details_layouts;
    private AppBarLayout app;
    CollapsingToolbarLayout collapsingToolbar;
    EditText inputSearch;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private NcertSolutionResponse success;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setModuleName(LogEventUtil.EVENT_Ncert_Page);
        setContentView(R.layout.activity_ncert);
        logEvent(LogEventUtil.KEY_Ncert_Page,LogEventUtil.EVENT_Ncert_Page);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        init();


        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.welcome_backgroundColor));
        drawable.setSize(2, 1);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);


        if (AppUtil.isInternetConnected(this)) {
            getNcertPapers();

        } else {
            startActivity(new Intent(NcertActivity.this, Internet_Activity.class));

            finish();
        }
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

    }

    public void ReturnHome(View view) {
        Intent in = new Intent(NcertActivity.this, NewDashboardActivity.class);
        startActivity(in);
        finish();
    }

    private void filter(String text) {
        ArrayList<NcertSolutionListData> filteredList = new ArrayList<>();

        for (NcertSolutionListData item : newList) {
            if (item.getNCERTSolutions_PDFName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        mAdapter.filterList(filteredList);
    }


    public void init() {


        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        SharedPreferences preference = getApplicationContext().getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        flag = preference.getString("flag", null);
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        dialog = new SpotsDialog(this, "Fun+Education", R.style.Custom);
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        session = new SessionManager(this);

        list = new ArrayList<>();
        newList = new ArrayList<>();

        toolbar = (Toolbar) findViewById(R.id.toolbar_learn_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("NCERT Solutions");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(NcertActivity.this, LearnActivity.class);
                startActivity(in);
                finish();
            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.ncert_recycler);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_laern_details);
        collapsingToolbar.setTitle("NCERT Solutions");

        sub_details_layouts = (CoordinatorLayout) findViewById(R.id.content_learn);
        app = (AppBarLayout) findViewById(R.id.appbar);

        pdfs_list = new ArrayList<>();


        mAdapter = new NCERT_Adapter(newList, "", NcertActivity.this);
        recyclerView.setAdapter(mAdapter);

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getNcertPapers() {
        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionGetNcertSolution(this, mServiceReceiver);
        } else {
            Intent in = new Intent(NcertActivity.this, Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        hideDialog();
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);
                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getList() != null
                        && success.getSuccess().getList().size() > 0) {


                    if (success.getSuccess().getList().size() == 0) {

                    } else {

                    }

                    newList.clear();
                    newList.addAll(success.getSuccess().getList());
                    mAdapter.notifyDataSetChanged();

                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(NcertActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(NcertActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(NcertActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }

    @Override
    public void onSupportActionModeStarted(@NonNull ActionMode mode) {
        super.onSupportActionModeStarted(mode);
    }



    @Override
    public void onBackPressed() {
        Intent in = new Intent(NcertActivity.this, LearnActivity.class);
        startActivity(in);
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}

