package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class HomeworkList implements Parcelable{

    private String HomeworkId;
    private String subject;
    private String homework;
    private String homeWorkDate;

    public String getHomeworkId() {
        return HomeworkId;
    }

    public void setHomeworkId(String homeworkId) {
        HomeworkId = homeworkId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public String getHomeWorkDate() {
        return homeWorkDate;
    }

    public void setHomeWorkDate(String homeWorkDate) {
        this.homeWorkDate = homeWorkDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.HomeworkId);
        dest.writeString(this.subject);
        dest.writeString(this.homework);
        dest.writeString(this.homeWorkDate);
    }

    public HomeworkList() {
    }

    protected HomeworkList(Parcel in) {
        this.HomeworkId = in.readString();
        this.subject = in.readString();
        this.homework = in.readString();
        this.homeWorkDate = in.readString();
    }

    public static final Creator<HomeworkList> CREATOR = new Creator<HomeworkList>() {
        @Override
        public HomeworkList createFromParcel(Parcel source) {
            return new HomeworkList(source);
        }

        @Override
        public HomeworkList[] newArray(int size) {
            return new HomeworkList[size];
        }
    };
}
