package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewAssignTeacher;
import com.sseduventures.digichamps.domain.AssignedTeacherDetails;
import com.sseduventures.digichamps.domain.AssignedTeacherSuccess;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AssignedTeachersActivity extends FormActivity
        implements ServiceReceiver.Receiver {

    RecyclerView recyclerview;
    Context mContext;
    CardviewAssignTeacher mCardviewAssignTeacher;
    ArrayList<AssignedTeacherDetails> mList;
    TextView tv_at;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private AssignedTeacherSuccess success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assigned_teachers);

        setModuleName(LogEventUtil.EVENT_assigned_teacher);
        logEvent(LogEventUtil.KEY_assigned_teacher,LogEventUtil.EVENT_assigned_teacher);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        mContext = this;
        tv_at =  findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext,tv_at);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        mList=new ArrayList<>();
        mCardviewAssignTeacher = new CardviewAssignTeacher(mList,mContext);
        recyclerview.setAdapter(mCardviewAssignTeacher);
        getAssignedTeacher();


    }



    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle("DIGICHAMPS")
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private void getAssignedTeacher() {
        if(AppUtil.isInternetConnected(this)){


            NetworkService.startActionPostAssignedTeacherDetails(AssignedTeachersActivity.this
                    ,mServiceReceiver);

        }else{
            Intent in = new Intent(AssignedTeachersActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(AssignedTeachersActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(AssignedTeachersActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(AssignedTeachersActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);


                if(success == null && success.getResultCount() == 0
                        ){
                    AskOptionDialog("No Teacher Found").show();
                }else{

                    if(success.getAssignDetailList()!=null
                            &&success.getAssignDetailList().size()>0){

                        mList.clear();
                        mList.addAll(success.getAssignDetailList());
                        mCardviewAssignTeacher.notifyDataSetChanged();

                    }else{
                        AskOptionDialog("No Teacher Found").show();
                    }
                }

                break;

        }
    }

}
