package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 8/31/2018.
 */

public class DoubtReplyResponse implements Parcelable{

    private DoubtReplySuccess success;

    public DoubtReplySuccess getSuccess() {
        return success;
    }

    public void setSuccess(DoubtReplySuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.success, flags);
    }

    public DoubtReplyResponse() {
    }

    protected DoubtReplyResponse(Parcel in) {
        this.success = in.readParcelable(DoubtReplySuccess.class.getClassLoader());
    }

    public static final Creator<DoubtReplyResponse> CREATOR = new Creator<DoubtReplyResponse>() {
        @Override
        public DoubtReplyResponse createFromParcel(Parcel source) {
            return new DoubtReplyResponse(source);
        }

        @Override
        public DoubtReplyResponse[] newArray(int size) {
            return new DoubtReplyResponse[size];
        }
    };
}
