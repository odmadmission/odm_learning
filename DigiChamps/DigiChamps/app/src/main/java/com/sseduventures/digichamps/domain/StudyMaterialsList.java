package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class StudyMaterialsList implements Parcelable {

    private String createdDate;
    private String fileType;
    private String fileURL;
    private String studyMaterial;
    private String topic;
    private String materialType;
    private String subject;

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getStudyMaterial() {
        return studyMaterial;
    }

    public void setStudyMaterial(String studyMaterial) {
        this.studyMaterial = studyMaterial;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMaterialType() {
        return materialType;
    }

    public void setMaterialType(String materialType) {
        this.materialType = materialType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.createdDate);
        dest.writeString(this.fileType);
        dest.writeString(this.fileURL);
        dest.writeString(this.studyMaterial);
        dest.writeString(this.topic);
        dest.writeString(this.materialType);
        dest.writeString(this.subject);
    }

    public StudyMaterialsList() {
    }

    protected StudyMaterialsList(Parcel in) {
        this.createdDate = in.readString();
        this.fileType = in.readString();
        this.fileURL = in.readString();
        this.studyMaterial = in.readString();
        this.topic = in.readString();
        this.materialType = in.readString();
        this.subject = in.readString();
    }

    public static final Creator<StudyMaterialsList> CREATOR = new Creator<StudyMaterialsList>() {
        @Override
        public StudyMaterialsList createFromParcel(Parcel source) {
            return new StudyMaterialsList(source);
        }

        @Override
        public StudyMaterialsList[] newArray(int size) {
            return new StudyMaterialsList[size];
        }
    };
}
