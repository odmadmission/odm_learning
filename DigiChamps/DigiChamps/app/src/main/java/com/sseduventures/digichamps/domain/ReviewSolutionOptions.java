package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/5/2018.
 */

public class ReviewSolutionOptions implements Parcelable{

    private String Option;

    private String Image;
    private int OptionID;

    public String getOption() {
        return Option;
    }

    public void setOption(String option) {
        Option = option;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getOptionID() {
        return OptionID;
    }

    public void setOptionID(int optionID) {
        OptionID = optionID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Option);
        dest.writeString(this.Image);
        dest.writeInt(this.OptionID);
    }

    public ReviewSolutionOptions() {
    }

    protected ReviewSolutionOptions(Parcel in) {
        this.Option = in.readString();
        this.Image = in.readString();
        this.OptionID = in.readInt();
    }

    public static final Creator<ReviewSolutionOptions> CREATOR = new Creator<ReviewSolutionOptions>() {
        @Override
        public ReviewSolutionOptions createFromParcel(Parcel source) {
            return new ReviewSolutionOptions(source);
        }

        @Override
        public ReviewSolutionOptions[] newArray(int size) {
            return new ReviewSolutionOptions[size];
        }
    };
}
