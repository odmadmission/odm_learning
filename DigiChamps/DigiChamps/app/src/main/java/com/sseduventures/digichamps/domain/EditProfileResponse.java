package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/26/2018.
 */

public class EditProfileResponse implements Parcelable {

     private EditProfileSuccess success;


    public EditProfileSuccess getSuccess() {
        return success;
    }

    public void setSuccess(EditProfileSuccess success) {
        this.success = success;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.success, flags);
    }

    public EditProfileResponse() {
    }

    protected EditProfileResponse(Parcel in) {
        this.success = in.readParcelable(EditProfileSuccess.class.getClassLoader());
    }

    public static final Creator<EditProfileResponse> CREATOR = new Creator<EditProfileResponse>() {
        @Override
        public EditProfileResponse createFromParcel(Parcel source) {
            return new EditProfileResponse(source);
        }

        @Override
        public EditProfileResponse[] newArray(int size) {
            return new EditProfileResponse[size];
        }
    };
}
