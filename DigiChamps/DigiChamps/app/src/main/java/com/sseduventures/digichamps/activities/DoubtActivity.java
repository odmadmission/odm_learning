package com.sseduventures.digichamps.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.FullImageActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.adapter.Doubt_listview_DialogAdapter;
import com.sseduventures.digichamps.domain.SubjectResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.PickerBuilder;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.BitmapUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Profile_utility;
import com.sseduventures.digichamps.utils.Utility;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.DoubtSubmitModel;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;

public class DoubtActivity extends FormActivity implements ServiceReceiver.Receiver ,ActivityCompat.OnRequestPermissionsResultCallback{

    private CollapsingToolbarLayout collapsing_toolbar;
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private Button post_btn;
    private TextInputLayout doubt;
    private TextInputEditText doubt_editText;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TextView select_chapter, select_subject;
    private ArrayList<SubjectResponse> success;
    private String userChoosenTask;

    private TextView doubt_list;

    private TextView subjects, chapter_name, attachment;
    private String nameClass;
    private String user_id;
    private RegPrefManager regPrefManager;

    private ImageView showImg;

    private String boardID, classID, subjectID, chapterID, path;
    private String chapterdetails;
    private TextView delete;
    private boolean preview = false;
    private Uri newPath;

    int height;
    int width;
    File photofile;
    File compressedImageFile = null;

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;


    private ArrayList<Basic_info_model> subjectListNew, chapterListNew;

    private Doubt_listview_DialogAdapter boardAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        logEvent(LogEventUtil.KEY_Doubts_Page_PAGE,
                LogEventUtil.EVENT_Doubts_Page_PAGE);
        setModuleName(LogEventUtil.EVENT_Doubts_Page_PAGE);
        setContentView(R.layout.activity_doubts);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);


        init();

        if (AppUtil.isInternetConnected(this)) {
            getSubject();
        } else {

            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }

    private void init() {


        post_btn = findViewById(R.id.post_btn);
        select_chapter = findViewById(R.id.select_chapter);
        select_subject = findViewById(R.id.select_subject);
        doubt = findViewById(R.id.doubt);
        subjects = findViewById(R.id.subjects);
        showImg = (ImageView) findViewById(R.id.image_upload);
        chapter_name = findViewById(R.id.chapter_name);
        attachment = findViewById(R.id.attachment);
        delete = findViewById(R.id.delete);
        delete.setVisibility(View.GONE);
        regPrefManager = RegPrefManager.getInstance(this);
        doubt_editText = findViewById(R.id.doubt_editText);
        doubt_list = findViewById(R.id.doubt_list);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        DoubtActivity.this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        subjectListNew = new ArrayList<>();
        chapterListNew = new ArrayList<>();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        /*toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoubtActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });*/
        select_subject.setText("Select");
        select_chapter.setText("Select");
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attachment.setText("Post doubt with an image");
                showImg.setImageDrawable(null);
                showImg.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
                preview = false;
                compressedImageFile = null;
                photofile = null;
                newPath = null;
            }
        });

        if (getIntent().getStringExtra("chapterdetails") != null) {
            chapterdetails = getIntent().getStringExtra("chapterdetails");
        } else {
            chapterdetails = "";
        }
        boardID = regPrefManager.getKeyBoardId();
        classID = String.valueOf(regPrefManager.getclassid());

        user_id = String.valueOf(regPrefManager.getRegId());

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        appBarLayout = findViewById(R.id.appbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Ask Doubts");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        doubt_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtil.isInternetConnected(DoubtActivity.this)) {
                    Intent intent = new Intent(DoubtActivity.this, DoubtListActivity.class);
                    startActivity(intent);
                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        post_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                
                if (RegPrefManager.getInstance(DoubtActivity.this).getIsDoubt()) {
                    if (AppUtil.isInternetConnected(DoubtActivity.this)) {

                        if (validation()) {

                            submitform();

                            // submitDoubtForm(boardID, classID, subjectID, chapterID, doubt_editText.getText().toString().trim());

                        }
                    } else {

                        AskOptionDialogNew("Please Check Your Internet Connection").show();
                    }
                } else {
                    //  showToast(RegPrefManager.getInstance(DoubtActivity.this).getDoubtCount()+"");


                    if (RegPrefManager.getInstance(DoubtActivity.this).
                            getDoubtCount() < 5) {
                        if (AppUtil.isInternetConnected(DoubtActivity.this)) {

                            if (validation()) {

                                submitform();



                            }
                        } else {

                            AskOptionDialogNew("Please Check Your Internet Connection").show();
                        }
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(DoubtActivity.this);
                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View viewnew = layoutInflater.inflate(R.layout.doubt_dialog, null);
                        builder.setView(viewnew);


                        final AlertDialog dialog1 = builder.create();
                        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        TextView headerw = (TextView) viewnew.findViewById(R.id.text);
                        TextView text = (TextView) viewnew.findViewById(R.id.subscribe_new);
                        TextView goBack = (TextView) viewnew.findViewById(R.id.goBack);
                        headerw.setText("You've asked five doubts already.To ask unlimited doubts 24X7");


                        goBack.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                dialog1.dismiss();
                                finish();

                            }
                        });
                        text.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent in = new Intent(DoubtActivity.this, PackageActivityNew.class);
                                startActivity(in);


                            }
                        });
                        dialog1.show();
                        dialog1.setCanceledOnTouchOutside(false);



                        Window window = dialog1.getWindow();
                        WindowManager.LayoutParams wlp = window.getAttributes();

                        window.setAttributes(wlp);

                    }
                }


            }
        });

        doubt_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (doubt.getError() != null) {
                    doubt.setError("");
                    doubt.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        select_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtil.isInternetConnected(DoubtActivity.this)) {

                    chapter_name.setVisibility(View.GONE);
                    chapter_name.setText("");
                    subjectDialog();
                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });

        select_chapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AppUtil.isInternetConnected(DoubtActivity.this)) {

                    if (board != null) {
                        dialogChapterNew();
                    } else {
                        AskOptionDialogNew("Please select your Subject First").show();
                        //Toast.makeText(this, "Please select your board", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }

            }
        });

        attachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppUtil.isInternetConnected(DoubtActivity.this)) {


                    if (attachment.getText().toString().equals("Preview")) {
                        Intent intent = new Intent(DoubtActivity.this, FullImageActivity.class);
                        intent.putExtra("url", compressedImageFile);
                        intent.putExtra("activity", "FromDoubt");
                        startActivity(intent);
                    } else {
                        checkPermission(DoubtActivity.this);


                    }


                } else {

                    AskOptionDialogNew("Please Check Your Internet Connection").show();
                }
            }
        });
    }


    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(DoubtActivity.this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    private void submitform() {

        String doubtDetails = doubt_editText.getText().toString().trim();
        Bundle bundle = new Bundle();
        bundle.putString("Regd_ID", user_id);
        bundle.putString("Board_id", boardID);
        bundle.putString("Class_id", classID);
        bundle.putString("Subject_ID", subjectID);
        bundle.putString("Chapter_ID", chapterID);
        bundle.putString("Question_Detail", doubtDetails);
        if (compressedImageFile != null) {
            bundle.putString("Image", compressedImageFile.toString());
        }

        if (AppUtil.isInternetConnected(this)) {

            showDialog(null, "please wait");
            NetworkService.startActionSubmitDoubt(this,
                    mServiceReceiver, bundle);

        } else {
            // Toast.makeText(mContext, "Please check your Internet Connection", Toast.LENGTH_SHORT).show();
            AskOptionDialogNew("Please Check Your Internet Connection").show();
        }
    }



    @Override
    public void onBackPressed() {

        finish();

    }


    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};

        AlertDialog.Builder builder = new AlertDialog.Builder(DoubtActivity.this);
        //builder.setTitle("Add Photo!");
        builder.setTitle(Html.fromHtml("<font color='#05ab9a'>Add Photo</font>"));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Profile_utility.checkPermission(DoubtActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result) {
                        new PickerBuilder(DoubtActivity.this, PickerBuilder.SELECT_FROM_CAMERA)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {


                                            path = imageUri.getPath();
                                            newPath = null;
                                            newPath = imageUri;
                                            photofile = null;
                                            photofile = new File(new URI(newPath.toString()));
                                            compressedImageFile = null;
                                            compressedImageFile = BitmapUtil.compressImage(
                                                    DoubtActivity.this,
                                                    photofile,
                                                    height,
                                                    width);
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(DoubtActivity.this,
                                                    imageUri);
                                            showImg.setVisibility(View.GONE);
                                            delete.setVisibility(View.VISIBLE);
                                            attachment.setText("Preview");
                                            showImg.setBackgroundResource(0);
                                            showImg.setImageBitmap(bitmap);
                                            preview = true;

                                        } catch (Exception e) {
                                            Toast.makeText(DoubtActivity.this, "Please insert photo again",
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .withTimeStamp(false)
                                .setCropScreenColor(Color.GREEN)
                                .start();
                    }
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result) {
                        new PickerBuilder(DoubtActivity.this, PickerBuilder.SELECT_FROM_GALLERY)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {

                                            path = imageUri.getPath();
                                            newPath = null;
                                            newPath = imageUri;
                                            photofile = null;
                                            photofile = new File(new URI(newPath.toString()));
                                            compressedImageFile = null;
                                            compressedImageFile = BitmapUtil.compressImage(
                                                    DoubtActivity.this,
                                                    photofile,
                                                    height,
                                                    width);
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(DoubtActivity.this, imageUri);
                                            showImg.setVisibility(View.GONE);
                                            delete.setVisibility(View.VISIBLE);
                                            attachment.setText("Preview");
                                            showImg.setBackgroundResource(0);
                                            showImg.setImageBitmap(bitmap);
                                            preview = true;
                                        } catch (Exception e) {
                                            Toast.makeText(DoubtActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setCropScreenColor(Color.GREEN)
                                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                    @Override
                                    public void onPermissionRefused() {
                                        Toast.makeText(DoubtActivity.this, "This permision is necessary", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .start();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }



    private void prepareClassName(String className) {
        chapter_name.setVisibility(View.VISIBLE);
        chapter_name.setText(className);
        select_chapter.setText("Change");
    }


    private void getSubject() {

        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please Wait..");
            NetworkService.startActionGetSubject(this, mServiceReceiver);

        } else {
            showToast("No Internet Connection");
        }

    }

    private Boolean validation() {


        if (subjects.getText().toString().trim().matches("")) {

            AskOptionDialogNew("Please select your subject first").show();
            return false;
        }

        if (chapter_name.getText().toString().trim().matches("")) {

            AskOptionDialogNew("Please select your chapter first").show();
            return false;
        }


        if (doubt_editText.getText().toString().trim().isEmpty()) {

            AskOptionDialogNew("Please write the details about your doubt").show();

            /*doubt.setErrorEnabled(true);
            doubt.setError("Please Enter Your Doubt");*/
            return false;
        }

        return true;
    }


    private String[] subjectNames = null;
    private String[] subjectid = null;
    private String[] chapterid = null;

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                hideDialog();
                Intent in = new Intent(DoubtActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                hideDialog();
                Intent intent = new Intent(DoubtActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                hideDialog();
                Intent inin = new Intent(DoubtActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                // hideDialog();
                hideDialog();
                success = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                //showToast(success.size()+"");
                if (success != null && success.size() > 0) {

                    createBoardNames();


                }

                break;

            case ResponseCodes.STATUS:
                hideDialog();

                final DoubtSubmitModel doubtSubmitModel = resultData.getParcelable(
                        IntentHelper.RESULT_DATA);

                if (doubtSubmitModel != null) {


                    RegPrefManager.getInstance(this)
                            .setDoubtCount(RegPrefManager.
                                    getInstance(this).getDoubtCount() + 1);

                    // showToast(RegPrefManager.
                    //       getInstance(this).getDoubtCount()+"");
                    new AlertDialog.Builder(DoubtActivity.this)

                            .setTitle("Doubt Submitted")
                            .setMessage("Doubt submitted successfully")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String message = null;

                                    try {
                                        message = doubtSubmitModel.getSuccess().getMessage();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    dialog.dismiss();

                                    chapter_name.setText(null);
                                    subjects.setText(null);
                                    chapter_name.setVisibility(View.GONE);
                                    subjects.setVisibility(View.GONE);
                                    doubt_editText.setText(null);
                                    select_chapter.setText("Select");
                                    select_subject.setText("Select");
                                    delete.setVisibility(View.GONE);
                                    attachment.setText("Post doubt with an image");
                                    path = "";
                                    compressedImageFile = null;
                                    photofile = null;
                                    newPath = null;


                                }
                            })
                            .setNegativeButton("", null).show();


                } else {
                    AskOptionDialogNew("Something went wrong...Please try again");

                }

                break;


        }
    }




    public void subjectDialog() {
        final Dialog dialog = new Dialog(DoubtActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);



        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);
        select_tv.setText("Select Subject");
        select_tv.setTextColor(Color.parseColor("#519B9C"));
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        ok_but.setTextColor(Color.parseColor("#519B9C"));
        boardAdapter = new Doubt_listview_DialogAdapter(subjectListNew, getApplicationContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                subjectListNew.get(position).setFlag(true);
                for (int i = 0; i < subjectListNew.size(); i++) {
                    if (position != i)
                        subjectListNew.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        board = basic_info_model.getName();
                        createClassList();
                        //counter=counter+1;
                        prepareBoardName(board);

                    }
                }


                dialog.dismiss();

            }
        });

        dialog.show();
    }


    public void dialogChapterNew() {
        final Dialog dialog = new Dialog(DoubtActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.chapter_cstm_dialog_list);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button


        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        select_tv.setText("Select Chapter");
        select_tv.setTextColor(Color.parseColor("#519B9C"));
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);

        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        ok_but.setTextColor(Color.parseColor("#519B9C"));
        boardAdapter = new Doubt_listview_DialogAdapter(chapterListNew, getApplicationContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                // String selectedItem = board_list.get(position);

                // Display the selected item text on TextView
                //   prepareBoardName(selectedItem);
                //   dialog.dismiss();
                chapterListNew.get(position).setFlag(true);
                chapterID = chapterListNew.get(position).getChapid();
                for (int i = 0; i < chapterListNew.size(); i++) {
                    if (position != i)
                        chapterListNew.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        board = basic_info_model.getName();
                        String chapterName = basic_info_model.getName();
                        //createClassList();
                        //counter=counter+1;
                        prepareClassName(chapterName);

                    }
                }


                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private String board;
    private String[] chapterNames = null;


    private void prepareBoardName(String boardname) {
        subjects.setVisibility(View.VISIBLE);
        subjects.setText(boardname);
        select_subject.setText("Change");
    }

    private void createBoardNames() {
        subjectNames = new String[success.size()];

        String subName = null;
        for (int i = 0; i < success.size(); i++) {


            subjectNames[i] = success.get(i).getSubName();
            Basic_info_model basic_info_model = new Basic_info_model();
            basic_info_model.setName(success.get(i).getSubName());
            Intent in = getIntent();

            String subIdId;
            int subIntId = 0;
            if (in != null) {
                subIdId = in.getStringExtra("subjectid");

                // String subIdId = getIntent().getStringExtra("subjectid");

                if (subIdId != null) {

                    subIntId = Integer.parseInt(subIdId);
                }

                if (success.get(i).getSubId() == subIntId) {
                    basic_info_model.setFlag(true);
                    subName = success.get(i).getSubName();
                }


            } else {

            }


            subjectListNew.add(basic_info_model);

        }


        if (subName != null) {
            board = subName;
            prepareBoardName(subName);
            createClassList();
        }

        // showToast(subjectNames.length + "");
    }


    private void createClassList() {
        for (int i = 0; i < success.size(); i++) {

            if (board != null && board.equalsIgnoreCase(success.get(i).getSubName())) {
                chapterNames = new String[success.get(i).getChapList().size()];
                subjectid = new String[success.get(i).getChapList().size()];
                chapterid = new String[success.get(i).getChapList().size()];

                chapterListNew.clear();
                String chapNameNew = null;
                for (int j = 0; j < success.get(i).getChapList().size(); j++) {
                    chapterNames[j] = success.get(i).getChapList().get(j).getChapName();
                    subjectid[j] = String.valueOf(success.get(i).getChapList().get(j).getSubId());
                    chapterid[j] = String.valueOf(success.get(i).getChapList().get(j).getChapId());
                    Basic_info_model basic_info_model = new Basic_info_model();

                    basic_info_model.setChapid(String.valueOf(success.get(i).getChapList().get(j).getChapId()));

                    if (board == null) {
                        basic_info_model.setName(success.get(i).getChapList().get(j).getChapName());
                        basic_info_model.setSubid(String.valueOf(success.get(i).getChapList().get(j).getSubId()));
                        subjectID = String.valueOf(success.get(i).getChapList().get(j).getSubId());
                    } else {
                        if (board.equals(success.get(i).getChapList().get(j).getChapName())) {
                            basic_info_model.setSubid(board);

                            basic_info_model.setFlag(true);
                            basic_info_model.setSubid(String.valueOf(success.get(i).getChapList().get(j).getSubId()));
                            subjectID = String.valueOf(success.get(i).getChapList().get(j).getSubId());
                            basic_info_model.setName(success.get(i).getChapList().get(j).getChapName());
                        } else {
                            basic_info_model.setSubid(board);
                            basic_info_model.setFlag(false);
                            basic_info_model.setSubid(String.valueOf(success.get(i).getChapList().get(j).getSubId()));
                            subjectID = String.valueOf(success.get(i).getChapList().get(j).getSubId());
                            basic_info_model.setName(success.get(i).getChapList().get(j).getChapName());
                        }
                    }


                    Intent in = getIntent();
                    int chapIdInt = 0;

                    if (in != null) {
                        String chapIdId = in.getStringExtra("chapId");
                        if (chapIdId != null) {

                            chapIdInt = Integer.parseInt(chapIdId);
                        }

                        if (success.get(i).getChapList().get(j).getChapId() == chapIdInt) {
                            basic_info_model.setFlag(true);
                            chapNameNew = success.get(i).getChapList().get(j).getChapName();
                            chapterID = String.valueOf(success.get(i).getChapList().get(j).getChapId());
                        }

                    } else {

                    }


                    chapterListNew.add(basic_info_model);
                }

                if (chapNameNew != null) {
                    prepareClassName(chapNameNew);
                }
            }

        }
    }

    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                selectImage();
                return true;
            }
        } else {
            selectImage();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

            //resume tasks needing this permission
            selectImage();
        }


    }
}

