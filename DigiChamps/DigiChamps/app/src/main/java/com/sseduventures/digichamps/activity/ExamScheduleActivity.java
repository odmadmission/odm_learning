package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewDiscussionDetail;
import com.sseduventures.digichamps.adapter.CardviewExamSchedule;
import com.sseduventures.digichamps.domain.ExamScheduleListNew;
import com.sseduventures.digichamps.domain.ExamScheduleResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.FeedBackFormModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.EXAM_SCHEDULE;

public class ExamScheduleActivity extends FormActivity implements
        ServiceReceiver.Receiver {

    RecyclerView recyclerview;
    CardviewExamSchedule mCardviewExamSchedule;
    Context mContext;
    ArrayList<ExamScheduleListNew> mList;
    ArrayList<ExamScheduleListNew> mSortedList = new ArrayList<>();
    String schoolid;
    int classid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ExamScheduleResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_schedule);

        setModuleName(LogEventUtil.EVENT_exam_schedule_activity);
        logEvent(LogEventUtil.KEY_exam_schedule_activity,LogEventUtil.EVENT_exam_schedule_activity);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int)RegPrefManager.getInstance(this).getclassid();


        mList = new ArrayList<ExamScheduleListNew>();
        mSortedList = new ArrayList<ExamScheduleListNew>();

        mContext = this;
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView tv_at = findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext, tv_at);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        getExamScheduleNew();


    }


    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    public void checkList() {
        if (mList != null && mList.size() > 0) {
            for (int i = 0; i < mList.size(); i++) {
                sortedList(mList.get(i));
            }
            mCardviewExamSchedule = new CardviewExamSchedule(mSortedList, mContext);
            recyclerview.setAdapter(mCardviewExamSchedule);
        }
    }

    public void sortedList(ExamScheduleListNew getList) {
        Boolean isExist = false;
        if (mSortedList != null && mSortedList.size() > 0) {
            for (int i = 0; i < mSortedList.size(); i++) {
                if (mSortedList.get(i).getExamName().equals(getList.getExamName())) {
                    isExist = true;
                }
            }
            if (!isExist) {
                mSortedList.add(getList);
            }
        } else {
            mSortedList.add(getList);
        }
    }


    private void getExamScheduleNew() {
        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionPostExamSchedule(ExamScheduleActivity.this, mServiceReceiver);

        } else {
            Intent in = new Intent(ExamScheduleActivity.this, Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ExamScheduleActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ExamScheduleActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ExamScheduleActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success.getResultCount() == 0) {
                    AskOptionDialog("No Exam Found").show();
                } else {


                    if (success.getExamScheduleList() != null && success.getExamScheduleList().size() > 0) {
                        mList.clear();
                        mList.addAll(success.getExamScheduleList());
                        checkList();
                    } else {
                        AskOptionDialog("No Exam Found").show();
                    }


                }

                break;

        }
    }
}
