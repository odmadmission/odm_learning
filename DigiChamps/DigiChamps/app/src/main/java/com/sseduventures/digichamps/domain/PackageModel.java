package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class PackageModel implements Parcelable {

    private int Package_ID;
    private String Package_Name;
    private String Package_Desc;
    private int Price;
    private int DiscPrice;
    private String Thumbnail;
    private int Disc_Percent;
    private int Subscripttion_Period;
    private int Subscripttion_Limit;
    private boolean Is_Offline;
    private boolean Is_School;
    private boolean Is_Doubt;
    private boolean Is_Mentor;
    private boolean Is_Dictionary;
    private boolean Is_Feed;

    private boolean Is_Video;
    private int InCart;

    private int Type;

    private List<Integer> ChapterList;

    public int getPackage_ID() {
        return Package_ID;
    }

    public void setPackage_ID(int package_ID) {
        Package_ID = package_ID;
    }

    public String getPackage_Name() {
        return Package_Name;
    }

    public void setPackage_Name(String package_Name) {
        Package_Name = package_Name;
    }

    public String getPackage_Desc() {
        return Package_Desc;
    }

    public void setPackage_Desc(String package_Desc) {
        Package_Desc = package_Desc;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }

    public int getDiscPrice() {
        return DiscPrice;
    }

    public void setDiscPrice(int discPrice) {
        DiscPrice = discPrice;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public int getDisc_Percent() {
        return Disc_Percent;
    }

    public void setDisc_Percent(int disc_Percent) {
        Disc_Percent = disc_Percent;
    }

    public int getSubscripttion_Period() {
        return Subscripttion_Period;
    }

    public void setSubscripttion_Period(int subscripttion_Period) {
        Subscripttion_Period = subscripttion_Period;
    }

    public int getSubscripttion_Limit() {
        return Subscripttion_Limit;
    }

    public void setSubscripttion_Limit(int subscripttion_Limit) {
        Subscripttion_Limit = subscripttion_Limit;
    }

    public boolean isIs_Offline() {
        return Is_Offline;
    }

    public void setIs_Offline(boolean is_Offline) {
        Is_Offline = is_Offline;
    }

    public boolean isIs_School() {
        return Is_School;
    }

    public void setIs_School(boolean is_School) {
        Is_School = is_School;
    }

    public boolean isIs_Doubt() {
        return Is_Doubt;
    }

    public void setIs_Doubt(boolean is_Doubt) {
        Is_Doubt = is_Doubt;
    }

    public boolean isIs_Mentor() {
        return Is_Mentor;
    }

    public void setIs_Mentor(boolean is_Mentor) {
        Is_Mentor = is_Mentor;
    }

    public boolean isIs_Dictionary() {
        return Is_Dictionary;
    }

    public void setIs_Dictionary(boolean is_Dictionary) {
        Is_Dictionary = is_Dictionary;
    }

    public boolean isIs_Feed() {
        return Is_Feed;
    }

    public void setIs_Feed(boolean is_Feed) {
        Is_Feed = is_Feed;
    }

    public boolean isIs_Video() {
        return Is_Video;
    }

    public void setIs_Video(boolean is_Video) {
        Is_Video = is_Video;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public List<Integer> getChapterList() {
        return ChapterList;
    }

    public void setChapterList(List<Integer> chapterList) {
        ChapterList = chapterList;
    }

    public int getInCart() {
        return InCart;
    }

    public void setInCart(int inCart) {
        InCart = inCart;
    }

    public PackageModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.Package_ID);
        dest.writeString(this.Package_Name);
        dest.writeString(this.Package_Desc);
        dest.writeInt(this.Price);
        dest.writeInt(this.DiscPrice);
        dest.writeString(this.Thumbnail);
        dest.writeInt(this.Disc_Percent);
        dest.writeInt(this.Subscripttion_Period);
        dest.writeInt(this.Subscripttion_Limit);
        dest.writeByte(this.Is_Offline ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_School ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Doubt ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Mentor ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Dictionary ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Feed ? (byte) 1 : (byte) 0);
        dest.writeByte(this.Is_Video ? (byte) 1 : (byte) 0);
        dest.writeInt(this.InCart);
        dest.writeInt(this.Type);
        dest.writeList(this.ChapterList);
    }

    protected PackageModel(Parcel in) {
        this.Package_ID = in.readInt();
        this.Package_Name = in.readString();
        this.Package_Desc = in.readString();
        this.Price = in.readInt();
        this.DiscPrice = in.readInt();
        this.Thumbnail = in.readString();
        this.Disc_Percent = in.readInt();
        this.Subscripttion_Period = in.readInt();
        this.Subscripttion_Limit = in.readInt();
        this.Is_Offline = in.readByte() != 0;
        this.Is_School = in.readByte() != 0;
        this.Is_Doubt = in.readByte() != 0;
        this.Is_Mentor = in.readByte() != 0;
        this.Is_Dictionary = in.readByte() != 0;
        this.Is_Feed = in.readByte() != 0;
        this.Is_Video = in.readByte() != 0;
        this.InCart = in.readInt();
        this.Type = in.readInt();
        this.ChapterList = new ArrayList<Integer>();
        in.readList(this.ChapterList, Integer.class.getClassLoader());
    }

    public static final Creator<PackageModel> CREATOR = new Creator<PackageModel>() {
        @Override
        public PackageModel createFromParcel(Parcel source) {
            return new PackageModel(source);
        }

        @Override
        public PackageModel[] newArray(int size) {
            return new PackageModel[size];
        }
    };
}
