package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.CardviewDiscussion;
import com.sseduventures.digichamps.domain.AddDiscussionResponse;
import com.sseduventures.digichamps.domain.DiscussionList;
import com.sseduventures.digichamps.domain.DiscussionResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;
import java.util.List;

public class DiscussionActivity extends FormActivity implements
        ServiceReceiver.Receiver {

    Context mContext;
    CardviewDiscussion mCardviewDiscussion;
    RecyclerView recyclerview;
    Button btnStartDiscussion;
    Dialog dialogEdit;
    RelativeLayout progressbarrel;
    int pageIndex = 0;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;
    String discussionTitle = "";
    String schoolid, regId, roleid;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private DiscussionResponse success;
    private AddDiscussionResponse mSuccess;

    private ArrayList<DiscussionList> list;
    private EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discussion_forum);
        setModuleName(LogEventUtil.EVENT_discussion_activity);
        logEvent(LogEventUtil.KEY_discussion_activity,LogEventUtil.EVENT_discussion_activity);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        regId = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        roleid = String.valueOf(RegPrefManager.getInstance(this).getroleid());

        mContext = this;
        progressbarrel = findViewById(R.id.progressbarrel);
        btnStartDiscussion = (Button) findViewById(R.id.btnStartDiscussion);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        search=(EditText) findViewById(R.id.searchview);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        TextView tv_at = findViewById(R.id.tv_at);
        FontManage.setFontHeaderBold(mContext, tv_at);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        List<String> m = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            m.add(i + "");
        }

        list = new ArrayList<>();
        mCardviewDiscussion = new CardviewDiscussion(this);
        recyclerview.setAdapter(mCardviewDiscussion);

        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {

                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            progressbarrel.setVisibility(View.VISIBLE);
                            Log.v("...", "Last Item Wow !" + pageIndex);
                            getDiscussionListNew(pageIndex);
                        }
                    }
                }
            }
        });

        btnStartDiscussion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMyDialog();
            }
        });

        getDiscussionListNew(pageIndex);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence newText, int i, int i1, int i2) {
                mCardviewDiscussion.getFilter().filter(newText);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }


    public void showMyDialog() {
        // TODO Auto-generated method stub

        dialogEdit = new Dialog(mContext);
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.dialog_start_discussion);
        final EditText et_text = (EditText) dialogEdit.findViewById(R.id.et_text);
        TextView btnStart = (TextView) dialogEdit.findViewById(R.id.btnStart);

        dialogEdit.show();
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_text.getText().toString().trim().equals("")) {
                    et_text.setError("Required");
                } else {
                    addDiscussionNew(et_text.getText().toString().trim());

                }
            }
        });

    }


    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    private void addDiscussionNew(String title) {
        discussionTitle = title;
        Bundle bun = new Bundle();
        bun.putString("title", title);

        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionPostAddDiscussionList(DiscussionActivity.this,
                    mServiceReceiver, bun);

        } else {
            Intent in = new Intent(DiscussionActivity.this, Internet_Activity.class);
            startActivity(in);
        }

    }

    private void getDiscussionListNew(int StartIndex) {
        if (AppUtil.isInternetConnected(this)) {

            Bundle bun = new Bundle();
            bun.putInt("index", pageIndex);
            NetworkService.startActionPostDiscussionList(DiscussionActivity.this
                    , mServiceReceiver, bun);

        } else {
            Intent in = new Intent(DiscussionActivity.this,
                    Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(DiscussionActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(DiscussionActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(DiscussionActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (success.getResultCount() == 0) {
                    progressbarrel.setVisibility(View.GONE);
                    if (pageIndex == 0) {
                        AskOptionDialog("No Discussion Found").show();
                    }
                } else {

                    if (success.getDiscussion() != null &&
                            success.getDiscussion().size() > 0) {
                        list.addAll(success.getDiscussion());

                        if (list != null && list.size() > 0) {

                            recyclerview.setVisibility(View.VISIBLE);
                            Log.v(LeaderBoardActivity.class.getSimpleName(), list.size() + "");

                            recyclerview.post(new Runnable() {
                                public void run() {
                                    mCardviewDiscussion.setNotifyData(list);
                                }
                            });


                            pageIndex = Constants.PAGE_SIZE + 1;
                            loading = true;
                            progressbarrel.setVisibility(View.GONE);

                        } else {
                            recyclerview.setVisibility(View.GONE);
                        }
                    } else {
                        progressbarrel.setVisibility(View.GONE);
                        if (pageIndex == 0) {
                            recyclerview.setVisibility(View.GONE);
                            recyclerview.post(new Runnable() {
                                public void run() {
                                    mCardviewDiscussion.setNotifyData(null);
                                }
                            });
                        }
                    }
                }
                break;

            case ResponseCodes.ADDFORUMSUCCESS:

                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (mSuccess.isStatus()) {
                    if (dialogEdit != null) {
                        dialogEdit.dismiss();
                    }
                    DiscussionList mModel = new DiscussionList();
                    mModel.setDiscussionID(mSuccess.getDiscussionID());
                    mModel.setTitle(discussionTitle);
                    mCardviewDiscussion.setNewDiscussuion(mModel);
                    getDiscussionListNew(pageIndex);

                } else {
                    AskOptionDialog(mSuccess.getMessage()).show();
                }

                break;

        }
    }

}
