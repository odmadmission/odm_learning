package com.sseduventures.digichamps.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.RWOnlineVideoPlayingActivity;
import com.sseduventures.digichamps.activity.PackageActivityNew;
import com.sseduventures.digichamps.domain.NewLearnRecentwatchedvideos;

import java.util.ArrayList;


public class LearnAdapter extends RecyclerView.Adapter<LearnAdapter.MyViewHolder> {

    private ArrayList<NewLearnRecentwatchedvideos> horizontalbannerList;
    Context context;
    CardView container, container1;
    public static int position;
    String subject_id,subject_name;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView name;
        LinearLayout container;


        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.card_image);
            name = (TextView) view.findViewById(R.id.diy_desc);
            container = (LinearLayout) view.findViewById(R.id.card_dash);

        }
    }

    public LearnAdapter(ArrayList<NewLearnRecentwatchedvideos> horizontalbannerList, Context context,String subject_id,String subject_name) {
        this.horizontalbannerList = horizontalbannerList;
        this.context = context;
        this.subject_id = subject_id;
        this.subject_name = subject_name;

    }

    @Override
    public LearnAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dashboard_card, parent, false);
        return new LearnAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LearnAdapter.MyViewHolder holder, final int listPosition) {
        position = listPosition;
        NewLearnRecentwatchedvideos rwv_videos = horizontalbannerList.get(position);
        holder.name.setText(rwv_videos.getModule_Name());


        Typeface face = Typeface.createFromAsset(context.getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        holder.name.setTypeface(face);

        Picasso.with(context).load(horizontalbannerList.get(listPosition).getImage_Key()).into( holder.imageView);
        this.container1 = container;
        holder.container.setOnClickListener(onClickListener(listPosition));
    }

    @Override
    public int getItemCount() {
        return horizontalbannerList.size();
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!horizontalbannerList.get(position).isIs_Expire()) {
                    Intent i = new Intent(context, RWOnlineVideoPlayingActivity.class);
                    Activity activity = (Activity) context;
                    i.putParcelableArrayListExtra("list",horizontalbannerList);
                    i.putExtra("videoid", horizontalbannerList.get(position).getModule_video());
                    context.startActivity(i);

                }else if(horizontalbannerList.get(position).isIs_Expire() ){
                    if(horizontalbannerList.get(position).isIs_Free()){
                        Intent i = new Intent(context, RWOnlineVideoPlayingActivity.class);
                        Activity activity = (Activity) context;
                        i.putParcelableArrayListExtra("list",horizontalbannerList);
                        i.putExtra("videoid", horizontalbannerList.get(position).getModule_video());
                        context.startActivity(i);

                    }else if(horizontalbannerList.get(position).isIs_Expire() ) {
                        if (!horizontalbannerList.get(position).isIs_Free()) {
                            @SuppressLint("RestrictedApi") final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.MyDialogTheme_buy));
                            LayoutInflater factory = LayoutInflater.from(context);
                            final View view = factory.inflate(R.layout.dialog_buy, null);
                            Button ok = (Button) view.findViewById(R.id.button2);
                            final AlertDialog alertDialog = builder.create();
                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    alertDialog.dismiss();
                                    Intent i = new Intent(context, PackageActivityNew.class);
                                    Activity activity = (Activity) context;
                                    context.startActivity(i);

                                }
                            });

                            builder.setView(view);
                            builder.setCancelable(true);
                        }
                    }
                }


            }
        };
    }
}

