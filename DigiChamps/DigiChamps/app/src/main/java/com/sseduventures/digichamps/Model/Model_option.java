package com.sseduventures.digichamps.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ntspl22 on 6/27/2017.
 */

public class Model_option implements Parcelable {


    private String Answer_ID,Option_Desc,Option_Image;


    public Model_option(String Answer_ID, String Option_Desc, String Option_Image) {

        this.Answer_ID = Answer_ID;
        this.Option_Desc = Option_Desc;
        this.Option_Image = Option_Image;
    }

    protected Model_option(Parcel in) {
        Answer_ID = in.readString();
        Option_Desc = in.readString();
        Option_Image = in.readString();
    }

    public static final Creator<Model_option> CREATOR = new Creator<Model_option>() {
        @Override
        public Model_option createFromParcel(Parcel in) {
            return new Model_option(in);
        }

        @Override
        public Model_option[] newArray(int size) {
            return new Model_option[size];
        }
    };

    public String getAnswer_ID() {
        return Answer_ID;
    }
    public String getOption_Desc() {
        return Option_Desc;
    }
    public String getOption_Image() {
        return Option_Image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Answer_ID);
        dest.writeString(Option_Desc);
        dest.writeString(Option_Image);
    }
}


