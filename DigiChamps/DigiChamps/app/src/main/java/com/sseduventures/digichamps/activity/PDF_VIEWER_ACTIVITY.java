package com.sseduventures.digichamps.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;


public class PDF_VIEWER_ACTIVITY extends FormActivity {

    private WebView webview;
    private Bundle extras;
    private String newString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_pdf__viewer__activity);
        setModuleName(LogEventUtil.EVENT_pdf_viewer);
        webview = (WebView) findViewById(R.id.web_view);

        extras = getIntent().getExtras();
        if (extras != null) {
            newString = extras.getString("url");
        }
        init();
        listener();

    }

    private void init() {
        webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);

        webview.loadUrl("https://drive.google.com/file/d/0B534aayZ5j7Yc3RhcnRlcl9maWxl/view");

    }

    private void listener() {
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });
    }
}

