package com.sseduventures.digichamps.webservice.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.nio.file.Path;

public class FeedBackFormRequestModel implements Parcelable {

    private String PhoneNo;
    private String className;

    public FeedBackFormRequestModel() {
    }

    public static final Creator<FeedBackFormRequestModel> CREATOR = new Creator<FeedBackFormRequestModel>() {
        @Override
        public FeedBackFormRequestModel createFromParcel(Parcel in) {
            return new FeedBackFormRequestModel();
        }

        @Override
        public FeedBackFormRequestModel[] newArray(int size) {
            return new FeedBackFormRequestModel[size];
        }
    };

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String aClass) {
        className = aClass;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getRegd_Id() {
        return Regd_Id;
    }

    public void setRegd_Id(String regd_Id) {
        Regd_Id = regd_Id;
    }

    private String Category;
    private String Message;
    private String Regd_Id;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(PhoneNo);
        dest.writeString(className);
        dest.writeString(Category);
        dest.writeString(Message);
        dest.writeString(Regd_Id);
    }
}
