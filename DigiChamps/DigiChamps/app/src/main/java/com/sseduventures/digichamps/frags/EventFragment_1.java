package com.sseduventures.digichamps.frags;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;

public class EventFragment_1 extends Fragment {

    public EventFragment_1() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((FormActivity) getActivity()).logEvent(LogEventUtil.KEY_LearnAndEarnGifts_PAGE,
                LogEventUtil.EVENT_LearnAndEarnGifts_PAGE);
        ((FormActivity) getActivity()).setModuleName(LogEventUtil.EVENT_LearnAndEarnGifts_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.event_frag_competition, container, false);
    }
}
