package com.sseduventures.digichamps.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.Model.AutocompleteModel;
import com.sseduventures.digichamps.Model.Basic_info_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.Board_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.Class_listview_DialogAdapter;
import com.sseduventures.digichamps.adapter.CustomAutoCompleteAdapter;
import com.sseduventures.digichamps.adapter.School_listview_DialogAdapter;
import com.sseduventures.digichamps.config.AppConfig;
import com.sseduventures.digichamps.domain.EditProfileResponse;
import com.sseduventures.digichamps.domain.UploadImageResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.PickerBuilder;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.CircleTransform;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.utils.Profile_utility;
import com.sseduventures.digichamps.utils.Utility;
import com.sseduventures.digichamps.utils.Utils;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import com.sseduventures.digichamps.webservice.model.ProfileModelClass;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends FormActivity implements
        ServiceReceiver.Receiver,ActivityCompat.OnRequestPermissionsResultCallback {


    private TextInputEditText input_name, input_phone, input_email,
            input_board,
            input_class,school_name_ed,input_section;
    private AutoCompleteTextView input_school;
    private CircleImageView header;
    private Button edit, submit;
    private Toolbar toolbar;
    private TextView select_class, select_tv,select_section_tv,error_email,
            error_phone,error_name;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;

    private ArrayList<ProfileModelClass> success;

    private String nameClass="null",schoolID="null";
    private String userChoosenTask;
    private TextView changepassword;

    private RelativeLayout new_school;

    private UploadImageResponse mSuccess;
    private EditProfileResponse newSuccess;
    private String[] arr = null;
    ArrayList<String> school_List;
    ArrayList<AutocompleteModel> school_details;

    ArrayList<Basic_info_model> section_List_new,class_detail_list;
    CustomAutoCompleteAdapter adapter;
    private String selecteditem="null";
    boolean flag_other = false,flagBoard=false,flag_section=false;
    private School_listview_DialogAdapter schoolAdapter;
    private String board = "null", class_name = "null", nameSection = "null",boardId="",classId="",classselect="null",sectionselect="null";
    private TextInputLayout school_name;
    ArrayList<Basic_info_model> board_list,class_list;
    private Board_listview_DialogAdapter boardAdapter;
    ArrayList<String> class_details;
    private Class_listview_DialogAdapter classAdapter;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_editprofile);
        logEvent(LogEventUtil.KEY_EditProfile_PAGE,
                LogEventUtil.EVENT_EditProfile_PAGE);
        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setNavigationIcon(
                getResources().
                getDrawable(R.drawable.ic_backarrow));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        input_name = (TextInputEditText) findViewById(R.id.input_name);
        input_phone = (TextInputEditText) findViewById(R.id.input_phone);
        input_email = (TextInputEditText) findViewById(R.id.input_email);
        input_school = (AutoCompleteTextView) findViewById(R.id.input_school);
        header = (CircleImageView) findViewById(R.id.header);
        input_board = (TextInputEditText) findViewById(R.id.input_board);
        input_class = (TextInputEditText) findViewById(R.id.input_class);
        edit = (Button) findViewById(R.id.edit);
        changepassword = findViewById(R.id.changepassword);
        submit = (Button) findViewById(R.id.submit);
        new_school=(RelativeLayout)findViewById(R.id.new_school);
        select_tv = (TextView) findViewById(R.id.select_tv);
        select_class = (TextView) findViewById(R.id.select_class);
        school_name_ed = findViewById(R.id.school_name_ed);
        school_name=(TextInputLayout)findViewById(R.id.school_name);
        select_section_tv=findViewById(R.id.select_section_tv);
        input_section=findViewById(R.id.input_section);
        error_email=(TextView)findViewById(R.id.error_email);
        error_phone=(TextView)findViewById(R.id.error_phone);
        error_name=(TextView)findViewById(R.id.error_name);

        select_class.setVisibility(View.GONE);
        select_tv.setVisibility(View.GONE);
        select_section_tv.setVisibility(View.GONE);


        school_List = new ArrayList<>();
        school_details = new ArrayList<>();
        board_list = new ArrayList<>();
        class_details = new ArrayList<>();
        section_List_new = new ArrayList<>();
        class_list = new ArrayList<>();
        class_details = new ArrayList<>();
        class_detail_list = new ArrayList<>();
        edit.setVisibility(View.VISIBLE);
        submit.setVisibility(View.GONE);


        input_name.setEnabled(false);
        input_phone.setEnabled(false);
        input_email.setEnabled(false);

        header.setEnabled(false);
        input_board.setEnabled(false);
        input_class.setEnabled(false);
        select_tv.setEnabled(false);
        select_section_tv.setEnabled(false);
        changepassword.setVisibility(View.GONE);
        select_class.setEnabled(false);

        input_section.setEnabled(false);
        input_school.setEnabled(false);


        String userName = RegPrefManager.getInstance(this).getUserName();
        String phone = RegPrefManager.getInstance(this).getPhone();
        String email = RegPrefManager.getInstance(this).getEmail();
        String schoolName = RegPrefManager.getInstance(this).getSchoolName();
        String image = RegPrefManager.getInstance(this).getImageString();
        final String boardName = RegPrefManager.getInstance(this).getBoardName();
        String className = RegPrefManager.getInstance(this).getClassName();
        final String sectionid=RegPrefManager.getInstance(this).getsectionName();
        String schoolid=RegPrefManager.getInstance(this).getSchoolId();
        boardId=RegPrefManager.getInstance(this).getKeyBoardId();
        classId=String.valueOf(RegPrefManager.getInstance(this).getKeyClassId());



        board = boardName;
        nameClass = className;
        nameSection=sectionid;
        input_name.setText(userName);
        input_phone.setText(phone);
        input_email.setText(email);

        if(schoolName!=null&&schoolName.contains("-"))
        {
            String split=schoolName.split("-")[1];
            input_school.setText(split);
        }
        else
        input_school.setText(schoolName);
        input_board.setText(boardName);
        input_class.setText(className);
        input_section.setText(sectionid);






        String correctImage = AppConfig.SRVR_URL + "/Images/Profile/" + image;
        Picasso.with(getApplicationContext()).load(correctImage)
                .transform(new CircleTransform()).into(header);

        getBoardClass();

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setVisibility(View.GONE);
                submit.setVisibility(View.VISIBLE);
                input_name.setEnabled(true);
                input_phone.setEnabled(false);
                input_email.setEnabled(true);

                header.setEnabled(true);
                changepassword.setVisibility(View.VISIBLE);
                input_board.setEnabled(false);
                input_class.setEnabled(false);
                input_section.setEnabled(false);
                select_tv.setEnabled(true);
                select_section_tv.setEnabled(true);
                select_class.setEnabled(true);
                select_class.setVisibility(View.VISIBLE);
                select_tv.setVisibility(View.VISIBLE);
                select_section_tv.setVisibility(View.VISIBLE);
                input_school.setEnabled(true);


            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (flag_other == true) {
                    selecteditem = school_name_ed.getText().toString();
                    schoolID="0";
                } else {

                    selecteditem = input_school.getText().toString();
                    schoolId(selecteditem);
                }
                if(flagBoard==false){
                    board=boardName;
                }

                if(flag_section==false){
                    nameSection=sectionid;
                    if(sectionselect.equals("null")){
                        nameSection = "null";
                    }
                }



                if (validationNew()) {

                    edit.setVisibility(View.VISIBLE);
                    submit.setVisibility(View.GONE);
                    input_name.setEnabled(false);
                    input_phone.setEnabled(false);
                    input_email.setEnabled(false);
                    // input_school.setEnabled(false);
                    header.setEnabled(false);
                    input_board.setEnabled(false);
                    changepassword.setVisibility(View.GONE);
                    input_class.setEnabled(false);
                    select_tv.setEnabled(false);
                    select_class.setEnabled(false);
                    select_section_tv.setEnabled(false);
                    school_name_ed.setEnabled(false);
                    input_school.setEnabled(false);
                    input_section.setEnabled(false);
                    select_class.setVisibility(View.GONE);
                    select_tv.setVisibility(View.GONE);
                    select_section_tv.setVisibility(View.GONE);
                    postProfileData();
                }
            }
        });


        select_class.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boarddialog();
            }
        });

        select_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(board!=null){
                    if(!input_board.getText().toString().isEmpty()){
                        createClassList();
                    }
                    if (classNames != null) {




                        dialogclass_dialog();
                    }
                }
               else {
                    AskOptionDialogNew("Please select your board").show();
                    //Toast.makeText(this, "Please select your board", Toast.LENGTH_SHORT).show();
                }
            }
        });



        header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // selectImage();
                checkPermission(ProfileActivity.this);
            }
        });



        input_school.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long l) {

               // if(!input_school.getText().toString().isEmpty())

                    Object item = parent.getItemAtPosition(position);


                    if (item instanceof String) {
                        selecteditem = (String) item;
                        flag_other = false;

                        school_name.setVisibility(View.GONE);
                        new_school.setVisibility(View.GONE);


                        schoolId(selecteditem);


                }

                board="null";
                nameClass ="null";
                nameSection="null";

                flag_section=true;
                flagBoard=true;


            }


        });
        input_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            if(!input_name.getText().toString().isEmpty()){
                error_name.setVisibility(View.GONE);
            }

            }
        });
        input_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!input_email.getText().toString().isEmpty()){
                    error_email.setVisibility(View.GONE);
                }

            }
        });
        input_phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!input_phone.getText().toString().isEmpty()){
                    error_phone.setVisibility(View.GONE);
                }

            }
        });
        school_name_ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                board="null";
                nameClass ="null";
                nameSection="null";

                flag_section=true;
                flagBoard=true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        select_section_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    if(!input_class.getText().toString().isEmpty()) {
                        createSectionNames();

                        if (section_List_new != null && section_List_new.size() > 0) {


                            sectiondialog();
                        }
                        else{
                            AskOptionDialogNew("Please select class first").show();
                        }
                    }else{
                        AskOptionDialogNew("Please select class first").show();
                    }



            }
        });


        changepassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //selectImage();
                checkPermission(ProfileActivity.this);
            }
        });

    }

    public void schoolId(String selecteditem){
        for(int i=0;i<school_details.size();i++){
            String name=school_details.get(i).getSchoolName();
            if(selecteditem.equalsIgnoreCase(name)){
                String id = school_details.get(i).getSchoolId();
                schoolID=id;
            }
        }
    }




    private long getBoardId(String b) {
        long r = 0;

        if (success != null) {
            for (int i = 0; i < success.size(); i++) {



                int size=success.get(i).getBoardList().size();
                for(int j=0;j<size;j++) {
                    String name=success.get(i).getBoardList().get(j).getBoardName();
                    if (name.equalsIgnoreCase(b)) {
                        r = success.get(i).getBoardList().get(j).getBoardId();
                    }
                }
            }
        }

        return r;
    }

    private long getClassId(String b, String c) {
        long r = 0;

        if (success != null) {

            for (int i = 0; i < success.size(); i++) {

                int size=success.get(i).getBoardList().size();
                for(int k=0;k<size;k++){
                    String name=success.get(i).getBoardList().get(k).getBoardName();
                    if(name.equalsIgnoreCase(b)){
                        int class_size= success.get(i).getBoardList().get(k).getClassList().size();
                        for(int j=0;j<class_size;j++){
                            String class_name=success.get(i).getBoardList().get(k).getClassList().get(j)
                                    .getClassName();
                            if(class_name.equalsIgnoreCase(c)){
                                r = success.get(i).getBoardList().get(k).getClassList().get(j).getClassId();
                            }
                        }
                    }
                }

            }
        }

        return r;
    }



    private void prepareClassName(String className) {
        input_class.setText(className);
    }




    private void prepareBoardName(String boardname) {
        input_board.setText(boardname);
    }





    private void postProfileData() {

        if (flag_other == true) {
            selecteditem = school_name_ed.getText().toString();
            schoolID="0";
        } else {

            selecteditem = input_school.getText().toString();
            schoolId(selecteditem);
        }

        String name=input_name.getText().toString();
        String phone=input_phone.getText().toString();
        String email=input_email.getText().toString();
        String school=input_school.getText().toString();
        boardId=String.valueOf(getBoardId(board));
        classId=String.valueOf(getClassId(board, nameClass));

        Bundle bun = new Bundle();
        bun.putInt("boardId", (int) getBoardId(board));
        bun.putInt("classId", (int) getClassId(board, nameClass));
        bun.putString("name", input_name.getText().toString().trim());
        bun.putString("sAns", "test");
        bun.putInt("sId", 1);
        bun.putString("email", input_email.getText().toString().trim());
        bun.putString("phone", input_phone.getText().toString().trim());
        bun.putString("schoolName", selecteditem);
        bun.putString("schoolId",schoolID);
        bun.putString("sectionName",input_section.getText().toString().trim());
        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please wait...");
            NetworkService.startPostEditProfile(
                    this, mServiceReceiver, bun);

        } else {

            Intent in = new Intent(ProfileActivity.this,
                    Internet_Activity.class);
            startActivity(in);

        }


    }


    private void postImage(String image) {


        File imageNew = null;
        imageNew = new File(image.toString());

        Bundle bun = new Bundle();
        bun.putString("image", imageNew.toString());
        if (AppUtil.isInternetConnected(this)) {
            showDialog(null, "Please wait...");
            NetworkService.startPostEPImage(this, mServiceReceiver, bun);

        } else {

            Intent in = new Intent(ProfileActivity.this, Internet_Activity.class);
            startActivity(in);

        }
    }

    private void getBoardClass() {

        if (AppUtil.isInternetConnected(this)) {

            Bundle bundle = new Bundle();
            showDialog(null, "Please wait...");
            NetworkService.startActionGetBoardClass(this, mServiceReceiver, bundle);

        } else {

            Intent in = new Intent(ProfileActivity.this, Internet_Activity.class);
            startActivity(in);

        }
    }

    @Override
    protected void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    private String[] boardNames = null;
    private String[] sectionName = null;

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ProfileActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ProfileActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ProfileActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelableArrayList(IntentHelper.RESULT_DATA);
                //showToast(success.size()+"");
                if (success != null && success.size() > 0) {
                    createBoardNames();

                    createSchoolNames();


                }
                break;
            case ResponseCodes.IMAGESUCCESS:
                mSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);


                break;

            case ResponseCodes.PROFILESUBMITSUCCESS:
                hideDialog();
                newSuccess = resultData.getParcelable(IntentHelper.RESULT_DATA);
                if (newSuccess.getSuccess().getMessage().equalsIgnoreCase("Profile Successfully Updated")) {
                    showToast("Successfully Uploaded");
                    RegPrefManager.getInstance(this).setUserName(input_name.getText().toString().trim());
                    RegPrefManager.getInstance(this).setPhone(input_phone.getText().toString().trim());
                    RegPrefManager.getInstance(this).setEmail(input_email.getText().toString().trim());

                    RegPrefManager.getInstance(this).setBoardName(input_board.getText().toString().trim());
                    RegPrefManager.getInstance(this).setClassName(input_class.getText().toString().trim());
                    RegPrefManager.getInstance(this).setsectionName(input_section.getText().toString().trim());

                    RegPrefManager.getInstance(this).setSchoolName(selecteditem);

                    RegPrefManager.getInstance(this).setKeyBoardId(boardId);
                    RegPrefManager.getInstance(this).setKeyClassId(Long.valueOf(classId));

                    finish();
                }


                break;
        }


    }
    private String[] sectionNames = null;
    private void createSectionNames() {

        for (int j = 0; j < success.size(); j++) {
            sectionNames = new String[success.get(j).getSectionList().size()];
            for (int i = 0; i < success.get(j).getSectionList().size(); i++) {
                sectionNames[i] = success.get(j).getSectionList().get(i).toString();

                Basic_info_model basic_info_model = new Basic_info_model();
                basic_info_model.setName(success.get(j).getSectionList().get(i).toString());
                section_List_new.add(basic_info_model);
            }
        }
    }
    private void createSchoolNames() {

        for (int j = 0; j < success.size(); j++) {
            arr = new String[success.get(j).getSchoolInfoList().size()];
            for (int i = 0; i < success.get(j).getSchoolInfoList().size(); i++) {
                arr[i] = success.get(j).getSchoolInfoList().get(i).getSchoolName().toString();
                school_List.add(success.get(j).getSchoolInfoList().get(i).getSchoolName().toString());

                AutocompleteModel autocompleteModel = new AutocompleteModel();
                autocompleteModel.setSchoolName(success.get(j).getSchoolInfoList().get(i).getSchoolName().toString());
                autocompleteModel.setSchoolId(success.get(j).getSchoolInfoList().get(i).getSchoolId().toString());
                school_details.add(autocompleteModel);
            }
        }
        if (school_List.size() > 0) {
            school_List.add(school_List.size(), "Other");
        }


        schoolList();

    }

    public void schoolList() {
        String footerText = "<font color=\"red\">School name not in the list? </font>";



        adapter = new CustomAutoCompleteAdapter(
                this, android.R.layout.simple_list_item_1, arr, footerText);
        adapter.setOnFooterClickListener(new CustomAutoCompleteAdapter.OnFooterClickListener() {
            @Override
            public void onFooterClicked() {
                flag_other = true;


                input_school.setText("School name not in the list?");

                new_school.setVisibility(View.VISIBLE);
                school_name.setVisibility(View.VISIBLE);

                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(input_school.getWindowToken(), 0);
                input_school.setSelection(input_school.length());
                input_school.dismissDropDown();
                schoolID="null";
                selecteditem="null";
            }
        });
        input_school.setThreshold(2);
        input_school.setAdapter(adapter);

    }



    private void createBoardNames() {
        boardNames = new String[success.size()];
        for (int i = 0; i < success.size(); i++) {
            int size=success.get(i).getBoardList().size();
            for(int j=0;j<size;j++) {
                String names=success.get(i).getBoardList().get(j).getBoardName();
                int id=success.get(i).getBoardList().get(j).getBoardId();
                Basic_info_model basic_info_model = new Basic_info_model();
                basic_info_model.setName(names);
                basic_info_model.setBoardId(String.valueOf(id));
                board_list.add(basic_info_model);

            }

        }


    }


   // private String board;
    private String[] classNames = null;



    private void createClassList() {

        for (int k = 0; k < success.size(); k++) {
            for (int i = 0; i < success.get(k).getBoardList().size(); i++) {

                if (board != null && board.equalsIgnoreCase(success.get(k).getBoardList().get(i).getBoardName())) {
                    classNames = new String[success.get(k).getBoardList().get(i).getClassList().size()];
                    for (int j = 0; j < success.get(k).getBoardList().get(i).getClassList().size(); j++) {
                        classNames[j] = success.get(k).getBoardList().get(i).getClassList().get(j).getClassName();
                        int classid=success.get(k).getBoardList().get(i).getClassList().get(j).getClassId();
                        Basic_info_model basic_info_model = new Basic_info_model();
                        basic_info_model.setName(success.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                        basic_info_model.setClassId(String.valueOf(success.get(k).getBoardList().get(i).getClassList().get(j).getClassId()));
                        class_list.add(basic_info_model);

                        class_details.add(success.get(k).getBoardList().get(i).getClassList().get(j).getClassName());
                    }
                }
            }

        }
    }

    public void dialogclass_dialog() {
        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);


        Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(class_details);
        class_details.clear();
        class_details.addAll(primesWithoutDuplicates);
        class_list.clear();
        for (int i = 0; i < class_details.size(); i++) {


            Basic_info_model basic_info_model = new Basic_info_model();
            String name = class_details.get(i).toString();
            if (class_name.equals("null")) {

                basic_info_model.setName(name);
            } else {
                if (class_name.equals(name)) {
                    basic_info_model.setFlag(true);
                    basic_info_model.setName(name);
                } else {
                    basic_info_model.setFlag(false);
                    basic_info_model.setName(name);
                }
            }



            class_list.add(basic_info_model);

        }
        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);


        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        select_class_tv.setVisibility(View.VISIBLE);
        select_tv.setVisibility(View.GONE);

        classAdapter = new Class_listview_DialogAdapter(class_list, getApplicationContext());

        class_detail_list.addAll(class_list);

        listView.setAdapter(classAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                class_list.get(position).setFlag(true);
                class_name = class_list.get(position).getName();
                for (int i = 0; i < class_list.size(); i++) {
                    if (position != i)
                        class_list.get(i).setFlag(false);
                }
                classAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = classAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        nameClass = basic_info_model.getName();
                        classId=basic_info_model.getClassId();
                        //counter=counter+1;
                        prepareClassName(nameClass);

                        classselect=nameClass;
                        if(!input_school.getText().toString().isEmpty()) {
                            String value=input_class.getText().toString();
                            if (value.equalsIgnoreCase("Class - IX")) {

                            } else {
                                RegPrefManager.getInstance(ProfileActivity.this).setschoolCode(null);
                            }
                        }



                    }
                }
                //if (section_List_new == null ) {

                    createSectionNames();
                  //  AskOptionDialogNew("Please select class first").show();


                //}


                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void boarddialog() {
        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);

        // Set dialog title
        // dialog.setTitle("Custom Dialog");

        // set values for custom dialog components - text, image and button


        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);
        boardAdapter = new Board_listview_DialogAdapter(board_list, getApplicationContext());

        listView.setAdapter(boardAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                board_list.get(position).setFlag(true);
                for (int i = 0; i < board_list.size(); i++) {
                    if (position != i)
                        board_list.get(i).setFlag(false);
                }
                boardAdapter.notifyDataSetChanged();
            }
        });
        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = boardAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                        board = basic_info_model.getName();
                        boardId=basic_info_model.getBoardId();
                        nameClass ="null";
                        createClassList();
                        prepareBoardName(board);
                        flagBoard=true;
                        if(!input_school.getText().toString().isEmpty()) {
                            String value=input_board.getText().toString();
                            if (value.equalsIgnoreCase("CBSE")) {

                            } else {
                                RegPrefManager.getInstance(ProfileActivity.this).setschoolCode(null);
                            }
                        }
                        //counter=counter+1;


                    }
                }


                dialog.dismiss();

            }
        });

        dialog.show();
    }


    private android.app.AlertDialog AskOptionDialogNew(String msg) {
        android.app.AlertDialog myQuittingDialogBox = new android.app.AlertDialog.Builder(this)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })

                .create();
        return myQuittingDialogBox;
    }

    //select image
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library"};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(Html.fromHtml("<font color='#05ab9a'>Add Photo</font>"));

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Profile_utility.checkPermission(ProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        new PickerBuilder(ProfileActivity.this, PickerBuilder.SELECT_FROM_CAMERA)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(ProfileActivity.this, imageUri);
                                            header.setBackgroundResource(0);
                                            header.setImageBitmap(bitmap);
                                            postImage(getPath(getImageUri(ProfileActivity.this, bitmap)));
                                            //imageUpload(getPath(getImageUri(ProfileActivity.this, bitmap)));
                                        } catch (Exception e) {
                                            Toast.makeText(ProfileActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                })
                                .withTimeStamp(false)
                                .setCropScreenColor(Color.GREEN)
                                .start();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        new PickerBuilder(ProfileActivity.this, PickerBuilder.SELECT_FROM_GALLERY)
                                .setOnImageReceivedListener(new PickerBuilder.onImageReceivedListener() {
                                    @Override
                                    public void onImageReceived(Uri imageUri) {
                                        try {
                                            Bitmap bitmap = Utility.handleSamplingAndRotationBitmap(ProfileActivity.this, imageUri);
                                            header.setBackgroundResource(0);
                                            header.setImageBitmap(bitmap);
                                            postImage(getPath(getImageUri(ProfileActivity.this, bitmap)));
                                            //imageUpload(getPath(getImageUri(ProfileActivity.this, bitmap)));
                                        } catch (Exception e) {
                                            Toast.makeText(ProfileActivity.this, "Please insert photo again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                })
                                .setCropScreenColor(Color.GREEN)
                                .setOnPermissionRefusedListener(new PickerBuilder.onPermissionRefusedListener() {
                                    @Override
                                    public void onPermissionRefused() {
                                        Toast.makeText(ProfileActivity.this, "This permision is necessary", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .start();

                } else {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }


    ////getting image uri from image bitmap
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", "not found");

        return Uri.parse(path);
    }


    // Get Path of selected image
    private String getPath(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getApplicationContext(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    @Override
    public void onBackPressed() {
        Intent in = new Intent(ProfileActivity.this, NewDashboardActivity.class);
        startActivity(in);
        finish();
    }

    public void sectiondialog() {
        section_List_new.clear();
        createSectionNames();
        final Dialog dialog = new Dialog(ProfileActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.board_editprofile_dialog);



        ListView listView = (ListView) dialog.findViewById(R.id.dialoglist);
        TextView select_tv = (TextView) dialog.findViewById(R.id.select_tv);
        TextView select_class_tv = (TextView) dialog.findViewById(R.id.select_class_tv);
        TextView select_school_tv = (TextView) dialog.findViewById(R.id.select_school_tv);
        TextView select_section_tv = (TextView) dialog.findViewById(R.id.select_section_tv);
        select_class_tv.setVisibility(View.GONE);
        select_tv.setVisibility(View.GONE);
        select_school_tv.setVisibility(View.GONE);
        select_section_tv.setVisibility(View.VISIBLE);
        TextView ok_but = (TextView) dialog.findViewById(R.id.ok_but);

        for(int i=0;i<section_List_new.size();i++){

        }
        schoolAdapter = new School_listview_DialogAdapter(section_List_new, getApplicationContext());

        listView.setAdapter(schoolAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                section_List_new.get(position).setFlag(true);
                nameSection = section_List_new.get(position).getName();
                for (int i = 0; i < section_List_new.size(); i++) {
                    if (position != i)
                        section_List_new.get(i).setFlag(false);
                }
                schoolAdapter.notifyDataSetChanged();

            }
        });

        ok_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<Basic_info_model> list = schoolAdapter.basic_info_modelArrayList();
                for (int i = 0; i < list.size(); i++) {
                    boolean flag = list.get(i).isFlag();
                    if (flag == true) {
                        Basic_info_model basic_info_model = list.get(i);
                      String  nameSection = basic_info_model.getName();
                        input_section.setText(nameSection);
                        //counter=counter+1;
                   //     prepareSectionName(nameSection);
                        flag_section=true;
                        if(!input_school.getText().toString().isEmpty()){
                            String value=input_section.getText().toString();
                            classselect=value;
                            sectionselect=value;
                            if(value.equals("A")){

                            }else {
                                RegPrefManager.getInstance(ProfileActivity.this).setschoolCode(null);
                            }
                        }

                    }
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    private Boolean validationNew() {
        Boolean valid_email = Utils.isValidMail(input_email.getText().toString().trim());
        if (input_name.getText().toString().trim().isEmpty()) {
            error_name.setVisibility(View.VISIBLE);
            error_name.setText("Please Enter Your Name");

            return false;
        }


        if (input_email.getText().toString().trim().isEmpty()) {
            error_email.setVisibility(View.VISIBLE);
            error_email.setText("Please Enter Your Email");

            return false;
        }

        if (!valid_email) {
            error_email.setVisibility(View.VISIBLE);
            error_email.setText("Please Enter Valid Email");

            return false;
        }

        if (input_phone.getText().toString().trim().isEmpty()) {
            error_phone.setVisibility(View.VISIBLE);
            error_phone.setError("Please Enter Your Phone Number");

            return false;
        }

        if (selecteditem.equalsIgnoreCase("null")) {
            showToast("Please Enter School Name");

            return false;
        }
        if (board.equals("null")) {
            showToast("Please Your Board Name");

            return false;
        }

        if (nameClass.equalsIgnoreCase("null")) {
            showToast("Please Your Class Name");

            return false;
        }
       /* if(classselect.equals("null")){
            showToast("Please Your Section Name");

            return false;
        }*/
        if (nameSection.equals("null")) {
            showToast("Please Your Section Name");

            return false;
        }




        return true;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public  boolean checkPermission(final Context context)
    {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if(currentAPIVersion>= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.
                    WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)
                        context, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permission necessary");
                    alertBuilder.setMessage("External storage permission is necessary");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                selectImage();
                return true;
            }
        } else {
            selectImage();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

            //resume tasks needing this permission
            selectImage();
        }


    }
}
