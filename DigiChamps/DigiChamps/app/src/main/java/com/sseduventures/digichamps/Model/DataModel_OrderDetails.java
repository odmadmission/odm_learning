package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl24 on 4/15/2017.
 */

public class DataModel_OrderDetails {
    private String package_name,subjects,chapters,val,val_upto,date;

    public DataModel_OrderDetails() {
    }

    public DataModel_OrderDetails(String date,String package_name, String subjects,String chapters,String val, String val_upto) {
        this.package_name = package_name;
        this.subjects = subjects;
        this.chapters = chapters;
        this.val = val;
        this.val_upto = val_upto;
        this.date=date;
    }

    public String getPackageName() {
        return package_name;
    }

    public void setPackageName(String package_name) {
        this.package_name = package_name;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getChapters() {
        return chapters;
    }

    public void setChapters(String chapters) {
        this.chapters = chapters;
    }
    public void setVal(String val) {
        this.val = val;
    }
    public String getVal() {
        return val;
    }
    public String getValUpto() {
        return val_upto;
    }

    public void setValUpto(String val_upto) {
        this.val_upto = val_upto;
    }
    public String getDate(){
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }

}

