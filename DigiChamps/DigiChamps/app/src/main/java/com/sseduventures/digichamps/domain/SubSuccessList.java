package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NISHIKANT on 7/9/2018.
 */

public class SubSuccessList implements Parcelable{

    private int SubjectId;
    private String Subject;
    private List<SubChapterList> ChapterList;

    public int getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(int subjectId) {
        SubjectId = subjectId;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public List<SubChapterList> getChapterList() {
        return ChapterList;
    }

    public void setChapterList(List<SubChapterList> chapterList) {
        ChapterList = chapterList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.SubjectId);
        dest.writeString(this.Subject);
        dest.writeList(this.ChapterList);
    }

    public SubSuccessList() {
    }

    protected SubSuccessList(Parcel in) {
        this.SubjectId = in.readInt();
        this.Subject = in.readString();
        this.ChapterList = new ArrayList<SubChapterList>();
        in.readList(this.ChapterList, SubChapterList.class.getClassLoader());
    }

    public static final Creator<SubSuccessList> CREATOR = new Creator<SubSuccessList>() {
        @Override
        public SubSuccessList createFromParcel(Parcel source) {
            return new SubSuccessList(source);
        }

        @Override
        public SubSuccessList[] newArray(int size) {
            return new SubSuccessList[size];
        }
    };
}
