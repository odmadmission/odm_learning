package com.sseduventures.digichamps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.adapter.Pdf_Adapter;
import com.sseduventures.digichamps.firebase.LogEventUtil;

import java.util.ArrayList;

/**
 * Created by ntspl22 on 6/24/2017.
 */

public class Fragment_PDF extends Fragment {
    private RecyclerView recyclerView;
    private TextView price,validity,subs_limit,type;
    private Pdf_Adapter adapter;
    private ArrayList<String> pdfsList,pdfsList1;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_pdf, container, false);

        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_difficulty_basis_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_difficulty_basis_frag,
                    LogEventUtil.EVENT_difficulty_basis_frag);
        }

        recyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        pdfsList = new ArrayList<>();
        pdfsList1 = new ArrayList<>();

        adapter = new Pdf_Adapter(pdfsList,pdfsList1,"pdf",getActivity());
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);
        adapter.notifyDataSetChanged();

        return rootView;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        return true;
    }
}
