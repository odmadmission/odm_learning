package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class EnCashResponse implements Parcelable{

    private int count;

    protected EnCashResponse(Parcel in) {
        count = in.readInt();
    }

    public static final Creator<EnCashResponse> CREATOR = new Creator<EnCashResponse>() {
        @Override
        public EnCashResponse createFromParcel(Parcel in) {
            return new EnCashResponse(in);
        }

        @Override
        public EnCashResponse[] newArray(int size) {
            return new EnCashResponse[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(count);
    }


}
