package com.sseduventures.digichamps.activity;


import android.os.Bundle;
import com.freshchat.consumer.sdk.Freshchat;
import com.freshchat.consumer.sdk.FreshchatConfig;
import com.freshchat.consumer.sdk.FreshchatUser;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;

public class FreshChatActivity extends FormActivity {


    private static final String TAG=FreshChatActivity.class.getSimpleName();
    private static final String APP_ID="3dca537a-ec70-4ac0-b37b-5567372cc8f6";
    private static final String APP_KEY="66b55915-3473-4baf-adab-5da1d99616f9";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fresh_chat);
        init();

    }

    private void init()
    {

        FreshchatConfig freshchatConfig=new
            FreshchatConfig(APP_ID,APP_KEY);
        Freshchat.getInstance(getApplicationContext()).init(freshchatConfig);

        FreshchatUser freshUser=Freshchat.getInstance(getApplicationContext()).getUser();

        freshUser.setFirstName("John");
        freshUser.setLastName("Doe");
        freshUser.setEmail("john.doe.1982@mail.com");
        freshUser.setPhone("+91", "9790987495");

        Freshchat.getInstance(getApplicationContext()).setUser(freshUser);

    }
}
