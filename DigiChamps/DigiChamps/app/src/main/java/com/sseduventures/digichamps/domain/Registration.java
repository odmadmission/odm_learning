package com.sseduventures.digichamps.domain;



import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


public class Registration implements Parcelable{


  private Long regdId;

  private String regdNo;

  private String customerName;

  private String email;

  private String mobile;

  private Date dateOfBirth;

  private String gender;

  private String phone;

  private String organisationName;

  private String pincode;

  private String address;

  private String image;

  private String isPrebook;

  private Date insertedDate;

  private String insertedBy;

  private String modifiedBy;

  private Date modifiedDate;

  private Boolean isActive;

  private Boolean isDeleted;

  private Long parentId;

  private String praentName;

  private String parentMail;

  private String parentMobile;

  private String pRelation;

  private String deviceId;

  private String schoolId;

  private String sectionId;

  private String sectionClass;

  private int coinTypeId;

  private String invitorMobile;

  public String getSectionName() {
    return sectionName;
  }

  public void setSectionName(String sectionName) {
    this.sectionName = sectionName;
  }

  private String sectionName;

  public int getCoinTypeId() {
    return coinTypeId;
  }

  public void setCoinTypeId(int coinTypeId) {
    this.coinTypeId = coinTypeId;
  }

  public String getInvitorMobile() {
    return invitorMobile;
  }

  public void setInvitorMobile(String invitorMobile) {
    this.invitorMobile = invitorMobile;
  }

  //  @JsonIgnore
//  @OneToOne(mappedBy = "registration",targetEntity = RegistrationDtl.class)
//  private RegistrationDtl registrationDtl;
//
//  @OneToOne(mappedBy = "registrationMentor",targetEntity = MentorMaster.class)
//  private MentorMaster mentorMaster;

//  public MentorMaster getMentorMaster() {
//    return mentorMaster;
//  }
//
//  public void setMentorMaster(MentorMaster mentorMaster) {
//    this.mentorMaster = mentorMaster;
//  }

  public Registration() {
  }



  private String board;

  private String className;

  public String getBoard() {
    return board;
  }

  public void setBoard(String board) {
    this.board = board;
  }

  public String getClassName() {
    return className;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  private String password;

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  private LoginStatus loginStatus;

  private RegistrationDtl registrationDtl;

  public Registration(String regdNo, String customerName, String email, String mobile, Date dateOfBirth, String gender, String phone, String organisationName, String pincode, String address, String image, String isPrebook, Date insertedDate, String insertedBy, String modifiedBy, Date modifiedDate, Boolean isActive, Boolean isDeleted, Long parentId, String praentName, String parentMail, String parentMobile, String pRelation, String deviceId,
                      String schoolId, String sectionId, String sectionClass, LoginStatus loginStatus, RegistrationDtl registrationDtl) {
    this.regdNo = regdNo;
    this.customerName = customerName;
    this.email = email;
    this.mobile = mobile;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
    this.phone = phone;
    this.organisationName = organisationName;
    this.pincode = pincode;
    this.address = address;
    this.image = image;
    this.isPrebook = isPrebook;
    this.insertedDate = insertedDate;
    this.insertedBy = insertedBy;
    this.modifiedBy = modifiedBy;
    this.modifiedDate = modifiedDate;
    this.isActive = isActive;
    this.isDeleted = isDeleted;
    this.parentId = parentId;
    this.praentName = praentName;
    this.parentMail = parentMail;
    this.parentMobile = parentMobile;
    this.pRelation = pRelation;
    this.deviceId = deviceId;
    this.schoolId = schoolId;
    this.sectionId = sectionId;
    this.sectionClass = sectionClass;
    this.loginStatus = loginStatus;
    this.registrationDtl = registrationDtl;
  }

  public Boolean getActive() {
    return isActive;
  }

  public void setActive(Boolean active) {
    isActive = active;
  }

  public Boolean getDeleted() {
    return isDeleted;
  }

  public void setDeleted(Boolean deleted) {
    isDeleted = deleted;
  }

  public LoginStatus getLoginStatus() {
    return loginStatus;
  }

  public void setLoginStatus(LoginStatus loginStatus) {
    this.loginStatus = loginStatus;
  }

  public RegistrationDtl getRegistrationDtl() {
    return registrationDtl;
  }

  public void setRegistrationDtl(RegistrationDtl registrationDtl) {
    this.registrationDtl = registrationDtl;
  }

  public String getpRelation() {
    return pRelation;
  }

  public void setpRelation(String pRelation) {
    this.pRelation = pRelation;
  }

//  public RegistrationDtl getRegistrationDtl() {
//    return registrationDtl;
//  }
//
//  public void setRegistrationDtl(RegistrationDtl registrationDtl) {
//    this.registrationDtl = registrationDtl;
//  }

  public Long getRegdId() {
    return regdId;
  }

  public void setRegdId(Long regdId) {
    this.regdId = regdId;
  }


  public String getRegdNo() {
    return regdNo;
  }

  public void setRegdNo(String regdNo) {
    this.regdNo = regdNo;
  }


  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }


  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }


  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getOrganisationName() {
    return organisationName;
  }

  public void setOrganisationName(String organisationName) {
    this.organisationName = organisationName;
  }


  public String getPincode() {
    return pincode;
  }

  public void setPincode(String pincode) {
    this.pincode = pincode;
  }


  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }


  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }


  public String getIsPrebook() {
    return isPrebook;
  }

  public void setIsPrebook(String isPrebook) {
    this.isPrebook = isPrebook;
  }


  public Date getInsertedDate() {
    return insertedDate;
  }

  public void setInsertedDate(Date insertedDate) {
    this.insertedDate = insertedDate;
  }


  public String getInsertedBy() {
    return insertedBy;
  }

  public void setInsertedBy(String insertedBy) {
    this.insertedBy = insertedBy;
  }


  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }


  public Date getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Date modifiedDate) {
    this.modifiedDate = modifiedDate;
  }


  public Boolean getIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  public Boolean getIsDeleted() {
    return isDeleted;
  }

  public void setIsDeleted(Boolean isDeleted) {
    this.isDeleted = isDeleted;
  }


  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }


  public String getPraentName() {
    return praentName;
  }

  public void setPraentName(String praentName) {
    this.praentName = praentName;
  }


  public String getParentMail() {
    return parentMail;
  }

  public void setParentMail(String parentMail) {
    this.parentMail = parentMail;
  }


  public String getParentMobile() {
    return parentMobile;
  }

  public void setParentMobile(String parentMobile) {
    this.parentMobile = parentMobile;
  }


  public String getPRelation() {
    return pRelation;
  }

  public void setPRelation(String pRelation) {
    this.pRelation = pRelation;
  }


  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }


  public String getSchoolId() {
    return schoolId;
  }

  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }


  public String getSectionId() {
    return sectionId;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }


  public String getSectionClass() {
    return sectionClass;
  }

  public void setSectionClass(String sectionClass) {
    this.sectionClass = sectionClass;
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeValue(this.regdId);
    dest.writeString(this.regdNo);
    dest.writeString(this.customerName);
    dest.writeString(this.email);
    dest.writeString(this.mobile);
    dest.writeLong(this.dateOfBirth != null ? this.dateOfBirth.getTime() : -1);
    dest.writeString(this.gender);
    dest.writeString(this.phone);
    dest.writeString(this.organisationName);
    dest.writeString(this.pincode);
    dest.writeString(this.address);
    dest.writeString(this.image);
    dest.writeString(this.isPrebook);
    dest.writeLong(this.insertedDate != null ? this.insertedDate.getTime() : -1);
    dest.writeString(this.insertedBy);
    dest.writeString(this.modifiedBy);
    dest.writeLong(this.modifiedDate != null ? this.modifiedDate.getTime() : -1);
    dest.writeValue(this.isActive);
    dest.writeValue(this.isDeleted);
    dest.writeValue(this.parentId);
    dest.writeString(this.praentName);
    dest.writeString(this.parentMail);
    dest.writeString(this.parentMobile);
    dest.writeString(this.pRelation);
    dest.writeString(this.deviceId);
    dest.writeString(this.schoolId);
    dest.writeString(this.sectionId);
    dest.writeString(this.sectionClass);
    dest.writeString(this.password);
    dest.writeParcelable((Parcelable) this.loginStatus, flags);
    dest.writeParcelable((Parcelable) this.registrationDtl, flags);
  }

  protected Registration(Parcel in) {
    this.regdId = (Long) in.readValue(Long.class.getClassLoader());
    this.regdNo = in.readString();
    this.customerName = in.readString();
    this.email = in.readString();
    this.mobile = in.readString();
    long tmpDateOfBirth = in.readLong();
    this.dateOfBirth = tmpDateOfBirth == -1 ? null : new Date(tmpDateOfBirth);
    this.gender = in.readString();
    this.phone = in.readString();
    this.organisationName = in.readString();
    this.pincode = in.readString();
    this.address = in.readString();
    this.image = in.readString();
    this.isPrebook = in.readString();
    long tmpInsertedDate = in.readLong();
    this.insertedDate = tmpInsertedDate == -1 ? null : new Date(tmpInsertedDate);
    this.insertedBy = in.readString();
    this.modifiedBy = in.readString();
    long tmpModifiedDate = in.readLong();
    this.modifiedDate = tmpModifiedDate == -1 ? null : new Date(tmpModifiedDate);
    this.isActive = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.isDeleted = (Boolean) in.readValue(Boolean.class.getClassLoader());
    this.parentId = (Long) in.readValue(Long.class.getClassLoader());
    this.praentName = in.readString();
    this.parentMail = in.readString();
    this.parentMobile = in.readString();
    this.pRelation = in.readString();
    this.deviceId = in.readString();
    this.schoolId = in.readString();
    this.sectionId = in.readString();
    this.sectionClass = in.readString();
    this.password = in.readString();
    this.loginStatus = in.readParcelable(LoginStatus.class.getClassLoader());
    this.registrationDtl = in.readParcelable(RegistrationDtl.class.getClassLoader());
  }

  public static final Creator<Registration> CREATOR = new Creator<Registration>() {
    @Override
    public Registration createFromParcel(Parcel source) {
      return new Registration(source);
    }

    @Override
    public Registration[] newArray(int size) {
      return new Registration[size];
    }
  };
}
