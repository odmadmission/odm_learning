package com.sseduventures.digichamps.activity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.Model.Olympiads_model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.LearnActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.adapter.Olympiads_adapter;
import com.sseduventures.digichamps.domain.OlympiadDataResponse;
import com.sseduventures.digichamps.domain.OlympiadListData;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SessionManager;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Tech_1 on 1/6/2018.
 */

public class OlympiadsActivity extends FormActivity implements ServiceReceiver.Receiver {
    private ArrayList<Olympiads_model> list;
    private ArrayList<OlympiadListData> newList;

    private RecyclerView recyclerView;
    private Olympiads_adapter mAdapter;
    ImageView subject_img;
    private Toolbar toolbar;
    public String resp, error, TAG = "Olympiads", Boardid;
    int classid;
    private SessionManager session;
    String sub_name, sub_id, user_id;
    private SpotsDialog dialog;
    private RelativeLayout bottom_relative, sub_card_layout, no_sub_error_layout;
    private CoordinatorLayout sub_details_layouts;
    private AppBarLayout app;
    private CollapsingToolbarLayout collapsingToolbar;
    private EditText inputSearch;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private OlympiadDataResponse success;
    private CoordinatorLayout cordinateLay;
    private ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_olympiads);
        setModuleName(LogEventUtil.EVENT_Olympiad_Page);
        logEvent(LogEventUtil.KEY_Olympiads_PAGE,
                LogEventUtil.EVENT_Olympiads_PAGE);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        cordinateLay = (CoordinatorLayout) findViewById(R.id.cordinateLay);
        cordinateLay.requestFocus();
        init();

        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.welcome_backgroundColor));
        drawable.setSize(2, 1);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setFocusable(false);


        if (AppUtil.isInternetConnected(this)) {
            getOlympiadsNew();

        } else {
            startActivity(new Intent(OlympiadsActivity.this, Internet_Activity.class));

            finish();
        }

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });


    }


    private void filter(String text) {
        ArrayList<OlympiadListData> filteredList = new ArrayList<>();

        for (OlympiadListData item : newList) {
            if (item.getCompetitiveExamQs_PDFName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        mAdapter.filterList(filteredList);
    }


    public void ReturnHome(View view) {
        finish();
    }

    public void init() {
        //for listening keyboard event

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        CoordinatorLayout mainLayout = findViewById(R.id.content_learn); // You must use your root layout
        InputMethodManager im = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_laern_details);

        collapsingToolbarLayout.setTitle("Olympiads");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    getSupportActionBar().setDisplayShowTitleEnabled(true);

                    isShow = true;
                } else if (isShow) {
                    isShow = false;
                    getSupportActionBar().setDisplayShowTitleEnabled(false);

                }
            }
        });
        // get values
        user_id = String.valueOf(RegPrefManager.getInstance(this).getRegId());
        inputSearch = (EditText) findViewById(R.id.inputSearch);
        bottom_relative = (RelativeLayout) findViewById(R.id.bottomLayout);

        dialog = new SpotsDialog(this, "Fun+Education", R.style.Custom);

        session = new SessionManager(this);

        list = new ArrayList<>();
        newList = new ArrayList<>();
        toolbar = (Toolbar) findViewById(R.id.toolbar_learn_details);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Olympiads");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(OlympiadsActivity.this, LearnActivity.class);
                startActivity(in);
                finish();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.olympiads_recycler);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_laern_details);
        collapsingToolbar.setTitle("Olympiads");

        app = (AppBarLayout) findViewById(R.id.appbar);


        mAdapter = new Olympiads_adapter(newList, "", OlympiadsActivity.this);
        recyclerView.setAdapter(mAdapter);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private void getOlympiadsNew() {
        if (AppUtil.isInternetConnected(this)) {

            NetworkService.startActionGetOlympiads(this, mServiceReceiver);
        } else {
            Intent in = new Intent(OlympiadsActivity.this, Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode) {
            case ResponseCodes.SUCCESS:
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if (success != null && success.getSuccess() != null
                        && success.getSuccess().getList() != null
                        && success.getSuccess().getList().size() > 0) {


                    if (success.getSuccess().getList().size() == 0) {

                    } else {

                    }

                    newList.clear();
                    newList.addAll(success.getSuccess().getList());
                    mAdapter.notifyDataSetChanged();

                } else {

                }
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(OlympiadsActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(OlympiadsActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(OlympiadsActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }


    @Override
    protected void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);


    }

    @Override
    protected void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmer();
    }

    @Override
    protected void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }

}

