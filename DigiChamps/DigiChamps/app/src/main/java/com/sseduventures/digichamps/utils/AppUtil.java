package com.sseduventures.digichamps.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 * Created by Raju Satheesh on 2/3/2017.
 */

public class AppUtil {

    private static final String TAG="AppUtil";
    public static void copy(File src, File dst) {
        try {
            FileInputStream inStream = new FileInputStream(src);
            FileOutputStream outStream = new FileOutputStream(dst);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            inStream.close();
            outStream.close();
        } catch (Exception e) {

        }
    }
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    public static int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }
    /*
       checks permission (Both system and dangerous )
       It supports v4
     */
    public static boolean requireRunTimePermission(Context context,int code)
    {

        boolean status=false;
        if(code>Build.VERSION_CODES.LOLLIPOP_MR1)
        {
            status=true;
        }
        return status;
    }
    public static boolean isGpsEnabled(Context context){

        LocationManager lManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        if ((lManager.isProviderEnabled(LocationManager.GPS_PROVIDER)))
            return true;

        return  false;

    }
    public static String getRelativeDate(Context context,final String dateStr, final String whichFormat) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat(whichFormat);

        try {

            TimeZone tz= TimeZone.getDefault();
            final Date date = dateFormat.parse(dateStr);
            String format= DateUtils.getRelativeTimeSpanString(date.getTime() + tz.getRawOffset(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS)
                    .toString();
            if(format.contains("hours"))
            {
                return format.replace("hours", "hrs");
            }
            if(format.contains("seconds"))
            {
                return format.replace("seconds", "secs");
            }
            if(format.contains("minutes"))
            {
                return format.replace("minutes", "mins");
            }


            if(format.contains("hour"))
            {
                return format.replace("hour", "hr");
            }
            if(format.contains("second"))
            {
                return format.replace("second", "sec");
            }
            if(format.contains("minute"))
            {
                return format.replace("minute", "min");
            }

            if(format.contains("days")||format.contains("day"))
            {
                return format;
            }

            if(format.contains("weeks")||format.contains("week"))
            {
                return format;
            }

            if(format.contains("months")||format.contains("month"))
            {

                return format;
            }

            if(format.contains("years")||format.contains("year"))
            {
                return format;
            }
            else
                return format;




        } catch (Exception e) {

        }
        return "";
    }
    public static String getDateFormat(long time) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(new SimpleTimeZone(SimpleTimeZone.UTC_TIME, "UTC"));
        String t=sdf.format(new Date(time));
        return t;
    }
    public static  boolean checkLocationServices(Context con)
    {

        LocationManager lManager =(LocationManager) con.getSystemService(Context.LOCATION_SERVICE);
        if((lManager.isProviderEnabled(LocationManager.GPS_PROVIDER))||(lManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)))
        {
            return false;
        }
        else
        {
            return true;
        }

    }
    public static int getBatteryLevel(Context context) {


        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        float batteryPct = level / (float) scale;

        return ((int)(batteryPct * 100));
    }
    /*
       checks permission (Both system and dangerous )
       It supports v4
     */
    public static boolean checkPermission(Context context,String permission)
    {

        boolean status=false;
        if(ContextCompat.checkSelfPermission(context,
                permission) == PackageManager.PERMISSION_GRANTED)
        {
            status=true;
        }
        return status;
    }
    public static boolean isSimSupport(Context context)
    {
        boolean status=false;
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(!(tm.getSimState() == TelephonyManager.SIM_STATE_ABSENT))
        {
            status=true;
        }

        tm=null;
        return status;
    }
    public static String getIMEINumber(Context context) {

        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tManager.getDeviceId();


    }

    @SuppressWarnings("deprecation")
    public static String getNetworkMode(Context act) {
        ConnectivityManager connMgr = (ConnectivityManager)act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifi.isConnectedOrConnecting()) {
            return "WIFI";
        }
        else if (mobile.isConnectedOrConnecting()) {

            if(Build.VERSION.SDK_INT>Build.VERSION_CODES.ECLAIR_MR1)
            {
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_1xRTT)
                    return "1xRTT";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_CDMA)
                    return "CDMA";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_EDGE)
                    return "EDGE";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_EVDO_0)
                    return "EVDO 0";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_EVDO_A)
                    return "EVDO A";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_GPRS)
                    return "GPRS";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_HSDPA)
                    return "HSDPA";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_HSPA)
                    return "HSPA";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_HSUPA)
                    return "HSUPA";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_UMTS)
                    return "UMTS";
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB)
                    if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_EHRPD)
                        return "EHRPD";
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.FROYO)
                    if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_IDEN)
                        return "IDEN";
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.GINGERBREAD_MR1)
                    if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_EVDO_B)
                        return "EVDO B";
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB)
                    if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_LTE)
                        return "LTE";
                if(Build.VERSION.SDK_INT>Build.VERSION_CODES.HONEYCOMB_MR2)
                    if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_HSPAP)
                        return "HSPAP";
                if(mobile.getSubtype() ==  TelephonyManager.NETWORK_TYPE_UNKNOWN)
                    return "UNKNOWN";
            }
        }
        else
        {
            return "No Network";
        }
        return null;
    }

    public static void killProcessAndExit()
    {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }
    public static String getIsoNumber(Context context) {

        TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return tManager.getSimCountryIso();


    }
    public static boolean isInternetConnected(final Context context) {
        boolean isConnected = false;
        if (context != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null) {
                isConnected = networkInfo.isConnectedOrConnecting();
            }
        }
        return isConnected;
    }


    public static void changeSystemBarsDim(View view,int flag)
    {
        view.setSystemUiVisibility(flag);
    }
    public static View changeSystemBarsDim(Activity activity, int flag)
    {
       View decorView=activity.getWindow().getDecorView();
             decorView.setSystemUiVisibility(flag);

        return decorView;
    }
    public static View hideStatusbar(Activity activity)
    {
        View decorView=null;
        if (Build.VERSION.SDK_INT < 16) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            decorView = activity.getWindow().getDecorView();
// Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ((AppCompatActivity)activity).getSupportActionBar().hide();
        }

        return decorView;


    }
    public static View hideNavigationbar(Activity activity)
    {
        View decorView=null;
        if (Build.VERSION.SDK_INT < 16) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            decorView = activity.getWindow().getDecorView();
// Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            ;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ((AppCompatActivity)activity).getSupportActionBar().hide();
        }

        return decorView;


    }
    public static View hideSystemUibars(Activity activity)
    {
        View decorView=null;
        if (Build.VERSION.SDK_INT < 16) {
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            decorView = activity.getWindow().getDecorView();
// Hide the status bar.
            int uiOptions =
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE|View.SYSTEM_UI_FLAG_LOW_PROFILE;

            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ((AppCompatActivity)activity).getSupportActionBar().hide();
        }

        return decorView;


    }
    public static void showSystembars(Activity activity,View decorView)
    {

        if (Build.VERSION.SDK_INT < 16) {
            activity.getWindow().setFlags(0,0);
        }
        else
        {

            int uiOptions = 0;
            decorView.setSystemUiVisibility(uiOptions);
// Remember that you should never show the action bar if the
// status bar is hidden, so hide that too if necessary.
            ((AppCompatActivity)activity).getSupportActionBar().show();
        }

    }
    private static void hideSystemUI(Activity activity,View mDecorView) {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
        ((AppCompatActivity)activity).getSupportActionBar().hide();
    }

    // This snippet shows the system bars. It does this by removing all the flags
// except for the ones that make the content appear under the system bars.
    private static void showSystemUI(Activity activity,View mDecorView) {
        mDecorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        ((AppCompatActivity)activity).getSupportActionBar().show();
    }
    public static void toggleHideBar(Activity activity) {

        // BEGIN_INCLUDE (get_current_ui_flags)
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions =activity.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        // END_INCLUDE (get_current_ui_flags)
        // BEGIN_INCLUDE (toggle_ui_flags)
        boolean isImmersiveModeEnabled =
                ((uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY) == uiOptions);
        if (isImmersiveModeEnabled) {
            //Log.i(TAG, "Turning immersive mode mode off. ");
        } else {
            //Log.i(TAG, "Turning immersive mode mode on.");
        }

        // Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        // Immersive mode: Backward compatible to KitKat.
        // Note that this flag doesn't do anything by itself, it only augments the behavior
        // of HIDE_NAVIGATION and FLAG_FULLSCREEN.  For the purposes of this sample
        // all three flags are being toggled together.
        // Note that there are two immersive mode UI flags, one of which is referred to as "sticky".
        // Sticky immersive mode differs in that it makes the bottom_navigation and status bars
        // semi-transparent, and the UI flag does not get cleared when the user interacts with
        // the screen.
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        activity.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
        //END_INCLUDE (set_ui_flags)
    }

    public static DisplayMetrics getDeviceDisplayMetrice(Activity activity)
    {
        DisplayMetrics cuDisplayMetrics=new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(cuDisplayMetrics);
        return cuDisplayMetrics;
    }

    public static boolean resolveIntent(Context context,Intent intent)
    {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent,0);
        return resInfo.size()>0?true:false;
    }

     public static Intent getPdfIntent(Context context,String filepath)
     {
         File file = new File(filepath);
         Intent intent = new Intent(Intent.ACTION_VIEW);
         intent.setDataAndType(Uri.fromFile(file), "application/pdf");
         intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

         if(resolveIntent(context,intent))
         return intent;
         else
             return null;
     }

    public static Intent getAudioIntent(Context context,String filepath)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        File file = new File(filepath);
        intent.setDataAndType(Uri.fromFile(file), "audio/*");
        if(resolveIntent(context,intent))
            return intent;
        else
            return null;
    }
    public static Intent getVideoIntent(Context context,String filepath)
    {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        File file = new File(filepath);
        intent.setDataAndType(Uri.fromFile(file), "video/*");
        if(resolveIntent(context,intent))
            return intent;
        else
            return null;
    }


    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
    public static String getOnline(final Context context) {

        String type =  "MOBILE DATA";
        if (context != null) {
            final ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            final NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null) {
                int t = networkInfo.getType();

                switch (t) {

                    case 0:
                        type = "MOBILE DATA";
                        break;
                    case 1:
                        type = "WIFI";
                        break;


                }
            }
        }
        return type;
    }
    public static boolean isMyServiceRunning(Context context,Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
    //rest service
    public static boolean checkNetworkProperties(final Context context) {
        return true;
    }
    public static Thread runAsynchronous(final Runnable runnable) {
        final Thread thread = new Thread(runnable);
        thread.start();
        return thread;
    }
    public static void runSynchronous(final Runnable runnable) {
        final Thread thread = runAsynchronous(runnable);
        try {
            thread.join();
        } catch (InterruptedException ex) {
            //Log.i(TAG, "Exception as runnable " + runnable.getClass().getSimpleName() + " was interrupted.");
        }
    }
    public  static String getDeviceId(Context context)
    {

        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getAppVersionName(Context context)
    {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String installedVersion = pInfo.versionName;
             return  installedVersion;
        }
        catch (Exception e)
        {
            ////Log.v("About",e.getMessage());
            return null;
        }
    }
    public static int getAppVersionCode(Context context)
    {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            int installedVersion = pInfo.versionCode;
            return  installedVersion;
        }
        catch (Exception e)
        {
            ////Log.v("About",e.getMessage());
            return 0;
        }
    }

    public static void checkNewVersionInPlayStore(Context context)
    {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));


        }
        catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
        // "https://play.google.com/store/apps/details?id=com.pack.surfmug&hl=en"

    }

    public static  void hideSoftKeyboard(Context context,EditText editText)
    {
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


}
