package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class ChapterDetailsRequestModel implements Parcelable {

    public ChapterDetailsRequestModel() {

    }

    public static final Creator<ChapterDetailsRequestModel> CREATOR = new Creator<ChapterDetailsRequestModel>() {
        @Override
        public ChapterDetailsRequestModel createFromParcel(Parcel in) {
            return new ChapterDetailsRequestModel();
        }

        @Override
        public ChapterDetailsRequestModel[] newArray(int size) {
            return new ChapterDetailsRequestModel[size];
        }
    };

    public String getChap_ID() {
        return chap_ID;
    }

    public void setChap_ID(String chap_ID) {
        this.chap_ID = chap_ID;
    }

    public String getRegId() {
        return regId;
    }

    public void setRegId(String regId) {
        this.regId = regId;
    }

    private String chap_ID;
    private String regId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chap_ID);
        dest.writeString(regId);
    }
}
