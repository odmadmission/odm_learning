package com.sseduventures.digichamps.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ntspl24 on 6/9/2017.
 */

public class Chapter_detail_Model implements Parcelable {
    private String Module_Id, Module_Title, Description, Module_Image,
            Image_Key, Is_Avail, pdf_file, Is_Free, Validity, isExpire,
            Question_Pdf, No_Of_Question, Is_Free_Test, file_resource, isBookmarked;

    private boolean isFlag;


    public Chapter_detail_Model(String Module_Id, String Module_Title,
                                String Description, String Module_Image,
                                String Image_Key, String Is_Avail, String pdf_file,
                                String Is_Free, String Validity, String isExpire,
                                String Question_Pdf, String No_Of_Question,
                                String Is_Free_Test, String file_resource, String isBookmarked) {
        this.Module_Id = Module_Id;
        this.Module_Title = Module_Title;
        this.Description = Description;
        this.Module_Image = Module_Image;
        this.Image_Key = Image_Key;
        this.Is_Avail = Is_Avail;
        this.pdf_file = pdf_file;
        this.Is_Free = Is_Free;
        this.Validity = Validity;
        this.Question_Pdf = Question_Pdf;
        this.No_Of_Question = No_Of_Question;
        this.Is_Free_Test = Is_Free_Test;
        this.file_resource = file_resource;
        this.isExpire = isExpire;
        this.isBookmarked = isBookmarked;
    }

    public boolean isFlag() {
        return isFlag;
    }

    public void setFlag(boolean flag) {
        isFlag = flag;
    }


    public void setModule_Id(String module_Id) {
        Module_Id = module_Id;
    }

    public void setModule_Title(String module_Title) {
        Module_Title = module_Title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setModule_Image(String module_Image) {
        Module_Image = module_Image;
    }

    public void setImage_Key(String image_Key) {
        Image_Key = image_Key;
    }

    public void setIs_Avail(String is_Avail) {
        Is_Avail = is_Avail;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public void setIs_Free(String is_Free) {
        Is_Free = is_Free;
    }

    public void setValidity(String validity) {
        Validity = validity;
    }

    public void setIsExpire(String isExpire) {
        this.isExpire = isExpire;
    }

    public void setQuestion_Pdf(String question_Pdf) {
        Question_Pdf = question_Pdf;
    }

    public void setNo_Of_Question(String no_Of_Question) {
        No_Of_Question = no_Of_Question;
    }

    public void setIs_Free_Test(String is_Free_Test) {
        Is_Free_Test = is_Free_Test;
    }

    public void setFile_resource(String file_resource) {
        this.file_resource = file_resource;
    }

    public void setIsBookmarked(String isBookmarked) {
        this.isBookmarked = isBookmarked;
    }

    public String getModule_Id() {
        return Module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public String getpdf_file() {
        return pdf_file;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public String getValidity() {
        return Validity;
    }

    public String getQuestion_Pdf() {
        return Question_Pdf;
    }

    public String getNo_Of_Question() {
        return No_Of_Question;
    }

    public String getIs_Free_Test() {
        return Is_Free_Test;
    }

    public String get_file_resource() {
        return file_resource;
    }

    public String getVideoExpiry() {
        return isExpire;
    }

    public String getIsBookmarked() {
        return isBookmarked;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Module_Id);
        dest.writeString(this.Module_Title);
        dest.writeString(this.Description);
        dest.writeString(this.Module_Image);
        dest.writeString(this.Image_Key);
        dest.writeString(this.Is_Avail);
        dest.writeString(this.pdf_file);
        dest.writeString(this.Is_Free);
        dest.writeString(this.Validity);
        dest.writeString(this.isExpire);
        dest.writeString(this.Question_Pdf);
        dest.writeString(this.No_Of_Question);
        dest.writeString(this.Is_Free_Test);
        dest.writeString(this.file_resource);
        dest.writeString(this.isBookmarked);
    }

    protected Chapter_detail_Model(Parcel in) {
        this.Module_Id = in.readString();
        this.Module_Title = in.readString();
        this.Description = in.readString();
        this.Module_Image = in.readString();
        this.Image_Key = in.readString();
        this.Is_Avail = in.readString();
        this.pdf_file = in.readString();
        this.Is_Free = in.readString();
        this.Validity = in.readString();
        this.isExpire = in.readString();
        this.Question_Pdf = in.readString();
        this.No_Of_Question = in.readString();
        this.Is_Free_Test = in.readString();
        this.file_resource = in.readString();
        this.isBookmarked = in.readString();
    }

    public static final Creator<Chapter_detail_Model> CREATOR = new Creator<Chapter_detail_Model>() {
        @Override
        public Chapter_detail_Model createFromParcel(Parcel source) {
            return new Chapter_detail_Model(source);
        }

        @Override
        public Chapter_detail_Model[] newArray(int size) {
            return new Chapter_detail_Model[size];
        }
    };
}





