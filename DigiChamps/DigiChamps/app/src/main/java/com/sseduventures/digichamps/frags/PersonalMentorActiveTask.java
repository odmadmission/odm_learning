package com.sseduventures.digichamps.frags;

import android.app.Dialog;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.PersonalMentorActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.adapter.PMActiveAdapter;
import com.sseduventures.digichamps.domain.PMActivetaskDataList;
import com.sseduventures.digichamps.domain.PMResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class PersonalMentorActiveTask extends Fragment implements ServiceReceiver.Receiver {


    private RecyclerView rv_active;
    private TextView no_activetask;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private PMResponse success;
    private FormActivity mContext;
    private List<PMActivetaskDataList> pmActivetaskDataLists;
    private PMActiveAdapter pmActiveAdapter;

    PersonalMentorActivity context;
    private ShimmerFrameLayout mShimmerViewContainer;


    public PersonalMentorActiveTask() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active_task, container, false);
        ((FormActivity)getActivity()).logEvent(LogEventUtil.
                        KEY_pm_active_task,
                LogEventUtil.EVENT_pm_active_task);

        ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_pm_active_task);
        init(view);

        return view;
    }

    private void init(View view) {

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);
        rv_active = view.findViewById(R.id.rv_active);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        no_activetask = view.findViewById(R.id.no_activetask);
        no_activetask.setVisibility(View.GONE);
        mContext = (PersonalMentorActivity) getActivity();
        pmActivetaskDataLists = new ArrayList<>();
        context = (PersonalMentorActivity) getActivity();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rv_active.setLayoutManager(mLayoutManager);


        pmActiveAdapter = new PMActiveAdapter(pmActivetaskDataLists, context);
        rv_active.setAdapter(pmActiveAdapter);


        getPmDetails();
    }

    private void getPmDetails() {

        if (AppUtil.isInternetConnected(getActivity())) {
            NetworkService.startActionGetPmDetails(getActivity(), mServiceReceiver);
        } else {
            mContext.showToast("No Internet Connection");
        }
    }

    @Override
    public void onStart() {
        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    public void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {


                mShimmerViewContainer.stopShimmer();
                mShimmerViewContainer.setVisibility(View.GONE);


        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                mContext.hideDialog();
                Intent in = new Intent(getActivity(), ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                mContext.hideDialog();
                Intent intent = new Intent(getActivity(), Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                mContext.hideDialog();
                Intent inin = new Intent(getActivity(), ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:
                mContext.hideDialog();
                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                setActiveTask();


                break;
        }
    }

    private void setActiveTask() {
        if (success != null
                && success.getSuccessresultTask() != null
                && success.getSuccessresultTask().getActiveTaskList() != null
                && success.getSuccessresultTask().getActiveTaskList()
                .getTaskDataList() != null
                && success.getSuccessresultTask().getActiveTaskList()
                .getTaskDataList().size() > 0) {
            rv_active.setVisibility(View.VISIBLE);
            no_activetask.setVisibility(View.GONE);
            pmActivetaskDataLists.clear();
            pmActivetaskDataLists.addAll(success.getSuccessresultTask().getActiveTaskList()
                    .getTaskDataList());
            pmActiveAdapter.notifyDataSetChanged();
        } else {
            rv_active.setVisibility(GONE);
            no_activetask.setVisibility(View.VISIBLE);

        }
    }

    public void showItemDialog(String taskName, String taskType, String taskDetails, String startTime, String endTime) {
        final Dialog dialog = new Dialog(getActivity().getApplicationContext());
        dialog.setContentView(R.layout.mentor_custom_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView title = dialog.findViewById(R.id.title);
        TextView taskT = dialog.findViewById(R.id.task_type);
        TextView detailsTask = dialog.findViewById(R.id.txt_task_details);
        TextView taskStart = dialog.findViewById(R.id.start_time);
        TextView taskEnd = dialog.findViewById(R.id.deadline_time);


        title.setText(taskName.trim());
        taskT.setText(taskType);
        detailsTask.setText(taskDetails.trim());
        taskStart.setText("Start Date: " + startTime);
        taskEnd.setText("End Date: " + endTime);


        dialog.show();

    }
}
