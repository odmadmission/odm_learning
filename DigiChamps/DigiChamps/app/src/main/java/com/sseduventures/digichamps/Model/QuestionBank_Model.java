package com.sseduventures.digichamps.Model;

/**
 * Created by CHANDRA BHANU on 19-Jan-18.
 */

public class QuestionBank_Model {

    String moduleName;
    String url;
    String qbmoduleId;
    String isQBBookMarked;

    public QuestionBank_Model(String moduleName, String url,String qbmoduleId,String isQBBookMarked) {
        this.moduleName = moduleName;
        this.url = url;
        this.qbmoduleId = qbmoduleId;
        this.isQBBookMarked = isQBBookMarked;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getQbmoduleId() {
        return qbmoduleId;
    }

    public void setQbmoduleId(String qbmoduleId) {
        this.qbmoduleId = qbmoduleId;
    }

    public String getIsQBBookMarked() {
        return isQBBookMarked;
    }

    public void setIsQBBookMarked(String isQBBookMarked) {
        this.isQBBookMarked = isQBBookMarked;
    }
}
