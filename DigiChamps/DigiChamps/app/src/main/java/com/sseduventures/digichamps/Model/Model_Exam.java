package com.sseduventures.digichamps.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Property;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ntspl22 on 4/29/2017.
 */

public class Model_Exam implements Parcelable {

    private String question_no , question_id, question,question_desc,option;
    private int ques_img1,ques_img2,optionA_img1, optionB_img1,optionC_img1,optionD_img1;
    private ArrayList<Model_option> Alloption;
    private ArrayList<String> Image;
    private int mData;

    //Model_Exam(question_no , question_id, question,question_desc, Image , Alloption)
    public Model_Exam(String question_no, String question_id,
                      String question, String question_desc,ArrayList Image,ArrayList<Model_option > Alloption) {

        this.question_no = question_no;
        this.question_id = question_id;
        this.question = question;
        this.question_desc = question_desc;
        this.Image = Image;
        this.Alloption = Alloption;

    }

    protected Model_Exam(Parcel in) {
        question_no = in.readString();
        question_id = in.readString();
        question = in.readString();
        question_desc = in.readString();
        option = in.readString();
        ques_img1 = in.readInt();
        ques_img2 = in.readInt();
        optionA_img1 = in.readInt();
        optionB_img1 = in.readInt();
        optionC_img1 = in.readInt();
        optionD_img1 = in.readInt();
        Alloption = in.createTypedArrayList(Model_option.CREATOR);
        Image = in.createStringArrayList();
        mData = in.readInt();
    }

    public static final Creator<Model_Exam> CREATOR = new Creator<Model_Exam>() {
        @Override
        public Model_Exam createFromParcel(Parcel in) {
            return new Model_Exam(in);
        }

        @Override
        public Model_Exam[] newArray(int size) {
            return new Model_Exam[size];
        }
    };

    public ArrayList<Model_option> getanswer() {

        return Alloption;
    }
    public String getQuestion_number() {
        return question_no;
    }
    public String getQuestion_Id() {
        return question_id;
    }
    public String getQuestion() {
        return question;
    }
    public ArrayList<Model_option> getoptions() {
        return Alloption;
    }
    public ArrayList<String> getImages() {
        return Image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(question_no);
        dest.writeString(question_id);
        dest.writeString(question);
        dest.writeString(question_desc);
        dest.writeString(option);
        dest.writeInt(ques_img1);
        dest.writeInt(ques_img2);
        dest.writeInt(optionA_img1);
        dest.writeInt(optionB_img1);
        dest.writeInt(optionC_img1);
        dest.writeInt(optionD_img1);
        dest.writeTypedList(Alloption);
        dest.writeStringList(Image);
        dest.writeInt(mData);
    }


}