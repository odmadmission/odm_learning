package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/4/2018.
 */

public class TestHighlightsDifficultys implements Parcelable{


    private int No_of_Question;
    private int Power_ID;
    private String Power_Type;

    public int getNo_of_Question() {
        return No_of_Question;
    }

    public void setNo_of_Question(int no_of_Question) {
        No_of_Question = no_of_Question;
    }

    public int getPower_ID() {
        return Power_ID;
    }

    public void setPower_ID(int power_ID) {
        Power_ID = power_ID;
    }

    public String getPower_Type() {
        return Power_Type;
    }

    public void setPower_Type(String power_Type) {
        Power_Type = power_Type;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.No_of_Question);
        dest.writeInt(this.Power_ID);
        dest.writeString(this.Power_Type);
    }

    public TestHighlightsDifficultys() {
    }

    protected TestHighlightsDifficultys(Parcel in) {
        this.No_of_Question = in.readInt();
        this.Power_ID = in.readInt();
        this.Power_Type = in.readString();
    }

    public static final Creator<TestHighlightsDifficultys> CREATOR = new Creator<TestHighlightsDifficultys>() {
        @Override
        public TestHighlightsDifficultys createFromParcel(Parcel source) {
            return new TestHighlightsDifficultys(source);
        }

        @Override
        public TestHighlightsDifficultys[] newArray(int size) {
            return new TestHighlightsDifficultys[size];
        }
    };
}
