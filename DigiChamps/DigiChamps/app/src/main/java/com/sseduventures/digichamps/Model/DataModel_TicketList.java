package com.sseduventures.digichamps.Model;

/**
 * Created by ARR on 4/28/2017.
 */

public class DataModel_TicketList {

    private String ticketID, ticketNo, status, sub, date;


    public DataModel_TicketList() {

    }

    public DataModel_TicketList(String ticketNo, String status, String sub, String date, String ticketID) {
        this.ticketNo = ticketNo;
        this.status = status;
        this.sub = sub;
        this.date = date;
        this.ticketID = ticketID;
    }

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {this.status = status;}

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {this.sub = sub;}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}