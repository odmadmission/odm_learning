package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/24/2018.
 */

public class SubjectchapList implements Parcelable {


    private int subId;
    private int chapId;
    private String chapName;

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public int getChapId() {
        return chapId;
    }

    public void setChapId(int chapId) {
        this.chapId = chapId;
    }

    public String getChapName() {
        return chapName;
    }

    public void setChapName(String chapName) {
        this.chapName = chapName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.subId);
        dest.writeInt(this.chapId);
        dest.writeString(this.chapName);
    }

    public SubjectchapList() {
    }

    protected SubjectchapList(Parcel in) {
        this.subId = in.readInt();
        this.chapId = in.readInt();
        this.chapName = in.readString();
    }

    public static final Creator<SubjectchapList> CREATOR = new Creator<SubjectchapList>() {
        @Override
        public SubjectchapList createFromParcel(Parcel source) {
            return new SubjectchapList(source);
        }

        @Override
        public SubjectchapList[] newArray(int size) {
            return new SubjectchapList[size];
        }
    };
}
