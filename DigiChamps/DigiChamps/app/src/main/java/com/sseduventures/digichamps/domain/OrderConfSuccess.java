package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class OrderConfSuccess implements Parcelable{

    private OrderConf get_confdata;

    public OrderConf getGet_confdata() {
        return get_confdata;
    }

    public void setGet_confdata(OrderConf get_confdata) {
        this.get_confdata = get_confdata;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) this.get_confdata, flags);
    }

    public OrderConfSuccess() {
    }

    protected OrderConfSuccess(Parcel in) {
        this.get_confdata = in.readParcelable(OrderConf.class.getClassLoader());
    }

    public static final Creator<OrderConfSuccess> CREATOR = new Creator<OrderConfSuccess>() {
        @Override
        public OrderConfSuccess createFromParcel(Parcel source) {
            return new OrderConfSuccess(source);
        }

        @Override
        public OrderConfSuccess[] newArray(int size) {
            return new OrderConfSuccess[size];
        }
    };
}
