package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class BoardRequest implements Parcelable{

    private String SchoolId;

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
    }

    public BoardRequest() {
    }

    protected BoardRequest(Parcel in) {
        this.SchoolId = in.readString();
    }

    public static final Creator<BoardRequest> CREATOR = new Creator<BoardRequest>() {
        @Override
        public BoardRequest createFromParcel(Parcel source) {
            return new BoardRequest(source);
        }

        @Override
        public BoardRequest[] newArray(int size) {
            return new BoardRequest[size];
        }
    };
}
