package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class LearnDetailsResponse implements Parcelable {

    private LearnDetailsSuccess success;

    protected LearnDetailsResponse(Parcel in) {
        success = in.readParcelable(LearnDetailsSuccess.class.getClassLoader());
    }

    public static final Creator<LearnDetailsResponse> CREATOR = new Creator<LearnDetailsResponse>() {
        @Override
        public LearnDetailsResponse createFromParcel(Parcel in) {
            return new LearnDetailsResponse(in);
        }

        @Override
        public LearnDetailsResponse[] newArray(int size) {
            return new LearnDetailsResponse[size];
        }
    };

    public LearnDetailsSuccess getSuccess() {
        return success;
    }

    public void setSuccess(LearnDetailsSuccess success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(success, flags);
    }
}
