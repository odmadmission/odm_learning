package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class DiscussionRequest implements Parcelable{

    private String SchoolId;
    private int PageSize;
    private int StartIndex;


    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }

    public int getStartIndex() {
        return StartIndex;
    }

    public void setStartIndex(int startIndex) {
        StartIndex = startIndex;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.SchoolId);
        dest.writeInt(this.PageSize);
        dest.writeInt(this.StartIndex);
    }

    public DiscussionRequest() {
    }

    protected DiscussionRequest(Parcel in) {
        this.SchoolId = in.readString();
        this.PageSize = in.readInt();
        this.StartIndex = in.readInt();
    }

    public static final Creator<DiscussionRequest> CREATOR = new Creator<DiscussionRequest>() {
        @Override
        public DiscussionRequest createFromParcel(Parcel source) {
            return new DiscussionRequest(source);
        }

        @Override
        public DiscussionRequest[] newArray(int size) {
            return new DiscussionRequest[size];
        }
    };
}
