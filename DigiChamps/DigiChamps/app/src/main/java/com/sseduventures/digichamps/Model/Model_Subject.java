package com.sseduventures.digichamps.Model;

/**
 * Created by ntspl22 on 6/6/2017.
 */

public class Model_Subject {
    private String sub_name;
    private String sub_id;
    public Model_Subject(String sub_name, String sub_id) {

        this.sub_id = sub_id;
        this.sub_name = sub_name;
    }
    public String getSub_id() {
        return sub_id;
    }

    public String getSub_name() {
        return sub_name;
    }

}
