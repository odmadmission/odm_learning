package com.sseduventures.digichamps.Model;

import com.sseduventures.digichamps.R;


public enum ModelObject_Package {

    RED(R.string.red, R.layout.first_package),
    BLUE(R.string.blue, R.layout.second_package),
    GREEN(R.string.green, R.layout.third_package),
    BLACK(R.string.black, R.layout.fourth_package);

    private int mTitleResId;
    private int mLayoutResId;

    ModelObject_Package(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}