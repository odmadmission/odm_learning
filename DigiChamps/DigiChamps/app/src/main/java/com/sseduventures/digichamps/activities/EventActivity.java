package com.sseduventures.digichamps.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.frags.EventFragment_1;
import com.sseduventures.digichamps.frags.EventFragment_2;
import com.sseduventures.digichamps.frags.EventFragment_3;
import com.sseduventures.digichamps.frags.LearnAndEarnFragmentGift;
import com.sseduventures.digichamps.frags.LearnAndEarnFragmentLeadership;
import com.sseduventures.digichamps.frags.LearnAndEarnFragmentPoint;

import java.util.ArrayList;
import java.util.List;

public class EventActivity extends FormActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CollapsingToolbarLayout collapsing_toolbar;
    AppBarLayout appBarLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_event);
        setModuleName(LogEventUtil.EVENT_LEARN_EARN_PAGE);
        logEvent(LogEventUtil.KEY_LEARN_EARN_PAGE,
                LogEventUtil.EVENT_LEARN_EARN_PAGE);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        appBarLayout = findViewById(R.id.appbar);
        collapsing_toolbar = findViewById(R.id.collapsing_toolbar);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Event");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        EventActivity.ViewPagerAdapter adapter = new EventActivity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new EventFragment_1(), "Competition");
        adapter.addFragment(new EventFragment_2(), "Workshop");
        adapter.addFragment(new EventFragment_3(), "Registration");

        viewPager.setAdapter(adapter);

    }

    @Override
    public void onBackPressed() {

        Intent in = new Intent(EventActivity.this, NewDashboardActivity.class);
        startActivity(in);

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);

        }

        @Override
        public int getCount() {
            return mFragmentList.size();

        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);

        }
    }
}
