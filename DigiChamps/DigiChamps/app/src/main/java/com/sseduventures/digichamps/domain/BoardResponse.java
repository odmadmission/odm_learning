package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class BoardResponse implements Parcelable{

    private List<BoardListData> list;
    private boolean status;
    private String message;

    public List<BoardListData> getList() {
        return list;
    }

    public void setList(List<BoardListData> list) {
        this.list = list;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
        dest.writeByte(this.status ? (byte) 1 : (byte) 0);
        dest.writeString(this.message);
    }

    public BoardResponse() {
    }

    protected BoardResponse(Parcel in) {
        this.list = in.createTypedArrayList(BoardListData.CREATOR);
        this.status = in.readByte() != 0;
        this.message = in.readString();
    }

    public static final Creator<BoardResponse> CREATOR = new Creator<BoardResponse>() {
        @Override
        public BoardResponse createFromParcel(Parcel source) {
            return new BoardResponse(source);
        }

        @Override
        public BoardResponse[] newArray(int size) {
            return new BoardResponse[size];
        }
    };
}
