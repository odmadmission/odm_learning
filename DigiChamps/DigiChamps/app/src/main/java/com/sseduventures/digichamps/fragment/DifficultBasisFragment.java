package com.sseduventures.digichamps.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.sseduventures.digichamps.Model.DifficultBasis_Model;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activity.ErrorActivity;
import com.sseduventures.digichamps.activity.Internet_Activity;
import com.sseduventures.digichamps.activity.TestStatisticsActivity;
import com.sseduventures.digichamps.domain.TestHighLightsDifficulty;
import com.sseduventures.digichamps.domain.TestHighlightsResponse;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.helper.MyYAxisValueFormatter;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.helper.SpotsDialog;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.sseduventures.digichamps.webservice.ResponseCodes;
import java.util.ArrayList;
import static com.facebook.FacebookSdk.getApplicationContext;


public class DifficultBasisFragment extends android.support.v4.app.Fragment implements ServiceReceiver.Receiver {
    private SpotsDialog dialog;
    private String resp,error,TAG="TestStatistics";
    ArrayList<DifficultBasis_Model>difficultyList;
    ArrayList<DifficultBasis_Model>typeList;
    ArrayList<TestHighLightsDifficulty> dList;
    private ArrayList<BarEntry> yVals1,yVals2,yVals3;
    private TextView total1,total2,total3;
    private TextView totalC1,totalC2,totalC3;
    private TextView totalIC1,totalIC2,totalIC3;
    private TextView totalSK1,totalSK2,totalSK3;
    private BarChart totalB1,totalB2,totalB3;
    CardView card_easy,card_difficulty2,card_difficulty3;
    String user_id;
    TextView type1,type2,type3;
    String resId;
    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private TestHighlightsResponse success;

    private FormActivity mContext;
    private ShimmerFrameLayout mShimmerViewContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_difficult_basis, container, false);
        init(view);
        if(getActivity() instanceof FormActivity)
        {
            ((FormActivity)getActivity()).setModuleName(LogEventUtil.EVENT_difficulty_basis_frag);
            ((FormActivity)getActivity()).logEvent(LogEventUtil.KEY_difficulty_basis_frag,
                    LogEventUtil.EVENT_difficulty_basis_frag);
        }

        SharedPreferences preference = getActivity().getSharedPreferences("user_data", getApplicationContext().MODE_PRIVATE);
        user_id = preference.getString("User_ID", null);
        resId=TestStatisticsActivity.getResultId();

        if(AppUtil.isInternetConnected(getActivity())){
            //getResult(resId);
            getResultNew();
        }else {
            startActivity(new Intent(getContext(), Internet_Activity.class));
            Activity activity = (Activity) getContext();

            activity.finish();
        }

        return view;
    }

    private void init(View view) {

        mHandler=new Handler();
        mServiceReceiver=new ServiceReceiver(mHandler);
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mContext = (TestStatisticsActivity) getActivity();
        dList=new ArrayList<>();
        yVals1=new ArrayList<>();
        yVals2=new ArrayList<>();
        yVals3=new ArrayList<>();
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        difficultyList = new ArrayList<>();
        typeList = new ArrayList<>();
        //yVals1 = new ArrayList<BarEntry>();


        pieChart=view.findViewById(R.id.chart1pie);
        pieChart2=view.findViewById(R.id.chart2pie);
        pieChart3=view.findViewById(R.id.chart3pie);

        total1=view.findViewById(R.id.total_que1);
        total2=view.findViewById(R.id.total_que2);
        total3=view.findViewById(R.id.total_que3);

        totalC1=view.findViewById(R.id.correct1);
        totalC2=view.findViewById(R.id.correct2);
        totalC3=view.findViewById(R.id.correct3);

        totalIC1=view.findViewById(R.id.incorrect1);
        totalIC2=view.findViewById(R.id.incorrect2);
        totalIC3=view.findViewById(R.id.incorrect3);

        totalSK1=view.findViewById(R.id.skipped1);
        totalSK2=view.findViewById(R.id.skipped2);
        totalSK3=view.findViewById(R.id.skipped3);

        totalB1=view.findViewById(R.id.chart1);
        totalB2=view.findViewById(R.id.chart2);
        totalB3=view.findViewById(R.id.chart3);

        type1=view.findViewById(R.id.type1);
        type2=view.findViewById(R.id.type2);
        type3=view.findViewById(R.id.type3);

        card_easy = view.findViewById(R.id.card_easy);
        card_difficulty2 = view.findViewById(R.id.card_difficulty2);
        card_difficulty3 = view.findViewById(R.id.card_difficulty3);


        type1.setVisibility(View.GONE);
        type2.setVisibility(View.GONE);
        type3.setVisibility(View.GONE);

        card_easy.setVisibility(View.GONE);
        card_difficulty2.setVisibility(View.GONE);
        card_difficulty3.setVisibility(View.GONE);



    }



    private void getResultNew(){

        int resultId = 0;
        if(resId!=null)
        {
            resultId=Integer.parseInt(resId);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("Result_ID",resultId);
        if(AppUtil.isInternetConnected(getActivity())){

            //mContext.showDialog(null,"Please wait");
            NetworkService.startActionGetTestHighlights(getActivity(),mServiceReceiver,bundle);
        }else{
            Intent in = new Intent(getActivity(),Internet_Activity.class);
            startActivity(in);
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //Toast.makeText(getApplicationContext(), "Yes"+resultCode, Toast.LENGTH_SHORT).show();
        //mContext.hideDialog();

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        switch (resultCode)
        {
            case ResponseCodes.SUCCESS:
                success=resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success!=null&&success.getSuccess()!=null
                        && success.getSuccess().getDifficulty()!=null
                        && success.getSuccess().getDifficulty().size()>0){

                    for (int n = 0; n < success.getSuccess().getDifficulty().size(); n++) {

                        int powerId = success.getSuccess().getDifficulty().get(n).getPowerId();
                        int TotalQuestions = success.getSuccess().getDifficulty().get(n).getTotalQuestions();
                        int  TotalCorrect = success.getSuccess().getDifficulty().get(n).getTotalCorrect();
                        int TotalInCorrect = success.getSuccess().getDifficulty().get(n).getTotalInCorrect();
                        int TotalSkipped = success.getSuccess().getDifficulty().get(n).getTotalSkipped();
                        int Acurasy = success.getSuccess().getDifficulty().get(n).getAccuracy();
                        String power=null;
                        switch (powerId)
                        {
                            case 1:
                                power="Easy";

                                entries1.add(new PieEntry(TotalCorrect));
                                entries1.add(new PieEntry(TotalInCorrect));
                                entries1.add(new PieEntry(TotalSkipped));

                                yVals1.add(new BarEntry(0,TotalCorrect));
                                yVals1.add(new BarEntry(1,TotalInCorrect));
                                yVals1.add(new BarEntry(2,TotalSkipped));
                                break;
                            case 2:
                                power="Medium";

                                entries2.add(new PieEntry(TotalCorrect));
                                entries2.add(new PieEntry(TotalInCorrect));
                                entries2.add(new PieEntry(TotalSkipped));


                                yVals2.add(new BarEntry(0,TotalCorrect));
                                yVals2.add(new BarEntry(1,TotalInCorrect));
                                yVals2.add(new BarEntry(2,TotalSkipped));
                                break;
                            case 3:
                                power="Difficulty";

                                entries3.add(new PieEntry(TotalCorrect));
                                entries3.add(new PieEntry(TotalInCorrect));
                                entries3.add(new PieEntry(TotalSkipped));


                                yVals3.add(new BarEntry(0,TotalCorrect));
                                yVals3.add(new BarEntry(1,TotalInCorrect));
                                yVals3.add(new BarEntry(2,TotalSkipped));
                                break;
                        }
                        dList.clear();
                        dList.addAll(success.getSuccess().getDifficulty());
                    }


                    setDificultyData();

                }
                else
                {

                }
                // showToast(list.size()+"");
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(getActivity(),ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(getActivity(),ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(getActivity(),Internet_Activity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mServiceReceiver.setReceiver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mServiceReceiver.setReceiver(null);
    }




   private void setDificultyData()
   {

       if(dList.size()>0)
       {

          for (int i=0;i<dList.size();i++)
          {
              TestHighLightsDifficulty d=dList.get(i);
              if(d.getPowerId() == 1)
              {
                  card_easy.setVisibility(View.VISIBLE);
                  type1.setVisibility(View.VISIBLE);
                  total1.setText(d.getTotalQuestions()+"");
                  totalC1.setText(d.getTotalCorrect()+" Correct");
                  totalIC1.setText(d.getTotalInCorrect()+" Incorrect");
                  totalSK1.setText(d.getTotalSkipped()+" Skipped");
                //  setData1(3,3,3);

                  pieChart1();
              }
              else
              if(d.getPowerId() == 2)
              {
                  card_difficulty2.setVisibility(View.VISIBLE);
                  type2.setVisibility(View.VISIBLE);
                  total2.setText(d.getTotalQuestions()+"");
                  totalC2.setText(d.getTotalCorrect()+" Correct");
                  totalIC2.setText(d.getTotalInCorrect()+" Incorrect");
                  totalSK2.setText(d.getTotalSkipped()+" Skipped");
                  //setData2(3,3,3);
                  pieChart2();
              }
              else
              if(d.getPowerId() == 3)
              {
                  card_difficulty3.setVisibility(View.VISIBLE);
                  type3.setVisibility(View.VISIBLE);
                  total3.setText(d.getTotalQuestions()+"");
                  totalC3.setText(d.getTotalCorrect()+" Correct");
                  totalIC3.setText(d.getTotalInCorrect()+" Incorrect");
                  totalSK3.setText(d.getTotalSkipped()+" Skipped");
                 // setData3(3,3,3);
                  pieChart3();
              }
          }
       }
   }

    private void setData1(int count, int xVal, int val) {
        XAxis xAxis = totalB1.getXAxis();
        xAxis.setAxisLineColor(Color.BLUE);
        xAxis.setEnabled(false);
        xAxis.setDrawLabels(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(yVals1.size());

        // xAxis.setValueFormatter(xAxisFormatter);

        //IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = totalB1.getAxisLeft();

        leftAxis.setValueFormatter(new MyYAxisValueFormatter());

        //leftAxis.setTypeface(mTfLight);
        leftAxis.setDrawLabels(false);
        leftAxis.setAxisLineColor(Color.BLUE);
        leftAxis.setEnabled(false);
        leftAxis.setLabelCount(val, false);
        //leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(count);
        YAxis rightAxis = totalB1.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        float start = 0f;
        totalB1.getXAxis().setAxisMinimum(start);
        totalB1.getXAxis().setAxisMaximum(xVal + 2);
        BarDataSet set1;

        if (totalB1.getData() != null &&
                totalB1.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) totalB1.getData().getDataSetByIndex(0);
            set1.setValues(yVals1);
            totalB1.getData().notifyDataChanged();
            totalB1.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "");
            set1.setColors(Constants.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            //data.setBarWidth(12f);
            data.setDrawValues(false); // add it


            totalB1.setDrawBarShadow(false);
          //  totalB1.setDrawValueAboveBar(false);
            totalB1.setFitBars(true); // add it


            Description des = totalB1.getDescription();
            des.setEnabled(false);
            totalB1.setData(data);

            Legend l = totalB1.getLegend();
            l.setEnabled(false);
        }
    }
    private void setData2(int count, int xVal, int val) {


        XAxis xAxis = totalB2.getXAxis();
        xAxis.setAxisLineColor(Color.BLUE);
        xAxis.setEnabled(false);
        xAxis.setDrawLabels(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(yVals2.size());

        // xAxis.setValueFormatter(xAxisFormatter);

        //IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = totalB2.getAxisLeft();

        leftAxis.setValueFormatter(new MyYAxisValueFormatter());

        //leftAxis.setTypeface(mTfLight);
        leftAxis.setDrawLabels(false);
        leftAxis.setAxisLineColor(Color.BLUE);
        leftAxis.setEnabled(false);
        leftAxis.setLabelCount(val, false);
        //leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(count);
        YAxis rightAxis = totalB2.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        float start = 0f;
        totalB2.getXAxis().setAxisMinimum(start);
        totalB2.getXAxis().setAxisMaximum(xVal + 2);
        BarDataSet set1;

        if (totalB2.getData() != null &&
                totalB2.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) totalB2.getData().getDataSetByIndex(0);
            set1.setValues(yVals2);
            totalB2.getData().notifyDataChanged();
            totalB2.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals2, "");
            set1.setColors(Constants.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            //data.setBarWidth(12f);
            data.setDrawValues(false); // add it


            totalB2.setDrawBarShadow(false);
            //  totalB1.setDrawValueAboveBar(false);
            totalB2.setFitBars(true); // add it


            Description des = totalB2.getDescription();
            des.setEnabled(false);
            totalB2.setData(data);

            Legend l = totalB2.getLegend();
            l.setEnabled(false);



            }
    }
    private void setData3(int count, int xVal, int val) {
        XAxis xAxis = totalB3.getXAxis();
        xAxis.setAxisLineColor(Color.BLUE);
        xAxis.setEnabled(false);
        xAxis.setDrawLabels(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        // xAxis.setTypeface(mTfLight);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setLabelCount(yVals3.size());

        // xAxis.setValueFormatter(xAxisFormatter);

        //IAxisValueFormatter custom = new MyAxisValueFormatter();

        YAxis leftAxis = totalB3.getAxisLeft();

        leftAxis.setValueFormatter(new MyYAxisValueFormatter());

        //leftAxis.setTypeface(mTfLight);
        leftAxis.setDrawLabels(false);
        leftAxis.setAxisLineColor(Color.BLUE);
        leftAxis.setEnabled(false);
        leftAxis.setLabelCount(val, false);
        //leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(count);
        YAxis rightAxis = totalB3.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        float start = 0f;
        totalB3.getXAxis().setAxisMinimum(start);
        totalB3.getXAxis().setAxisMaximum(xVal + 2);
        BarDataSet set1;

        if (totalB3.getData() != null &&
                totalB3.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) totalB3.getData().getDataSetByIndex(0);
            set1.setValues(yVals3);
            totalB3.getData().notifyDataChanged();
            totalB3.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals3, "");
            set1.setColors(Constants.MATERIAL_COLORS);

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(10f);
            // data.setValueTypeface(mTfLight);
            //data.setBarWidth(12f);
            data.setDrawValues(false); // add it


            totalB3.setDrawBarShadow(false);
            //  totalB1.setDrawValueAboveBar(false);
            totalB3.setFitBars(true); // add it


            Description des = totalB3.getDescription();
            des.setEnabled(false);
            totalB3.setData(data);

            Legend l = totalB3.getLegend();
            l.setEnabled(false);
        }
    }
    private PieChart pieChart;
    private PieChart pieChart2;
    private PieChart pieChart3;
    private void pieChart1()
    {

        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);
        pieChart.setExtraOffsets(5, 10, 5, 5);

        pieChart.setDragDecelerationFrictionCoef(0.95f);

        // pieChart.setCenterTextTypeface(mTfLight);
        // pieChart.setCenterText(generateCenterSpannableText());

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);


        pieChart.setHoleRadius(28f);
        pieChart.setTransparentCircleRadius(150f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        setData1();


        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        pieChart.getLegend().setEnabled(false);

        // entry label styling
        pieChart.setEntryLabelColor(Color.WHITE);
        //pieChart.setEntryLabelTypeface(mTfRegular);
        pieChart.setEntryLabelTextSize(12f);
    }
    ArrayList<PieEntry> entries1 = new ArrayList<PieEntry>();

    private void pieChart2()
    {

        pieChart2.setUsePercentValues(true);
        pieChart2.getDescription().setEnabled(false);
        pieChart2.setExtraOffsets(5, 10, 5, 5);

        pieChart2.setDragDecelerationFrictionCoef(0.95f);

        // pieChart.setCenterTextTypeface(mTfLight);
        // pieChart.setCenterText(generateCenterSpannableText());

        pieChart2.setDrawHoleEnabled(true);
        pieChart2.setHoleColor(Color.WHITE);

        pieChart2.setTransparentCircleColor(Color.WHITE);
        pieChart2.setTransparentCircleAlpha(110);


        pieChart2.setHoleRadius(28f);
        pieChart2.setTransparentCircleRadius(150f);

        pieChart2.setDrawCenterText(true);

        pieChart2.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart2.setRotationEnabled(true);
        pieChart2.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart2.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        setData2();

        // pieChart.animateY(1400, Easing.EaseInOutQuad);
        // pieChart.spin(2000, 0, 360);

//        mSeekBarX.setOnSeekBarChangeListener(this);
        //      mSeekBarY.setOnSeekBarChangeListener(this);

        Legend l = pieChart2.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        pieChart2.getLegend().setEnabled(false);

        // entry label styling
        pieChart2.setEntryLabelColor(Color.WHITE);
        //pieChart.setEntryLabelTypeface(mTfRegular);
        pieChart2.setEntryLabelTextSize(12f);
    }
    ArrayList<PieEntry> entries2 = new ArrayList<PieEntry>();
    private void pieChart3()
    {

        pieChart3.setUsePercentValues(true);
        pieChart3.getDescription().setEnabled(false);
        pieChart3.setExtraOffsets(5, 10, 5, 5);

        pieChart3.setDragDecelerationFrictionCoef(0.95f);

        // pieChart.setCenterTextTypeface(mTfLight);
        // pieChart.setCenterText(generateCenterSpannableText());

        pieChart3.setDrawHoleEnabled(true);
        pieChart3.setHoleColor(Color.WHITE);

        pieChart3.setTransparentCircleColor(Color.WHITE);
        pieChart3.setTransparentCircleAlpha(110);


        pieChart3.setHoleRadius(28f);
        pieChart3.setTransparentCircleRadius(150f);

        pieChart3.setDrawCenterText(true);

        pieChart3.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart3.setRotationEnabled(true);
        pieChart3.setHighlightPerTapEnabled(true);

        // pieChart.setUnit(" €");
        // pieChart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart3.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        setData3();

        // pieChart.animateY(1400, Easing.EaseInOutQuad);
        // pieChart.spin(2000, 0, 360);

//        mSeekBarX.setOnSeekBarChangeListener(this);
        //      mSeekBarY.setOnSeekBarChangeListener(this);

        Legend l = pieChart3.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        pieChart3.getLegend().setEnabled(false);

        // entry label styling
        pieChart3.setEntryLabelColor(Color.WHITE);
        //pieChart.setEntryLabelTypeface(mTfRegular);
        pieChart3.setEntryLabelTextSize(12f);
    }
    ArrayList<PieEntry> entries3 = new ArrayList<PieEntry>();

    private void setData1() {

        //   float mult = range;




        // NOTE: The order of the entries when being added to
        // the entries array determines their position around the center of
        // the chart.
//        for (int i = 0; i < count ; i++) {
//            entries1.add(new PieEntry((float) ((Math.random() * mult) + mult / 5)));
//        }

        PieDataSet dataSet = new PieDataSet(entries1, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : Constants.MATERIAL_COLORS)
            colors.add(c);



        // colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueTextSize(9f);
        data.setValueTextColor(Color.TRANSPARENT);

        // data.setValueTypeface(mTfLight);
        pieChart.setData(data);

        // undo all highlights
        pieChart.highlightValues(null);

        pieChart.invalidate();
    }

    private void setData2() {

        //   float mult = range;




        // NOTE: The order of the entries when being added to
        // the entries array determines their position around the center of
        // the chart.
//        for (int i = 0; i < count ; i++) {
//            entries1.add(new PieEntry((float) ((Math.random() * mult) + mult / 5)));
//        }

        PieDataSet dataSet = new PieDataSet(entries2, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : Constants.MATERIAL_COLORS)
            colors.add(c);



        // colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueTextSize(9f);
        data.setValueTextColor(Color.TRANSPARENT);

        // data.setValueTypeface(mTfLight);
        pieChart2.setData(data);

        // undo all highlights
        pieChart2.highlightValues(null);

        pieChart2.invalidate();
    }
    private void setData3() {

        //   float mult = range;




        // NOTE: The order of the entries when being added to
        // the entries array determines their position around the center of
        // the chart.
//        for (int i = 0; i < count ; i++) {
//            entries1.add(new PieEntry((float) ((Math.random() * mult) + mult / 5)));
//        }

        PieDataSet dataSet = new PieDataSet(entries3, "");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : Constants.MATERIAL_COLORS)
            colors.add(c);



        // colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueTextSize(9f);
        data.setValueTextColor(Color.TRANSPARENT);

        // data.setValueTypeface(mTfLight);
        pieChart3.setData(data);

        // undo all highlights
        pieChart3.highlightValues(null);

        pieChart3.invalidate();
    }




}

