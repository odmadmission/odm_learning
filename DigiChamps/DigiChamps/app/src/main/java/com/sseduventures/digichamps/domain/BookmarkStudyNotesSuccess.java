package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 9/5/2018.
 */

public class BookmarkStudyNotesSuccess implements Parcelable{

    private List<BookmarkStudyNotesList> StudynoteList;

    protected BookmarkStudyNotesSuccess(Parcel in) {
        StudynoteList = in.createTypedArrayList(BookmarkStudyNotesList.CREATOR);
    }

    public static final Creator<BookmarkStudyNotesSuccess> CREATOR = new Creator<BookmarkStudyNotesSuccess>() {
        @Override
        public BookmarkStudyNotesSuccess createFromParcel(Parcel in) {
            return new BookmarkStudyNotesSuccess(in);
        }

        @Override
        public BookmarkStudyNotesSuccess[] newArray(int size) {
            return new BookmarkStudyNotesSuccess[size];
        }
    };

    public List<BookmarkStudyNotesList> getStudynoteList() {
        return StudynoteList;
    }

    public void setStudynoteList(List<BookmarkStudyNotesList> studynoteList) {
        StudynoteList = studynoteList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(StudynoteList);
    }
}
