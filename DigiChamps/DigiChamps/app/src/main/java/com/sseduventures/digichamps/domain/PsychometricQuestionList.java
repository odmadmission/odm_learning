package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by NISHIKANT on 6/7/2018.
 */

public class PsychometricQuestionList implements Parcelable
{

    private ArrayList<PsychometricQuestion> success;

    public ArrayList<PsychometricQuestion> getSuccess() {
        return success;
    }

    public void setSuccess(ArrayList<PsychometricQuestion> success) {
        this.success = success;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.success);
    }

    public PsychometricQuestionList() {
    }

    protected PsychometricQuestionList(Parcel in) {
        this.success = in.createTypedArrayList(PsychometricQuestion.CREATOR);
    }

    public static final Creator<PsychometricQuestionList> CREATOR = new Creator<PsychometricQuestionList>() {
        @Override
        public PsychometricQuestionList createFromParcel(Parcel source) {
            return new PsychometricQuestionList(source);
        }

        @Override
        public PsychometricQuestionList[] newArray(int size) {
            return new PsychometricQuestionList[size];
        }
    };
}
