package com.sseduventures.digichamps.helper;


import android.content.Context;
import android.content.SharedPreferences;

import com.sseduventures.digichamps.database.DbContract;

/**
 * Created by Raju Satheesh on 10/18/2016.
 */
public class RegPrefManager {

    private SharedPreferences mSharedPreferences;
    private static RegPrefManager mPrefManager;

    public static final int MEMBER_STATUS_FAMILY = 2;
    public static final int MEMBER_STATUS_REGISTER = 1;
    public static final int MEMBER_STATUS_DELETE = 3;
    public static final int MEMBER_STATUS_MIGRATED = 4;
    public static final int MEMBER_STATUS_NODATA = 0;
    public static String KEY_CLASS_ID = "classid";


    public static String KEY_BOARD_ID = "boardid";

    public Context mContext;


    private RegPrefManager(Context context) {
        this.mContext=context;
        mSharedPreferences = context.
                getSharedPreferences("digichamps_pref", Context.MODE_PRIVATE);
    }

    public static RegPrefManager getInstance(Context context) {
        if (mPrefManager == null) {
            mPrefManager = new RegPrefManager(context);
        }
        return mPrefManager;
    }

    public String getFcmRegId() {
        return mSharedPreferences.getString("fcm_id", null);
    }

    public void setFcmLink(String value) {
        mSharedPreferences.edit().putString("fcm_link", value).apply();

    }

    public String getMentorVideoLink() {
        return mSharedPreferences.getString("MentorVideoLink", null);
    }

    public void setMentorVideoLink(String value) {
        mSharedPreferences.edit().putString("MentorVideoLink", value).apply();

    }

    public long getLoginId() {
        return mSharedPreferences.getLong("LoginId", 0);
    }

    public void setLoginId(long value) {
        mSharedPreferences.edit().putLong("LoginId", value).apply();

    }

    public String getschoolCode() {
        return mSharedPreferences.getString("schoolCode", null);
    }

    public void setschoolCode(String value) {
        mSharedPreferences.edit().putString("schoolCode", value).apply();

    }

    public int getDoubtCount() {
        return mSharedPreferences.getInt("DoubtCount", 0);
    }

    public void setDoubtCount(int value) {
        mSharedPreferences.edit().putInt("DoubtCount", value).apply();

    }

    public String getschoolId() {
        return mSharedPreferences.getString("schoolId", null);
    }

    public void setschoolId(String value) {
        mSharedPreferences.edit().putString("schoolId", value).apply();

    }

    public int getMentorid() {
        return mSharedPreferences.getInt("Mentorid", 0);
    }

    public void setReedeem(int value) {
        mSharedPreferences.edit().putInt("Reedeem", value).apply();

    }

    public int getReedeem() {
        return mSharedPreferences.getInt("Reedeem", 0);
    }

    public void setMentorid(int value) {
        mSharedPreferences.edit().putInt("Mentorid", value).apply();

    }

    public long getclassid() {
        return mSharedPreferences.getLong("classid", 0);
    }

    public void setclassid(long value) {
        mSharedPreferences.edit().putLong("classid", value).apply();

    }

    public int getroleid() {
        return mSharedPreferences.getInt("roleid", 0);
    }

    public void setroleid(int value) {
        mSharedPreferences.edit().putInt("roleid", value).apply();

    }

    public String getFcmLink() {
        return mSharedPreferences.getString("fcm_link", null);
    }

    public void setTypeId(int value) {
        mSharedPreferences.edit().putInt("fcm_type_id", value).apply();

    }

    public int getTypeId() {
        return mSharedPreferences.getInt("fcm_type_id", 0);
    }

    public long getKeyClassId() {
        return mSharedPreferences.getLong("classid", 0);
    }

    public void setKeyClassId(long value) {
        mSharedPreferences.edit().putLong("classid", value).apply();
    }

    public String getKeyBoardId() {
        return mSharedPreferences.getString("boardid", null);
    }

    public void setKeyBoardId(String value) {
        mSharedPreferences.edit().putString("boardid", value).apply();
    }

    public void setFcmRegId(String value) {
        mSharedPreferences.edit().putString("fcm_id", value).apply();

    }

    public long getMobile() {
        return mSharedPreferences.getLong("mobile", 0);
    }

    public void setInvitorFamilyId(String value) {
        mSharedPreferences.edit().putString("invitor_family_id", value).apply();

    }

    public String getInvitorFamilyId() {
        return mSharedPreferences.getString("invitor_family_id", null);
    }

    public void setMobile(long value) {
        mSharedPreferences.edit().putLong("mobile", value).apply();

    }

    public String getMemberId() {
        return mSharedPreferences.getString("member_id", null);
    }

    public void setLocationInterval(int value) {
        mSharedPreferences.edit().putInt("sync_interval", value).apply();

    }


    public int getLocationInterval() {
        return mSharedPreferences.getInt("sync_interval", 2);
    }

    public void setInvitationCount(int value) {
        mSharedPreferences.edit().putInt("InvitationCount", value).apply();

    }

    public int getInvitationCount() {
        return mSharedPreferences.getInt("InvitationCount", 0);
    }


    public void setFamilyChatUnreadCount(int value) {
        mSharedPreferences.edit().putInt("FamilyChatCount", value).apply();

    }

    public int getFamilyChatUnreadCount() {
        return mSharedPreferences.getInt("FamilyChatCount", 0);
    }

    public void setFamilyChatLastMessage(String value) {
        mSharedPreferences.edit().putString("FamilyChatLast", value).apply();

    }

    public String getFamilyChatLastMessage() {
        return mSharedPreferences.getString("FamilyChatLast", "created family");
    }

    public void setFamilyChatLastMessageName(String value) {
        mSharedPreferences.edit().putString("FamilyChatName", value).apply();

    }

    public String getFamilyChatLastMessageName() {
        return mSharedPreferences.getString("FamilyChatName", "You");
    }

    public void setFamilyChatLastMessageTime(long value) {
        mSharedPreferences.edit().putLong("FamilyChatTime", value).apply();

    }

    public long getFamilyChatLastMessageTime() {
        return mSharedPreferences.getLong("FamilyChatTime", System.currentTimeMillis());
    }


    public int getNotificationMute() {
        return mSharedPreferences.getInt("NotificationMute", 0);
    }


    public void setNotificationMute(int value) {
        mSharedPreferences.edit().putInt("NotificationMute", value).apply();

    }


    public void setMemberId(String value) {
        mSharedPreferences.edit().putString("member_id", value).apply();

    }

    public String getCountryId() {
        return mSharedPreferences.getString("country", null);
    }

    public void setCountryId(String value) {
        mSharedPreferences.edit().putString("country", value).apply();

    }

    public String getFamilyInviteLink() {
        return mSharedPreferences.getString("FamilyInviteLink", null);
    }

    public void setName(String value) {
        mSharedPreferences.edit().putString("Name", value).apply();

    }

    public String getName() {
        return mSharedPreferences.getString("Name", null);
    }

    public void setFamilyName(String value) {
        mSharedPreferences.edit().putString("FamilyName", value).apply();

    }

    public String getFamilyName() {
        return mSharedPreferences.getString("FamilyName", null);
    }

    public void setFamilyInviteLink(String value) {
        mSharedPreferences.edit().putString("FamilyInviteLink", value).apply();

    }


    public String getFamilyId() {
        return mSharedPreferences.getString("FamilyId", null);
    }

    public void setFamilyId(String value) {
        mSharedPreferences.edit().putString("FamilyId", value).apply();

    }

    public int getVisibility() {
        return mSharedPreferences.getInt("Visibility", 1);
    }

    public void setVisibility(int value) {
        mSharedPreferences.edit().putInt("Visibility", value).apply();

    }

    public int getAdmin() {
        return mSharedPreferences.getInt("Admin", 0);
    }

    public void setAdmin(int value) {
        mSharedPreferences.edit().putInt("Admin", value).apply();

    }


    public int getSmsTestRecievedCode() {
        return mSharedPreferences.
                getInt("sms_test_received_code", 0);
    }

    public void setSmsTestRecievedCode(int value) {

        mSharedPreferences.edit().putInt("sms_test_received_code", value).apply();

    }

    public int getSmsTestSavedCode() {
        return mSharedPreferences.
                getInt("sms_test_saved_code", 0);
    }

    public void setSmsTestSavedCode(int value) {
        mSharedPreferences.
                edit().putInt("sms_test_saved_code", value).apply();

    }

    public void removeSmsTestSavedCode() {
        mSharedPreferences.
                edit().remove("sms_test_saved_code").apply();

    }

    public void removeSmsTestRecievedCode() {
        mSharedPreferences.
                edit().remove("sms_test_received_code").apply();

    }

    public void setStatus(int value) {
        mSharedPreferences.
                edit().putInt("status", value).apply();
    }

    public int getStatus() {
        return mSharedPreferences.
                getInt("status", 0);
    }


   /* public void setUserName(String name) {
        mSharedPreferences.
                edit().putString("userName", name).apply();
    }

    public String getUserName() {
        return mSharedPreferences.
                getString("userName", null);

    }*/

   public void setUserName(String name){
       mSharedPreferences.edit().putString("userName",name).apply();
   }
   public String getUserName(){
       return mSharedPreferences.getString("userName",null);
   }


    public void setImageString(String image) {
        mSharedPreferences.
                edit().putString("image", image).apply();
    }

    public String getImageString() {
        return mSharedPreferences.
                getString("image", null);
    }


    public void setRegId(long regId) {
        mSharedPreferences.
                edit().putLong("regId", regId).apply();
    }


    public long getRegId() {
        return mSharedPreferences.
                getLong("regId", 0);
    }


    public void setPhone(String ph) {
        mSharedPreferences.
                edit().putString("ph", ph).apply();
    }


    public String getPhone() {
        return mSharedPreferences.
                getString("ph", null);
    }


    public void setEmail(String mail) {
        mSharedPreferences.
                edit().putString("mail", mail).apply();
    }


    public String getEmail() {
        return mSharedPreferences.
                getString("mail", null);
    }

    public void setSchoolName(String schoolName) {
        mSharedPreferences.
                edit().putString("schoolName", schoolName).apply();
    }


    public String getSchoolName() {
        return mSharedPreferences.
                getString("schoolName", null);
    }

    public void setDOB(String dob) {
        mSharedPreferences.
                edit().putString("dob", dob).apply();
    }


    public String getDOB() {
        return mSharedPreferences.
                getString("dob", null);
    }


    public void setBoardName(String boardName) {
        mSharedPreferences.
                edit().putString("boardName", boardName).apply();
    }


    public String getBoardName() {
        return mSharedPreferences.
                getString("boardName", null);
    }


    public void setClassName(String className) {
        mSharedPreferences.
                edit().putString("className", className).apply();
    }


    public String getClassName() {
        return mSharedPreferences.
                getString("className", null);
    }



    public void setIsDoubt(boolean isDoubt) {
        mSharedPreferences.
                edit().putBoolean("isDoubt", isDoubt).apply();
    }


    public boolean getIsDoubt() {
        return mSharedPreferences.
                getBoolean("isDoubt", false);
    }


    public void setIsMentor(boolean isMentor) {
        mSharedPreferences.
                edit().putBoolean("isMentor", isMentor).apply();
    }


    public boolean getIsMentor() {
        return mSharedPreferences.
                getBoolean("isMentor", false);
    }



    public void setResultCount(int ResultCount) {
        mSharedPreferences.
                edit().putInt("ResultCount", ResultCount).apply();
    }

    public int getResultCount() {
        return mSharedPreferences.
                getInt("ResultCount", 0);
    }



    public void setCoinCount(int coinCount) {
        mSharedPreferences.
                edit().putInt("coinCount", coinCount).apply();
    }

    public int getCoinCount() {
        return mSharedPreferences.
                getInt("coinCount", 0);
    }

    public void setIsSectionEnabled(boolean IsSectionEnabled) {
        mSharedPreferences.
                edit().putBoolean("IsSectionEnabled", IsSectionEnabled).apply();
    }


    public boolean getIsSectionEnabled() {
        return mSharedPreferences.
                getBoolean("IsSectionEnabled", false);
    }


    public void setIsSchool(boolean IsSchool) {
        mSharedPreferences.
                edit().putBoolean("IsSchool", IsSchool).apply();
    }


    public boolean getIsSchool() {
        return mSharedPreferences.
                getBoolean("IsSchool", false);
    }


    public void setSchoolId(String schoolId) {
        mSharedPreferences.
                edit().putString("schoolId", schoolId).apply();
    }


    public String getSchoolId() {
        return mSharedPreferences.
                getString("schoolId", null);
    }

    public void setSectionId(String sectionId) {
        mSharedPreferences.
                edit().putString("sectionId", sectionId).apply();
    }


    public String getSectionId() {
        return mSharedPreferences.
                getString("sectionId", null);
    }

    public void setSectionClass(String sectionName) {
        mSharedPreferences.
                edit().putString("sectionClass", sectionName).apply();
    }


    public String getsectionName() {
        return mSharedPreferences.
                getString("sectionName", null);
    }


    public void setsectionName(String sectionName) {
        mSharedPreferences.
                edit().putString("sectionName", sectionName).apply();
    }


    public String getSectionClass() {
        return mSharedPreferences.
                getString("sectionClass", null);
    }




    public void setExpireDate(String expireDate) {
        mSharedPreferences.
                edit().putString("expireDate", expireDate).apply();
    }


    public String getExpireDate() {
        return mSharedPreferences.
                getString("expireDate", null);
    }

    public void setDeviceToken(String device) {
        mSharedPreferences.
                edit().putString("deviceToken", device).apply();
    }


    public String getDeviceToken() {
        return mSharedPreferences.
                getString("deviceToken", null);
    }
    public void setFlagLearnToday(String flag) {
        mSharedPreferences.
                edit().putString("flag", flag).apply();
    }


    public String getFlagLearnToday() {
        return mSharedPreferences.
                getString("flag", null);
    }

    public void logout() {

        SharedPreferences preferences=mContext.
                getSharedPreferences("user_data",Context.MODE_PRIVATE);
        if(preferences!=null)
        {
            preferences.edit().clear().apply();
        }
        mSharedPreferences.edit().clear().apply();
        mContext.getContentResolver().delete(DbContract.CoinTable.CONTENT_URI,null,
                null);
        mContext.getContentResolver().delete(DbContract.NotificationTable.CONTENT_URI,
                null,null);

        mContext.getContentResolver().delete(DbContract.ExamTable.CONTENT_URI,
                null,null);

        mContext.getContentResolver().delete(DbContract.UserTable.CONTENT_URI,
                null,null);

        mContext.getContentResolver().delete(DbContract.VideoLogTable.CONTENT_URI,
                null,null);

        mContext.getContentResolver().delete(DbContract.ProgressTable.CONTENT_URI,
                null,null);

        mContext.getContentResolver().delete(DbContract.ActivityTimeTable.CONTENT_URI,
                null,null);

    }




    public int getFeedDataOffline() {
        return mSharedPreferences.getInt("FeedDataOffline", 0);
    }

    public void setFeedDataOffline(int value) {
        mSharedPreferences.edit().putInt("FeedDataOffline", value).apply();

    }

}

