package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 6/26/2018.
 */

public class BookMarkRequest implements Parcelable{

    private long RegID;
    private String DatatypeId;
    private int DataId;


    public long getRegID() {
        return RegID;
    }

    public void setRegID(long regID) {
        RegID = regID;
    }

    public String getDatatypeId() {
        return DatatypeId;
    }

    public void setDatatypeId(String datatypeId) {
        DatatypeId = datatypeId;
    }

    public int getDataId() {
        return DataId;
    }

    public void setDataId(int dataId) {
        DataId = dataId;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.RegID);
        dest.writeString(this.DatatypeId);
        dest.writeInt(this.DataId);
    }

    public BookMarkRequest() {
    }

    protected BookMarkRequest(Parcel in) {
        this.RegID = in.readLong();
        this.DatatypeId = in.readString();
        this.DataId = in.readInt();
    }

    public static final Creator<BookMarkRequest> CREATOR = new Creator<BookMarkRequest>() {
        @Override
        public BookMarkRequest createFromParcel(Parcel source) {
            return new BookMarkRequest(source);
        }

        @Override
        public BookMarkRequest[] newArray(int size) {
            return new BookMarkRequest[size];
        }
    };
}
