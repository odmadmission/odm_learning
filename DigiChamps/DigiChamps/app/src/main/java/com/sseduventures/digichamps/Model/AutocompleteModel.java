package com.sseduventures.digichamps.Model;

import java.io.Serializable;

public class AutocompleteModel implements Serializable {
    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    String SchoolName;
    String schoolId;
}
