package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/13/2018.
 */

public class PostDiscussionDetailsResponse implements Parcelable{


    private String Message;
    private int DiscussionDetailID;
    private int ResultCount;
    private PostDiscussionMessageDetails messageDetail;

    protected PostDiscussionDetailsResponse(Parcel in) {
        Message = in.readString();
        DiscussionDetailID = in.readInt();
        ResultCount = in.readInt();
        messageDetail = in.readParcelable(PostDiscussionMessageDetails.class.getClassLoader());
    }

    public static final Creator<PostDiscussionDetailsResponse> CREATOR = new Creator<PostDiscussionDetailsResponse>() {
        @Override
        public PostDiscussionDetailsResponse createFromParcel(Parcel in) {
            return new PostDiscussionDetailsResponse(in);
        }

        @Override
        public PostDiscussionDetailsResponse[] newArray(int size) {
            return new PostDiscussionDetailsResponse[size];
        }
    };

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getDiscussionDetailID() {
        return DiscussionDetailID;
    }

    public void setDiscussionDetailID(int discussionDetailID) {
        DiscussionDetailID = discussionDetailID;
    }

    public int getResultCount() {
        return ResultCount;
    }

    public void setResultCount(int resultCount) {
        ResultCount = resultCount;
    }

    public PostDiscussionMessageDetails getMessageDetail() {
        return messageDetail;
    }

    public void setMessageDetail(PostDiscussionMessageDetails messageDetail) {
        this.messageDetail = messageDetail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Message);
        parcel.writeInt(DiscussionDetailID);
        parcel.writeInt(ResultCount);
        parcel.writeParcelable(messageDetail, i);
    }
}
