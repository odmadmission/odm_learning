package com.sseduventures.digichamps.Model;

public class ChapterBasedlistModel {
    private String subjectId, TotalExamsAppeared, AverageMarks, Accuracy,AverageTimePerQuestion;


    public ChapterBasedlistModel(String subjectId, String TotalExamsAppeared,
                                   String AverageMarks, String Accuracy,
                                   String AverageTimePerQuestion) {
        this.subjectId = subjectId;
        this.TotalExamsAppeared = TotalExamsAppeared;
        this.AverageMarks = AverageMarks;
        this.Accuracy = Accuracy;
        this.AverageTimePerQuestion = AverageTimePerQuestion;

    }

    public String getSubjectId() {
        return subjectId;
    }

    public String getTotalExamsAppeared() {
        return TotalExamsAppeared;
    }

    public String getAverageMarks() {
        return AverageMarks;
    }

    public String getAccuracy() {
        return Accuracy;
    }

    public String getAverageTimePerQuestion() {
        return AverageTimePerQuestion;
    }
}







