package com.sseduventures.digichamps.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.FormActivity;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.adapter.CardviewDiscussionDetail;
import com.sseduventures.digichamps.adapter.CardviewExamDetail;
import com.sseduventures.digichamps.domain.ExamScheduleDetailsResponse;
import com.sseduventures.digichamps.domain.ExamScheduleDetailsSuccess;
import com.sseduventures.digichamps.helper.RegPrefManager;
import com.sseduventures.digichamps.helper.ServiceReceiver;
import com.sseduventures.digichamps.services.NetworkService;
import com.sseduventures.digichamps.utils.AppUtil;
import com.sseduventures.digichamps.utils.Constants;
import com.sseduventures.digichamps.utils.FontManage;
import com.sseduventures.digichamps.utils.IntentHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sseduventures.digichamps.webservice.ResponseCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.sseduventures.digichamps.utils.Constants.EXAM_SCHEDULE_BY_ID;


public class ExamDetailActivity extends FormActivity implements
        ServiceReceiver.Receiver{

    Context mContext;
    CardviewExamDetail mCardviewExamDetail;
    RecyclerView recyclerview;
    TextView tv_exam;
    String examID = "";
    ArrayList<ExamScheduleDetailsSuccess> mList;


    String schoolid;
    int classid;

    private Handler mHandler;
    private ServiceReceiver mServiceReceiver;
    private ExamScheduleDetailsResponse success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_detail);

        mHandler = new Handler();
        mServiceReceiver = new ServiceReceiver(mHandler);

        schoolid = RegPrefManager.getInstance(this).getSchoolId();
        classid = (int) RegPrefManager.getInstance(this).getclassid();

        mContext = this;
        tv_exam =  findViewById(R.id.tv_exam);
        ImageView iv_menu = (ImageView) findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mList = new ArrayList<ExamScheduleDetailsSuccess>();
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(mLayoutManager);
        recyclerview.setItemAnimator(new DefaultItemAnimator());

        TextView tv_at =  findViewById(R.id.tv_exam);
        TextView tv_date =  findViewById(R.id.tv_date);
        TextView tv_subject =  findViewById(R.id.tv_subject);
        TextView tv_time =  findViewById(R.id.tv_time);
        FontManage.setFontHeaderBold(mContext,tv_at);
        FontManage.setFontImpact(mContext,tv_date);
        FontManage.setFontImpact(mContext,tv_subject);
        FontManage.setFontImpact(mContext,tv_time);

        if(getIntent() != null){
            tv_exam.setText(getIntent().getStringExtra("examName"));
            examID = getIntent().getStringExtra("examID");
            getExamScheduleDetailsNew();
        }
    }






    private AlertDialog AskOptionDialog(String msg) {
        AlertDialog myQuittingDialogBox = new AlertDialog.Builder(mContext)
                .setTitle(R.string.digichamps)
                .setMessage(msg)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }


    private void getExamScheduleDetailsNew() {
        if(AppUtil.isInternetConnected(this)){

            Bundle bun = new Bundle();
            bun.putString("ExamId",examID);
            NetworkService.startActionPostExamScheduleDetails(ExamDetailActivity.this,mServiceReceiver,bun);

        }else{
            Intent in = new Intent(ExamDetailActivity.this,Internet_Activity.class);
            startActivity(in);
        }
    }


    @Override
    protected void onStart() {

        mServiceReceiver.setReceiver(this);
        super.onStart();
    }

    @Override
    protected void onStop() {

        mServiceReceiver.setReceiver(null);
        super.onStop();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        //hideDialog();
        switch (resultCode) {
            case ResponseCodes.EXCEPTION:
                Intent in = new Intent(ExamDetailActivity.this, ErrorActivity.class);
                startActivity(in);
                break;
            case ResponseCodes.NO_INTERNET:
                Intent intent = new Intent(ExamDetailActivity.this, Internet_Activity.class);
                startActivity(intent);
                break;
            case ResponseCodes.FAILURE:
                Intent inin = new Intent(ExamDetailActivity.this, ErrorActivity.class);
                startActivity(inin);
                break;
            case ResponseCodes.SUCCESS:

                success = resultData.getParcelable(IntentHelper.RESULT_DATA);

                if(success.getResultCount() == 0){
                    AskOptionDialog("No Exam Details Found").show();
                }else{

                    if (success.getExamScheduleList()!=null &&
                            success.getExamScheduleList().size()>0){

                        mList.clear();
                        mList.addAll(success.getExamScheduleList());
                        mCardviewExamDetail = new CardviewExamDetail(mList,mContext);
                        recyclerview.setAdapter(mCardviewExamDetail);

                    }
                }


                break;

        }
    }
}