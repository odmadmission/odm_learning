package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 9/1/2018.
 */

public class BookMarkVideoList implements Parcelable{


    private String Module_Id;
    private String Module_Title;
    private String Module_Image;
    private String Description;
    private String Image_Key;
    private String VideoKey;
    private String Is_Expire;
    private String Is_Free;
    private String Is_Avail;



    private boolean flag;


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String getModule_Id() {
        return Module_Id;
    }

    public void setModule_Id(String module_Id) {
        Module_Id = module_Id;
    }

    public String getModule_Title() {
        return Module_Title;
    }

    public void setModule_Title(String module_Title) {
        Module_Title = module_Title;
    }

    public String getModule_Image() {
        return Module_Image;
    }

    public void setModule_Image(String module_Image) {
        Module_Image = module_Image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getImage_Key() {
        return Image_Key;
    }

    public void setImage_Key(String image_Key) {
        Image_Key = image_Key;
    }

    public String getVideoKey() {
        return VideoKey;
    }

    public void setVideoKey(String videoKey) {
        VideoKey = videoKey;
    }

    public String getIs_Expire() {
        return Is_Expire;
    }

    public void setIs_Expire(String is_Expire) {
        Is_Expire = is_Expire;
    }

    public String getIs_Free() {
        return Is_Free;
    }

    public void setIs_Free(String is_Free) {
        Is_Free = is_Free;
    }

    public String getIs_Avail() {
        return Is_Avail;
    }

    public void setIs_Avail(String is_Avail) {
        Is_Avail = is_Avail;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Module_Id);
        dest.writeString(this.Module_Title);
        dest.writeString(this.Module_Image);
        dest.writeString(this.Description);
        dest.writeString(this.Image_Key);
        dest.writeString(this.VideoKey);
        dest.writeString(this.Is_Expire);
        dest.writeString(this.Is_Free);
        dest.writeString(this.Is_Avail);
    }

    public BookMarkVideoList() {
    }

    protected BookMarkVideoList(Parcel in) {
        this.Module_Id = in.readString();
        this.Module_Title = in.readString();
        this.Module_Image = in.readString();
        this.Description = in.readString();
        this.Image_Key = in.readString();
        this.VideoKey = in.readString();
        this.Is_Expire = in.readString();
        this.Is_Free = in.readString();
        this.Is_Avail = in.readString();
    }

    public static final Creator<BookMarkVideoList> CREATOR = new Creator<BookMarkVideoList>() {
        @Override
        public BookMarkVideoList createFromParcel(Parcel source) {
            return new BookMarkVideoList(source);
        }

        @Override
        public BookMarkVideoList[] newArray(int size) {
            return new BookMarkVideoList[size];
        }
    };
}
