package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class FAQSuccess implements Parcelable{


    private List<FAQListData> list;

    public List<FAQListData> getList() {
        return list;
    }

    public void setList(List<FAQListData> list) {
        this.list = list;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.list);
    }

    public FAQSuccess() {
    }

    protected FAQSuccess(Parcel in) {
        this.list = in.createTypedArrayList(FAQListData.CREATOR);
    }

    public static final Creator<FAQSuccess> CREATOR = new Creator<FAQSuccess>() {
        @Override
        public FAQSuccess createFromParcel(Parcel source) {
            return new FAQSuccess(source);
        }

        @Override
        public FAQSuccess[] newArray(int size) {
            return new FAQSuccess[size];
        }
    };
}
