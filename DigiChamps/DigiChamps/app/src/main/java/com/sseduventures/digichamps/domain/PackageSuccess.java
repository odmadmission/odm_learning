package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by NISHIKANT on 6/5/2018.
 */

public class PackageSuccess implements Parcelable
{
    private List<PackageModel> Package;

    public List<PackageModel> getPackage() {
        return Package;
    }

    public void setPackage(List<PackageModel> aPackage) {
        Package = aPackage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.Package);
    }

    public PackageSuccess() {
    }

    protected PackageSuccess(Parcel in) {
        this.Package = in.createTypedArrayList(PackageModel.CREATOR);
    }

    public static final Creator<PackageSuccess> CREATOR = new Creator<PackageSuccess>() {
        @Override
        public PackageSuccess createFromParcel(Parcel source) {
            return new PackageSuccess(source);
        }

        @Override
        public PackageSuccess[] newArray(int size) {
            return new PackageSuccess[size];
        }
    };
}
