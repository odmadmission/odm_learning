package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/12/2018.
 */

public class AssignedTeacherDetails implements Parcelable {

    private String AssignmentId;
    private String SchoolId;
    private String ClassId;
    private int Class_Id;
    private String SectionId;;
    private String SubjectId;
    private String TeacherId;
    private String Section;
    private String Teacher;
    private String ClassName;
    private String SubjectName;
    private String TeacherName;
    private String SectionName;
    private String EmailAddress;
    private String ImageUrl;
    private String IsActive;

    public String getAssignmentId() {
        return AssignmentId;
    }

    public void setAssignmentId(String assignmentId) {
        AssignmentId = assignmentId;
    }

    public String getSchoolId() {
        return SchoolId;
    }

    public void setSchoolId(String schoolId) {
        SchoolId = schoolId;
    }

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public int getClass_Id() {
        return Class_Id;
    }

    public void setClass_Id(int class_Id) {
        Class_Id = class_Id;
    }

    public String getSectionId() {
        return SectionId;
    }

    public void setSectionId(String sectionId) {
        SectionId = sectionId;
    }

    public String getSubjectId() {
        return SubjectId;
    }

    public void setSubjectId(String subjectId) {
        SubjectId = subjectId;
    }

    public String getTeacherId() {
        return TeacherId;
    }

    public void setTeacherId(String teacherId) {
        TeacherId = teacherId;
    }

    public String getSection() {
        return Section;
    }

    public void setSection(String section) {
        Section = section;
    }

    public String getTeacher() {
        return Teacher;
    }

    public void setTeacher(String teacher) {
        Teacher = teacher;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getTeacherName() {
        return TeacherName;
    }

    public void setTeacherName(String teacherName) {
        TeacherName = teacherName;
    }

    public String getSectionName() {
        return SectionName;
    }

    public void setSectionName(String sectionName) {
        SectionName = sectionName;
    }

    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        EmailAddress = emailAddress;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.AssignmentId);
        dest.writeString(this.SchoolId);
        dest.writeString(this.ClassId);
        dest.writeInt(this.Class_Id);
        dest.writeString(this.SectionId);
        dest.writeString(this.SubjectId);
        dest.writeString(this.TeacherId);
        dest.writeString(this.Section);
        dest.writeString(this.Teacher);
        dest.writeString(this.ClassName);
        dest.writeString(this.SubjectName);
        dest.writeString(this.TeacherName);
        dest.writeString(this.SectionName);
        dest.writeString(this.EmailAddress);
        dest.writeString(this.ImageUrl);
        dest.writeString(this.IsActive);
    }

    public AssignedTeacherDetails() {
    }

    protected AssignedTeacherDetails(Parcel in) {
        this.AssignmentId = in.readString();
        this.SchoolId = in.readString();
        this.ClassId = in.readString();
        this.Class_Id = in.readInt();
        this.SectionId = in.readString();
        this.SubjectId = in.readString();
        this.TeacherId = in.readString();
        this.Section = in.readString();
        this.Teacher = in.readString();
        this.ClassName = in.readString();
        this.SubjectName = in.readString();
        this.TeacherName = in.readString();
        this.SectionName = in.readString();
        this.EmailAddress = in.readString();
        this.ImageUrl = in.readString();
        this.IsActive = in.readString();
    }

    public static final Creator<AssignedTeacherDetails> CREATOR = new Creator<AssignedTeacherDetails>() {
        @Override
        public AssignedTeacherDetails createFromParcel(Parcel source) {
            return new AssignedTeacherDetails(source);
        }

        @Override
        public AssignedTeacherDetails[] newArray(int size) {
            return new AssignedTeacherDetails[size];
        }
    };
}
