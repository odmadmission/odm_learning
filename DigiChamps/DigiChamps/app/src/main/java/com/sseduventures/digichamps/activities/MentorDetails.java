package com.sseduventures.digichamps.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.utils.CircleTransform;

import de.hdodenhof.circleimageview.CircleImageView;

public class MentorDetails extends FormActivity {


    TextView remakrs_today, txt_goal, mentor_email, mentor_phone, teacher_name;
    CircleImageView header_image;
    private Toolbar toolbar;

    String remarks, goal, email, phone, name, image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_mentor);
        setModuleName(LogEventUtil.EVENT_Mentor_details);

        logEvent(LogEventUtil.KEY_Know_your_Mentor_PAGE,
                LogEventUtil.EVENT_KnowYourMentor_PAGE);
        Intent in = getIntent();

        if (in != null) {
            name = in.getStringExtra("TName");
            image = in.getStringExtra("TImage");
            email = in.getStringExtra("TMail");
            phone = in.getStringExtra("TPh");
            goal = in.getStringExtra("Goal");
            remarks = in.getStringExtra("Tremarks");
        }

        remakrs_today = (TextView) findViewById(R.id.remakrs_today);
        txt_goal = (TextView) findViewById(R.id.txt_goal);
        mentor_email = (TextView) findViewById(R.id.mentor_email);
        mentor_phone = (TextView) findViewById(R.id.mentor_phone);
        teacher_name = (TextView) findViewById(R.id.teacher_name);
        header_image = (CircleImageView) findViewById(R.id.header_image);
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_backarrow));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        if (goal != null) {
            txt_goal.setText(goal);
        } else {
            txt_goal.setText("Goal is not yet set");
        }
        mentor_email.setText(email);
        mentor_phone.setText(phone);
        teacher_name.setText(name);

        mentor_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (phone != null) {

                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + phone));
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        mentor_email.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (email != null) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("mailto:" + email));
                        startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        if (remarks == null || remarks.equalsIgnoreCase("null")) {
            remakrs_today.setText("No Remarks");
        } else {
            remakrs_today.setText(remarks);
        }


        Picasso.with(getApplicationContext()).load(image)
                .transform(new CircleTransform()).into(header_image);


    }

    @Override
    public void onBackPressed() {

        Intent in = new Intent(MentorDetails.this, PersonalMentorActivity.class);
        startActivity(in);
        finish();

    }
}