package com.sseduventures.digichamps.domain;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by NISHIKANT on 7/3/2018.
 */

public class NcertSolutionListData implements Parcelable{

    private int NCERTSolutions_ID;
    private String NCERTSolutions_PDFName;
    private String PDF_UploadPath;
    private String PDFOnline;
    private String PDFBeta;

    public int getNCERTSolutions_ID() {
        return NCERTSolutions_ID;
    }

    public void setNCERTSolutions_ID(int NCERTSolutions_ID) {
        this.NCERTSolutions_ID = NCERTSolutions_ID;
    }

    public String getNCERTSolutions_PDFName() {
        return NCERTSolutions_PDFName;
    }

    public void setNCERTSolutions_PDFName(String NCERTSolutions_PDFName) {
        this.NCERTSolutions_PDFName = NCERTSolutions_PDFName;
    }

    public String getPDF_UploadPath() {
        return PDF_UploadPath;
    }

    public void setPDF_UploadPath(String PDF_UploadPath) {
        this.PDF_UploadPath = PDF_UploadPath;
    }

    public String getPDFOnline() {
        return PDFOnline;
    }

    public void setPDFOnline(String PDFOnline) {
        this.PDFOnline = PDFOnline;
    }

    public String getPDFBeta() {
        return PDFBeta;
    }

    public void setPDFBeta(String PDFBeta) {
        this.PDFBeta = PDFBeta;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.NCERTSolutions_ID);
        dest.writeString(this.NCERTSolutions_PDFName);
        dest.writeString(this.PDF_UploadPath);
        dest.writeString(this.PDFOnline);
        dest.writeString(this.PDFBeta);
    }

    public NcertSolutionListData() {
    }

    protected NcertSolutionListData(Parcel in) {
        this.NCERTSolutions_ID = in.readInt();
        this.NCERTSolutions_PDFName = in.readString();
        this.PDF_UploadPath = in.readString();
        this.PDFOnline = in.readString();
        this.PDFBeta = in.readString();
    }

    public static final Creator<NcertSolutionListData> CREATOR = new Creator<NcertSolutionListData>() {
        @Override
        public NcertSolutionListData createFromParcel(Parcel source) {
            return new NcertSolutionListData(source);
        }

        @Override
        public NcertSolutionListData[] newArray(int size) {
            return new NcertSolutionListData[size];
        }
    };
}
