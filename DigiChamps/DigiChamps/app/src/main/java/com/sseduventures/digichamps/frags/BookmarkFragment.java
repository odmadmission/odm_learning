package com.sseduventures.digichamps.frags;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sseduventures.digichamps.R;
import com.sseduventures.digichamps.activities.NewDashboardActivity;
import com.sseduventures.digichamps.firebase.LogEventUtil;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks1;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks2;
import com.sseduventures.digichamps.fragment.Fragment_Bookmarks3;
import com.sseduventures.digichamps.utils.AppUtil;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class BookmarkFragment extends BaseFragment
{


    private View mView;

    private String resp, error;
    private ImageView apptext,dash_but, bookmarks_but, search_but, feed_but, more_but;
    private Toolbar toolbar;
    private String TAG = "login status";
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String User_Id, chapter_id, prtTest = "false";
    private CollapsingToolbarLayout collapsing_toolbar;

    AppBarLayout appBarLayout;

    public static BookmarkFragment getInstance()
    {
        BookmarkFragment fragment=new BookmarkFragment();

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((NewDashboardActivity) getActivity())
                .logEvent(LogEventUtil.KEY_Bookmarks_PAGE,
                        LogEventUtil.EVENT_Bookmarks_PAGE);
        ((NewDashboardActivity) getActivity())
                .setModuleName(LogEventUtil.EVENT_BOOKMARKS_PAGE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             Bundle savedInstanceState)
    {
        mView=inflater.inflate(R.layout.bookmarks_activity,
                container,false);

        ((NewDashboardActivity)getActivity()).updateStatusBarColor("#FFCE5C27");


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) mView.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        appBarLayout =(AppBarLayout)mView. findViewById(R.id.appbar);
        collapsing_toolbar =(CollapsingToolbarLayout)mView.findViewById(R.id.collapsing_toolbar);
        setupTabIcons();

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsing_toolbar.setTitle("Bookmark");
                    isShow = true;
                } else if (isShow) {
                    collapsing_toolbar.setTitle(" ");//carefull there should a space between double quote otherwise it wont work
                    isShow = false;
                }
            }
        });

        init();
        return mView;

    }


    @Override
    public void onResume() {
        super.onResume();

    }

    //back navigation
    public void ReturnHome(View view){
        Intent in = new Intent(getActivity(),NewDashboardActivity.class);
        startActivity(in);

    }

    public void init() {
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dash_but = (ImageView) mView.findViewById(R.id.home_dash);
        bookmarks_but = (ImageView) mView.findViewById(R.id.bookmark_dash);
        search_but = (ImageView) mView.findViewById(R.id.search_dash);
        feed_but = (ImageView) mView.findViewById(R.id.feed_dash);
        more_but = (ImageView) mView.findViewById(R.id.more_dash);


    }


    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("VIDEOS");
        tabOne.setTextSize(13);
        tabOne.setTextColor(getResources().getColor(R.color.light_black));//New
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/911Fonts.com_CenturyGothicRegular__-_911fonts.com_fonts_mhpY.ttf");
        tabOne.setTypeface(face);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabTwo.setText("STUDY NOTES");
        tabTwo.setTextSize(13);
        tabTwo.setTypeface(face);
        tabTwo.setTextColor(getResources().getColor(R.color.light_black));//New
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabThree.setText("QUESTION BANKS");
        tabThree.setTextSize(13);
        tabThree.setTypeface(face);
        tabThree.setTextColor(getResources().getColor(R.color.light_black));//New
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new
                ViewPagerAdapter(getFragmentManager());
        adapter.addFrag(new Fragment_Bookmarks1(), "VIDEOS");
        adapter.addFrag(new Fragment_Bookmarks2(), "STUDY NOTES");
        adapter.addFrag(new Fragment_Bookmarks3(), "QUESTION BANKS");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(android.support.v4.app.FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
